/**
 * Automatically generated file. DO NOT MODIFY
 */
package pl.aprilapps.easyphotopicker;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "pl.aprilapps.easyphotopicker";
  public static final String BUILD_TYPE = "release";
}
