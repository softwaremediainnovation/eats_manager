package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class SplashScreenActivity extends AppCompatActivity {

    private TextView txtVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            //change font app


            //hidden keyboard
            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            //Remove title bar
            this.requestWindowFeature(Window.FEATURE_NO_TITLE);

            //Remove notification bar
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

            setContentView(R.layout.activity_splash_screen);
            txtVersion = findViewById(R.id.txtVersion);
            try {
                String mCurrentVersion = SplashScreenActivity.this.getPackageManager().getPackageInfo(SplashScreenActivity.this.getPackageName(), 0).versionName;
                txtVersion.setText(getApplicationInfo().loadLabel(getPackageManager()).toString() + " " + getResources().getString(R.string.title_versi) + " " +  mCurrentVersion);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }


            //change transition open and close layout
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                }
            }, 3000L); //3000 L = 3 detik

        } catch (Exception e) {
            Log.e("errrorr", e.getMessage());
        }

    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasaIndonesia(SplashScreenActivity.this);


    }


    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "in";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }





}
