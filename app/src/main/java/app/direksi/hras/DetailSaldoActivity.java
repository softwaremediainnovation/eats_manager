package app.direksi.hras;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.ResponseSaldoXendit;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class DetailSaldoActivity extends AppCompatActivity {


    public TextView txtSaldo;


    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private RelativeLayout rllayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailSaldoActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_saldo));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        setContentView(R.layout.activity_detail_saldo);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        txtSaldo = findViewById(R.id.txtSaldo);
        rllayout = findViewById(R.id.rllayout);
        rllayout.setVisibility(View.GONE);

        loading = new SweetAlertDialog(DetailSaldoActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

         CekSaldo();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




    public String getBase64(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.NO_WRAP);
    }


    public void CekSaldo() {
        loading.show();

        try {

            String username = (PreferenceManager.getDefaultSharedPreferences(DetailSaldoActivity.this).getString(
                    getResources().getString(R.string.pref_key_xendit), ""));
            String password = "";
            String svcCredentials = "Basic "+ getBase64(username + ":" + password);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url_xendid))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseSaldoXendit> call = api.getSaldo(svcCredentials);

            call.enqueue(new Callback<ResponseSaldoXendit>() {
                @Override
                public void onResponse(Call<ResponseSaldoXendit> call, Response<ResponseSaldoXendit> response) {


                    if (response.isSuccessful()) {
                        loading.dismiss();
                        rllayout.setVisibility(View.VISIBLE);
                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        txtSaldo.setText(String.valueOf(response.body().getBalance() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getBalance()) : "-"));

                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(DetailSaldoActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseSaldoXendit> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(DetailSaldoActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }catch (Exception r){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(DetailSaldoActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            }catch (Exception r){

            }
        }
    }


    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailSaldoActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}