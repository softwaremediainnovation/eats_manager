package app.direksi.hras;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;

import app.direksi.hras.fragment.BePartnerStepFiveFragment;
import app.direksi.hras.fragment.BePartnerStepFourFragment;
import app.direksi.hras.fragment.BePartnerStepOneFragment;
import app.direksi.hras.fragment.BePartnerStepThreeFragment;
import app.direksi.hras.fragment.BePartnerStepTwoFragment;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.badoualy.stepperindicator.StepperIndicator;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class BeriPenilaianActivity extends AppCompatActivity implements BePartnerStepOneFragment.OnStepOneListener,
        BePartnerStepTwoFragment.OnStepTwoListener, BePartnerStepThreeFragment.OnStepThreeListener,
        BePartnerStepFourFragment.OnStepFourListener, BePartnerStepFiveFragment.OnStepFiveListener{


    private SectionsPagerAdapter mSectionsPagerAdapter;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private NonSwipeableViewPager mViewPager;

    private StepperIndicator stepperIndicator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(BeriPenilaianActivity.this);

        //remove line in bar
        //getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.beri_penilaian));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        setContentView(R.layout.activity_beri_penilaian);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.setOffscreenPageLimit(5);

        stepperIndicator = findViewById(R.id.stepperIndicator);


        stepperIndicator.showLabels(false);
        stepperIndicator.setViewPager(mViewPager);
        // or keep last page as "end page"
        stepperIndicator.setViewPager(mViewPager, mViewPager.getAdapter().getCount() - 1); //

        /*// or manual change
        indicator.setStepCount(3);
        indicator.setCurrentStep(2);
*/

    }




  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_become_partner, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/


    public class SectionsPagerAdapter extends androidx.fragment.app.FragmentPagerAdapter {

        public SectionsPagerAdapter(androidx.fragment.app.FragmentManager fm) {
            super(fm);
        }

        @Override
        public androidx.fragment.app.Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    return BePartnerStepOneFragment.newInstance("", "");
                case 1:
                    return BePartnerStepTwoFragment.newInstance("", "");
                case 2:
                    return BePartnerStepThreeFragment.newInstance("", "");
                case 3:
                    return BePartnerStepFourFragment.newInstance("", "");
                case 4:
                    return BePartnerStepFiveFragment.newInstance("", "");
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 5;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "1";
                case 1:
                    return "2";
                case 2:
                    return "3";
                case 3:
                    return "4";
                case 4:
                    return "5";
            }
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                new android.app.AlertDialog.Builder(BeriPenilaianActivity.this)
                        .setTitle(getResources().getString(R.string.title_apakah_anda_yakin_ingin_keluar))
                        .setMessage(getResources().getString(R.string.title_semua_data_tidak_disimpan))
                        .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                finish();

                            }

                        })
                        .setNegativeButton(getResources().getString(R.string.txt_no), null)
                        .show();

                // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onNextPressed(Fragment fragment) {
        if (fragment instanceof BePartnerStepOneFragment) {
            mViewPager.setCurrentItem(1, true);
        } else if (fragment instanceof BePartnerStepTwoFragment) {
            mViewPager.setCurrentItem(2, true);
        } else if (fragment instanceof BePartnerStepThreeFragment) {
            mViewPager.setCurrentItem(3, true);
        } else if (fragment instanceof BePartnerStepFourFragment) {
            mViewPager.setCurrentItem(4, true);
        } else if (fragment instanceof BePartnerStepFiveFragment) {
           // Toast.makeText(this, "Thanks For Voting", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    public void onBackPressed(Fragment fragment) {
        if (fragment instanceof BePartnerStepTwoFragment) {
            mViewPager.setCurrentItem(0, true);
        } else if (fragment instanceof BePartnerStepThreeFragment) {
            mViewPager.setCurrentItem(1, true);
        }else if (fragment instanceof BePartnerStepFourFragment) {
            mViewPager.setCurrentItem(2, true);
        }else if (fragment instanceof BePartnerStepFiveFragment) {
            mViewPager.setCurrentItem(3, true);
        }
    }

    @Override
    public void onBackPressed()
    {

        new android.app.AlertDialog.Builder(BeriPenilaianActivity.this)
                .setTitle(getResources().getString(R.string.title_apakah_anda_yakin_ingin_keluar))
                .setMessage(getResources().getString(R.string.title_semua_data_tidak_disimpan))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();

    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(BeriPenilaianActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}