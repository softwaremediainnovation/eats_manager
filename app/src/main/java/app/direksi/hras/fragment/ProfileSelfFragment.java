package app.direksi.hras.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ChangePasswordActivity;
import app.direksi.hras.HomeActivity;
import app.direksi.hras.R;
import app.direksi.hras.ZoomPhotoActivity;
import app.direksi.hras.model.ResponseUploadPhoto;
import app.direksi.hras.util.CompressImage;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 18/06/2019.
 */

public class ProfileSelfFragment extends Fragment {

    private TextView txtNama, txtJabatan, txtEmail, txtPhone, txtAddress, txtNIK,
            txtDob, txtExpiredContract, txtGender, txtJoinDate, txtJatah, txtRekening, txtID;
    private ImageView profile_image, profile_image2;
    private DateFormat sdf, mViewSdf;
    private CardView cardLayout;
    private static final int REQUEST_PHONE_CALL = 1;
    private Button btnChangePassword;
    private Uri returnUri = null;
    private SweetAlertDialog loading;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static Fragment newInstance() {
        return new ProfileSelfFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile_self, container, false);
        txtID = v.findViewById(R.id.txtID);
        txtRekening = v.findViewById(R.id.txtRekening);
        txtJatah = v.findViewById(R.id.txtJatah);
        txtJoinDate = v.findViewById(R.id.txtJoinDate);
        txtExpiredContract = v.findViewById(R.id.txtExpiredContract);
        txtDob = v.findViewById(R.id.txtDob);
        txtGender = v.findViewById(R.id.txtGender);
        txtNama = v.findViewById(R.id.txtNama);
        txtJabatan = v.findViewById(R.id.txtJabatan);
        txtEmail = v.findViewById(R.id.txtEmail);
        txtPhone = v.findViewById(R.id.txtPhone);
        txtAddress = v.findViewById(R.id.txtAddress);
        txtNIK = v.findViewById(R.id.txtNIK);
        profile_image = v.findViewById(R.id.profile_image);
        profile_image2 = v.findViewById(R.id.profile_image2);
        cardLayout = v.findViewById(R.id.cardLayout);
        btnChangePassword = v.findViewById(R.id.btnChangePassword);


        txtID.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_employeeid), ""));

        txtRekening.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_rekening), ""));

        txtJatah.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_jatah), ""));

        txtGender.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_gender), ""));

        txtNama.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_nama), ""));
        txtJabatan.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_role), ""));
        txtEmail.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_email), ""));
        txtPhone.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_phone), ""));
        txtAddress.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_adress), ""));
        txtNIK.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_nik), ""));
        String dob = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_dob), ""));

        if(dob == null){

        } else {

            try {
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date a = dateFormat.parse(dob);

                txtDob.setText(mViewSdf.format(a.getTime()));
            } catch (Exception e){

            }

        }

        String expired = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_contract_expired), ""));

        if(expired == null){

           // txtExpiredContract.setText("[TETAP]");

        } else {

            try {
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date a = dateFormat.parse(expired);

                txtExpiredContract.setText(mViewSdf.format(a.getTime()));
            } catch (Exception e){

            }

        }

        String joinDate = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_join_date), ""));

        if(joinDate == null){

        } else {

            try {
                sdf = new SimpleDateFormat("yyyy-MM-dd");
                mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date a = dateFormat.parse(joinDate);

                txtJoinDate.setText(mViewSdf.format(a.getTime()));
            } catch (Exception e){

            }

        }



        String url = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getResources().getString(R.string.pref_photourl), "");

        if (url == null || url.equals(getContext().getResources().getString(R.string.base_url))){

            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);

            Glide.with(getContext())
                    .load(getResources().getDrawable(R.drawable.profile))
                    .error(Glide.with(profile_image).load(R.drawable.profile))
                    .into(profile_image);

        }else {
            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);
            Glide.with(getContext())
                    .load(  PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getResources().getString(R.string.pref_photourl), ""))
                    .into(profile_image);

            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ZoomPhotoActivity.class);
                    intent.putExtra("image", url);
                    startActivity(intent);
                }
            });
        }

        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent intent = new Intent(getContext(), ChangePasswordActivity.class);
                startActivity(intent);*/


                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Bahasa");
                // Add the buttons
                builder.setPositiveButton("en", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        String languageToLoad = "en"; // your language
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getContext().getResources().updateConfiguration(config,
                                getContext().getResources().getDisplayMetrics());
                        dialog.dismiss();
                       /* rEditor.putString("language", languageToLoad);
                        rEditor.commit();*/



                        Intent refresh = new Intent(getActivity(), HomeActivity.class);
                        startActivity(refresh);
                        getActivity().finish();

                    }
                });
                builder.setNegativeButton("id", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog

                        String languageToLoad = "id"; // your language
                        Locale locale = new Locale(languageToLoad);
                        Locale.setDefault(locale);
                        Configuration config = new Configuration();
                        config.locale = locale;
                        getContext().getResources().updateConfiguration(config,
                                getContext().getResources().getDisplayMetrics());
                        dialog.dismiss();
                       /* rEditor.putString("language", languageToLoad);
                        rEditor.commit();*/


                        Intent refresh = new Intent(getActivity(), HomeActivity.class);
                        startActivity(refresh);
                        getActivity().finish();

                    }
                });

                builder.create().show();
            }
        });
      /*  profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });*/
       /* profile_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });*/




        return v;
    }

    private void startCropImageActivity() {
       /* CropImage.activity()
                .start(getActivity(), this);*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {
                // Uri resultUri = result.getUri();

                Bitmap mImageBitmap = CompressImage.loadBitmap(result.getUri().toString());
                Matrix matrix = new Matrix();
                int width = 0;
                int height = 0;
                try {

                    matrix.postRotate(0);
                    if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                        width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                        height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                    } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                        width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                        height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                    } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                        width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                        height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                    } else {

                        width = mImageBitmap.getWidth();
                        height = mImageBitmap.getHeight();
                    }

                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                    //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                    Uri uri = CompressImage.getImageUri(getActivity(), scaledBitmap);
                    *//*END OF COMPRESS FEATURE*//*
                    returnUri = uri;
                    mCreateDayOff();

                } catch (Exception e){

                }
                *//*returnUri = result.getUri();
                Bitmap bitmapImage = null;

                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                } catch (IOException e) {
                   // Log.v(TAG, e.getMessage());
                }

                Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, bitmapImage);

                String a = Path.getRealPathFromURI(getActivity(), returnUri);

                try {

                  *//**//*  mImageChoose.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(android.R.color.transparent);
                    mImageView.setImageBitmap(BitmapImage.modifyOrientation(scaled, a));*//**//*

                    mCreateDayOff();



                } catch (Exception e) {
                    e.printStackTrace();
                }*//*
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }*/
    }



    public void mCreateDayOff() {

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        loading.show();



        String ids = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_employeeid), ""));

        RequestBody id = RequestBody.create(MediaType.parse("text/plain"), ids);

        //File creating from selected URL
        File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
        MultipartBody.Part multipart = null;
        if (file.exists()) {
            String name = URLConnection.guessContentTypeFromName(file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
            multipart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
        }

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseUploadPhoto> call = api.uploadImage(Authorization,id, multipart);

        call.enqueue(new Callback<ResponseUploadPhoto>() {
            @Override
            public void onResponse(Call<ResponseUploadPhoto> call, Response<ResponseUploadPhoto> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                          /*  dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.asset_success), Toast.LENGTH_SHORT).show();


                            Intent intent = new Intent(getActivity(), AssetNewActivity.class);
                            startActivity(intent);
                            getActivity().finish();*/

                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_photourl), (getResources().getString(R.string.base_url) ) + response.body().getData().getUrl())
                                    .apply();


                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));

                           /* new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), AssetNewActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);
                                        }
                                    })
                                    .show();*/

                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), response.body().getErrMessage(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                       // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseUploadPhoto> call, Throwable t) {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

            }
        });

    }




}
