package app.direksi.hras.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ClaimActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataGetActivityForReimburse;
import app.direksi.hras.model.DataKasbon;
import app.direksi.hras.model.ResponseCreateReimbursement;
import app.direksi.hras.model.ResponseGetActivityForReimburse;
import app.direksi.hras.model.ResponseGetKasbon;
import app.direksi.hras.model.ResponseReimbursCategory;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_1;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_2;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_3;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.app.AppConstants.addMaxdaysReimburse;
import static app.direksi.hras.app.AppConstants.addMindaysReimburse;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateReimbursementFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateReimbursementFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;


    private EditText mEdtMulai,
            mEdtCategory,
            mJumlah,
            mEdtNote,
            mEdtActivity,
            mEdtKasbon;

    private String jamServer = "";
    private String mIdActivity = "0", mIdKasbon = "0";


    private Integer mIdCategory[];
    private String mNamaCategory[];
    private Button mButtonSend, mButtonCancel;
    private Integer category, activity;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri = null;
    private SweetAlertDialog loading;
    private ImageView imgClose;
    private List<DataGetActivityForReimburse> dataFetchActivityItems = new ArrayList<>();
    private List<DataKasbon> dataFetchKasbonItems = new ArrayList<>();
    private TextInputLayout mKasbonLayout, mActivityLayout;
    private File file;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_reimbursement, container, false);

        imgClose = rootView.findViewById(R.id.imgClose);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mActivityLayout = rootView.findViewById(R.id.mActivityLayout);
        mKasbonLayout = rootView.findViewById(R.id.mKasbonLayout);
        mEdtKasbon = rootView.findViewById(R.id.mEdtKasbon);
        mEdtActivity = rootView.findViewById(R.id.mEdtActivity);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        mJumlah = rootView.findViewById(R.id.mJumlah);
        imgClose.setOnClickListener(this);

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMM yyyy");


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                /*if (enteredString.startsWith("0")) {
                    MDToast.makeText(getActivity(), "Tidak bisa dimulai dengan nol (0)",
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                    if (enteredString.length() > 0) {
                        mJumlah.setText(enteredString.substring(1));
                    } else {
                        mJumlah.setText("");
                    }
                }*/

                if (enteredString.length() >= 12){
                    String str = enteredString.substring(0, 11);
                    mJumlah.setText(str);
                    mJumlah.setSelection(str.length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mJumlah.getText().hashCode() == s.hashCode()) {
                    mJumlah.removeTextChangedListener(this);
                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                    symbols.setGroupingSeparator(',');
                    try {
                        mJumlah.setText(formatter.format(Double.parseDouble(s.toString().replace(",", "") + "")));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    } catch (Exception e) {
                        mJumlah.setText(s.toString().replace(",", ""));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    }
                    mJumlah.addTextChangedListener(this);
                }

            }


        };
        mJumlah.addTextChangedListener(mPriceWatcher);


      /*  mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               *//* openDateRangePicker();*//*
                openDateRangePickerStart();
            }
        });*/


        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mEdtCategory = rootView.findViewById(R.id.mEdtCategory);
        mEdtCategory.setOnClickListener(this);
        mButtonSend.setOnClickListener(this);
        mEdtMulai.setOnClickListener(this);
        mEdtActivity.setOnClickListener(this);
        mEdtKasbon.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });


        mGetConstanta();
        mGetServerTim();


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(getActivity(), this);
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }

    public void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        returnUri = Uri.fromFile(photo);
        startActivityForResult(intent, 100);
        Log.v(TAG, "takePhoto " + 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    Bitmap mImageBitmap = BitmapFactory.decodeStream(inputStream);
                    int width = 0;
                    int height = 0;
                    try {


                        if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                        } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                        } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                        } else {

                            width = mImageBitmap.getWidth();
                            height = mImageBitmap.getHeight();
                        }

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                        //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                        // Uri uri = CompressImage.getImageUri(getActivity(), scaledBitmap);
                        String urr = CompressImage.makeBitmapPath(getActivity(), scaledBitmap);

                        file = new File (urr);
                        returnUri = result.getUri();

                        mImageChoose.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        mImageView.setImageBitmap(scaledBitmap);

                    } catch (Exception e){

                        try {
                            String urr = CompressImage.makeBitmapPath(getActivity(), mImageBitmap);
                            file = new File (urr);
                            returnUri = result.getUri();

                            mImageChoose.setVisibility(View.GONE);
                            mImageView.setVisibility(View.VISIBLE);
                            mImageView.setImageURI(result.getUri());

                        } catch (Exception f){
                            MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                            //Toast.makeText(getActivity(), "failed", Toast.LENGTH_LONG).show();
                        }


                    } finally {
                        try {
                            inputStream.close();

                        } catch (Exception e){

                        }

                    }

                } catch (IOException e){
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

            }
        }
    }

    public static class DateUtil {
        public static Date addDays(Date date, int days) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, days); //minus number would decrement the days
            return cal.getTime();
        }
    }


    private void openDateRangePicker() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {


                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(true);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseReimbursCategory> call = api.mGetCategoryReimburse(Authorization);

            call.enqueue(new Callback<ResponseReimbursCategory>() {
                @Override
                public void onResponse(Call<ResponseReimbursCategory> call, Response<ResponseReimbursCategory> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getDataReimbursCategory().size() > 0) {
                                mIdCategory = new Integer[response.body().getDataReimbursCategory().size()];
                                mNamaCategory = new String[response.body().getDataReimbursCategory().size()];

                                for (int i = 0; i < response.body().getDataReimbursCategory().size(); i++) {
                                    mIdCategory[i] = response.body().getDataReimbursCategory().get(i).getReimbursementCategoryID();
                                    mNamaCategory[i] = response.body().getDataReimbursCategory().get(i).getName();
                                }

                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseReimbursCategory> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }




    public void mCreateDayOff() {


        try {

            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), category.toString());
            RequestBody KasbonID = RequestBody.create(MediaType.parse("text/plain"), mIdKasbon);
            RequestBody ActivityID = RequestBody.create(MediaType.parse("text/plain"), mIdActivity);
            RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"),
                    PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_employeeid), ""));
            RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), dtStartPost + "T" + jmStart);
            RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));

            RequestBody Amount = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mJumlah).replaceAll(",", ""));


            //File creating from selected URL
            //File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
            MultipartBody.Part multipart = null;
            if (file.exists()) {
                String name = URLConnection.guessContentTypeFromName(file.getName());
                RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
                multipart = MultipartBody.Part.createFormData("reimbursementFile", file.getName(), requestFile);
            }

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


            // simplified call to request the news with already initialized service
            Call<ResponseCreateReimbursement> call = api.mCreateReimburse(Authorization, id, Date, Amount, Note, EmployeeID, KasbonID, ActivityID, multipart);

            call.enqueue(new Callback<ResponseCreateReimbursement>() {
                @Override
                public void onResponse(Call<ResponseCreateReimbursement> call, Response<ResponseCreateReimbursement> response) {

                    try {
                        loading.dismiss();

                        if (response.isSuccessful()) {
                            int statusCode = response.body().getErrCode();

                            if (statusCode == 0) {

                          /*  dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.reimburse_success), Toast.LENGTH_SHORT).show();


                            Intent intent = new Intent(getActivity(), ClaimActivity.class);
                            startActivity(intent);
                            getActivity().finish();*/

                                SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                alertDialog.setContentText("");
                                alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        Intent login = new Intent(getActivity(), ClaimActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                    }
                                });
                                alertDialog.show();

                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));

                           /* new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), ClaimActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);
                                        }
                                    })
                                    .show();*/

                            } else {

                                try {
                                    loading.dismiss();
                                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                    //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                    redirected();
                                } catch (Exception e) {

                                }

                            }

                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e) {

                            }
                        }
                    } catch (Exception e) {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception s) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseCreateReimbursement> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception e) {

                    }
                }
            });
        } catch (Exception e){
            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                redirected();
            } catch (Exception s) {

            }
        }

    }

    private void mAttemptCreateDayOff() {

        mEdtMulai.setError(null);
        mEdtNote.setError(null);
        mJumlah.setError(null);
        mEdtCategory.setError(null);
        mEdtKasbon.setError(null);


        String start = mEdtMulai.getText().toString();
        String note = mEdtNote.getText().toString();
        String categories = mEdtCategory.getText().toString();
        String jumlah = mJumlah.getText().toString();
        String kasbon = mEdtKasbon.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(start) || start.contains("null")) {
            mEdtMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtMulai;
            cancel = true;
        }


        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }

        if (TextUtils.isEmpty(categories)) {
            mEdtCategory.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtCategory;
            cancel = true;
        }

        if (TextUtils.isEmpty(jumlah)) {
            mJumlah.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mJumlah;
            cancel = true;
        }

        if (category != null) {
            if (category == 7 & TextUtils.isEmpty(kasbon)) {
                mEdtKasbon.setError(getResources().getString(R.string.title_isian_harus_diisi));
                focusView = mEdtKasbon;
                cancel = true;
            }
        }


        if (returnUri == null) {
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_silahkan_pilih_foto_dahulu),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            // Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loading.show();
            mCreateDayOff();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mEdtCategory) {
            try {
                if (mIdCategory.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mEdtCategory.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaCategory, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mEdtCategory.setText(mNamaCategory[item]);
                            category = mIdCategory[item];
                            imm.hideSoftInputFromWindow(mEdtCategory.getWindowToken(), 0);
                            mIdKasbon = "0";
                            mEdtKasbon.setText("");
                            mIdActivity = "0";
                            mEdtActivity.setText("");
                            if (category == 7){
                                mKasbonLayout.setVisibility(View.VISIBLE);
                                mActivityLayout.setVisibility(View.GONE);


                            } else {
                                mKasbonLayout.setVisibility(View.GONE);
                                mActivityLayout.setVisibility(View.VISIBLE);
                            }

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        }  else if (v.getId() == R.id.mEdtMulai) {
            if (jamServer.equals("")){
                mGetServerTim();
            } else {
                openDateRangePickerStart();
            }

        }  else if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        } else if (v.getId() == R.id.mEdtActivity) {

            if (TextUtils.isEmpty(mEdtMulai.getText().toString()) || mEdtMulai.getText().toString().contains("null")) {

                MDToast.makeText(getActivity(), getResources().getString(R.string.title_tanggal_harap_diisi_dahulu),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
              /*  Toast.makeText(getActivity(),
                        "Tanggal harus diisi dulu",
                        Toast.LENGTH_SHORT).show();*/

            } else {
                dataFetchActivityItems.clear();
                showListActivity();

            }


        } else if (v.getId() == R.id.mEdtKasbon) {
            dataFetchKasbonItems.clear();
            showListKasbon();
        }

    }


    TimePickerDialog timePickerDialog;
    int hour1,minutes1;
    int cYear,cMonth,cDay, cHour, cMinute;
    private String dtStartPost, dtStart, dtEndPost, dtEnd, jmStart, jmEnd, fullStart, fullEnd;

    private void openDateRangePickerStart() {
        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            calstart.setTime(date);
            calEnd.setTime(date);
            calstart.add(Calendar.DAY_OF_YEAR, addMindaysReimburse);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdaysReimburse);

          /*  hour1 = cal.HOUR_OF_DAY;
            minutes1 = cal.MINUTE;*/


        } catch (Exception e) {

        }

        Calendar getDate = Calendar.getInstance();

        cDay = cal.get(Calendar.DAY_OF_MONTH);
        cMonth = cal.get(Calendar.MONTH);
        cYear = cal.get(Calendar.YEAR);
        cHour = cal.get(Calendar.HOUR_OF_DAY);
        cMinute = cal.get(Calendar.MINUTE);


        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                month = month + 1;

                String day, mont, yer;
                if (dayOfMonth > 9){
                    day = String.valueOf(dayOfMonth);
                } else {
                    day = "0" + String.valueOf(dayOfMonth);
                }

                if (month > 9){
                    mont = String.valueOf(month);
                } else {
                    mont = "0" + String.valueOf(month);
                }
                yer = String.valueOf(year);


                String formattt = yer + "-" +mont + "-" + day;
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = format.parse(formattt);

                    dtStartPost = sdf.format(date.getTime());
                    dtStart = mViewSdf.format(date.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                cYear = year;
                cMonth = month;
                cDay = dayOfMonth;


                //  mEdtSelesai.setText(hour1 + ":" +minutes1 + AM_PM  + "     " + cDay + "/" + cMonth + "/" + cYear);
                timePickerDialog = new TimePickerDialog(getActivity(),  R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {



                        String jaaam, meniiiit;
                        if (hourOfDay > 9){
                            jaaam = String.valueOf(hourOfDay);
                        }else{
                            jaaam = "0" +String.valueOf(hourOfDay);
                        }
                        if (minute > 9){

                            meniiiit = String.valueOf(minute);
                        }else{
                            meniiiit = "0"+String.valueOf(minute);
                        }
                        hour1 = hourOfDay;
                        minutes1 = minute;
                        jmStart = jaaam + ":" +meniiiit;

                        mEdtMulai.setText(dtStart+  "   " + jmStart);

                        mIdActivity = "0";
                        mEdtActivity.setText("");



                    }
                },cHour,cMinute,true);

                timePickerDialog.show();

            }
        },cYear,cMonth,cDay);

        /*timePickerDialog = new TimePickerDialog(getActivity(),  R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {



                String jaaam, meniiiit;
                if (hourOfDay > 9){
                    jaaam = String.valueOf(hourOfDay);
                }else{
                    jaaam = "0" +String.valueOf(hourOfDay);
                }
                if (minute > 9){

                    meniiiit = String.valueOf(minute);
                }else{
                    meniiiit = "0"+String.valueOf(minute);
                }
                hour1 = hourOfDay;
                minutes1 = minute;
                jmStart = jaaam + ":" +meniiiit;

                mEdtMulai.setText(dtStart+  "   " + jmStart);



            }
        },hour1,minutes1,true);

        timePickerDialog.show();*/

        datePicker.getDatePicker().setMinDate(calstart.getTimeInMillis());
        datePicker.getDatePicker().setMaxDate(calEnd.getTimeInMillis());
        datePicker.show();





    }

    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");*/

                            jamServer = response.body().getData();

                            // mTimeServer = date;

                            // Log.e(TAG, mTimeServer.toString());

                            // timer(mTimeServer);

                        }

                    } catch (Exception e) {
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }

    }

    void redirected (){

        Intent login = new Intent(getActivity(), ClaimActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }


    private void showListActivity() {


        final Dialog overlayInfo = new Dialog(getActivity());
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        overlayInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayInfo.setCancelable(true);
        final View view = inflater.inflate(R.layout.dialog_choose_activity, null);


        final TextView txtEmptyList = (TextView) view.findViewById(R.id.txtEmptyList);
        final ProgressBar loadingTemp = (ProgressBar) view.findViewById(R.id.loadingTemp);
        loadingTemp.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        final LinearLayout containerDialogAddress = (LinearLayout) view.findViewById(R.id.containerCleaner);
        overlayInfo.setContentView(view);
        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(overlayInfo.getWindow().getAttributes());
        lp.width = width - 30;
        overlayInfo.show();
        overlayInfo.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        try {
            txtEmptyList.setVisibility(View.GONE);
            loadingTemp.setVisibility(View.VISIBLE);
            dataFetchActivityItems.clear();
            containerDialogAddress.removeAllViews();

            Map<String, String> data = new HashMap<>();
            data.put("tanggal", String.valueOf(dtStartPost));

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGetActivityForReimburse> call = api.mGetActivityForReimburse(Authorization, data);

            call.enqueue(new Callback<ResponseGetActivityForReimburse>() {
                @Override
                public void onResponse(Call<ResponseGetActivityForReimburse> call, Response<ResponseGetActivityForReimburse> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();
                        loadingTemp.setVisibility(View.GONE);
                        if (statusCode == 0) {

                            Log.e(TAG, response.toString());

                            dataFetchActivityItems = response.body().getData();


                            for (int i = 0; i < dataFetchActivityItems.size(); i++) {
                                final LayoutInflater layoutInflaterDetail = (LayoutInflater)
                                        getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View addDetail = layoutInflaterDetail.inflate(R.layout.item_emp, null);

                                final TextView mTxtName = addDetail.findViewById(R.id.mTxtName);
                                final TextView mTxtHidden = addDetail.findViewById(R.id.mTxtHidden);

                                mTxtName.setText(dataFetchActivityItems.get(i).getDescription());
                                mTxtHidden.setText(Integer.toString(dataFetchActivityItems.get(i).getActivityID()));
                                mTxtName.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mIdActivity = mTxtHidden.getText().toString();

                                        //  Toast.makeText(getActivity(), mIdEmployee, Toast.LENGTH_SHORT).show();
                                        mEdtActivity.setText(mTxtName.getText().toString());
                                        overlayInfo.dismiss();

                                    }
                                });

                                containerDialogAddress.addView(addDetail);
                            }


                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchActivityItems.isEmpty() ? View.VISIBLE : View.GONE);

                        } else {
                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchActivityItems.isEmpty() ? View.VISIBLE : View.GONE);
                        }

                    } else {
                        loadingTemp.setVisibility(View.GONE);
                        txtEmptyList.setVisibility(dataFetchActivityItems.isEmpty() ? View.VISIBLE : View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetActivityForReimburse> call, Throwable t) {
                    loadingTemp.setVisibility(View.GONE);
                    txtEmptyList.setVisibility(dataFetchActivityItems.isEmpty() ? View.VISIBLE : View.GONE);
                }
            });


        } catch (Exception e) {
            loadingTemp.setVisibility(View.GONE);
            txtEmptyList.setVisibility(dataFetchActivityItems.isEmpty() ? View.VISIBLE : View.GONE);
        }



    }

    private void showListKasbon() {


        final Dialog overlayInfo = new Dialog(getActivity());
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        overlayInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayInfo.setCancelable(true);
        final View view = inflater.inflate(R.layout.dialog_choose_activity, null);


        final TextView txtEmptyList = (TextView) view.findViewById(R.id.txtEmptyList);
        final ProgressBar loadingTemp = (ProgressBar) view.findViewById(R.id.loadingTemp);
        loadingTemp.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        final LinearLayout containerDialogAddress = (LinearLayout) view.findViewById(R.id.containerCleaner);
        overlayInfo.setContentView(view);
        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(overlayInfo.getWindow().getAttributes());
        lp.width = width - 30;
        overlayInfo.show();
        overlayInfo.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        try {
            txtEmptyList.setVisibility(View.GONE);
            loadingTemp.setVisibility(View.VISIBLE);
            dataFetchActivityItems.clear();
            containerDialogAddress.removeAllViews();



            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGetKasbon> call = api.mGetKasbonForReimburse(Authorization);

            call.enqueue(new Callback<ResponseGetKasbon>() {
                @Override
                public void onResponse(Call<ResponseGetKasbon> call, Response<ResponseGetKasbon> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();
                        loadingTemp.setVisibility(View.GONE);
                        if (statusCode == 0) {

                            Log.e(TAG, response.toString());

                            dataFetchKasbonItems = response.body().getData();

                            DecimalFormat formatter = new DecimalFormat("#,###,###");
                            for (int i = 0; i < dataFetchKasbonItems.size(); i++) {
                                final LayoutInflater layoutInflaterDetail = (LayoutInflater)
                                        getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                final View addDetail = layoutInflaterDetail.inflate(R.layout.item_emp, null);

                                final TextView mTxtName = addDetail.findViewById(R.id.mTxtName);
                                final TextView mTxtHidden = addDetail.findViewById(R.id.mTxtHidden);

                                mTxtName.setText(dataFetchKasbonItems.get(i).getNote() + "\n"+ "Rp " + formatter.format(dataFetchKasbonItems.get(i).getAmount()));
                                mTxtHidden.setText(dataFetchKasbonItems.get(i).getReimbursementID().toString());
                                mTxtName.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mIdKasbon = mTxtHidden.getText().toString();

                                        //  Toast.makeText(getActivity(), mIdEmployee, Toast.LENGTH_SHORT).show();
                                        mEdtKasbon.setText(mTxtName.getText().toString());
                                        overlayInfo.dismiss();

                                    }
                                });

                                containerDialogAddress.addView(addDetail);
                            }


                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchKasbonItems.isEmpty() ? View.VISIBLE : View.GONE);

                        } else {
                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchKasbonItems.isEmpty() ? View.VISIBLE : View.GONE);
                        }

                    } else {
                        loadingTemp.setVisibility(View.GONE);
                        txtEmptyList.setVisibility(dataFetchKasbonItems.isEmpty() ? View.VISIBLE : View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetKasbon> call, Throwable t) {
                    loadingTemp.setVisibility(View.GONE);
                    txtEmptyList.setVisibility(dataFetchKasbonItems.isEmpty() ? View.VISIBLE : View.GONE);
                }
            });


        } catch (Exception e) {
            loadingTemp.setVisibility(View.GONE);
            txtEmptyList.setVisibility(dataFetchKasbonItems.isEmpty() ? View.VISIBLE : View.GONE);
        }



    }





}