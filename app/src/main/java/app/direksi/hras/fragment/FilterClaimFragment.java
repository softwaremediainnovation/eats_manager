package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ClaimActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseDataListCompanies;
import app.direksi.hras.model.ResponseDataListOrgz;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 26/02/2019.
 */

public class FilterClaimFragment  extends DialogFragment {

    private static final String TAG = FilterTeguranFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;
    private ImageView imgClose;
    private int mIdConstanta;
    private String mNamaConstanta[],  namaCompany[], namaOrg[];
    TextInputLayout mLayoutStatus;
    private SweetAlertDialog loading;
    ArrayList<String> itemsCompany=new ArrayList<>();
    ArrayList<String> itemsOrganization=new ArrayList<>();
    SpinnerDialog spinnerDialogCompany;
    SpinnerDialog spinnerDialogOrg;


    EditText mEdtSelesai, mEdtMulai, mEdtSearch, mEdtStatus,mEdtCompany, mEdtOrg;
    private Integer idCompany[], idOrg[], idCompanyPosition = 0, idOrganizationPosition = 0;


    private Button mButtonSearch,
            mButtonClear;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_filter_claim, container, false);
        mIdConstanta = 0;
        mNamaConstanta = new String[4];
        mNamaConstanta[0] = "Semua Status";
        mNamaConstanta[1] = "Menunggu";
        mNamaConstanta[2] = "Disetujui";
        mNamaConstanta[3] = "Ditolak";

        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mLayoutStatus = rootView.findViewById(R.id.mLayoutStatus);
        imgClose = rootView.findViewById(R.id.imgClose);
        mEdtStatus = rootView.findViewById(R.id.mEdtStatus);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtSearch = rootView.findViewById(R.id.mEdtSearch);
        mButtonSearch = rootView.findViewById(R.id.mButtonSearch);
        mButtonClear = rootView.findViewById(R.id.mButtonClear);

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mEdtCompany = rootView.findViewById(R.id.mEdtCompany);
        mEdtOrg = rootView.findViewById(R.id.mEdtOrg);

        mStartDate = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_start), "");
        mEndDate = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_end), "");


        mEdtMulai.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_start1), ""));
        mEdtSelesai.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_end1), ""));
        mEdtSearch.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_search), ""));

        String status =PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_status), "");

        if (status.equals("0")){
            mEdtStatus.setText("Semua Status");


        } else if (status.equals("1")) {
            mEdtStatus.setText("Menunggu");



        } else if (status.equals("2")) {
            mEdtStatus.setText("Disetujui");



        } else if (status.equals("3")) {
            mEdtStatus.setText("Ditolak");


        }
        String tempidCompany =PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_search_idCompany), "");
        if (!tempidCompany.equals("0")){
            mEdtCompany.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_dayoff_search_namaCompany), ""));
            idCompanyPosition = Integer.valueOf(tempidCompany);
        }

        String tempidOrganization =PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_search_idOrganization), "");
        if (!tempidOrganization.equals("0")){
            mEdtOrg.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_dayoff_search_namaOrganization), ""));
            idOrganizationPosition = Integer.valueOf(tempidOrganization);


        }


        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        mEdtStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mEdtStatus.getWindowToken(), 0);
                final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        mIdConstanta = item;
                       // Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();

                        mEdtStatus.setText(mNamaConstanta[item]);
                        imm.hideSoftInputFromWindow(mLayoutStatus.getWindowToken(), 0);

                    }
                }).show();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        mEdtCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilterCompany();
            }
        });

        mEdtOrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFilterOrg();
            }
        });

        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerStart();
            }
        });

        mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerEnd();
            }
        });

        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEdtCompany.setError(null);
                String comp = mEdtCompany.getText().toString();

                mEdtOrg.setError(null);
                String orgz = mEdtOrg.getText().toString();

                mEdtMulai.setError(null);
                String start = mEdtMulai.getText().toString();

                mEdtSelesai.setError(null);
                String end = mEdtSelesai.getText().toString();


                boolean cancelStart = false;
                boolean cancelEnd = false;
                boolean cancelcomp = false;
                boolean cancelorgz = false;
                View focusView = null;


                if (TextUtils.isEmpty(comp)) {
                    mEdtCompany.setError(getResources().getString(R.string.title_isian_harus_diisi));
                    focusView = mEdtCompany;
                    cancelcomp = true;
                }

               /* if (TextUtils.isEmpty(orgz)) {
                    mEdtOrg.setError("Organisasi harus diisi");
                    focusView = mEdtOrg;
                    cancelorgz = true;
                }*/


                // Check for a valid email address.
                if (TextUtils.isEmpty(start)) {
                    mEdtMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
                    focusView = mEdtMulai;
                    cancelStart = true;
                }

                if (TextUtils.isEmpty(end)) {
                    mEdtSelesai.setError(getResources().getString(R.string.title_isian_harus_diisi));
                    focusView = mEdtSelesai;
                    cancelEnd = true;
                }


                if (cancelStart == true && cancelEnd == false || cancelStart == false && cancelEnd == true ) {

                    mEdtCompany.setError(null);
                    focusView.requestFocus();

                }   else {

                    if ( cancelStart == false && cancelEnd == false && !Validation.mIsValidDateyyMMdd(mStartDate, mEndDate)){
                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_tgl_mulai_lebih_besar),
                                MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                       // Toast.makeText(getActivity(), "Tanggal mulai lebih besar dari tanggal selesai", Toast.LENGTH_SHORT).show();
                    } else {

                        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                edit().putString(getResources().getString(R.string.pref_dayoff_search), Validation.mGetText(mEdtSearch))
                                .putString(getResources().getString(R.string.pref_dayoff_start), mStartDate)
                                .putString(getResources().getString(R.string.pref_dayoff_end), mEndDate)
                                .putString(getResources().getString(R.string.pref_dayoff_start1), Validation.mGetText(mEdtMulai))
                                .putString(getResources().getString(R.string.pref_dayoff_end1), Validation.mGetText(mEdtSelesai))
                                .putString(getResources().getString(R.string.pref_dayoff_search_idCompany),String.valueOf(idCompanyPosition))
                                .putString(getResources().getString(R.string.pref_dayoff_search_idOrganization), String.valueOf(idOrganizationPosition))
                                .putString(getResources().getString(R.string.pref_dayoff_search_namaCompany), Validation.mGetText(mEdtCompany))
                                .putString(getResources().getString(R.string.pref_dayoff_search_namaOrganization), Validation.mGetText(mEdtOrg))
                     /*   .putString(getResources().getString(R.string.pref_dayoff_status), String.valueOf(mIdConstanta))*/
                                .apply();

                  /*  dismiss();

                    Intent intent = new Intent(getActivity(), ClaimActivity.class);
                    startActivity(intent);
                    getActivity().finish();*/
                        ClaimActivity callingActivity = (ClaimActivity) getActivity();
                        callingActivity.onUserSelectValue("insert selected value here");
                        dismiss();
                    }
                }

            }
        });

        mButtonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mEdtCompany.setText("");
                mEdtOrg.setText("");
                mEdtMulai.setText("");
                mEdtSelesai.setText("");
                mEdtSearch.setText("");
                mEdtStatus.setText("Semua Status");
                mIdConstanta = 0;
                idCompanyPosition=0;
                idOrganizationPosition=0;

                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                        .putString(getResources().getString(R.string.pref_dayoff_start), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                        .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                        .putString(getResources().getString(R.string.pref_dayoff_search_idCompany), "0")
                        .putString(getResources().getString(R.string.pref_dayoff_search_idOrganization), "0")
                        .putString(getResources().getString(R.string.pref_dayoff_search_namaCompany), "")
                        .putString(getResources().getString(R.string.pref_dayoff_search_namaOrganization), "")
                       /* .putString(getResources().getString(R.string.pref_dayoff_status), "0")*/
                        .apply();

                mStartDate = "";
                mEndDate = "";


            }
        });


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    @Override
    public void onStart() {


        new CountDownTimer(500, 100) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                mGetCompany();
            }
        }.start();


        String tempidCompany =PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_search_idCompany), "");
        if (!tempidCompany.equals("0")){

            new CountDownTimer(500, 100) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    mGetOrg();
                }
            }.start();

        }


        super.onStart();
    }


    private void openDateRangePickerStart() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    private void openDateRangePickerEnd() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mEndDate = sdf.format(selectedDate.getEndDate().getTime());
                mEdtSelesai.setText(mViewSdf.format(selectedDate.getEndDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    private void  openFilterCompany(){
        if (idCompany == null) {
            mGetCompany();
        } else {
            spinnerDialogCompany.showSpinerDialog();
        }


    }

    private void  openFilterOrg(){
        if (!TextUtils.isEmpty(mEdtCompany.getText().toString())){

            if (idOrg == null){
                mGetOrg();
            }else {
                spinnerDialogOrg.showSpinerDialog();
            }

        } else{
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_harap_pilih_perusahaan_dahulu),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
        }


    }


    public void mGetCompany() {


        try {
            loading.show();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("role", "CanApproveReimbursement");

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataListCompanies> call = api.getCompanies(data,Authorization);

            call.enqueue(new Callback<ResponseDataListCompanies>() {
                @Override
                public void onResponse(Call<ResponseDataListCompanies> call, Response<ResponseDataListCompanies> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {

                                idCompany=null;
                                namaCompany=null;
                                itemsCompany=new ArrayList<>();

                                idCompany = new Integer[response.body().getData().size()];
                                namaCompany = new String[response.body().getData().size()];


                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    idCompany[i] = response.body().getData().get(i).getCompanyID();
                                    namaCompany[i] = response.body().getData().get(i).getCompanyName();
                                    itemsCompany.add(response.body().getData().get(i).getCompanyName());

                                }


                                spinnerDialogCompany=new SpinnerDialog(getActivity(),itemsCompany,getResources().getString(R.string.title_pilih_perusahaan),getResources().getString(R.string.title_tutup));// With No Animation
                                spinnerDialogCompany.setCancellable(true); // for cancellable
                                spinnerDialogCompany.setShowKeyboard(false);// for open keyboard by default
                                spinnerDialogCompany.bindOnSpinerListener(new OnSpinerItemClick() {
                                    @Override
                                    public void onClick(String item, int position) {
                                        //  Toast.makeText(getActivity(), item + "  ID : " + idCompany[position], Toast.LENGTH_SHORT).show();
                                        idCompanyPosition = idCompany[position];
                                        idOrg = null;
                                        namaOrg = null;
                                        itemsOrganization = new ArrayList<>();
                                        mEdtOrg.setText("");
                                        mEdtCompany.setText(item);
                                        mGetOrg();
                                    }
                                });


                            }

                        }
                        else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch (Exception e){

                            }
                        }

                        loading.dismiss();
                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataListCompanies> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch (Exception s){

            }
        }
    }

    public void mGetOrg() {


        try {
            loading.show();
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("CompanyId", String.valueOf(String.valueOf(idCompanyPosition)));
            data.put("role", "CanApproveReimbursement");



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataListOrgz> call = api.getOrg(data, Authorization);

            call.enqueue(new Callback<ResponseDataListOrgz>() {
                @Override
                public void onResponse(Call<ResponseDataListOrgz> call, Response<ResponseDataListOrgz> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {

                                idOrg = null;
                                namaOrg = null;
                                itemsOrganization = new ArrayList<>();

                                idOrg = new Integer[response.body().getData().size()];
                                namaOrg = new String[response.body().getData().size()];


                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    idOrg[i] = response.body().getData().get(i).getOrganizationID();
                                    namaOrg[i] = response.body().getData().get(i).getName();
                                    itemsOrganization.add(response.body().getData().get(i).getName());

                                }

                                spinnerDialogOrg=new SpinnerDialog(getActivity(),itemsOrganization,getResources().getString(R.string.title_pilih_organisasi),getResources().getString(R.string.title_tutup));// With No Animation
                                spinnerDialogOrg.setCancellable(true); // for cancellable
                                spinnerDialogOrg.setShowKeyboard(false);// for open keyboard by default
                                spinnerDialogOrg.bindOnSpinerListener(new OnSpinerItemClick() {
                                    @Override
                                    public void onClick(String item, int position) {
                                        //  Toast.makeText(getActivity(), item + "  ID : " + idOrg[position], Toast.LENGTH_SHORT).show();
                                        mEdtOrg.setText(item);
                                        idOrganizationPosition = idOrg[position];
                                    }
                                });



                            }

                        } else{
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch (Exception e){

                            }
                        }

                        loading.dismiss();
                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataListOrgz> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch (Exception s){

            }
        }
    }


}

