package app.direksi.hras.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.CheckinActivity;
import app.direksi.hras.FingerprintHandler;
import app.direksi.hras.HomeActivity;;
import app.direksi.hras.R;
import app.direksi.hras.app.AppConstants;
import app.direksi.hras.model.DataCompany;
import app.direksi.hras.model.DataDalamDashboard;
import app.direksi.hras.model.ResponseDashboard;
import app.direksi.hras.model.ResponseDataEmployeeID;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.DefaultFormatter;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static app.direksi.hras.app.AppConstants.RANGE_DISTANCE_BOUNDS;

public class FingerPrintFragmentNew extends Fragment implements OnMapReadyCallback {

    private static final String TAG = FingerPrintFragmentNew.class.getSimpleName();
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private TextView textView, mTxtDate;
    private List<DataCompany> dataCompanies;
    private GpsLocationReceiver gpsLocationReceiver;
    private NetworkChangeReceiver networkChangeReceiver;


    SweetAlertDialog mAlertDialog, mAlertInternet, mShowDialog;


    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.USE_FINGERPRINT,
    };
    private PermissionsChecker checker;
    private LayoutInflater mInflater;

    private boolean mIsActiveGPS = false;
    private boolean mIsActiveInternet = true;



    private DateFormat sdf, mViewSdf;
    private Date mTimeServer = new Date();

    private Chronometer cronometer;
    private TextView mTxtTotalLate;

    private Button mButtonCheckin;
    private LinearLayout mLayoutFinger,
            mLayoutNonFinger;

    private LocationManager manager;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private double latitude = 0, longitude = 0, mLatitude = 0, mLongitude = 0;
    private LatLngBounds latLngBounds = null;
    private View mapView;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checker = new PermissionsChecker(getActivity());

        if (checker.lacksPermissions(PERMISSIONS)) {
            startPermissionsActivity(PERMISSIONS);
        }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup viewGroup = (ViewGroup) view;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_fingerprint_home, null);
        mInflater = inflater;

       /* Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins.otf");
        SpannableStringBuilder SS = new SpannableStringBuilder(getResources().getString(R.string.empty));
        SS.setSpan(new HrsApp.CustomTypefaceSpan("", font2), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle(SS);*/

        mTxtTotalLate = v.findViewById(R.id.mTxtTotalLate);
        textView = v.findViewById(R.id.errorText);

        cronometer = v.findViewById(R.id.cronometer);
        textView = v.findViewById(R.id.errorText);
        mTxtDate = v.findViewById(R.id.mTxtDate);
        mButtonCheckin = v.findViewById(R.id.mButtonCheckin);

        mLayoutFinger = v.findViewById(R.id.mLayoutFinger);
        mLayoutNonFinger = v.findViewById(R.id.mLayoutNonFinger);


        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        gpsLocationReceiver = new GpsLocationReceiver();
        IntentFilter intentFilter = new IntentFilter(getResources().getString(R.string.gps_update_filter));
        getActivity().registerReceiver(gpsLocationReceiver, intentFilter);


        networkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter intentNetworkFilter = new IntentFilter(getResources().getString(R.string.network_updater_filter));
        getActivity().registerReceiver(networkChangeReceiver, intentNetworkFilter);

        mAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_gps))
                .setContentText(getResources().getString(R.string.text_gps_nonactive))
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        mIsActiveGPS = false;
                        mAlertDialog.dismiss();
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        mAlertDialog.setCancelable(false);

        mAlertInternet = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_internet))
                .setContentText(getResources().getString(R.string.text_internet_nonactive))
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        try {
                            mIsActiveInternet = true;
                            mAlertInternet.dismiss();
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.android.phone", "com.android.phone.MobileNetworkSettings"));
                            startActivity(intent);
                        } catch (Exception e) {
                        }
                    }
                });

        mAlertInternet.setCancelable(false);


        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            mLayoutFinger.setVisibility(View.GONE);
            mLayoutNonFinger.setVisibility(View.VISIBLE);
            mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mButtonCheckin.setEnabled(false);
                    mButtonCheckin.setClickable(false);

                    Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                    mIntent.putExtra("isFragment", true);
                    startActivity(mIntent);
                }
            });

        } else {

            try {
                FingerprintManager fingerprintManager = null;

                // Initializing both Android Keyguard Manager and Fingerprint Manager
                KeyguardManager keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);
                }

                // Check whether the device has a Fingerprint sensor.
                if (!fingerprintManager.isHardwareDetected()) {
                    // mShowDialog("Device anda tidak support fingerprint", 0);
                    mLayoutFinger.setVisibility(View.GONE);
                    mLayoutNonFinger.setVisibility(View.VISIBLE);
                    mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            mButtonCheckin.setEnabled(false);
                            mButtonCheckin.setClickable(false);

                            Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                            mIntent.putExtra("isFragment", true);
                            startActivity(mIntent);
                        }
                    });
                } else {

                    mLayoutFinger.setVisibility(View.VISIBLE);
                    mLayoutNonFinger.setVisibility(View.GONE);

                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        mShowDialog(getResources().getString(R.string.title_finger_izin_otentikasi), 0);
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            mShowDialog(getResources().getString(R.string.title_finger_daftarkan), 0);
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                mShowDialog(getResources().getString(R.string.title_finger_keamanan), 0);
                            } else {
                                generateKey();

                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    FingerprintHandler helper = new FingerprintHandler(getActivity());
                                    helper.startAuth(fingerprintManager, cryptoObject,  manager.isProviderEnabled(LocationManager.GPS_PROVIDER), dataCompanies);

                                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !mAlertDialog.isShowing()) {
                                        mAlertDialog.show();
                                    } else {
                                        mAlertDialog.dismiss();
                                    }

                                }

                            }
                        }
                    }
                }
            } catch (Exception e) {
                mLayoutFinger.setVisibility(View.GONE);
                mLayoutNonFinger.setVisibility(View.VISIBLE);
                mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mButtonCheckin.setEnabled(false);
                        mButtonCheckin.setClickable(false);


                        Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                        mIntent.putExtra("isFragment", true);
                        startActivity(mIntent);
                    }
                });
            }

        }

        mGetDetailEmp();
        mGetDashboard();
        mGetServerTim();

        if (checkActiveInternet())
            mAlertInternet.show();


        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        mLocationCallback = new LocationCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        };

        getMaps();

        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_finger), "true")
                .apply();


        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_finger), "false")
                .apply();
    }

    public void mGetDetailEmp() {

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .readTimeout(300, TimeUnit.SECONDS)
                    .writeTimeout(300, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployeeID> call = api.getEmployeeID(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""), Authorization);


            call.enqueue(new Callback<ResponseDataEmployeeID>() {
                @Override
                public void onResponse(Call<ResponseDataEmployeeID> call, Response<ResponseDataEmployeeID> response) {
                    try {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0 && response.body().getData().getOrganization() == null) {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.no_organization), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.loading_error), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseDataEmployeeID> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.loading_error), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.loading_error), 1);
        }


    }

    void getMaps() {

        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Bundle extras = null;
                            if (location != null)
                                extras = location.getExtras();
                            boolean isMockLocation = extras != null && extras.getBoolean(FusedLocationProviderApi.KEY_MOCK_LOCATION, false);

                            if (isMockLocation) {
                                mShowDialog(getResources().getString(R.string.title_matikan_mock_lokasi), 2);
                                return;
                            }

                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();

                                mLoadMaps();


                            } else {
                                getLastLocation();
                            }


                        }


                    });

            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getLastLocation();
                    Log.e(TAG, e.getMessage());
                }
            });


            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getLastLocation();
                    Log.e(TAG, e.getMessage());
                }
            });

            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getLastLocation();
                    Log.e(TAG, e.getMessage());
                }
            });


        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }


    public void mLoadMaps() {

        try {

            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {

                    try {

                        int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                getResources().getString(R.string.pref_radius), ""));

                        mMap.clear();

                        LatLng locationA = null;


                        int height = 134;
                        int width = 75;
                        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_kantor);
                        Bitmap b = bitmapdraw.getBitmap();
                        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

                        BitmapDrawable bitmapUser = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_user);
                        Bitmap bUser = bitmapUser.getBitmap();
                        Bitmap smallMarkeruser = Bitmap.createScaledBitmap(bUser, width, height, false);

                        if (!PreferenceManager.getDefaultSharedPreferences(getActivity())
                                .getString(getResources().getString(R.string.pref_longitude), "").isEmpty() &&
                                !PreferenceManager.getDefaultSharedPreferences(getActivity())
                                        .getString(getResources().getString(R.string.pref_longitude), "").isEmpty()) {

                            mLatitude = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                    getResources().getString(R.string.pref_latitude), "0"));

                            mLongitude = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                    getResources().getString(R.string.pref_longitude), "0"));

          /*  mLatitude = -7.2634205;
            mLongitude = 112.738152;*/

                            mMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude, mLongitude)).title("Lokasi Kantor").icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                            locationA = new LatLng(mLatitude, mLongitude);

                            LatLng locationB = new LatLng(latitude, longitude);

                            latLngBounds = new LatLngBounds.Builder()
                                    .include(locationA)
                                    .include(locationB)
                                    .build();
                        } else {
                            mLatitude = latitude;
                            mLongitude = longitude;
                        }


                        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Lokasiku").icon(BitmapDescriptorFactory.fromBitmap(smallMarkeruser)));


                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 10));
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(mLatitude, mLongitude))      // Sets the center of the map to location user
                                .zoom(19)                   // Sets the zoom
                                .bearing(0)                // Sets the orientation of the camera to east
                                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                                .build();                   // Creates a CameraPosition from the builder
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                        if (!isCloseRange() && mLatitude != 0 && mLongitude != 0)
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100), 1000, null);


                        Circle circle = mMap.addCircle(new CircleOptions()
                                .center(new LatLng(mLatitude, mLongitude))
                                .radius(radius)
                                .strokeColor(Color.parseColor("#20ff5252"))
                                .strokeWidth(3)
                                .fillColor(Color.parseColor("#50ff5252")));
                    } catch (Exception e) {
                    }
                }
            });
        } catch (Exception e) {

        }

    }

    public boolean isCloseRange() {

        boolean isCloseRange = false;

        Location locationA = new Location("");

        locationA.setLatitude(latitude);
        locationA.setLongitude(longitude);


        Location locationB = new Location("");

        locationB.setLatitude(mLatitude);
        locationB.setLongitude(mLongitude);

        float distancse = locationA.distanceTo(locationB);

        if (distancse < RANGE_DISTANCE_BOUNDS) {
            isCloseRange = true;
        }

        return isCloseRange;
    }


    private void getLastLocation() {
        try {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    mLoadMaps();

                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null);
        } catch (Exception e) {
        }

    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try {
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.my_json_maps));
            if (!isSuccess) {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }

        double lat, longi;

        if (!PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString(getResources().getString(R.string.pref_longitude), "").isEmpty() &&
                !PreferenceManager.getDefaultSharedPreferences(getActivity())
                        .getString(getResources().getString(R.string.pref_longitude), "").isEmpty()) {

            lat = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_latitude), "0"));

            longi = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_longitude), "0"));

        } else {
            lat = -7.265465;
            longi = 112.745543;
        }


        mMap = map;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            float zoomLevel = 16.0f; //This goes up to 21

            mMap.getUiSettings().setMapToolbarEnabled(true);
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            LatLng currentLatLng = new LatLng(lat,
                    longi);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, zoomLevel));
        }

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 500);
        }


        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions

                getMaps();

                Toast.makeText(getActivity(), getResources().getString(R.string.title_berhasil_update_lokasi), Toast.LENGTH_SHORT).show();

                return false;
            }
        });


    }


    private final class GpsLocationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boolean isActiveGps = intent.getBooleanExtra("isActiveGps", false);

                if (!isActiveGps && !mAlertDialog.isShowing()) {
                    mAlertDialog.show();
                    mIsActiveGPS = false;
                    Log.e(TAG, mIsActiveGPS + "");
                } else {
                    mAlertDialog.dismiss();
                    mIsActiveGPS = true;
                    Log.e(TAG, mIsActiveGPS + "");
                }

            } catch (Exception e) {
            }
        }
    }

    private final class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boolean noInternet = intent.getBooleanExtra("no_internet", true);

                if (noInternet && !mAlertInternet.isShowing()) {
                    mAlertInternet.show();
                    mIsActiveInternet = false;
                } else {
                    mAlertInternet.dismiss();
                    mIsActiveInternet = true;
                    mGetServerTim();
                }

            } catch (Exception e) {
            }
        }

    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(gpsLocationReceiver);
        getActivity().unregisterReceiver(networkChangeReceiver);
        cronometer.stop();
        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_finger), "false")
                .apply();
        super.onDestroy();
    }


    //request permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());
    }

    //start permission
    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }

    private void mShowDialog(String message, int type) {
        try {
            SweetAlertDialog mShowDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getResources().getString(R.string.string_fingerprint))
                    .setContentText(message)
                    .setConfirmText(getResources().getString(R.string.string_yes))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.cancel();
                            if (type == 0) {
                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                            } else if (type == 1) {
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            } else if (type == 2) {

                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                startActivity(intent);
                                getActivity().finish();

                                startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));

                            }

                        }
                    });

            mShowDialog.setCancelable(false);
            mShowDialog.show();
        } catch (Exception e) {
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

                           /* Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins.otf");
                            SpannableStringBuilder SS = new SpannableStringBuilder(DefaultFormatter.dFIdNoHour(response.body().getData()));
                            SS.setSpan(new HrsApp.CustomTypefaceSpan("", font2), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                            ((HomeActivity) getActivity()).getSupportActionBar().setTitle(SS);*/

                            mTimeServer = date;


                            timer(mTimeServer);

                        } else {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
        }

    }


    public void timer(Date dat) {

        int second = dat.getSeconds();
        int minute = dat.getMinutes();
        int hour = dat.getHours();
        int dates = dat.getDate();
        int month = dat.getMonth();
        int year = dat.getYear();

        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_timer), Long.toString(hour))
                .apply();


        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dates, hour, minute, second);

        long hours = calendar.get(Calendar.HOUR);
        long minutes = calendar.get(Calendar.MINUTE);
        long seconds = calendar.get(Calendar.SECOND);

        cronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;

                long a = Long.parseLong(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_timer), "0"));

                if (m == 59 && s == 59) {

                    if (a == 23) {
                        a = 0;
                        mGetServerTim();
                    } else {
                        a++;
                    }

                    PreferenceManager.getDefaultSharedPreferences(getActivity()).
                            edit().putString(getResources().getString(R.string.pref_timer), Long.toString(a))
                            .apply();
                }

                String hh = a < 10 ? "0" + a : a + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                cArg.setText(hh + ":" + mm + ":" + ss);
            }
        });
        cronometer.setBase(SystemClock.elapsedRealtime() - (hours * 3600000 + minutes * 60000 + seconds * 1000));
        cronometer.start();

    }


    public boolean checkActiveInternet() {

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            return false;
        } else {
            return true;
        }
    }


    private void mGetDashboard() {


        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), "tes"));

        String emp = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_employeeid), "tes"));

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(300, TimeUnit.SECONDS)
                .readTimeout(300, TimeUnit.SECONDS)
                .writeTimeout(300, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        Call<ResponseDashboard> call = api.mGetDashboard(Authorization);
        call.enqueue(new Callback<ResponseDashboard>() {
            @Override
            public void onResponse(Call<ResponseDashboard> call, Response<ResponseDashboard> response) {

                try {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();
                        String statusMessage = response.body().getErrMessage();
                        //status code 1 if login success
                        if (statusCode == 0) {

                            DataDalamDashboard data = response.body().getDataLuar().getDataDalam();


                            mTxtTotalLate.setText( getResources().getString(R.string.title_terlambat_bulan_berjalan) + " : " + DefaultFormatter.mGetHourMinute(data.getTotalTerlambat()));


                        }
                    }

                } catch (Exception e) {
                }

            }

            @Override
            public void onFailure(Call<ResponseDashboard> call, Throwable t) {

            }

        });


    }


}