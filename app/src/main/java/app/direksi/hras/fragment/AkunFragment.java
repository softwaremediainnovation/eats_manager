package app.direksi.hras.fragment;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.nex3z.notificationbadge.NotificationBadge;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.DetailTaskActivity;
import app.direksi.hras.GajiActivity;
import app.direksi.hras.RequestAttendanceActivity;
import app.direksi.hras.TaskActivity;
import app.direksi.hras.model.ResponseCounter;
import app.direksi.hras.AktivitasActivity;
import app.direksi.hras.AssetNewActivity;
import app.direksi.hras.AttendanceActivity;
import app.direksi.hras.ClaimActivity;
import app.direksi.hras.ContractExpiredActivity;
import app.direksi.hras.EmployeActivity;
import app.direksi.hras.LateActivity;
import app.direksi.hras.ListCutiActivity;
import app.direksi.hras.ListSnapActivity;
import app.direksi.hras.LoanActivity;
import app.direksi.hras.LoginActivity;
import app.direksi.hras.NewsActivity;
import app.direksi.hras.OvertimeActivity;
import app.direksi.hras.ProfileActivity;
import app.direksi.hras.R;
import app.direksi.hras.WarningLetterActivity;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.ResponseDataEmployeeID;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.NOWACS;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.Validation.KnowisAppInstalled;
import static app.direksi.hras.util.Validation.openWhatsAppConversationUsingUri;

/**
 * Created by dhimaz on 08/05/2019.
 */

public class AkunFragment extends Fragment {
    private View layoutInflater;
    private RecyclerView recyclerView;
    private List<DataAttendance> reportResult;
    private List<DataAttendance> report;
    private TextView mTxtName, mTxtJabatan, mTxtNik, mTxtEmployeeID;

    private int count_company = 0, count_job = 0;
    private String Authorization;
    RelativeLayout   header_profile ;
    LinearLayout rlDayoff, rlKehadiran, rlLembur, rlNews, rlGaji, rlPegawai, rlReimburse, rlLoan, rlReprimand, rlLogout, rl11, rl12, rl13, rlAktivitas, rl62, rlAsset, rlSnap, rlKontrak, rlLate, rlRequestAttendance,rlTask , rlCs;
    private ImageView profile_image;
    private TextView txtVersion;
    private NotificationBadge badgeDayoff, badgeLoan, badgeReimburse, badgeLembur, badgeAset, badgeKontrak, badgeRequestAttendance, badgeTask;
    private ScrollView mLayoutMain;
    private SwipeRefreshLayout swiperefresh;
    private SweetAlertDialog loading;
    private MessageDialog messageDialog;
    int temp = 0;

    private boolean isSupportBiometric = false;
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private String absenMethod = "";
    PermissionsChecker checker;
    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportResult = new ArrayList<>();
        report = new ArrayList<DataAttendance>();
//        setContentView(R.layout.activity_reports);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Jobs");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle saveInstanceState) {
        layoutInflater = inflater.inflate(R.layout.akun_fragment_layout, container, false);
        setHasOptionsMenu(true); //to set toolbar menu


        header_profile = (RelativeLayout) layoutInflater.findViewById(R.id.header_profile);
        header_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });



        mLayoutMain = layoutInflater.findViewById(R.id.mLayoutMain);
        mLayoutMain.setVisibility(View.VISIBLE);

        loading = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        messageDialog = new MessageDialog();

        swiperefresh = (SwipeRefreshLayout) layoutInflater.findViewById(R.id.swiperefresh);
        badgeDayoff = (NotificationBadge) layoutInflater.findViewById(R.id.badgeDayoff);
        badgeLoan = (NotificationBadge) layoutInflater.findViewById(R.id.badgeLoan);
        badgeReimburse = (NotificationBadge) layoutInflater.findViewById(R.id.badgeReimburse);
        badgeLembur = (NotificationBadge) layoutInflater.findViewById(R.id.badgeLembur);
        badgeAset = (NotificationBadge) layoutInflater.findViewById(R.id.badgeAset);
        badgeKontrak = (NotificationBadge) layoutInflater.findViewById(R.id.badgeKontrak);
        badgeRequestAttendance = (NotificationBadge) layoutInflater.findViewById(R.id.badgeRequestAttendance);
        badgeTask = (NotificationBadge) layoutInflater.findViewById(R.id.badgeTask);

        txtVersion = (TextView) layoutInflater.findViewById(R.id.txtVersion);
        mTxtName = (TextView) layoutInflater.findViewById(R.id.mTxtName);
        mTxtJabatan = (TextView) layoutInflater.findViewById(R.id.mTxtJabatan);
        mTxtNik = (TextView) layoutInflater.findViewById(R.id.mTxtNik);
        mTxtEmployeeID = (TextView) layoutInflater.findViewById(R.id.mTxtEmployeeID);

        absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_absen_method), ""));

        mTxtName.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_nama), ""));
        mTxtJabatan.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_email), ""));
        mTxtNik.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_nik), ""));
        mTxtEmployeeID.setText( getResources().getString(R.string.title_id_pegawai) + " : " +PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_employeeid), ""));


        profile_image = (ImageView) layoutInflater.findViewById(R.id.profile_image);
        String url = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getResources().getString(R.string.pref_photourl), "");
        if (url == null || url.equals(getContext().getResources().getString(R.string.base_url)))
        {

            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);

            Glide.with(getContext())
                    .load(getResources().getDrawable(R.drawable.profile))
                    .error(Glide.with(profile_image).load(R.drawable.profile))
                    .into(profile_image);

        } else {
            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);
            Glide.with(getContext())
                    .load(  PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getResources().getString(R.string.pref_photourl), ""))
                    .into(profile_image);

          /*  Glide.with(HomeActivity.this)
                    .load((getResources().getString(R.string.base_url) +  PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(getResources().getString(R.string.pref_photourl), "")))
                            .apply(requestOptions)
                            .into(imageViewNav);*/
        }

        rlTask = (LinearLayout) layoutInflater.findViewById(R.id.rlTask);
        rlTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), TaskActivity.class);
                startActivity(intent);
            }
        });

        rlKontrak = (LinearLayout) layoutInflater.findViewById(R.id.rlKontrak);
        rlKontrak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ContractExpiredActivity.class);
                startActivity(intent);
            }
        });

        rlLate = (LinearLayout) layoutInflater.findViewById(R.id.rlLate);
        rlLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LateActivity.class);
                startActivity(intent);
            }
        });


        rlAktivitas = (LinearLayout) layoutInflater.findViewById(R.id.rlAktivitas);
        rlAktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AktivitasActivity.class);
                startActivity(intent);
            }
        });

        rlAsset = (LinearLayout) layoutInflater.findViewById(R.id.rlAsset);
        rlAsset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AssetNewActivity.class);
                startActivity(intent);
            }
        });



        rlDayoff = (LinearLayout) layoutInflater.findViewById(R.id.rlDayoff);
        rlDayoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ListCutiActivity.class);
                startActivity(intent);
            }
        });
        rlKehadiran = (LinearLayout) layoutInflater.findViewById(R.id.rlKehadiran);
        rlKehadiran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), AttendanceActivity.class);
                startActivity(intent);
            }
        });
        rlLembur = (LinearLayout) layoutInflater.findViewById(R.id.rlLembur);
        rlLembur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), OvertimeActivity.class);
                startActivity(intent);
            }
        });
        rlNews = (LinearLayout) layoutInflater.findViewById(R.id.rlNews);
        rlNews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), NewsActivity.class);
                startActivity(intent);
            }
        });
        rlGaji = (LinearLayout) layoutInflater.findViewById(R.id.rlGaji);
        rlGaji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                    if (isSupportBiometric) {

                        biometricPrompt.authenticate(promptInfo);
                    }


                    else{

                        Intent intent = new Intent(getContext(), GajiActivity.class);
                        startActivity(intent);
                    }

                } else {

                    Intent intent = new Intent(getContext(), GajiActivity.class);
                    startActivity(intent);

                }
            }
        });
        rlPegawai = (LinearLayout) layoutInflater.findViewById(R.id.rlPegawai);
        rlPegawai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), EmployeActivity.class);
                startActivity(intent);
            }
        });
        rlReimburse = (LinearLayout) layoutInflater.findViewById(R.id.rlReimburse);
        rlReimburse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ClaimActivity.class);
                startActivity(intent);
            }
        });
        rlLoan = (LinearLayout) layoutInflater.findViewById(R.id.rlLoan);
        rlLoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoanActivity.class);
                startActivity(intent);
            }
        });
        rlReprimand = (LinearLayout) layoutInflater.findViewById(R.id.rlReprimand);
        rlReprimand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WarningLetterActivity.class);
                startActivity(intent);
            }
        });
        rlSnap = (LinearLayout) layoutInflater.findViewById(R.id.rlSnap);
        rlSnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* CreateSnapFragment dialog = new CreateSnapFragment();
                dialog.show(getActivity().getFragmentManager(), "");*/

                Intent intent = new Intent(getContext(), ListSnapActivity.class);
                startActivity(intent);
            }
        });
        rlRequestAttendance = (LinearLayout) layoutInflater.findViewById(R.id.rlRequestAttendance);
        rlRequestAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* CreateSnapFragment dialog = new CreateSnapFragment();
                dialog.show(getActivity().getFragmentManager(), "");*/

                Intent intent = new Intent(getContext(), RequestAttendanceActivity.class);
                startActivity(intent);
            }
        });

        rlCs = (LinearLayout) layoutInflater.findViewById(R.id.rlCs);
        rlCs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    String str = NOWACS;


                    if (str.substring(0, 1).equals("0")) {
                        str = "+62" + str.substring(1);

                    } else {
                        str = "+62" + str;
                    }

                    boolean installed = KnowisAppInstalled(getActivity(),"com.whatsapp");
                    if(installed) {

                        String message = "*" + getResources().getString(R.string.title_email) + "* : " +PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                getResources().getString(R.string.pref_email), "") + " \n" +
                                "*"+ getResources().getString(R.string.title_nama) + "* : " + PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                getResources().getString(R.string.pref_nama), "") + " \n" +
                                "*"+ getResources().getString(R.string.title_organisasi) +"* : " + PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                getResources().getString(R.string.pref_namaOrganization), "") + " \n" +
                                "*" + getResources().getString(R.string.title_versi) + "* : " + txtVersion.getText() + " \n" +
                                "*"+ getResources().getString(R.string.title_pertanyaan) +"* : ";




                        openWhatsAppConversationUsingUri(getActivity(), str, message);
                    } else {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.whatsapp")));
                        } catch (android.content.ActivityNotFoundException anfe) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + "com.whatsapp")));
                        }
                    }






                } catch ( Exception e){


                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.whatsapp&hl=in&gl=US")));

                }


            }
        });

        rlLogout = (LinearLayout) layoutInflater.findViewById(R.id.rlLogout);
        rlLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(getResources().getString(R.string.string_confirmation))
                        .setContentText(getResources().getString(R.string.txt_message_log_out))
                        .setCancelText(getResources().getString(R.string.txt_no))
                        .setConfirmText(getResources().getString(R.string.string_yes))
                        .showCancelButton(true)
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {

                                sweetAlertDialog.cancel();
                                PostLogout();


                            }
                        })
                        .show();
            }
        });


        //counter jml job
       /* jml_company = (TextView) layoutInflater.findViewById(R.id.jumlah_jobs);
        label_company = (TextView) layoutInflater.findViewById(R.id.tv_label_company);
        loading = (ProgressBar) layoutInflater.findViewById(R.id.loading);
        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        loadJSON();*/

        //kode untuk swipe to refresh
        /*final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) layoutInflater.findViewById(R.id.refreshCompany);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.darker_gray);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                loadJSON();
                onItemLoad();
            }

            void onItemLoad() {
                swipeRefreshLayout.setRefreshing(false);
            }

        });*/

        //code untuk recyclerview
       /* recyclerView = (RecyclerView) layoutInflater.findViewById(R.id.rv_reports);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null)); //untuk divider
        recyclerView.setAdapter(new ReportsAdapter(report));

        txt_empty_rv = (TextView) layoutInflater.findViewById(R.id.tv_empty_rv_report);*/


        PreferenceManager.getDefaultSharedPreferences(getContext()).
                edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                .putString(getResources().getString(R.string.pref_dayoff_start), "")
                .putString(getResources().getString(R.string.pref_dayoff_end), "")
                .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                .putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                .putString(getResources().getString(R.string.pref_dayoff_idProses), "0")
                .putString(getResources().getString(R.string.pref_dayoff_status), "1")
                .apply();

        try {
            String mCurrentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            txtVersion.setText(getActivity().getApplicationInfo().loadLabel(getActivity().getPackageManager()).toString() + " " + getResources().getString(R.string.title_versi) + " " +  mCurrentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        checkProfile();


        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                //dataListGR = new ArrayList<>();
                // GRAdapter = new BirthdayDashboardAdapter(dataListGR, getActivity());
                swiperefresh.setRefreshing(false);

                mLayoutMain.setVisibility(View.VISIBLE);
                checkCounterBackground();



            }});

        checker = new PermissionsChecker(getActivity());

        if (checkBiometricSupport()) {
            isSupportBiometric = true;
        } else {

            isSupportBiometric = false;

        }

        if (isSupportBiometric) {
            if (checker.lacksPermissions(PERMISSIONS)) {
                startPermissionsActivity(PERMISSIONS);
            }
        }





        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle(getActivity().getResources().getString(R.string.txt_biometric_verification))
                .setSubtitle(getActivity().getResources().getString(R.string.txt_mesage_presence_biometric))
                .setNegativeButtonText(getActivity().getResources().getString(R.string.title_batal_small))
                .setConfirmationRequired(false)
                .build();

        executor = ContextCompat.getMainExecutor(getContext());
        biometricPrompt = new BiometricPrompt(this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);

                if (errString.equals(getActivity().getResources().getString(R.string.title_batal_small))){

                } else {

                    MDToast.makeText(getActivity(),  errString.toString(),
                            MDToast.LENGTH_SHORT, MDToast.TYPE_INFO).show();

                }

            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                Intent intent = new Intent(getContext(), GajiActivity.class);
                startActivity(intent);



            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                MDToast.makeText(getActivity(), getActivity().getResources().getString(R.string.txt_failed_verification),
                        MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();
            }
        });


        return layoutInflater;
    }


    private Boolean checkBiometricSupport(){

        Boolean hasil = false;
        BiometricManager biometricManager = BiometricManager.from(getActivity());
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                hasil = true;
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                hasil = false;
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                hasil = false;
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                hasil = true;
                Log.e("MY_APP_TAG", "BIOMETRIC_ERROR_NONE_ENROLLED");
                // Prompts the user to create credentials that your app accepts.
                break;
        }

        return hasil;
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }

    private void loadJSON() {
       /* loading.setVisibility(View.VISIBLE);
        try {
            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            //untuk mencegah onFailure jika terjadi socketTimeOut
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(600, TimeUnit.SECONDS)
                    .readTimeout(600, TimeUnit.SECONDS)
                    .writeTimeout(600, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            //assign variabel request ke class interface TabAdverRequest
            final APIInterfaces request = retrofit.create(APIInterfaces.class);
            Call<ModelCompany> call = request.getReportResultNew(Authorization); //ngirim ke API
            call.enqueue(new Callback<ModelCompany>() {
                @Override
                public void onResponse(Call<ModelCompany> call, Response<ModelCompany> response) {
                    if (response.isSuccessful()){
//                        reportResult.clear();
                        reportResult = response.body().getResult();
                        if (!reportResult.isEmpty()){
                            report.clear();
                            report.addAll(reportResult);

                            //counter jumlah jobs
                            count_company = report.size();
                            jml_company.setText(String.valueOf(count_company));
                            if (count_company > 1){
                                label_company.setText("Total Companies");
                            }else {
                                label_company.setText("Total Company");
                            }

                            //sinkronisasi untuk filter
                            synchronized(recyclerView.getAdapter()){
                                recyclerView.getAdapter().notifyDataSetChanged();
                            }
                        }else {
                            txt_empty_rv.setText("No Result.");
                            txt_empty_rv.setVisibility(View.VISIBLE);
                        }
                        loading.setVisibility(View.GONE);

                    } else if (response.errorBody() != null) {
                        if (response.code() == 500) {
                            Toast.makeText(getActivity(), "Server Error, Silahkan Coba Lagi Nanti.", Toast.LENGTH_LONG).show();
                            loading.setVisibility(View.GONE);
                        }else if(response.code() == 401){
                            //handle unauthorized
                            Toast.makeText(getActivity(), "Silahkan Login Kembali.", Toast.LENGTH_LONG).show();
                            getActivity().finish(); //semua activity dan preferences di clear. untuk bisa login kembali.
                            mClearPreferences();
                            Intent logout = new Intent(getActivity(), LoginActivity.class);
                            logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);    //set flag untuk logout
                            startActivity(logout);
                        } else{
                            Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
                            loading.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
                        loading.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onFailure(Call<ModelCompany> call, Throwable t) {
                    Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
                    System.out.println("onFailure = "+t.toString());
                    loading.setVisibility(View.GONE);
                }
            });
        }catch (Exception e) {
            Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
            loading.setVisibility(View.GONE);
        }*/
    }

    //filter untuk searchView, filterable ada di adapter.
    private void filterList(String name){
        synchronized(recyclerView.getAdapter()){
            /* ((ReportsAdapter)recyclerView.getAdapter()).getFilter().filter(name);*/
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    //method untuk searchView di MENU PENGIKLAN
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    //clear share preference untuk login
    void mClearPreferences() {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putString("pref_token", "")
                .putString("pref_email", "")
                .putString("pref_password", "")
                .apply();
    }

    public void checkProfile() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));




            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployeeID> call = api.getEmployeeID(Authorization, id);

            call.enqueue(new Callback<ResponseDataEmployeeID>() {
                @Override
                public void onResponse(Call<ResponseDataEmployeeID> call, Response<ResponseDataEmployeeID> response) {
                    if (response.isSuccessful()) {
                        if (getActivity() != null) {

                            String temp = "";
                            if (response.body().getData().getBankName() != null && response.body().getData().getBankNumber()  != null  && response.body().getData().getBankAccountName()!= null){
                                temp =    response.body().getData().getBankName() + " - " + response.body().getData().getBankNumber() + " \n" + response.body().getData().getBankAccountName();
                            }


                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_nama), response.body().getData().getFirstName() + " " + response.body().getData().getLastName())
                                    .putString(getResources().getString(R.string.pref_adress), response.body().getData().getAddress())
                                    .putString(getResources().getString(R.string.pref_gender), response.body().getData().getGender() != null ? response.body().getData().getGender() : "")
                                    .putString(getResources().getString(R.string.pref_nik), response.body().getData().getNik())
                                    .putString(getResources().getString(R.string.pref_rekening), temp)
                                    .putString(getResources().getString(R.string.pref_photourl), response.body().getData().getPhotoUrl())
                                    .putString(getResources().getString(R.string.pref_phone), response.body().getData().getPhone())
                                    .putString(getResources().getString(R.string.pref_dob), response.body().getData().getDob())
                                    .putString(getResources().getString(R.string.pref_email), response.body().getData().getEmail())
                                    .putString(getResources().getString(R.string.pref_contract_expired), response.body().getData().getContractExpiredDate())
                                    .putString(getResources().getString(R.string.pref_join_date), response.body().getData().getJoinDate())
                                    .putString(getResources().getString(R.string.pref_key_xendit), response.body().getData().getKeyXendit() == null ? "" : response.body().getData().getKeyXendit())
                                    .putString(getResources().getString(R.string.pref_jatah), response.body().getData().getSisaCuti() != null ? response.body().getData().getSisaCuti() + " " + getResources().getString(R.string.title_hari) : "")
                                    .apply();
                            // Toast.makeText(getActivity(), response.body().getData().getKeyXendit() == null ? "" : response.body().getData().getKeyXendit(), Toast.LENGTH_LONG).show();

                            mTxtName.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                    getResources().getString(R.string.pref_nama), ""));
                            mTxtJabatan.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                    getResources().getString(R.string.pref_email), ""));
                            mTxtNik.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                    getResources().getString(R.string.pref_nik), ""));
                            mTxtEmployeeID.setText(getResources().getString(R.string.title_id_pegawai) + " : "+PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                    getResources().getString(R.string.pref_employeeid), ""));


                            profile_image = (ImageView) layoutInflater.findViewById(R.id.profile_image);
                            String url = PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getResources().getString(R.string.pref_photourl), "");
                            if (url == null || url.equals(getContext().getResources().getString(R.string.base_url))) {

                                RequestOptions requestOptions = new RequestOptions();
                                //  requestOptions.placeholder(R.drawable.warnawarni);
                                requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
                                requestOptions.skipMemoryCache(true);

                                Glide.with(getContext())
                                        .load(getResources().getDrawable(R.drawable.profile))
                                        .error(Glide.with(profile_image).load(R.drawable.profile))
                                        .into(profile_image);

                            } else {
                                RequestOptions requestOptions = new RequestOptions();
                                //  requestOptions.placeholder(R.drawable.warnawarni);
                                requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
                                requestOptions.skipMemoryCache(true);
                                Glide.with(getContext())
                                        .load(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(getResources().getString(R.string.pref_photourl), ""))
                                        .into(profile_image);

                            }
                        }


                    } else {
                        //  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataEmployeeID> call, Throwable t) {
                    // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            // Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
        }
    }


    public void PostLogout() {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));






            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.Logout(Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {
                    if (response.isSuccessful()) {

                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            try {
                                NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.cancelAll();

                            } catch (Exception e){

                            }

                            PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                                    .putString(getResources().getString(R.string.pref_nama), "")
                                    .putString(getResources().getString(R.string.pref_email), "")
                                    .putString(getResources().getString(R.string.pref_token), "")
                                    .putString(getResources().getString(R.string.pref_role), "")
                                    .putString(getResources().getString(R.string.pref_phone), "")
                                    .putString(getResources().getString(R.string.pref_key_xendit), "")
                                    .apply();


                            Intent login = new Intent(getActivity(), LoginActivity.class);
                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(login);
                            getActivity().finish();


                        }
                        else {
                            try {
                                loading.dismiss();
                                if (getActivity()!= null) {
                                    messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                            , "");
                                }
                            } catch (Exception e){

                            }
                        }

                    } else {
                        try {
                            loading.dismiss();
                            if (getActivity()!= null) {
                                messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                        , "");
                            }
                        } catch (Exception e){

                        }
                        //  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    try {
                        loading.dismiss();
                        if (getActivity()!= null) {
                            messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                    , "");
                        }
                    } catch (Exception e){

                    }
                    // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            try {
                loading.dismiss();
                if (getActivity()!= null) {
                    messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                            , "");
                }
            } catch (Exception x){
                loading.dismiss();
            }
            // Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
        }
    }


    public void checkCounterBackground() {

        if (isAdded()) {

            try {

                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_token), ""));


                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                        .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                        .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                        .build();


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseCounter> call = api.getCounter(Authorization);

                call.enqueue(new Callback<ResponseCounter>() {
                    @Override
                    public void onResponse(Call<ResponseCounter> call, Response<ResponseCounter> response) {
                        if (response.isSuccessful()) {

                            if (getActivity() != null) {
                                Log.v("badge", "activity not null");

                                if (response.body().getData().size() > 0) {

                                    try {

                                        Log.v("badge", "try");
                                        mLayoutMain.setVisibility(View.VISIBLE);
                                        badgeDayoff.setNumber(response.body().getData().get(0).getDayoffs());
                                        badgeLoan.setNumber(response.body().getData().get(0).getLoans());
                                        badgeReimburse.setNumber(response.body().getData().get(0).getReimbursements());
                                        badgeLembur.setNumber(response.body().getData().get(0).getOvertimes());
                                        badgeAset.setNumber(response.body().getData().get(0).getAssets());
                                        badgeKontrak.setNumber(response.body().getData().get(0).getKontrak());
                                        badgeRequestAttendance.setNumber(response.body().getData().get(0).getRequestAttendance());
                                        badgeTask.setNumber(response.body().getData().get(0).getTaskheader());
                                    } catch (Exception e) {
                                        Log.v("badge", "catch");
                                    }
                                }


                            } else {

                                Log.v("badge", "activity null");

                            }


                        } else {

                            //  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCounter> call, Throwable t) {

                        // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    }
                });
            } catch (Exception e) {

                // Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
            }
        } else {

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        checkCounterBackground();

    }

    public void onDestroy() {
        super.onDestroy();
        loading.dismiss();
    }

}