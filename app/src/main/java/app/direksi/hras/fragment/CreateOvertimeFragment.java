package app.direksi.hras.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.app.Activity;
import android.Manifest;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.OvertimeActivity;
import  app.direksi.hras.R;
import app.direksi.hras.adapter.ActivityAdapter;
import app.direksi.hras.model.DataOvertimeAttendance;
import  app.direksi.hras.model.ResponseCreateOvertimes;
import app.direksi.hras.model.ResponseDataOvertimeAttendance;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.util.BitmapImage;
import  app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.DefaultFormatter;
import  app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.ChooserType;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.MediaFile;
import pl.aprilapps.easyphotopicker.MediaSource;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_1;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_2;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_3;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.app.AppConstants.addLemburOption;
import static app.direksi.hras.app.AppConstants.addMaxdaysLembur;
import static app.direksi.hras.app.AppConstants.addMindaysLembur;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateOvertimeFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateOvertimeFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;


    TextInputLayout mLayoutSelesai, mLayoutMulai;
    EditText mEdtSelesai, mEdtMulai,
            mEdtNote;

    TextInputLayout mReason;

    private Integer mIdConstanta[];
    private String mNamaConstanta[];
    private Button mButtonSend, mButtonCancel;
    private Integer constanta, mHowManyDays;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri = null;
    private SweetAlertDialog loading;
    private ImageView imgClose;
    private String jamServer = "";
    private File file = null;

    private String mCurrentPhotoPath;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String deviceModel;
    private Bitmap rotatedBitmap;
    private boolean isInRange = false;
    private String timeWPOut = "", timwWPOutPlus = "";

    EasyImage easyImage;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_overtime, container, false);


        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        imgClose = rootView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);
        mEdtMulai.setOnClickListener(this);
        mEdtSelesai.setOnClickListener(this);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMM yyyy");


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

     /*   String option = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_overtime_option), ""));

        if (option.equals("true")) {
            mEdtSelesai.setEnabled(false);
        }*/

       /* mLayoutMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });*/

       /* mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerStart();
            }
        });*/

      /*  mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerEnd();
            }
        });*/

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

       /* mLayoutSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });*/

        mButtonSend.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    String option = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_overtime_option), ""));

                    if (option.equals("true")) {

                        easyImage.openCameraForImage(CreateOvertimeFragment.this);

                    } else {

                        startCropImageActivity();

                    }
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();




                    String option = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_overtime_option), ""));

                    if (option.equals("true")) {

                        easyImage.openCameraForImage(CreateOvertimeFragment.this);

                    } else {

                        startCropImageActivity();

                    }
                }
            }
        });

        easyImage = new EasyImage.Builder(getActivity())
                .setChooserTitle("Pick media")
                .setCopyImagesToPublicGalleryFolder(false)
                //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                .setFolderName("EasyImage sample")
                .allowMultiple(true)
                .build();




        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mGetServerTim();
        return rootView;
    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(getActivity(), this);
    }



    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }

    public void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        returnUri = Uri.fromFile(photo);
        startActivityForResult(intent, 100);
        Log.v(TAG, "takePhoto " + 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {

            String option = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_overtime_option), ""));

            if (option.equals("true")) {

                easyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
                    @Override
                    public void onMediaFilesPicked(MediaFile[] imageFiles, MediaSource source) {
                        for (MediaFile imageFile : imageFiles) {

                            file = imageFile.getFile();

                            mImageChoose.setVisibility(View.GONE);
                            mImageView.setVisibility(View.VISIBLE);
                            mImageView.setImageURI(Uri.fromFile(imageFile.getFile()));

                        }

                    }

                    @Override
                    public void onImagePickerError(@NonNull Throwable error, @NonNull MediaSource source) {
                        //Some error handling
                        error.printStackTrace();
                    }

                    @Override
                    public void onCanceled(@NonNull MediaSource source) {
                        //Not necessary to remove any files manually anymore
                    }
                });



            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == getActivity().RESULT_OK) {


                    mImageChoose.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageURI(result.getUri());
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), result.getUri());
                        file = BitmapImage.saveToSD(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                }
            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
        }


    }

    public static class DateUtil {
        public static Date addDays(Date date, int days) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, days); //minus number would decrement the days
            return cal.getTime();
        }
    }

    TimePickerDialog timePickerDialog;
    int hour1,minutes1;
    int cYear,cMonth,cDay, cHour, cMinute;

    private void openDateRangePicker() {
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {


                try{
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); // here set the pattern as you date in string was containing like date/month/year
                    Date d = sdf.parse(dayOfMonth + "/" + month + "/" + year);


                    mStartDate = sdf.format(d.getTime());
                    mEdtMulai.setText(mViewSdf.format(d.getTime()));

                }catch(ParseException ex){
                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                }

            }
        },cYear,cMonth,cDay);

        datePicker.show();
    }


    private String dtStartPost, dtStart, dtEndPost, dtEnd, jmStart, jmEnd, fullStart, fullEnd;

    private void openDateRangePickerStart() {

        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            calstart.setTime(date);
            calEnd.setTime(date);

            calstart.add(Calendar.DAY_OF_YEAR, addMindaysLembur);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdaysLembur);


           /* hour1 = cal.HOUR_OF_DAY;
            minutes1 = cal.MINUTE;*/


        } catch (Exception e) {

        }

        Calendar getDate = Calendar.getInstance();

        cDay = cal.get(Calendar.DAY_OF_MONTH);
        cMonth = cal.get(Calendar.MONTH);
        cYear = cal.get(Calendar.YEAR);
        cHour = cal.get(Calendar.HOUR_OF_DAY);
        cMinute = cal.get(Calendar.MINUTE);



        DatePickerDialog datePicker = new DatePickerDialog(getActivity(), R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                month = month + 1;

                String day, mont, yer;
                if (dayOfMonth > 9){
                    day = String.valueOf(dayOfMonth);
                } else {
                    day = "0" + String.valueOf(dayOfMonth);
                }

                if (month > 9){
                    mont = String.valueOf(month);
                } else {
                    mont = "0" + String.valueOf(month);
                }
                yer = String.valueOf(year);


                String formattt = yer + "-" +mont + "-" + day;
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = format.parse(formattt);

                    dtStartPost = sdf.format(date.getTime());
                    dtStart = mViewSdf.format(date.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                cYear = year;
                cMonth = month;
                cDay = dayOfMonth;

                //  mEdtSelesai.setText(hour1 + ":" +minutes1 + AM_PM  + "     " + cDay + "/" + cMonth + "/" + cYear);

                timePickerDialog = new TimePickerDialog(getActivity(),  R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {



                        String jaaam, meniiiit;
                        if (hourOfDay > 9){
                            jaaam = String.valueOf(hourOfDay);
                        }else{
                            jaaam = "0" +String.valueOf(hourOfDay);
                        }
                        if (minute > 9){

                            meniiiit = String.valueOf(minute);
                        }else{
                            meniiiit = "0"+String.valueOf(minute);
                        }
                        hour1 = hourOfDay;
                        minutes1 = minute;
                        jmStart = jaaam + ":" +meniiiit;

                        mEdtMulai.setText(dtStart+  "   " + jmStart);

                        String option = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                getResources().getString(R.string.pref_overtime_option), ""));

                        if (option.equals("true")) {
                            loading.show();
                            GetAttendance();

                        } else {


                        }


                    }
                },cHour,cMinute,true);

                timePickerDialog.show();

            }
        },cYear,cMonth,cDay);


        datePicker.getDatePicker().setMinDate(calstart.getTimeInMillis());
        datePicker.getDatePicker().setMaxDate(calEnd.getTimeInMillis());
        datePicker.show();



    }

    private void openDateRangePickerEnd() {

        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();

        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            calstart.setTime(date);
            calEnd.setTime(date);

            calstart.add(Calendar.DAY_OF_YEAR, addMindaysLembur);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdaysLembur);

          /*  hour1 = cal.HOUR_OF_DAY;
            minutes1 = cal.MINUTE;*/


        } catch (Exception e) {

        }



        Calendar getDate = Calendar.getInstance();

        cDay = cal.get(Calendar.DAY_OF_MONTH);
        cMonth = cal.get(Calendar.MONTH);
        cYear = cal.get(Calendar.YEAR);
        cHour = cal.get(Calendar.HOUR_OF_DAY);
        cMinute = cal.get(Calendar.MINUTE);



        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),  R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                month = month + 1;

                String day, mont, yer;
                if (dayOfMonth > 9){
                    day = String.valueOf(dayOfMonth);
                } else {
                    day = "0" + String.valueOf(dayOfMonth);
                }

                if (month > 9){
                    mont = String.valueOf(month);
                } else {
                    mont = "0" + String.valueOf(month);
                }
                yer = String.valueOf(year);


                String formattt = yer + "-" +mont + "-" + day;
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date date = format.parse(formattt);

                    dtEndPost = sdf.format(date.getTime());
                    dtEnd = mViewSdf.format(date.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                cYear = year;
                cMonth = month;
                cDay = dayOfMonth;

                //  mEdtSelesai.setText(hour1 + ":" +minutes1 + AM_PM  + "     " + cDay + "/" + cMonth + "/" + cYear);
                timePickerDialog = new TimePickerDialog(getActivity(),  R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {


                        String jaaam, meniiiit;
                        if (hourOfDay > 9){
                            jaaam = String.valueOf(hourOfDay);
                        }else{
                            jaaam = "0" +String.valueOf(hourOfDay);
                        }
                        if (minute > 9){

                            meniiiit = String.valueOf(minute);
                        }else{
                            meniiiit = "0"+String.valueOf(minute);
                        }
                        hour1 = hourOfDay;
                        minutes1 = minute;
                        jmEnd = jaaam + ":" +meniiiit;
                        mEdtSelesai.setText(dtEnd+  "   " + jmEnd);

                    }
                },cHour,cMinute,true);

                timePickerDialog.show();

            }
        },cYear,cMonth,cDay);


        datePicker.getDatePicker().setMinDate(calstart.getTimeInMillis());
        datePicker.getDatePicker().setMaxDate(calEnd.getTimeInMillis());
        datePicker.show();

    }

    public void GetAttendance() {


        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeID", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));
            data.put("date", dtStartPost);


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


            // simplified call to request the news with already initialized service
            Call<ResponseDataOvertimeAttendance> call = api.getOvertimeAttendance(Authorization, data);

            call.enqueue(new Callback<ResponseDataOvertimeAttendance>() {
                @Override
                public void onResponse(Call<ResponseDataOvertimeAttendance> call, Response<ResponseDataOvertimeAttendance> response) {

                    try {

                        loading.dismiss();

                        if (response.isSuccessful()) {
                            int statusCode = response.body().getErrCode();

                            if (statusCode == 0) {

                                if (response.body().getData().getWpOut()!= null){
                                    Calendar wpOut = Calendar.getInstance();
                                    Calendar wpOutPlus = Calendar.getInstance();
                                    Calendar lemburIN = Calendar.getInstance();

                                    String tempLemburIn = dtStartPost + "T" + jmStart;
                                    lemburIN = DefaultFormatter.changeFormatToDateWithoutSecond(tempLemburIn);
                                    wpOut = DefaultFormatter.changeFormatToDate(response.body().getData().getWpOut());
                                    wpOutPlus = DefaultFormatter.changeFormatToDate(response.body().getData().getWpOut());
                                    wpOutPlus.add(Calendar.MINUTE, addLemburOption);

                                    Date out = wpOut.getTime();
                                    Date outPlus = wpOutPlus.getTime();

                                    timeWPOut = DefaultFormatter.changeFormatDateToString(out);
                                    timwWPOutPlus = DefaultFormatter.changeFormatDateToString(outPlus);


                                    if (DefaultFormatter.isSameDateTime(lemburIN, wpOut) || DefaultFormatter.isSameDateTime(lemburIN, wpOutPlus)){

                                        isInRange = true;
                                        ShowTglEnd(response.body().getData());

                                    } else  if (lemburIN.after(wpOut)){

                                        if (lemburIN.before(wpOutPlus)){

                                            isInRange = true;
                                            ShowTglEnd(response.body().getData());

                                        } else {
                                            isInRange = false;
                                        }

                                    } else {
                                        isInRange = false;
                                    }


                                } else {

                                    isInRange = false;
                                }




                            } else {
                                isInRange = false;
                                try {
                                    loading.dismiss();
                                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                    //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                                } catch (Exception e){

                                }

                            }

                        } else if (response.code() == 404){
                           /* try {
                                mEdtSelesai.setText("");
                                dtEnd="";
                                jmEnd="";
                                dtEndPost="";

                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_ada_absen) + " " + dtStart,
                                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                            } catch (Exception e){

                            }*/

                            isInRange = false;

                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                            } catch (Exception e){

                            }
                        }
                    } catch (Exception e) {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                        } catch (Exception s){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataOvertimeAttendance> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                    } catch (Exception e){

                    }
                }
            });

        } catch (Exception ex){

            String temp = ex.toString();
            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

            } catch (Exception e){

            }
        }


    }

    public void ShowTglEnd(DataOvertimeAttendance data){

        if (data.getTimeOut()!= null){

            dtEnd = DefaultFormatter.changeFormatDateWithOutHour(data.getTimeOut());
            jmEnd = DefaultFormatter.changeFormatDateHourOnly(data.getTimeOut());
            dtEndPost = DefaultFormatter.changeFormatDateYearMonthDay(data.getTimeOut());
            mEdtSelesai.setText(dtEnd + "   " + jmEnd);

        } else {
            try {

                mEdtSelesai.setText("");
                dtEnd="";
                jmEnd="";
                dtEndPost="";
                loading.dismiss();

                if (dtStart.equals(DefaultFormatter.changeFormatDateWithOutHour(jamServer))){

                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_anda_belum_absen_out),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();


                } else {

                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_ada_absen_out) + " " + dtStart,
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                }

            } catch (Exception e){

            }
        }

    }


    public void mCreateDayOff() {


        try {

            RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), "0"));
            RequestBody DateStart = RequestBody.create(MediaType.parse("text/plain"), dtStartPost + "T" + jmStart);
            RequestBody DateEnd = RequestBody.create(MediaType.parse("text/plain"), dtEndPost + "T" + jmEnd);
            RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));


            //File creating from selected URL
            //File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
            // File file = new File(CompressImage.createCopyAndReturnRealPath(getActivity(), returnUri));
            MultipartBody.Part multipart = null;
            if (file.exists()) {
                String name = URLConnection.guessContentTypeFromName(file.getName());
                RequestBody requestFile = null;
                try {
                    requestFile = RequestBody.create(MediaType.parse(name), CompressImage.compressImage(file, getActivity()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                multipart = MultipartBody.Part.createFormData("OvertimeFile", file.getName(), requestFile);
            }

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


            // simplified call to request the news with already initialized service
            Call<ResponseCreateOvertimes> call = api.mCreateOvertime(Authorization, DateStart,
                    DateEnd, Note, EmployeeID, multipart);

            call.enqueue(new Callback<ResponseCreateOvertimes>() {
                @Override
                public void onResponse(Call<ResponseCreateOvertimes> call, Response<ResponseCreateOvertimes> response) {

                    try {

                        loading.dismiss();

                        if (response.isSuccessful()) {
                            int statusCode = response.body().getErrCode();

                            if (statusCode == 0) {

                          /*  dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.overtime_success), Toast.LENGTH_SHORT).show();


                            Intent intent = new Intent(getActivity(), OvertimeActivity.class);
                            startActivity(intent);
                            getActivity().finish();*/

                                SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                                alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                alertDialog.setContentText("");
                                alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        Intent login = new Intent(getActivity(), OvertimeActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                    }
                                });
                                alertDialog.show();

                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));


                           /* new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), OvertimeActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);
                                        }
                                    })
                                    .show();*/

                            } else {

                                try {
                                    loading.dismiss();
                                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                    //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                    redirected();
                                } catch (Exception e){

                                }

                            }

                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }
                        }
                    } catch (Exception e) {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception s){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseCreateOvertimes> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception e){

                    }
                }
            });

        } catch (Exception ex){

            String temp = ex.toString();
            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                redirected();
            } catch (Exception e){

            }
        }


    }

    private void mAttemptCreateDayOff() {

        mEdtMulai.setError(null);
        mEdtSelesai.setError(null);
        mEdtNote.setError(null);


        String start = mEdtMulai.getText().toString();
        String end = mEdtSelesai.getText().toString();
        String note = mEdtNote.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(start) || start.contains("null")) {
            mEdtMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtMulai;
            cancel = true;
        }

        if (TextUtils.isEmpty(end) || end.contains("null")) {
            mEdtSelesai.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtSelesai;
            cancel = true;
        }


        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }

        if (file == null) {
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_silahkan_pilih_foto_dahulu),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            // Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }

        Date sd, ed;
        String ds = dtStartPost + "T" + jmStart + ":59";
        String de = dtEndPost + "T" + jmEnd + ":01";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


        try {
            sd = sdf.parse(ds);
            ed = sdf.parse(de);


            if (sd.after(ed)) {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_waktu_selesai_harus_lebih_besar),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                // Toast.makeText(getActivity(), "Waktu mulai lebih besar dari waktu selesai", Toast.LENGTH_SHORT).show();
                return;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }





        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loading.show();
            mCreateDayOff();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        } else if (v.getId() == R.id.imgClose) {
            dismiss();
        } else if (v.getId() == R.id.mEdtMulai) {
            if (jamServer.equals("")){
                mGetServerTim();
            } else {
                openDateRangePickerStart();
            }

        } else if (v.getId() == R.id.mEdtSelesai) {

            String option = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_overtime_option), ""));

            if (option.equals("true")) {

                if (isInRange){

                    String end = mEdtSelesai.getText().toString();
                    if (TextUtils.isEmpty(end) || end.contains("null")) {

                      /*  MDToast.makeText(getActivity(), getResources().getString(R.string.title_pilih_waktu_mulai_dahulu),
                                MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();*/
                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_waktu_selesai_otomatis_terisi_jika) + " "+ timeWPOut + " " + getResources().getString(R.string.title_sampai_dengan) + " " + timwWPOutPlus,
                                MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                      /*  MDToast.makeText(getActivity(), getResources().getString(R.string.title_waktu_selesai_otomatis_terisi),
                                MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();*/

                    } else {

                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_waktu_selesai_otomatis_terisi),
                                MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                    }

                } else {

                    String start = mEdtMulai.getText().toString();
                    if (TextUtils.isEmpty(start) || start.contains("null")) {

                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_pilih_waktu_mulai_dahulu),
                                MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                    } else {

                        if (jamServer.equals("")){
                            mGetServerTim();
                        } else {
                            openDateRangePickerEnd();

                        }

                    }


                }




            } else {

                if (jamServer.equals("")){
                    mGetServerTim();
                } else {
                    openDateRangePickerEnd();

                }
            }



        }

    }


    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");*/

                            jamServer = response.body().getData();

                            // mTimeServer = date;

                            // Log.e(TAG, mTimeServer.toString());

                            // timer(mTimeServer);

                        }

                    } catch (Exception e) {
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }

    }

    void redirected (){

        Intent login = new Intent(getActivity(), OvertimeActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }

    private void mOpenCamera() {
        final String appPackageName = "." + getContext().getPackageName();
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            Log.v("cropPhoto", "if");
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 100);
        } else {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                Log.v("cropPhoto", "if");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            } else {

                File photoFile = null;
                photoFile = createImageFile();

                if (photoFile != null) {
                 /*   cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,  FileProvider.getUriForFile(getApplicationContext(), "com.mii.pms.provider", photoFile));
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);*/


                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                            getContext().getPackageName() + appPackageName,
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                    //COMPATIBILITY
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } else {
                        List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                        for (ResolveInfo resolveInfo : resInfoList) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                    }
                    //COMPATIBILITY
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }


    }


    private File createImageFile() {

        File root = new File(Environment.getExternalStorageDirectory(), "EATS/");

        if (!root.exists()) {
            root.mkdirs(); // this will create folder.
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",         // suffix
                    root      // directory
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();

        return image;
    }


}