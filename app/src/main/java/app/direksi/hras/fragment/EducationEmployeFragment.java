package app.direksi.hras.fragment;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.EducationEmployeAdapter;
import app.direksi.hras.R;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.model.DataEducationEmploye;
import app.direksi.hras.model.ResponseEducationEmploye;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class EducationEmployeFragment extends Fragment /*implements PenawaranDetailActivity.DataUpdateListener*/ {
    private RecyclerView mRootView;
    private List<DataEducationEmploye> dataList = new ArrayList<>();
    private List<String> existLoad = new ArrayList<>();
    private EducationEmployeAdapter cAdapter;
    private TextView textNoInternet;
    private ProgressBar loading;
    private SwipeRefreshLayout swiperefresh;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.nul_list, container, false);
        mRootView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
    }

    private void initRecyclerView() {
        cAdapter = new EducationEmployeAdapter(dataList, getActivity());
        mRootView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRootView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRootView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

      //  mRootView.addItemDecoration(itemDecorator);
        mRootView.setAdapter(cAdapter);
        mRootView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRootView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               /* Penawaran penawaran = penawaranList.get(position);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();

                GoTo("com.mii.erp.PenawaranDetailActivity", penawaran);*/
                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                dataList = new ArrayList<>();
                cAdapter = new EducationEmployeAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
                RefreshService();

            }});
        fetchService();

    }

    public static Fragment newInstance() {
        return new EducationEmployeFragment();
    }


    private static class FakePageAdapter extends RecyclerView.Adapter<EducationEmployeFragment.FakePageVH> {
        private final int numItems;

        FakePageAdapter(int numItems) {
            this.numItems = numItems;
        }

        @Override
        public EducationEmployeFragment.FakePageVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_relative_employe, viewGroup, false);

            return new EducationEmployeFragment.FakePageVH(itemView);
        }

        @Override
        public void onBindViewHolder(EducationEmployeFragment.FakePageVH fakePageVH, int i) {
            // do nothing
        }

        @Override
        public int getItemCount() {
            return numItems;
        }
    }

    private static class FakePageVH extends RecyclerView.ViewHolder {
        FakePageVH(View itemView) {
            super(itemView);
        }
    }




  /*  @Override
    public void onDataUpdate(List<PenawaranDetail> mData) {

        if (mData.get(0).getPnwrAdv().size() > 0 )
        {
            klienList.clear();
            klienList.addAll(mData.get(0).getPnwrAdv());
            cAdapter.notifyDataSetChanged();
            Log.v("AdvFragment", "aaaaaaaaaaa");
        }
        // put your UI update logic here
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((PenawaranDetailActivity) activity).registerDataUpdateListener(this);
    }*/

    public void fetchService() {

        if (dataList.size() > 0 || existLoad.size() > 0)
        {
            if (dataList.size() > 0) {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.GONE);
            }
            else
            {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.VISIBLE);
            }
        }
        else {

            textNoInternet.setVisibility(View.GONE);
            try {
                Log.v("DoneFragment", "masuk try");
                Map<String, String> data = new HashMap<>();
                data.put("page", "1");
                data.put("size", "50");
                data.put("employeeId", (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_employeeid_view), "")));


                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_token), ""));
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseEducationEmploye> call = api.getEducationEmploye( Authorization, data);

                call.enqueue(new Callback<ResponseEducationEmploye>() {
                    @Override
                    public void onResponse(Call<ResponseEducationEmploye> call, Response<ResponseEducationEmploye> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                        if (response.isSuccessful()) {

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                            if (response.body().getErrCode() == 0) {
                                if (response.body().getData().size() > 0) {
                                    dataList.clear();
                                    existLoad.add("Reload");
                                    dataList.addAll(response.body().getData());
                                    cAdapter.notifyDataSetChanged();
                                } else {
                                    existLoad.add("Reload");
                                    textNoInternet.setVisibility(View.VISIBLE);
                                }
                                loading.setVisibility(View.GONE);
                            } else {
                                loading.setVisibility(View.GONE);
                            }

                            loading.setVisibility(View.GONE);


                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            Log.v("DoneFragment", "tidak sukses");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEducationEmploye> call, Throwable t) {
                        Log.v("DoneFragment", "on failure");
                        loading.setVisibility(View.GONE);
                        // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                    }
                });
            } catch (Exception e) {
                Log.v("DoneFragment", "masuk catch");
                if (isAdded())
                    loading.setVisibility(View.GONE);
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
               // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

            }
        }


    }

    public void RefreshService() {


            loading.setVisibility(View.VISIBLE);
            textNoInternet.setVisibility(View.GONE);
            try {
                Log.v("DoneFragment", "masuk try");
                Map<String, String> data = new HashMap<>();
                data.put("page", "1");
                data.put("size", "50");
                data.put("employeeId", (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_employeeid_view), "")));


                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_token), ""));
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseEducationEmploye> call = api.getEducationEmploye( Authorization, data);

                call.enqueue(new Callback<ResponseEducationEmploye>() {
                    @Override
                    public void onResponse(Call<ResponseEducationEmploye> call, Response<ResponseEducationEmploye> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                        if (response.isSuccessful()) {

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                            if (response.body().getErrCode() == 0) {
                                if (response.body().getData().size() > 0) {
                                    dataList.clear();
                                    existLoad.add("Reload");
                                    dataList.addAll(response.body().getData());
                                    cAdapter.notifyDataSetChanged();
                                } else {
                                    existLoad.add("Reload");
                                    textNoInternet.setVisibility(View.VISIBLE);
                                }
                                loading.setVisibility(View.GONE);
                            } else {
                                loading.setVisibility(View.GONE);
                            }

                            loading.setVisibility(View.GONE);


                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            Log.v("DoneFragment", "tidak sukses");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseEducationEmploye> call, Throwable t) {
                        Log.v("DoneFragment", "on failure");
                        loading.setVisibility(View.GONE);
                        // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                    }
                });
            } catch (Exception e) {
                Log.v("DoneFragment", "masuk catch");
                if (isAdded())
                    loading.setVisibility(View.GONE);
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

            }



    }


}


