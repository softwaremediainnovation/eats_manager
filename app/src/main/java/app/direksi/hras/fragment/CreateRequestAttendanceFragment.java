package app.direksi.hras.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.CheckinActivity;
import app.direksi.hras.LocationAssistant;
import app.direksi.hras.R;
import app.direksi.hras.RequestAttendanceActivity;
import app.direksi.hras.app.AppConstants;
import app.direksi.hras.model.ResponseDataCheckAbsenIN;
import app.direksi.hras.model.ResponseGeneralRequestAttendance;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.BitmapImage;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static app.direksi.hras.app.AppConstants.AllowMock;
import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

public class CreateRequestAttendanceFragment extends DialogFragment implements View.OnClickListener,  LocationAssistant.Listener, OnMapReadyCallback, RadioGroup.OnCheckedChangeListener {

    private static final String TAG = CreateActivitiesFragment.class.getSimpleName();

    private DateFormat sdf;
    private String mCurrentPhotoPath;
    EditText mEdtDescription;
    private String deviceModel;
    private Button mButtonSend, mButtonCancel;
    private Bitmap rotatedBitmap;
    private boolean isIN = true;
    private RelativeLayout rlFoto;

    private SweetAlertDialog loading;
    //private RelativeLayout mLayoutChoose;

    PermissionsChecker checker;
    private static final String[] PERMISSIONS_READ_STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private Uri mCropImageUri = null;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    private String datenow;
    private Calendar calendar;
    private DateFormat dateFormat;
    private Date dateDismissUpdate = null, dateNow = null;


    private double latitude = 0, longitude = 0;

    private LocationManager manager;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;

    private ImageView imgClose;

    LocationAssistant assistant;
    private int Mock = 0;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private TextView txtAlamat;
    private RadioGroup mGroupPbon;
    private boolean loadAddress = false;
    private ImageView mImageView, mImageChoose;
    private boolean isSupportBiometric = false;
    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private String absenMethod = "";


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        deviceModel = Build.MANUFACTURER;

        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_request_attendance, container, false);


        // mLayoutChoose = rootView.findViewById(R.id.mLayoutChoose);
        absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_absen_method), ""));


        rlFoto = rootView.findViewById(R.id.rlFoto);
        rlFoto.setVisibility(View.GONE);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);
        mGroupPbon = rootView.findViewById(R.id.mGroupPbon);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        mEdtDescription = rootView.findViewById(R.id.mEdtDescription);
        txtAlamat = rootView.findViewById(R.id.txtAlamat);
        imgClose = rootView.findViewById(R.id.imgClose);
        mGroupPbon.setOnCheckedChangeListener(this);
        imgClose.setOnClickListener(this);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        checker = new PermissionsChecker(getActivity());

        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        datenow = dateFormat.format(calendar.getTime());


        Log.v(TAG, datenow);


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mButtonSend.setOnClickListener(this);




        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        mLocationCallback = new LocationCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        };


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        assistant = new LocationAssistant(getActivity(), this, LocationAssistant.Accuracy.HIGH, 1000, false);


       /* Intent intent = new Intent(getActivity(), GPSTrackerActivity.class);
        startActivityForResult(intent,1);*/
        mGetLocation();

        /*mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/


        mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                } else {
                    // startCropImageActivity();
                    mOpenCamera();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                } else {
                    // startCropImageActivity();
                    mOpenCamera();
                }
            }
        });

        if (checkBiometricSupport()) {
            isSupportBiometric = true;
        } else {

            isSupportBiometric = false;

        }

        if (isSupportBiometric) {
            if (checker.lacksPermissions(PERMISSIONS)) {
                startPermissionsActivity(PERMISSIONS);
            }
        }



        if(absenMethod.equals("foto") || absenMethod.equals("photo")){

            rlFoto.setVisibility(View.VISIBLE);

        } else {

            rlFoto.setVisibility(View.GONE);
        }


        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle(getActivity().getResources().getString(R.string.txt_biometric_verification))
                .setSubtitle(getActivity().getResources().getString(R.string.txt_mesage_presence_biometric))
                .setNegativeButtonText(getActivity().getResources().getString(R.string.title_batal_small))
                .setConfirmationRequired(false)
                .build();

        executor = ContextCompat.getMainExecutor(getContext());
        biometricPrompt = new BiometricPrompt(this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);

                if (errString.equals(getActivity().getResources().getString(R.string.title_batal_small))){

                } else {

                    MDToast.makeText(getActivity(),  errString.toString(),
                            MDToast.LENGTH_SHORT, MDToast.TYPE_INFO).show();

                }

            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                /*Toast.makeText(getContext(),
                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();*/
                loading.show();

                if (isIN){
                    mCheckAlreadyIn();
                }else {
                    mCheckAlreadyOut();
                }



            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                MDToast.makeText(getActivity(), getActivity().getResources().getString(R.string.txt_failed_verification),
                        MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();
            }
        });

        //biometricPrompt.authenticate(promptInfo);


        return rootView;
    }


    private Boolean checkBiometricSupport(){

        Boolean hasil = false;
        BiometricManager biometricManager = BiometricManager.from(getActivity());
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                hasil = true;
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                hasil = false;
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                hasil = false;
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                hasil = true;
                Log.e("MY_APP_TAG", "BIOMETRIC_ERROR_NONE_ENROLLED");
                // Prompts the user to create credentials that your app accepts.
                break;
        }

        return hasil;
    }

    private void startCropImageActivity(Uri imageUri) {
        /*CropImage.activity(imageUri)
                .start(getActivity(), this);*/
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }


    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                MDToast.makeText(getActivity(), "Cancelling, required permissions are not granted",
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
               // Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }

    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());

       /* if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mOpenCamera();
            } else {
                MDToast.makeText(getActivity(), "Cancelling, required permissions are not granted",
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            }
        }*/

    }

    private void startCropImageActivity() {
       /* CropImage.activity()
                .start(getActivity(), this);*/
    }


    private void getLastLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                drawMarker(latitude, longitude);
                getAddress(getActivity(), latitude, longitude);
                // mCreateActivities();

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null);
    }


    void mGetLocation() {

        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Bundle extras = null;
                            if (location != null)
                                extras = location.getExtras();
                            boolean isMockLocation = extras != null && extras.getBoolean(FusedLocationProviderApi.KEY_MOCK_LOCATION, false);

                            if (isMockLocation) {

                                if (AllowMock){

                                } else {

                                    mShowDialog(getResources().getString(R.string.title_matikan_mock_lokasi), 2);
                                    return;

                                }

                            }

                            if (location != null) {

                                latitude = location.getLatitude();
                                longitude = location.getLongitude();

                                drawMarker(latitude, longitude);
                                getAddress(getActivity(), latitude, longitude);
                                // mCreateActivities();


                            } else {
                                getLastLocation();
                            }


                        }


                    });
            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });


            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });

            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });


        } catch (Exception e) {
            Log.e("FingerprintHandler", e.getMessage());
        }
    }


    private void mShowDialog(String message, int type) {
        try {
            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                    edit()
                    .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                    .apply();

            loading.dismiss();
            SweetAlertDialog mShowDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(getResources().getString(R.string.title_gagal))
                    .setContentText(message)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            if (type == 0) {
                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                            } else if (type == 1) {
                                Intent intent = new Intent(getActivity(), RequestAttendanceActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            } else if (type == 2) {

                                Intent intent = new Intent(getActivity(), RequestAttendanceActivity.class);
                                startActivity(intent);
                                getActivity().finish();

                                startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));

                            } else if (type == 3) {

                                Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                                intent.setAction(Intent.ACTION_VIEW);
                                startActivity(intent);

                            } else if (type == 4) {

                                Intent intent = new Intent(getActivity(), RequestAttendanceActivity.class);
                                startActivity(intent);
                                getActivity().finish();

                            }

                        }
                    });


            mShowDialog.setCancelable(false);


            mShowDialog.show();
            Button btn = (Button) mShowDialog.findViewById(R.id.confirm_button);
            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            btn.setTextColor(getResources().getColor(R.color.colorWhite));
        } catch (Exception e) {
        }
    }


    private void mShowDialogNotYetOut() {
        try {
            loading.dismiss();
            SweetAlertDialog mShowDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getResources().getString(R.string.string_confirmation))
                    .setContentText(getResources().getString(R.string.confirmation_out_yesterday_request))
                    .setCancelText(getResources().getString(R.string.txt_in))
                    .setConfirmText(getResources().getString(R.string.txt_out))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                            sweetAlertDialog.cancel();

                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_out))
                                    .putBoolean(getResources().getString(R.string.pref_out_date), true)
                                    .apply();

                            if(absenMethod.equals("foto") || absenMethod.equals("photo")){

                                mCreateRequestPhoto();

                            } else {

                                mCreateRequestBiometric();

                            }


                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            sweetAlertDialog.cancel();

                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_in))
                                    .putBoolean(getResources().getString(R.string.pref_out_date), false)
                                    .apply();

                            if(absenMethod.equals("foto") || absenMethod.equals("photo")){

                                mCreateRequestPhoto();

                            } else {

                                mCreateRequestBiometric();

                            }

                        }
                    });
            mShowDialog.setCanceledOnTouchOutside(true);
            mShowDialog.setCancelable(true);
            mShowDialog.show();


            Button btn = mShowDialog.findViewById(R.id.confirm_button);
            btn.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_red));

            Button red = mShowDialog.findViewById(R.id.cancel_button);
            red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_gren));


        } catch (Exception e) {
        }
    }


    /*@Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                mCropImageUri = imageUri;
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        // handle result of CropImageActivity
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {


                Bitmap mImageBitmap = CompressImage.loadBitmap(result.getUri().toString());

                int width = 0;
                int height = 0;
                try {

                    if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                        width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                        height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                    } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                        width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                        height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                    } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                        width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                        height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                    } else {

                        width = mImageBitmap.getWidth();
                        height = mImageBitmap.getHeight();
                    }

                    Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                    //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                    Uri uri = CompressImage.getImageUri(getContext(), scaledBitmap);
                    *//*END OF COMPRESS FEATURE*//*

                    mImageChoose.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageURI(uri);
                    mCropImageUri = uri;

                } catch (Exception e){

                    try {

                        mImageChoose.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        mImageView.setImageURI(result.getUri());
                        mCropImageUri = result.getUri();

                    } catch (Exception f){
                        MDToast.makeText(getActivity(), "failed",
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                       // Toast.makeText(getActivity(), "failed", Toast.LENGTH_LONG).show();
                    }

                }

               // mLayoutChoose.setVisibility(View.GONE);


              *//*  mImageChoose.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageURI(result.getUri());

                mCropImageUri = result.getUri();*//*

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                MDToast.makeText(getActivity(), "Cropping failed",
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
               // Toast.makeText(getActivity(), "Cropping failed", Toast.LENGTH_LONG).show();
            }
        }

        if(requestCode == 1){
            Bundle extras = data.getExtras();
            Double longitude = extras.getDouble("Longitude");
            Double latitude = extras.getDouble("Latitude");
            //  errorText.setText("Latitude = " + latitude + " , Longitude = " + longitude);
           // errorText.setText("Mendapatkan Lokasi...");

            Location loc1 = new Location("");
            loc1.setLatitude(latitude);
            loc1.setLongitude(longitude);

            try {

                Double lat = Double.valueOf(loc1.getLatitude());
                Double longi = Double.valueOf(loc1.getLongitude());

                int height = SIZE_MARKER;
                int width = SIZE_MARKER;
                BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
                Bitmap b=bitmapdraw.getBitmap();
                Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, longi))
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                        .title("Lokasi Ku"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                        .zoom(15)                   // Sets the zoom
                        .bearing(0)                // Sets the orientation of the camera to east
                        .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            } catch (Exception e){

                String a = e.toString();
            }



            // CheckLatLong(loc1);



        }

    }*/

    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {

            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

                Bitmap bm = null;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 3;
                AssetFileDescriptor fileDescriptor = null;
                try {
                    fileDescriptor = getContext().getContentResolver().openAssetFileDescriptor(Uri.parse(mCurrentPhotoPath), "r");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                        fileDescriptor.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Matrix matrix = new Matrix();

                matrix.postRotate(90);
                int width = bm.getWidth();
                int height = bm.getHeight();


                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, width, height, true);

                if (deviceModel.equals("samsung")) {
                    rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                } else {
                    rotatedBitmap = scaledBitmap;
                }



                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    mCropImageUri = BitmapImage.getImageUri(getContext(), rotatedBitmap);
                } else {
                    mCropImageUri = BitmapImage.getImageUri1(getContext(), rotatedBitmap);
                }

              /*  mLayoutChoose.setVisibility(View.GONE);
                mImageChoose.setVisibility(View.GONE);
                mLayoutImageView.setVisibility(View.VISIBLE);
                mImageView.setImageBitmap(rotatedBitmap);*/


                mImageChoose.setVisibility(View.GONE);
                mImageView.setVisibility(View.VISIBLE);
                mImageView.setImageBitmap(rotatedBitmap);



            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
        }

    }


    public void mCreateRequestBiometric() {

        loading.show();


        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        datenow = dateFormat.format(calendar.getTime());


        RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"),
                PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_employeeid), ""));
        RequestBody InOut = RequestBody.create(MediaType.parse("text/plain"), "IN");
        RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), datenow);
        RequestBody Description = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtDescription));
        RequestBody Latitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(latitude));
        RequestBody Longitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(longitude));
        RequestBody Address = RequestBody.create(MediaType.parse("text/plain"), txtAlamat.getText().toString());


        if (isIN){
            InOut = RequestBody.create(MediaType.parse("text/plain"), "IN");
        }else {
            InOut = RequestBody.create(MediaType.parse("text/plain"), "OUT");
        }
        RequestBody IsYesterday  = RequestBody.create(MediaType.parse("text/plain"),
                Boolean.toString(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                        getResources().getString(R.string.pref_out_date), false)));

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        MultipartBody.Part multipart = null;
        if (absenMethod.equals("foto") || absenMethod.equals("photo")) {
            File file = new File(CompressImage.getRealPathFromURI(mCropImageUri, getActivity()));
            if (file.exists()) {
                String name = URLConnection.guessContentTypeFromName(file.getName());
                RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
                multipart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
            }
        }


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

        Call<ResponseGeneralRequestAttendance> call;

        if (!PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                getResources().getString(R.string.pref_out_date), false))
        {
            call = api.mCreateRequestAttendanceBiometric(Authorization, InOut, Description, Latitude, Longitude, Address, multipart);

        } else {

            InOut = RequestBody.create(MediaType.parse("text/plain"), "OUT");
            call = api.mCreateRequestAttendanceBiometric(Authorization, InOut, Description, Latitude, Longitude, Address, multipart, IsYesterday);

        }


        call.enqueue(new Callback<ResponseGeneralRequestAttendance>() {
            @Override
            public void onResponse(Call<ResponseGeneralRequestAttendance> call, Response<ResponseGeneralRequestAttendance> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loan_success), Toast.LENGTH_SHORT).show();
                            getActivity().finish();*/

                           if (response.body().getData().getStatus()== null){

                               SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                               alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                               alertDialog.setContentText("");
                               alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                   @Override
                                   public void onClick(SweetAlertDialog sweetAlertDialog) {


                                       PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                               edit()
                                               .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                                               .apply();

                                       Intent login = new Intent(getActivity(), RequestAttendanceActivity.class);
                                       login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                       startActivity(login);
                                   }
                               });
                               alertDialog.show();

                               Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                               btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                               btn.setTextColor(getResources().getColor(R.color.colorWhite));

                           } else {

                               SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                               alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
                               alertDialog.setContentText(response.body().getData().getResult());
                               alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                   @Override
                                   public void onClick(SweetAlertDialog sweetAlertDialog) {


                                       PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                               edit()
                                               .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                                               .apply();

                                       Intent login = new Intent(getActivity(), RequestAttendanceActivity.class);
                                       login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                       startActivity(login);
                                   }
                               });
                               alertDialog.show();

                               Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                               btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                               btn.setTextColor(getResources().getColor(R.color.colorWhite));
                           }




                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }

                        }

                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception e){

                        }

                    }

                } catch (Exception e) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception s){

                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseGeneralRequestAttendance> call, Throwable t) {
                try {
                    loading.dismiss();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    redirected();
                } catch (Exception e){

                }

            }
        });

    }

    public void mCreateRequestPhoto() {

        loading.show();


        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        datenow = dateFormat.format(calendar.getTime());


        RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"),
                PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_employeeid), ""));
        RequestBody InOut = RequestBody.create(MediaType.parse("text/plain"), "IN");
        RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), datenow);
        RequestBody Description = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtDescription));
        RequestBody Latitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(latitude));
        RequestBody Longitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(longitude));
        RequestBody Address = RequestBody.create(MediaType.parse("text/plain"), txtAlamat.getText().toString());


        if (isIN){
            InOut = RequestBody.create(MediaType.parse("text/plain"), "IN");
        }else {
            InOut = RequestBody.create(MediaType.parse("text/plain"), "OUT");
        }
        RequestBody IsYesterday  = RequestBody.create(MediaType.parse("text/plain"),
                Boolean.toString(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                        getResources().getString(R.string.pref_out_date), false)));

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        MultipartBody.Part multipart = null;
        if (absenMethod.equals("foto") || absenMethod.equals("photo")) {
            File file = new File(CompressImage.getRealPathFromURI(mCropImageUri, getActivity()));
            if (file.exists()) {
                String name = URLConnection.guessContentTypeFromName(file.getName());
                RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
                multipart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
            }
        }


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

        Call<ResponseGeneralRequestAttendance> call;

        if (!PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                getResources().getString(R.string.pref_out_date), false))
            call = api.mCreateRequestAttendancePhoto(Authorization, InOut, Description, Latitude, Longitude, Address, multipart);
        else
            InOut = RequestBody.create(MediaType.parse("text/plain"), "OUT");
        call = api.mCreateRequestAttendancePhoto(Authorization, InOut, Description, Latitude, Longitude, Address, multipart, IsYesterday);



        call.enqueue(new Callback<ResponseGeneralRequestAttendance>() {
            @Override
            public void onResponse(Call<ResponseGeneralRequestAttendance> call, Response<ResponseGeneralRequestAttendance> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loan_success), Toast.LENGTH_SHORT).show();
                            getActivity().finish();*/

                            if (response.body().getData().getStatus()== null){

                                SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                                alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                alertDialog.setContentText("");
                                alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                                edit()
                                                .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                                                .apply();

                                        Intent login = new Intent(getActivity(), RequestAttendanceActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                    }
                                });
                                alertDialog.show();

                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));

                            } else {

                                SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                                alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
                                alertDialog.setContentText(response.body().getData().getResult());
                                alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                                edit()
                                                .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                                                .apply();

                                        Intent login = new Intent(getActivity(), RequestAttendanceActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                    }
                                });
                                alertDialog.show();

                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));
                            }




                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }

                        }

                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception e){

                        }

                    }

                } catch (Exception e) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception s){

                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseGeneralRequestAttendance> call, Throwable t) {
                try {
                    loading.dismiss();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    redirected();
                } catch (Exception e){

                }

            }
        });

    }

    private void mAttemptCreateActivities() {

        mEdtDescription.setError(null);

        String deskripsi = mEdtDescription.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(deskripsi)) {
            mEdtDescription.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtDescription;
            cancel = true;
        }

        if (Double.toString(latitude).equals("0.0") & Double.toString(longitude).equals("0.0")){

            MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_mendapatkan_lokasi),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            // Toast.makeText(getActivity(), "Gagal mendapatkan lokasi. Silahkan coba beberapa saat lagi", Toast.LENGTH_SHORT).show();
            redirected();
            return;
        }

        if (!loadAddress) {
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_mendapatkan_lokasi),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            //Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }


        if(absenMethod.equals("foto") || absenMethod.equals("photo")){

            if (mCropImageUri == null) {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_silahkan_pilih_foto_dahulu),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
             }
        }



        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {


            if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                if (isSupportBiometric) {

                    biometricPrompt.authenticate(promptInfo);

                } else{
                    loading.show();

                    if (isIN){
                        mCheckAlreadyIn();
                    }else {
                        mCheckAlreadyOut();
                    }
                }

            } else {

                loading.show();

                    if (isIN){
                        mCheckAlreadyIn();
                    }else {
                        mCheckAlreadyOut();
                    }

            }



         //   mCreateActivities();

        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateActivities();
        } else if (v.getId() == R.id.imgClose) {
            /* dismiss();*/
            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                    edit()
                    .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                    .apply();

            getActivity(). finish();
            getActivity().overridePendingTransition(0, 0);
            startActivity(getActivity().getIntent());
            getActivity().overridePendingTransition(0, 0);

           /* Intent login = new Intent(getActivity(), AktivitasActivity.class);
            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(login);*/
        }

    }

    @Override
    public void onExplainLocationPermission() {
        Log.v("Maps", "onExplainLocationPermission");
     /*   new AlertDialog.Builder(this)
                .setMessage("Permission")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        assistant.requestLocationPermission();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       *//* tvLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                assistant.requestLocationPermission();
                            }
                        });*//*
                    }
                })
                .show();*/
    }

    @Override
    public void onNeedLocationPermission() {
        Log.v("Maps", "onNeedLocationPermission");
        /* tvLocation.setText("Need\nPermission");*/
      /*  tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assistant.requestLocationPermission();
            }
        });*/
        assistant.requestAndPossiblyExplainLocationPermission();
    }

    @Override
    public void onNeedLocationSettingsChange() {
        Log.v("Maps", "onNeedLocationSettingsChange");
      /*  new AlertDialog.Builder(this)
                .setMessage("SWITCH")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        assistant.changeLocationSettings();
                    }
                })
                .show();*/
    }

    @Override
    public void onFallBackToSystemSettings(View.OnClickListener fromView, DialogInterface.OnClickListener fromDialog) {
        Log.v("Maps", "onFallBackToSystemSettings");
     /*   new AlertDialog.Builder(this)
                .setMessage("SWITCH")
                .setPositiveButton("OK", fromDialog)
                .show();*/
    }

    @Override
    public void onNewLocationAvailable(Location location) {
        Log.v("Maps", "onNewLocationAvailable");
        if (location == null) return;
     /*   tvLocation.setOnClickListener(null);
        tvLocation.setText(location.getLongitude() + "\n" + location.getLatitude());
        tvLocation.setAlpha(1.0f);
        tvLocation.animate().alpha(0.5f).setDuration(400);*/
    }

    @Override
    public void onMockLocationsDetected(View.OnClickListener fromView, DialogInterface.OnClickListener fromDialog) {
        Mock = 1;
    }

    @Override
    public void onError(LocationAssistant.ErrorType type, String message) {
        Log.v("Maps", "onError");
        /* tvLocation.setText(getString(R.string.error));*/
    }

    @Override
    public void onLocationPermissionPermanentlyDeclined(View.OnClickListener fromView,
                                                        DialogInterface.OnClickListener fromDialog) {
        Log.v("Maps", "onLocationPermissionPermanentlyDeclined");
     /*   new AlertDialog.Builder(this)
                .setMessage("DECLINE")
                .setPositiveButton("OK", fromDialog)
                .show();*/
    }



    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try{
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.my_json_maps));

            if (!isSuccess)
            {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        }
        catch (Resources.NotFoundException ex)
        {
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        else
        {
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);



            map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    // Toast.makeText(getActivity(), "Good", Toast.LENGTH_SHORT).show();

                    try {
                        mMap.clear();
                        mGetLocation();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_berhasil_update_lokasi) , MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();


                    }catch (Exception e){
                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_update_lokasi), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();


                    }

                    return false;

                }
            });
        }

      /*  Double lat = Double.valueOf(DataAttendance.getLatitude().replace(",",""));
        Double longi = Double.valueOf(DataAttendance.getLongitude().replace(",",""));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, longi))
                .title("Lokasi Check in"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                .zoom(15)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

        LatLng currentLatLng = new LatLng(-7.265465,
                112.745543);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(currentLatLng,
                3);
        mMap.moveCamera(update);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit()
                .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                .apply();

        getActivity(). finish();
        getActivity().overridePendingTransition(0, 0);
        startActivity(getActivity().getIntent());
        getActivity().overridePendingTransition(0, 0);

       /* Intent login = new Intent(getActivity(), AktivitasActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);*/
        // Add you codition
    }

    void drawMarker (Double lat,Double longi){
        try {



            int height = SIZE_MARKER;
            int width = SIZE_MARKER;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);

            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, longi))
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                    .title(getResources().getString(R.string.title_lokasi_ku)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } catch (Exception e){

            String a = e.toString();
        }
    }

    void getAddress(Context context, double LATITUDE, double LONGITUDE) {

        //Set Address

        if (context!= null) {
            try {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
                if (addresses != null && addresses.size() > 0) {


                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();
                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                    txtAlamat.setText(address);
                    loadAddress = true;
              /*  Log.d(TAG, "getAddress:  address" + address);
                Log.d(TAG, "getAddress:  city" + city);
                Log.d(TAG, "getAddress:  state" + state);
                Log.d(TAG, "getAddress:  postalCode" + postalCode);
                Log.d(TAG, "getAddress:  knownName" + knownName);*/

                }
            } catch (IOException e) {
                e.printStackTrace();
                txtAlamat.setText("-");
                loadAddress = false;
            }
        }

    }

    void redirected (){

        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit()
                .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                .apply();

        Intent login = new Intent(getActivity(), RequestAttendanceActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }


    private void mOpenCamera() {
        final String appPackageName = "." + getContext().getPackageName();
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            Log.v("cropPhoto", "if");
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 100);
        } else {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                Log.v("cropPhoto", "if");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            } else {

                File photoFile = null;
                photoFile = createImageFile();

                if (photoFile != null) {
                 /*   cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,  FileProvider.getUriForFile(getApplicationContext(), "com.mii.pms.provider", photoFile));
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);*/


                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                            getContext().getPackageName() + appPackageName,
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                    //COMPATIBILITY
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } else {
                        List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                        for (ResolveInfo resolveInfo : resInfoList) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                    }
                    //COMPATIBILITY
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }


    }




    private File createImageFile() {

        File root = new File(Environment.getExternalStorageDirectory(), "EATS/");

        if (!root.exists()) {
            root.mkdirs(); // this will create folder.
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",         // suffix
                    root      // directory
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();

        return image;
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.mRadioYes) {
            isIN = true;


        } else if (checkedId == R.id.mRadioNo) {
            isIN = false;

        }
    }

    private void mCheckAlreadyIn() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataCheckAbsenIN> call = api.getDataCheckAbsenIN(Authorization);


            call.enqueue(new Callback<ResponseDataCheckAbsenIN>() {


                @Override
                public void onResponse(Call<ResponseDataCheckAbsenIN> call, Response<ResponseDataCheckAbsenIN> response) {

                    try {

                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().getStatus().equals(getResources().getString(R.string.response_success))) {


                                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                        edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_in))
                                        .putBoolean(getResources().getString(R.string.pref_out_date), false)
                                        .apply();

                                if(absenMethod.equals("foto") || absenMethod.equals("photo")){

                                    mCreateRequestPhoto();

                                } else {

                                    mCreateRequestBiometric();

                                }

                            } else {

                                if (response.body().getData().getResult().equals("Anda sudah Absen IN hari ini!")) {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                } else if (response.body().getData().getResult().equals("Anda belum Absen OUT kemarin!")) {

                                    if (isAdded())
                                        mShowDialogNotYetOut();

                                } else {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                }

                            }


                        } else {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseDataCheckAbsenIN> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
        }

    }


    private void mCheckAlreadyOut() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Log.v(TAG, Authorization);

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataCheckAbsenIN> call = api.getDataCheckAbsenOUT(Authorization);


            call.enqueue(new Callback<ResponseDataCheckAbsenIN>() {


                @Override
                public void onResponse(Call<ResponseDataCheckAbsenIN> call, Response<ResponseDataCheckAbsenIN> response) {

                    try {


                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().getStatus().equals(getResources().getString(R.string.response_success))) {


                                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                        edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_out))
                                        .putBoolean(getResources().getString(R.string.pref_out_date), false)
                                        .apply();


                                if(absenMethod.equals("foto") || absenMethod.equals("photo")){

                                    mCreateRequestPhoto();

                                } else {

                                    mCreateRequestBiometric();

                                }

                            } else {

                                if (response.body().getData().getResult().equals("Anda sudah Absen OUT hari ini!")) {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                } else if (response.body().getData().getResult().equals("Anda belum Absen OUT kemarin!")) {

                                    if (isAdded())
                                        mShowDialogNotYetOut();

                                } else if (response.body().getData().getResult().equals("Anda belum Absen IN hari ini!")) {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                } else {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                }

                            }


                        } else {

                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseDataCheckAbsenIN> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
        }

    }


}
