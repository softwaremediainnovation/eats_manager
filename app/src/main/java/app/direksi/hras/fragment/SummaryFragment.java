package app.direksi.hras.fragment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.DetailListAttendanceActivity;
import app.direksi.hras.R;
import app.direksi.hras.SelectedDayoffActivity;
import app.direksi.hras.SelectedOvertimeActivity;
import app.direksi.hras.SelectedReimburseActivity;
import app.direksi.hras.SelectedReprimandActivity;
import app.direksi.hras.adapter.GeneralRuleAdapter;
import app.direksi.hras.adapter.OrganizationRuleAdapter;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.model.DataSummary;
import app.direksi.hras.model.EmployeeInfoGeneralRule;
import app.direksi.hras.model.EmployeeInfoOrganizationRule;
import app.direksi.hras.model.ResponseDataGetSummary;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 26/08/2019.
 */


public class SummaryFragment  extends Fragment /*implements PenawaranDetailActivity.DataUpdateListener*/ {
    private RecyclerView recyclerViewGeneral, recyclerViewOrganization;
    private List<EmployeeInfoGeneralRule> dataListGR = new ArrayList<>();
    private List<EmployeeInfoOrganizationRule> dataListOR = new ArrayList<>();
    private List<String> existLoad = new ArrayList<>();
    private GeneralRuleAdapter GRAdapter;
    private OrganizationRuleAdapter ORAdapter;
    private TextView textNoInternet, lblOrgRule, lblGeneralRule;
    private ProgressBar loading;
    private SwipeRefreshLayout swiperefresh;
    private ScrollView sv1;
    private TextView txtCuti, txtSP, txtLembur, txtKlaim, txtLoan, txtPayout, txtSisa, txtTelat;
    private LinearLayout llDayoff, llReprimand, llOvertime, llReimburse, llSumAttendannce;
    private DataSummary dataSummary = new DataSummary();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_summary, container, false);

        txtCuti = (TextView) v.findViewById(R.id.txtCuti);
        txtSP = (TextView) v.findViewById(R.id.txtSP);
        txtLembur = (TextView) v.findViewById(R.id.txtLembur);
        txtKlaim = (TextView) v.findViewById(R.id.txtKlaim);
        txtLoan = (TextView) v.findViewById(R.id.txtLoan);
        txtPayout = (TextView) v.findViewById(R.id.txtPayout);
        txtSisa = (TextView) v.findViewById(R.id.txtSisa);
        txtTelat = (TextView) v.findViewById(R.id.txtTelat);


        llSumAttendannce = (LinearLayout) v.findViewById(R.id.llSumAttendannce);
        llSumAttendannce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_attendance_id), ""));
                String first = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_attendance_firstname), ""));
                String last = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_attendance_lastname), ""));

                DataEmployee data = new DataEmployee(Long.valueOf(id), first, last);


                PreferenceManager.getDefaultSharedPreferences(getContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(getContext(), DetailListAttendanceActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);
            }
        });

        llDayoff = (LinearLayout) v.findViewById(R.id.llDayoff);
        llDayoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SelectedDayoffActivity.class);
                startActivity(intent);
            }
        });
        llReprimand = (LinearLayout) v.findViewById(R.id.llReprimand);
        llReprimand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SelectedReprimandActivity.class);
                startActivity(intent);
            }
        });
        llOvertime = (LinearLayout) v.findViewById(R.id.llOvertime);
        llOvertime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SelectedOvertimeActivity.class);
                startActivity(intent);
            }
        });
        llReimburse = (LinearLayout) v.findViewById(R.id.llReimburse);
        llReimburse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SelectedReimburseActivity.class);
                startActivity(intent);
            }
        });

        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        sv1 = (ScrollView) v.findViewById(R.id.mLayoutMain);
        sv1.setVisibility(View.GONE);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
    }

    private void initRecyclerView() {
      /*  GRAdapter = new GeneralRuleAdapter(dataListGR, getActivity());
        ORAdapter = new OrganizationRuleAdapter(dataListOR, getActivity());
        recyclerViewGeneral.setHasFixedSize(true);
        recyclerViewOrganization.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManagerGR = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManagerOR = new LinearLayoutManager(getActivity());

        recyclerViewGeneral.setLayoutManager(mLayoutManagerGR);
        recyclerViewOrganization.setLayoutManager(mLayoutManagerOR);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerViewGeneral.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOrganization.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

        recyclerViewGeneral.addItemDecoration(itemDecorator);
        recyclerViewGeneral.setAdapter(GRAdapter);
        recyclerViewGeneral.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewGeneral, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               *//* Penawaran penawaran = penawaranList.get(position);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();

                GoTo("com.mii.erp.PenawaranDetailActivity", penawaran);*//*
                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerViewOrganization.addItemDecoration(itemDecorator);
        recyclerViewOrganization.setAdapter(ORAdapter);
        recyclerViewOrganization.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewOrganization, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               *//* Penawaran penawaran = penawaranList.get(position);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();

                GoTo("com.mii.erp.PenawaranDetailActivity", penawaran);*//*
                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
       *//* swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                dataList = new ArrayList<>();
                cAdapter = new EmployeeAttendanceAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
              //  RefreshService();

            }});*//*
        fetchService();*/

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swiperefresh.setRefreshing(false);
                loading.setVisibility(View.VISIBLE);
                sv1.setVisibility(View.GONE);
                existLoad.clear();
                fetchService();
                //  RefreshService();

            }});

        fetchService();

    }

    public static Fragment newInstance() {
        return new SummaryFragment();
    }



    public void fetchService() {

        if ( existLoad.size() > 0)
        {

            sv1.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
            textNoInternet.setVisibility(View.GONE);

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            txtSP.setText(dataSummary.getReprimand().toString());
            txtCuti.setText(dataSummary.getDayOffs().toString() + " hari");
            txtKlaim.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getReimbursement()));
            txtLoan.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getLoan()));
            txtPayout.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getPayout()));
            txtSisa.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getLoan() - dataSummary.getPayout()));


            Double temp1 = Double.parseDouble(dataSummary.getOvertime().toString())/60;
            Double temp2 = Double.parseDouble(dataSummary.getOvertime().toString()) % 60;
            Double temp3 =  Double.parseDouble(dataSummary.getOvertime().toString());

            int hour =  temp1.intValue();
            int minut = temp2.intValue();
            int total = temp3.intValue();

            txtLembur.setText(hour + " " +
                    getResources().getString(R.string.title_jam) + " "
                    + minut + " " +
                    getResources().getString(R.string.title_menit)
                    +" ( " +
                    getResources().getString(R.string.title_total) + " "
                    + total + " " +
                    getResources().getString(R.string.title_menit) +" )");

            Double temp11 = dataSummary.getLate() / 60;
            Double temp22 = dataSummary.getLate() % 60;
            Double temp33 = dataSummary.getLate();

            int hour1 = temp11.intValue();
            int minut1 = temp22.intValue();
            int total1 = temp33.intValue();

            txtTelat.setText(hour1 + " " +
                    getResources().getString(R.string.title_jam) + " "
                    + minut1 + " " +
                    getResources().getString(R.string.title_menit)
                    +" ( " +
                    getResources().getString(R.string.title_total) + " "
                    + total1 + " " +
                    getResources().getString(R.string.title_menit) +" )");


        }
        else {

            textNoInternet.setVisibility(View.GONE);
            try {
                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_token), ""));
                Map<String, String> data = new HashMap<>();
                data.put("employeeId", (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_employeeid_view), "")));

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseDataGetSummary> call = api.getSummaryEmployee( Authorization, data);

                call.enqueue(new Callback<ResponseDataGetSummary>() {
                    @Override
                    public void onResponse(Call<ResponseDataGetSummary> call, Response<ResponseDataGetSummary> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                        if (response.isSuccessful()) {

                            if (response.body().getErrCode() == 0) {
                                sv1.setVisibility(View.VISIBLE);

                                dataSummary = response.body().getData();
                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                txtSP.setText(dataSummary.getReprimand().toString());
                                txtCuti.setText(dataSummary.getDayOffs().toString() + " " +  getResources().getString(R.string.title_hari));
                                txtKlaim.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getReimbursement()));
                                txtLoan.setText(getResources().getString(R.string.title_rp) +" " + formatter.format(dataSummary.getLoan()));
                                txtPayout.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getPayout()));
                                txtSisa.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(dataSummary.getLoan() - dataSummary.getPayout()));

                                Double temp1 = Double.parseDouble(dataSummary.getOvertime().toString()) / 60;
                                Double temp2 = Double.parseDouble(dataSummary.getOvertime().toString()) % 60;
                                Double temp3 = Double.parseDouble(dataSummary.getOvertime().toString());

                                int hour = temp1.intValue();
                                int minut = temp2.intValue();
                                int total = temp3.intValue();

                                txtLembur.setText(hour + " " +
                                        getResources().getString(R.string.title_jam) + " "
                                        + minut + " " +
                                        getResources().getString(R.string.title_menit)
                                        +" ( " +
                                        getResources().getString(R.string.title_total) + " "
                                        + formatter.format(total ) + " " +
                                        getResources().getString(R.string.title_menit) +" )");


                                Double temp11 = dataSummary.getLate() / 60;
                                Double temp22 = dataSummary.getLate() % 60;
                                Double temp33 = dataSummary.getLate();

                                int hour1 = temp11.intValue();
                                int minut1 = temp22.intValue();
                                int total1 = temp33.intValue();

                                txtTelat.setText(hour1 + " " +
                                        getResources().getString(R.string.title_jam) + " "
                                        + minut1 + " " +
                                        getResources().getString(R.string.title_menit)
                                        +" ( " +
                                        getResources().getString(R.string.title_total) + " "
                                        + formatter.format(total1 ) + " " +
                                        getResources().getString(R.string.title_menit) +" )");


                                existLoad.add("Reload");
                            } else {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                               // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            }


                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                            /*if (response.body().getErrCode() == 0 & (response.body().getData().getRules().size() > 0 ||
                                    response.body().getData().getOrgRules().size() > 0)) {

                                sv1.setVisibility(View.VISIBLE);
                                if (response.body().getData().getRules().size() > 0) {
                                    dataListGR.clear();
                                    existLoad.add("Reload");
                                    dataListGR.addAll(response.body().getData().getRules());
                                    GRAdapter.notifyDataSetChanged();
                                    lblGeneralRule.setVisibility(View.VISIBLE);
                                } else {
                                    existLoad.add("Reload");
                                    lblGeneralRule.setVisibility(View.GONE);
                                }
                                if (response.body().getData().getOrgRules().size() > 0) {
                                    dataListOR.clear();
                                    existLoad.add("Reload");
                                    dataListOR.addAll(response.body().getData().getOrgRules());
                                    ORAdapter.notifyDataSetChanged();
                                    lblOrgRule.setVisibility(View.VISIBLE);
                                } else {
                                    existLoad.add("Reload");
                                    lblOrgRule.setVisibility(View.GONE);
                                }



                                loading.setVisibility(View.GONE);
                            } else {
                                existLoad.add("Reload");
                                textNoInternet.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                            }*/

                            loading.setVisibility(View.GONE);


                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            Log.v("DoneFragment", "tidak sukses");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataGetSummary> call, Throwable t) {
                        Log.v("DoneFragment", "on failure");
                        loading.setVisibility(View.GONE);
                        // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                    }
                });
            } catch (Exception e) {
                Log.v("DoneFragment", "masuk catch");
                if (isAdded())
                    loading.setVisibility(View.GONE);
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

            }
        }


    }






}



