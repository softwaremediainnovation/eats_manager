package app.direksi.hras.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.AssetNewActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseCreateAsset;
import app.direksi.hras.model.ResponseDataKategoriAset;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_1;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_2;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_3;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateAssetFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateAssetFragment.class.getSimpleName();
    private DateFormat sdf, mViewSdf;

    EditText mKeperluan,
            mEdtNote, mEdtLast, mCategory;
    private String mStartDate = "";
    TextInputLayout  mLayoutMulai;
    EditText  mEdtMulai;


    private Button mButtonSend, mButtonCancel;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri = null;
    private SweetAlertDialog loading;
    private ImageView imgClose;

    private Integer mIdConstanta[];
    private String mNamaConstanta[], valueConstanta [];
    private Integer constanta, mHowManyDays;
    private File file;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_asset, container, false);


        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        mEdtLast = rootView.findViewById(R.id.mEdtLast);

        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        imgClose = rootView.findViewById(R.id.imgClose);


        mCategory = rootView.findViewById(R.id.mCategory);
        mCategory.setOnClickListener(this);
        mKeperluan = rootView.findViewById(R.id.mKeperluan);
        mKeperluan.setOnClickListener(this);
        mButtonSend.setOnClickListener(this);
        imgClose.setOnClickListener(this);



        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mLayoutMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerStart();
            }
        });

        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerStart();
            }
        });


        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mGetConstanta();
        return rootView;
    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(getActivity(), this);
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }


    public void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        returnUri = Uri.fromFile(photo);
        startActivityForResult(intent, 100);
        Log.v(TAG, "takePhoto " + 100);
    }

    private void openDateRangePickerStart() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(result.getUri());

                    Bitmap mImageBitmap = BitmapFactory.decodeStream(inputStream);
                    int width = 0;
                    int height = 0;
                    try {


                        if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                        } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                        } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                        } else {

                            width = mImageBitmap.getWidth();
                            height = mImageBitmap.getHeight();
                        }

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                        //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                        // Uri uri = CompressImage.getImageUri(getActivity(), scaledBitmap);
                        String urr = CompressImage.makeBitmapPath(getActivity(), scaledBitmap);

                        file = new File (urr);
                        returnUri = result.getUri();

                        mImageChoose.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        mImageView.setImageBitmap(scaledBitmap);

                    } catch (Exception e){


                        MDToast.makeText(getActivity(), e.toString(),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();

                    }

                } catch (IOException e){
                    String temp = e.getMessage().toString();

                    MDToast.makeText(getActivity(), temp,
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();

                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();

                MDToast.makeText(getActivity(), error.toString(),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();

            }
        }
    }


    public void mCreateDayOff() {


        Date currentTime = Calendar.getInstance().getTime();

        RequestBody Name = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mKeperluan));
        RequestBody LastLocation = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtLast));
        RequestBody ConstantaID = RequestBody.create(MediaType.parse("text/plain"), constanta.toString());

       /* RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"),
                PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_employeeid), ""));
        RequestBody DateCreated = RequestBody.create(MediaType.parse("text/plain"), sdf.format(currentTime));*/
        RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));


        RequestBody ExpiredDate = RequestBody.create(MediaType.parse("text/plain"), mStartDate);



        //File creating from selected URL
        //File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
        MultipartBody.Part multipart = null;
        if (file.exists()) {
            String name = URLConnection.guessContentTypeFromName(file.getName());
            RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
            multipart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
        }

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseCreateAsset> call = api.mCreateAsset(Authorization,
                Name, LastLocation, Note, ExpiredDate, ConstantaID,  multipart);

        call.enqueue(new Callback<ResponseCreateAsset>() {
            @Override
            public void onResponse(Call<ResponseCreateAsset> call, Response<ResponseCreateAsset> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                          /*  dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.asset_success), Toast.LENGTH_SHORT).show();


                            Intent intent = new Intent(getActivity(), AssetNewActivity.class);
                            startActivity(intent);
                            getActivity().finish();*/


                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    Intent login = new Intent(getActivity(), AssetNewActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));

                           /* new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), AssetNewActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);
                                        }
                                    })
                                    .show();*/

                        } else {

                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }

                        }

                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception e){

                        }

                    }

                } catch (Exception e) {

                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception s){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseCreateAsset> call, Throwable t) {
                try {
                    loading.dismiss();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    redirected();
                } catch (Exception e){

                }

            }
        });

    }

    private void mAttemptCreateDayOff() {

        mCategory.setError(null);
        mKeperluan.setError(null);
        mEdtNote.setError(null);
        mEdtLast.setError(null);


        String categori = mCategory.getText().toString();
        String reason = mKeperluan.getText().toString();
        String note = mEdtNote.getText().toString();
        String last = mEdtLast.getText().toString();


        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(categori)) {
            mCategory.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mCategory;
            cancel = true;
        }

        if (TextUtils.isEmpty(reason)) {
            mKeperluan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mKeperluan;
            cancel = true;
        }

        if (TextUtils.isEmpty(last)) {
            mEdtLast.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtLast;
            cancel = true;
        }

        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }

        if (returnUri == null) {
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_silahkan_pilih_foto_dahulu),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loading.show();
            mCreateDayOff();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        } else if (v.getId() == R.id.mCategory) {
            try {
                if (mIdConstanta.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mCategory.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mCategory.setText(mNamaConstanta[item]);



                            // checkTglMasuk();

                            constanta = mIdConstanta[item];
                            imm.hideSoftInputFromWindow(mCategory.getWindowToken(), 0);

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        }

    }

    void redirected (){

        Intent login = new Intent(getActivity(), AssetNewActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataKategoriAset> call = api.mGetConstantaAset(Authorization);

            call.enqueue(new Callback<ResponseDataKategoriAset>() {
                @Override
                public void onResponse(Call<ResponseDataKategoriAset> call, Response<ResponseDataKategoriAset> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {
                                mIdConstanta = new Integer[response.body().getData().size()];
                                mNamaConstanta = new String[response.body().getData().size()];
                                valueConstanta = new String[response.body().getData().size()];

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mIdConstanta[i] = response.body().getData().get(i).getConstantaID();
                                    mNamaConstanta[i] = response.body().getData().get(i).getName();
                                    valueConstanta[i] = response.body().getData().get(i).getValue();
                                }

                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseDataKategoriAset> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }


}