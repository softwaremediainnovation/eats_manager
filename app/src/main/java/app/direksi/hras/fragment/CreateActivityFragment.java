package app.direksi.hras.fragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.text.DateFormat;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import  app.direksi.hras.R;
import  app.direksi.hras.model.ResponseConstanta;
import  app.direksi.hras.model.ResponseCreateCuti;
import  app.direksi.hras.util.BitmapImage;
import  app.direksi.hras.util.CompressImage;
import  app.direksi.hras.util.DefaultFormatter;
import  app.direksi.hras.util.Path;
import  app.direksi.hras.util.Validation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateActivityFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateCutiFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;


    TextInputLayout mLayoutSelesai, mLayoutMulai;
    EditText mEdtSelesai, mEdtMulai,
            mKeperluan,
            mEdtNote;

    TextInputLayout mReason;

    private Integer mIdConstanta[];
    private String mNamaConstanta[];
    private Button mButtonSend;
    private Integer constanta, mHowManyDays;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_aktivitas, container, false);


     /*   mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        mLayoutMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mLayoutSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mKeperluan = rootView.findViewById(R.id.mKeperluan);
        mKeperluan.setOnClickListener(this);
        mButtonSend.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    startGallery();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    startGallery();
                }
            }
        });


        mGetConstanta();


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/

        return rootView;
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == 1000) {
                returnUri = data.getData();
                Bitmap bitmapImage = null;

                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                } catch (IOException e) {
                    Log.v(TAG, e.getMessage());
                }

                Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, bitmapImage);

                String a = Path.getRealPathFromURI(getActivity(), returnUri);

                try {

                    mImageChoose.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(android.R.color.transparent);
                    mImageView.setImageBitmap(BitmapImage.modifyOrientation(scaled, a));

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }


    private void openDateRangePicker() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEndDate = sdf.format(selectedDate.getEndDate().getTime());

                mHowManyDays = DefaultFormatter.calculateNumberOfDaysExcludeWeekends(selectedDate.getStartDate().getTime(),
                        selectedDate.getEndDate().getTime());


                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));
                mEdtSelesai.setText(mViewSdf.format(selectedDate.getEndDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(true);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseConstanta> call = api.mGetConstanta(Authorization);

            call.enqueue(new Callback<ResponseConstanta>() {
                @Override
                public void onResponse(Call<ResponseConstanta> call, Response<ResponseConstanta> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getDataConstanta().size() > 0) {
                                mIdConstanta = new Integer[response.body().getDataConstanta().size()];
                                mNamaConstanta = new String[response.body().getDataConstanta().size()];

                                for (int i = 0; i < response.body().getDataConstanta().size(); i++) {
                                    mIdConstanta[i] = response.body().getDataConstanta().get(i).getConstantaID();
                                    mNamaConstanta[i] = response.body().getDataConstanta().get(i).getName();
                                }

                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseConstanta> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    public void mCreateDayOff() {

        RequestBody mBodyConstanta = RequestBody.create(MediaType.parse("text/plain"), constanta.toString());
        RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"),
                PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_employeeid), ""));
        RequestBody HowManyDays = RequestBody.create(MediaType.parse("text/plain"), mHowManyDays.toString());
        RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), mStartDate);
        RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));
        MultipartBody.Part multipart = null;

        //File creating from selected URL
        if (returnUri!=null) {
            File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
            multipart = null;
            if (file.exists()) {
                String name = URLConnection.guessContentTypeFromName(file.getName());
                RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
                multipart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
            }
        }

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(15, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseCreateCuti> call = api.mCreateDayOffS(Authorization, mBodyConstanta, EmployeeID, HowManyDays, Date, Note, multipart);

        call.enqueue(new Callback<ResponseCreateCuti>() {
            @Override
            public void onResponse(Call<ResponseCreateCuti> call, Response<ResponseCreateCuti> response) {

                Log.v(TAG, response.toString());

                if (response.isSuccessful()) {
                    int statusCode = response.body().getErrCode();

                    if (statusCode == 0) {

                        dismiss();
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.dayoff_success), Toast.LENGTH_SHORT).show();

                    } else {

                        Toast.makeText(getActivity(), response.body().getErrMessage(), Toast.LENGTH_SHORT).show();

                    }

                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<ResponseCreateCuti> call, Throwable t) {
                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void mAttemptCreateDayOff() {

        mEdtMulai.setError(null);
        mEdtSelesai.setError(null);
        mKeperluan.setError(null);
        mEdtNote.setError(null);


        String start = mEdtMulai.getText().toString();
        String end = mEdtSelesai.getText().toString();
        String reason = mKeperluan.getText().toString();
        String note = mEdtNote.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(start)) {
            mEdtMulai.setError(getString(R.string.start_date_blank));
            focusView = mEdtMulai;
            cancel = true;
        }

        if (TextUtils.isEmpty(end)) {
            mEdtSelesai.setError(getString(R.string.end_date_blank));
            focusView = mEdtSelesai;
            cancel = true;
        }

        if (TextUtils.isEmpty(reason)) {
            mKeperluan.setError(getString(R.string.reason_blank));
            focusView = mKeperluan;
            cancel = true;
        }

        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getString(R.string.note_blank));
            focusView = mEdtNote;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            mCreateDayOff();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mKeperluan) {
            try {
                if (mIdConstanta.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mKeperluan.setText(mNamaConstanta[item]);

                            constanta = mIdConstanta[item];

                            imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        } else if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        }

    }


}