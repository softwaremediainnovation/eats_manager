package app.direksi.hras.fragment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ListHolidayActivity;
import app.direksi.hras.LokasiKerjaActivity;
import app.direksi.hras.MutasiActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.EmployeeInfoData;
import app.direksi.hras.model.ResponseDataDetailEmployeeInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 19/06/2019.
 */

public class OrganizationFragment extends Fragment /*implements PenawaranDetailActivity.DataUpdateListener*/ {


    private EmployeeInfoData dataListAll = new EmployeeInfoData();
    private List<String> existLoad = new ArrayList<>();
    private TextView textNoInternet;
    private ProgressBar loading;
    private SwipeRefreshLayout swiperefresh;
    private TextView txtOrg , txtCompany;
    private ImageView mIconStatus3;
    private ScrollView sv1;
    private Button mButtonHoliday, mButtonMutasi, mButtonLokasi;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.organization_fragment, container, false);

        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        txtOrg = (TextView) v.findViewById(R.id.txtOrg);
        txtCompany = (TextView) v.findViewById(R.id.txtCompany);
        mIconStatus3 = (ImageView) v.findViewById(R.id.mIconStatus3);
        sv1 = (ScrollView) v.findViewById(R.id.sv1);
        sv1.setVisibility(View.GONE);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        mButtonHoliday = (Button) v.findViewById(R.id.mButtonHoliday);
        mButtonHoliday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ListHolidayActivity.class);
                startActivity(intent);

            }
        });

        mButtonMutasi = (Button) v.findViewById(R.id.mButtonMutasi);
        mButtonMutasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MutasiActivity.class);
                startActivity(intent);

            }
        });

        mButtonLokasi = (Button) v.findViewById(R.id.mButtonLokasi);
        mButtonLokasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LokasiKerjaActivity.class);
                startActivity(intent);

            }
        });

        String idSelf = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_employeeid), ""));

        String idView = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_employeeid_view), ""));

        if (idSelf.equals(idView)){
            mButtonLokasi.setVisibility(View.GONE);
        }

        //  swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
    }

    private void initRecyclerView() {

       /* swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                dataList = new ArrayList<>();
                cAdapter = new EmployeeAttendanceAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
              //  RefreshService();

            }});*/
        fetchService();

    }

    public static Fragment newInstance() {
        return new OrganizationFragment();
    }



    public void fetchService() {

        if ( existLoad.size() > 0)
        {
           /* if (dataList.size() > 0) {*/
                sv1.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.GONE);

                if (dataListAll.getMembership() == null){

                    sv1.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                    textNoInternet.setVisibility(View.VISIBLE);

                } else {
                    if (dataListAll.getMembership().size() > 0) {
                        txtOrg.setText(dataListAll.getMembership().get(0).getOrganization());
                        txtCompany.setText(dataListAll.getMembership().get(0).getCompany());
                        if (dataListAll.getMembership().get(0).getIsLeader() != true) {
                            mIconStatus3.setBackgroundResource(R.drawable.status_reject);
                        } else {
                            mIconStatus3.setBackgroundResource(R.drawable.status_accept);
                        }
                    }
                }
            /*}
            else
            {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.VISIBLE);
            }*/
        }
        else {

            textNoInternet.setVisibility(View.GONE);
            try {
                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_token), ""));

                Map<String, String> data = new HashMap<>();
                data.put("employeeId", (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_employeeid_view), "")));

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseDataDetailEmployeeInfo> call = api.getEmployeeInfo( Authorization, data);

                call.enqueue(new Callback<ResponseDataDetailEmployeeInfo>() {
                    @Override
                    public void onResponse(Call<ResponseDataDetailEmployeeInfo> call, Response<ResponseDataDetailEmployeeInfo> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                        if (response.isSuccessful()) {

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                            if (response.body().getErrCode() == 0) {
                                if (response.body().getData().getMembership().size() > 0) {
                                    //dataListAll = new EmployeeInfoData();
                                    dataListAll = response.body().getData();
                                    sv1.setVisibility(View.VISIBLE);
                                    txtOrg.setText(dataListAll.getMembership().get(0).getOrganization());
                                    txtCompany.setText(dataListAll.getMembership().get(0).getCompany());
                                    if (response.body().getData().getMembership().get(0).getIsLeader() != true){
                                        mIconStatus3.setBackgroundResource(R.drawable.status_reject);
                                    } else {
                                        mIconStatus3.setBackgroundResource(R.drawable.status_accept);
                                    }

                                    existLoad.add("Reload");
                                } else {
                                    existLoad.add("Reload");
                                    textNoInternet.setVisibility(View.VISIBLE);
                                }
                                loading.setVisibility(View.GONE);
                            } else {
                                loading.setVisibility(View.GONE);
                            }

                            loading.setVisibility(View.GONE);


                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            Log.v("DoneFragment", "tidak sukses");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataDetailEmployeeInfo> call, Throwable t) {
                        Log.v("DoneFragment", "on failure");
                        loading.setVisibility(View.GONE);
                        // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                    }
                });
            } catch (Exception e) {
                Log.v("DoneFragment", "masuk catch");
                if (isAdded())
                    loading.setVisibility(View.GONE);
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
               // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

            }
        }


    }






}


