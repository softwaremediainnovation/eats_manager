package app.direksi.hras.fragment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.DetailAttendancesActivity;
import app.direksi.hras.LateActivity;
import app.direksi.hras.R;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.adapter.AttendanceLateAdapter;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.ResponseDataAttendance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 05/09/2019.
 */

public class NotLateAttendanceFragment extends Fragment  /*implements PenawaranDetailActivity.DataUpdateListener*/ {
    private RecyclerView mRootView;
    private List<DataAttendance> dataList = new ArrayList<>();
    private List<String> existLoad = new ArrayList<>();
    private AttendanceLateAdapter cAdapter;
    private TextView textNoInternet;
    private ProgressBar loading;
    com.google.android.material.floatingactionbutton.FloatingActionButton fab;
    private SwipeRefreshLayout swiperefresh;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.all_list, container, false);
        mRootView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);

        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
       /* ((AttendanceActivity)getActivity()).setFragmentRefreshListener(new AttendanceActivity.FragmentRefreshListener2() {
            @Override
            public void onRefresh() {

                // pageNumber = 1;
                dataList = new ArrayList<>();
                cAdapter = new AttendanceAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
                fetchService();
            }
        });*/
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
    }

    private void initRecyclerView() {
        cAdapter = new AttendanceLateAdapter(dataList, getActivity());
        mRootView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRootView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRootView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        mRootView.setAdapter(cAdapter);
        mRootView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRootView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                DataAttendance data = dataList.get(position);
                PreferenceManager.getDefaultSharedPreferences(getContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(getContext(), DetailAttendancesActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();


                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                String approval = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_CanViewAttendance), ""));

                if (approval.equals("true")) {

                    ((LateActivity) getActivity()).updateCount("getCount");

                    dataList = new ArrayList<>();
                    cAdapter = new AttendanceLateAdapter(dataList, getActivity());
                    mRootView.setAdapter(cAdapter);
                    swiperefresh.setRefreshing(false);
                    fetchService();
                } else {
                    swiperefresh.setRefreshing(false);
                    textNoInternet.setVisibility(View.VISIBLE);
                    textNoInternet.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
                    loading.setVisibility(View.GONE);

                }

            }});
        String approval = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_CanViewAttendance), ""));
        if (approval.equals("true")) {

            fetchService();


        } else {
            textNoInternet.setVisibility(View.VISIBLE);
            textNoInternet.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
            loading.setVisibility(View.GONE);

        }
       // fetchService();

    }

    public static Fragment newInstance() {
        return new NotLateAttendanceFragment();
    }


    private static class FakePageAdapter extends RecyclerView.Adapter<NotLateAttendanceFragment.FakePageVH> {
        private final int numItems;

        FakePageAdapter(int numItems) {
            this.numItems = numItems;
        }

        @Override
        public NotLateAttendanceFragment.FakePageVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_relative_employe, viewGroup, false);

            return new NotLateAttendanceFragment.FakePageVH(itemView);
        }

        @Override
        public void onBindViewHolder(NotLateAttendanceFragment.FakePageVH fakePageVH, int i) {
            // do nothing
        }

        @Override
        public int getItemCount() {
            return numItems;
        }
    }

    private static class FakePageVH extends RecyclerView.ViewHolder {
        FakePageVH(View itemView) {
            super(itemView);
        }
    }




  /*  @Override
    public void onDataUpdate(List<PenawaranDetail> mData) {

        if (mData.get(0).getPnwrAdv().size() > 0 )
        {
            klienList.clear();
            klienList.addAll(mData.get(0).getPnwrAdv());
            cAdapter.notifyDataSetChanged();
            Log.v("AdvFragment", "aaaaaaaaaaa");
        }
        // put your UI update logic here
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((PenawaranDetailActivity) activity).registerDataUpdateListener(this);
    }*/

    public void fetchService() {
        loading.setVisibility(View.VISIBLE);

      /*  if (dataList.size() > 0 || existLoad.size() > 0)
        {
            if (dataList.size() > 0) {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.GONE);
            }
            else
            {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.VISIBLE);
            }
        }
        else {*/

        textNoInternet.setVisibility(View.GONE);
        try {




            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));
            String idEmployee = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));



            Map<String, String> data = new HashMap<>();
            data.put("employeeID", idEmployee);

            if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty()) {
                data.put("dateFilter", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));
            } else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = sdf.format(c.getTime());
                data.put("dateFilter", strDate);

            }

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataAttendance> call = api.getAttendanceTidakTelatHariIni( Authorization, data);

            call.enqueue(new Callback<ResponseDataAttendance>() {
                @Override
                public void onResponse(Call<ResponseDataAttendance> call, Response<ResponseDataAttendance> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                    if (response.isSuccessful()) {

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                        if (response.body().getErrCode() == 0) {
                            if (response.body().getData().size() > 0) {
                                //  dataList.clear();
                                existLoad.add("Reload");
                                dataList.addAll(response.body().getData());
                                cAdapter.notifyDataSetChanged();
                            } else {
                                existLoad.add("Reload");
                                textNoInternet.setVisibility(View.VISIBLE);
                            }
                            loading.setVisibility(View.GONE);
                        } else {
                            loading.setVisibility(View.GONE);
                        }

                        loading.setVisibility(View.GONE);


                    } else {
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                        loading.setVisibility(View.GONE);
                        Log.v("DoneFragment", "tidak sukses");
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataAttendance> call, Throwable t) {
                    Log.v("DoneFragment", "on failure");
                    loading.setVisibility(View.GONE);
                    // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();

                       // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e)
                    {

                    }
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                }
            });
        } catch (Exception e) {
            Log.v("DoneFragment", "masuk catch");
            if (isAdded())
                loading.setVisibility(View.GONE);
            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

        }
        /*}*/


    }




}

