package app.direksi.hras.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import static android.app.Activity.RESULT_OK;
import android.location.Geocoder;

import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.AttendanceActivity;
import app.direksi.hras.CheckinActivity;
import app.direksi.hras.HomeActivity;
import app.direksi.hras.InOutActivity;
import app.direksi.hras.R;
import app.direksi.hras.app.AppConstants;
import app.direksi.hras.model.ResponseCheckIn;
import app.direksi.hras.model.ResponseDataCheckAbsenIN;
import app.direksi.hras.model.ResponseDataEmployeeID;
import app.direksi.hras.model.ResponseDataEmployeeResign;
import app.direksi.hras.model.ResponseGetServerTimePost;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.BitmapImage;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.GetLocation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.ChooserType;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.MediaFile;
import pl.aprilapps.easyphotopicker.MediaSource;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static app.direksi.hras.app.AppConstants.AllowMock;
import static app.direksi.hras.app.AppConstants.LogoutPermission;
import static app.direksi.hras.app.AppConstants.RANGE_DISTANCE_BOUNDS;
import static app.direksi.hras.app.AppConstants.RANGE_DISTANCE_NULL;
import static app.direksi.hras.util.DefaultFormatter.isNegative;
import static app.direksi.hras.util.Validation.replaceNull;


public class CheckInFragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    private static final String TAG = CheckInFragment.class.getSimpleName();

    private GpsLocationReceiver gpsLocationReceiver;
    private NetworkChangeReceiver networkChangeReceiver;

    private Location myLocation;
    private SweetAlertDialog mAlertDialog, mAlertInternet;

    private DateFormat sdf, mViewSdf;

    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    private static final String[] PERMISSION_FINGERPRINT = new String[]{
            Manifest.permission.USE_FINGERPRINT
    };
    private PermissionsChecker checker;
    private boolean mIsActiveGPS = false;
    private boolean mIsActiveInternet = true;

    private LocationManager manager;
    private RelativeLayout mLayoutOut, mLayoutIn;
    private SimpleDateFormat dateFormat;

    private Chronometer cronometer;
    private TextView mTxtTotalLate, txtDates;
    private LayoutInflater mInflater;
    private SupportMapFragment mapFragment;
    private View mapView;
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private double latitude = 0, longitude = 0, mLatitude = 0, mLongitude = 0;
    private GoogleMap mMap;
    private LatLngBounds latLngBounds = null;
    private Date mTimeServer = new Date();
    private boolean isSupportBiometric = false;
    private Intent mIntent;
    private float mDistande = 0;
    private TextView txtgmt;
    private boolean isLoad = false;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private static final String[] PERMISSIONS_READ_STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String deviceModel;
    private Bitmap rotatedBitmap;
    private Uri mCropImageUri = null;
    private SweetAlertDialog loading;
    private String mAddress = "";
    private String mCurrentPhotoPath;
    EasyImage easyImage;
    private File myFile = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.v(TAG, "mGetDetailEmp");

        checker = new PermissionsChecker(getActivity());

        if (checker.lacksPermissions(PERMISSIONS)) {
            startPermissionsActivity(PERMISSIONS);
        }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup viewGroup = (ViewGroup) view;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.checkin_fragment_layout, null);

        Log.v(TAG, "mGetDetailEmp");

        mInflater = inflater;



       /* Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins.otf");
        SpannableStringBuilder SS = new SpannableStringBuilder(getResources().getString(R.string.empty));
        SS.setSpan(new HrsApp.CustomTypefaceSpan("", font2), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle(SS);*/

        // mTxtTotalLate = v.findViewById(R.id.mTxtTotalLate);
        txtDates = v.findViewById(R.id.txtDates);
        txtgmt = v.findViewById(R.id.txtgmt);
        mLayoutOut = v.findViewById(R.id.mLayoutOut);
        mLayoutIn = v.findViewById(R.id.mLayoutIn);
        mLayoutIn.setOnClickListener(this);
        mLayoutOut.setOnClickListener(this);


        cronometer = v.findViewById(R.id.cronometer);

        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        gpsLocationReceiver = new GpsLocationReceiver();
        IntentFilter intentFilter = new IntentFilter(getResources().getString(R.string.gps_update_filter));
        getActivity().registerReceiver(gpsLocationReceiver, intentFilter);


        networkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter intentNetworkFilter = new IntentFilter(getResources().getString(R.string.network_updater_filter));
        getActivity().registerReceiver(networkChangeReceiver, intentNetworkFilter);

        mAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_gps))
                .setContentText(getResources().getString(R.string.text_gps_nonactive))
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        mIsActiveGPS = false;
                        mAlertDialog.dismiss();
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
        mAlertDialog.setCancelable(false);

        mAlertInternet = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_internet))
                .setContentText(getResources().getString(R.string.text_internet_nonactive))
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        try {
                            mIsActiveInternet = true;
                            mAlertInternet.dismiss();
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.android.phone", "com.android.phone.MobileNetworkSettings"));
                            startActivity(intent);
                        } catch (Exception e) {
                        }
                    }
                });

        mAlertInternet.setCancelable(false);


        Log.v(TAG, "mGetDetailEmp");


        if (checkActiveInternet())
            mAlertInternet.show();


        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        easyImage = new EasyImage.Builder(getActivity())
                .setChooserTitle("Pick media")
                .setCopyImagesToPublicGalleryFolder(false)
                //.setChooserType(ChooserType.CAMERA_AND_DOCUMENTS)
                .setChooserType(ChooserType.CAMERA_AND_GALLERY)
                .setFolderName("EasyImage sample")
                .allowMultiple(true)
                .build();

        mLocationCallback = new LocationCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        };

        mGetActiveUser();
        mGetDetailEmp();
        //mGetServerTim();


        if (checkBiometricSupport()) {
            isSupportBiometric = true;
        } else {

            isSupportBiometric = false;

        }

        if (isSupportBiometric) {
            if (checker.lacksPermissions(PERMISSIONS)) {
                startPermissionsActivity(PERMISSIONS);
            }
        }

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle(getActivity().getResources().getString(R.string.txt_biometric_verification))
                .setSubtitle(getActivity().getResources().getString(R.string.txt_mesage_presence_biometric))
                .setNegativeButtonText(getActivity().getResources().getString(R.string.title_batal_small))
                .setConfirmationRequired(false)
                .build();

        executor = ContextCompat.getMainExecutor(getContext());
        biometricPrompt = new BiometricPrompt(getActivity(),
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);

                if (errString.equals(getActivity().getResources().getString(R.string.title_batal_small))){

                } else {

                    MDToast.makeText(getActivity(),  errString.toString(),
                            MDToast.LENGTH_SHORT, MDToast.TYPE_INFO).show();

                }

            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                /*Toast.makeText(getContext(),
                        "Authentication succeeded!", Toast.LENGTH_SHORT).show();*/
                mIntent = new Intent(getActivity(), CheckinActivity.class);
                startActivity(mIntent);



            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                MDToast.makeText(getActivity(), getActivity().getResources().getString(R.string.txt_failed_verification),
                        MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();
            }
        });

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        return v;
    }

    private Boolean checkBiometricSupport(){

        Boolean hasil = false;
        BiometricManager biometricManager = BiometricManager.from(getActivity());
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                hasil = true;
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                hasil = false;
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                hasil = false;
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                hasil = true;
                Log.e("MY_APP_TAG", "BIOMETRIC_ERROR_NONE_ENROLLED");
                // Prompts the user to create credentials that your app accepts.
                break;
        }

        return hasil;
    }



    @Override
    public void onResume() {
        super.onResume();
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !mAlertDialog.isShowing()) {
            mAlertDialog.show();
        } else {
            getMaps();
        }
    }

    public void mGetDetailEmp() {

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(300, TimeUnit.SECONDS)
                    .readTimeout(300, TimeUnit.SECONDS)
                    .writeTimeout(300, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployeeID> call = api.getEmployeeID(Authorization, PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));


            call.enqueue(new Callback<ResponseDataEmployeeID>() {
                @Override
                public void onResponse(Call<ResponseDataEmployeeID> call, Response<ResponseDataEmployeeID> response) {
                    try {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0 && response.body().getData().getOrganization() == null) {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.no_organization), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.loading_error), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseDataEmployeeID> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.loading_error), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.loading_error), 1);
        }


    }

    void getMaps() {

        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Bundle extras = null;
                            if (location != null)
                                extras = location.getExtras();
                            boolean isMockLocation = extras != null && extras.getBoolean(FusedLocationProviderApi.KEY_MOCK_LOCATION, false);

                            if (isMockLocation) {

                                if (AllowMock){

                                } else {

                                    mShowDialog(getResources().getString(R.string.title_matikan_mock_lokasi), 2);
                                    return;
                                }

                            }

                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();

                                mLoadMaps();


                            } else {
                                getLastLocation();
                            }


                        }


                    });

            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getLastLocation();
                    Log.e(TAG, e.getMessage());
                }
            });


            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getLastLocation();
                    Log.e(TAG, e.getMessage());
                }
            });

            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    getLastLocation();
                    Log.e(TAG, e.getMessage());
                }
            });


        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void getLastLocation() {
        try {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    mLoadMaps();

                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null);
        } catch (Exception e) {
        }

    }


    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try {
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.my_json_maps));
            if (!isSuccess) {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }

        double lat, longi;

        if (!PreferenceManager.getDefaultSharedPreferences(getActivity())
                .getString(getResources().getString(R.string.pref_longitude), "").isEmpty() &&
                !PreferenceManager.getDefaultSharedPreferences(getActivity())
                        .getString(getResources().getString(R.string.pref_longitude), "").isEmpty()) {

            lat = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_latitude), "0"));

            longi = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_longitude), "0"));

        } else {
            lat = -7.265465;
            longi = 112.745543;
        }


        mMap = map;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            float zoomLevel = 16.0f; //This goes up to 21

            mMap.getUiSettings().setMapToolbarEnabled(true);
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            LatLng currentLatLng = new LatLng(lat,
                    longi);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, zoomLevel));
        }

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 700);
        }


        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                //TODO: Any custom actions

                getMaps();

                //Toast.makeText(getActivity(), "Update lokasi berhasil", Toast.LENGTH_SHORT).show();
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_berhasil_update_lokasi), MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();
                return false;
            }
        });


    }


    public void mLoadMaps() {

        try {

            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {

                    try {

                        int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                getResources().getString(R.string.pref_radius), ""));

                        mMap.clear();

                        LatLng locationA = null;


                        int height = 134;
                        int width = 75;
                        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_kantor);
                        Bitmap b = bitmapdraw.getBitmap();
                        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

                        BitmapDrawable bitmapUser = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_user);
                        Bitmap bUser = bitmapUser.getBitmap();
                        Bitmap smallMarkeruser = Bitmap.createScaledBitmap(bUser, width, height, false);

                        if (!PreferenceManager.getDefaultSharedPreferences(getActivity())
                                .getString(getResources().getString(R.string.pref_longitude), "").isEmpty() &&
                                !PreferenceManager.getDefaultSharedPreferences(getActivity())
                                        .getString(getResources().getString(R.string.pref_longitude), "").isEmpty()) {

                            mLatitude = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                    getResources().getString(R.string.pref_latitude), "0"));

                            mLongitude = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                    getResources().getString(R.string.pref_longitude), "0"));

          /*  mLatitude = -7.2634205;
            mLongitude = 112.738152;*/

                            mMap.addMarker(new MarkerOptions().position(new LatLng(mLatitude, mLongitude)).title("Lokasi Kantor").icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                            locationA = new LatLng(mLatitude, mLongitude);

                            LatLng locationB = new LatLng(latitude, longitude);

                            latLngBounds = new LatLngBounds.Builder()
                                    .include(locationA)
                                    .include(locationB)
                                    .build();
                        } else {
                            mLatitude = latitude;
                            mLongitude = longitude;
                        }


                        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Lokasiku").icon(BitmapDescriptorFactory.fromBitmap(smallMarkeruser)));

                        mGetServerTim(latitude, longitude);

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLatitude, mLongitude), 10));
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(mLatitude, mLongitude))      // Sets the center of the map to location user
                                .zoom(19)                   // Sets the zoom
                                .bearing(0)                // Sets the orientation of the camera to east
                                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                                .build();                   // Creates a CameraPosition from the builder
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                        if (!isCloseRange() && mLatitude != 0 && mLongitude != 0)
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 100), 1000, null);


                        Circle circle = mMap.addCircle(new CircleOptions()
                                .center(new LatLng(mLatitude, mLongitude))
                                .radius(radius)
                                .strokeColor(Color.parseColor("#20ff5252"))
                                .strokeWidth(3)
                                .fillColor(Color.parseColor("#50ff5252")));
                    } catch (Exception e) {
                    }
                }
            });
        } catch (Exception e) {

        }

    }

    public boolean isCloseRange() {

        boolean isCloseRange = false;

        Location locationA = new Location("");

        locationA.setLatitude(latitude);
        locationA.setLongitude(longitude);


        Location locationB = new Location("");

        locationB.setLatitude(mLatitude);
        locationB.setLongitude(mLongitude);

        float distancse = locationA.distanceTo(locationB);

        if (distancse < RANGE_DISTANCE_BOUNDS) {
            isCloseRange = true;
        }

        return isCloseRange;
    }


    public boolean checkActiveInternet() {

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            return false;
        } else {
            return true;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_finger), "false")
                .apply();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.mLayoutIn) {

            mCheckRadius(true);

        } else if (view.getId() == R.id.mLayoutOut) {
            mCheckRadius(false);

        }
    }

    private void mCheckAlreadyOut() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Log.v(TAG, Authorization);

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataCheckAbsenIN> call = api.getDataCheckAbsenOUT(Authorization);


            call.enqueue(new Callback<ResponseDataCheckAbsenIN>() {


                @Override
                public void onResponse(Call<ResponseDataCheckAbsenIN> call, Response<ResponseDataCheckAbsenIN> response) {

                    try {


                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().getStatus().equals(getResources().getString(R.string.response_success))) {


                                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                        edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_out))
                                        .putBoolean(getResources().getString(R.string.pref_out_date), false)
                                        .apply();


                                String absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                        getResources().getString(R.string.pref_absen_method), ""));

                                if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                                    if (isSupportBiometric) {

                                        biometricPrompt.authenticate(promptInfo);
                                    }


                                    else{
                                        mIntent = new Intent(getActivity(), CheckinActivity.class);
                                        startActivity(mIntent);
                                    }

                                } else if (absenMethod.equals("foto") || absenMethod.equals("photo")){

                                    if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                                        startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                                    } else {
                                        // startCropImageActivity();
                                       // mOpenCamera();
                                        easyImage.openCameraForImage(CheckInFragment.this);
                                    }

                                } else if (absenMethod.equals("kosong")){

                                    mIntent = new Intent(getActivity(), CheckinActivity.class);
                                    startActivity(mIntent);

                                } else {

                                    MDToast.makeText(getActivity(),  getActivity().getResources().getString(R.string.title_absen_tidak_dikenali) + " ( " + absenMethod + " ) ",
                                            MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();

                                }



                            } else {

                                /*if (response.body().getData().getResult().equals(getResources().getString(R.string.failed_already_out))) {*/
                                if (response.body().getData().getResult().equals("Anda sudah Absen OUT hari ini!")) {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                    /*} else if (response.body().getData().getResult().equals(getResources().getString(R.string.failed_not_yet_out))) {*/
                                } else if (response.body().getData().getResult().equals("Anda belum Absen OUT kemarin!")) {

                                    if (isAdded())
                                        mShowDialogNotYetOut();

                                    /*} else if (response.body().getData().getResult().equals(getResources().getString(R.string.failed_not_yet_in))) {*/
                                } else if (response.body().getData().getResult().equals("Anda belum Absen IN hari ini!")) {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                } else {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);
                                }

                            }


                        } else {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseDataCheckAbsenIN> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
        }

    }


    private void mCheckAlreadyIn() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataCheckAbsenIN> call = api.getDataCheckAbsenIN(Authorization);


            call.enqueue(new Callback<ResponseDataCheckAbsenIN>() {


                @Override
                public void onResponse(Call<ResponseDataCheckAbsenIN> call, Response<ResponseDataCheckAbsenIN> response) {

                    try {

                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().getStatus().equals(getResources().getString(R.string.response_success))) {


                                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                        edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_in))
                                        .putBoolean(getResources().getString(R.string.pref_out_date), false)
                                        .apply();

                                String absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                        getResources().getString(R.string.pref_absen_method), ""));

                                if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                                    if (isSupportBiometric) {

                                        biometricPrompt.authenticate(promptInfo);
                                    }


                                    else{
                                        mIntent = new Intent(getActivity(), CheckinActivity.class);
                                        startActivity(mIntent);
                                    }

                                } else if (absenMethod.equals("foto") || absenMethod.equals("photo")){

                                    if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                                        startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                                    } else {
                                        // startCropImageActivity();
                                       // mOpenCamera();
                                        easyImage.openCameraForImage(CheckInFragment.this);
                                    }

                                } else if (absenMethod.equals("kosong")){

                                    mIntent = new Intent(getActivity(), CheckinActivity.class);
                                    startActivity(mIntent);

                                } else {

                                    MDToast.makeText(getActivity(),  getActivity().getResources().getString(R.string.title_absen_tidak_dikenali) + " ( " + absenMethod + " ) ",
                                            MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();

                                }


                            } else {

                                /*if (response.body().getData().getResult().equals(getResources().getString(R.string.failed_already_in))) {*/
                                if (response.body().getData().getResult().equals("Anda sudah Absen IN hari ini!")) {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);

                                    /*} else if (response.body().getData().getResult().equals(getResources().getString(R.string.failed_not_yet_out))) {*/
                                } else if (response.body().getData().getResult().equals("Anda belum Absen OUT kemarin!")) {

                                    if (isAdded())
                                        mShowDialogNotYetOut();

                                } else {

                                    if (isAdded())
                                        mShowDialog(response.body().getData().getResult(), 4);
                                }

                            }


                        } else {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseDataCheckAbsenIN> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.failed_checkin), 1);
        }

    }


    private final class GpsLocationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boolean isActiveGps = intent.getBooleanExtra("isActiveGps", false);

                if (!isActiveGps && !mAlertDialog.isShowing()) {
                    mAlertDialog.show();
                    mIsActiveGPS = false;
                    Log.e(TAG, mIsActiveGPS + "");
                } else {
                    mAlertDialog.dismiss();
                    mIsActiveGPS = true;
                    Log.e(TAG, mIsActiveGPS + "");
                }

            } catch (Exception e) {
            }
        }
    }

    private final class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boolean noInternet = intent.getBooleanExtra("no_internet", true);

                if (noInternet && !mAlertInternet.isShowing()) {
                    mAlertInternet.show();
                    mIsActiveInternet = false;
                } else {
                    mAlertInternet.dismiss();
                    mIsActiveInternet = true;

                }

            } catch (Exception e) {
            }
        }

    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(gpsLocationReceiver);
        getActivity().unregisterReceiver(networkChangeReceiver);
        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_finger), "false")
                .apply();
        super.onDestroy();
    }


    //request permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());
    }

    //start permission
    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }

    private void mShowDialog(String message, int type) {
        try {
            SweetAlertDialog mShowDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getResources().getString(R.string.string_fingerprint))
                    .setContentText(message)
                    .setConfirmText(getResources().getString(R.string.string_yes))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.cancel();
                            if (type == 0) {
                                startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                            } else if (type == 1) {
                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                startActivity(intent);
                                getActivity().finish();
                            } else if (type == 2) {

                                Intent intent = new Intent(getActivity(), HomeActivity.class);
                                startActivity(intent);
                                getActivity().finish();

                                startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));

                            } else if (type == 3) {

                                Intent intent = getActivity().getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                                intent.setAction(Intent.ACTION_VIEW);
                                startActivity(intent);

                            }

                        }
                    });

            if (type == 4) {
                mShowDialog.setCanceledOnTouchOutside(true);
                mShowDialog.setCancelable(true);
            } else {
                mShowDialog.setCancelable(false);
            }

            mShowDialog.show();
        } catch (Exception e) {
        }
    }


    private void mShowDialogNotYetOut() {
        try {
            SweetAlertDialog mShowDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getResources().getString(R.string.string_confirmation))
                    .setContentText(getResources().getString(R.string.confirmation_out_yesterday))
                    .setCancelText(getResources().getString(R.string.txt_in))
                    .setConfirmText(getResources().getString(R.string.txt_out))
                    .showCancelButton(true)
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                            sweetAlertDialog.cancel();

                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_out))
                                    .putBoolean(getResources().getString(R.string.pref_out_date), true)
                                    .apply();


                            String absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                    getResources().getString(R.string.pref_absen_method), ""));

                            if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                                if (isSupportBiometric) {

                                    biometricPrompt.authenticate(promptInfo);
                                }


                                else{
                                    mIntent = new Intent(getActivity(), CheckinActivity.class);
                                    startActivity(mIntent);
                                }

                            } else if (absenMethod.equals("foto")  || absenMethod.equals("photo")){

                                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                                } else {
                                    // startCropImageActivity();
                                   // mOpenCamera();
                                    easyImage.openCameraForImage(CheckInFragment.this);
                                }

                            } else if (absenMethod.equals("kosong")){

                                mIntent = new Intent(getActivity(), CheckinActivity.class);
                                startActivity(mIntent);

                            } else {

                                MDToast.makeText(getActivity(),  getActivity().getResources().getString(R.string.title_absen_tidak_dikenali) + " ( " + absenMethod + " ) ",
                                        MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();

                            }

                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            sweetAlertDialog.cancel();

                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_in_out), getResources().getString(R.string.txt_in))
                                    .putBoolean(getResources().getString(R.string.pref_out_date), false)
                                    .apply();

                            String absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                    getResources().getString(R.string.pref_absen_method), ""));

                            if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                                if (isSupportBiometric) {

                                    biometricPrompt.authenticate(promptInfo);
                                }


                                else{
                                    mIntent = new Intent(getActivity(), CheckinActivity.class);
                                    startActivity(mIntent);
                                }

                            } else if (absenMethod.equals("foto") || absenMethod.equals("photo")){

                                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                                } else {
                                    // startCropImageActivity();
                                    //mOpenCamera();
                                    easyImage.openCameraForImage(CheckInFragment.this);
                                }

                            } else if (absenMethod.equals("kosong")){

                                mIntent = new Intent(getActivity(), CheckinActivity.class);
                                startActivity(mIntent);

                            } else {

                                MDToast.makeText(getActivity(),  getActivity().getResources().getString(R.string.title_absen_tidak_dikenali) + " ( " + absenMethod + " ) ",
                                        MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();

                            }

                        }
                    });
            mShowDialog.setCanceledOnTouchOutside(true);
            mShowDialog.setCancelable(true);
            mShowDialog.show();


            Button btn = mShowDialog.findViewById(R.id.confirm_button);
            btn.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_red));

            Button red = mShowDialog.findViewById(R.id.cancel_button);
            red.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_gren));


        } catch (Exception e) {
        }
    }

    private void mGetServerTim( double lat, double longi) {
        try {



            RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));
            RequestBody Latitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(lat));
            RequestBody Longitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(longi));

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGetServerTimePost> call = api.getServerPost(EmployeeID, Latitude, Longitude, null);


            call.enqueue(new Callback<ResponseGetServerTimePost>() {


                @Override
                public void onResponse(Call<ResponseGetServerTimePost> call, Response<ResponseGetServerTimePost> response) {

                    try {

                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData().getDateNow());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");


                           /* Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins.otf");
                            SpannableStringBuilder SS = new SpannableStringBuilder(DefaultFormatter.dFIdNoHour(response.body().getData()));
                            SS.setSpan(new HrsApp.CustomTypefaceSpan("", font2), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                            ((HomeActivity) getActivity()).getSupportActionBar().setTitle(SS);*/
                            txtDates.setText(DefaultFormatter.changeFormatDateWithOutHourFull(response.body().getData().getDateNow()));
                            mTimeServer = date;
                            String isnegative = "+";
                            if (isNegative(response.body().getData().getGmt())){
                                isnegative = "";
                            }
                            txtgmt.setText("GMT" + isnegative + String.valueOf(response.body().getData().getGmt()));

                            PreferenceManager.getDefaultSharedPreferences(getContext()).
                                    edit().putString(getResources().getString(R.string.pref_gmt), String.valueOf(response.body().getData().getGmt()))
                                    .apply();

                            isLoad = true;
                            Log.v(TAG, mTimeServer.toString() + "");


                            timer(mTimeServer);

                        } else {
                            if (isAdded())
                                mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
                        }

                    } catch (Exception e) {
                        if (isAdded())
                            mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
                    }

                }

                @Override
                public void onFailure(Call<ResponseGetServerTimePost> call, Throwable t) {
                    if (isAdded())
                        mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
                }
            });
        } catch (Exception e) {
            if (isAdded())
                mShowDialog(getActivity().getResources().getString(R.string.failed_time_server), 1);
        }

    }

    private void mCheckRadius(boolean isIn) {

        if (isLoad){

            if (insideRadius()) {

                if (isIn)
                    mCheckAlreadyIn();
                else
                    mCheckAlreadyOut();

            } else {

                String mTextDistance = "";

                if (latitude == 0 && longitude == 0) {

                    mShowDialog(getResources().getString(R.string.title_belum_dapat_lokasi), 1);

                } else {

                    if (mDistande < 1000) {
                        mTextDistance = DefaultFormatter.formatDefaultCurrencyRupiah(Math.round(mDistande)) + " m ";
                    } else {
                        mTextDistance = DefaultFormatter.formatDefaultCurrencyRupiah(Math.round((mDistande / 1000))) + " km ";
                    }


                    mShowDialog(getResources().getString(R.string.title_jarak_telalu_jauh_dari_kantor) +  "." +
                            getResources().getString(R.string.title_saat_ini_anda_berjarak) +  " " + mTextDistance + getResources().getString(R.string.title_dari_kantor) , 3);

                }

            }
        } else {

            mShowDialog(getResources().getString(R.string.title_belum_mendapat_jam_server), 1);
        }

    }

    public boolean insideRadius() {

        boolean isInside = false;

        int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_radius), ""));

        myLocation = GetLocation.getLastKnownLocation(getActivity());

        if (latitude == 0 && longitude == 0) {
            try {
                latitude = myLocation.getLatitude();
                longitude = myLocation.getLongitude();
            } catch (Exception e) {
            }
        }

        Location locationA = new Location("");

        locationA.setLatitude(latitude);
        locationA.setLongitude(longitude);


        if ( (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_latitude), "")).equals("") &&
                (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_longitude), "")).equals("")){

            isInside = true;

        } else {

            Location locationB = new Location("");

            double myLatitude = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_latitude), "0"));

            double myLongitude = Double.parseDouble(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_longitude), "0"));

            locationB.setLatitude(myLatitude);
            locationB.setLongitude(myLongitude);

            float distancse = locationA.distanceTo(locationB);
            mDistande = distancse;

            Log.e(TAG, "Lokasi saat ini = " + latitude + " --- " + longitude);
            Log.e(TAG, "Lokasi kantor satt ini = " + myLatitude + " --- " + myLongitude);
            Log.e(TAG, "Jarak " + distancse + "");

            if (distancse < radius) {
                isInside = true;

            }

        }

        return isInside;
    }


    public void timer(Date dat) {

        int second = dat.getSeconds();
        int minute = dat.getMinutes();
        int hour = dat.getHours();
        int dates = dat.getDate();
        int month = dat.getMonth();
        int year = dat.getYear();

        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit().putString(getResources().getString(R.string.pref_timer), Long.toString(hour))
                .apply();


        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dates, hour, minute, second);

        long hours = calendar.get(Calendar.HOUR);
        long minutes = calendar.get(Calendar.MINUTE);
        long seconds = calendar.get(Calendar.SECOND);

        cronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;

                long a = Long.parseLong(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_timer), "0"));

                if (m == 59 && s == 59) {

                    if (a == 23) {
                        a = 0;
                        // mGetServerTim();
                        mGetServerTim(latitude, longitude);
                    } else {
                        a++;
                    }

                    PreferenceManager.getDefaultSharedPreferences(getActivity()).
                            edit().putString(getResources().getString(R.string.pref_timer), Long.toString(a))
                            .apply();
                }

                String hh = a < 10 ? "0" + a : a + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                cArg.setText(hh + ":" + mm + ":" + ss);
            }
        });
        cronometer.setBase(SystemClock.elapsedRealtime() - (hours * 3600000 + minutes * 60000 + seconds * 1000));
        cronometer.start();

    }

    public void mGetActiveUser() {

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Map<String, String> data = new HashMap<>();

            data.put("employeeid", PreferenceManager.getDefaultSharedPreferences(getActivity())
                    .getString(getResources().getString(R.string.pref_employeeid), ""));


            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployeeResign> call = api.getEmployeeResign(Authorization, data);

            call.enqueue(new Callback<ResponseDataEmployeeResign>() {
                @Override
                public void onResponse(Call<ResponseDataEmployeeResign> call, Response<ResponseDataEmployeeResign> response) {
                    try {
                        if (response.isSuccessful()) {

                            if (response.body().getData().getIsActive())
                                //save data profile to sharepreferences
                                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                        edit().putString(getResources().getString(R.string.pref_latitude), replaceNull(response.body().getData().getLatitude()))
                                        .putString(getResources().getString(R.string.pref_longitude), replaceNull(response.body().getData().getLongitude()))
                                        .putString(getResources().getString(R.string.pref_radius), replaceNull(response.body().getData().getDistance() != null ? response.body().getData().getDistance() : RANGE_DISTANCE_NULL))
                                        .putString(getResources().getString(R.string.pref_absen_method), response.body().getData().getAttendanceMethod() != null ? response.body().getData().getAttendanceMethod().toLowerCase() : "biometric")
                                        .apply();

                        }

                    } catch (Exception e) {

                    }
                }

                @Override
                public void onFailure(Call<ResponseDataEmployeeResign> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    private void mOpenCamera() {
        final String appPackageName = "." + getContext().getPackageName();
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);



        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            Log.v("cropPhoto", "if");
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 100);
        } else {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED) {
                Log.v("cropPhoto", "if");
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 100);
            } else {

                File photoFile = null;
                photoFile = createImageFile();


                if (photoFile != null) {
                 /*   cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,  FileProvider.getUriForFile(getApplicationContext(), "com.mii.pms.provider", photoFile));
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);*/


                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                            getContext().getPackageName() + appPackageName,
                            photoFile);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                    //COMPATIBILITY
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } else {
                        List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(cameraIntent, PackageManager.MATCH_DEFAULT_ONLY);
                        for (ResolveInfo resolveInfo : resInfoList) {
                            String packageName = resolveInfo.activityInfo.packageName;
                            getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }
                    }
                    //COMPATIBILITY
                    startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                }
            }
        }


    }

    /*@SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {


        try {

            if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

                Bitmap bm = null;
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inSampleSize = 3;
                AssetFileDescriptor fileDescriptor = null;
                try {
                    fileDescriptor = getContext().getContentResolver().openAssetFileDescriptor(Uri.parse(mCurrentPhotoPath), "r");
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        bm = BitmapFactory.decodeFileDescriptor(fileDescriptor.getFileDescriptor(), null, options);
                        fileDescriptor.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                Matrix matrix = new Matrix();

                matrix.postRotate(90);
                int width = bm.getWidth();
                int height = bm.getHeight();


                Bitmap scaledBitmap = Bitmap.createScaledBitmap(bm, width, height, true);
                deviceModel = Build.MANUFACTURER;
                if (deviceModel.equals("samsung")) {
                    rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                } else {
                    rotatedBitmap = scaledBitmap;
                }



                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                    mCropImageUri = BitmapImage.getImageUri(getContext(), rotatedBitmap);
                } else {
                    mCropImageUri = BitmapImage.getImageUri1(getContext(), rotatedBitmap);
                }


                //MDToast.makeText(getActivity(), mCropImageUri.toString(), MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();
                CheckIn();

            }

        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
            MDToast.makeText(getActivity(), e.toString(),
                    MDToast.LENGTH_LONG, MDToast.TYPE_INFO).show();
            Log.v("cropPhoto", e.toString());
        }

    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        easyImage.handleActivityResult(requestCode, resultCode, data, getActivity(), new DefaultCallback() {
            @Override
            public void onMediaFilesPicked(MediaFile[] imageFiles, MediaSource source) {
                for (MediaFile imageFile : imageFiles) {

                    myFile = imageFile.getFile();
                    CheckIn();



                }

            }

            @Override
            public void onImagePickerError(@NonNull Throwable error, @NonNull MediaSource source) {
                //Some error handling
                error.printStackTrace();
            }

            @Override
            public void onCanceled(@NonNull MediaSource source) {
                //Not necessary to remove any files manually anymore
            }
        });
    }

    private void CheckIn() {


        if(isAdded()) {
            loading.show();

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                mAddress = addresses.get(0).getAddressLine(0);

            } catch (IOException e) {
                e.printStackTrace();
            }


            try {

               // File file = new File(CompressImage.getRealPathFromURI(mCropImageUri, getActivity()));
                MultipartBody.Part multipart = null;
                if (myFile.exists()) {
                    String name = URLConnection.guessContentTypeFromName(myFile.getName());
                    RequestBody requestFile = null;
                    try {
                        requestFile = RequestBody.create(MediaType.parse(name), CompressImage.compressImage(myFile, getActivity()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    multipart = MultipartBody.Part.createFormData("File", myFile.getName(), requestFile);
                }

                RequestBody Address = RequestBody.create(MediaType.parse("text/plain"), mAddress);
                RequestBody Latitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude));
                RequestBody Longitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude));
                // PostCheckIN up = new PostCheckIN( String.valueOf(locc.getLatitude()), String.valueOf(locc.getLongitude()));
                RequestBody InOut = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_in_out), ""));
                RequestBody GMT = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_gmt), "0"));
                RequestBody IsYesterday = RequestBody.create(MediaType.parse("text/plain"),
                        Boolean.toString(PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                                getResources().getString(R.string.pref_out_date), false)));
                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_token), ""));

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                // simplified call to request the news with already initialized service
                Call<ResponseCheckIn> call;

                if (!PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
                        getResources().getString(R.string.pref_out_date), false))
                    call = api.mPostAttendancesInOutWorkingPatternPhoto(Authorization, Longitude, Latitude, InOut, Address,GMT, multipart);
                else
                    call = api.mPostAttendancesInOutDateWorkingPatternPhoto(Authorization, Longitude, Latitude, InOut, Address, IsYesterday,GMT, multipart);


                call.enqueue(new Callback<ResponseCheckIn>() {
                    @Override
                    public void onResponse(Call<ResponseCheckIn> call, Response<ResponseCheckIn> response) {

                        if (response.isSuccessful()) {

                            loading.dismiss();
                            if (response.body().getData().getStatus() == null) {

                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                // String mes = "ID : " + response.body().getData().getAttendanceID().toString() + "\n" + "Waktu : " + DefaultFormatter.changeFormatDateWithDayNameWithSecond(response.body().getData().getDateTime());
                                alertDialog.setContentText( getResources().getString(R.string.title_absen_berhasil_pada) + " " + DefaultFormatter.changeFormatDateWithDayName(response.body().getData().getDateTime()) + " " + getResources().getString(R.string.title_dengan_id)  + " "+ formatter.format(response.body().getData().getAttendanceID()));
                                alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        redirected();
                                    }
                                });
                                alertDialog.show();

                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));

                            } else {


                                gagal(response.body().getData().getResult());

                            }

                        } else if (response.code() == 201) {
                            loading.dismiss();
                        } else if (response.code() == 400) {
                            loading.dismiss();
                            gagal("Bad Request");
                        } else if (response.code() == 404) {
                            loading.dismiss();
                            gagal("Jam kerja tidak ada");
                        } else if (response.code() == 401) {
                            loading.dismiss();
                            gagal("Unauthorized");
                        } else if (response.code() == 415) {
                            loading.dismiss();
                            gagal("Unsupported Media Type");
                        } else if (response.code() == 500) {
                            loading.dismiss();
                            gagal("Internal Server Error");
                        } else {
                            loading.dismiss();
                            gagal("");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseCheckIn> call, Throwable t) {

                        String a = t.toString();
                        loading.dismiss();
                        gagal("Failure");


                    }
                });

            } catch (Exception e) {
                loading.dismiss();
                gagal("Exception");


            }
        }


    }

    private void gagal(String message){

        SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
        alertDialog.setContentText(message);
        alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                redirected ();
            }
        });
        alertDialog.show();

        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btn.setTextColor(getResources().getColor(R.color.colorWhite));

    }

    void redirected (){

        PreferenceManager.getDefaultSharedPreferences(getActivity()).
                edit()
                .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                .apply();

        Intent login = new Intent(getActivity(), AttendanceActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity(login);
    }

    private File createImageFile() {

        File root = new File(Environment.getExternalStorageDirectory(), "EATS/");

        if (!root.exists()) {
            root.mkdirs(); // this will create folder.
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  // prefix
                    ".jpg",         // suffix
                    root      // directory
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        mCurrentPhotoPath = "file:" + image.getAbsolutePath();

        return image;
    }


}