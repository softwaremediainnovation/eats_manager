package app.direksi.hras.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ListSnapActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseCreateSnap;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_1;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_2;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_3;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateSnapFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateSnapFragment.class.getSimpleName();
    private String mStartDate = "";

    private DateFormat sdf, mViewSdf;

    private Button mButtonSend, mButtonCancel;
    private ImageView mImageView, mImageChoose;

    private SweetAlertDialog loading;
    private RelativeLayout mLayoutChoose, mLayoutImageView;
    EditText mKeperluan;

    PermissionsChecker checker;
    private static final String[] PERMISSIONS_READ_STORAGE = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};

    private Uri mCropImageUri = null;
    private File file;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_snap, container, false);


        mLayoutChoose = rootView.findViewById(R.id.mLayoutChoose);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        mLayoutImageView = rootView.findViewById(R.id.mLayoutImageView);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMM yyyy");
        checker = new PermissionsChecker(getActivity());


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mKeperluan = rootView.findViewById(R.id.mKeperluan);


        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mButtonSend.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                } else {
                    startCropImageActivity();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
                    startPermissionsActivity(PERMISSIONS_READ_STORAGE);
                } else {
                    startCropImageActivity();
                }
            }
        });


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(getActivity(), this);
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());

        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                MDToast.makeText(getActivity(), "Cancelling, required permissions are not granted",
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                // Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }

    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(getActivity(), this);
    }


    @Override
    @SuppressLint("NewApi")
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    Bitmap mImageBitmap = BitmapFactory.decodeStream(inputStream);
                    int width = 0;
                    int height = 0;
                    try {


                        if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                        } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                        } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                        } else {

                            width = mImageBitmap.getWidth();
                            height = mImageBitmap.getHeight();
                        }

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                        //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                        // Uri uri = CompressImage.getImageUri(getActivity(), scaledBitmap);
                        String urr = CompressImage.makeBitmapPath(getActivity(), scaledBitmap);

                        file = new File (urr);
                        mLayoutChoose.setVisibility(View.GONE);
                        mImageChoose.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        mLayoutImageView.setVisibility(View.VISIBLE);
                        mImageView.setImageURI(result.getUri());
                        mCropImageUri = result.getUri();

                    } catch (Exception e){

                        try {
                            String urr = CompressImage.makeBitmapPath(getActivity(), mImageBitmap);
                            file = new File (urr);
                            mLayoutChoose.setVisibility(View.GONE);
                            mImageChoose.setVisibility(View.GONE);
                            mImageView.setVisibility(View.VISIBLE);
                            mLayoutImageView.setVisibility(View.VISIBLE);
                            mImageView.setImageURI(result.getUri());

                        } catch (Exception f){
                            MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                            //Toast.makeText(getActivity(), "failed", Toast.LENGTH_LONG).show();
                        }


                    } finally {
                        try {
                            inputStream.close();

                        } catch (Exception e){

                        }

                    }

                } catch (IOException e){
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

            }
        }
    }


    public void mCreateSnap() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        RequestBody email = RequestBody.create(MediaType.parse("text/plain"),
                Validation.getFirstString(PreferenceManager.getDefaultSharedPreferences(getActivity()).
                        getString(getResources().getString(R.string.pref_email), "")));

        //File creating from selected URL
        //File file = new File(CompressImage.getRealPathFromURI(mCropImageUri, getActivity()));
        Log.v(TAG, file.getName());


        MultipartBody.Part multipart = null;
        if (file.exists()) {
            String name = URLConnection.guessContentTypeFromName(file.getName());
            RequestBody requestFile = null;
            requestFile = RequestBody.create(MediaType.parse(name), file);
            multipart = MultipartBody.Part.createFormData("foto", currentDateandTime + "_" +Validation.mGetText(mKeperluan)+".jpg", requestFile);
        }

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url_snap))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseCreateSnap> call = api.mCreateSnap(email, multipart);

        call.enqueue(new Callback<ResponseCreateSnap>() {
            @Override
            public void onResponse(Call<ResponseCreateSnap> call, Response<ResponseCreateSnap> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        String status = response.body().getStatus();

                        if (status.equals(getResources().getString(R.string.response_success))) {

                            dismiss();
                            MDToast.makeText(getActivity(), response.body().getResult(),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();
                            //Toast.makeText(getActivity(), response.body().getResult(), Toast.LENGTH_SHORT).show();

                            ListSnapActivity callingActivity = (ListSnapActivity) getActivity();
                            callingActivity.onUserSelectValue("");
                            dismiss();

                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), response.body().getResult(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ResponseCreateSnap> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void mAttemptCreateSnap() {

        String reason = mKeperluan.getText().toString();

        boolean cancel = false;
        View focusView = null;


        if (TextUtils.isEmpty(reason)) {
            mKeperluan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mKeperluan;
            cancel = true;
        }

        if (mCropImageUri == null) {
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_silahkan_pilih_foto_dahulu),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            // Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loading.show();
            mCreateSnap();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateSnap();
        }

    }


}