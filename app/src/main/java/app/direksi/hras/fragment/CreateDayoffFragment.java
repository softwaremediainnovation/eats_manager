package app.direksi.hras.fragment;

import android.Manifest;
import android.app.Activity;

import android.app.Dialog;
import android.app.DialogFragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ListCutiActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseConstanta;
import app.direksi.hras.model.ResponseGeneralRequestAttendance;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_1;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_2;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_3;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.app.AppConstants.addMaxdaysCuti;
import static app.direksi.hras.app.AppConstants.addMindaysCuti;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateDayoffFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateDayoffFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;


    TextInputLayout mLayoutSelesai, mLayoutMulai;
    EditText mEdtSelesai, mEdtMulai,
            mKeperluan,
            mEdtNote,
            mJumlah;

    TextInputLayout mReason;

    private Date dStart, dEnd;
    private Integer mIdConstanta[];
    private String mNamaConstanta[], valueConstanta [];
    private Button mButtonSend, mButtonCancel;
    private Integer constanta, mHowManyDays;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri = null;
    private SweetAlertDialog loading;
    private ImageView imgClose;

    private String jamServer = "";
    private LinearLayout llLast;
    private TextView txtLastTitle;

    private LinearLayout llCheckbox;
    private CheckBox cbFinish;
    private File file;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_dayoff, container, false);


        cbFinish = rootView.findViewById(R.id.cbFinish);
        llCheckbox = rootView.findViewById(R.id.llCheckbox);
        llCheckbox.setVisibility(View.GONE);
        txtLastTitle = rootView.findViewById(R.id.txtLastTitle);
        llLast = rootView.findViewById(R.id.llLast);
        llLast.setVisibility(View.GONE);
        mJumlah = rootView.findViewById(R.id.mEdtJumlahHari);
        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        imgClose = rootView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

      /*  mLayoutMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerStart();
            }
        });*/

     /*   mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openDateRangePickerStart();
            }
        });*/

        mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerEnd();
            }
        });

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mLayoutSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerEnd();
            }
        });

        mKeperluan = rootView.findViewById(R.id.mKeperluan);
        mKeperluan.setOnClickListener(this);
        mEdtMulai.setOnClickListener(this);
        mButtonSend.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0")) {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_bisa_dimulai_nol),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                   /* Toast.makeText(getActivity(),
                            "Tidak bisa dimulai dengan nol (0)",
                            Toast.LENGTH_SHORT).show();*/
                    if (enteredString.length() > 0) {
                        mJumlah.setText(enteredString.substring(1));
                    } else {
                        mJumlah.setText("");
                    }
                } else {

                }

            }

            @Override
            public void afterTextChanged(Editable s) {


            }


        };
        // mJumlah.addTextChangedListener(mPriceWatcher);

        mJumlah.setOnClickListener(this);

        cbFinish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                @Override
                                                public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                                                    if (isChecked){

                                                        if (returnUri == null) {

                                                            MDToast.makeText(getActivity(), getResources().getString(R.string.title_silahkan_pilih_foto_dahulu),
                                                                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                                            cbFinish.setChecked(false);
                                                            cbFinish.jumpDrawablesToCurrentState();

                                                        } else {

                                                        }



                                                    }else {

                                                    }

                                                }
                                            }
        );



        mGetConstanta();
        mGetServerTim();


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(getActivity(), this);
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }


    public void takePhoto() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));
        returnUri = Uri.fromFile(photo);
        startActivityForResult(intent, 100);
        Log.v(TAG, "takePhoto " + 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    Bitmap mImageBitmap = BitmapFactory.decodeStream(inputStream);
                    int width = 0;
                    int height = 0;
                    try {


                        if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                        } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                        } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                        } else {

                            width = mImageBitmap.getWidth();
                            height = mImageBitmap.getHeight();
                        }

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                        //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                        // Uri uri = CompressImage.getImageUri(getActivity(), scaledBitmap);
                        String urr = CompressImage.makeBitmapPath(getActivity(), scaledBitmap);

                        file = new File (urr);
                        returnUri = result.getUri();

                        mImageChoose.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        mImageView.setImageBitmap(scaledBitmap);

                    } catch (Exception e){

                        try {
                            String urr = CompressImage.makeBitmapPath(getActivity(), mImageBitmap);
                            file = new File (urr);
                            returnUri = result.getUri();

                            mImageChoose.setVisibility(View.GONE);
                            mImageView.setVisibility(View.VISIBLE);
                            mImageView.setImageURI(result.getUri());

                        } catch (Exception f){
                            MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                            //Toast.makeText(getActivity(), "failed", Toast.LENGTH_LONG).show();
                        }


                    } finally {
                        try {
                            inputStream.close();

                        } catch (Exception e){

                        }

                    }

                } catch (IOException e){
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

            }
        }
    }

    public static class DateUtil {
        public static Date addDays(Date date, int days) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DATE, days); //minus number would decrement the days
            return cal.getTime();
        }
    }


    private void openDateRangePickerStart() {

        try {
            SublimePickerFragment pickerFrag = new SublimePickerFragment();
            pickerFrag.setCallback(new SublimePickerFragment.Callback() {
                @Override
                public void onCancelled() {

                }

                @Override
                public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                    mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                    mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));

                    dStart = selectedDate.getStartDate().getTime();

                    try {

                        mHowManyDays = DefaultFormatter.calculateNumberOfDaysExcludeWeekends(dStart,
                                dEnd);

                    } catch (Exception e) {

                    }

                    //  checkTglMasuk();


                }


            });

            Calendar cal = null;
            Calendar calstart = null, calEnd = null;
            cal = Calendar.getInstance();
            calstart = Calendar.getInstance();
            calEnd = Calendar.getInstance();
            try {

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                Date date = dateFormat.parse(jamServer);
                cal = Calendar.getInstance();

                cal.setTime(date);
                calstart.setTime(date);
                calEnd.setTime(date);


                if (mKeperluan.getText().toString().equals("Cuti")) {

                    if ( Integer.valueOf(mJumlah.getText().toString()) > 3){

                        calstart.add(Calendar.DAY_OF_YEAR, 0);
                        calEnd.add(Calendar.DAY_OF_YEAR, 30);
                    } else {
                        calstart.add(Calendar.DAY_OF_YEAR, 0);
                        calEnd.add(Calendar.DAY_OF_YEAR, addMaxdaysCuti);
                    }
                }

                else if (mKeperluan.getText().toString().equals("Melahirkan")) {

                    calstart.add(Calendar.DAY_OF_YEAR, addMindaysCuti);
                    calEnd.add(Calendar.DAY_OF_YEAR, 30);
                }
                else{
                    calstart.add(Calendar.DAY_OF_YEAR, addMindaysCuti);
                    calEnd.add(Calendar.DAY_OF_YEAR, addMaxdaysCuti);
                }


            } catch (Exception e) {

            }



            long now = System.currentTimeMillis() - 1000;
            // ini configurasi agar library menggunakan method Date Range Picker
            SublimeOptions options = new SublimeOptions();
            options.setCanPickDateRange(false);
            options.setDateRange(calstart.getTimeInMillis(), calEnd.getTimeInMillis());
            // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
            options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
            options.setDateParams(cal);
            // options.setDateRange( Long.MIN_VALUE , now);

            Bundle bundle = new Bundle();
            bundle.putParcelable("SUBLIME_OPTIONS", options);
            pickerFrag.setArguments(bundle);

            pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
            pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");

        } catch (Exception f){

        }
    }

    private void openDateRangePickerEnd() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mEndDate = sdf.format(selectedDate.getEndDate().getTime());
                mEdtSelesai.setText(mViewSdf.format(selectedDate.getEndDate().getTime()));

                dEnd = selectedDate.getStartDate().getTime();
                try {

                    mHowManyDays = DefaultFormatter.calculateNumberOfDaysExcludeWeekends(dStart,
                            dEnd);

                } catch (Exception e){

                }
            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseConstanta> call = api.mGetConstanta(Authorization);

            call.enqueue(new Callback<ResponseConstanta>() {
                @Override
                public void onResponse(Call<ResponseConstanta> call, Response<ResponseConstanta> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getDataConstanta().size() > 0) {
                                mIdConstanta = new Integer[response.body().getDataConstanta().size()];
                                mNamaConstanta = new String[response.body().getDataConstanta().size()];
                                valueConstanta = new String[response.body().getDataConstanta().size()];

                                for (int i = 0; i < response.body().getDataConstanta().size(); i++) {
                                    mIdConstanta[i] = response.body().getDataConstanta().get(i).getConstantaID();
                                    mNamaConstanta[i] = response.body().getDataConstanta().get(i).getName();
                                    valueConstanta[i] = response.body().getDataConstanta().get(i).getValue();
                                }

                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseConstanta> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    public void mCreateDayOff() {


        try {


            RequestBody IsDisease = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(cbFinish.isChecked()));
            RequestBody mBodyConstanta = RequestBody.create(MediaType.parse("text/plain"), constanta.toString());
            RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"),
                    PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_employeeid), ""));
            RequestBody HowManyDays = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mJumlah));
            RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), mStartDate);
            RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));


            //File creating from selected URL
            MultipartBody.Part multipart = null;
            if (returnUri != null) {
                //File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
                multipart = null;
                if (file.exists()) {
                    String name = URLConnection.guessContentTypeFromName(file.getName());
                    RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
                    multipart = MultipartBody.Part.createFormData("File", file.getName(), requestFile);
                }
            }

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


            // simplified call to request the news with already initialized service
            Call<ResponseGeneralRequestAttendance> call = api.mCreateDayOff(Authorization, mBodyConstanta, EmployeeID, HowManyDays, Date, Note, IsDisease, multipart);

            call.enqueue(new Callback<ResponseGeneralRequestAttendance>() {
                @Override
                public void onResponse(Call<ResponseGeneralRequestAttendance> call, Response<ResponseGeneralRequestAttendance> response) {

                    try {

                        loading.dismiss();

                        if (response.isSuccessful()) {
                            int statusCode = response.body().getErrCode();

                            if (statusCode == 0) {

                                if (response.body().getData().getStatus() == null) {

                                    SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                                    alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                    alertDialog.setContentText("");
                                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), ListCutiActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);
                                        }
                                    });
                                    alertDialog.show();

                                    Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                    btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                    btn.setTextColor(getResources().getColor(R.color.colorWhite));

                                } else {
                                    SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
                                    alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
                                    alertDialog.setContentText(response.body().getData().getResult());
                                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), ListCutiActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);
                                        }
                                    });
                                    alertDialog.show();

                                    Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                    btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                    btn.setTextColor(getResources().getColor(R.color.colorWhite));
                                }


                            } else {

                                try {
                                    loading.dismiss();
                                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                    redirected();
                                } catch (Exception e) {

                                }

                            }

                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e) {

                            }

                        }

                    } catch (Exception e) {

                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception s) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneralRequestAttendance> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception e) {

                    }

                }
            });
        } catch (Exception s){

            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                redirected();
            } catch (Exception ss) {

            }

        }

    }

    private void mAttemptCreateDayOff() {

        mEdtMulai.setError(null);
        mJumlah.setError(null);
        mKeperluan.setError(null);
        mEdtNote.setError(null);


        String start = mEdtMulai.getText().toString();
        String end = mJumlah.getText().toString();
        String reason = mKeperluan.getText().toString();
        String note = mEdtNote.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(start)) {
            mEdtMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtMulai;
            cancel = true;
        }

        if (TextUtils.isEmpty(end)) {
            mJumlah.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mJumlah;
            cancel = true;
        }

        if (TextUtils.isEmpty(reason)) {
            mKeperluan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mKeperluan;
            cancel = true;
        }

        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }

       /* if (returnUri == null) {
            Toast.makeText(getActivity(), "Silakan pilih foto terlebih dahulu", Toast.LENGTH_SHORT).show();
            return;
        }*/


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loading.show();
            mCreateDayOff();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mKeperluan) {
            try {
                if (mIdConstanta.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mKeperluan.setText(mNamaConstanta[item]);
                            mEdtMulai.setText("");
                            if (valueConstanta[item].equals("")){
                                mJumlah.setEnabled(true);
                                mJumlah.setText("");

                                if (mNamaConstanta[item].equals("Sakit")){
                                    llCheckbox.setVisibility(View.VISIBLE);
                                } else {
                                    llCheckbox.setVisibility(View.GONE);
                                    cbFinish.setChecked(false);
                                }

                            }else {

                                llCheckbox.setVisibility(View.GONE);
                                cbFinish.setChecked(false);

                                if (mNamaConstanta[item].equals("Menikah")){
                                    mJumlah.setEnabled(true);
                                    mJumlah.setText(valueConstanta[item]);
                                } else {
                                    mJumlah.setEnabled(false);
                                    mJumlah.setText(valueConstanta[item]);
                                }


                            }

                            // checkTglMasuk();

                            constanta = mIdConstanta[item];
                            imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        } else if (v.getId() == R.id.mEdtMulai) {

            if (mKeperluan.getText().toString().equals("Cuti")) {

                if (!TextUtils.isEmpty(mJumlah.getText().toString())){
                    if (jamServer.equals("")) {
                        mGetServerTim();
                    } else {
                        openDateRangePickerStart();
                    }
                } else {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_isian_harus_diisi),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                }
            } else {

                if (jamServer.equals("")) {
                    mGetServerTim();
                } else {
                    openDateRangePickerStart();
                }
            }

        } else if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        } else if (v.getId() == R.id.mEdtJumlahHari) {
            final String[] listrik = getResources().getStringArray(R.array.array_cuti);

            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mJumlah.getWindowToken(), 0);
            final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
            b.setItems(listrik, (dialog, item) -> {
                mJumlah.setText("");
                mJumlah.setText(listrik[item]);
                imm.hideSoftInputFromWindow(mJumlah.getWindowToken(), 0);

                if (mKeperluan.getText().toString().equals("Cuti")) {
                    mEdtMulai.setText("");
                }
                //checkTglMasuk();
            }).show();
        }

    }


    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");*/

                            jamServer = response.body().getData();

                            // mTimeServer = date;

                            // Log.e(TAG, mTimeServer.toString());

                            // timer(mTimeServer);

                        }

                    } catch (Exception e) {
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }

    }

    void checkTglMasuk (){

        if (mJumlah.getText().length() > 0 & mKeperluan.getText().length() > 0 && mEdtMulai.getText().length() > 0 ){

            llLast.setVisibility(View.VISIBLE);

            Calendar c = Calendar.getInstance();
            try {
                c.setTime(dStart);
                c.add(Calendar.DATE, Integer.parseInt(mJumlah.getText().toString()));
                String value = ("Tanggal masuk  <b>" + mViewSdf.format(c.getTime()) + "</b>");
                txtLastTitle.setText(Html.fromHtml(value));
            } catch (Exception e) {

            }

        } else {
            llLast.setVisibility(View.GONE);
        }

    }

    void redirected (){

        Intent login = new Intent(getActivity(), ListCutiActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }


}