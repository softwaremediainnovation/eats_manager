package app.direksi.hras.fragment;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.adapter.GeneralRuleAdapter;
import app.direksi.hras.adapter.OrganizationRuleAdapter;
import app.direksi.hras.model.EmployeeInfoGeneralRule;
import app.direksi.hras.model.EmployeeInfoOrganizationRule;
import app.direksi.hras.model.ResponseDataDetailEmployeeInfo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 17/06/2019.
 */

public class RuleEmployeFragment  extends Fragment /*implements PenawaranDetailActivity.DataUpdateListener*/ {
    private RecyclerView recyclerViewGeneral, recyclerViewOrganization;
    private List<EmployeeInfoGeneralRule> dataListGR = new ArrayList<>();
    private List<EmployeeInfoOrganizationRule> dataListOR = new ArrayList<>();
    private List<String> existLoad = new ArrayList<>();
    private GeneralRuleAdapter GRAdapter;
    private OrganizationRuleAdapter ORAdapter;
    private TextView textNoInternet, lblOrgRule, lblGeneralRule;
    private ProgressBar loading;
    private SwipeRefreshLayout swiperefresh;
    private ScrollView sv1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.rule_fragment, container, false);
        recyclerViewGeneral = (RecyclerView) v.findViewById(R.id.recyclerViewGeneral);
        recyclerViewOrganization = (RecyclerView) v.findViewById(R.id.recyclerViewOrganization);
        lblOrgRule = (TextView) v.findViewById(R.id.lblOrgRule);
        lblGeneralRule = (TextView) v.findViewById(R.id.lblGeneralRule);
        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        sv1 = (ScrollView) v.findViewById(R.id.sv1);
        sv1.setVisibility(View.GONE);
        loading = (ProgressBar) v.findViewById(R.id.loading);
      //  swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
    }

    private void initRecyclerView() {
        GRAdapter = new GeneralRuleAdapter(dataListGR, getActivity());
        ORAdapter = new OrganizationRuleAdapter(dataListOR, getActivity());
        recyclerViewGeneral.setHasFixedSize(true);
        recyclerViewOrganization.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManagerGR = new LinearLayoutManager(getActivity());
        RecyclerView.LayoutManager mLayoutManagerOR = new LinearLayoutManager(getActivity());

        recyclerViewGeneral.setLayoutManager(mLayoutManagerGR);
        recyclerViewOrganization.setLayoutManager(mLayoutManagerOR);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerViewGeneral.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOrganization.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

        recyclerViewGeneral.addItemDecoration(itemDecorator);
        recyclerViewGeneral.setAdapter(GRAdapter);
        recyclerViewGeneral.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewGeneral, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               /* Penawaran penawaran = penawaranList.get(position);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();

                GoTo("com.mii.erp.PenawaranDetailActivity", penawaran);*/
                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        recyclerViewOrganization.addItemDecoration(itemDecorator);
        recyclerViewOrganization.setAdapter(ORAdapter);
        recyclerViewOrganization.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewOrganization, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               /* Penawaran penawaran = penawaranList.get(position);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();

                GoTo("com.mii.erp.PenawaranDetailActivity", penawaran);*/
                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
       /* swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                dataList = new ArrayList<>();
                cAdapter = new EmployeeAttendanceAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
              //  RefreshService();

            }});*/
        fetchService();

    }

    public static Fragment newInstance() {
        return new RuleEmployeFragment();
    }



    public void fetchService() {

        if ( existLoad.size() > 0)
        {

            //sv1.setVisibility(View.VISIBLE);
            if (dataListGR.size() > 0 || dataListOR.size() > 0) {
                sv1.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.GONE);

                if (dataListGR.size() > 0){
                    lblGeneralRule.setVisibility(View.VISIBLE);
                } else {
                    lblGeneralRule.setVisibility(View.GONE);
                }
                if (dataListOR.size() > 0){
                    lblOrgRule.setVisibility(View.VISIBLE);
                } else {
                    lblOrgRule.setVisibility(View.GONE);
                }
            }
            else
            {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.VISIBLE);
            }
        }
        else {

            textNoInternet.setVisibility(View.GONE);
            try {
                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_token), ""));
                Map<String, String> data = new HashMap<>();
                data.put("employeeId", (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_employeeid_view), "")));

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseDataDetailEmployeeInfo> call = api.getEmployeeInfo( Authorization, data);

                call.enqueue(new Callback<ResponseDataDetailEmployeeInfo>() {
                    @Override
                    public void onResponse(Call<ResponseDataDetailEmployeeInfo> call, Response<ResponseDataDetailEmployeeInfo> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                        if (response.isSuccessful()) {

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                            if (response.body().getErrCode() == 0 & (response.body().getData().getRules().size() > 0 ||
                                    response.body().getData().getOrgRules().size() > 0)) {

                                sv1.setVisibility(View.VISIBLE);
                                if (response.body().getData().getRules().size() > 0) {
                                    dataListGR.clear();
                                    existLoad.add("Reload");
                                    dataListGR.addAll(response.body().getData().getRules());
                                    GRAdapter.notifyDataSetChanged();
                                    lblGeneralRule.setVisibility(View.VISIBLE);
                                } else {
                                    existLoad.add("Reload");
                                    lblGeneralRule.setVisibility(View.GONE);
                                }
                                if (response.body().getData().getOrgRules().size() > 0) {
                                    dataListOR.clear();
                                    existLoad.add("Reload");
                                    dataListOR.addAll(response.body().getData().getOrgRules());
                                    ORAdapter.notifyDataSetChanged();
                                    lblOrgRule.setVisibility(View.VISIBLE);
                                } else {
                                    existLoad.add("Reload");
                                    lblOrgRule.setVisibility(View.GONE);
                                }



                                loading.setVisibility(View.GONE);
                            } else {
                                existLoad.add("Reload");
                                textNoInternet.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                            }

                            loading.setVisibility(View.GONE);


                        } else {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            loading.setVisibility(View.GONE);
                            Log.v("DoneFragment", "tidak sukses");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataDetailEmployeeInfo> call, Throwable t) {
                        Log.v("DoneFragment", "on failure");
                        loading.setVisibility(View.GONE);
                        // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                    }
                });
            } catch (Exception e) {
                Log.v("DoneFragment", "masuk catch");
                if (isAdded())
                    loading.setVisibility(View.GONE);
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
               // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

            }
        }


    }






}


