package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import app.direksi.hras.R;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class UpdateAppFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_update_app, container, false);
        setCancelable(false);

        Bundle bundle = getArguments();
        String mAction = bundle.getString("action", "");
        String nNewVersion = bundle.getString("version", "");
        final String mDateNow = bundle.getString("datenow", "");
        final TextView titleDialog = (TextView) view.findViewById(R.id.titleDialog);
        final TextView messageDialog = (TextView) view.findViewById(R.id.messageDialog);
        final LinearLayout layoutTwoButton = (LinearLayout) view.findViewById(R.id.layoutTwoButton);
        final LinearLayout layoutSingleButton = (LinearLayout) view.findViewById(R.id.layoutSingleButton);
        final Button btnNoDialog = (Button) view.findViewById(R.id.btnNoDialog);
        final Button btnYesDialog = (Button) view.findViewById(R.id.btnYesDialog);
        final Button btnOkDialog = (Button) view.findViewById(R.id.btnOkDialog);

        if (mAction.equals("force")) {
            layoutSingleButton.setVisibility(View.VISIBLE);
            btnOkDialog.setText("Update");
        } else {
            layoutTwoButton.setVisibility(View.VISIBLE);
            btnYesDialog.setText(getResources().getString(R.string.text_button_update_later));
            btnNoDialog.setText(getResources().getString(R.string.text_button_update_now));
        }

        messageDialog.setText(getResources().getString(R.string.text_update_1) + " " +  nNewVersion + " " +
                getResources().getString(R.string.text_update_2));

        btnNoDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                        .putString(getResources().getString(R.string.pref_update_application), "1").apply();

                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                        .putString(getResources().getString(R.string.pref_count_date_application), mDateNow).apply();

                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        btnYesDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                        .putString(getResources().getString(R.string.pref_update_application), "1").apply();


                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                        .putString(getResources().getString(R.string.pref_count_date_application), mDateNow).apply();

                dismiss();
            }
        });

        btnOkDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 15;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return view;
    }


}