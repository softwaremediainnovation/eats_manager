package app.direksi.hras.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.CheckinActivity;
import app.direksi.hras.FingerprintActivity;
import app.direksi.hras.R;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.adapter.AttendanceKuAdapter;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.ResponseDataAttendance;
import app.direksi.hras.util.MessageDialog;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 08/05/2019.
 */

public class AttendanceFragment extends Fragment {
    private View layoutInflater;
    private RecyclerView recyclerView;
    private List<DataAttendance> reportResult;
    private List<DataAttendance> report;
    private SweetAlertDialog loading;
    private int count_company = 0, count_job = 0;
    private String Authorization;
    private RelativeLayout activity_reports;
    private Button mButtonSend;
    private MessageDialog messageDialog;
    private TextView tv_empty_rv_report;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportResult = new ArrayList<>();
        report = new ArrayList<DataAttendance>();
//        setContentView(R.layout.activity_reports);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle saveInstanceState) {
        layoutInflater = inflater.inflate(R.layout.attendance_fragment_layout, container, false);
        setHasOptionsMenu(true); //to set toolbar menu

        //counter jml job
        tv_empty_rv_report = layoutInflater.findViewById(R.id.tv_empty_rv_report);
        tv_empty_rv_report.setVisibility(View.GONE);
        messageDialog = new MessageDialog();
        activity_reports = layoutInflater.findViewById(R.id.activity_reports);
        mButtonSend = layoutInflater.findViewById(R.id.mButtonSend);
        activity_reports.setVisibility(View.GONE);


        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {

                        Intent intent = new Intent(getContext(), CheckinActivity.class);
                        startActivity(intent);

                    } else {
                        Intent intent = new Intent(getContext(), FingerprintActivity.class);
                        startActivity(intent);

                    }



            }
        });



        //kode untuk swipe to refresh
        swipeRefreshLayout = (SwipeRefreshLayout) layoutInflater.findViewById(R.id.refreshCompany);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.darker_gray);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                loadJSON();
                onItemLoad();
            }

            void onItemLoad() {
                swipeRefreshLayout.setRefreshing(false);
            }

        });

        //code untuk recyclerview
        recyclerView = (RecyclerView) layoutInflater.findViewById(R.id.rv_reports);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
      //  recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL)); //untuk divider
        recyclerView.setAdapter(new AttendanceKuAdapter(report, getContext()));



        return layoutInflater;
    }

    @Override
    public void onResume() {
        Log.e("DEBUG", "onResume of HomeFragment");
        super.onResume();

        loading = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        loadJSON();
    }

    private void loadJSON() {
        loading.show();
        try {
            swipeRefreshLayout.setVisibility(View.GONE);

            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String strDate = sdf.format(c.getTime());

            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));
            String idEmployee = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));

            Map<String, String> data = new HashMap<>();
            data.put("dateFilter", strDate);
            data.put("employeeID", idEmployee);

            //untuk mencegah onFailure jika terjadi socketTimeOut
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            //assign variabel request ke class interface TabAdverRequest
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataAttendance> call = api.getAttendanceHariIni( Authorization, data);
            call.enqueue(new Callback<ResponseDataAttendance>() {
                @Override
                public void onResponse(Call<ResponseDataAttendance> call, Response<ResponseDataAttendance> response) {
                    if (response.isSuccessful()){

                        activity_reports.setVisibility(View.VISIBLE);
//                        reportResult.clear();
                        reportResult = response.body().getData();
                        if (!reportResult.isEmpty()){
                            report.clear();
                            report.addAll(reportResult);
                            swipeRefreshLayout.setVisibility(View.VISIBLE);
                            //counter jumlah jobs
                            count_company = report.size();
                          /*  jml_company.setText(String.valueOf(count_company));
                            if (count_company > 1){
                                label_company.setText("Total Companies");
                            }else {
                                label_company.setText("Total Company");
                            }*/

                            //sinkronisasi untuk filter
                            synchronized(recyclerView.getAdapter()){
                                recyclerView.getAdapter().notifyDataSetChanged();
                            }
                        }else {
                            swipeRefreshLayout.setVisibility(View.GONE);
                            tv_empty_rv_report.setText("Tidak ada hasil");
                            tv_empty_rv_report.setVisibility(View.VISIBLE);
                        }
                        loading.hide();

                    } else if (response.errorBody() != null) {
                        if (response.code() == 500) {
                          //  Toast.makeText(getActivity(), "Server Error, Silahkan Coba Lagi Nanti.", Toast.LENGTH_LONG).show();
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();

                               /* messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_activity_attendance)
                                        , getResources().getString(R.string.loading_error));*/
                            } catch (Exception e){

                            }
                        }else if(response.code() == 401){
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                               /* messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_activity_attendance)
                                        , getResources().getString(R.string.loading_error));*/
                            } catch (Exception e){

                            }
                            //handle unauthorized
                           // Toast.makeText(getActivity(), "Silahkan Login Kembali.", Toast.LENGTH_LONG).show();
                           /* getActivity().finish(); //semua activity dan preferences di clear. untuk bisa login kembali.
                            mClearPreferences();
                            Intent logout = new Intent(getActivity(), LoginActivity.class);
                            logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);    //set flag untuk logout
                            startActivity(logout);*/
                        } else{
                           /* Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
                            loading.hide();*/
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                              /*  messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_activity_attendance)
                                        , getResources().getString(R.string.loading_error));*/
                            } catch (Exception e){

                            }
                        }
                    } else {
                       /* Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
                        loading.hide();*/
                        loading.hide();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           /* messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_activity_attendance)
                                    , getResources().getString(R.string.loading_error));*/
                        } catch (Exception e){

                        }
                    }
                }
                @Override
                public void onFailure(Call<ResponseDataAttendance> call, Throwable t) {
                   /* Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();
                    System.out.println("onFailure = "+t.toString());*/
                    loading.hide();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                     /*   messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_activity_attendance)
                                , getResources().getString(R.string.loading_error));*/
                    } catch (Exception e){

                    }
                }
            });
        }catch (Exception e) {
          /*  Toast.makeText(getActivity(), "Gagal Memuat, Periksa Koneksi Anda.", Toast.LENGTH_LONG).show();*/
            loading.hide();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
               /* messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_activity_attendance)
                        , getResources().getString(R.string.loading_error));*/
            } catch (Exception f){

            }
        }
    }

    //filter untuk searchView, filterable ada di adapter.
    private void filterList(String name){
        synchronized(recyclerView.getAdapter()){
           /* ((AttendanceAdapter)recyclerView.getAdapter()).getFilter().filter(name);*/
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    //method untuk searchView di MENU PENGIKLAN
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
       /* inflater.inflate(R.menu.main, menu);
        try {
            // Associate searchable configuration with the SearchView
            final SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            searchView.setQueryHint("search by company, address");
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {
                    filterList(s);
                    return false;
                }
                @Override
                public boolean onQueryTextChange(String s) {
                    filterList(s);
                    return true;
                }
            });
        }catch(Exception e){e.printStackTrace();}*/
    }

    //clear share preference untuk login
    void mClearPreferences() {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putString("pref_token", "")
                .putString("pref_email", "")
                .putString("pref_password", "")
                .apply();
    }
}
