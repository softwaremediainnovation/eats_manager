package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.LoanActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseDetailLoan;
import app.direksi.hras.model.ResponsePostLoan;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateLoanFragment extends DialogFragment implements View.OnClickListener, View.OnFocusChangeListener, RadioGroup.OnCheckedChangeListener {

    private static final String TAG = CreateLoanFragment.class.getSimpleName();

    private SweetAlertDialog loading;
    private EditText mCicilan, mJumlah, mEdtLoan,
            mEdtNote;
    private ScrollView mMainLayout;
    private Button mButtonSend, mButtonCancel;
    private RadioGroup mGroupPbon;
    private RadioButton mRadioYes, mRadioNo;
    private boolean isMingguan = false;
    private DateFormat sdf;
    private ImageView imgClose;
    private TextView txtLastTitle;

    private RelativeLayout rlLoading;
    private TextView txtTerbayarLast, txtSisaLast, txtJumlahLast;
    private LinearLayout llLast;
    private TableLayout tbLast;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_loan, container, false);


        sdf = new SimpleDateFormat("yyyy-MM-dd");


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        imgClose = rootView.findViewById(R.id.imgClose);
        mGroupPbon = rootView.findViewById(R.id.mGroupPbon);
        mRadioYes = rootView.findViewById(R.id.mRadioYes);
        mRadioNo = rootView.findViewById(R.id.mRadioNo);
        txtLastTitle = rootView.findViewById(R.id.txtLastTitle);
        String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + "  <b> " + getResources().getString(R.string.title_rp_0) + "</b>");
        txtLastTitle.setText(Html.fromHtml(value));
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        mJumlah = rootView.findViewById(R.id.mJumlah);
        mCicilan = rootView.findViewById(R.id.mCicilan);
        mEdtLoan = rootView.findViewById(R.id.mEdtLoan);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mMainLayout = rootView.findViewById(R.id.mMainLayout);

        txtJumlahLast = rootView.findViewById(R.id.txtJumlahLast);
        txtSisaLast = rootView.findViewById(R.id.txtSisaLast);
        txtTerbayarLast = rootView.findViewById(R.id.txtTerbayarLast);

        tbLast = rootView.findViewById(R.id.tbLast);
        tbLast.setVisibility(View.GONE);
        llLast = rootView.findViewById(R.id.llLastllLast);
        llLast.setVisibility(View.GONE);
      //  txtLastTitle = rootView.findViewById(R.id.txtLastTitle);
        rlLoading = rootView.findViewById(R.id.rlLoading);

        mCicilan.setOnClickListener(this);
        mButtonSend.setOnClickListener(this);
        mButtonCancel.setOnClickListener(this);
        mCicilan.setOnFocusChangeListener(this);

        mGroupPbon.setOnCheckedChangeListener(this);
        imgClose.setOnClickListener(this);

        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0")) {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_bisa_dimulai_nol),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                  /*  Toast.makeText(getActivity(),
                            "Tidak bisa dimulai dengan nol (0)",
                            Toast.LENGTH_SHORT).show();*/
                    if (enteredString.length() > 0) {
                        mJumlah.setText(enteredString.substring(1));

                    } else {
                        mJumlah.setText("");
                    }
                } else {
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    int length = mCicilan.getText().length();
                    if (length > 0 &&  mJumlah.getText().length() > 0 && enteredString.length() < 12 ){

                        if (isMingguan) {
                            String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + "  <b> " + getResources().getString(R.string.title_rp) + " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))) + "</b>");
                            txtLastTitle.setText(Html.fromHtml(value));
                           // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))));
                        } else {
                            String value = (getResources().getString(R.string.title_cicilan_perbulan_sebesar) + "  <b> " + getResources().getString(R.string.title_rp) + " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))) + "</b>");
                            txtLastTitle.setText(Html.fromHtml(value));
                           // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))));
                        }
                    }
                }


                if (enteredString.length() >= 12){
                    String str = enteredString.substring(0, 11);
                    mJumlah.setText(str);
                    mJumlah.setSelection(str.length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mJumlah.getText().hashCode() == s.hashCode()) {
                    mJumlah.removeTextChangedListener(this);
                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                    symbols.setGroupingSeparator(',');
                    try {
                        mJumlah.setText(formatter.format(Double.parseDouble(s.toString().replace(",", "") + "")));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    } catch (Exception e) {
                        mJumlah.setText(s.toString().replace(",", ""));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    }
                    mJumlah.addTextChangedListener(this);
                }

            }


        };
        mJumlah.addTextChangedListener(mPriceWatcher);


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_employeeid), ""));

        getLast(Long.parseLong(id));

        return rootView;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mCicilan) {
            final String[] listrik = getResources().getStringArray(R.array.array_cicilan);

            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mCicilan.getWindowToken(), 0);
            final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
            b.setItems(listrik, (dialog, item) -> {
                mCicilan.setText("");
                mCicilan.setText(listrik[item]);
                DecimalFormat formatter = new DecimalFormat("#,###,###");
                int length = mJumlah.getText().length();
                if (length > 0){
                     if (isMingguan) {
                         String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b> " + getResources().getString(R.string.title_rp) + " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))) + "</b>");
                         txtLastTitle.setText(Html.fromHtml(value));
                        // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) ));
                     }else {
                         String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b> " + getResources().getString(R.string.title_rp) + " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))) + "</b>");
                         txtLastTitle.setText(Html.fromHtml(value));
                        // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))));
                     }
                }

                imm.hideSoftInputFromWindow(mCicilan.getWindowToken(), 0);
            }).show();
        } else if (v.getId() == R.id.mButtonCancel) {
            dismiss();
        } else if (v.getId() == R.id.mButtonSend) {

            checkField();
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }
    }


    private void checkField() {



        mEdtLoan.setError(null);
        mJumlah.setError(null);
        mCicilan.setError(null);
        mEdtNote.setError(null);


        String loan = mEdtLoan.getText().toString();
        String jumlah = mJumlah.getText().toString();
        String cicilan = mCicilan.getText().toString();
        String note = mEdtNote.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(loan)) {
            mEdtLoan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtLoan;
            cancel = true;
        }

        if (TextUtils.isEmpty(jumlah)) {
            mJumlah.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mJumlah;
            cancel = true;
        }

        if (TextUtils.isEmpty(cicilan)) {
            mCicilan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mCicilan;
            cancel = true;
        }

        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            mLoginUser();
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final Handler handler;
        handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                mMainLayout.smoothScrollTo(0, 500);
                handler.postDelayed(this, 200);
            }
        };
        handler.postDelayed(r, 200);
    }


    private void mLoginUser() {

        try {
            loading.show();

            Date currentTime = Calendar.getInstance().getTime();


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), "tes"));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            RequestBody Name = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtLoan));
            RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), sdf.format(currentTime));
            RequestBody DatePaid = RequestBody.create(MediaType.parse("text/plain"), sdf.format(currentTime));
            RequestBody Amount = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mJumlah).replaceAll(",", ""));
            RequestBody Installment = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mCicilan).replaceAll("x", ""));
            RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));
            RequestBody IsPbonMingguan = RequestBody.create(MediaType.parse("text/plain"), Boolean.toString(isMingguan));
            RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), "0"));

            Log.v(TAG, Validation.mGetText(mEdtLoan));
            Log.v(TAG, sdf.format(currentTime));
            Log.v(TAG, sdf.format(currentTime));
            Log.v(TAG, Validation.mGetText(mJumlah).replaceAll(",", ""));
            Log.v(TAG, Validation.mGetText(mCicilan).replaceAll("x", ""));
            Log.v(TAG, Validation.mGetText(mEdtNote));
            Log.v(TAG, Boolean.toString(isMingguan));
            Log.v(TAG, PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), "0"));


            Call<ResponsePostLoan> call = api.mPostLoans(Authorization, Name, Date, Amount, Installment, Note, IsPbonMingguan, EmployeeID, DatePaid);


            call.enqueue(new Callback<ResponsePostLoan>() {
                @Override
                public void onResponse(Call<ResponsePostLoan> call, Response<ResponsePostLoan> response) {

                    Log.v(TAG, response.toString());

                    try {
                        //loading hide
                        loading.hide();

                        if (response.isSuccessful()) {
                            int statusCode = response.body().getErrCode();
                            String statusMessage = response.body().getErrMessage();
                            //status code 1 if login success
                            if (statusCode == 0) {

                               /* dismiss();
                                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loan_success), Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getActivity(), LoanActivity.class);
                                startActivity(intent);
                                getActivity().finish();*/

                                SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                                alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                alertDialog.setContentText("");
                                alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        Intent login = new Intent(getActivity(), LoanActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                    }
                                });
                                alertDialog.show();

                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));


                             /*   new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                        .setTitleText("SUKSES")
                                        .setContentText("")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                            @Override
                                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                                Intent login = new Intent(getActivity(), LoanActivity.class);
                                                login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                startActivity(login);
                                            }
                                        })
                                        .show();*/

                            } else {

                                try {
                                    loading.dismiss();
                                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                   // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                    redirected();
                                } catch (Exception e){

                                }

                            }

                        } else {
                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                               // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }
                        }
                    } catch (Exception e) {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception s){

                        }
                    }


                }

                @Override
                public void onFailure(Call<ResponsePostLoan> call, Throwable t) {
                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                       // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception e){

                    }
                }

            });


        } catch (Exception e) {
            try {
                loading.dismiss();
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
               // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                redirected();
            } catch (Exception r){

            }
        }
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.mRadioYes) {
            isMingguan = true;
            DecimalFormat formatter = new DecimalFormat("#,###,###");
            int length = mCicilan.getText().length();
            if (length > 0 &&  mJumlah.getText().length() > 0){
                String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b> " + getResources().getString(R.string.title_rp) + " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))) + "</b>");
                txtLastTitle.setText(Html.fromHtml(value));
               // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))));
            }
            final String[] listrik = getResources().getStringArray(R.array.array_cicilan);
            mCicilan.setText(listrik[0]);
            mCicilan.setEnabled(false);

        } else if (checkedId == R.id.mRadioNo) {
            isMingguan = false;

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            int length = mCicilan.getText().length();
            if (length > 0 &&  mJumlah.getText().length() > 0){
                String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b> " + getResources().getString(R.string.title_rp) + " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))) + "</b>");
                txtLastTitle.setText(Html.fromHtml(value));
               // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))));
            }
            mCicilan.setEnabled(true);
        }
    }


    void redirected (){

        Intent login = new Intent(getActivity(), LoanActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }


    public void getLast(Long id) {

        // loading.show();

        try {

            rlLoading.setVisibility(View.VISIBLE);
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", String.valueOf(id));



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailLoan> call = api.getLastLoan(Authorization, data);

            call.enqueue(new Callback<ResponseDetailLoan>() {
                @Override
                public void onResponse(Call<ResponseDetailLoan> call, Response<ResponseDetailLoan> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            llLast.setVisibility(View.VISIBLE);
                            rlLoading.setVisibility(View.GONE);
                            if (response.body().getData() != null){

                                tbLast.setVisibility(View.VISIBLE);
                               // txtLastTitle.setText("Total Pinjaman");

                             /*   if (response.body().getData().getDate() != null)
                                    txtTanggalLast.setText(DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getDate()));*/

                                Double temp = ( response.body().getData().getTotalPinjaman() != null ? response.body().getData().getTotalPinjaman() : 0) - ( response.body().getData().getTotalPembayaran() != null ? response.body().getData().getTotalPembayaran() : 0);


                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                txtSisaLast.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(temp));
                                txtJumlahLast.setText(String.valueOf(response.body().getData().getTotalPinjaman() != null ? getResources().getString(R.string.title_rp) +  " " + formatter.format(response.body().getData().getTotalPinjaman()) : ""));
                              /*  txtInstallmentLast.setText(String.valueOf(response.body().getData().getInstallment() != null ?response.body().getData().getInstallment() + "x" : ""));*/
                                txtTerbayarLast.setText(String.valueOf(response.body().getData().getTotalPembayaran() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getData().getTotalPembayaran()) : ""));
                               /* txtCicilanLast.setText(String.valueOf(response.body().getData().getInstallmentAmount() != null ? "Rp " + formatter.format(response.body().getData().getInstallmentAmount()) : ""));
                                if (response.body().getData().getIsPaid()!= null) {
                                    txtStatusLast.setText(String.valueOf(response.body().getData().getIsPaid() == true ? "Lunas" : "Belum Lunas"));
                                } else {
                                    txtStatusLast.setText("");
                                }
                                txtNoteLast.setText(response.body().getData().getNote()!=null ? response.body().getData().getNote() : "");

                                try {
                                    txtCreatorLast.setText(response.body().getData().getApprover().getFirstName() + " " + response.body().getData().getApprover().getLastName());
                                    txtCatatanLast.setText(response.body().getData().getComment());
                                } catch (Exception e){

                                }*/




                            }else {
                                rlLoading.setVisibility(View.GONE);
                                txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_pinjaman));
                                tbLast.setVisibility(View.GONE);
                            }
                            // loading.hide();




                        } else {
                            // loading.hide();
                            rlLoading.setVisibility(View.GONE);
                            llLast.setVisibility(View.VISIBLE);
                            txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_pinjaman));
                            tbLast.setVisibility(View.GONE);
                           /* Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();*/
                        }

                    } else {
                        // loading.hide();
                        rlLoading.setVisibility(View.GONE);
                        llLast.setVisibility(View.VISIBLE);
                        txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_pinjaman));
                        tbLast.setVisibility(View.GONE);
                       /* Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();*/
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailLoan> call, Throwable t) {
                    //loading.hide();
                    rlLoading.setVisibility(View.GONE);
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception s){

                    }
                }
            });
        } catch (Exception e) {
            // loading.hide();
            rlLoading.setVisibility(View.GONE);
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
               // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch (Exception s){

            }
        }
    }

}