package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.ZoomPhotoActivity;
import app.direksi.hras.model.ResponseDataEmployeeID;
import app.direksi.hras.model.ResponseDataEmployeeIDData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhimaz on 29/03/2019.
 */

public class ProfilFragmentActivity extends Fragment {

    private TextView txtNama, txtJabatan, txtEmail, txtPhone, txtAddress,
            txtNIK, txtDob, textNoInternet, txtJoinDate, txtExpiredDate, txtID, txtNotes, txtJatah, txtRekening;
    private ImageView profile_image;
    private DateFormat sdf, mViewSdf;
    private CardView cardLayout;
    private static final int REQUEST_PHONE_CALL = 1;
    private TextView textCall;
    private ScrollView sv1;
    private ProgressBar loading;
    private List<String> existLoad = new ArrayList<>();
    private ResponseDataEmployeeIDData dataSummary = new ResponseDataEmployeeIDData();

    public static ProfilFragmentActivity newInstance() {
        ProfilFragmentActivity fragment = new ProfilFragmentActivity();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        txtRekening = v.findViewById(R.id.txtRekening);
        txtJatah = v.findViewById(R.id.txtJatah);
        txtNotes = v.findViewById(R.id.txtNotes);
        txtID = v.findViewById(R.id.txtID);
        txtDob = v.findViewById(R.id.txtDob);
        txtNama = v.findViewById(R.id.txtNama);
        txtJabatan = v.findViewById(R.id.txtJabatan);
        txtEmail = v.findViewById(R.id.txtEmail);
        txtPhone = v.findViewById(R.id.txtPhone);
        txtAddress = v.findViewById(R.id.txtAddress);
        txtNIK = v.findViewById(R.id.txtNIK);
        profile_image = v.findViewById(R.id.profile_image);
        cardLayout = v.findViewById(R.id.cardLayout);
        textCall = v.findViewById(R.id.textCall);

        txtJoinDate = v.findViewById(R.id.txtJoinDate);
        txtExpiredDate = v.findViewById(R.id.txtExpiredDate);

        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        sv1 = (ScrollView) v.findViewById(R.id.sv1);
        sv1.setVisibility(View.GONE);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        checkProfile();
    }

    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getActivity().startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(getActivity(), "Tidak terinstall WA",
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
           // Toast.makeText(getActivity(), "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }

    public void checkProfile() {


        if ( existLoad.size() > 0)
        {


            sv1.setVisibility(View.VISIBLE);
            loading.setVisibility(View.GONE);
            textNoInternet.setVisibility(View.GONE);


            String nama = dataSummary.getFirstName()==null ? "-":dataSummary.getFirstName() + " " + dataSummary.getLastName();
            String role = dataSummary.getGender()== null? "-": dataSummary.getGender();
            String email = dataSummary.getEmail()== null? "-":dataSummary.getEmail();
            String phone = dataSummary.getPhone()== null? "-": dataSummary.getPhone();
            String alamat = dataSummary.getAddress()== null? "-": dataSummary.getAddress();
            String nik = dataSummary.getNik()==null? "-": dataSummary.getNik();
            String url = dataSummary.getPhotoUrl() == null ? "-" : dataSummary.getPhotoUrl();
            String dob = dataSummary.getDob() == null ? "-" : dataSummary.getDob();
            String joinDate = dataSummary.getJoinDate() == null ? "-" : dataSummary.getJoinDate();
            String expiredDate = dataSummary.getContractExpiredDate() == null ? "-" : dataSummary.getContractExpiredDate();
            String ID = dataSummary.getEmployeeID().toString()==null? "-": dataSummary.getEmployeeID().toString();
            String Note = dataSummary.getNotes()==null? "": dataSummary.getNotes();
            String jatah = dataSummary.getSisaCuti() == null? "" : dataSummary.getSisaCuti() + " " + getResources().getString(R.string.title_hari);
            String rekening = dataSummary.getBankName()==null? "": dataSummary.getBankName() + " - " + dataSummary.getBankNumber() + "  \n" + dataSummary.getBankAccountName();


            txtNama.setText(nama);
            txtJabatan.setText(role);
            txtEmail.setText(email);
            txtPhone.setText(phone);
            txtAddress.setText(alamat);
            txtNIK.setText(nik);
            txtID.setText(ID);
            txtNotes.setText(Note);
            txtJatah.setText(jatah);
            txtRekening.setText(rekening);


            if(joinDate.equals("-")){

            } else {

                try {
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date a = dateFormat.parse(joinDate);

                    txtJoinDate.setText(mViewSdf.format(a.getTime()));
                } catch (Exception e){

                }

            }

            if(expiredDate.equals("-")){

            } else {

                try {
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date a = dateFormat.parse(expiredDate);

                    txtExpiredDate.setText(mViewSdf.format(a.getTime()));
                } catch (Exception e){

                }

            }


            if(dob.equals("-")){

            } else {

                try {
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                    Date a = dateFormat.parse(dob);

                    txtDob.setText(mViewSdf.format(a.getTime()));
                } catch (Exception e){

                }

            }




            if (url.equals("-") || url.equals(getContext().getResources().getString(R.string.base_url))){

                Glide.with(getContext())
                        .load(getContext().getResources().getDrawable(R.drawable.profile))
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);

            }else {
                RequestOptions requestOptions = new RequestOptions();
                //  requestOptions.placeholder(R.drawable.warnawarni);
                requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
                requestOptions.skipMemoryCache(true);

                Glide.with(getContext())
                        .load(( url))
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);

                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getActivity(), ZoomPhotoActivity.class);
                        intent.putExtra("image", url);
                        startActivity(intent);
                    }
                });
            }




            if (phone.equals("") || phone == null) {
                textCall.setVisibility(View.GONE);

            } else{
                textCall.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                    @Override
                    public void onClick(View v) {
                        final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                        CharSequence colors[] = new CharSequence[]{getResources().getString(R.string.title_panggil), getResources().getString(R.string.title_kirim_sms), getResources().getString(R.string.title_simpan_kontak), getResources().getString(R.string.title_chat_wa)};

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(getResources().getString(R.string.title_aksi));
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // Check Permissions Now
                                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone.toString().trim()));
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(intent);
                                    }
                                } else if (which == 1) {
                                    Uri uri = Uri.parse("smsto:" + phone.toString().trim());
                                    Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                    mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(mSendSms);
                                } else if (which == 2) {
                                    String name = nama;
//                            String email = ReportResult.get(position).getEmail();
                                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                    contactIntent
                                            .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                            .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                    ((Activity) context).startActivityForResult(contactIntent, 1);

                                } else if (which == 3) {

                                    voiceCall(phone.toString().trim());
                                }
                            }
                        });
                        builder.show();
                    }
                });
            }

        }else {

            sv1.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);
            textNoInternet.setVisibility(View.GONE);

            try {

                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_token), ""));

                String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_employeeid_view), ""));




                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseDataEmployeeID> call = api.getEmployeeID(Authorization, id);

                call.enqueue(new Callback<ResponseDataEmployeeID>() {
                    @Override
                    public void onResponse(Call<ResponseDataEmployeeID> call, Response<ResponseDataEmployeeID> response) {
                        if (response.isSuccessful()) {
                            if (getActivity() != null) {

                                sv1.setVisibility(View.VISIBLE);
                                loading.setVisibility(View.GONE);
                                textNoInternet.setVisibility(View.GONE);
                                existLoad.add("Reload");

                                dataSummary = response.body().getData();


                                String nama = dataSummary.getFirstName()==null ? "-":dataSummary.getFirstName() + " " + dataSummary.getLastName();
                                String role = dataSummary.getGender()== null? "-": dataSummary.getGender();
                                String email = dataSummary.getEmail()== null? "-":dataSummary.getEmail();
                                String phone = dataSummary.getPhone()== null? "-": dataSummary.getPhone();
                                String alamat = dataSummary.getAddress()== null? "-": dataSummary.getAddress();
                                String nik = dataSummary.getNik()==null? "-": dataSummary.getNik();
                                String url = dataSummary.getPhotoUrl() == null ? "-" : dataSummary.getPhotoUrl();
                                String dob = dataSummary.getDob() == null ? "-" : dataSummary.getDob();
                                String joinDate = dataSummary.getJoinDate() == null ? "-" : dataSummary.getJoinDate();
                                String expiredDate = dataSummary.getContractExpiredDate() == null ? "-" : dataSummary.getContractExpiredDate();
                                String ID = dataSummary.getEmployeeID().toString()==null? "-": dataSummary.getEmployeeID().toString();
                                String Note = dataSummary.getNotes()==null? "": dataSummary.getNotes();
                                String jatah = dataSummary.getSisaCuti() == null? "" : dataSummary.getSisaCuti() + " " + getResources().getString(R.string.title_hari);
                                String rekening = "";
                                if (dataSummary.getBankName()!=null && dataSummary.getBankNumber()!=null && dataSummary.getBankAccountName()!=null) {
                                    rekening =  dataSummary.getBankName() + " - " + dataSummary.getBankNumber() + "  \n" + dataSummary.getBankAccountName();
                                }

                                txtNama.setText(nama);
                                txtJabatan.setText(role);
                                txtEmail.setText(email);
                                txtPhone.setText(phone);
                                txtAddress.setText(alamat);
                                txtNIK.setText(nik);
                                txtID.setText(ID);
                                txtNotes.setText(Note);
                                txtJatah.setText(jatah);
                                txtRekening.setText(rekening);

                                if(joinDate.equals("-")){

                                } else {

                                    try {
                                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                        Date a = dateFormat.parse(joinDate);

                                        txtJoinDate.setText(mViewSdf.format(a.getTime()));
                                    } catch (Exception e){

                                    }

                                }

                                if(expiredDate.equals("-")){

                                   // txtExpiredDate.setText("[TETAP]");

                                } else {

                                    try {
                                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                        Date a = dateFormat.parse(expiredDate);

                                        txtExpiredDate.setText(mViewSdf.format(a.getTime()));
                                    } catch (Exception e){

                                    }

                                }


                                if(dob.equals("-")){

                                } else {

                                    try {
                                        sdf = new SimpleDateFormat("yyyy-MM-dd");
                                        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                        Date a = dateFormat.parse(dob);

                                        txtDob.setText(mViewSdf.format(a.getTime()));
                                    } catch (Exception e){

                                    }

                                }




                                if (url.equals("-") || url.equals(getContext().getResources().getString(R.string.base_url))){

                                    Glide.with(getContext())
                                            .load(getContext().getResources().getDrawable(R.drawable.profile))
                                            .error(Glide.with(profile_image).load(R.drawable.profile))
                                            .into(profile_image);

                                }else {
                                    RequestOptions requestOptions = new RequestOptions();
                                    //  requestOptions.placeholder(R.drawable.warnawarni);
                                    requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
                                    requestOptions.skipMemoryCache(true);

                                    Glide.with(getContext())
                                            .load(( url))
                                            .error(Glide.with(profile_image).load(R.drawable.profile))
                                            .into(profile_image);

                                    profile_image.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            Intent intent = new Intent(getActivity(), ZoomPhotoActivity.class);
                                            intent.putExtra("image", url);
                                            startActivity(intent);
                                        }
                                    });
                                }




                                if (phone.equals("") || phone == null) {
                                    textCall.setVisibility(View.GONE);

                                } else{
                                    textCall.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                                        @Override
                                        public void onClick(View v) {
                                            final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                                            CharSequence colors[] = new CharSequence[]{getResources().getString(R.string.title_panggil), getResources().getString(R.string.title_kirim_sms), getResources().getString(R.string.title_simpan_kontak), getResources().getString(R.string.title_chat_wa)};

                                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                            builder.setTitle(getResources().getString(R.string.title_aksi));
                                            builder.setItems(colors, new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    if (which == 0) {
                                                        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                            // Check Permissions Now
                                                            ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                                        } else {
                                                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone.toString().trim()));
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                            context.getApplicationContext().startActivity(intent);
                                                        }
                                                    } else if (which == 1) {
                                                        Uri uri = Uri.parse("smsto:" + phone.toString().trim());
                                                        Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                                        mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                        context.getApplicationContext().startActivity(mSendSms);
                                                    } else if (which == 2) {
                                                        String name = nama;
//                            String email = ReportResult.get(position).getEmail();
                                                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                                        contactIntent
                                                                .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                                                .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                                        ((Activity) context).startActivityForResult(contactIntent, 1);

                                                    } else if (which == 3) {

                                                        voiceCall(phone.toString().trim());
                                                    }
                                                }
                                            });
                                            builder.show();
                                        }
                                    });
                                }


                            }


                        } else {
                            loading.setVisibility(View.GONE);
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e)
                            {

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataEmployeeID> call, Throwable t) {
                        loading.setVisibility(View.GONE);
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                           // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                    }
                });
            } catch (Exception e) {
                loading.setVisibility(View.GONE);
                try {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                   // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                }
                catch (Exception se)
                {

                }
            }
        }


    }




}
