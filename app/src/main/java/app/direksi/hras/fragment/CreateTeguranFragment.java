package app.direksi.hras.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;

import com.canhub.cropper.CropImage;
import com.google.android.material.textfield.TextInputLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.google.gson.Gson;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.DetailLemburKuActivity;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.WarningLetterActivity;
import app.direksi.hras.adapter.LastReprimandAdapter;
import app.direksi.hras.adapter.LemburAdapter;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.DataFetchEmployeeItem;
import app.direksi.hras.model.DataLastReprimand;
import app.direksi.hras.model.DataLembur;
import app.direksi.hras.model.ResponseCreateTeguran;
import app.direksi.hras.model.ResponseDataAttendance;
import app.direksi.hras.model.ResponseDataLastReprimand;
import app.direksi.hras.model.ResponseFetchEmployee;
import app.direksi.hras.model.ResponseKategori;
import app.direksi.hras.util.CompressImage;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseEmploye;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_1;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_2;
import static app.direksi.hras.app.AppConstants.SIZE_COMPRESS_3;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 26/02/2019.
 */

public class CreateTeguranFragment extends DialogFragment implements View.OnClickListener , AdapterView.OnItemSelectedListener{

    private static final String TAG = CreateTeguranFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;
    private SearchableSpinner spinnerSearchable;
    private ArrayAdapter<String> adapter;
    private List<String> data;
    private int check = 0;
    TextInputLayout mLayoutSelesai, mLayoutMulai;
    EditText mEdtSelesai, mEdtMulai,
            mKeperluan,
            mEdtNote,
            txtCatatan;
    private List<DataFetchEmployeeItem> dataFetchEmployeeItems = new ArrayList<>();
    private String mIdEmployee = "";

    TextInputLayout mReason;

    private Long mIdConstanta[], mIdEmploye[];
    private String mNamaConstanta[], mNamaEmployee[];
    private Button mButtonSend;
    private Long constanta, idEmployee;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri;
    private SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private Boolean flag = false;
    private ImageView imgClose;

    private TextView txtLastTitle ;
    private File file;

    private RecyclerView mRootView;
    private LastReprimandAdapter cAdapter;
    private List<DataLastReprimand> dataList = new ArrayList<>();
    private LinearLayout llLast;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {



        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_teguran, container, false);
        imgClose = rootView.findViewById(R.id.imgClose);
        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        txtCatatan = rootView.findViewById(R.id.txtCatatan);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);

        mRootView = (RecyclerView) rootView.findViewById(R.id.my_recycler_view);
        llLast = rootView.findViewById(R.id.llLast);
        llLast.setVisibility(View.GONE);
        txtLastTitle = rootView.findViewById(R.id.txtLastTitle);


        spinnerSearchable = (SearchableSpinner) rootView.findViewById(R.id.spinner1);
        spinnerSearchable.setTitle("Pilih Pegawai");
        spinnerSearchable.setOnItemSelectedListener(this);

        messageDialog = new MessageDialog();

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        mLayoutMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mLayoutSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mKeperluan = rootView.findViewById(R.id.mKeperluan);
        mKeperluan.setOnClickListener(this);
        // mEdtNote.setOnClickListener(this);
        mEdtNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataFetchEmployeeItems.clear();
                showListEmployee();
            }
        });
        mButtonSend.setOnClickListener(this);
        imgClose.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        /* loading.show();*//*
        mGetEmploye();
        mGetConstanta();*/

        cAdapter = new LastReprimandAdapter(dataList, getActivity());
        mRootView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRootView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
       // mRootView.setItemAnimator(new DefaultItemAnimator());
       // RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

       // DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
       // itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

        //  mRootView.addItemDecoration(itemDecorator);
        mRootView.addItemDecoration(new DividerItemDecoration(this.getActivity(), LinearLayout.VERTICAL));

        mRootView.setAdapter(cAdapter);
        mRootView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRootView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    private void startCropImageActivity() {
        CropImage.activity()
                .start(getActivity(), this);
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {

                try {
                    InputStream inputStream = getActivity().getContentResolver().openInputStream(result.getUri());
                    Bitmap mImageBitmap = BitmapFactory.decodeStream(inputStream);
                    int width = 0;
                    int height = 0;
                    try {


                        if (mImageBitmap.getWidth() > 2000 || mImageBitmap.getHeight() > 2000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_1 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_1 / 100;

                        } else if (mImageBitmap.getWidth() > 1000 || mImageBitmap.getHeight() > 1000){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_2 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_2 / 100;

                        } else if (mImageBitmap.getWidth() > 500 || mImageBitmap.getHeight() > 500){

                            width = mImageBitmap.getWidth() * SIZE_COMPRESS_3 / 100;
                            height = mImageBitmap.getHeight() * SIZE_COMPRESS_3 / 100;

                        } else {

                            width = mImageBitmap.getWidth();
                            height = mImageBitmap.getHeight();
                        }

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(mImageBitmap, width, height, true);
                        //  Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
                        // Uri uri = CompressImage.getImageUri(getActivity(), scaledBitmap);
                        String urr = CompressImage.makeBitmapPath(getActivity(), scaledBitmap);

                        file = new File (urr);
                        returnUri = result.getUri();

                        mImageChoose.setVisibility(View.GONE);
                        mImageView.setVisibility(View.VISIBLE);
                        mImageView.setImageBitmap(scaledBitmap);

                    } catch (Exception e){

                        try {
                            String urr = CompressImage.makeBitmapPath(getActivity(), mImageBitmap);
                            file = new File (urr);
                            returnUri = result.getUri();

                            mImageChoose.setVisibility(View.GONE);
                            mImageView.setVisibility(View.VISIBLE);
                            mImageView.setImageURI(result.getUri());

                        } catch (Exception f){
                            MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                            //Toast.makeText(getActivity(), "failed", Toast.LENGTH_LONG).show();
                        }


                    } finally {
                        try {
                            inputStream.close();

                        } catch (Exception e){

                        }

                    }

                } catch (IOException e){
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_memuat_foto),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

            }
        }
    }


    private void openDateRangePicker() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEndDate = sdf.format(selectedDate.getEndDate().getTime());



                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));
                mEdtSelesai.setText(mViewSdf.format(selectedDate.getEndDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(true);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseKategori> call = api.getCategory(Authorization);

            call.enqueue(new Callback<ResponseKategori>() {
                @Override
                public void onResponse(Call<ResponseKategori> call, Response<ResponseKategori> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {
                                mIdConstanta = new Long[response.body().getData().size()];
                                mNamaConstanta = new String[response.body().getData().size()];

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mIdConstanta[i] = response.body().getData().get(i).getReprimandCategoryID();
                                    mNamaConstanta[i] = response.body().getData().get(i).getName() +  " ( "+ response.body().getData().get(i).getNote() + " )";
                                }
                                loading.hide();

                            } else {
                                loading.hide();
                                mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                        , response.body().getErrMessage());
                            }

                        } else {
                            loading.hide();
                            mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.hide();
                        mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                , response.body().getErrMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseKategori> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        if ( flag ) {

        } else {
            loading.show();
            mGetConstanta();
            flag = true;
        }





    }

    public void mGetEmploye() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseEmploye> call = api.getEmploye(Authorization);

            call.enqueue(new Callback<ResponseEmploye>() {
                @Override
                public void onResponse(Call<ResponseEmploye> call, Response<ResponseEmploye> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {
                                mIdEmploye = new Long[response.body().getData().size()];
                                mNamaEmployee = new String[response.body().getData().size()];

                                data = new ArrayList<>();

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mIdEmploye[i] = response.body().getData().get(i).getEmployeeID();
                                    mNamaEmployee[i] = response.body().getData().get(i).getFirstName() +  " "+ response.body().getData().get(i).getLastName();
                                    data.add (response.body().getData().get(i).getFirstName() +  " "+ response.body().getData().get(i).getLastName());
                                }
                                adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, data);
                                spinnerSearchable.setAdapter(adapter);

                                mGetConstanta();

                            } else {
                                loading.hide();
                                mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                        , response.body().getErrMessage());
                            }

                        } else {
                            loading.hide();
                            mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.hide();
                        mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                , response.body().getErrMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseEmploye> call, Throwable t) {
                    loading.hide();
                    mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.hide();
            mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }
    }

    public void mCreateTeguran() {


        loading.show();
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);

        RequestBody mBodyConstanta = RequestBody.create(MediaType.parse("text/plain"), constanta.toString());
        RequestBody EmployeeID = RequestBody.create(MediaType.parse("text/plain"), mIdEmployee);
        RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), formattedDate);
        RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(txtCatatan));

        MultipartBody.Part multipart = null;

        //File creating from selected URL
        if (returnUri != null) {
           // File file = new File(CompressImage.getRealPathFromURI(returnUri, getActivity()));
            multipart = null;
            if (file.exists()) {
                String name = URLConnection.guessContentTypeFromName(file.getName());
                RequestBody requestFile = RequestBody.create(MediaType.parse(name), file);
                multipart = MultipartBody.Part.createFormData("reprimandFile", file.getName(), requestFile);
            }
        }

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseCreateTeguran> call = api.createTeguran(Authorization, mBodyConstanta, EmployeeID, Date, Note, multipart);

        call.enqueue(new Callback<ResponseCreateTeguran>() {
            @Override
            public void onResponse(Call<ResponseCreateTeguran> call, Response<ResponseCreateTeguran> response) {

                Log.v(TAG, response.toString());

                if (response.isSuccessful()) {
                    long statusCode = response.body().getErrCode();

                    if (statusCode == 0) {

                        loading.hide();


                        SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                        alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                        alertDialog.setContentText("");
                        alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                Intent login = new Intent(getActivity(), WarningLetterActivity.class);
                                login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(login);
                            }
                        });
                        alertDialog.show();

                        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        btn.setTextColor(getResources().getColor(R.color.colorWhite));

                      /*  new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("SUKSES")
                                .setContentText("")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        Intent login = new Intent(getActivity(), WarningLetterActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);
                                    }
                                })
                                .show();*/

                        /* dismiss();*/
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.teguran_success), Toast.LENGTH_SHORT).show();

                    } else {

                        try {
                            loading.hide();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception e){

                        }

                    }

                } else {
                    try {
                        loading.hide();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception e){

                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseCreateTeguran> call, Throwable t) {
                try {
                    loading.hide();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    redirected();
                } catch (Exception e){

                }

            }
        });

    }

    private void mAttemptCreateDayOff() {

        mEdtMulai.setError(null);
        mEdtSelesai.setError(null);
        mKeperluan.setError(null);
        mEdtNote.setError(null);
        txtCatatan.setError(null);


        String start = mEdtMulai.getText().toString();
        String end = mEdtSelesai.getText().toString();
        String reason = mKeperluan.getText().toString();
        String note = mEdtNote.getText().toString();
        String catatan = txtCatatan.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
       /* if (TextUtils.isEmpty(start)) {
            mEdtMulai.setError(getString(R.string.start_date_blank));
            focusView = mEdtMulai;
            cancel = true;
        }

        if (TextUtils.isEmpty(end)) {
            mEdtSelesai.setError(getString(R.string.end_date_blank));
            focusView = mEdtSelesai;
            cancel = true;
        }*/

        if (TextUtils.isEmpty(reason)) {
            mKeperluan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mKeperluan;
            cancel = true;
        }
        if (TextUtils.isEmpty(catatan)) {
            txtCatatan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = txtCatatan;
            cancel = true;
        }

        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            mCreateTeguran();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mKeperluan) {
            try {
                if (mIdConstanta.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mKeperluan.setText(mNamaConstanta[item]);

                            constanta = mIdConstanta[item];


                            imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        } else if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        }  /*else if (v.getId() == R.id.mEdtNote) {
            MotionEvent motionEvent = MotionEvent.obtain( 0, 0, MotionEvent.ACTION_UP, 0, 0, 0 );
            spinnerSearchable.dispatchTouchEvent(motionEvent);
        }*/  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = parent.getItemAtPosition(position).toString();

        if (check == 0)
        {
            check = 1;
            //  mEdtNote.setText(selectedItem);
        }
        else
        {
            idEmployee = mIdEmploye[position];
            // Toast.makeText(getActivity(), idEmployee.toString(), Toast.LENGTH_SHORT).show();
            // getLast(idEmployee);
            mEdtNote.setText(selectedItem);
        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void mShowMessageError(Activity activity, String title, String message) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                        Intent login = new Intent(getActivity(), WarningLetterActivity.class);
                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                    }
                })
                .show();
    }

    public void mShowMessageErrorSave(Activity activity, String title, String message) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }

    /*public void getLast(String id) {

        loading.show();

        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", id);



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataLastReprimand> call = api.getLastReprimand(Authorization, data);

            call.enqueue(new Callback<ResponseDataLastReprimand>() {
                @Override
                public void onResponse(Call<ResponseDataLastReprimand> call, Response<ResponseDataLastReprimand> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            llLast.setVisibility(View.VISIBLE);

                            if (response.body().getData() != null){

                                tbLast.setVisibility(View.VISIBLE);
                                txtLastTitle.setText(getResources().getString(R.string.title_sp_terakhir));

                                if (response.body().getData().getDate() != null)
                                    txtTanggalLast.setText(DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getDate()));

                                txtKategoriLast.setText(response.body().getData().getReprimandCategory().getName());
                                txtCreatorLast.setText(response.body().getData().getIssuer().getFirstName() + " " + response.body().getData().getIssuer().getLastName());
                                txtCatatanLast.setText(response.body().getData().getNote());

                            }else {
                                txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_sp));
                                tbLast.setVisibility(View.GONE);
                            }
                            loading.hide();




                        } else {
                            llLast.setVisibility(View.VISIBLE);
                            loading.hide();
                            txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_sp));
                            tbLast.setVisibility(View.GONE);
                        }

                    } else {
                        llLast.setVisibility(View.VISIBLE);
                        loading.hide();
                        txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_sp));
                        tbLast.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataLastReprimand> call, Throwable t) {
                    loading.hide();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            loading.hide();
            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
        }
    }*/

    public void getLast(String id) {
        dataList.clear();
        loading.show();

        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataLastReprimand> call = api.getLastReprimand(Authorization, data);

            call.enqueue(new Callback<ResponseDataLastReprimand>() {
                @Override
                public void onResponse(Call<ResponseDataLastReprimand> call, Response<ResponseDataLastReprimand> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            llLast.setVisibility(View.VISIBLE);

                            if (response.body().getData().size() > 0){


                                if (response.body().getData().size() > 1){

                                     txtLastTitle.setText(getResources().getString(R.string.title_sp_terakhir));
                                     dataList.clear();
                                     dataList.addAll(response.body().getData());
                                     cAdapter.notifyDataSetChanged();

                                    final float scale = getActivity().getResources().getDisplayMetrics().density;
                                    int pixels = (int) (200 * scale + 0.5f);
                                    RelativeLayout.LayoutParams lp =
                                            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, pixels);
                                    mRootView.setLayoutParams(lp);

                                } else {

                                     txtLastTitle.setText(getResources().getString(R.string.title_sp_terakhir));
                                     dataList.clear();
                                     dataList.addAll(response.body().getData());
                                     cAdapter.notifyDataSetChanged();

                                    final float scale = getActivity().getResources().getDisplayMetrics().density;
                                    int pixels = (int) (130 * scale + 0.5f);
                                    RelativeLayout.LayoutParams lp =
                                            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, pixels);
                                    mRootView.setLayoutParams(lp);

                                }


                            }else {
                                txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_sp));
                                RelativeLayout.LayoutParams lp =
                                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                mRootView.setLayoutParams(lp);
                            }
                            loading.hide();




                        } else {
                            llLast.setVisibility(View.VISIBLE);
                            loading.hide();
                            txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_sp));
                            RelativeLayout.LayoutParams lp =
                                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                            mRootView.setLayoutParams(lp);
                        }

                    } else {
                        llLast.setVisibility(View.VISIBLE);
                        loading.hide();
                        txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_sp));
                        RelativeLayout.LayoutParams lp =
                                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        mRootView.setLayoutParams(lp);
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataLastReprimand> call, Throwable t) {
                    loading.hide();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            loading.hide();
            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
        }
    }

    private void showListEmployee() {


        final Dialog overlayInfo = new Dialog(getActivity());
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        overlayInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayInfo.setCancelable(true);
        final View view = inflater.inflate(R.layout.dialog_choose_emp, null);

        final EditText edtSearch = (EditText) view.findViewById(R.id.edtSearch);
        final Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        final TextView txtEmptyList = (TextView) view.findViewById(R.id.txtEmptyList);
        final ProgressBar loadingTemp = (ProgressBar) view.findViewById(R.id.loadingTemp);
        loadingTemp.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        final LinearLayout containerDialogAddress = (LinearLayout) view.findViewById(R.id.containerCleaner);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txtEmptyList.setVisibility(View.GONE);
                    loadingTemp.setVisibility(View.VISIBLE);
                    dataFetchEmployeeItems.clear();
                    containerDialogAddress.removeAllViews();

                    Map<String, String> data = new HashMap<>();
                    data.put("q", edtSearch.getText().toString().toLowerCase().trim());

                    String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_token), ""));


                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(getResources().getString(R.string.base_url))
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                    // simplified call to request the news with already initialized service
                    Call<ResponseFetchEmployee> call = api.mGetListEmpReprimands(Authorization, data);

                    call.enqueue(new Callback<ResponseFetchEmployee>() {
                        @Override
                        public void onResponse(Call<ResponseFetchEmployee> call, Response<ResponseFetchEmployee> response) {


                            if (response.isSuccessful()) {
                                int statusCode = response.body().getErrCode();
                                loadingTemp.setVisibility(View.GONE);
                                if (statusCode == 0) {

                                    Log.e(TAG, response.toString());

                                    dataFetchEmployeeItems = response.body().getDataFetchEmployee();


                                    for (int i = 0; i < dataFetchEmployeeItems.size(); i++) {
                                        final LayoutInflater layoutInflaterDetail = (LayoutInflater)
                                                getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        final View addDetail = layoutInflaterDetail.inflate(R.layout.item_emp, null);

                                        final TextView mTxtName = addDetail.findViewById(R.id.mTxtName);
                                        final TextView mTxtHidden = addDetail.findViewById(R.id.mTxtHidden);

                                        mTxtName.setText(dataFetchEmployeeItems.get(i).getFullNameNik() + "\n" + dataFetchEmployeeItems.get(i).getOrganizationName());
                                        mTxtHidden.setText(Integer.toString(dataFetchEmployeeItems.get(i).getEmployeeID()));
                                        mTxtName.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mIdEmployee = mTxtHidden.getText().toString();
                                                getLast(mIdEmployee);
                                                //  Toast.makeText(getActivity(), mIdEmployee, Toast.LENGTH_SHORT).show();
                                                mEdtNote.setText(mTxtName.getText().toString());
                                                overlayInfo.dismiss();

                                            }
                                        });

                                        containerDialogAddress.addView(addDetail);
                                    }


                                    loadingTemp.setVisibility(View.GONE);
                                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);

                                } else {
                                    loadingTemp.setVisibility(View.GONE);
                                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                                }

                            } else {
                                loadingTemp.setVisibility(View.GONE);
                                txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseFetchEmployee> call, Throwable t) {
                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                        }
                    });


                } catch (Exception e) {
                    loadingTemp.setVisibility(View.GONE);
                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                }
            }
        });


        overlayInfo.setContentView(view);
        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(overlayInfo.getWindow().getAttributes());
        lp.width = width - 30;
        overlayInfo.show();
        overlayInfo.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    void redirected (){

        Intent login = new Intent(getActivity(), WarningLetterActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }

}