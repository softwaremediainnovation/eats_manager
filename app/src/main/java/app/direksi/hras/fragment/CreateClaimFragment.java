package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseConstanta;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by warnawarni on 8/31/2017.
 */

public class CreateClaimFragment extends DialogFragment implements View.OnClickListener {

    private Integer mIdConstanta[];
    private String mNamaConstanta[];
    private EditText mKeperluan;
    private ImageView imgClose;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {


        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_claim_fragment, container, false);


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        imgClose = rootView.findViewById(R.id.imgClose);
        mKeperluan = rootView.findViewById(R.id.mKeperluan);
        mKeperluan.setOnClickListener(this);
        imgClose.setOnClickListener(this);

        return rootView;
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseConstanta> call = api.mGetConstanta(Authorization);

            call.enqueue(new Callback<ResponseConstanta>() {
                @Override
                public void onResponse(Call<ResponseConstanta> call, Response<ResponseConstanta> response) {


                    if (response.isSuccessful()) {
                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getDataConstanta().size() > 0) {
                                mIdConstanta = new Integer[response.body().getDataConstanta().size()];
                                mNamaConstanta = new String[response.body().getDataConstanta().size()];

                                for (int i = 0; i < response.body().getDataConstanta().size(); i++) {
                                    mIdConstanta[i] = response.body().getDataConstanta().get(i).getConstantaID();
                                    mNamaConstanta[i] = response.body().getDataConstanta().get(i).getName();
                                }

                            }

                        }

                    }
                }

                @Override
                public void onFailure(Call<ResponseConstanta> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mKeperluan) {
            try {
                if (mIdConstanta.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mKeperluan.setText(mNamaConstanta[item]);

                            imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }

    }


}