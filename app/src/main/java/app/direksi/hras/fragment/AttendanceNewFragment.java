package app.direksi.hras.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.CheckinActivity;
import app.direksi.hras.FingerprintHandler;
import app.direksi.hras.HomeActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataCompany;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.CustomDigitalClock;
import app.direksi.hras.util.DefaultFormatter;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.content.Context.FINGERPRINT_SERVICE;
import static android.content.Context.KEYGUARD_SERVICE;
import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 23/05/2019.
 */

public class AttendanceNewFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = AttendanceNewFragment.class.getSimpleName();
    private KeyStore keyStore;
    // Variable used for storing the key in the Android Keystore container
    private static final String KEY_NAME = "androidHive";
    private Cipher cipher;
    private TextView textView, txtDates;

    private GpsLocationReceiver gpsLocationReceiver;
    private NetworkChangeReceiver networkChangeReceiver;


    SweetAlertDialog mAlertDialog, mAlertInternet, mShowDialog;
    View mapView;



    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.USE_FINGERPRINT,
    };
    private PermissionsChecker checker;
    private LayoutInflater mInflater;

    private boolean mIsActiveGPS = false;
    private boolean mIsActiveInternet = true;
    private List<DataCompany> dataCompanies;


    FloatingActionButton fab;
    CustomDigitalClock mDigitalClock;

    private DateFormat sdf, mViewSdf;
    private Date mTimeServer = new Date();

    private Chronometer cronometer;

    private Button mButtonCheckin;
    private LinearLayout mLayoutFinger,
            mLayoutNonFinger;

    private LocationManager manager;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;
    private double latitude = 0, longitude = 0;
    FingerprintManager fingerprintManager;
    FingerprintHandler helper;
    private ScrollView activity_fingerprint;
    SweetAlertDialog loading;
    private float distance = 0;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checker = new PermissionsChecker(getActivity());


        if (checker.lacksPermissions(PERMISSIONS)) {
            startPermissionsActivity(PERMISSIONS);
        }


    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup viewGroup = (ViewGroup) view;


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_fingerprint, null);
        mInflater = inflater;

     /*   Typeface font2 = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Poppins.otf");
        SpannableStringBuilder SS = new SpannableStringBuilder(getResources().getString(R.string.title_time_server));
        SS.setSpan(new HrsApp.CustomTypefaceSpan("", font2), 0, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle(SS);*/
        txtDates = v.findViewById(R.id.txtDates);
        textView = v.findViewById(R.id.errorText);
        fab = v.findViewById(R.id.fab);
        //mDigitalClock = v.findViewById(R.id.mDigitalClock);

        cronometer = v.findViewById(R.id.cronometer);
        //    activity_fingerprint = v.findViewById(R.id.activity_fingerprint);
        textView = v.findViewById(R.id.errorText);
        mButtonCheckin = v.findViewById(R.id.mButtonCheckin);

        mLayoutFinger = v.findViewById(R.id.mLayoutFinger);
        mLayoutNonFinger = v.findViewById(R.id.mLayoutNonFinger);


        cronometer = v.findViewById(R.id.cronometer);


        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


       /* fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ListAttendancesActivity.class);
                startActivity(intent);
            }
        });
*/


        manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        gpsLocationReceiver = new GpsLocationReceiver();
        IntentFilter intentFilter = new IntentFilter(getResources().getString(R.string.gps_update_filter));
        getActivity().registerReceiver(gpsLocationReceiver, intentFilter);


        networkChangeReceiver = new NetworkChangeReceiver();
        IntentFilter intentNetworkFilter = new IntentFilter(getResources().getString(R.string.network_updater_filter));
        getActivity().registerReceiver(networkChangeReceiver, intentNetworkFilter);



        mAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_gps))
                .setContentText(getResources().getString(R.string.text_gps_nonactive))
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        try {
                            mIsActiveGPS = false;
                            mAlertDialog.dismiss();
                            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        } catch (Exception e){

                        }
                    }
                });
        mAlertDialog.setCancelable(false);

        mAlertInternet = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_internet))
                .setContentText(getResources().getString(R.string.text_internet_nonactive))
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        try {
                            mIsActiveInternet = true;
                            mAlertInternet.dismiss();
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.android.phone", "com.android.phone.MobileNetworkSettings"));
                            startActivity(intent);
                        } catch (Exception e){

                        }
                    }
                });

        mAlertInternet.setCancelable(false);


        /*if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {

            mLayoutFinger.setVisibility(View.GONE);
            mLayoutNonFinger.setVisibility(View.VISIBLE);
            mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                    mIntent.putExtra("isFragment", true);
                    startActivity(mIntent);
                    getActivity().finish();
                }
            });

        } else {


             fingerprintManager = null;

            // Initializing both Android Keyguard Manager and Fingerprint Manager
            KeyguardManager  keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);
            }

            // Check whether the device has a Fingerprint sensor.
            if (!fingerprintManager.isHardwareDetected()) {
                // mShowDialog("Device anda tidak support fingerprint", 0);
                mLayoutFinger.setVisibility(View.GONE);
                mLayoutNonFinger.setVisibility(View.VISIBLE);
                mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                        mIntent.putExtra("isFragment", true);
                        startActivity(mIntent);
                        getActivity().finish();
                    }
                });
            } else {

                mLayoutFinger.setVisibility(View.VISIBLE);
                mLayoutNonFinger.setVisibility(View.GONE);

                // Checks whether fingerprint permission is set on manifest
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    mShowDialog("Izin otentikasi sidik jari tidak diaktifkan", 0);
                } else {
                    // Check whether at least one fingerprint is registered
                    if (!fingerprintManager.hasEnrolledFingerprints()) {
                        mShowDialog("Daftarkan setidaknya satu sidik jari di Pengaturan", 0);
                    } else {
                        // Checks whether lock screen security is enabled or not
                        if (!keyguardManager.isKeyguardSecure()) {
                            mShowDialog("Keamanan layar kunci tidak diaktifkan di Pengaturan", 0);
                        } else {
                            generateKey();

                            if (cipherInit()) {
                                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                helper = new FingerprintHandler(getActivity());
                                helper.startAuth(fingerprintManager, cryptoObject, true, dataCompanies);

                                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !mAlertDialog.isShowing()) {
                                    mAlertDialog.show();
                                } else {
                                    mAlertDialog.dismiss();
                                }

                            }

                        }
                    }
                }
            }

        }

        mGetServerTim();*/

        if (checkActiveInternet())
            mAlertInternet.show();


        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

      /*  View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(0, 0, 30, 30);*/

        mLocationCallback = new LocationCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        };

        getMaps();


        return v;
    }

    void getMaps() {

        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Bundle extras = null;
                            if (location != null)
                                extras = location.getExtras();
                            boolean isMockLocation = extras != null && extras.getBoolean(FusedLocationProviderApi.KEY_MOCK_LOCATION, false);

                            if (isMockLocation) {
                                mShowDialog("Anda menggunakan lokasi palsu, silahkan matikan lokasi palsu anda", 2);
                                return;
                            }

                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();

                                mLoadMaps();


                            } else {
                                getLastLocation();
                            }


                        }


                    });
            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });


            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });

            mFusedLocationClient.getLastLocation().addOnFailureListener(getActivity(), new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });


        } catch (Exception e) {
            Log.e("FingerprintHandler", e.getMessage());
        }
    }


    public void mLoadMaps() {

//error saat java.lang.String android.content.Context.getPackageName()' on a null object reference

        int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_radius), ""));

        if (getContext() != null) {
            String latt = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_latitude), ""));

            String longi = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_longitude), ""));

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            try {

                if ((latt.equals("") && longi.equals(""))) {

                } else {

                    int height = SIZE_MARKER;
                    int width = SIZE_MARKER;
                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_kantor);
                    Bitmap b = bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth() * width / 100, bitmapdraw.getBitmap().getHeight() * height / 100, false);


                    MarkerOptions marker = new MarkerOptions().position(new LatLng(Double.parseDouble(latt), Double.parseDouble(longi)))
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                            .title(getResources().getString(R.string.title_lokasi_kantor));
                    mMap.addMarker(marker);
                    builder.include(marker.getPosition());

             /*   mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(latt), Double.parseDouble(longi)))
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                        .title("Lokasi kantor"));*/

                    Circle circle = mMap.addCircle(new CircleOptions()
                            .center(new LatLng(Double.parseDouble(latt), Double.parseDouble(longi)))
                            .radius(radius)
                            .strokeColor(Color.parseColor("#20ff5252"))
                            .strokeWidth(3)
                            .fillColor(Color.parseColor("#50ff5252")));

                    Location loc1 = new Location("");
                    loc1.setLatitude(latitude);
                    loc1.setLongitude(longitude);


                    Location loc2 = new Location("");
                    loc2.setLatitude(Double.parseDouble(latt));
                    loc2.setLongitude(Double.parseDouble(longi));
                    distance = loc1.distanceTo(loc2);


                    if (distance <= radius) {

                    } else {

                    }
                }
            } catch (Exception e) {

            }

            int height = SIZE_MARKER;
            int width = SIZE_MARKER;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_user);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth() * width / 100, bitmapdraw.getBitmap().getHeight() * height / 100, false);

            MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude))
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                    .title(getResources().getString(R.string.title_lokasi_ku));
            mMap.addMarker(marker);
            builder.include(marker.getPosition());

            int swidth = getResources().getDisplayMetrics().widthPixels;
            int sheight = getResources().getDisplayMetrics().heightPixels;
            int spadding = (int) (swidth * 0.20);


            if ((latt.equals("") && longi.equals(""))) {

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                        .zoom(18)                   // Sets the zoom
                        .bearing(0)                // Sets the orientation of the camera to east
                        .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            } else {

                if (distance <= radius) {

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                            .zoom(18)                   // Sets the zoom
                            .bearing(0)                // Sets the orientation of the camera to east
                            .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), swidth, sheight, spadding));
                }

            }
        }


        // mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), swidth, sheight, spadding));

     /*   mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                .title("Lokasi Ku"));*/

        //  mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
      /*  CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                .zoom(18)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/


    /*    Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(RANGE_DISTANCE)
                .strokeColor(Color.parseColor("#27F57F17"))
                .strokeWidth(3)
                .fillColor(Color.parseColor("#0FF57F17")));*/


    }


    private void getLastLocation() {

        if (getActivity()!= null) {
            LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);


            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                    mLoadMaps();

                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };

            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null);
        }

    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try {
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.my_json_maps));
            if (!isSuccess) {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(),
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            LatLng currentLatLng = new LatLng(-7.265465,
                    112.745543);
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(currentLatLng,
                    3);
            mMap.moveCamera(update);

            if (mapView != null &&
                    mapView.findViewById(Integer.parseInt("1")) != null) {
                // Get the button view
                View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                // and next place it, on bottom right (as Google Maps app)
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                        locationButton.getLayoutParams();
                // position on right bottom
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                layoutParams.setMargins(0, 0, 30, 500);
            }


            map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    // Toast.makeText(getActivity(), "Good", Toast.LENGTH_SHORT).show();

                    try {
                        mMap.clear();
                        getMaps();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_berhasil_update_lokasi), MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();


                    }catch (Exception e){
                        MDToast.makeText(getActivity(), getResources().getString(R.string.title_gagal_update_lokasi), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();


                    }

                    return false;

                }
            });
          /*  mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(-7.257472, 112.752090)));*/
        }
    }


    private final class GpsLocationReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boolean isActiveGps = intent.getBooleanExtra("isActiveGps", false);

                if (!isActiveGps && !mAlertDialog.isShowing()) {
                    mAlertDialog.show();
                    mIsActiveGPS = false;
                    Log.e(TAG, mIsActiveGPS + "");
                } else {
                    mAlertDialog.dismiss();
                    mIsActiveGPS = true;
                    Log.e(TAG, mIsActiveGPS + "");
                }

            } catch (Exception e) {
            }
        }
    }

    private final class NetworkChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                boolean noInternet = intent.getBooleanExtra("no_internet", true);

                if (noInternet && !mAlertInternet.isShowing()) {
                    mAlertInternet.show();
                    mIsActiveInternet = false;
                } else {
                    mAlertInternet.dismiss();
                    mIsActiveInternet = true;
                    mGetServerTim();
                }

            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(gpsLocationReceiver);
        getActivity().unregisterReceiver(networkChangeReceiver);
        super.onDestroy();
    }


    //request permission
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, getActivity());
    }

    //start permission
    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }

    private void mShowDialog(String message, int type) {
        SweetAlertDialog mShowDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                .setTitleText(getResources().getString(R.string.string_fingerprint))
                .setContentText(message)
                .setConfirmText(getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                        if (type == 0)
                            startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                        else if (type == 1)
                            getActivity().finish();
                        else if (type == 2)
                            startActivity(new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));

                    }
                });

        mShowDialog.setCancelable(false);
        mShowDialog.show();
    }


    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }


    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            //Date date = dateFormat.parse("2019-11-06T00:46:54.3644601+07:00");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                            txtDates.setText(DefaultFormatter.changeFormatDateWithOutHourFull(response.body().getData()));
                            mTimeServer = date;

                            Log.e(TAG, mTimeServer.toString());

                            timer(mTimeServer);

                            // showDialogErorGetServerTime();

                        } else {

                            try {
                                if (getContext()!=null) {
                                    showDialogErorGetServerTime();
                                }
                            } catch (Exception e){

                            }
                        }

                    } catch (Exception e) {
                        try {
                            if (getContext()!=null) {
                                showDialogErorGetServerTime();
                            }
                        } catch (Exception s){

                        }
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                    try {
                        if (getContext()!=null) {
                            showDialogErorGetServerTime();
                        }
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            try {
                if (getContext()!=null) {
                    showDialogErorGetServerTime();
                }
            } catch (Exception q){

            }
        }

    }


    public void timer(Date dat) {
        /*Calendar calendar = Calendar.getInstance();
        calendar.setTime(dat);

        long hour = calendar.get(Calendar.HOUR_OF_DAY);
        long minute = calendar.get(Calendar.MINUTE);
        long second = calendar.get(Calendar.SECOND);

        cronometer.setBase(SystemClock.elapsedRealtime() - (hour * 3600000 + minute * 60000 + second * 1000));
        cronometer.start();
        Log.e(TAG, " cronometer.start();");*/


        int second = dat.getSeconds();
        int minute = dat.getMinutes();
        int hour = dat.getHours();
        int dates = dat.getDate();
        int month = dat.getMonth();
        int year = dat.getYear();


        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, dates, hour, minute, second);

        long hours = calendar.get(Calendar.HOUR_OF_DAY);
        long minutes = calendar.get(Calendar.MINUTE);
        long seconds = calendar.get(Calendar.SECOND);


        cronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer cArg) {
                long time = SystemClock.elapsedRealtime() - cArg.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;
                String hh = h < 10 ? "0" + h : h + "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";
                cArg.setText(hh + ":" + mm + ":" + ss);
            }
        });
        cronometer.setBase(SystemClock.elapsedRealtime() - (hours * 3600000 + minutes * 60000 + seconds * 1000));
        cronometer.start();

    }


    public boolean checkActiveInternet() {

        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onStart(){
        super.onStart();

        String getOrganization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_idOrganization), ""));

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {


            mLayoutFinger.setVisibility(View.GONE);
            mLayoutNonFinger.setVisibility(View.VISIBLE);
            mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtonCheckin.setEnabled(false);
                    Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                    mIntent.putExtra("isFragment", true);
                    startActivity(mIntent);
                    getActivity().finish();
                }
            });

            if (!getOrganization.equals("")) {

            } else {

                showDialogEror();

            }

        } else {

            if (!getOrganization.equals("")) {

                fingerprintManager = null;

                // Initializing both Android Keyguard Manager and Fingerprint Manager
                KeyguardManager keyguardManager = (KeyguardManager) getActivity().getSystemService(KEYGUARD_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    fingerprintManager = (FingerprintManager) getActivity().getSystemService(FINGERPRINT_SERVICE);
                }

                // Check whether the device has a Fingerprint sensor.
                if (fingerprintManager == null){
                    mLayoutFinger.setVisibility(View.GONE);
                    mLayoutNonFinger.setVisibility(View.VISIBLE);
                    mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                            mIntent.putExtra("isFragment", true);
                            startActivity(mIntent);
                            getActivity().finish();
                        }
                    });
                }
                else if (!fingerprintManager.isHardwareDetected()) {
                    // mShowDialog("Device anda tidak support fingerprint", 0);
                    mLayoutFinger.setVisibility(View.GONE);
                    mLayoutNonFinger.setVisibility(View.VISIBLE);
                    mButtonCheckin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent mIntent = new Intent(getActivity(), CheckinActivity.class);
                            mIntent.putExtra("isFragment", true);
                            startActivity(mIntent);
                            getActivity().finish();
                        }
                    });
                }
                 else {

                    mLayoutFinger.setVisibility(View.VISIBLE);
                    mLayoutNonFinger.setVisibility(View.GONE);

                    // Checks whether fingerprint permission is set on manifest
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                        mShowDialog(getResources().getString(R.string.title_finger_izin_otentikasi), 0);
                    } else {
                        // Check whether at least one fingerprint is registered
                        if (!fingerprintManager.hasEnrolledFingerprints()) {
                            mShowDialog(getResources().getString(R.string.title_finger_daftarkan), 0);
                        } else {
                            // Checks whether lock screen security is enabled or not
                            if (!keyguardManager.isKeyguardSecure()) {
                                mShowDialog(getResources().getString(R.string.title_finger_keamanan), 0);
                            } else {
                                generateKey();

                                if (cipherInit()) {
                                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                    helper = new FingerprintHandler(getActivity());
                                    helper.startAuth(fingerprintManager, cryptoObject, true, dataCompanies);

                                    if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !mAlertDialog.isShowing()) {
                                        mAlertDialog.show();
                                    } else {
                                        mAlertDialog.dismiss();
                                    }

                                }

                            }
                        }
                    }
                }
            }
            else {
                showDialogEror();
            }

        }

        mGetServerTim();
    }


    @Override
    public void onPause() {
        super.onPause();
        textView.setText("");
        cronometer.stop();
        String getOrganization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_idOrganization), ""));

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {


        } else {

            if (!getOrganization.equals("")) {

                if (fingerprintManager == null){

                }
                else if (!fingerprintManager.isHardwareDetected()) {


                    //  textView.setText("Perangkat Anda tidak memiliki Sensor Sidik Jari");
                } else {
                    // Checks whether fingerprint permission is set on manifest

                    // Check whether at least one fingerprint is registered

                    helper.stopFingerAuth();


                }
            }

        }



    }
    private void showDialogEror(){

        SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
        alertDialog.setContentText(getResources().getString(R.string.title_gagal_absen_no_org));
        alertDialog.setCancelable(false);
        alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                Intent intent = new Intent(getActivity(), HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                getActivity().finish();

            }
        });
        alertDialog.show();

        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(ContextCompat.getColor(getActivity(),R.color.colorPrimary));
        btn.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorWhite));
    }

    private void showDialogErorGetServerTime(){


        SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
        alertDialog.setContentText(getResources().getString(R.string.title_gagal_jam_server));
        alertDialog.setCancelable(false);
        alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                try {

                    alertDialog.dismiss();
                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    getActivity().finish();
                }catch (Exception e){

                }

            }
        });
        alertDialog.show();

        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.colorPrimary));
        btn.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorWhite));

    }






}
