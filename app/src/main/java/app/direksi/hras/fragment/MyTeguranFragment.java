package app.direksi.hras.fragment;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.DetailTeguranKuActivity;
import app.direksi.hras.R;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.WarningLetterActivity;
import app.direksi.hras.adapter.TeguranAdapter;
import app.direksi.hras.model.DataReprimand;
import app.direksi.hras.model.ResponseDataReprimand;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 07/05/2019.
 */

public class MyTeguranFragment extends Fragment  /*implements PenawaranDetailActivity.DataUpdateListener*/ {
    private RecyclerView mRootView;
    private List<DataReprimand> dataList = new ArrayList<>();
    private List<String> existLoad = new ArrayList<>();
    private TeguranAdapter cAdapter;
    private TextView textNoInternet;
    private ProgressBar loading;
    com.google.android.material.floatingactionbutton.FloatingActionButton fab;
    private SwipeRefreshLayout swiperefresh;
    boolean isLoading = false;
    private int pageNumber = 1;
    int scrollPosition = 0 ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_list, container, false);
        mRootView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        ((WarningLetterActivity)getActivity()).setFragmentRefreshListener(new WarningLetterActivity.FragmentRefreshListener2() {
            @Override
            public void onRefresh() {

                String approval = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_CanIssueReprimand), ""));

                if (approval.equals("true")) {

                    pageNumber = 1;
                    dataList = new ArrayList<>();
                    cAdapter = new TeguranAdapter(dataList, getActivity());
                    mRootView.setAdapter(cAdapter);
                    swiperefresh.setRefreshing(false);
                    fetchService();
                    initScrollListener();

                } else {
                    swiperefresh.setRefreshing(false);
                    textNoInternet.setVisibility(View.VISIBLE);
                    textNoInternet.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
                    loading.setVisibility(View.GONE);

                }
            }
        });
        swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager fm = getActivity().getFragmentManager();
                CreateTeguranFragment dialogFragment = new CreateTeguranFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");


            }
        });
        String approval = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_CanIssueReprimand), ""));
        if (approval.equals("true")){
            fab.setVisibility(View.VISIBLE);
        } else {
            fab.setVisibility(View.GONE);
        }

        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();
    }

    private void initRecyclerView() {
        cAdapter = new TeguranAdapter(dataList, getActivity());
        mRootView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRootView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRootView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

     //   mRootView.addItemDecoration(itemDecorator);
        mRootView.setAdapter(cAdapter);
        mRootView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRootView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                DataReprimand data = dataList.get(position);
                PreferenceManager.getDefaultSharedPreferences(getContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(getContext(), DetailTeguranKuActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();


                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                String approval = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                        getResources().getString(R.string.pref_CanIssueReprimand), ""));

                if (approval.equals("true")) {

                    pageNumber = 1;
                    dataList = new ArrayList<>();
                    cAdapter = new TeguranAdapter(dataList, getActivity());
                    mRootView.setAdapter(cAdapter);
                    swiperefresh.setRefreshing(false);
                    fetchService();
                    initScrollListener();

                } else {
                    swiperefresh.setRefreshing(false);
                    textNoInternet.setVisibility(View.VISIBLE);
                    textNoInternet.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
                    loading.setVisibility(View.GONE);

                }


            }});
        String approval = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_CanIssueReprimand), ""));

        if (approval.equals("true")) {

            fetchService();
            initScrollListener();

        } else {
            textNoInternet.setVisibility(View.VISIBLE);
            textNoInternet.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
            loading.setVisibility(View.GONE);

        }
    }

    private void initScrollListener() {
        mRootView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == dataList.size() - 1) {
                        //bottom of list!
                        double mod = dataList.size() % Integer.parseInt(getResources().getString(R.string.count_paging));
                        if ( mod > 0 ){

                        }else{
                            loadMore();
                            isLoading = true;

                        }
                    }
                }
            }
        });


    }

    private void loadMore() {
        dataList.add(null);
        cAdapter.notifyItemInserted(dataList.size() - 1);
        fetchService();


       /* Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                dataList.remove(dataList.size() - 1);
                scrollPosition = dataList.size();
                fetchService();
                } catch (Exception e){

                }

               *//* while (currentSize - 1 < nextLimit) {
                    rowsArrayList.add("Item " + currentSize);
                    currentSize++;
                }*//*

               *//* cAdapter.notifyDataSetChanged();
                isLoading = false;*//*
            }
        }, 2000);*/


    }

    public static Fragment newInstance() {
        return new MyTeguranFragment();
    }


    private static class FakePageAdapter extends RecyclerView.Adapter<MyTeguranFragment.FakePageVH> {
        private final int numItems;

        FakePageAdapter(int numItems) {
            this.numItems = numItems;
        }

        @Override
        public MyTeguranFragment.FakePageVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_relative_employe, viewGroup, false);

            return new MyTeguranFragment.FakePageVH(itemView);
        }

        @Override
        public void onBindViewHolder(MyTeguranFragment.FakePageVH fakePageVH, int i) {
            // do nothing
        }

        @Override
        public int getItemCount() {
            return numItems;
        }
    }

    private static class FakePageVH extends RecyclerView.ViewHolder {
        FakePageVH(View itemView) {
            super(itemView);
        }
    }




  /*  @Override
    public void onDataUpdate(List<PenawaranDetail> mData) {

        if (mData.get(0).getPnwrAdv().size() > 0 )
        {
            klienList.clear();
            klienList.addAll(mData.get(0).getPnwrAdv());
            cAdapter.notifyDataSetChanged();
            Log.v("AdvFragment", "aaaaaaaaaaa");
        }
        // put your UI update logic here
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((PenawaranDetailActivity) activity).registerDataUpdateListener(this);
    }*/

    public void fetchService() {
        if (dataList.size() > 0){

        }else {
            loading.setVisibility(View.VISIBLE);
        }

       /* if (dataList.size() > 0 || existLoad.size() > 0)
        {
            if (dataList.size() > 0) {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.GONE);
            }
            else
            {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.VISIBLE);
            }
        }
        else {*/

            textNoInternet.setVisibility(View.GONE);
            try {
                Log.v("DoneFragment", "masuk try");

                String Authorization = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_token), ""));

                Map<String, String> data = new HashMap<>();
                data.put("page", String.valueOf(pageNumber));
                data.put("size", getResources().getString(R.string.count_paging));
                data.put("Grab", "approval");

                String idCompany =PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_search_idCompany), "");

                if (!idCompany.equals("0")) {
                    // Toast.makeText(ListCutiActivity.this, "approved", Toast.LENGTH_LONG).show();
                    data.put("CompanyId", idCompany);
                }

                String idOrganization =PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_search_idOrganization), "");

                if (!idOrganization.equals("0")) {
                    // Toast.makeText(EmployeActivity.this, idOrganization, Toast.LENGTH_LONG).show();
                    data.put("OrganizationId", idOrganization);
                }

                if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_start), "").isEmpty())
                    data.put("start", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_dayoff_start), ""));

                if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_end), "").isEmpty())
                    data.put("end", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_dayoff_end), ""));

                if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_search), "").isEmpty())
                    data.put("search", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_dayoff_search), ""));

                if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_status), "").isEmpty())
                    data.put("status", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_dayoff_status), ""));



               /* String status =PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_status), "");



                if (status.equals("0")){



                } else if (status.equals("1")) {
                    // Toast.makeText(ListCutiActivity.this, "pending", Toast.LENGTH_LONG).show();
                    data.put("status", "inreview");



                } else if (status.equals("2")) {
                    // Toast.makeText(ListCutiActivity.this, "approved", Toast.LENGTH_LONG).show();
                    data.put("status", "approved");




                } else if (status.equals("3")) {
                    // Toast.makeText(ListCutiActivity.this, "rejected", Toast.LENGTH_LONG).show();
                    data.put("status", "rejected");


                }*/
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
                        .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                        .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                        .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                        .build();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .client(okHttpClient)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseDataReprimand> call = api.getTeguran( Authorization, data);

                call.enqueue(new Callback<ResponseDataReprimand>() {
                    @Override
                    public void onResponse(Call<ResponseDataReprimand> call, Response<ResponseDataReprimand> response) {
                 /*   Log.v("DoneFragment", response.toString());
                    Log.v("DoneFragment", "" + driverId);
                    Log.v("DoneFragment", response.body().getMessage());
                    Log.v("DoneFragment", response.body().getStatus());
                    Log.v("DoneFragment", response.body().getResult().toString());*/

                        if (response.isSuccessful()) {
                            if (pageNumber != 1) {
                                try {
                                    dataList.remove(dataList.size() - 1);
                                    scrollPosition = dataList.size();
                                } catch (Exception e){

                                }
                                cAdapter.notifyItemRemoved(scrollPosition);
                            }

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                            if (response.body().getErrCode() == 0) {
                                if (response.body().getData().size() > 0) {
                                   // dataList.clear();
                                    existLoad.add("Reload");
                                    dataList.addAll(response.body().getData());
                                    cAdapter.notifyDataSetChanged();
                                    pageNumber = pageNumber + 1;
                                    isLoading = false;
                                } else {
                                    existLoad.add("Reload");
                                    if (dataList.size() > 0){

                                    }else {
                                       // textNoInternet.setText(getActivity().getResources().getString(R.string.title_tidak_ada_data));
                                        textNoInternet.setVisibility(View.VISIBLE);
                                    }
                                }
                                loading.setVisibility(View.GONE);
                            } else {
                                if (pageNumber != 1) {
                                    try {
                                        dataList.remove(dataList.size() - 1);
                                        scrollPosition = dataList.size();
                                    } catch (Exception e){

                                    }
                                    cAdapter.notifyItemRemoved(scrollPosition);
                                }
                                try {
                                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                    // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception e)
                                {

                                }
                                loading.setVisibility(View.GONE);
                                isLoading = false;
                            }

                            loading.setVisibility(View.GONE);


                        } else {
                            if (pageNumber != 1) {
                                try {
                                    dataList.remove(dataList.size() - 1);
                                    scrollPosition = dataList.size();
                                } catch (Exception e){

                                }
                                cAdapter.notifyItemRemoved(scrollPosition);
                            }
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                               // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e)
                            {

                            }
                            loading.setVisibility(View.GONE);
                            isLoading = false;
                            Log.v("DoneFragment", "tidak sukses");
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDataReprimand> call, Throwable t) {
                        if (pageNumber != 1) {
                            try {
                                dataList.remove(dataList.size() - 1);
                                scrollPosition = dataList.size();
                            } catch (Exception e){

                            }
                            cAdapter.notifyItemRemoved(scrollPosition);
                        }
                        Log.v("DoneFragment", "on failure");
                        loading.setVisibility(View.GONE);
                        // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                        isLoading = false;
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                    }
                });
            } catch (Exception e) {
                if (pageNumber != 1) {
                    try {
                        dataList.remove(dataList.size() - 1);
                        scrollPosition = dataList.size();
                    } catch (Exception s){

                    }
                    cAdapter.notifyItemRemoved(scrollPosition);
                }
                Log.v("DoneFragment", "masuk catch");
                if (isAdded())
                    loading.setVisibility(View.GONE);
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                isLoading = false;
                //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
               // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

            }
        /*}*/


    }




}



