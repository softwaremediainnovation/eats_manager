package app.direksi.hras.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.model.FormPenilaian;
import app.direksi.hras.model.ResponseGeneral;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

public class BePartnerStepFiveFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private SweetAlertDialog loading;

    private EditText mJumlah, txtKenaikan, txtKeterangan;
    private CheckBox cbA, cbB,cbC,cbD;
    private TextView txtGrade;
    private RadioGroup rbtipe;
    private String catatanPenilaiGlobal = "";
    private String grade = "";

    private BePartnerStepFiveFragment.OnStepFiveListener mListener;

    public BePartnerStepFiveFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BePartnerStepOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BePartnerStepFiveFragment newInstance(String param1, String param2) {
        BePartnerStepFiveFragment fragment = new BePartnerStepFiveFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_be_partner_step_five, container, false);


    }

    private Button backBT;
    private Button nextBT;


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        backBT=view.findViewById(R.id.backBT);
        nextBT=view.findViewById(R.id.nextBT);

        mJumlah=view.findViewById(R.id.mJumlah);
        txtKenaikan=view.findViewById(R.id.txtKenaikan);
        txtKeterangan=view.findViewById(R.id.txtKeterangan);
        txtGrade=view.findViewById(R.id.txtGrade);

        cbA=view.findViewById(R.id.cbA);
        cbB=view.findViewById(R.id.cbB);
        cbC=view.findViewById(R.id.cbC);
        cbD=view.findViewById(R.id.cbD);

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

       catatanPenilaiGlobal = "Perpanjang kontrak";

        rbtipe = view.findViewById(R.id.rbtipe);
        rbtipe.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = rbtipe.findViewById(checkedId);
                int index = rbtipe.indexOfChild(radioButton);

                if(index == 0){

                    catatanPenilaiGlobal = "Perpanjang kontrak";

                } else if (index == 1){

                    catatanPenilaiGlobal = "Putus kontrak";

                }else if (index == 2){

                    catatanPenilaiGlobal = "Angkat pegawai tetap";

                }else if (index == 3){

                    catatanPenilaiGlobal = "Promosi";

                }else {

                    catatanPenilaiGlobal = "Demosi";

                }
                // checkedId is the RadioButton selected
            }
        });

        CheckBoxLoad();



        final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0")) {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_bisa_dimulai_nol),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                  /*  Toast.makeText(getActivity(),
                            "Tidak bisa dimulai dengan nol (0)",
                            Toast.LENGTH_SHORT).show();*/
                    if (enteredString.length() > 0) {
                        mJumlah.setText(enteredString.substring(1));

                    } else {
                        mJumlah.setText("");
                    }
                } else {

                }


                if (enteredString.length() >= 12){
                    String str = enteredString.substring(0, 11);
                    mJumlah.setText(str);
                    mJumlah.setSelection(str.length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mJumlah.getText().hashCode() == s.hashCode()) {
                    mJumlah.removeTextChangedListener(this);
                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                    symbols.setGroupingSeparator(',');
                    try {
                        mJumlah.setText(formatter.format(Double.parseDouble(s.toString().replace(",", "") + "")));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    } catch (Exception e) {
                        mJumlah.setText(s.toString().replace(",", ""));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    }
                    mJumlah.addTextChangedListener(this);
                }

            }


        };
        mJumlah.addTextChangedListener(mPriceWatcher);
    }


    @Override
    public void onResume() {
        super.onResume();
        backBT.setOnClickListener(this);
        nextBT.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        backBT.setOnClickListener(null);
        nextBT.setOnClickListener(null);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.backBT:
                if (mListener != null)
                    mListener.onBackPressed(this);
                break;

            case R.id.nextBT:

                CheckNext();


                break;
        }
    }

    private void Save(){
        if (mListener != null)
            mListener.onNextPressed(this);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BePartnerStepFiveFragment.OnStepFiveListener) {
            mListener = (BePartnerStepFiveFragment.OnStepFiveListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        backBT=null;
        nextBT=null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStepFiveListener {
        void onBackPressed(Fragment fragment);
        void onNextPressed(Fragment fragment);
    }


    private void CheckNext(){


        mJumlah.setError(null);
        txtKenaikan.setError(null);
        txtKeterangan.setError(null);
        txtGrade.setError(null);

        String jumlah = mJumlah.getText().toString();
        String kenaikan = txtKenaikan.getText().toString();
        String keterangan = txtKeterangan.getText().toString();

        boolean cancel = false;
        View focusView = null;

        /*if (TextUtils.isEmpty(jumlah)) {
            mJumlah.setError("Isian harap diisi");
            focusView = mJumlah;
            cancel = true;
        }

        if (TextUtils.isEmpty(kenaikan)) {
            txtKenaikan.setError("Isian harap diisi");
            focusView = txtKenaikan;
            cancel = true;
        }*/

        if (TextUtils.isEmpty(keterangan)) {
            txtKeterangan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = txtKeterangan;
            cancel = true;
        }

        /*if (cbA.isChecked()== true || cbB.isChecked()== true || cbC.isChecked()== true || cbD.isChecked()== true  ) {

        } else {

            txtGrade.setError("");
            focusView = txtGrade;
            cancel = true;
        }*/

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.string_confirmation))
                    .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                    .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                          /*  Save();*/
                            SaveEvaluation();

                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.txt_no), null)
                    .show();

        }



    }

    private void CheckBoxLoad(){

        cbA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    cbB.setChecked(false);
                    cbC.setChecked(false);
                    cbD.setChecked(false);
                    GradeResult();

                } else {
                    GradeResult();
                }

            }
        });

        cbB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    cbA.setChecked(false);
                    cbC.setChecked(false);
                    cbD.setChecked(false);
                    GradeResult();

                } else {
                    GradeResult();
                }

            }
        });
        cbC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    cbB.setChecked(false);
                    cbA.setChecked(false);
                    cbD.setChecked(false);
                    GradeResult();

                } else {
                    GradeResult();
                }

            }
        });

        cbD.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    cbB.setChecked(false);
                    cbC.setChecked(false);
                    cbA.setChecked(false);
                    GradeResult();

                } else {
                    GradeResult();
                }

            }
        });



    }

    private void SaveEvaluation() {
        try {

            loading.show();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            FormPenilaian formPenilaian = formPenilaianRestore();

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.createEvaluation(Authorization, formPenilaian);


            call.enqueue(new Callback<ResponseGeneral>() {


                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            loading.dismiss();
                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    /*Intent login = new Intent(getActivity(), ContractExpiredActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);*/
                                    Save();
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));

                        }

                    } catch (Exception e) {

                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception se)
                        {

                        }
                    }
                    loading.dismiss();

                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {

                    loading.dismiss();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e)
                    {

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
            }
            catch (Exception xe)
            {

            }

        }

    }



    private FormPenilaian formPenilaianRestore(){

        String start = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_1_start), ""));
        String end = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_1_end), ""));
        Integer gaji =  Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_1_gaji), "0"));

        int disiplinKerja = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_a1), ""));
        int minatTerhadapPekerjaan  = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_a2), ""));
        int caraKerja = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_a3), ""));
        int tempoKerja = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_a4), ""));
        int tanggapTerhadapTugasPekerjaanA = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_a5), ""));
        int kesimpulanA = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_aKesimpulan), ""));
        int dasarPemikiran = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_b1), ""));
        int inisiatif = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_b2), ""));
        int kepemimpinan = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_b3), ""));
        int pertanggunganJawabAdministrasi = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_b4), ""));
        int tanggapTerhadapTugasPekerjaanB = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_b5), ""));
        int kesimpulanB = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_bKesimpulan), ""));
        int kesimpulanHasilKondite = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_2_cKesimpulan), ""));

        int pengabdian = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a1), ""));
        int kejujuran = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a2), ""));
        int rasaTanggungJawab = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a3), ""));
        int keinginanUntukMaju = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a4), ""));
        int penguasaanEmosi = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a5), ""));
        int penampilanDiri = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a6), ""));
        int komunikasi = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_a7), ""));
        int kesimpulanHasilKepribadian = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_3_aKesimpulan), ""));

        int loyalitasDanRasaMemiliki = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_4_aKesimpulan), ""));


        String perincianAbsensiM = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_amangkir), ""));
        String perincianAbsensiSI = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_asuratijin), ""));
        String perincianAbsensiSK = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_asuratketerangan), ""));
        String perincianAbsensiSD = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_asuratdokter), ""));
        String perincianAbsensiDis = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_adispensasi), ""));
        String perincianAbsensiCH = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_acutihaid), ""));
        String perincianAbsensiCT = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_acutitahunan), ""));



        String hubunganKeluargaDenganPenilai = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_b1), ""));
        String penghargaanYangDiberikan = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_b2), ""));
        String peringatanYangDiberikan = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_form_5_b3), ""));


        String kenaikanGajiPokok = mJumlah.getText().toString();
        String kenaikanJabatan =  txtKenaikan.getText().toString();
        String kategoriKenaikanGajiSesuaiGrade = grade;
        String catatanPenilai = catatanPenilaiGlobal;
        String keterangan =  txtKeterangan.getText().toString();

        int employeeID = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_employeeid_view), ""));
        int evaluatorID = Integer.parseInt(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(getResources().getString(R.string.pref_employeeid), ""));

         FormPenilaian formPenilaian = new FormPenilaian( disiplinKerja,  minatTerhadapPekerjaan,  caraKerja,  tempoKerja,  tanggapTerhadapTugasPekerjaanA,  kesimpulanA,
         dasarPemikiran,  inisiatif,  kepemimpinan,  pertanggunganJawabAdministrasi,  tanggapTerhadapTugasPekerjaanB,  kesimpulanB,  kesimpulanHasilKondite,
         pengabdian,  kejujuran,  rasaTanggungJawab,  keinginanUntukMaju,  penguasaanEmosi,  penampilanDiri,  komunikasi,  kesimpulanHasilKepribadian,
         loyalitasDanRasaMemiliki,  hubunganKeluargaDenganPenilai,  penghargaanYangDiberikan,  peringatanYangDiberikan,
                 kenaikanGajiPokok,  kenaikanJabatan,  kategoriKenaikanGajiSesuaiGrade,  catatanPenilai,  keterangan, employeeID, evaluatorID ,
                 perincianAbsensiM, perincianAbsensiSI, perincianAbsensiSK, perincianAbsensiSD, perincianAbsensiDis, perincianAbsensiCH, perincianAbsensiCT, start, end, gaji
         );


        return formPenilaian;
    }

    private void GradeResult(){

        grade = "";

        if (cbA.isChecked()== true){
            grade = "A";
        }
        if (cbB.isChecked()== true){
            grade = "B";
        }
        if (cbC.isChecked()== true){
            grade = "C";
        }
        if (cbD.isChecked()== true){
            grade = "D";
        }


    }
}

