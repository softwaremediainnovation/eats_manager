package app.direksi.hras.fragment;

import android.Manifest;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import com.valdesekamdem.library.mdtoast.MDToast;

import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.AttendanceActivity;
import app.direksi.hras.BirthdayActivity;
import app.direksi.hras.ClaimActivity;
import app.direksi.hras.GajiActivity;
import app.direksi.hras.ListCutiActivity;
import app.direksi.hras.ListHolidayActivity;
import app.direksi.hras.LoanActivity;
import app.direksi.hras.NewsActivity;
import app.direksi.hras.OvertimeActivity;
import app.direksi.hras.R;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.WarningLetterActivity;
import app.direksi.hras.adapter.BirthdayDashboardAdapter;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.EmployeeBirthdayDashboard;
import app.direksi.hras.model.ResponseDashboard;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.MessageDialog;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 08/05/2019.
 */

public class DashboardFragment extends Fragment implements  View.OnClickListener {

    private List<EmployeeBirthdayDashboard> dataListGR = new ArrayList<>();
    private ScrollView mLayoutMain;
    private View layoutInflater;
    private RecyclerView recyclerViewBirthaday;
    private List<DataAttendance> reportResult;
    private List<DataAttendance> report;
    private TextView jml_company, jml_job, label_company, txt_empty_rv;
    private int count_company = 0, count_job = 0;
    private String Authorization;
    private TextView title, txtNoBirthday;
    private Toolbar toolbar;
    private LinearLayout  llAtendance, llCuti, llPinjaman, llTeguran, llBirthday;
    private RelativeLayout llHome;
    private TextView txtEmailNav, txtNameNav;
    private TextView mTitleNews, mTxtNews, mTxtApprove, mTxtWait, mTxtTotalSurat, mTxtSurat, mTxtReject, mTxtLembur1, mTxtLembur2,
            mTxtLembur3, mTxtTotalGaji, mTxtPinjaman,
            mTxtKlaim,
            mTxtNotif,
            txtHaiName,
            txtWelcome;
    private TextView mTxtCutiDiterima, mTxtCutiMenunggu, mTxtCutiDitolak, mTxtCutiTotal,
            mTxtLemburDiterima, mTxtLemburMenunggu, mTxtLemburDitolak, mTxtLemburTotal,
            mTxtKlaimDiterima, mTxtKlaimMenunggu, mTxtKlaimDitolak, mTxtKlaimTotal,
            mTxtLoanDiterima, mTxtLoanMenunggu, mTxtLoanDitolak, mTxtLoanTotal, SPDesc, mTxtSisaPinjaman, mTxtTotalTelat;

    private BirthdayDashboardAdapter GRAdapter;
    private ImageView img_cal;

    private NavigationView navigationView;
    private LinearLayout mLayoutNews, mLayoutCuti, mLayoutWarning, mLayoutOvertime, mLayoutSalary, mLayoutClaim, mLayoutPinjaman, llHai, mLayoutTelat;
    private SweetAlertDialog loading;
    private MessageDialog messageDialog;
    ImageView imageViewNav;
    private View vwNoBirthday;
    private SwipeRefreshLayout swiperefresh;


    private boolean isSupportBiometric = false;
    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private String absenMethod = "";
    PermissionsChecker checker;
    private static final String[] PERMISSIONS = new String[]{
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportResult = new ArrayList<>();
        report = new ArrayList<DataAttendance>();
//        setContentView(R.layout.activity_reports);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Jobs");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle saveInstanceState) {
        layoutInflater = inflater.inflate(R.layout.dahboard_fragment_layout, container, false);
        setHasOptionsMenu(true); //to set toolbar menu

        //counter jml job

        vwNoBirthday = layoutInflater.findViewById(R.id.vwNoBirthday);
        messageDialog = new MessageDialog();
        llHai = layoutInflater.findViewById(R.id.llHai);
        llHai.setVisibility(View.GONE);
        SPDesc = layoutInflater.findViewById(R.id.SPDesc);
        txtHaiName = layoutInflater.findViewById(R.id.txtHaiName);
        mLayoutMain = layoutInflater.findViewById(R.id.mLayoutMain);
        mLayoutTelat = layoutInflater.findViewById(R.id.mLayoutTelat);
        mTxtTotalTelat = layoutInflater.findViewById(R.id.mTxtTotalTelat);
        mLayoutMain.setVisibility(View.GONE);
        mLayoutNews = layoutInflater.findViewById(R.id.mLayoutNews);
        mLayoutPinjaman = layoutInflater.findViewById(R.id.mLayoutPinjaman);
        mLayoutCuti = layoutInflater.findViewById(R.id.mLayoutCuti);
        mLayoutWarning = layoutInflater.findViewById(R.id.mLayoutWarning);
        mLayoutOvertime = layoutInflater.findViewById(R.id.mLayoutOvertime);
        mLayoutSalary = layoutInflater.findViewById(R.id.mLayoutSalary);
        mLayoutClaim = layoutInflater.findViewById(R.id.mLayoutClaim);
        mTitleNews = layoutInflater.findViewById(R.id.mTitleNews);
        mTxtNews = layoutInflater.findViewById(R.id.mTxtNews);
        llHome = layoutInflater.findViewById(R.id.llHome);
        txtWelcome = layoutInflater.findViewById(R.id.txtWelcome);
        swiperefresh = (SwipeRefreshLayout) layoutInflater.findViewById(R.id.swiperefresh);

        recyclerViewBirthaday = (RecyclerView) layoutInflater.findViewById(R.id.recyclerViewBirthaday);
        llBirthday = layoutInflater.findViewById(R.id.llBirthday);
        txtNoBirthday = layoutInflater.findViewById(R.id.txtNoBirthday);

        absenMethod = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_absen_method), ""));

        llBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), BirthdayActivity.class);
                startActivity(intent);
            }
        });


        mTxtApprove = layoutInflater.findViewById(R.id.mTxtApprove);
        mTxtWait = layoutInflater.findViewById(R.id.mTxtWait);
        mTxtReject = layoutInflater.findViewById(R.id.mTxtReject);
        mTxtTotalSurat = layoutInflater.findViewById(R.id.mTxtTotalSurat);
        mTxtSurat = layoutInflater.findViewById(R.id.mTxtSurat);

        mTxtLembur1 = layoutInflater.findViewById(R.id.mTxtLembur1);
        mTxtLembur2 = layoutInflater.findViewById(R.id.mTxtLembur2);
        mTxtLembur3 = layoutInflater.findViewById(R.id.mTxtLembur3);
        mTxtTotalGaji = layoutInflater.findViewById(R.id.mTxtTotalGaji);
        mTxtNotif = layoutInflater.findViewById(R.id.mTxtNotif);


        mTxtPinjaman = layoutInflater.findViewById(R.id.mTxtPinjaman);
        mTxtKlaim = layoutInflater.findViewById(R.id.mTxtKlaim);

        mTxtSisaPinjaman = layoutInflater.findViewById(R.id.mTxtSisaPinjaman);


        mTxtCutiDiterima = layoutInflater.findViewById(R.id.mTxtCutiDiterima);
        mTxtCutiMenunggu = layoutInflater.findViewById(R.id.mTxtCutiMenunggu);
        mTxtCutiDitolak = layoutInflater.findViewById(R.id.mTxtCutiDitolak);
        mTxtCutiTotal = layoutInflater.findViewById(R.id.mTxtCutiTotal);

        mTxtLemburDiterima = layoutInflater.findViewById(R.id.mTxtLemburDiterima);
        mTxtLemburMenunggu = layoutInflater.findViewById(R.id.mTxtLemburMenunggu);
        mTxtLemburDitolak = layoutInflater.findViewById(R.id.mTxtLemburDitolak);
        mTxtLemburTotal = layoutInflater.findViewById(R.id.mTxtLemburTotal);

        mTxtKlaimDiterima = layoutInflater.findViewById(R.id.mTxtKlaimDiterima);
        mTxtKlaimMenunggu = layoutInflater.findViewById(R.id.mTxtKlaimMenunggu);
        mTxtKlaimDitolak = layoutInflater.findViewById(R.id.mTxtKlaimDitolak);
        mTxtKlaimTotal = layoutInflater.findViewById(R.id.mTxtKlaimTotal);

        mTxtLoanDiterima = layoutInflater.findViewById(R.id.mTxtLoanDiterima);
        mTxtLoanMenunggu = layoutInflater.findViewById(R.id.mTxtLoanMenunggu);
        mTxtLoanDitolak = layoutInflater.findViewById(R.id.mTxtLoanDitolak);
        mTxtLoanTotal = layoutInflater.findViewById(R.id.mTxtLoanTotal);

        img_cal = layoutInflater.findViewById(R.id.img_cal);

        loading = new SweetAlertDialog(getContext(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mLayoutNews.setOnClickListener(this);
        mLayoutCuti.setOnClickListener(this);
        mLayoutWarning.setOnClickListener(this);
        mLayoutOvertime.setOnClickListener(this);
        mLayoutSalary.setOnClickListener(this);
        mLayoutClaim.setOnClickListener(this);
        mLayoutPinjaman.setOnClickListener(this);
        mLayoutTelat.setOnClickListener(this);
        img_cal.setOnClickListener(this);



        txtHaiName.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_nama), ""));

        DateFormat df = new SimpleDateFormat("HH");
        String date = df.format(Calendar.getInstance().getTime());

        if (date.equals("04") || date.equals("05") || date.equals("06") || date.equals("07") || date.equals("08") || date.equals("09")){
            txtWelcome.setText(getResources().getString(R.string.title_selamat_pagi));
        }else if ( date.equals("10") || date.equals("11") || date.equals("12") || date.equals("13") || date.equals("14") || date.equals("15") ){
            txtWelcome.setText(getResources().getString(R.string.title_selamat_siang));
        }else if (date.equals("16") || date.equals("17") || date.equals("18") || date.equals("19")){
            txtWelcome.setText(getResources().getString(R.string.title_selamat_sore));
        }else {
            txtWelcome.setText(getResources().getString(R.string.title_selamat_malam));
        }


        PreferenceManager.getDefaultSharedPreferences(getContext()).
                edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                .putString(getResources().getString(R.string.pref_dayoff_start), "")
                .putString(getResources().getString(R.string.pref_dayoff_end), "")
                .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                .putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                .putString(getResources().getString(R.string.pref_dayoff_idProses), "0")
                .putString(getResources().getString(R.string.pref_dayoff_status), "1")
                .apply();


        GRAdapter = new BirthdayDashboardAdapter(dataListGR, getActivity());

        recyclerViewBirthaday.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManagerGR = new LinearLayoutManager(getActivity());
        recyclerViewBirthaday.setLayoutManager(mLayoutManagerGR);
        recyclerViewBirthaday.setItemAnimator(new DefaultItemAnimator());
        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));
        recyclerViewBirthaday.addItemDecoration(itemDecorator);
        recyclerViewBirthaday.setAdapter(GRAdapter);
        recyclerViewBirthaday.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewBirthaday, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               /* Penawaran penawaran = penawaranList.get(position);


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();

                GoTo("com.mii.erp.PenawaranDetailActivity", penawaran);*/
                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        mGetDashboard();


        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                dataListGR.clear();
                //dataListGR = new ArrayList<>();
                // GRAdapter = new BirthdayDashboardAdapter(dataListGR, getActivity());
                swiperefresh.setRefreshing(false);
                llHai.setVisibility(View.GONE);
                mLayoutMain.setVisibility(View.GONE);
                mGetDashboard();



            }});

        //kode untuk swipe to refresh
        /*final SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) layoutInflater.findViewById(R.id.refreshCompany);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.darker_gray);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener(){
            @Override
            public void onRefresh() {
                refreshItem();
            }

            void refreshItem() {
                loadJSON();
                onItemLoad();
            }

            void onItemLoad() {
                swipeRefreshLayout.setRefreshing(false);
            }

        });
*/
        //code untuk recyclerview
       /* recyclerView = (RecyclerView) layoutInflater.findViewById(R.id.rv_reports);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), null)); //untuk divider
        recyclerView.setAdapter(new ReportsAdapter(report));

        txt_empty_rv = (TextView) layoutInflater.findViewById(R.id.tv_empty_rv_report);*/


        checker = new PermissionsChecker(getActivity());

        if (checkBiometricSupport()) {
            isSupportBiometric = true;
        } else {

            isSupportBiometric = false;

        }

        if (isSupportBiometric) {
            if (checker.lacksPermissions(PERMISSIONS)) {
                startPermissionsActivity(PERMISSIONS);
            }
        }





        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle(getActivity().getResources().getString(R.string.txt_biometric_verification))
                .setSubtitle(getActivity().getResources().getString(R.string.txt_mesage_presence_biometric))
                .setNegativeButtonText(getActivity().getResources().getString(R.string.title_batal_small))
                .setConfirmationRequired(false)
                .build();

        executor = ContextCompat.getMainExecutor(getContext());
        biometricPrompt = new BiometricPrompt(this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);

                if (errString.equals(getActivity().getResources().getString(R.string.title_batal_small))){

                } else {

                    MDToast.makeText(getActivity(),  errString.toString(),
                            MDToast.LENGTH_SHORT, MDToast.TYPE_INFO).show();

                }

            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                Intent intent = new Intent(getContext(), GajiActivity.class);
                startActivity(intent);



            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                MDToast.makeText(getActivity(), getActivity().getResources().getString(R.string.txt_failed_verification),
                        MDToast.LENGTH_SHORT, MDToast.TYPE_WARNING).show();
            }
        });

        return layoutInflater;
    }

    private Boolean checkBiometricSupport(){

        Boolean hasil = false;
        BiometricManager biometricManager = BiometricManager.from(getActivity());
        switch (biometricManager.canAuthenticate()) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                hasil = true;
                Log.d("MY_APP_TAG", "App can authenticate using biometrics.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                hasil = false;
                Log.e("MY_APP_TAG", "No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                hasil = false;
                Log.e("MY_APP_TAG", "Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                hasil = true;
                Log.e("MY_APP_TAG", "BIOMETRIC_ERROR_NONE_ENROLLED");
                // Prompts the user to create credentials that your app accepts.
                break;
        }

        return hasil;
    }

    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(getActivity(), 0, permission);
    }

    private void mGetDashboard() {

        try {
            loading.show();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_token), "tes"));


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


            Call<ResponseDashboard> call = api.mGetDashboard(Authorization);
            call.enqueue(new Callback<ResponseDashboard>() {
                @Override
                public void onResponse(Call<ResponseDashboard> call, Response<ResponseDashboard> response) {

                    try {
                        //loading hide
                        loading.dismiss();

                        if (response.isSuccessful()) {
                            long statusCode = response.body().getErrCode();
                            String statusMessage = response.body().getErrMessage();
                            //status code 1 if login success
                            if (statusCode == 0) {


                                if (response.body().getDataLuar().getDataDalam().getSurat().getSurat()!=null){
                                    SPDesc.setText(response.body().getDataLuar().getDataDalam().getSurat().getSurat());
                                }

                                if (response.body().getDataLuar().getDataDalam().getNews()!=null) {
                                    mTitleNews.setText(response.body().getDataLuar().getDataDalam().getNews().getTitle() != null ? response.body().getDataLuar().getDataDalam().getNews().getTitle() : "");
                                    mTxtNews.setText(response.body().getDataLuar().getDataDalam().getNews().getNews() != null ? response.body().getDataLuar().getDataDalam().getNews().getNews() : "");
                                } else{
                                    mTitleNews.setText(getResources().getString(R.string.title_tidak_ada_berita));
                                    mTxtNews.setText("");
                                }
                                mLayoutMain.setVisibility(View.VISIBLE);
                                llHai.setVisibility(View.VISIBLE);
                                DecimalFormat formatter = new DecimalFormat("#,###,###");

                                mTxtApprove.setText( getResources().getString(R.string.title_disetujui_low) + " : " + response.body().getDataLuar().getDataDalam().getCuti().getDisetujui());
                                mTxtWait.setText( getResources().getString(R.string.title_menunggu_low) + " : " + response.body().getDataLuar().getDataDalam().getCuti().getMenunggu());
                                mTxtReject.setText( getResources().getString(R.string.title_ditolak_low) + " : " + response.body().getDataLuar().getDataDalam().getCuti().getDitolak());


                                mTxtLembur1.setText( getResources().getString(R.string.title_disetujui_low) + " : " + response.body().getDataLuar().getDataDalam().getLembur().getDisetujui());
                                mTxtLembur2.setText( getResources().getString(R.string.title_menunggu_low) + " : " + response.body().getDataLuar().getDataDalam().getLembur().getMenunggu());
                                mTxtLembur3.setText( getResources().getString(R.string.title_ditolak_low) + " : " + response.body().getDataLuar().getDataDalam().getLembur().getDitolak());


                                mTxtTotalSurat.setText(response.body().getDataLuar().getDataDalam().getSurat().getTotal().toString());
                                mTxtSurat.setText(response.body().getDataLuar().getDataDalam().getSurat().getSurat());

                                mTxtTotalGaji.setText( getResources().getString(R.string.title_total_gaji) + " " + getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getGaji().getRemunerationNominalID()));
                                mTxtKlaim.setText( getResources().getString(R.string.title_pengajuan_claim) + " " + getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtPinjaman.setText( getResources().getString(R.string.title_pengajuan_pinjaman) + " " + getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtNotif.setText(response.body().getDataLuar().getDataDalam().getGaji().getNotifikasi());

                                mTxtCutiDiterima.setText(response.body().getDataLuar().getDataDalam().getCuti().getDisetujui().toString());
                                mTxtCutiMenunggu.setText(response.body().getDataLuar().getDataDalam().getCuti().getMenunggu().toString());
                                mTxtCutiDitolak.setText(response.body().getDataLuar().getDataDalam().getCuti().getDitolak().toString());
                                mTxtCutiTotal.setText(String.valueOf(response.body().getDataLuar().getDataDalam().getCuti().getTotal()) + " " +  getResources().getString(R.string.title_hari));

                                mTxtLemburDiterima.setText(response.body().getDataLuar().getDataDalam().getLembur().getDisetujui().toString());
                                mTxtLemburMenunggu.setText(response.body().getDataLuar().getDataDalam().getLembur().getMenunggu().toString());
                                mTxtLemburDitolak.setText(response.body().getDataLuar().getDataDalam().getLembur().getDitolak().toString());
                                // mTxtLemburTotal.setText(String.valueOf(response.body().getDataLuar().getDataDalam().getLembur().getDitolak() + response.body().getDataLuar().getDataDalam().getLembur().getDisetujui() + response.body().getDataLuar().getDataDalam().getLembur().getMenunggu()));

                                mTxtKlaimDiterima.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtKlaimMenunggu.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtKlaimDitolak.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtKlaimTotal.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim() + response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim() + response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));

                                mTxtLoanDiterima.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtLoanMenunggu.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtLoanDitolak.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPembayaran()));
                                mTxtLoanTotal.setText( getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman() + response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman() + response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));

                                mTxtSisaPinjaman.setText( getResources().getString(R.string.title_rp) + " " + formatter.format (response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman() - response.body().getDataLuar().getDataDalam().getPengajuan().getPembayaran()));


                                try {
                                    long hour = response.body().getDataLuar().getDataDalam().getLembur().getTotal()/60;
                                    long minut = response.body().getDataLuar().getDataDalam().getLembur().getTotal() % 60;
                                    long total  = response.body().getDataLuar().getDataDalam().getLembur().getTotal();



                                    //tbLast.setVisibility(View.VISIBLE);
                                    // String value = ("Jumlah lembur dalam 1 bulan adalah  <b>" + hour +  " jam " +  minut + " menit ( Total " + total+ " menit )</b>");
                                    //txtLastTitle.setText(Html.fromHtml(value));


                                    mTxtLemburTotal.setText(hour + " " +
                                            getResources().getString(R.string.title_jam) + " "
                                            + minut + " " +
                                            getResources().getString(R.string.title_menit)
                                            +" ( " +
                                            getResources().getString(R.string.title_total) + " "
                                            + total + " " +
                                            getResources().getString(R.string.title_menit) +" )");
                                } catch ( Exception e){

                                }


                                try {
                                    Double temp1 = response.body().getDataLuar().getDataDalam().getTotalTerlambat()/60;
                                    Double temp2 = response.body().getDataLuar().getDataDalam().getTotalTerlambat() % 60;
                                    Double temp3 =  response.body().getDataLuar().getDataDalam().getTotalTerlambat();

                                    int hour =  temp1.intValue();
                                    int minut = temp2.intValue();
                                    int total = temp3.intValue();

                                    //tbLast.setVisibility(View.VISIBLE);
                                    // String value = ("Jumlah lembur dalam 1 bulan adalah  <b>" + hour +  " jam " +  minut + " menit ( Total " + total+ " menit )</b>");
                                    //txtLastTitle.setText(Html.fromHtml(value));


                                    mTxtTotalTelat.setText(hour + " " +
                                            getResources().getString(R.string.title_jam) + " "
                                            + minut + " " +
                                            getResources().getString(R.string.title_menit)
                                            +" ( " +
                                            getResources().getString(R.string.title_total) + " "
                                            + total + " " +
                                            getResources().getString(R.string.title_menit) +" )");
                                } catch ( Exception e){

                                }

                                dataListGR.addAll(response.body().getDataLuar().getDataDalam().getEmployees());
                                // Toast.makeText(getActivity(), "selected " + dataListGR.size(), Toast.LENGTH_SHORT).show();
                                GRAdapter.notifyDataSetChanged();
                                if (response.body().getDataLuar().getDataDalam().getEmployees().size() > 0){
                                    txtNoBirthday.setVisibility(View.GONE);
                                    vwNoBirthday.setVisibility(View.GONE);
                                } else {
                                    txtNoBirthday.setVisibility(View.VISIBLE);
                                    vwNoBirthday.setVisibility(View.VISIBLE);
                                }




                               /* mTxtCutiDiterima, mTxtCutiMenunggu, mTxtCutiDitolak, mTxtCutiTotal,
                                        mTxtLemburDiterima, mTxtLemburMenunggu, mTxtLemburDitolak, mTxtLemburTotal,
                                        mTxtKlaimDiterima, mTxtKlaimMenunggu, mTxtKlaimDitolak, mTxtKlaimTotal,
                                        mTxtLoanDiterima, mTxtLoanMenunggu, mTxtLoanDitolak, mTxtLoanTotal;*/


                            } else {

                                try {
                                    loading.dismiss();
                                    if (getActivity()!= null) {
                                        messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                                , "");
                                    }
                                } catch (Exception e){

                                }
                            }

                        } else {
                            try {
                                loading.dismiss();
                                if (getActivity()!= null) {
                                    messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                            , "");
                                }
                            } catch (Exception e){

                            }
                        }
                    } catch (Exception e) {
                        try {
                            loading.dismiss();
                            if (getActivity()!= null) {
                                messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                        , "");
                            }
                        } catch (Exception d){

                        }
                    }


                }

                @Override
                public void onFailure(Call<ResponseDashboard> call, Throwable t) {
                    try {
                        loading.dismiss();
                        if (getActivity()!= null) {
                            messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                    , "");
                        }
                    } catch (Exception d){

                    }
                }

            });


        } catch (Exception e) {
            try {
                loading.dismiss();
                if (getActivity()!= null) {
                    messageDialog.mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                            , "");
                }
            } catch (Exception d){

            }
        }
    }

    //filter untuk searchView, filterable ada di adapter.
  /*  private void filterList(String name){
        synchronized(recyclerView.getAdapter()){
           *//* ((ReportsAdapter)recyclerView.getAdapter()).getFilter().filter(name);*//*
        }
    }*/

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }

    //method untuk searchView di MENU PENGIKLAN
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    //clear share preference untuk login
    void mClearPreferences() {
        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit()
                .putString("pref_token", "")
                .putString("pref_email", "")
                .putString("pref_password", "")
                .apply();
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mLayoutNews) {
            Intent intent = new Intent(getContext(), NewsActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutCuti) {
            Intent intent = new Intent(getContext(), ListCutiActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutWarning) {
            Intent intent = new Intent(getContext(), WarningLetterActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutOvertime) {
            Intent intent = new Intent(getContext(), OvertimeActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutSalary) {

            if(absenMethod.equals("biometric") || absenMethod.equals("face") || absenMethod.equals("finger")){

                if (isSupportBiometric) {

                    biometricPrompt.authenticate(promptInfo);
                }


                else{

                    Intent intent = new Intent(getContext(), GajiActivity.class);
                    startActivity(intent);
                }

            } else {

                Intent intent = new Intent(getContext(), GajiActivity.class);
                startActivity(intent);

            }


        } else if (v.getId() == R.id.mLayoutSalary) {
           /* Intent intent = new Intent(getContext(), SalaryActivity.class);
            startActivity(intent);*/
        } else if (v.getId() == R.id.mLayoutClaim) {
            Intent intent = new Intent(getContext(), ClaimActivity.class);
            startActivity(intent);
        }  else if (v.getId() == R.id.mLayoutPinjaman) {
            Intent intent = new Intent(getContext(), LoanActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutTelat) {
            Intent intent = new Intent(getContext(), AttendanceActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.img_cal) {
            String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid), ""));


            PreferenceManager.getDefaultSharedPreferences(getContext()).
                    edit().putString(getResources().getString(R.string.pref_employeeid_view), id)
                    .apply();
            Intent intent = new Intent(getContext(), ListHolidayActivity.class);
            startActivity(intent);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        loading.dismiss();
    }
}
