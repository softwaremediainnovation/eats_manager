package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.AssetNewActivity;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.R;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 01/03/2019.
 */

public class EditKadaluarsaFragment extends DialogFragment implements View.OnClickListener{

    private static final String TAG = CreateCutiFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;


    private EditText  mEdtMulai;


    private Button btnUpdate;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ImageView imgClose;


    private RelativeLayout rlLoading;



    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_edit_kadaluarsa, container, false);


        DecimalFormat formatter = new DecimalFormat("#,###,###");

        Bundle mArgs = getArguments();
        String amount = mArgs.getString("amount");
        String installment = mArgs.getString("installment");
        String installmentpayout = mArgs.getString("installmentpayout");



        // mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        // mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        // mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        // mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        imgClose = rootView.findViewById(R.id.imgClose);

        btnUpdate = rootView.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(this);







        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        messageDialog = new MessageDialog();
        imgClose.setOnClickListener(this);
        rlLoading = rootView.findViewById(R.id.rlLoading);
        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerStart();
            }
        });





        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_idProses_employee), ""));



        return rootView;
    }


    private void openDateRangePickerStart() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }


    public void mCreateDayOff() {

        loading.show();

        try {

            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_dayoff_idProses), "")));

            RequestBody expiryDate = RequestBody.create(MediaType.parse("text/plain"), mStartDate);


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.editAset(Authorization, id, expiryDate);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.hide();


                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    String intnet = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                            getResources().getString(R.string.pref_dayoff_notif), ""));

                                    if (intnet.equals("1")) {

                                        closeApplication();

                                    } else{

                                        getActivity().finish();

                                        Intent login = new Intent(getActivity(), AssetNewActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);

                                    }



                                }
                            });
                            alertDialog.show();
                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));


                        }
                        else {
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch ( Exception e){

                            }
                        }

                    } else {
                        loading.hide();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch ( Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.hide();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch ( Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.hide();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception f){

            }
        }

    }

    private void mAttemptCreateDayOff() {



        mEdtMulai.setError(null);



        String dates = mEdtMulai.getText().toString();


        boolean cancel = false;
        View focusView = null;




        // Check for a valid email address.
        if (TextUtils.isEmpty(dates)) {
            mEdtMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtMulai;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.string_confirmation))
                    .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                    .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCreateDayOff();
                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.txt_no), null)
                    .show();
        }
    }





    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnUpdate) {
            mAttemptCreateDayOff();
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }
    }

    private void closeApplication() {
        getActivity().finishAffinity();
        System.exit(0);
    }



}
