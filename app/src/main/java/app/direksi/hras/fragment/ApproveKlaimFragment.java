package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ClaimActivity;
import app.direksi.hras.model.ResponseDetailClaim;
import app.direksi.hras.model.ResponseDisbursement;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.model.User;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.Validation;
import app.direksi.hras.R;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 01/03/2019.
 */

public class ApproveKlaimFragment extends DialogFragment implements View.OnClickListener{

    private static final String TAG = CreateCutiFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;



    EditText mEdtNote, mJumlah;
    private Button btnUpdate, btnDel;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ImageView imgClose;
    private LinearLayout llLast;
    private TextView txtLastTitle, txtTanggalLast, txtJumlahHariLast,txtCreatorLast, txtCatatanLast, txtKeperluanLast ;
    private TableLayout tbLast;
    private RelativeLayout rlLoading;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_approve_klaim, container, false);


        DecimalFormat formatter = new DecimalFormat("#,###,###");

        Bundle mArgs = getArguments();
        String amount = mArgs.getString("amount");


        // mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        // mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        // mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        // mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mJumlah = rootView.findViewById(R.id.mJumlah);
        try {
            mJumlah.setText((formatter.format(Integer.parseInt(amount))).replace(".", ","));
        }catch (Exception e){
            mJumlah.setText("999,999,999");
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_jumlah_terlalu_besar_1milyar),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
        }
        imgClose = rootView.findViewById(R.id.imgClose);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        btnUpdate = rootView.findViewById(R.id.btnUpdate);
        btnDel = rootView.findViewById(R.id.btnDel);

        btnUpdate.setOnClickListener(this);
        btnDel.setOnClickListener(this);


        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        messageDialog = new MessageDialog();
        imgClose.setOnClickListener(this);

        tbLast = rootView.findViewById(R.id.tbLast);
        tbLast.setVisibility(View.GONE);
        llLast = rootView.findViewById(R.id.llLast);
        llLast.setVisibility(View.GONE);
        txtLastTitle = rootView.findViewById(R.id.txtLastTitle);
        txtTanggalLast = rootView.findViewById(R.id.txtTanggalLast);
        txtJumlahHariLast = rootView.findViewById(R.id.txtJumlahHariLast);
        txtCreatorLast = rootView.findViewById(R.id.txtCreatorLast);
        txtCatatanLast = rootView.findViewById(R.id.txtCatatanLast);
        txtKeperluanLast = rootView.findViewById(R.id.txtKeperluanLast);

        rlLoading = rootView.findViewById(R.id.rlLoading);


        final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0")) {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_bisa_dimulai_nol),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                   /* Toast.makeText(getActivity(),
                            "Tidak bisa dimulai dengan nol (0)",
                            Toast.LENGTH_SHORT).show();*/
                    if (enteredString.length() > 0) {
                        mJumlah.setText(enteredString.substring(1));
                    } else {
                        mJumlah.setText("");
                    }
                }

                if (enteredString.length() >= 12){
                    String str = enteredString.substring(0, 11);
                    mJumlah.setText(str);
                    mJumlah.setSelection(str.length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mJumlah.getText().hashCode() == s.hashCode()) {
                    mJumlah.removeTextChangedListener(this);
                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                    symbols.setGroupingSeparator(',');
                    try {
                        mJumlah.setText(formatter.format(Double.parseDouble(s.toString().replace(",", "") + "")));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    } catch (Exception e) {
                        mJumlah.setText(s.toString().replace(",", ""));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    }
                    mJumlah.addTextChangedListener(this);
                }

            }


        };
        mJumlah.addTextChangedListener(mPriceWatcher);




        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_idProses_employee), ""));

        getLast(Long.parseLong(id));

        return rootView;
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }


    public void mCreateDayOff(int status) {

        loading.show();

        try {

            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_dayoff_idProses), "")));
            RequestBody isApprove = RequestBody.create(MediaType.parse("text/plain"), Integer.toString(status));
            RequestBody comment = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));
            RequestBody Amount = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mJumlah).replaceAll(",", ""));
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.ApproveKlaim(Authorization, id, isApprove, comment, Amount);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.hide();

                          /*  new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), ClaimActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);



                                        }
                                    })
                                    .show();*/

                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    String intnet = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                            getResources().getString(R.string.pref_dayoff_notif), ""));

                                    if (intnet.equals("1")) {

                                        closeApplication();

                                    } else{

                                        getActivity().finish();

                                        Intent login = new Intent(getActivity(), ClaimActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);

                                    }



                                }
                            });
                            alertDialog.show();
                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));


                        }
                        else {
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch ( Exception e){

                            }
                        }

                    } else {
                        loading.hide();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch ( Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.hide();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch ( Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.hide();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception f){

            }
        }

    }



    private void mAttemptCreateDayOff( int status) {

        mJumlah.setError(null);
        mEdtNote.setError(null);
        String jumlah = mJumlah.getText().toString();
        String note = mEdtNote.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(jumlah)) {
            mJumlah.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mJumlah;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.string_confirmation))
                    .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                    .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCreateDayOff(status);
                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.txt_no), null)
                    .show();
        }
    }


    public void terimaCuti(View v) {
        mAttemptCreateDayOff( 1);

        /*new android.app.AlertDialog.Builder(getActivity())
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAttemptCreateDayOff();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();*/


    }

    public void tolakCuti(View v) {
        mAttemptCreateDayOff(0);

       /* new android.app.AlertDialog.Builder(getActivity())
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAttemptCreateDayOff();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();*/


    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnUpdate) {
            mAttemptCreateDayOff(1);
        }  else if (v.getId() == R.id.btnDel) {
            mAttemptCreateDayOff(0);
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }
    }

    private void closeApplication() {
        getActivity().finishAffinity();
        System.exit(0);
    }

    public void getLast(Long id) {

        // loading.show();

        try {

            rlLoading.setVisibility(View.VISIBLE);
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", String.valueOf(id));



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailClaim> call = api.getLastReimbursment(Authorization, data);

            call.enqueue(new Callback<ResponseDetailClaim>() {
                @Override
                public void onResponse(Call<ResponseDetailClaim> call, Response<ResponseDetailClaim> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            llLast.setVisibility(View.VISIBLE);
                            rlLoading.setVisibility(View.GONE);
                            if (response.body().getData() != null){

                                tbLast.setVisibility(View.VISIBLE);
                                txtLastTitle.setText(getResources().getString(R.string.title_penggantian_pengeluaran_terakhir));

                                if (response.body().getData().getDate() != null)
                                    txtTanggalLast.setText(DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getDate()));

                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                txtJumlahHariLast.setText(String.valueOf(response.body().getData().getAmount() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getData().getAmount()) : ""));
                                txtKeperluanLast.setText(response.body().getData().getReimbursementCategory().getName());

                                try {
                                    txtCreatorLast.setText(response.body().getData().getApprover().getFirstName() + " " + response.body().getData().getApprover().getLastName());
                                    txtCatatanLast.setText(response.body().getData().getNote());
                                } catch (Exception e){

                                }

                            }else {
                                rlLoading.setVisibility(View.GONE);
                                txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_penggantian_pengeluaran));
                                tbLast.setVisibility(View.GONE);
                            }
                            // loading.hide();




                        } else {

                            // loading.hide();
                            rlLoading.setVisibility(View.GONE);
                            llLast.setVisibility(View.VISIBLE);
                            txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_penggantian_pengeluaran));
                            tbLast.setVisibility(View.GONE);
                            /* Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();*/
                        }

                    } else {
                        // loading.hide();
                        rlLoading.setVisibility(View.GONE);
                        llLast.setVisibility(View.VISIBLE);
                        txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_penggantian_pengeluaran));
                        tbLast.setVisibility(View.GONE);
                        /*  Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();*/
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailClaim> call, Throwable t) {
                    //loading.hide();
                    rlLoading.setVisibility(View.GONE);
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception s){

                    }
                }
            });
        } catch (Exception e) {
            // loading.hide();
            rlLoading.setVisibility(View.GONE);
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch (Exception s){

            }
        }
    }

}

