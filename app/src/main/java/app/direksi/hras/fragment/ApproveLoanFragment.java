package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.ResponseDetailLoan;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.LoanActivity;
import app.direksi.hras.R;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.app.AppConstants.addMaxdaysApproveLoan;

/**
 * Created by dhimaz on 01/03/2019.
 */

public class ApproveLoanFragment extends DialogFragment implements View.OnClickListener{

    private static final String TAG = CreateCutiFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;


    private EditText mCicilan, mJumlah, mEdtMulai;
    TextView txtLastTitless;
    EditText mEdtNote;
    private Button btnUpdate, btnDel;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ImageView imgClose;

    private LinearLayout llLast;
    private TextView txtLastTitle, txtTanggalLast, txtJumlahLast, txtBonWeek,
            txtInstallmentLast, txtTerbayarLast, txtCicilanLast, txtStatusLast, txtNoteLast, txtCreatorLast, txtCatatanLast, txtSisaLast ;
    private TableLayout tbLast;
    private RelativeLayout rlLoading;
    String isMingguan = "";
    private String jamServer = "";
    private String loanStart = "";
    private String isApprove = "";
    private TextInputLayout mLayoutMulai;

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_approve_loan, container, false);


        DecimalFormat formatter = new DecimalFormat("#,###,###");

        Bundle mArgs = getArguments();
        String amount = mArgs.getString("amount");
        String installment = mArgs.getString("installment");
        String installmentpayout = mArgs.getString("installmentpayout");
        isMingguan = mArgs.getString("ismingguan");
        loanStart = mArgs.getString("loanStart");
        isApprove = mArgs.getString("isApprove");


        // mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        // mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        // mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        // mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        imgClose = rootView.findViewById(R.id.imgClose);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        btnUpdate = rootView.findViewById(R.id.btnUpdate);
        btnDel = rootView.findViewById(R.id.btnDel);

        btnUpdate.setOnClickListener(this);
        btnDel.setOnClickListener(this);

        mJumlah = rootView.findViewById(R.id.mJumlah);
        mCicilan = rootView.findViewById(R.id.mCicilan);
        txtLastTitless = rootView.findViewById(R.id.txtLastTitless);

        // mJumlah.setText((formatter.format(Integer.parseInt(amount))).replace(".", ","));
        try {
            mJumlah.setText((formatter.format(Integer.parseInt(amount))).replace(".", ","));
        }catch (Exception e){
            mJumlah.setText("999,999,999");
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_jumlah_terlalu_besar_1milyar),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
        }
        mCicilan.setText(installment);
        txtLastTitless.setText(getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " " + getResources().getString(R.string.title_rp) +  " " + (formatter.format(Integer.parseInt(installmentpayout))).replace(".", ","));




        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        messageDialog = new MessageDialog();
        imgClose.setOnClickListener(this);

        mCicilan.setOnClickListener(this);


        txtBonWeek = rootView.findViewById(R.id.txtBonWeek);
        if (isMingguan.equals("T")){
            txtBonWeek.setText("YA");
            mCicilan.setEnabled(false);
        }else {
            txtBonWeek.setText("TIDAK");
            mCicilan.setEnabled(true);
        }

        tbLast = rootView.findViewById(R.id.tbLast);
        tbLast.setVisibility(View.GONE);
        llLast = rootView.findViewById(R.id.llLast);
        llLast.setVisibility(View.GONE);
        txtLastTitle = rootView.findViewById(R.id.txtLastTitle);

        txtTanggalLast = rootView.findViewById(R.id.txtTanggalLast);
        txtJumlahLast = rootView.findViewById(R.id.txtJumlahLast);
        txtInstallmentLast = rootView.findViewById(R.id.txtInstallmentLast);
        txtTerbayarLast = rootView.findViewById(R.id.txtTerbayarLast);
        txtCicilanLast = rootView.findViewById(R.id.txtCicilanLast);
        txtStatusLast = rootView.findViewById(R.id.txtStatusLast);
        txtNoteLast = rootView.findViewById(R.id.txtNoteLast);
        txtCreatorLast = rootView.findViewById(R.id.txtCreatorLast);
        txtCatatanLast = rootView.findViewById(R.id.txtCatatanLast);
        txtSisaLast = rootView.findViewById(R.id.txtSisaLast);

        rlLoading = rootView.findViewById(R.id.rlLoading);

        final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String enteredString = s.toString();
                if (enteredString.startsWith("0")) {
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_bisa_dimulai_nol),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                  /*  Toast.makeText(getActivity(),
                            "Tidak bisa dimulai dengan nol (0)",
                            Toast.LENGTH_SHORT).show();*/
                    if (enteredString.length() > 0) {
                        mJumlah.setText(enteredString.substring(1));

                    } else {
                        mJumlah.setText("");
                    }
                } else {
                    DecimalFormat formatter = new DecimalFormat("#,###,###");
                    int length = mCicilan.getText().length();
                    if (length > 0 &&  mJumlah.getText().length() > 0 && enteredString.length() < 12 ){

                        if (isMingguan.equals("T")) {
                            String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b>"+ getResources().getString(R.string.title_rp)+ " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))) + "</b>");
                            txtLastTitless.setText(Html.fromHtml(value));
                            // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))));
                        } else {
                            String value = ( getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b>"+getResources().getString(R.string.title_rp) +" " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))) + "</b>");
                            txtLastTitless.setText(Html.fromHtml(value));
                            // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))));
                        }
                    }
                }


                if (enteredString.length() >= 12){
                    String str = enteredString.substring(0, 11);
                    mJumlah.setText(str);
                    mJumlah.setSelection(str.length());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (mJumlah.getText().hashCode() == s.hashCode()) {
                    mJumlah.removeTextChangedListener(this);
                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    DecimalFormatSymbols symbols = formatter.getDecimalFormatSymbols();
                    symbols.setGroupingSeparator(',');
                    try {
                        mJumlah.setText(formatter.format(Double.parseDouble(s.toString().replace(",", "") + "")));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    } catch (Exception e) {
                        mJumlah.setText(s.toString().replace(",", ""));
                        mJumlah.setSelection(mJumlah.getText().toString().length());
                    }
                    mJumlah.addTextChangedListener(this);
                }

            }


        };
        mJumlah.addTextChangedListener(mPriceWatcher);

        if (loanStart.equals("T")){

            mLayoutMulai.setHint(getResources().getString(R.string.title_tgl_mulai_potong_opsional));
           // mEdtMulai.setHint("Tgl mulai dipotong ( Opsional )");

        } else {

            if (isApprove.equals("N")){

                mLayoutMulai.setHint(getResources().getString(R.string.title_tgl_mulai_potong_opsional));
              //  mEdtMulai.setHint("Tgl mulai dipotong ( Opsional )");

            } else {

                mLayoutMulai.setHint(getResources().getString(R.string.title_tgl_mulai_potong_wajib));
             //   mEdtMulai.setHint("Tgl mulai dipotong ( Opsional )");
            }


        }

        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jamServer.equals("")) {
                    mGetServerTim();
                } else {
                    openDateRangePickerStart();
                }
            }
        });





        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_idProses_employee), ""));

        getLast(Long.parseLong(id));
        mGetServerTim();

        return rootView;
    }


    private void openDateRangePickerStart() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            calstart.setTime(date);
            calEnd.setTime(date);


            calstart.add(Calendar.DAY_OF_YEAR, 0);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdaysApproveLoan);


        } catch (Exception e) {

        }

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        options.setDateRange(calstart.getTimeInMillis(), calEnd.getTimeInMillis());
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        options.setDateParams(cal);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }


    public void mCreateDayOff(int status) {

        loading.show();

        try {

            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_dayoff_idProses), "")));
            RequestBody isApprove = RequestBody.create(MediaType.parse("text/plain"), Integer.toString(status));
            RequestBody comment = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtNote));
            RequestBody loanStartDate = RequestBody.create(MediaType.parse("text/plain"), mStartDate);

            RequestBody Amount = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mJumlah).replaceAll(",", ""));
            RequestBody Installment = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mCicilan).replaceAll("x", ""));

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.ApproveLoans(Authorization, id, isApprove, comment, Amount, Installment, loanStartDate);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.hide();

                           /* new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(getActivity(), LoanActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);



                                        }
                                    })
                                    .show();*/
                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    String intnet = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                            getResources().getString(R.string.pref_dayoff_notif), ""));

                                    if (intnet.equals("1")) {

                                        closeApplication();

                                    } else{

                                        getActivity().finish();

                                        Intent login = new Intent(getActivity(), LoanActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(login);

                                    }



                                }
                            });
                            alertDialog.show();
                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));


                        }
                        else {
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch ( Exception e){

                            }
                        }

                    } else {
                        loading.hide();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch ( Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.hide();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch ( Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.hide();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception f){

            }
        }

    }

    private void mAttemptCreateDayOff( int status) {


        mEdtMulai.setError(null);
        mJumlah.setError(null);
        mEdtNote.setError(null);
        String tglMulai = mEdtMulai.getText().toString();
        String jumlah = mJumlah.getText().toString();
        String note = mEdtNote.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (loanStart.equals("F")){

            if (isApprove.equals("T")){

                if (TextUtils.isEmpty(tglMulai)) {
                    mEdtMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
                    focusView = mEdtMulai;
                    cancel = true;
                }
            }

        }

        if (TextUtils.isEmpty(jumlah)) {
            mJumlah.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mJumlah;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.string_confirmation))
                    .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                    .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCreateDayOff(status);
                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.txt_no), null)
                    .show();
        }
    }


    public void terimaCuti(View v) {
        mAttemptCreateDayOff( 1);

        /*new android.app.AlertDialog.Builder(getActivity())
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAttemptCreateDayOff();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();*/


    }

    public void tolakCuti(View v) {
        mAttemptCreateDayOff(0);

       /* new android.app.AlertDialog.Builder(getActivity())
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAttemptCreateDayOff();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();*/


    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnUpdate) {
            mAttemptCreateDayOff(1);
        }  else if (v.getId() == R.id.btnDel) {
            mAttemptCreateDayOff(0);
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        } else if (v.getId() == R.id.mCicilan) {
            final String[] listrik = getResources().getStringArray(R.array.array_cicilan);

            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mCicilan.getWindowToken(), 0);
            final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
            b.setItems(listrik, (dialog, item) -> {
                mCicilan.setText("");
                mCicilan.setText(listrik[item]);
                DecimalFormat formatter = new DecimalFormat("#,###,###");
                int length = mJumlah.getText().length();
                if (length > 0){

                    if (isMingguan.equals("T")) {
                        String value = (getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b>"+ getResources().getString(R.string.title_rp)+ " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))) + "</b>");
                        txtLastTitless.setText(Html.fromHtml(value));
                        // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", ""))));
                    } else {
                        String value = (getResources().getString(R.string.title_cicilan_perbulan_sebesar) + " <b>"+ getResources().getString(R.string.title_rp)+ " " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))) + "</b>");
                        txtLastTitless.setText(Html.fromHtml(value));
                        // txtLastTitle.setText("Cicilan per bulan sebesar Rp " + formatter.format(Integer.parseInt(Validation.mGetText(mJumlah).replaceAll(",", "")) / Integer.parseInt(Validation.mGetText(mCicilan).replaceAll("x", ""))));
                    }

                }

                imm.hideSoftInputFromWindow(mCicilan.getWindowToken(), 0);
            }).show();
        }
    }

    private void closeApplication() {
        getActivity().finishAffinity();
        System.exit(0);
    }

    public void getLast(Long id) {

        // loading.show();

        try {

            rlLoading.setVisibility(View.VISIBLE);
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", String.valueOf(id));



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailLoan> call = api.getLastLoan(Authorization, data);

            call.enqueue(new Callback<ResponseDetailLoan>() {
                @Override
                public void onResponse(Call<ResponseDetailLoan> call, Response<ResponseDetailLoan> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            llLast.setVisibility(View.VISIBLE);
                            rlLoading.setVisibility(View.GONE);
                            if (response.body().getData() != null){

                                tbLast.setVisibility(View.VISIBLE);
                                txtLastTitle.setText("Total Pinjaman");

                             /*   if (response.body().getData().getDate() != null)
                                    txtTanggalLast.setText(DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getDate()));*/

                                Double temp = ( response.body().getData().getTotalPinjaman() != null ? response.body().getData().getTotalPinjaman() : 0) - ( response.body().getData().getTotalPembayaran() != null ? response.body().getData().getTotalPembayaran() : 0);


                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                txtSisaLast.setText(getResources().getString(R.string.title_rp) + " " + formatter.format(temp));
                                txtJumlahLast.setText(String.valueOf(response.body().getData().getTotalPinjaman() != null ? getResources().getString(R.string.title_rp) +  " " + formatter.format(response.body().getData().getTotalPinjaman()) : ""));
                                /*  txtInstallmentLast.setText(String.valueOf(response.body().getData().getInstallment() != null ?response.body().getData().getInstallment() + "x" : ""));*/
                                txtTerbayarLast.setText(String.valueOf(response.body().getData().getTotalPembayaran() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getData().getTotalPembayaran()) : ""));
                               /* txtCicilanLast.setText(String.valueOf(response.body().getData().getInstallmentAmount() != null ? "Rp " + formatter.format(response.body().getData().getInstallmentAmount()) : ""));
                                if (response.body().getData().getIsPaid()!= null) {
                                    txtStatusLast.setText(String.valueOf(response.body().getData().getIsPaid() == true ? "Lunas" : "Belum Lunas"));
                                } else {
                                    txtStatusLast.setText("");
                                }
                                txtNoteLast.setText(response.body().getData().getNote()!=null ? response.body().getData().getNote() : "");

                                try {
                                    txtCreatorLast.setText(response.body().getData().getApprover().getFirstName() + " " + response.body().getData().getApprover().getLastName());
                                    txtCatatanLast.setText(response.body().getData().getComment());
                                } catch (Exception e){

                                }*/




                            }else {
                                rlLoading.setVisibility(View.GONE);
                                txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_pinjaman));
                                tbLast.setVisibility(View.GONE);
                            }
                            // loading.hide();




                        } else {
                            // loading.hide();
                            rlLoading.setVisibility(View.GONE);
                            llLast.setVisibility(View.VISIBLE);
                            txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_pinjaman));
                            tbLast.setVisibility(View.GONE);
                            /* Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();*/
                        }

                    } else {
                        // loading.hide();
                        rlLoading.setVisibility(View.GONE);
                        llLast.setVisibility(View.VISIBLE);
                        txtLastTitle.setText(getResources().getString(R.string.title_tidak_ada_data_pinjaman));
                        tbLast.setVisibility(View.GONE);
                        /* Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();*/
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailLoan> call, Throwable t) {
                    //loading.hide();
                    rlLoading.setVisibility(View.GONE);
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch (Exception s){

                    }
                }
            });
        } catch (Exception e) {
            // loading.hide();
            rlLoading.setVisibility(View.GONE);
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch (Exception s){

            }
        }
    }

    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");*/

                            jamServer = response.body().getData();

                            // mTimeServer = date;

                            // Log.e(TAG, mTimeServer.toString());

                            // timer(mTimeServer);

                        }

                    } catch (Exception e) {
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }

    }

}
