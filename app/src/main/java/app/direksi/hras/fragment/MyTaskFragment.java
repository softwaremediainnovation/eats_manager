package app.direksi.hras.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.DetailTaskDailyActivity;
import app.direksi.hras.DetailTaskKuActivity;
import app.direksi.hras.R;
import app.direksi.hras.RecyclerTouchListener;
import app.direksi.hras.TaskActivity;
import app.direksi.hras.adapter.TaskAdapter;
import app.direksi.hras.model.DataTask;
import app.direksi.hras.model.DataTaskSummary;
import app.direksi.hras.model.ResponseDataTask;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

public class MyTaskFragment extends Fragment /*implements PenawaranDetailActivity.DataUpdateListener*/ {
    private RecyclerView mRootView;
    private List<DataTask> dataList = new ArrayList<>();
    private List<String> existLoad = new ArrayList<>();
    private TaskAdapter cAdapter;
    private TextView textNoInternet;
    private ProgressBar loading;
    com.google.android.material.floatingactionbutton.FloatingActionButton fab;
    private SwipeRefreshLayout swiperefresh;
    boolean isLoading = false;
    private int pageNumber = 1;
    int scrollPosition = 0 ;
    protected BarChart mChart;
    List<Hasil> hasilArrayList = new ArrayList<>();
    private Boolean isLoaded = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.my_list_task, container, false);
        mRootView = (RecyclerView) v.findViewById(R.id.my_recycler_view);
        textNoInternet = (TextView) v.findViewById(R.id.textNoInternet);
        loading = (ProgressBar) v.findViewById(R.id.loading);
        swiperefresh = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
        fab = v.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.FragmentManager fm = getActivity().getFragmentManager();
                CreateTaskFragment dialogFragment = new CreateTaskFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");


            }
        });
        String approval = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                getResources().getString(R.string.pref_CanViewTask), ""));
        if (approval.equals("true")){
            fab.show();
        } else {
            fab.hide();
        }


        loading.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        ((TaskActivity)getActivity()).setFragmentRefreshListener(new TaskActivity.FragmentRefreshListener2() {
            @Override
            public void onRefresh() {

                pageNumber = 1;
                dataList = new ArrayList<>();
                cAdapter = new TaskAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
                fetchService();
                initScrollListener();
            }
        });

        mChart = (BarChart) v.findViewById(R.id.chart1);
        mChart.getDescription().setEnabled(false);
        mChart.setMaxVisibleValueCount(40);
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
        mChart.setPinchZoom(false);
        mChart.setDrawGridBackground(false);
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.setHighlightFullBarEnabled(false);
        mChart.setNoDataTextColor(getResources().getColor(R.color.color_black));
        mChart.setNoDataText(getResources().getString(R.string.title_tidak_ada_data));
        mChart.setTouchEnabled(true);
        mChart.setDoubleTapToZoomEnabled(false);
        mChart.setScaleEnabled(false);
       // mChart.setHighlightPerTapEnabled(false);
     //   mChart.setOnChartValueSelectedListener(this);



        mChart.setOnChartValueSelectedListener( new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
               /* Toast.makeText(getContext(), "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex()
                        + ", Data index: " + h.getDataIndex() , Toast.LENGTH_SHORT).show();*/

              //  Toast.makeText(getContext(), "Value: " + hasilArrayList.get((int) e.getX()).getNama(), Toast.LENGTH_SHORT).show();

                int indeks = (int) e.getX();
                if (indeks == 0){

                    PreferenceManager.getDefaultSharedPreferences(getContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_status), "1")
                            .apply();

                } else if (indeks == 1){

                    PreferenceManager.getDefaultSharedPreferences(getContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_status), "2")
                            .apply();

                }else if (indeks == 2){

                    PreferenceManager.getDefaultSharedPreferences(getContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_status), "5")
                            .apply();

                }else if (indeks == 3){

                    PreferenceManager.getDefaultSharedPreferences(getContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_status), "4")
                            .apply();

                }
                RefreshListByStatus();

            }

            @Override
            public void onNothingSelected() {

            }
        });












        return v;
    }

    /*@Override
    public void onValueSelected(Entry e, Highlight h) {

        Toast.makeText(getContext(), "I clicked on " + String.valueOf(e.getData()), Toast.LENGTH_SHORT).show();
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", xIndex: " + e.getX()
                        + ", DataSet index: " + h.getDataSetIndex()
                        + ", Data index: " + h.getDataIndex());

    }

    @Override
    public void onNothingSelected() {

    }*/



    public class DayAxisValueFormatter extends ValueFormatter {
        private final BarLineChartBase<?> chart;
        public DayAxisValueFormatter(BarLineChartBase<?> chart) {
            this.chart = chart;
        }
        @Override
        public String getFormattedValue(float value) {
            return hasilArrayList .get((int)value).getNama();
        }
    }

    private int[] getColors() {

        int[] colors = {getResources().getColor(R.color.colorThemeOrangeStatus), getResources().getColor(R.color.colorThemeGreenStatus), getResources().getColor(R.color.colorThemeRedStatus),
                getResources().getColor(R.color.colorPrimary)};


        return colors;
    }

    public class Hasil{
        private String nama;
        private int total;

        public Hasil(String nama, int total){
            this.nama = nama;
            this.total = total;

        }

        public void setNama(String nama) {
            this.nama = nama;
        }
        public String getNama() {
            return nama;
        }
        public void setTotal(int total) {
            this.total = total;
        }
        public int getTotal() {
            return total;
        }
    }

    private void loadChart(DataTaskSummary dataTask)
    {

        Hasil ongoing = new Hasil( getResources().getString(R.string.title_belum_selesai), dataTask.getBlmSelesai());
        Hasil done = new Hasil(getResources().getString(R.string.title_selesai), dataTask.getSdhSelesai());
        Hasil kadaluarsa = new Hasil(getResources().getString(R.string.title_kadaluarsa), dataTask.getExpired());


        hasilArrayList.add(ongoing);
        hasilArrayList.add(done);
        hasilArrayList.add(kadaluarsa);



        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTextSize(12f);
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        leftAxis.setGranularity(1.0f);
        leftAxis.setGranularityEnabled(true);

        mChart.getAxisRight().setEnabled(false);

        final ArrayList<String> xAxisLabel = new ArrayList<>();
        for (int i = 0; i < hasilArrayList.size() ; i ++)
        {
            xAxisLabel.add(hasilArrayList.get(i).getNama());
        }



        ValueFormatter xAxisFormatter = new DayAxisValueFormatter(mChart);
        XAxis xLabels = mChart.getXAxis();
        xLabels.setTextSize(12f);
        xLabels.setPosition(XAxis.XAxisPosition.BOTTOM);
        xLabels.setGranularity(1f);
        xLabels.setValueFormatter(xAxisFormatter);



        mChart.setExtraOffsets(0f, 5f, 0f, 10f);

        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setFormSize(10f);
        l.setFormToTextSpace(6f);
        l.setXEntrySpace(8f);
        mChart.getLegend().setEnabled(false);


        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();




        for (int i = 0; i <hasilArrayList.size(); i++) {



            float val1 = (float)  hasilArrayList.get(i).getTotal();


            yVals1.add(new BarEntry(
                    i,
                    new float[]{val1}));
        }

        BarDataSet set1;


        if (mChart.getData() != null &&
                mChart.getData().getDataSetCount() > 0) {
            set1 = (BarDataSet) mChart.getData().getDataSetByIndex(0);
            set1.setHighLightColor(Color.TRANSPARENT);
            set1.setHighLightAlpha(0);
            set1.setValues(yVals1);
            mChart.getData().notifyDataChanged();
            mChart.notifyDataSetChanged();
        } else {
            set1 = new BarDataSet(yVals1, "");
            set1.setHighLightColor(Color.TRANSPARENT);
            set1.setHighLightAlpha(0);
            set1.setColors(getColors());
            set1.setStackLabels(new String[]{""});

            ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setHighlightEnabled(true);
            data.setValueFormatter(new LargeValueFormatter());
            data.setValueTextColor(Color.BLACK);
            data.setValueTextSize(12f);
           // data.setBarWidth(0.8f);




            mChart.setData(data);
        }
        mChart.setScaleMinima(1f, 1f);

       /* mChart.setFitBars(true);
        if (mChart.getData() != null) {
            mChart.getData().setHighlightEnabled(!mChart.getData().isHighlightEnabled());

        }*/
        for (IBarDataSet set : mChart.getData().getDataSets())
            ((BarDataSet) set).setBarBorderWidth(set.getBarBorderWidth() == 2.f ? 2.f : 2.f);
        mChart.animateXY(3000, 3000);
        mChart.invalidate();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initRecyclerView();

    }

    private void initRecyclerView() {
        cAdapter = new TaskAdapter(dataList, getActivity());
        mRootView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRootView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRootView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        mRootView.setAdapter(cAdapter);
        mRootView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), mRootView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                DataTask data = dataList.get(position);

                if (data.getFlag().equals("daily")){

                    PreferenceManager.getDefaultSharedPreferences(getContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                            .apply();
                    Intent mIntent = new Intent(getContext(), DetailTaskDailyActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);

                } else {

                    PreferenceManager.getDefaultSharedPreferences(getContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                            .apply();
                    Intent mIntent = new Intent(getContext(), DetailTaskKuActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);

                }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                pageNumber = 1;
                dataList = new ArrayList<>();
                cAdapter = new TaskAdapter(dataList, getActivity());
                mRootView.setAdapter(cAdapter);
                swiperefresh.setRefreshing(false);
                fetchService();
                initScrollListener();

            }});

        fetchService();
        initScrollListener();

    }

    private void initScrollListener() {
        mRootView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                try {
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                    if (!isLoading) {
                        if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == dataList.size() - 1) {
                            //bottom of list!
                            double mod = dataList.size() % Integer.parseInt(getResources().getString(R.string.count_paging));
                            if (mod > 0) {

                            } else {
                                loadMore();
                                isLoading = true;

                            }
                        }
                    }
                } catch (Exception e){

                }
            }
        });


    }

    private void loadMore() {
        dataList.add(null);
        cAdapter.notifyItemInserted(dataList.size() - 1);
        fetchService();

        /*Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                dataList.remove(dataList.size() - 1);
                scrollPosition = dataList.size();
                fetchService();
                } catch (Exception e){

                }

               *//* while (currentSize - 1 < nextLimit) {
                    rowsArrayList.add("Item " + currentSize);
                    currentSize++;
                }*//*

         *//* cAdapter.notifyDataSetChanged();
                isLoading = false;*//*
            }
        }, 2000);*/


    }

    private void closeMore(){
        try {
            dataList.remove(dataList.size() - 1);
            scrollPosition = dataList.size();
            fetchService();
        } catch (Exception e){

        }
    }



    public static Fragment newInstance() {
        return new MyTaskFragment();
    }


    private static class FakePageAdapter extends RecyclerView.Adapter<MyTaskFragment.FakePageVH> {
        private final int numItems;

        FakePageAdapter(int numItems) {
            this.numItems = numItems;
        }

        @Override
        public MyTaskFragment.FakePageVH onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_relative_employe, viewGroup, false);

            return new MyTaskFragment.FakePageVH(itemView);
        }

        @Override
        public void onBindViewHolder(MyTaskFragment.FakePageVH fakePageVH, int i) {
            // do nothing
        }

        @Override
        public int getItemCount() {
            return numItems;
        }
    }

    private static class FakePageVH extends RecyclerView.ViewHolder {
        FakePageVH(View itemView) {
            super(itemView);
        }
    }




  /*  @Override
    public void onDataUpdate(List<PenawaranDetail> mData) {

        if (mData.get(0).getPnwrAdv().size() > 0 )
        {
            klienList.clear();
            klienList.addAll(mData.get(0).getPnwrAdv());
            cAdapter.notifyDataSetChanged();
            Log.v("AdvFragment", "aaaaaaaaaaa");
        }
        // put your UI update logic here
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((PenawaranDetailActivity) activity).registerDataUpdateListener(this);
    }*/

    public void fetchService() {

        if (dataList.size() > 0){

        }else {
            loading.setVisibility(View.VISIBLE);
        }



       /* if (dataList.size() > 0 || existLoad.size() > 0)
        {
            if (dataList.size() > 0) {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.GONE);
            }
            else
            {
                loading.setVisibility(View.GONE);
                textNoInternet.setVisibility(View.VISIBLE);
            }
        }
        else {*/

        textNoInternet.setVisibility(View.GONE);
        try {
            Log.v("DoneFragment", "masuk try");

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Map<String, String> data = new HashMap<>();
            data.put("page", String.valueOf(pageNumber));
            data.put("size", getResources().getString(R.string.count_paging));
              /*  String approval = (PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_CanApproveOvertime), ""));
                if (approval.equals("true")){
                    data.put("Grab", "approval");
                } else {

                }*/




            if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty())
                data.put("start", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));

            if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_dayoff_end), "").isEmpty())
                data.put("end", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_end), ""));

            /*if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_dayoff_search), "").isEmpty())
                data.put("search", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_search), ""));*/



            String status =PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                    getResources().getString(R.string.pref_dayoff_status), "");



            if (status.equals("0")){



            } else if (status.equals("1")) {
                // Toast.makeText(ListCutiActivity.this, "pending", Toast.LENGTH_LONG).show();
                data.put("status", "process");


            } else if (status.equals("2")) {
                // Toast.makeText(ListCutiActivity.this, "approved", Toast.LENGTH_LONG).show();
                data.put("status", "finish");


            } else if (status.equals("3")) {
                // Toast.makeText(ListCutiActivity.this, "rejected", Toast.LENGTH_LONG).show();
                data.put("status", "mine");


            } else if (status.equals("4")) {
                // Toast.makeText(ListCutiActivity.this, "rejected", Toast.LENGTH_LONG).show();
                data.put("status", "daily");

            } else if (status.equals("5")) {
                // Toast.makeText(ListCutiActivity.this, "rejected", Toast.LENGTH_LONG).show();
                data.put("status", "expired");

            }

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataTask> call = api.taskFilter( Authorization, data);

            call.enqueue(new Callback<ResponseDataTask>() {
                @Override
                public void onResponse(Call<ResponseDataTask> call, Response<ResponseDataTask> response) {


                    if (response.isSuccessful()) {


                        if (pageNumber != 1) {
                            try {
                                dataList.remove(dataList.size() - 1);
                                scrollPosition = dataList.size();
                            } catch (Exception e){

                            }
                            try {
                                cAdapter.notifyItemRemoved(scrollPosition);
                            } catch (Exception e){

                            }
                        }
                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                        if (response.body().getErrCode() == 0) {

                            if(!isLoaded){
                                loadChart(response.body().getData());
                                isLoaded = true;
                            }
                            if (response.body().getData().getData().size() > 0) {
                                //  dataList.clear();

                                existLoad.add("Reload");
                                dataList.addAll(response.body().getData().getData());
                                cAdapter.notifyDataSetChanged();
                                pageNumber = pageNumber + 1;
                                isLoading = false;
                            } else {
                                existLoad.add("Reload");
                                if (dataList.size() > 0){

                                }else {
                                   // textNoInternet.setText(getActivity().getResources().getString(R.string.title_tidak_ada_data));
                                    textNoInternet.setVisibility(View.VISIBLE);
                                }
                            }
                            loading.setVisibility(View.GONE);
                        } else {
                            if (pageNumber != 1) {
                                try {
                                    dataList.remove(dataList.size() - 1);
                                    scrollPosition = dataList.size();
                                } catch (Exception e){

                                }
                                cAdapter.notifyItemRemoved(scrollPosition);
                            }
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception e)
                            {

                            }
                            loading.setVisibility(View.GONE);
                            isLoading = false;
                        }

                        loading.setVisibility(View.GONE);


                    } else {
                        if (pageNumber != 1) {
                            try {
                                dataList.remove(dataList.size() - 1);
                                scrollPosition = dataList.size();
                            } catch (Exception e){

                            }
                            cAdapter.notifyItemRemoved(scrollPosition);
                        }
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            //Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception e)
                        {

                        }
                        loading.setVisibility(View.GONE);
                        isLoading = false;
                        Log.v("DoneFragment", "tidak sukses");
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataTask> call, Throwable t) {
                    if (pageNumber != 1) {
                        try {
                            dataList.remove(dataList.size() - 1);
                            scrollPosition = dataList.size();
                        } catch (Exception e){

                        }
                        cAdapter.notifyItemRemoved(scrollPosition);
                    }
                    Log.v("DoneFragment", "on failure");
                    loading.setVisibility(View.GONE);
                    // Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e)
                    {

                    }
                    isLoading = false;
                  /*  if (isAdded())
                        Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
*/

                  /*  Snackbar.make(viewPos, R.string.loading_error, Snackbar.LENGTH_LONG)
                            .show();*/
                }
            });
        } catch (Exception e) {
            if (pageNumber != 1) {
                try {
                    dataList.remove(dataList.size() - 1);
                    scrollPosition = dataList.size();
                } catch (Exception s){

                }
                cAdapter.notifyItemRemoved(scrollPosition);
            }
            Log.v("DoneFragment", "masuk catch");
            if (isAdded())
                loading.setVisibility(View.GONE);
            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            isLoading = false;
            //  Snackbar.make(getActivity().getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
            // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();

        }
        /* }*/


    }

    private void RefreshListByStatus(){
        pageNumber = 1;
        dataList = new ArrayList<>();
        cAdapter = new TaskAdapter(dataList, getActivity());
        mRootView.setAdapter(cAdapter);
        fetchService();
        initScrollListener();
    }




}


