package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.TaskActivity;
import app.direksi.hras.adapter.StringAdapter;
import app.direksi.hras.model.DataFetchEmployeeItem;
import app.direksi.hras.model.PostTask;
import app.direksi.hras.model.PostTaskSchedule;
import app.direksi.hras.model.ResponseFetchEmployee;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.model.StringClass;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.app.AppConstants.addMaxdays1Year;
import static app.direksi.hras.app.AppConstants.addMindays1Day;
import static app.direksi.hras.app.AppConstants.addMindaysZero;

public class CreateTaskFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = CreateDayoffFragment.class.getSimpleName();
    private String mStartDate = "", mStartDateType = "", mEndDateType = "";
    private DateFormat sdf, mViewSdf, mViewSdfType;


    TextInputLayout mLayoutSelesai, mLayoutMulai, MulaiDate, SelesaiDate;
    EditText mEdtSelesai, mEdtMulai,
            mKeperluan,
            mEdtNote,
            mEdtCatatan,
            mHeader,
            txtDateMulai,txtDateSelesai ;

    TextInputLayout mReason;

    private Date dStart, dEnd;
    private Integer mIdConstanta[];
    private String mNamaConstanta[], valueConstanta [];
    private Button mButtonSend, mButtonCancel;
    private Integer constanta, mHowManyDays;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri = null;
    private SweetAlertDialog loading;
    private ImageView imgClose;

    private String jamServer = "",  mEndDateShadow = "";
    private LinearLayout llLast;
    private TextView txtLastTitle, txtNotice;

    RecyclerView recyclerView;
    private List<StringClass> dataList = new ArrayList<>();
    private StringAdapter cAdapter;
    private ScrollView scrollView;
    private List<DataFetchEmployeeItem> dataFetchEmployeeItems = new ArrayList<>();
    private String mIdEmployee = "";
    private LinearLayout llAdd;

    private Button btnMinggu, btnSenin, btnSelasa, btnRabu, btnKamis, btnJumat, btnSabtu;
    private Boolean bMinggu = false, bSenin = false, bSelasa = false, bRabu = false, bKamis = false, bJumat = false, bSabtu = false;
    private LinearLayout  llBtn;
    private RadioGroup rbtipe;

    private int tempTipe = 0;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_create_task, container, false);


        txtNotice = rootView.findViewById(R.id.txtNotice);
        txtNotice.setVisibility(View.GONE);
        recyclerView = rootView.findViewById(R.id.recyclerView);
        llAdd = rootView.findViewById(R.id.llAdd);
        MulaiDate = rootView.findViewById(R.id.MulaiDate);
        MulaiDate.setVisibility(View.GONE);
        SelesaiDate = rootView.findViewById(R.id.SelesaiDate);
        SelesaiDate.setVisibility(View.GONE);
        llBtn = rootView.findViewById(R.id.llBtn);
        llBtn.setVisibility(View.GONE);
        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mLayoutMulai.setVisibility(View.VISIBLE);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        mHeader = rootView.findViewById(R.id.mHeader);
        scrollView = rootView.findViewById(R.id.scrollView);
        mButtonCancel = rootView.findViewById(R.id.mButtonCancel);
        imgClose = rootView.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(this);
        mEdtCatatan = rootView.findViewById(R.id.mEdtCatatan);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jamServer.equals("")) {
                    mGetServerTim();
                } else {
                    openDateRangePickerStart();
                }

            }
        });

        txtDateMulai = rootView.findViewById(R.id.txtDateMulai);
        txtDateMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jamServer.equals("")) {
                    mGetServerTim();
                } else {
                    openDateRangePickerStartType();
                }

            }
        });

        txtDateSelesai = rootView.findViewById(R.id.txtDateSelesai);
        txtDateSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (jamServer.equals("")) {
                    mGetServerTim();
                } else {
                    openDateRangePickerEndType();
                }


            }
        });

        btnMinggu = rootView.findViewById(R.id.btnMinggu);
        btnMinggu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickMinggu();
            }
        });
        btnSenin = rootView.findViewById(R.id.btnSenin);
        btnSenin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSenin();
            }
        });
        btnSelasa = rootView.findViewById(R.id.btnSelasa);
        btnSelasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSelasa();
            }
        });
        btnRabu = rootView.findViewById(R.id.btnRabu);
        btnRabu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickRabu();
            }
        });
        btnKamis = rootView.findViewById(R.id.btnKamis);
        btnKamis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickKamis();
            }
        });
        btnJumat = rootView.findViewById(R.id.btnJumat);
        btnJumat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickJumat();
            }
        });
        btnSabtu = rootView.findViewById(R.id.btnSabtu);
        btnSabtu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSabtu();
            }
        });

        rbtipe = rootView.findViewById(R.id.rbtipe);
        rbtipe.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                View radioButton = rbtipe.findViewById(checkedId);
                int index = rbtipe.indexOfChild(radioButton);

                if(index == 0){

                    tempTipe = 0;
                    mLayoutMulai.setVisibility(View.VISIBLE);
                    txtNotice.setVisibility(View.GONE);
                    MulaiDate.setVisibility(View.GONE);
                    SelesaiDate.setVisibility(View.GONE);
                    llBtn.setVisibility(View.GONE);
                    ClearDay ();
                    ClearTanggal();

                } else if (index == 1){

                    tempTipe = 1;
                    mLayoutMulai.setVisibility(View.GONE);
                    txtNotice.setVisibility(View.VISIBLE);
                    MulaiDate.setVisibility(View.VISIBLE);
                    SelesaiDate.setVisibility(View.VISIBLE);
                    llBtn.setVisibility(View.GONE);
                    ClearDay ();
                    ClearTanggal();

                }else {

                    tempTipe = 2;
                    mLayoutMulai.setVisibility(View.GONE);
                    txtNotice.setVisibility(View.VISIBLE);
                    MulaiDate.setVisibility(View.VISIBLE);
                    SelesaiDate.setVisibility(View.VISIBLE);
                    llBtn.setVisibility(View.VISIBLE);
                    ClearDay ();
                    ClearTanggal();

                }
                // checkedId is the RadioButton selected
            }
        });


        cAdapter = new StringAdapter(dataList, getActivity());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(cAdapter);

        StringClass a = new StringClass("");
        dataList.add(a);
        cAdapter.notifyDataSetChanged();




        llAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StringClass a = new StringClass("");
                dataList.add(a);
                cAdapter.notifyDataSetChanged();
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);

                InputMethodManager immHide = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                immHide.hideSoftInputFromWindow(rootView.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);

                InputMethodManager imm = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(rootView, InputMethodManager.SHOW_IMPLICIT);

                /*InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);*/
            }
        });

        mEdtNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataFetchEmployeeItems.clear();
                showListEmployee();
            }
        });

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAttemptCreateDayOff();
            }
        });

        // Create an `ItemTouchHelper` and attach it to the `RecyclerView`


        // Extend the Callback class
        ItemTouchHelper.Callback _ithCallback = new ItemTouchHelper.Callback() {
            //and in your imlpementaion of
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                // get the viewHolder's and target's positions in your adapter data, swap them
                Collections.swap(dataList, viewHolder.getAdapterPosition(), target.getAdapterPosition());
                // and notify the adapter that its dataset has changed
                cAdapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                //TODO
            }

            //defines the enabled move directions in each state (idle, swiping, dragging).
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

                return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                        ItemTouchHelper.DOWN | ItemTouchHelper.UP | ItemTouchHelper.START | ItemTouchHelper.END);

            }

            @Override
            public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                super.clearView(recyclerView, viewHolder);
                // Action finished

                cAdapter.notifyDataSetChanged();
            }

        };

        ItemTouchHelper ith = new ItemTouchHelper(_ithCallback);
        ith.attachToRecyclerView(recyclerView);


        mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");
        mViewSdfType = new SimpleDateFormat("dd MMM yy");




        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mGetServerTim();

       /* final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/
        //insertEvent();

        scrollView.smoothScrollTo(0,0);
        return rootView;
    }

    private void ClearTanggal (){

        txtDateMulai.setText("");
        txtDateSelesai.setText("");
        mEdtMulai.setText("");
        mStartDateType = "";
        mEndDateType = "";
        mStartDate = "";

    }

    private void ClearDay (){

        bMinggu = true; bSenin = true; bSelasa = true; bRabu = true; bKamis = true; bJumat = true; bSabtu = true;
        clickMinggu();
        clickSenin();
        clickSelasa();
        clickRabu();
        clickKamis();
        clickJumat();
        clickSabtu();

    }

    private void clickMinggu(){

        if (bMinggu == false){
            btnMinggu.setTextColor(getResources().getColor(R.color.colorWhite));
            btnMinggu.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bMinggu = true;
        }
        else {
            btnMinggu.setTextColor(getResources().getColor(R.color.text_gray));
            btnMinggu.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bMinggu = false;
        }

    }

    private void clickSenin(){

        if (bSenin == false){
            btnSenin.setTextColor(getResources().getColor(R.color.colorWhite));
            btnSenin.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bSenin = true;
        }
        else {
            btnSenin.setTextColor(getResources().getColor(R.color.text_gray));
            btnSenin.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bSenin = false;
        }

    }

    private void clickSelasa(){

        if (bSelasa == false){
            btnSelasa.setTextColor(getResources().getColor(R.color.colorWhite));
            btnSelasa.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bSelasa = true;
        }
        else {
            btnSelasa.setTextColor(getResources().getColor(R.color.text_gray));
            btnSelasa.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bSelasa = false;
        }

    }

    private void clickRabu(){

        if (bRabu == false){
            btnRabu.setTextColor(getResources().getColor(R.color.colorWhite));
            btnRabu.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bRabu = true;
        }
        else {
            btnRabu.setTextColor(getResources().getColor(R.color.text_gray));
            btnRabu.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bRabu = false;
        }

    }

    private void clickKamis(){

        if (bKamis == false){
            btnKamis.setTextColor(getResources().getColor(R.color.colorWhite));
            btnKamis.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bKamis = true;
        }
        else {
            btnKamis.setTextColor(getResources().getColor(R.color.text_gray));
            btnKamis.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bKamis = false;
        }

    }

    private void clickJumat(){

        if (bJumat == false){
            btnJumat.setTextColor(getResources().getColor(R.color.colorWhite));
            btnJumat.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bJumat = true;
        }
        else {
            btnJumat.setTextColor(getResources().getColor(R.color.text_gray));
            btnJumat.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bJumat = false;
        }

    }

    private void clickSabtu(){

        if (bSabtu == false){
            btnSabtu.setTextColor(getResources().getColor(R.color.colorWhite));
            btnSabtu.setBackground(getResources().getDrawable(R.drawable.square_selected_day));
            bSabtu = true;
        }
        else {
            btnSabtu.setTextColor(getResources().getColor(R.color.text_gray));
            btnSabtu.setBackground(getResources().getDrawable(R.drawable.square_unselected_day));
            bSabtu = false;
        }

    }

    private void openDateRangePickerStart() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            calstart.setTime(date);
            calEnd.setTime(date);

            calstart.add(Calendar.DAY_OF_YEAR, addMindaysZero);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdays1Year);


        } catch (Exception e) {

        }

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        options.setDateRange(calstart.getTimeInMillis(), calEnd.getTimeInMillis());
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        options.setDateParams(cal);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    private void openDateRangePickerStartType() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDateType = sdf.format(selectedDate.getStartDate().getTime());
                txtDateMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            calstart.setTime(date);
            calEnd.setTime(date);

            calstart.add(Calendar.DAY_OF_YEAR, addMindaysZero);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdays1Year);


        } catch (Exception e) {

        }

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        options.setDateRange(calstart.getTimeInMillis(), calEnd.getTimeInMillis());
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        options.setDateParams(cal);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    private void openDateRangePickerEndType() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mEndDateType = sdf.format(selectedDate.getStartDate().getTime());
                txtDateSelesai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));
                mEndDateShadow = sdf.format(selectedDate.getStartDate().getTime());

            }


        });

        Calendar cal = null;
        Calendar calstart = null, calEnd = null;
        cal = Calendar.getInstance();
        calstart = Calendar.getInstance();
        calEnd = Calendar.getInstance();
        try {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Date date = dateFormat.parse(jamServer);
            cal = Calendar.getInstance();

            cal.setTime(date);
            cal.add(Calendar.DAY_OF_YEAR, addMindays1Day);
            calstart.setTime(date);
            calEnd.setTime(date);

            calstart.add(Calendar.DAY_OF_YEAR, addMindays1Day);
            calEnd.add(Calendar.DAY_OF_YEAR, addMaxdays1Year);


        } catch (Exception e) {

        }

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        options.setDateRange(calstart.getTimeInMillis(), calEnd.getTimeInMillis());
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        options.setDateParams(cal);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }


    private void showListEmployee() {


        final Dialog overlayInfo = new Dialog(getActivity());
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        overlayInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayInfo.setCancelable(true);
        final View view = inflater.inflate(R.layout.dialog_choose_emp, null);

        final EditText edtSearch = (EditText) view.findViewById(R.id.edtSearch);
        final Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        final TextView txtEmptyList = (TextView) view.findViewById(R.id.txtEmptyList);
        final ProgressBar loadingTemp = (ProgressBar) view.findViewById(R.id.loadingTemp);
        loadingTemp.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        final LinearLayout containerDialogAddress = (LinearLayout) view.findViewById(R.id.containerCleaner);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txtEmptyList.setVisibility(View.GONE);
                    loadingTemp.setVisibility(View.VISIBLE);
                    dataFetchEmployeeItems.clear();
                    containerDialogAddress.removeAllViews();

                    Map<String, String> data = new HashMap<>();
                    data.put("q", edtSearch.getText().toString().toLowerCase().trim());

                    String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_token), ""));


                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(getResources().getString(R.string.base_url))
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                    // simplified call to request the news with already initialized service
                    Call<ResponseFetchEmployee> call = api.mGetListEmpTask(Authorization, data);

                    call.enqueue(new Callback<ResponseFetchEmployee>() {
                        @Override
                        public void onResponse(Call<ResponseFetchEmployee> call, Response<ResponseFetchEmployee> response) {


                            if (response.isSuccessful()) {
                                int statusCode = response.body().getErrCode();
                                loadingTemp.setVisibility(View.GONE);
                                if (statusCode == 0) {

                                    Log.e(TAG, response.toString());

                                    dataFetchEmployeeItems = response.body().getDataFetchEmployee();


                                    for (int i = 0; i < dataFetchEmployeeItems.size(); i++) {
                                        final LayoutInflater layoutInflaterDetail = (LayoutInflater)
                                                getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        final View addDetail = layoutInflaterDetail.inflate(R.layout.item_emp, null);

                                        final TextView mTxtName = addDetail.findViewById(R.id.mTxtName);
                                        final TextView mTxtHidden = addDetail.findViewById(R.id.mTxtHidden);

                                        mTxtName.setText(dataFetchEmployeeItems.get(i).getFullNameNik() + "\n " +dataFetchEmployeeItems.get(i).getOrganizationName() );
                                        mTxtHidden.setText(Integer.toString(dataFetchEmployeeItems.get(i).getEmployeeID()));
                                        mTxtName.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mIdEmployee = mTxtHidden.getText().toString();
                                                //  Toast.makeText(getActivity(), mIdEmployee, Toast.LENGTH_SHORT).show();
                                                mEdtNote.setText(mTxtName.getText().toString());
                                                overlayInfo.dismiss();
                                                scrollView.fullScroll(ScrollView.FOCUS_UP);

                                            }
                                        });

                                        containerDialogAddress.addView(addDetail);
                                    }


                                    loadingTemp.setVisibility(View.GONE);
                                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);

                                } else {
                                    loadingTemp.setVisibility(View.GONE);
                                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                                }

                            } else {
                                loadingTemp.setVisibility(View.GONE);
                                txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseFetchEmployee> call, Throwable t) {
                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                        }
                    });


                } catch (Exception e) {
                    loadingTemp.setVisibility(View.GONE);
                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                }
            }
        });


        overlayInfo.setContentView(view);
        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(overlayInfo.getWindow().getAttributes());
        lp.width = width - 30;
        overlayInfo.show();
        overlayInfo.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }






    @Override
    public void onClick(View v) {
           if (v.getId() == R.id.imgClose) {
            dismiss();
        }

    }




    public void mCreateDayOff() {

        String id = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_employeeid), "");

        PostTask postTask;

        if (mStartDate.equals("")){

            postTask = new PostTask(mEdtCatatan.getText().toString(), "0001-01-01 00:00:00.0000000",Integer.valueOf(id) ,Integer.valueOf(mIdEmployee), mHeader.getText().toString(), dataList  );

        }else {

            postTask = new PostTask(mEdtCatatan.getText().toString(), mStartDate,Integer.valueOf(id) ,Integer.valueOf(mIdEmployee), mHeader.getText().toString(), dataList  );

        }

        String temp = postTask.toString();
        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseGeneral> call = api.PostTask(Authorization, postTask);

        call.enqueue(new Callback<ResponseGeneral>() {
            @Override
            public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    Intent login = new Intent(getActivity(), TaskActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));



                        } else {

                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }

                        }

                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception e){

                        }

                    }

                } catch (Exception e) {

                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception s){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                try {
                    loading.dismiss();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    redirected();
                } catch (Exception e){

                }

            }
        });
    }

    public void mCreateDayOffSchedule() {


        /*String note, String dueDate, int creatorID, int employeeID,String taskName,
        boolean senin, boolean selasa, boolean rabu, boolean kamis, boolean jumat, boolean sabtu, boolean minggu,
        int taskType, String dateStart, String dateEnd,*/

        String id = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_employeeid), "");

        PostTaskSchedule postTask;

        if (SelesaiDate.equals("")){

            if (mStartDate.equals("")){

                postTask = new PostTaskSchedule(mEdtCatatan.getText().toString(), "0001-01-01 00:00:00.0000000",
                        Integer.valueOf(id) ,Integer.valueOf(mIdEmployee), mHeader.getText().toString(),
                         bSenin, bSelasa, bRabu, bKamis, bJumat, bSabtu, bMinggu,
                        tempTipe, mStartDateType, "",  dataList  );

            }else {

                postTask = new PostTaskSchedule(mEdtCatatan.getText().toString(), mStartDate,
                        Integer.valueOf(id) ,Integer.valueOf(mIdEmployee), mHeader.getText().toString(),
                        bSenin, bSelasa, bRabu, bKamis, bJumat, bSabtu, bMinggu,
                        tempTipe, mStartDateType, "",  dataList  );

            }


        } else {

            if (mStartDate.equals("")){

                postTask = new PostTaskSchedule(mEdtCatatan.getText().toString(), "0001-01-01 00:00:00.0000000",
                        Integer.valueOf(id) ,Integer.valueOf(mIdEmployee), mHeader.getText().toString(),
                        bSenin, bSelasa, bRabu, bKamis, bJumat, bSabtu, bMinggu,
                        tempTipe, mStartDateType, mEndDateType,  dataList  );

            }else {

                postTask = new PostTaskSchedule(mEdtCatatan.getText().toString(), mStartDate,
                        Integer.valueOf(id) ,Integer.valueOf(mIdEmployee), mHeader.getText().toString(),
                        bSenin, bSelasa, bRabu, bKamis, bJumat, bSabtu, bMinggu,
                        tempTipe, mStartDateType, mEndDateType,  dataList  );

            }

        }



        String temp = postTask.toString();
        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseGeneral> call = api.PostTaskSchedule(Authorization, postTask);

        call.enqueue(new Callback<ResponseGeneral>() {
            @Override
            public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {

                try {

                    loading.dismiss();

                    if (response.isSuccessful()) {
                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    Intent login = new Intent(getActivity(), TaskActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));



                        } else {

                            try {
                                loading.dismiss();
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                                redirected();
                            } catch (Exception e){

                            }

                        }

                    } else {
                        try {
                            loading.dismiss();
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            redirected();
                        } catch (Exception e){

                        }

                    }

                } catch (Exception e) {

                    try {
                        loading.dismiss();
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        redirected();
                    } catch (Exception s){

                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                try {
                    loading.dismiss();
                    MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    //Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    redirected();
                } catch (Exception e){

                }

            }
        });
    }

    private void mAttemptCreateDayOff() {

        mHeader.setError(null);
        mEdtNote.setError(null);
        mEdtCatatan.setError(null);

        txtDateMulai.setError(null);
        txtDateSelesai.setError(null);



        String header = mHeader.getText().toString();
        String pegawai = mEdtNote.getText().toString();
        String catatan = mEdtCatatan.getText().toString();

        String start = txtDateMulai.getText().toString();
        String end = txtDateSelesai.getText().toString();


        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(catatan)) {
            mEdtCatatan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtCatatan;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(header)) {
            mHeader.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mHeader;
            cancel = true;
        }

        if (TextUtils.isEmpty(pegawai)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }

        if (dataList.size() > 0){

            for(int i = 0; i < dataList.size(); i++){
                if (dataList.get(i).getString().equals("") || dataList.get(i).getString() == null){

                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_item_tidak_boleh_kosong),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                    return;

                }
            }

        }else {
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_minimal_harus_ada_item),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            return;
        }

        if (tempTipe == 0){



        } else if (tempTipe == 1){

            if (TextUtils.isEmpty(start)) {
                txtDateMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
                focusView = txtDateMulai;
                cancel = true;
            }

            if (TextUtils.isEmpty(end)) {


            } else {

                if (!Validation.mIsValidDateyyMMdd(mStartDateType, mEndDateShadow)){
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tgl_mulai_lebih_besar),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                    return;
                }
            }

        } else {


            if (TextUtils.isEmpty(start)) {
                txtDateMulai.setError(getResources().getString(R.string.title_isian_harus_diisi));
                focusView = txtDateMulai;
                cancel = true;

            }

            if (TextUtils.isEmpty(end)) {


            } else {

                if (!Validation.mIsValidDateyyMMdd(mStartDateType, mEndDateShadow)){
                    MDToast.makeText(getActivity(), getResources().getString(R.string.title_tgl_mulai_lebih_besar),
                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                    return;
                }
            }


            if (bMinggu == true || bSenin == true || bSelasa == true || bRabu == true || bKamis == true || bJumat == true || bSabtu == true){

            }
            else {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_tgl_minimal_satu_hari_aktif),
                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                return;
            }

        }





        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            loading.show();

            if (tempTipe == 0){

                mCreateDayOff();

            } else {

                mCreateDayOffSchedule();

            }
            loading.show();
        }
    }



    void redirected (){

        Intent login = new Intent(getActivity(), TaskActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);
    }

    private void mGetServerTim() {
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                           /* sdf = new SimpleDateFormat("yyyy-MM-dd");
                            mViewSdf = new SimpleDateFormat("HH:mm");

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            Date date = dateFormat.parse(response.body().getData());
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");*/

                            jamServer = response.body().getData();

                            // mTimeServer = date;

                            // Log.e(TAG, mTimeServer.toString());

                            // timer(mTimeServer);

                        }

                    } catch (Exception e) {
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }

    }





}
