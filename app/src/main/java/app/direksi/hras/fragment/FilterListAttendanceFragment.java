package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import app.direksi.hras.DetailListAttendanceActivity;
import app.direksi.hras.R;
import app.direksi.hras.util.Validation;

/**
 * Created by dhimaz on 28/05/2019.
 */

public class FilterListAttendanceFragment extends DialogFragment {

    private static final String TAG = FilterTeguranFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;
    private ImageView imgClose;
    private int mIdConstanta;
    private String mNamaConstanta[];
    TextInputLayout mLayoutStatus;


    EditText mEdtSelesai, mEdtMulai, mEdtSearch, mEdtStatus;


    private Button mButtonSearch,
            mButtonClear;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_filter_list_attendance, container, false);
        mIdConstanta = 0;
        mNamaConstanta = new String[3];
        mNamaConstanta[0] = "Semua Status";
        mNamaConstanta[1] = "Telat";
        mNamaConstanta[2] = "Tidak telat";


        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mLayoutStatus = rootView.findViewById(R.id.mLayoutStatus);
        imgClose = rootView.findViewById(R.id.imgClose);
        mEdtStatus = rootView.findViewById(R.id.mEdtStatus);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtSearch = rootView.findViewById(R.id.mEdtSearch);
        mButtonSearch = rootView.findViewById(R.id.mButtonSearch);
        mButtonClear = rootView.findViewById(R.id.mButtonClear);


        mStartDate = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_start), "");
        mEndDate = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_end), "");


        mEdtMulai.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_start1), ""));
        mEdtSelesai.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_end1), ""));
        mEdtSearch.setText(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_search), ""));

        String status = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_status), "");

        if (status.equals("0")) {
            mEdtStatus.setText("Semua Status");


        } else if (status.equals("1")) {
            mEdtStatus.setText("Telat");


        } else if (status.equals("2")) {
            mEdtStatus.setText("Tidak telat");

        }

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("MMMM yyyy");

        mEdtStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(mEdtStatus.getWindowToken(), 0);
                final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        mIdConstanta = item;
                        // Toast.makeText(getActivity(), status, Toast.LENGTH_SHORT).show();

                        mEdtStatus.setText(mNamaConstanta[item]);
                        imm.hideSoftInputFromWindow(mLayoutStatus.getWindowToken(), 0);

                    }
                }).show();
            }
        });

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                testTanggal();

            }
        });

        mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePickerEnd();
            }
        });

        mButtonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_search), Validation.mGetText(mEdtSearch))
                        .putString(getResources().getString(R.string.pref_dayoff_start), mStartDate)
                        .putString(getResources().getString(R.string.pref_dayoff_end), mEndDate)
                        .putString(getResources().getString(R.string.pref_dayoff_start1), Validation.mGetText(mEdtMulai))
                        .putString(getResources().getString(R.string.pref_dayoff_end1), Validation.mGetText(mEdtSelesai))
                        .putString(getResources().getString(R.string.pref_dayoff_status), String.valueOf(mIdConstanta))
                        .apply();


                DetailListAttendanceActivity callingActivity = (DetailListAttendanceActivity) getActivity();
                callingActivity.onUserSelectValue("insert selected value here");
                dismiss();



            }
        });

        mButtonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mEdtMulai.setText("");
                mEdtSelesai.setText("");
                mEdtSearch.setText("");
                mEdtStatus.setText("Semua Status");
                mIdConstanta = 0;

                PreferenceManager.getDefaultSharedPreferences(getActivity()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                        .putString(getResources().getString(R.string.pref_dayoff_start), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                        .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                        .putString(getResources().getString(R.string.pref_dayoff_status), "0")
                        .apply();

                mStartDate = "";
                mEndDate = "";


            }
        });


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        Calendar calendar = Calendar.getInstance();

        int currentYear = calendar.get(Calendar.YEAR);
        int currentMonth = calendar.get(Calendar.MONTH) + 1;
        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);




        return rootView;
    }


    private void openDateRangePickerStart() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    private void openDateRangePickerEnd() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mEndDate = sdf.format(selectedDate.getEndDate().getTime());
                mEdtSelesai.setText(mViewSdf.format(selectedDate.getEndDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(false);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    private void testTanggal (){

        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(), new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                Log.d(TAG, "selectedMonth : " + selectedMonth + " selectedYear : " + selectedYear);


                Calendar c1 = Calendar.getInstance(); // August  16th, 2012 AD
                c1.set(Calendar.YEAR, selectedYear);             // August  16th,    0 AD
                c1.set(Calendar.MONTH, selectedMonth);
                c1.set(Calendar.DAY_OF_MONTH, 1); // January  1st,    0 AD
                // January  1st,    1 BC (corrected)


                mStartDate = sdf.format(c1.getTime());
                mEdtMulai.setText(mViewSdf.format(c1.getTime()));

                //  Toast.makeText(getActivity(), mStartDate, Toast.LENGTH_SHORT).show();
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setActivatedMonth(today.get(Calendar.MONTH))
                .setMinYear(today.get(Calendar.YEAR) - 10)
                .setActivatedYear(today.get(Calendar.YEAR))
                .setMaxYear(today.get(Calendar.YEAR) + 10)
                // .setMinMonth(Calendar.JANUARY)
                // .setTitle("Select trading month")
                //   .setMonthRange(Calendar.FEBRUARY, Calendar.NOVEMBER)
                // .setMaxMonth(Calendar.OCTOBER)
                // .setYearRange(1890, 1890)
                // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                //.showMonthOnly()
                // .showYearOnly()
                .setOnMonthChangedListener(new MonthPickerDialog.OnMonthChangedListener() {
                    @Override
                    public void onMonthChanged(int selectedMonth) {
                        Log.d(TAG, "Selected month : " + selectedMonth);
                        //  Toast.makeText(getActivity(), " Selected month : " + selectedMonth, Toast.LENGTH_SHORT).show();
                    }
                })
                .setOnYearChangedListener(new MonthPickerDialog.OnYearChangedListener() {
                    @Override
                    public void onYearChanged(int selectedYear) {
                        Log.d(TAG, "Selected year : " + selectedYear);
                        //  Toast.makeText(getActivity(), " Selected year : " + selectedYear, Toast.LENGTH_SHORT).show();
                    }
                })
                .build()
                .show();

    }










}

