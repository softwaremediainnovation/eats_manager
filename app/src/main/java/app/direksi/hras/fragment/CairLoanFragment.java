package app.direksi.hras.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.ClaimActivity;
import app.direksi.hras.LoanActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.ResponseDisbursement;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.model.ResultDisbursement;
import app.direksi.hras.model.ResultDisbursementLoan;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;


public class CairLoanFragment extends DialogFragment implements View.OnClickListener{


    EditText mEdtNote;
    private Button btnUpdate;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ImageView imgClose;
    private String amount, idRem, email, holder, norek,bank;

    private RadioGroup mGroupTransfer;
    private RadioButton mRadioTunai, mRadioTransfer;


    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_cair_loan, container, false);


        Bundle mArgs = getArguments();
        amount = mArgs.getString("amount");
        idRem = mArgs.getString("id");
        email = mArgs.getString("email");
        holder = mArgs.getString("holder");
        norek = mArgs.getString("norek");
        bank = mArgs.getString("bank");


        imgClose = rootView.findViewById(R.id.imgClose);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        btnUpdate = rootView.findViewById(R.id.btnUpdate);

        mGroupTransfer = rootView.findViewById(R.id.mGroupTransfer);
        mRadioTunai = rootView.findViewById(R.id.mRadioTunai);
        mRadioTransfer = rootView.findViewById(R.id.mRadioTransfer);


        btnUpdate.setOnClickListener(this);


        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        messageDialog = new MessageDialog();
        imgClose.setOnClickListener(this);









        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }


    public void mCreateDayOff(ResponseDisbursement disbursement) {

        try {
            long idRemburse = Long.parseLong (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_dayoff_idProses), ""));

            ResultDisbursementLoan resultDisbursement;

            if (disbursement == null){

                resultDisbursement = new ResultDisbursementLoan("", "", "", 0,
                        "", "", "", idRemburse, Validation.mGetText(mEdtNote),
                        "0",null, null);

            }else {
                resultDisbursement = new ResultDisbursementLoan(disbursement.getStatus(), disbursement.getUserId(), disbursement.getExternalId(), disbursement.getAmount(),
                        disbursement.getBankCode(), disbursement.getAccountHolderName(), disbursement.getDisbursementDescription(), idRemburse, Validation.mGetText(mEdtNote),
                        disbursement.getId(),disbursement.getEmailTo(), disbursement.getEmailCc());
            }


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.CairLoan(Authorization, resultDisbursement);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.hide();
                            PreferenceManager.getDefaultSharedPreferences(getActivity()).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();



                            SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            if (disbursement!= null){
                                alertDialog.setContentText( getResources().getString(R.string.title_transfer_berhasil) +  " " + disbursement.getStatus());

                            }else {
                                alertDialog.setContentText("");
                            }
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                                        String intnet = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                                getResources().getString(R.string.pref_dayoff_notif), ""));

                                        if (intnet.equals("1")) {

                                            closeApplication();

                                        } else{

                                            getActivity().finish();

                                            Intent login = new Intent(getActivity(), LoanActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);

                                        }



                                    }
                                });
                                alertDialog.show();
                                Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                btn.setTextColor(getResources().getColor(R.color.colorWhite));





                        }
                        else {
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch ( Exception e){

                            }
                        }

                    } else {
                        loading.hide();
                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch ( Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.hide();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch ( Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.hide();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception f){

            }
        }

    }


    public String getBase64(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.NO_WRAP);
    }

    public void transferXendit() {


        try {

            List<String> email_to = new ArrayList<String>();
           // email_to.add("dhimazruntunuwu@gmail.com");
            email_to.add(email);
            email_to.add((PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_email), "")));

            List<String> email_cc = new ArrayList<String>();
            email_cc.add("percobaanemailblast@gmail.com");
            // email_cc.add("rudy.130390@gmail.com");

            String username = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_key_xendit), ""));
            String password = "";
            String svcCredentials = "Basic "+ getBase64(username + ":" + password);

            ResponseDisbursement disbursement = new ResponseDisbursement( "L_"+ idRem,Integer.parseInt(amount) ,
                    bank,holder,norek,Validation.mGetText(mEdtNote) + "( by apps )", email_to, email_cc);


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url_xendid))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDisbursement> call = api.requestDisbursement(svcCredentials, disbursement);

            call.enqueue(new Callback<ResponseDisbursement>() {
                @Override
                public void onResponse(Call<ResponseDisbursement> call, Response<ResponseDisbursement> response) {


                    if (response.isSuccessful()) {
                        String statusCode = response.body().getErrorCode();

                        if (statusCode == null) {

                            mCreateDayOff(response.body());


                        }
                        else {
                            loading.hide();
                            try {
                                MDToast.makeText(getActivity(), response.body().getMessage(), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                            } catch ( Exception e){

                            }
                        }

                    } else {
                        loading.hide();
                        try {

                            Gson gson = new Gson();
                            Type type = new TypeToken<ResponseDisbursement>() {}.getType();
                            ResponseDisbursement errorResponse = gson.fromJson(response.errorBody().charStream(),type);

                            MDToast.makeText(getActivity(), errorResponse.getMessage(), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                        } catch ( Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDisbursement> call, Throwable t) {
                    loading.hide();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                    } catch ( Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.hide();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception f){

            }
        }

    }

    private void mAttemptCreateDayOff( int status) {

        mEdtNote.setError(null);
        String note = mEdtNote.getText().toString();
        String keyxendit = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_key_xendit), ""));

        boolean cancel = false;
        View focusView = null;



        // Check for a valid email address.
        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }

        if (mRadioTransfer.isChecked() == true && (bank == null || norek == null || holder == null)){

            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_data_rekening_tidak_lengkap), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception e){

            }
            return;
        }

        if (mRadioTransfer.isChecked() == true && keyxendit.equals("")){

            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.title_tidak_punya_token_xendit), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
            } catch ( Exception e){

            }
            return;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {

            String temp = "";
            if (mRadioTransfer.isChecked()){
                temp = " " + getResources().getString(R.string.title_transfer_ke) +  " "+ bank + " - " + norek + " - "+ holder;
            }

            new android.app.AlertDialog.Builder(getActivity())
                    .setTitle(getResources().getString(R.string.string_confirmation))
                    .setMessage(getResources().getString(R.string.title_apakah_anda_yakin_tanpa_koma) + temp + " ?")
                    .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            loading.show();
                            if (mRadioTunai.isChecked()){

                                mCreateDayOff(null);
                            }
                            else {

                                transferXendit();

                            }
                        }

                    })
                    .setNegativeButton(getResources().getString(R.string.txt_no), null)
                    .show();
        }
    }


    public void terimaCuti(View v) {
        mAttemptCreateDayOff( 1);

        /*new android.app.AlertDialog.Builder(getActivity())
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAttemptCreateDayOff();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();*/


    }

    public void tolakCuti(View v) {
        mAttemptCreateDayOff(0);

       /* new android.app.AlertDialog.Builder(getActivity())
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAttemptCreateDayOff();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();*/


    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.btnUpdate) {
            mAttemptCreateDayOff(1);
        }  else if (v.getId() == R.id.btnDel) {
            mAttemptCreateDayOff(0);
        }  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }
    }

    private void closeApplication() {
        getActivity().finishAffinity();
        System.exit(0);
    }



}

