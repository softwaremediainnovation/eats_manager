package app.direksi.hras.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import com.google.android.material.textfield.TextInputLayout;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.AssetNewActivity;
import app.direksi.hras.R;
import app.direksi.hras.WarningLetterActivity;
import app.direksi.hras.model.DataFetchEmployeeItem;
import app.direksi.hras.model.ResponseDataLastReprimand;
import app.direksi.hras.model.ResponseEmploye;
import app.direksi.hras.model.ResponseFetchEmployee;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.model.ResponseKategori;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

/**
 * Created by dhimaz on 29/05/2019.
 */

public class HandoverFragment  extends DialogFragment implements View.OnClickListener , AdapterView.OnItemSelectedListener{

    private static final String TAG = CreateTeguranFragment.class.getSimpleName();
    private String mStartDate = "", mEndDate = "";
    private DateFormat sdf, mViewSdf;
    private SearchableSpinner spinnerSearchable;
    private ArrayAdapter<String> adapter;
    private List<String> data;
    private int check = 0;
    TextInputLayout mLayoutSelesai, mLayoutMulai;
    EditText mEdtSelesai, mEdtMulai,
            mKeperluan,
            mEdtNote,
            txtCatatan;
    private TableLayout tbLast;

    TextInputLayout mReason;

    private Long mIdConstanta[], mIdEmploye[];
    private String mNamaConstanta[], mNamaEmployee[];
    private Button mButtonSend;
    private Long constanta, idEmployee;
    private ImageView mImageView, mImageChoose;
    private Uri returnUri;
    private SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private Boolean flag = false;
    private ImageView imgClose;
    private LinearLayout llLast;
    private TextView txtLastTitle, txtTanggalLast, txtKategoriLast,txtCreatorLast, txtCatatanLast ;
    private List<DataFetchEmployeeItem> dataFetchEmployeeItems = new ArrayList<>();
    private String mIdEmployee = "";

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {



        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(root);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


        return dialog;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_handover_aset, container, false);
        imgClose = rootView.findViewById(R.id.imgClose);
        mLayoutMulai = rootView.findViewById(R.id.mLayoutMulai);
        mLayoutSelesai = rootView.findViewById(R.id.mLayoutSelesai);
        mEdtSelesai = rootView.findViewById(R.id.mEdtSelesai);
        mEdtMulai = rootView.findViewById(R.id.mEdtMulai);
        mEdtNote = rootView.findViewById(R.id.mEdtNote);
        txtCatatan = rootView.findViewById(R.id.txtCatatan);
        mButtonSend = rootView.findViewById(R.id.mButtonSend);
        mImageView = rootView.findViewById(R.id.mImageView);
        mImageChoose = rootView.findViewById(R.id.mImageChoose);

        tbLast = rootView.findViewById(R.id.tbLast);
        llLast = rootView.findViewById(R.id.llLast);
        llLast.setVisibility(View.GONE);
        txtLastTitle = rootView.findViewById(R.id.txtLastTitle);
        txtTanggalLast = rootView.findViewById(R.id.txtTanggalLast);
        txtKategoriLast = rootView.findViewById(R.id.txtKategoriLast);
        txtCreatorLast = rootView.findViewById(R.id.txtCreatorLast);
        txtCatatanLast = rootView.findViewById(R.id.txtCatatanLast);

        spinnerSearchable = (SearchableSpinner) rootView.findViewById(R.id.spinner1);
        spinnerSearchable.setTitle(getResources().getString(R.string.title_pilih_pegawai));
        spinnerSearchable.setOnItemSelectedListener(this);

        messageDialog = new MessageDialog();

        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        sdf = new SimpleDateFormat("yyyy-MM-dd");
        mViewSdf = new SimpleDateFormat("dd MMMM yyyy");

        mLayoutMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mEdtMulai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mEdtSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mLayoutSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDateRangePicker();
            }
        });

        mKeperluan = rootView.findViewById(R.id.mKeperluan);
        mKeperluan.setOnClickListener(this);
       // mEdtNote.setOnClickListener(this);
        mEdtNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataFetchEmployeeItems.clear();
                showListEmployee();
            }
        });
        mButtonSend.setOnClickListener(this);
        imgClose.setOnClickListener(this);

        mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

        mImageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 2000);
                } else {
                    // startGallery();
                    startCropImageActivity();
                }
            }
        });

       /* loading.show();*//*
        mGetEmploye();
        mGetConstanta();*/


        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(getActivity().getWindow().getAttributes());
        lp.width = width - 30;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    private void startCropImageActivity() {
       /* CropImage.activity()
                .start(getActivity(), this);*/
    }

    void startGallery() {
        Intent cameraIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(cameraIntent, 1000); // you should define a constant instead of 1
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == getActivity().RESULT_OK) {
                // Uri resultUri = result.getUri();
                returnUri = result.getUri();
                Bitmap bitmapImage = null;

                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                } catch (IOException e) {
                    Log.v(TAG, e.getMessage());
                }

                Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, bitmapImage);

                String a = Path.getRealPathFromURI(getActivity(), returnUri);

                try {

                    mImageChoose.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(android.R.color.transparent);
                    mImageView.setImageBitmap(BitmapImage.modifyOrientation(scaled, a));

                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }*/
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (resultCode == getActivity().RESULT_OK) {
            if (requestCode == 1000) {
                returnUri = data.getData();
                Bitmap bitmapImage = null;

                try {
                    bitmapImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), returnUri);
                } catch (IOException e) {
                    Log.v(TAG, e.getMessage());
                }

                Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, bitmapImage);

                String a = Path.getRealPathFromURI(getActivity(), returnUri);

                try {

                    mImageChoose.setVisibility(View.GONE);
                    mImageView.setVisibility(View.VISIBLE);
                    mImageView.setImageResource(android.R.color.transparent);
                    mImageView.setImageBitmap(BitmapImage.modifyOrientation(scaled, a));

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }*/


    private void openDateRangePicker() {
        SublimePickerFragment pickerFrag = new SublimePickerFragment();
        pickerFrag.setCallback(new SublimePickerFragment.Callback() {
            @Override
            public void onCancelled() {

            }

            @Override
            public void onDateTimeRecurrenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {

                mStartDate = sdf.format(selectedDate.getStartDate().getTime());
                mEndDate = sdf.format(selectedDate.getEndDate().getTime());



                mEdtMulai.setText(mViewSdf.format(selectedDate.getStartDate().getTime()));
                mEdtSelesai.setText(mViewSdf.format(selectedDate.getEndDate().getTime()));


            }


        });

        long now = System.currentTimeMillis() - 1000;
        // ini configurasi agar library menggunakan method Date Range Picker
        SublimeOptions options = new SublimeOptions();
        options.setCanPickDateRange(true);
        // options.setPickerToShow(SublimeOptions.Picker.DATE_PICKER);
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        // options.setDateRange( Long.MIN_VALUE , now);

        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", options);
        pickerFrag.setArguments(bundle);

        pickerFrag.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        pickerFrag.show(getFragmentManager(), "SUBLIME_PICKER");
    }

    public void mGetConstanta() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseKategori> call = api.getCategory(Authorization);

            call.enqueue(new Callback<ResponseKategori>() {
                @Override
                public void onResponse(Call<ResponseKategori> call, Response<ResponseKategori> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {
                                mIdConstanta = new Long[response.body().getData().size()];
                                mNamaConstanta = new String[response.body().getData().size()];

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mIdConstanta[i] = response.body().getData().get(i).getReprimandCategoryID();
                                    mNamaConstanta[i] = response.body().getData().get(i).getName() +  " ( "+ response.body().getData().get(i).getNote() + " )";
                                }
                                loading.hide();

                            } else {
                                loading.hide();
                                mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                        , response.body().getErrMessage());
                            }

                        } else {
                            loading.hide();
                            mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.hide();
                        mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                , response.body().getErrMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseKategori> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        /*if ( flag ) {

        } else {
            loading.show();
            mGetEmploye();
            flag = true;
        }*/





    }

    public void mGetEmploye() {


        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseEmploye> call = api.getEmploye(Authorization);

            call.enqueue(new Callback<ResponseEmploye>() {
                @Override
                public void onResponse(Call<ResponseEmploye> call, Response<ResponseEmploye> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            if (response.body().getData().size() > 0) {
                                mIdEmploye = new Long[response.body().getData().size()];
                                mNamaEmployee = new String[response.body().getData().size()];

                                data = new ArrayList<>();

                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mIdEmploye[i] = response.body().getData().get(i).getEmployeeID();
                                    mNamaEmployee[i] = response.body().getData().get(i).getFirstName() +  " "+ response.body().getData().get(i).getLastName();
                                    data.add (response.body().getData().get(i).getFirstName() +  " "+ response.body().getData().get(i).getLastName());
                                }
                                adapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item, data);
                                spinnerSearchable.setAdapter(adapter);

                                mGetConstanta();

                            } else {
                                loading.hide();
                                mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                        , response.body().getErrMessage());
                            }

                        } else {
                            loading.hide();
                            mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.hide();
                        mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                                , response.body().getErrMessage());
                    }
                }

                @Override
                public void onFailure(Call<ResponseEmploye> call, Throwable t) {
                    loading.hide();
                    mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.hide();
            mShowMessageError(getActivity(), getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }
    }

    public void mCreateTeguran() {

        loading.show();


        RequestBody AssetID = RequestBody.create(MediaType.parse("text/plain"), (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_dayoff_idProses), "")));
        RequestBody DestinationHolderID = RequestBody.create(MediaType.parse("text/plain"), mIdEmployee);
        RequestBody Note = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(txtCatatan));



        //File creating from selected URL


        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                getResources().getString(R.string.pref_token), ""));





        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseGeneral> call = api.createHandOver(Authorization, AssetID , DestinationHolderID, Note);

        call.enqueue(new Callback<ResponseGeneral>() {
            @Override
            public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {

                Log.v(TAG, response.toString());

                if (response.isSuccessful()) {
                    long statusCode = response.body().getErrCode();

                    if (statusCode == 0) {

                        loading.hide();


                        SweetAlertDialog alertDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                        alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                        alertDialog.setContentText("");
                        alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {


                                String intnet = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                                        getResources().getString(R.string.pref_dayoff_notif), ""));

                                if (intnet.equals("1")) {

                                    closeApplication();

                                } else{

                                    getActivity().finish();

                                    Intent login = new Intent(getActivity(), AssetNewActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);

                                }



                            }
                        });
                        alertDialog.show();

                        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                        btn.setTextColor(getResources().getColor(R.color.colorWhite));



                    } else {
                        loading.hide();
                        messageDialog.mShowMessageError(getActivity(),getResources().getString(R.string.title_gagal)
                                , "");

                    }

                } else {
                    loading.hide();
                   /* mShowMessageErrorSave(getActivity(), "GAGAL"
                            , response.body().getErrMessage());*/
                    messageDialog.mShowMessageError(getActivity(),getResources().getString(R.string.title_gagal)
                            , "");

                }
            }

            @Override
            public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                loading.hide();
               /* mShowMessageErrorSave(getActivity(), "GAGAL"
                        , getResources().getString(R.string.loading_error));*/
                messageDialog.mShowMessageError(getActivity(),getResources().getString(R.string.title_gagal)
                        , "");

            }
        });

    }

    private void closeApplication() {
        getActivity().finishAffinity();
        System.exit(0);
    }

    private void mAttemptCreateDayOff() {

        mEdtMulai.setError(null);
        mEdtSelesai.setError(null);
        mKeperluan.setError(null);
        mEdtNote.setError(null);
        txtCatatan.setError(null);


        String start = mEdtMulai.getText().toString();
        String end = mEdtSelesai.getText().toString();
        String reason = mKeperluan.getText().toString();
        String note = mEdtNote.getText().toString();
        String catatan = txtCatatan.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
       /* if (TextUtils.isEmpty(start)) {
            mEdtMulai.setError(getString(R.string.start_date_blank));
            focusView = mEdtMulai;
            cancel = true;
        }

        if (TextUtils.isEmpty(end)) {
            mEdtSelesai.setError(getString(R.string.end_date_blank));
            focusView = mEdtSelesai;
            cancel = true;
        }*/

       /* if (TextUtils.isEmpty(reason)) {
            mKeperluan.setError("Kategori harus diisi");
            focusView = mKeperluan;
            cancel = true;
        }*/
        if (TextUtils.isEmpty(catatan)) {
            txtCatatan.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = txtCatatan;
            cancel = true;
        }

        if (TextUtils.isEmpty(note)) {
            mEdtNote.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = mEdtNote;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            mCreateTeguran();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mKeperluan) {
            try {
                if (mIdConstanta.length == 0) {
                    mGetConstanta();
                } else {
                    final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);
                    final AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setItems(mNamaConstanta, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            mKeperluan.setText(mNamaConstanta[item]);

                            constanta = mIdConstanta[item];


                            imm.hideSoftInputFromWindow(mKeperluan.getWindowToken(), 0);

                        }
                    }).show();
                }
            } catch (Exception e) {
                mGetConstanta();
            }
        } else if (v.getId() == R.id.mButtonSend) {
            mAttemptCreateDayOff();
        } /* else if (v.getId() == R.id.mEdtNote) {
            MotionEvent motionEvent = MotionEvent.obtain( 0, 0, MotionEvent.ACTION_UP, 0, 0, 0 );
            spinnerSearchable.dispatchTouchEvent(motionEvent);
        }*/  else if (v.getId() == R.id.imgClose) {
            dismiss();
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = parent.getItemAtPosition(position).toString();

        if (check == 0)
        {
            check = 1;
            //  mEdtNote.setText(selectedItem);
        }
        else
        {
            idEmployee = mIdEmploye[position];
            // Toast.makeText(getActivity(), idEmployee.toString(), Toast.LENGTH_SHORT).show();
          //  getLast(idEmployee);
            mEdtNote.setText(selectedItem);
        }



    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void mShowMessageError(Activity activity, String title, String message) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .setConfirmText("OK")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                        Intent login = new Intent(getActivity(), WarningLetterActivity.class);
                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(login);
                    }
                })
                .show();
    }

    public void mShowMessageErrorSave(Activity activity, String title, String message) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();
    }



    private void showListEmployee() {


        final Dialog overlayInfo = new Dialog(getActivity());
        overlayInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        overlayInfo.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        overlayInfo.setCancelable(true);
        final View view = inflater.inflate(R.layout.dialog_choose_emp, null);

        final EditText edtSearch = (EditText) view.findViewById(R.id.edtSearch);
        final Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        final TextView txtEmptyList = (TextView) view.findViewById(R.id.txtEmptyList);
        final ProgressBar loadingTemp = (ProgressBar) view.findViewById(R.id.loadingTemp);
        loadingTemp.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        final LinearLayout containerDialogAddress = (LinearLayout) view.findViewById(R.id.containerCleaner);


        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    txtEmptyList.setVisibility(View.GONE);
                    loadingTemp.setVisibility(View.VISIBLE);
                    dataFetchEmployeeItems.clear();
                    containerDialogAddress.removeAllViews();

                    Map<String, String> data = new HashMap<>();
                    data.put("q", edtSearch.getText().toString().toLowerCase().trim());

                    String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                            getResources().getString(R.string.pref_token), ""));


                    Retrofit retrofit = new Retrofit.Builder()
                            .baseUrl(getResources().getString(R.string.base_url))
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();

                    RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                    // simplified call to request the news with already initialized service
                    Call<ResponseFetchEmployee> call = api.mGetListEmp(Authorization, data);

                    call.enqueue(new Callback<ResponseFetchEmployee>() {
                        @Override
                        public void onResponse(Call<ResponseFetchEmployee> call, Response<ResponseFetchEmployee> response) {


                            if (response.isSuccessful()) {
                                int statusCode = response.body().getErrCode();
                                loadingTemp.setVisibility(View.GONE);
                                if (statusCode == 0) {

                                    Log.e(TAG, response.toString());

                                    dataFetchEmployeeItems = response.body().getDataFetchEmployee();


                                    for (int i = 0; i < dataFetchEmployeeItems.size(); i++) {
                                        final LayoutInflater layoutInflaterDetail = (LayoutInflater)
                                                getActivity().getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        final View addDetail = layoutInflaterDetail.inflate(R.layout.item_emp, null);

                                        final TextView mTxtName = addDetail.findViewById(R.id.mTxtName);
                                        final TextView mTxtHidden = addDetail.findViewById(R.id.mTxtHidden);

                                        mTxtName.setText(dataFetchEmployeeItems.get(i).getFullNameNik());
                                        mTxtHidden.setText(Integer.toString(dataFetchEmployeeItems.get(i).getEmployeeID()));
                                        mTxtName.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mIdEmployee = mTxtHidden.getText().toString();
                                                mEdtNote.setText(mTxtName.getText().toString());
                                                overlayInfo.dismiss();

                                            }
                                        });

                                        containerDialogAddress.addView(addDetail);
                                    }

                                    loadingTemp.setVisibility(View.GONE);
                                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);

                                } else {
                                    loadingTemp.setVisibility(View.GONE);
                                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                                }

                            } else {
                                loadingTemp.setVisibility(View.GONE);
                                txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseFetchEmployee> call, Throwable t) {
                            loadingTemp.setVisibility(View.GONE);
                            txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                        }
                    });


                } catch (Exception e) {
                    loadingTemp.setVisibility(View.GONE);
                    txtEmptyList.setVisibility(dataFetchEmployeeItems.isEmpty() ? View.VISIBLE : View.GONE);
                }
            }
        });


        overlayInfo.setContentView(view);
        final WindowManager manager = (WindowManager) getActivity().getSystemService(Activity.WINDOW_SERVICE);
        int width;
        width = manager.getDefaultDisplay().getWidth();
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

        lp.copyFrom(overlayInfo.getWindow().getAttributes());
        lp.width = width - 30;
        overlayInfo.show();
        overlayInfo.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }




}