package app.direksi.hras.fragment;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import app.direksi.hras.R;

public class BePartnerStepTwoFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CheckBox a1a, a1b, a1c, a1d, a1e;
    private CheckBox a2a, a2b, a2c, a2d, a2e;
    private CheckBox a3a, a3b, a3c, a3d, a3e;
    private CheckBox a4a, a4b, a4c, a4d, a4e;
    private CheckBox a5a, a5b, a5c, a5d, a5e;


    private CheckBox b1a, b1b, b1c, b1d, b1e;
    private CheckBox b2a, b2b, b2c, b2d, b2e;
    private CheckBox b3a, b3b, b3c, b3d, b3e;
    private CheckBox b4a, b4b, b4c, b4d, b4e;
    private CheckBox b5a, b5b, b5c, b5d, b5e;

    private static int  INT_10 = 10;
    private static int  INT_8 = 8;
    private static int  INT_6 = 6;
    private static int  INT_4 = 4;
    private static int  INT_2 = 2;

    private int tempA1 = 0;
    private int tempA2 = 0;
    private int tempA3 = 0;
    private int tempA4 = 0;
    private int tempA5 = 0;

    private int tempB1 = 0;
    private int tempB2 = 0;
    private int tempB3 = 0;
    private int tempB4 = 0;
    private int tempB5 = 0;

    private int tempA = 0;
    private int tempB = 0;
    private int tempC = 0;



    private TextView txtKesimpulanC, txtKesimpulanB, txtKesimpulanA;

    private TextView txtA1,txtA2,txtA3,txtA4,txtA5,txtB1,txtB2,txtB3,txtB4,txtB5;


    private OnStepTwoListener mListener;

    public BePartnerStepTwoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BePartnerStepOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BePartnerStepTwoFragment newInstance(String param1, String param2) {
        BePartnerStepTwoFragment fragment = new BePartnerStepTwoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_be_partner_step_two, container, false);
    }

    private Button backBT;
    private Button nextBT;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backBT=view.findViewById(R.id.backBT);
        nextBT=view.findViewById(R.id.nextBT);

        txtKesimpulanA=view.findViewById(R.id.txtKesimpulanA);
        txtKesimpulanB=view.findViewById(R.id.txtKesimpulanB);
        txtKesimpulanC=view.findViewById(R.id.txtKesimpulanC);

        txtA1=view.findViewById(R.id.txtA1);
        txtA2=view.findViewById(R.id.txtA2);
        txtA3=view.findViewById(R.id.txtA3);
        txtA4=view.findViewById(R.id.txtA4);
        txtA5=view.findViewById(R.id.txtA5);

        txtB1=view.findViewById(R.id.txtB1);
        txtB2=view.findViewById(R.id.txtB2);
        txtB3=view.findViewById(R.id.txtB3);
        txtB4=view.findViewById(R.id.txtB4);
        txtB5=view.findViewById(R.id.txtB5);

        a1a=view.findViewById(R.id.a1a);
        a1b=view.findViewById(R.id.a1b);
        a1c=view.findViewById(R.id.a1c);
        a1d=view.findViewById(R.id.a1d);
        a1e=view.findViewById(R.id.a1e);

        a2a=view.findViewById(R.id.a2a);
        a2b=view.findViewById(R.id.a2b);
        a2c=view.findViewById(R.id.a2c);
        a2d=view.findViewById(R.id.a2d);
        a2e=view.findViewById(R.id.a2e);

        a3a=view.findViewById(R.id.a3a);
        a3b=view.findViewById(R.id.a3b);
        a3c=view.findViewById(R.id.a3c);
        a3d=view.findViewById(R.id.a3d);
        a3e=view.findViewById(R.id.a3e);

        a4a=view.findViewById(R.id.a4a);
        a4b=view.findViewById(R.id.a4b);
        a4c=view.findViewById(R.id.a4c);
        a4d=view.findViewById(R.id.a4d);
        a4e=view.findViewById(R.id.a4e);

        a5a=view.findViewById(R.id.a5a);
        a5b=view.findViewById(R.id.a5b);
        a5c=view.findViewById(R.id.a5c);
        a5d=view.findViewById(R.id.a5d);
        a5e=view.findViewById(R.id.a5e);


        b1a=view.findViewById(R.id.b1a);
        b1b=view.findViewById(R.id.b1b);
        b1c=view.findViewById(R.id.b1c);
        b1d=view.findViewById(R.id.b1d);
        b1e=view.findViewById(R.id.b1e);

        b2a=view.findViewById(R.id.b2a);
        b2b=view.findViewById(R.id.b2b);
        b2c=view.findViewById(R.id.b2c);
        b2d=view.findViewById(R.id.b2d);
        b2e=view.findViewById(R.id.b2e);

        b3a=view.findViewById(R.id.b3a);
        b3b=view.findViewById(R.id.b3b);
        b3c=view.findViewById(R.id.b3c);
        b3d=view.findViewById(R.id.b3d);
        b3e=view.findViewById(R.id.b3e);

        b4a=view.findViewById(R.id.b4a);
        b4b=view.findViewById(R.id.b4b);
        b4c=view.findViewById(R.id.b4c);
        b4d=view.findViewById(R.id.b4d);
        b4e=view.findViewById(R.id.b4e);

        b5a=view.findViewById(R.id.b5a);
        b5b=view.findViewById(R.id.b5b);
        b5c=view.findViewById(R.id.b5c);
        b5d=view.findViewById(R.id.b5d);
        b5e=view.findViewById(R.id.b5e);


        CheckBoxLoadA1();
        CheckBoxLoadA2();
        CheckBoxLoadA3();
        CheckBoxLoadA4();
        CheckBoxLoadA5();
        CheckBoxLoadB1();
        CheckBoxLoadB2();
        CheckBoxLoadB3();
        CheckBoxLoadB4();
        CheckBoxLoadB5();

    }

    @Override
    public void onResume() {
        super.onResume();
        backBT.setOnClickListener(this);
        nextBT.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        backBT.setOnClickListener(null);
        nextBT.setOnClickListener(null);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.backBT:
                if (mListener != null)
                    mListener.onBackPressed(this);
                break;

            case R.id.nextBT:

                CheckNext();

                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnStepTwoListener) {
            mListener = (OnStepTwoListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        backBT=null;
        nextBT=null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStepTwoListener {
        void onBackPressed(Fragment fragment);

        void onNextPressed(Fragment fragment);

    }

    private void CheckBoxLoadA1(){

        a1a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA1.setError(null);
                    a1b.setChecked(false);
                    a1c.setChecked(false);
                    a1d.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA1.setError(null);
                    a1a.setChecked(false);
                    a1c.setChecked(false);
                    a1d.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA1.setError(null);
                    a1b.setChecked(false);
                    a1a.setChecked(false);
                    a1d.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA1.setError(null);
                    a1b.setChecked(false);
                    a1c.setChecked(false);
                    a1a.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA1.setError(null);
                    a1b.setChecked(false);
                    a1c.setChecked(false);
                    a1d.setChecked(false);
                    a1a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA2(){

        a2a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA2.setError(null);
                    a2b.setChecked(false);
                    a2c.setChecked(false);
                    a2d.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA2.setError(null);
                    a2a.setChecked(false);
                    a2c.setChecked(false);
                    a2d.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA2.setError(null);
                    a2b.setChecked(false);
                    a2a.setChecked(false);
                    a2d.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA2.setError(null);
                    a2b.setChecked(false);
                    a2c.setChecked(false);
                    a2a.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA2.setError(null);
                    a2b.setChecked(false);
                    a2c.setChecked(false);
                    a2d.setChecked(false);
                    a2a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA3(){

        a3a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA3.setError(null);
                    a3b.setChecked(false);
                    a3c.setChecked(false);
                    a3d.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA3.setError(null);
                    a3a.setChecked(false);
                    a3c.setChecked(false);
                    a3d.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA3.setError(null);
                    a3b.setChecked(false);
                    a3a.setChecked(false);
                    a3d.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA3.setError(null);
                    a3b.setChecked(false);
                    a3c.setChecked(false);
                    a3a.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA3.setError(null);
                    a3b.setChecked(false);
                    a3c.setChecked(false);
                    a3d.setChecked(false);
                    a3a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA4(){

        a4a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA4.setError(null);
                    a4b.setChecked(false);
                    a4c.setChecked(false);
                    a4d.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA4.setError(null);
                    a4a.setChecked(false);
                    a4c.setChecked(false);
                    a4d.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA4.setError(null);
                    a4b.setChecked(false);
                    a4a.setChecked(false);
                    a4d.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA4.setError(null);
                    a4b.setChecked(false);
                    a4c.setChecked(false);
                    a4a.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA4.setError(null);
                    a4b.setChecked(false);
                    a4c.setChecked(false);
                    a4d.setChecked(false);
                    a4a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA5(){

        a5a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA5.setError(null);
                    a5b.setChecked(false);
                    a5c.setChecked(false);
                    a5d.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA5.setError(null);
                    a5a.setChecked(false);
                    a5c.setChecked(false);
                    a5d.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA5.setError(null);
                    a5b.setChecked(false);
                    a5a.setChecked(false);
                    a5d.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA5.setError(null);
                    a5b.setChecked(false);
                    a5c.setChecked(false);
                    a5a.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtA5.setError(null);
                    a5b.setChecked(false);
                    a5c.setChecked(false);
                    a5d.setChecked(false);
                    a5a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadB1(){

        b1a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB1.setError(null);
                    b1b.setChecked(false);
                    b1c.setChecked(false);
                    b1d.setChecked(false);
                    b1e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b1b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB1.setError(null);
                    b1a.setChecked(false);
                    b1c.setChecked(false);
                    b1d.setChecked(false);
                    b1e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b1c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB1.setError(null);
                    b1b.setChecked(false);
                    b1a.setChecked(false);
                    b1d.setChecked(false);
                    b1e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b1d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB1.setError(null);
                    b1b.setChecked(false);
                    b1c.setChecked(false);
                    b1a.setChecked(false);
                    b1e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b1e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB1.setError(null);
                    b1b.setChecked(false);
                    b1c.setChecked(false);
                    b1d.setChecked(false);
                    b1a.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

    }

    private void CheckBoxLoadB2(){

        b2a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB2.setError(null);
                    b2b.setChecked(false);
                    b2c.setChecked(false);
                    b2d.setChecked(false);
                    b2e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b2b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB2.setError(null);
                    b2a.setChecked(false);
                    b2c.setChecked(false);
                    b2d.setChecked(false);
                    b2e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b2c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB2.setError(null);
                    b2b.setChecked(false);
                    b2a.setChecked(false);
                    b2d.setChecked(false);
                    b2e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b2d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB2.setError(null);
                    b2b.setChecked(false);
                    b2c.setChecked(false);
                    b2a.setChecked(false);
                    b2e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b2e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB2.setError(null);
                    b2b.setChecked(false);
                    b2c.setChecked(false);
                    b2d.setChecked(false);
                    b2a.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

    }

    private void CheckBoxLoadB3(){

        b3a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB3.setError(null);
                    b3b.setChecked(false);
                    b3c.setChecked(false);
                    b3d.setChecked(false);
                    b3e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b3b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB3.setError(null);
                    b3a.setChecked(false);
                    b3c.setChecked(false);
                    b3d.setChecked(false);
                    b3e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b3c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB3.setError(null);
                    b3b.setChecked(false);
                    b3a.setChecked(false);
                    b3d.setChecked(false);
                    b3e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b3d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB3.setError(null);
                    b3b.setChecked(false);
                    b3c.setChecked(false);
                    b3a.setChecked(false);
                    b3e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b3e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB3.setError(null);
                    b3b.setChecked(false);
                    b3c.setChecked(false);
                    b3d.setChecked(false);
                    b3a.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

    }

    private void CheckBoxLoadB4(){

        b4a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB4.setError(null);
                    b4b.setChecked(false);
                    b4c.setChecked(false);
                    b4d.setChecked(false);
                    b4e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b4b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB4.setError(null);
                    b4a.setChecked(false);
                    b4c.setChecked(false);
                    b4d.setChecked(false);
                    b4e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b4c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB4.setError(null);
                    b4b.setChecked(false);
                    b4a.setChecked(false);
                    b4d.setChecked(false);
                    b4e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b4d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB4.setError(null);
                    b4b.setChecked(false);
                    b4c.setChecked(false);
                    b4a.setChecked(false);
                    b4e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b4e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB4.setError(null);
                    b4b.setChecked(false);
                    b4c.setChecked(false);
                    b4d.setChecked(false);
                    b4a.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

    }

    private void CheckBoxLoadB5(){

        b5a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB5.setError(null);
                    b5b.setChecked(false);
                    b5c.setChecked(false);
                    b5d.setChecked(false);
                    b5e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b5b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB5.setError(null);
                    b5a.setChecked(false);
                    b5c.setChecked(false);
                    b5d.setChecked(false);
                    b5e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b5c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB5.setError(null);
                    b5b.setChecked(false);
                    b5a.setChecked(false);
                    b5d.setChecked(false);
                    b5e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b5d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB5.setError(null);
                    b5b.setChecked(false);
                    b5c.setChecked(false);
                    b5a.setChecked(false);
                    b5e.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

        b5e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txtB5.setError(null);
                    b5b.setChecked(false);
                    b5c.setChecked(false);
                    b5d.setChecked(false);
                    b5a.setChecked(false);
                    LoadEvaluationB ();
                } else {
                    LoadEvaluationB ();
                }

            }
        });

    }


    private void LoadEvaluationA (){


          tempA1 = 0;
          tempA2 = 0;
          tempA3 = 0;
          tempA4 = 0;
          tempA5 = 0;

          tempA = 0;


        if (a1a.isChecked()== true){
            tempA1 = INT_10;
        }
        if (a1b.isChecked()== true){
            tempA1 = INT_8;
        }
        if (a1c.isChecked()== true){
            tempA1 = INT_6;
        }
        if (a1d.isChecked()== true){
            tempA1 = INT_4;
        }
        if (a1e.isChecked()== true){
            tempA1 = INT_2;
        }


        if (a2a.isChecked()== true){
            tempA2 = INT_10;
        }
        if (a2b.isChecked()== true){
            tempA2 = INT_8;
        }
        if (a2c.isChecked()== true){
            tempA2 = INT_6;
        }
        if (a2d.isChecked()== true){
            tempA2 = INT_4;
        }
        if (a2e.isChecked()== true){
            tempA2 = INT_2;
        }


        if (a3a.isChecked()== true){
            tempA3 = INT_10;
        }
        if (a3b.isChecked()== true){
            tempA3 = INT_8;
        }
        if (a3c.isChecked()== true){
            tempA3 = INT_6;
        }
        if (a3d.isChecked()== true){
            tempA3 = INT_4;
        }
        if (a3e.isChecked()== true){
            tempA3 = INT_2;
        }


        if (a4a.isChecked()== true){
            tempA4 = INT_10;
        }
        if (a4b.isChecked()== true){
            tempA4 = INT_8;
        }
        if (a4c.isChecked()== true){
            tempA4 = INT_6;
        }
        if (a4d.isChecked()== true){
            tempA4 = INT_4;
        }
        if (a4e.isChecked()== true){
            tempA4 = INT_2;
        }


        if (a5a.isChecked()== true){
            tempA5 = INT_10;
        }
        if (a5b.isChecked()== true){
            tempA5 = INT_8;
        }
        if (a5c.isChecked()== true){
            tempA5 = INT_6;
        }
        if (a5d.isChecked()== true){
            tempA5 = INT_4;
        }
        if (a5e.isChecked()== true){
            tempA5 = INT_2;
        }


        tempA = (tempA1 + tempA2 + tempA3 + tempA4 + tempA5);
        txtKesimpulanA.setText(getResources().getString(R.string.title_kesimpulan_hasil_prestasi) + " "  + String.valueOf(tempA));
        loadKondite();
    }

    private void LoadEvaluationB (){


         tempB1 = 0;
         tempB2 = 0;
         tempB3 = 0;
         tempB4 = 0;
         tempB5 = 0;

        tempB = 0;

        if (b1a.isChecked()== true){
            tempB1 = INT_10;
        }
        if (b1b.isChecked()== true){
            tempB1 = INT_8;
        }
        if (b1c.isChecked()== true){
            tempB1 = INT_6;
        }
        if (b1d.isChecked()== true){
            tempB1 = INT_4;
        }
        if (b1e.isChecked()== true){
            tempB1 = INT_2;
        }


        if (b2a.isChecked()== true){
            tempB2 = INT_10;
        }
        if (b2b.isChecked()== true){
            tempB2 = INT_8;
        }
        if (b2c.isChecked()== true){
            tempB2 = INT_6;
        }
        if (b2d.isChecked()== true){
            tempB2 = INT_4;
        }
        if (b2e.isChecked()== true){
            tempB2 = INT_2;
        }


        if (b3a.isChecked()== true){
            tempB3 = INT_10;
        }
        if (b3b.isChecked()== true){
            tempB3 = INT_8;
        }
        if (b3c.isChecked()== true){
            tempB3 = INT_6;
        }
        if (b3d.isChecked()== true){
            tempB3 = INT_4;
        }
        if (b3e.isChecked()== true){
            tempB3 = INT_2;
        }


        if (b4a.isChecked()== true){
            tempB4 = INT_10;
        }
        if (b4b.isChecked()== true){
            tempB4 = INT_8;
        }
        if (b4c.isChecked()== true){
            tempB4 = INT_6;
        }
        if (b4d.isChecked()== true){
            tempB4 = INT_4;
        }
        if (b4e.isChecked()== true){
            tempB4 = INT_2;
        }


        if (b5a.isChecked()== true){
            tempB5 = INT_10;
        }
        if (b5b.isChecked()== true){
            tempB5 = INT_8;
        }
        if (b5c.isChecked()== true){
            tempB5 = INT_6;
        }
        if (b5d.isChecked()== true){
            tempB5 = INT_4;
        }
        if (b5e.isChecked()== true){
            tempB5 = INT_2;
        }


        tempB = (tempB1 + tempB2 + tempB3 + tempB4 + tempB5);
        txtKesimpulanB.setText( getResources().getString(R.string.title_kesimpulan_hasil_prestasi) + " "  + String.valueOf(tempB));
        loadKondite();
    }

    private void loadKondite(){

        tempC = 0;


        tempC = (tempA1 + tempA2 + tempA3 + tempA4 + tempA5 +tempB1 + tempB2 + tempB3 + tempB4 + tempB5);
        txtKesimpulanC.setText(getResources().getString(R.string.title_kesimpulan_hasil_kondite) + " " + String.valueOf(tempC));
    }

    private void CheckNext(){

        txtA1.setError(null);
        txtA2.setError(null);
        txtA3.setError(null);
        txtA4.setError(null);
        txtA5.setError(null);

        txtB1.setError(null);
        txtB2.setError(null);
        txtB3.setError(null);
        txtB4.setError(null);
        txtB5.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (a1a.isChecked()== true || a1b.isChecked()== true || a1c.isChecked()== true || a1d.isChecked()== true || a1e.isChecked()== true ) {

        } else {

            txtA1.setError("");
            focusView = txtA1;
            cancel = true;
        }

        if (a2a.isChecked()== true || a2b.isChecked()== true || a2c.isChecked()== true || a2d.isChecked()== true || a2e.isChecked()== true ) {

        } else {

            txtA2.setError("");
            focusView = txtA2;
            cancel = true;
        }

        if (a3a.isChecked()== true || a3b.isChecked()== true || a3c.isChecked()== true || a3d.isChecked()== true || a3e.isChecked()== true ) {

        } else {

            txtA3.setError("");
            focusView = txtA3;
            cancel = true;
        }

        if (a4a.isChecked()== true || a4b.isChecked()== true || a4c.isChecked()== true || a4d.isChecked()== true || a4e.isChecked()== true ) {

        } else {

            txtA4.setError("");
            focusView = txtA4;
            cancel = true;
        }

        if (a5a.isChecked()== true || a5b.isChecked()== true || a5c.isChecked()== true || a5d.isChecked()== true || a5e.isChecked()== true ) {

        } else {

            txtA5.setError("");
            focusView = txtA5;
            cancel = true;
        }

        if (b1a.isChecked()== true || b1b.isChecked()== true || b1c.isChecked()== true || b1d.isChecked()== true || b1e.isChecked()== true ) {

        } else {

            txtB1.setError("");
            focusView = txtB1;
            cancel = true;
        }

        if (b2a.isChecked()== true || b2b.isChecked()== true || b2c.isChecked()== true || b2d.isChecked()== true || b2e.isChecked()== true ) {

        } else {

            txtB2.setError("");
            focusView = txtB2;
            cancel = true;
        }

        if (b3a.isChecked()== true || b3b.isChecked()== true || b3c.isChecked()== true || b3d.isChecked()== true || b3e.isChecked()== true ) {

        } else {

            txtB3.setError("");
            focusView = txtB3;
            cancel = true;
        }

        if (b4a.isChecked()== true || b4b.isChecked()== true || b4c.isChecked()== true || b4d.isChecked()== true || b4e.isChecked()== true ) {

        } else {

            txtB4.setError("");
            focusView = txtB4;
            cancel = true;
        }

        if (b5a.isChecked()== true || b5b.isChecked()== true || b5c.isChecked()== true || b5d.isChecked()== true || b5e.isChecked()== true ) {

        } else {

            txtB5.setError("");
            focusView = txtB5;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_ada_pertanyaan_belum_diisi),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            focusView.requestFocus();
        } else {

            if (mListener != null)
                SavePreference();
                mListener.onNextPressed(this);

        }


    }

    private void SavePreference(){

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(getResources().getString(R.string.pref_form_2_a1),  String.valueOf(tempA1))
                .putString(getResources().getString(R.string.pref_form_2_a2),  String.valueOf(tempA2))
                .putString(getResources().getString(R.string.pref_form_2_a3),  String.valueOf(tempA3))
                .putString(getResources().getString(R.string.pref_form_2_a4),  String.valueOf(tempA4))
                .putString(getResources().getString(R.string.pref_form_2_a5),  String.valueOf(tempA5))
                .putString(getResources().getString(R.string.pref_form_2_b1),  String.valueOf(tempB1))
                .putString(getResources().getString(R.string.pref_form_2_b2),  String.valueOf(tempB2))
                .putString(getResources().getString(R.string.pref_form_2_b3),  String.valueOf(tempB3))
                .putString(getResources().getString(R.string.pref_form_2_b4),  String.valueOf(tempB4))
                .putString(getResources().getString(R.string.pref_form_2_b5),  String.valueOf(tempB5))
                .putString(getResources().getString(R.string.pref_form_2_aKesimpulan),  String.valueOf(tempA))
                .putString(getResources().getString(R.string.pref_form_2_bKesimpulan),  String.valueOf(tempB))
                .putString(getResources().getString(R.string.pref_form_2_cKesimpulan),  String.valueOf(tempC))

                .apply();
    }
}
