package app.direksi.hras.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.model.DataInformations;
import app.direksi.hras.model.ResponseInformations;
import app.direksi.hras.util.DefaultFormatter;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

public class BePartnerStepOneFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnStepOneListener mListener;
    private SweetAlertDialog loading;
    private RelativeLayout rlView;
    private TextView txtNama, txtNik, txtDob, txtStart, txtEnd, txtGaji, txtPenilai, txtinfo, txtlink;
    private LinearLayout llinfo;
    DataInformations info;

    public BePartnerStepOneFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BePartnerStepOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BePartnerStepOneFragment newInstance(String param1, String param2) {
        BePartnerStepOneFragment fragment = new BePartnerStepOneFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View rootView =  inflater.inflate(R.layout.fragment_be_partner_step_one, container, false);


        llinfo = rootView.findViewById(R.id.llinfo);
        llinfo.setVisibility(View.GONE);
        txtinfo = rootView.findViewById(R.id.txtinfo);
        txtlink = rootView.findViewById(R.id.txtlink);
        txtNama = rootView.findViewById(R.id.txtNama);
        txtNik = rootView.findViewById(R.id.txtNik);
        txtDob = rootView.findViewById(R.id.txtDob);
        txtStart = rootView.findViewById(R.id.txtStart);
        txtEnd = rootView.findViewById(R.id.txtEnd);
        txtGaji = rootView.findViewById(R.id.txtGaji);
        txtPenilai = rootView.findViewById(R.id.txtPenilai);

        rlView = rootView.findViewById(R.id.rlView);
        rlView.setVisibility(View.GONE);
        loading = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        txtlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    String url = getResources().getString(R.string.base_url)+ "/Laporan/EvaluationFormMobile/" + info.getEncrypted();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    browserIntent.putExtra("detail", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_token), ""));
                    browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(browserIntent);

                } catch (Exception e){
                    Toast.makeText(getContext(), getResources().getString(R.string.title_gagal_membuka_detail), Toast.LENGTH_LONG).show();
                }
            }
        });




        String tempString= getResources().getString(R.string.title_disini);
        SpannableString spanString = new SpannableString(tempString);
        spanString.setSpan(new UnderlineSpan(), 1, spanString.length(), 0);
        spanString.setSpan(new StyleSpan(Typeface.BOLD), 1, spanString.length(), 0);
        spanString.setSpan(new StyleSpan(Typeface.ITALIC), 1, spanString.length(), 0);

        txtlink.setText(spanString);

        mGetOInformation();

        return  rootView;
    }

    private Button nextBT;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        nextBT = view.findViewById(R.id.nextBT);

    }


    @Override
    public void onResume() {
        super.onResume();
        nextBT.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        nextBT.setOnClickListener(null);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.nextBT:
                if (mListener != null)
                    mListener.onNextPressed(this);
                break;
        }

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnStepOneListener) {
            mListener = (OnStepOneListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        nextBT = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStepOneListener {
        //void onFragmentInteraction(Uri uri);
        void onNextPressed(Fragment fragment);
    }

    public interface myListener {
        public void updateView(boolean success, Object message);
    }

    private void mGetOInformation() {
        try {

            loading.show();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid_view), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", id);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseInformations> call = api.getInformation(Authorization, data);


            call.enqueue(new Callback<ResponseInformations>() {


                @Override
                public void onResponse(Call<ResponseInformations> call, Response<ResponseInformations> response) {

                    try {

                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            DecimalFormat formatter = new DecimalFormat("#,###,###");

                            info = response.body().getData();
                            rlView.setVisibility(View.VISIBLE);
                            loading.dismiss();
                            txtNama.setText(response.body().getData().getEmployee().getFirstName() + " "+ response.body().getData().getEmployee().getLastName());
                            txtNik.setText(response.body().getData().getEmployee().getNik() != null ? response.body().getData().getEmployee().getNik() : "");
                            txtDob.setText(response.body().getData().getEmployee().getDob() != null ? DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getEmployee().getDob()) : "");
                            txtStart.setText(response.body().getData().getEmployee().getJoinDate() != null ? DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getEmployee().getJoinDate()) : "");
                            txtEnd.setText(response.body().getData().getEmployee().getContractExpiredDate() != null ? DefaultFormatter.changeFormatDateWithOutHour(response.body().getData().getEmployee().getContractExpiredDate()) : "");
                            txtGaji.setText(response.body().getData().getGajiSekarang() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(response.body().getData().getGajiSekarang()) : "");
                            txtPenilai.setText(PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                                    getResources().getString(R.string.pref_nama), ""));

                            if (response.body().getData().getLastEvaluatioID()!= null){

                                llinfo.setVisibility(View.VISIBLE);
                                String info = getResources().getString(R.string.title_terakhir_memberi_penilaian_pada) +  " <b>" + response.body().getData().getEmployee().getFirstName() + " " +response.body().getData().getEmployee().getLastName()
                                        + "</b> " + getResources().getString(R.string.title_pada_tanggal) + " " +  DefaultFormatter.changeFormatDate(response.body().getData().getLastCreatedDate()) ;
                                txtinfo.setText(Html.fromHtml(info));
                            }


                            SavePreference(response.body().getData());

                        }

                    } catch (Exception e) {

                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception se)
                        {

                        }
                    }
                    loading.dismiss();

                }

                @Override
                public void onFailure(Call<ResponseInformations> call, Throwable t) {

                    loading.dismiss();
                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e)
                    {

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
            }
            catch (Exception xe)
            {

            }

        }

    }

    private void SavePreference( DataInformations data){

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(getResources().getString(R.string.pref_form_1_gaji),  data.getGajiSekarang() != null ? data.getGajiSekarang().toString() : "0")
                .putString(getResources().getString(R.string.pref_form_1_start),  data.getEmployee().getJoinDate() != null ? data.getEmployee().getJoinDate() : "0001-01-01T00:00:00")
                .putString(getResources().getString(R.string.pref_form_1_end),  data.getEmployee().getContractExpiredDate() != null ? data.getEmployee().getContractExpiredDate() : "0001-01-01T00:00:00")
                .apply();
    }
}
