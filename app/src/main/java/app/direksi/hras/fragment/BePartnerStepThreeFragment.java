package app.direksi.hras.fragment;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import app.direksi.hras.R;


public class BePartnerStepThreeFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CheckBox a1a, a1b, a1c, a1d, a1e;
    private CheckBox a2a, a2b, a2c, a2d, a2e;
    private CheckBox a3a, a3b, a3c, a3d, a3e;
    private CheckBox a4a, a4b, a4c, a4d, a4e;
    private CheckBox a5a, a5b, a5c, a5d, a5e;
    private CheckBox a6a, a6b, a6c, a6d, a6e;
    private CheckBox a7a, a7b, a7c, a7d, a7e;
    private CheckBox a8a, a8b, a8c, a8d, a8e;

    private static int  INT_10 = 10;
    private static int  INT_8 = 8;
    private static int  INT_6 = 6;
    private static int  INT_4 = 4;
    private static int  INT_2 = 2;

    private int tempA1 = 0;
    private int tempA2 = 0;
    private int tempA3 = 0;
    private int tempA4 = 0;
    private int tempA5 = 0;
    private int tempA6 = 0;
    private int tempA7 = 0;
    private int tempA8 = 0;

    private int tempA = 0;

    private TextView txtKesimpulanA;

    private TextView txt1,txt2,txt3,txt4,txt5,txt6,txt7,txt8;



    private BePartnerStepThreeFragment.OnStepThreeListener mListener;

    public BePartnerStepThreeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BePartnerStepOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BePartnerStepThreeFragment newInstance(String param1, String param2) {
        BePartnerStepThreeFragment fragment = new BePartnerStepThreeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_be_partner_step_three, container, false);
    }

    private Button backBT;
    private Button nextBT;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backBT=view.findViewById(R.id.backBT);
        nextBT=view.findViewById(R.id.nextBT);

        txtKesimpulanA=view.findViewById(R.id.txtKesimpulanA);

        txt1=view.findViewById(R.id.txt1);
        txt2=view.findViewById(R.id.txt2);
        txt3=view.findViewById(R.id.txt3);
        txt4=view.findViewById(R.id.txt4);
        txt5=view.findViewById(R.id.txt5);
        txt6=view.findViewById(R.id.txt6);
        txt7=view.findViewById(R.id.txt7);
        txt8=view.findViewById(R.id.txt8);

        a1a=view.findViewById(R.id.a1a);
        a1b=view.findViewById(R.id.a1b);
        a1c=view.findViewById(R.id.a1c);
        a1d=view.findViewById(R.id.a1d);
        a1e=view.findViewById(R.id.a1e);

        a2a=view.findViewById(R.id.a2a);
        a2b=view.findViewById(R.id.a2b);
        a2c=view.findViewById(R.id.a2c);
        a2d=view.findViewById(R.id.a2d);
        a2e=view.findViewById(R.id.a2e);

        a3a=view.findViewById(R.id.a3a);
        a3b=view.findViewById(R.id.a3b);
        a3c=view.findViewById(R.id.a3c);
        a3d=view.findViewById(R.id.a3d);
        a3e=view.findViewById(R.id.a3e);

        a4a=view.findViewById(R.id.a4a);
        a4b=view.findViewById(R.id.a4b);
        a4c=view.findViewById(R.id.a4c);
        a4d=view.findViewById(R.id.a4d);
        a4e=view.findViewById(R.id.a4e);

        a5a=view.findViewById(R.id.a5a);
        a5b=view.findViewById(R.id.a5b);
        a5c=view.findViewById(R.id.a5c);
        a5d=view.findViewById(R.id.a5d);
        a5e=view.findViewById(R.id.a5e);

        a6a=view.findViewById(R.id.a6a);
        a6b=view.findViewById(R.id.a6b);
        a6c=view.findViewById(R.id.a6c);
        a6d=view.findViewById(R.id.a6d);
        a6e=view.findViewById(R.id.a6e);

        a7a=view.findViewById(R.id.a7a);
        a7b=view.findViewById(R.id.a7b);
        a7c=view.findViewById(R.id.a7c);
        a7d=view.findViewById(R.id.a7d);
        a7e=view.findViewById(R.id.a7e);

        a8a=view.findViewById(R.id.a8a);
        a8b=view.findViewById(R.id.a8b);
        a8c=view.findViewById(R.id.a8c);
        a8d=view.findViewById(R.id.a8d);
        a8e=view.findViewById(R.id.a8e);

        CheckBoxLoadA1();
        CheckBoxLoadA2();
        CheckBoxLoadA3();
        CheckBoxLoadA4();
        CheckBoxLoadA5();
        CheckBoxLoadA6();
        CheckBoxLoadA7();
        CheckBoxLoadA8();


    }

    @Override
    public void onResume() {
        super.onResume();
        backBT.setOnClickListener(this);
        nextBT.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        backBT.setOnClickListener(null);
        nextBT.setOnClickListener(null);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.backBT:
                if (mListener != null)
                    mListener.onBackPressed(this);
                break;

            case R.id.nextBT:

                CheckNext();

                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BePartnerStepTwoFragment.OnStepTwoListener) {
            mListener = (BePartnerStepThreeFragment.OnStepThreeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        backBT=null;
        nextBT=null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStepThreeListener {
        void onBackPressed(Fragment fragment);

        void onNextPressed(Fragment fragment);

    }

    private void CheckBoxLoadA1(){

        a1a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt1.setError(null);
                    a1b.setChecked(false);
                    a1c.setChecked(false);
                    a1d.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt1.setError(null);
                    a1a.setChecked(false);
                    a1c.setChecked(false);
                    a1d.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt1.setError(null);
                    a1b.setChecked(false);
                    a1a.setChecked(false);
                    a1d.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt1.setError(null);
                    a1b.setChecked(false);
                    a1c.setChecked(false);
                    a1a.setChecked(false);
                    a1e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a1e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt1.setError(null);
                    a1b.setChecked(false);
                    a1c.setChecked(false);
                    a1d.setChecked(false);
                    a1a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA2(){

        a2a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt2.setError(null);
                    a2b.setChecked(false);
                    a2c.setChecked(false);
                    a2d.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt2.setError(null);
                    a2a.setChecked(false);
                    a2c.setChecked(false);
                    a2d.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt2.setError(null);
                    a2b.setChecked(false);
                    a2a.setChecked(false);
                    a2d.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt2.setError(null);
                    a2b.setChecked(false);
                    a2c.setChecked(false);
                    a2a.setChecked(false);
                    a2e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a2e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt2.setError(null);
                    a2b.setChecked(false);
                    a2c.setChecked(false);
                    a2d.setChecked(false);
                    a2a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA3(){

        a3a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt3.setError(null);
                    a3b.setChecked(false);
                    a3c.setChecked(false);
                    a3d.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt3.setError(null);
                    a3a.setChecked(false);
                    a3c.setChecked(false);
                    a3d.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt3.setError(null);
                    a3b.setChecked(false);
                    a3a.setChecked(false);
                    a3d.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt3.setError(null);
                    a3b.setChecked(false);
                    a3c.setChecked(false);
                    a3a.setChecked(false);
                    a3e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a3e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt3.setError(null);
                    a3b.setChecked(false);
                    a3c.setChecked(false);
                    a3d.setChecked(false);
                    a3a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA4(){

        a4a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt4.setError(null);
                    a4b.setChecked(false);
                    a4c.setChecked(false);
                    a4d.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt4.setError(null);
                    a4a.setChecked(false);
                    a4c.setChecked(false);
                    a4d.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt4.setError(null);
                    a4b.setChecked(false);
                    a4a.setChecked(false);
                    a4d.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt4.setError(null);
                    a4b.setChecked(false);
                    a4c.setChecked(false);
                    a4a.setChecked(false);
                    a4e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a4e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt4.setError(null);
                    a4b.setChecked(false);
                    a4c.setChecked(false);
                    a4d.setChecked(false);
                    a4a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA5(){

        a5a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt5.setError(null);
                    a5b.setChecked(false);
                    a5c.setChecked(false);
                    a5d.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt5.setError(null);
                    a5a.setChecked(false);
                    a5c.setChecked(false);
                    a5d.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt5.setError(null);
                    a5b.setChecked(false);
                    a5a.setChecked(false);
                    a5d.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt5.setError(null);
                    a5b.setChecked(false);
                    a5c.setChecked(false);
                    a5a.setChecked(false);
                    a5e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a5e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt5.setError(null);
                    a5b.setChecked(false);
                    a5c.setChecked(false);
                    a5d.setChecked(false);
                    a5a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA6(){

        a6a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt6.setError(null);
                    a6b.setChecked(false);
                    a6c.setChecked(false);
                    a6d.setChecked(false);
                    a6e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a6b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt6.setError(null);
                    a6a.setChecked(false);
                    a6c.setChecked(false);
                    a6d.setChecked(false);
                    a6e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a6c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt6.setError(null);
                    a6b.setChecked(false);
                    a6a.setChecked(false);
                    a6d.setChecked(false);
                    a6e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a6d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt6.setError(null);
                    a6b.setChecked(false);
                    a6c.setChecked(false);
                    a6a.setChecked(false);
                    a6e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a6e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt6.setError(null);
                    a6b.setChecked(false);
                    a6c.setChecked(false);
                    a6d.setChecked(false);
                    a6a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA7(){

        a7a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt7.setError(null);
                    a7b.setChecked(false);
                    a7c.setChecked(false);
                    a7d.setChecked(false);
                    a7e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a7b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt7.setError(null);
                    a7a.setChecked(false);
                    a7c.setChecked(false);
                    a7d.setChecked(false);
                    a7e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a7c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt7.setError(null);
                    a7b.setChecked(false);
                    a7a.setChecked(false);
                    a7d.setChecked(false);
                    a7e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a7d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt7.setError(null);
                    a7b.setChecked(false);
                    a7c.setChecked(false);
                    a7a.setChecked(false);
                    a7e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a7e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt7.setError(null);
                    a7b.setChecked(false);
                    a7c.setChecked(false);
                    a7d.setChecked(false);
                    a7a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void CheckBoxLoadA8(){

        a8a.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt8.setError(null);
                    a8b.setChecked(false);
                    a8c.setChecked(false);
                    a8d.setChecked(false);
                    a8e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a8b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt8.setError(null);
                    a8a.setChecked(false);
                    a8c.setChecked(false);
                    a8d.setChecked(false);
                    a8e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a8c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt8.setError(null);
                    a8b.setChecked(false);
                    a8a.setChecked(false);
                    a8d.setChecked(false);
                    a8e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a8d.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt8.setError(null);
                    a8b.setChecked(false);
                    a8c.setChecked(false);
                    a8a.setChecked(false);
                    a8e.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

        a8e.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                if ( isChecked )
                {
                    txt8.setError(null);
                    a8b.setChecked(false);
                    a8c.setChecked(false);
                    a8d.setChecked(false);
                    a8a.setChecked(false);
                    LoadEvaluationA ();
                } else {
                    LoadEvaluationA ();
                }

            }
        });

    }

    private void LoadEvaluationA (){


        tempA1 = 0;
        tempA2 = 0;
        tempA3 = 0;
        tempA4 = 0;
        tempA5 = 0;
        tempA6 = 0;
        tempA7 = 0;
        tempA8 = 0;

        tempA = 0;

        if (a1a.isChecked()== true){
            tempA1 = INT_10;
        }
        if (a1b.isChecked()== true){
            tempA1 = INT_8;
        }
        if (a1c.isChecked()== true){
            tempA1 = INT_6;
        }
        if (a1d.isChecked()== true){
            tempA1 = INT_4;
        }
        if (a1e.isChecked()== true){
            tempA1 = INT_2;
        }


        if (a2a.isChecked()== true){
            tempA2 = INT_10;
        }
        if (a2b.isChecked()== true){
            tempA2 = INT_8;
        }
        if (a2c.isChecked()== true){
            tempA2 = INT_6;
        }
        if (a2d.isChecked()== true){
            tempA2 = INT_4;
        }
        if (a2e.isChecked()== true){
            tempA2 = INT_2;
        }


        if (a3a.isChecked()== true){
            tempA3 = INT_10;
        }
        if (a3b.isChecked()== true){
            tempA3 = INT_8;
        }
        if (a3c.isChecked()== true){
            tempA3 = INT_6;
        }
        if (a3d.isChecked()== true){
            tempA3 = INT_4;
        }
        if (a3e.isChecked()== true){
            tempA3 = INT_2;
        }


        if (a4a.isChecked()== true){
            tempA4 = INT_10;
        }
        if (a4b.isChecked()== true){
            tempA4 = INT_8;
        }
        if (a4c.isChecked()== true){
            tempA4 = INT_6;
        }
        if (a4d.isChecked()== true){
            tempA4 = INT_4;
        }
        if (a4e.isChecked()== true){
            tempA4 = INT_2;
        }


        if (a5a.isChecked()== true){
            tempA5 = INT_10;
        }
        if (a5b.isChecked()== true){
            tempA5 = INT_8;
        }
        if (a5c.isChecked()== true){
            tempA5 = INT_6;
        }
        if (a5d.isChecked()== true){
            tempA5 = INT_4;
        }
        if (a5e.isChecked()== true){
            tempA5 = INT_2;
        }

        if (a6a.isChecked()== true){
            tempA6 = INT_10;
        }
        if (a6b.isChecked()== true){
            tempA6 = INT_8;
        }
        if (a6c.isChecked()== true){
            tempA6 = INT_6;
        }
        if (a6d.isChecked()== true){
            tempA6 = INT_4;
        }
        if (a6e.isChecked()== true){
            tempA6 = INT_2;
        }

        if (a7a.isChecked()== true){
            tempA7 = INT_10;
        }
        if (a7b.isChecked()== true){
            tempA7 = INT_8;
        }
        if (a7c.isChecked()== true){
            tempA7 = INT_6;
        }
        if (a7d.isChecked()== true){
            tempA7 = INT_4;
        }
        if (a7e.isChecked()== true){
            tempA7 = INT_2;
        }

        if (a8a.isChecked()== true){
            tempA8 = INT_10;
        }
        if (a8b.isChecked()== true){
            tempA8 = INT_8;
        }
        if (a8c.isChecked()== true){
            tempA8 = INT_6;
        }
        if (a8d.isChecked()== true){
            tempA8 = INT_4;
        }
        if (a8e.isChecked()== true){
            tempA8 = INT_2;
        }



        tempA = (tempA1 + tempA2 + tempA3 + tempA4 + tempA5 + tempA6 + tempA7) ;
        txtKesimpulanA.setText( getResources().getString(R.string.title_kesimpulan_hasil_kepribadian_titikdua) + " " + String.valueOf(tempA));
    }


    private void CheckNext(){

        txt1.setError(null);
        txt2.setError(null);
        txt3.setError(null);
        txt4.setError(null);
        txt5.setError(null);
        txt6.setError(null);
        txt7.setError(null);
        txt8.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (a1a.isChecked()== true || a1b.isChecked()== true || a1c.isChecked()== true || a1d.isChecked()== true || a1e.isChecked()== true ) {

        } else {

            txt1.setError("");
            focusView = txt1;
            cancel = true;
        }

        if (a2a.isChecked()== true || a2b.isChecked()== true || a2c.isChecked()== true || a2d.isChecked()== true || a2e.isChecked()== true ) {

        } else {

            txt2.setError("");
            focusView = txt2;
            cancel = true;
        }

        if (a3a.isChecked()== true || a3b.isChecked()== true || a3c.isChecked()== true || a3d.isChecked()== true || a3e.isChecked()== true ) {

        } else {

            txt3.setError("");
            focusView = txt3;
            cancel = true;
        }

        if (a4a.isChecked()== true || a4b.isChecked()== true || a4c.isChecked()== true || a4d.isChecked()== true || a4e.isChecked()== true ) {

        } else {

            txt4.setError("");
            focusView = txt4;
            cancel = true;
        }

        if (a5a.isChecked()== true || a5b.isChecked()== true || a5c.isChecked()== true || a5d.isChecked()== true || a5e.isChecked()== true ) {

        } else {

            txt5.setError("");
            focusView = txt5;
            cancel = true;
        }

        if (a6a.isChecked()== true || a6b.isChecked()== true || a6c.isChecked()== true || a6d.isChecked()== true || a6e.isChecked()== true ) {

        } else {

            txt6.setError("");
            focusView = txt6;
            cancel = true;
        }

        if (a7a.isChecked()== true || a7b.isChecked()== true || a7c.isChecked()== true || a7d.isChecked()== true || a7e.isChecked()== true ) {

        } else {

            txt7.setError("");
            focusView = txt7;
            cancel = true;
        }

        if (a8a.isChecked()== true || a8b.isChecked()== true || a8c.isChecked()== true || a8d.isChecked()== true || a8e.isChecked()== true ) {

        } else {

            txt8.setError("");
            focusView = txt8;
            cancel = true;
        }



        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            MDToast.makeText(getActivity(), getResources().getString(R.string.title_ada_pertanyaan_belum_diisi),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            focusView.requestFocus();
        } else {

            if (mListener != null)
                SavePreference();
                mListener.onNextPressed(this);

        }


    }

    private void SavePreference(){

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(getResources().getString(R.string.pref_form_3_a1),  String.valueOf(tempA1))
                .putString(getResources().getString(R.string.pref_form_3_a2),  String.valueOf(tempA2))
                .putString(getResources().getString(R.string.pref_form_3_a3),  String.valueOf(tempA3))
                .putString(getResources().getString(R.string.pref_form_3_a4),  String.valueOf(tempA4))
                .putString(getResources().getString(R.string.pref_form_3_a5),  String.valueOf(tempA5))
                .putString(getResources().getString(R.string.pref_form_3_a6),  String.valueOf(tempA6))
                .putString(getResources().getString(R.string.pref_form_3_a7),  String.valueOf(tempA7))
                .putString(getResources().getString(R.string.pref_form_3_aKesimpulan),  String.valueOf(tempA))
                .putString(getResources().getString(R.string.pref_form_4_aKesimpulan),  String.valueOf(tempA8))

                .apply();
    }
}
