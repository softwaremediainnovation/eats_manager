package app.direksi.hras.fragment;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.R;
import app.direksi.hras.model.DataInformations;
import app.direksi.hras.model.ResponseInformations;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;

public class BePartnerStepFourFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText txtHubungan, txtPenghargaan, txtPeringatan;
    private TextView txtMangkir, txtSuratIjin, txtSuratKeterangan, txtSuratDokter, txtDispensasi, txtCutiHaid, txtCutiTahunan;

    private BePartnerStepFourFragment.OnStepFourListener mListener;

    public BePartnerStepFourFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BePartnerStepOneFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BePartnerStepFourFragment newInstance(String param1, String param2) {
        BePartnerStepFourFragment fragment = new BePartnerStepFourFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_be_partner_step_four, container, false);
    }

    private Button backBT;
    private Button nextBT;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        backBT=view.findViewById(R.id.backBT);
        nextBT=view.findViewById(R.id.nextBT);

        txtMangkir=view.findViewById(R.id.txtMangkir);
        txtSuratIjin=view.findViewById(R.id.txtSuratIjin);
        txtSuratKeterangan=view.findViewById(R.id.txtSuratKeterangan);
        txtSuratDokter=view.findViewById(R.id.txtSuratDokter);
        txtDispensasi=view.findViewById(R.id.txtDispensasi);
        txtCutiHaid=view.findViewById(R.id.txtCutiHaid);
        txtCutiTahunan=view.findViewById(R.id.txtCutiTahunan);

        txtHubungan=view.findViewById(R.id.txtHubungan);
        txtPenghargaan=view.findViewById(R.id.txtPenghargaan);
        txtPeringatan=view.findViewById(R.id.txtPeringatan);

        mGetOInformation();
    }

    @Override
    public void onResume() {
        super.onResume();
        backBT.setOnClickListener(this);
        nextBT.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        backBT.setOnClickListener(null);
        nextBT.setOnClickListener(null);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.backBT:
                if (mListener != null)
                    mListener.onBackPressed(this);
                break;

            case R.id.nextBT:

                CheckNext();

                break;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BePartnerStepTwoFragment.OnStepTwoListener) {
            mListener = (BePartnerStepFourFragment.OnStepFourListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        backBT=null;
        nextBT=null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnStepFourListener {
        void onBackPressed(Fragment fragment);

        void onNextPressed(Fragment fragment);

    }

    private void CheckNext(){


        txtHubungan.setError(null);
        txtPenghargaan.setError(null);
        txtPeringatan.setError(null);

        String hubungan = txtHubungan.getText().toString();
        String penghargaan = txtPenghargaan.getText().toString();
        String peringatan = txtPeringatan.getText().toString();

        boolean cancel = false;
        View focusView = null;

        /*if (TextUtils.isEmpty(hubungan)) {
            txtHubungan.setError("Isian harap diisi");
            focusView = txtHubungan;
            cancel = true;
        }

        if (TextUtils.isEmpty(penghargaan)) {
            txtPenghargaan.setError("Isian harap diisi");
            focusView = txtPenghargaan;
            cancel = true;
        }

        if (TextUtils.isEmpty(peringatan)) {
            txtPeringatan.setError("Isian harap diisi");
            focusView = txtPeringatan;
            cancel = true;
        }*/

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            if (mListener != null)
                SavePreference();
                mListener.onNextPressed(this);
        }



    }

    private void SavePreference(){

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(getResources().getString(R.string.pref_form_5_b1),  txtHubungan.getText().toString())
                .putString(getResources().getString(R.string.pref_form_5_b2),  txtPenghargaan.getText().toString())
                .putString(getResources().getString(R.string.pref_form_5_b3),  txtPeringatan.getText().toString())

                .apply();
    }

    private void mGetOInformation() {
        try {



            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_token), ""));

            String id = (PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
                    getResources().getString(R.string.pref_employeeid_view), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", id);

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseInformations> call = api.getInformation(Authorization, data);


            call.enqueue(new Callback<ResponseInformations>() {


                @Override
                public void onResponse(Call<ResponseInformations> call, Response<ResponseInformations> response) {

                    try {

                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            DecimalFormat formatter = new DecimalFormat("#,###,###");


                            if (isAdded()) {

                                txtMangkir.setText(response.body().getData().getMangkir() != null ? response.body().getData().getMangkir() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));
                                txtSuratIjin.setText(response.body().getData().getSuratIjin() != null ? response.body().getData().getSuratIjin() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));
                                txtSuratKeterangan.setText(response.body().getData().getSuratKeterangan() != null ? response.body().getData().getSuratKeterangan() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));
                                txtSuratDokter.setText(response.body().getData().getSuratDokter() != null ? response.body().getData().getSuratDokter() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));
                                txtDispensasi.setText(response.body().getData().getDispensasi() != null ? response.body().getData().getDispensasi() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));
                                txtCutiHaid.setText(response.body().getData().getCutiHaid() != null ? response.body().getData().getCutiHaid() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));
                                txtCutiTahunan.setText(response.body().getData().getCutiTahunan() != null ? response.body().getData().getCutiTahunan() + " " + getResources().getString(R.string.title_hari) : "0 " + getResources().getString(R.string.title_hari));


                            }

                            SavePreference(response.body().getData());
                        }

                    } catch (Exception e) {

                        try {
                            MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception se)
                        {

                        }
                    }


                }

                @Override
                public void onFailure(Call<ResponseInformations> call, Throwable t) {


                    try {
                        MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception e)
                    {

                    }
                }
            });
        } catch (Exception e) {

            try {
                MDToast.makeText(getActivity(), getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
            }
            catch (Exception xe)
            {

            }

        }

    }

    private void SavePreference(DataInformations data){

        PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                .putString(getResources().getString(R.string.pref_form_5_acutihaid),  String.valueOf(data.getCutiHaid() != null ? data.getCutiHaid() : ""))
                .putString(getResources().getString(R.string.pref_form_5_acutitahunan),  String.valueOf(data.getCutiTahunan() != null ? data.getCutiTahunan() : ""))
                .putString(getResources().getString(R.string.pref_form_5_adispensasi),  String.valueOf(data.getDispensasi() !=null ? data.getDispensasi() : ""))
                .putString(getResources().getString(R.string.pref_form_5_amangkir),  String.valueOf(data.getMangkir() != null ? data.getMangkir() : ""))
                .putString(getResources().getString(R.string.pref_form_5_asuratdokter),  String.valueOf(data.getSuratDokter() != null ? data.getSuratDokter() : ""))
                .putString(getResources().getString(R.string.pref_form_5_asuratijin),  String.valueOf(data.getSuratIjin() != null ? data.getSuratIjin() : ""))
                .putString(getResources().getString(R.string.pref_form_5_asuratketerangan),  String.valueOf(data.getSuratKeterangan() != null ? data.getSuratKeterangan() : ""))


                .apply();
    }


}

