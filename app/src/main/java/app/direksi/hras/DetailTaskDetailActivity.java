package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.cardview.widget.CardView;

import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import app.direksi.hras.model.DataTaskIDDetailItem;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class DetailTaskDetailActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = DetailAktivitasActivity.class.getSimpleName();

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private TextView mTxtDate,
            mTxtNote;
    private ImageView mImageView;
    private MessageDialog messageDialog;
    private SweetAlertDialog loading;
    private ScrollView scrollView;
    private double latitude = 0, longitude = 0;
    private static final int REQUEST_PHONE_CALL = 1;
    public TextView txtAlamat,
            txtDetails;
    private CardView mLayoutImages;

    DataTaskIDDetailItem dataTask;
    CheckBox cbFinish;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailTaskDetailActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detailss));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_task_detail);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        Gson gson = new Gson();
        dataTask = gson.fromJson(getIntent().getStringExtra("detail"), DataTaskIDDetailItem.class);

        cbFinish = findViewById(R.id.cbFinish);
        cbFinish.setChecked(dataTask.isIsFinish());
        txtDetails = findViewById(R.id.txtDetails);
        txtAlamat = findViewById(R.id.txtAlamat);
        mTxtDate = findViewById(R.id.mTxtDate);
        mTxtNote = findViewById(R.id.mTxtNote);
        mImageView = findViewById(R.id.mImageView);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);

        /*if (dataTask.isIsFinish()){

            txtDetails.setPaintFlags(txtDetails.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txtDetails.setTypeface(txtDetails.getTypeface(), Typeface.ITALIC);
        }*/



        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        messageDialog = new MessageDialog();
        loading = new SweetAlertDialog(DetailTaskDetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);



        mLayoutImages = findViewById(R.id.mLayoutImages);

        cbFinish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                @Override
                                                public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

                                                    if (isChecked){
                                                        cbFinish.setChecked(false);
                                                        cbFinish.jumpDrawablesToCurrentState();

                                                    }else {
                                                        cbFinish.setChecked(true);
                                                        cbFinish.jumpDrawablesToCurrentState();
                                                    }

                                                }
                                            }
        );





    }



    public void mLoadMaps() {


        int height = SIZE_MARKER;
        int width = SIZE_MARKER;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);


        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                .title(getResources().getString(R.string.title_lokasi)));

        Log.v(TAG, latitude + " --- " + longitude);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                .zoom(16)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


     /*   Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(RANGE_DISTANCE)
                .strokeColor(Color.parseColor("#20ff5252"))
                .strokeWidth(3)
                .fillColor(Color.parseColor("#50ff5252")));*/


    }


    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try {
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_json_maps));

            if (!isSuccess) {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        } catch (Resources.NotFoundException ex) {

            Log.v(TAG, ex.toString());
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            mMap.getUiSettings().setMapToolbarEnabled(false);
            // mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);
          /*  mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(-7.257472, 112.752090)));*/

            ShowData(dataTask);
        }


    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





    private void ShowData(DataTaskIDDetailItem data){

        scrollView.setVisibility(View.VISIBLE);
        txtDetails.setText(data.getTaskName());
        mTxtNote.setText(data.getNote());
    //    txtAlamat.setText(data.getAddress());
        mTxtDate.setText(DefaultFormatter.changeFormatDate(data.getDateUpdated()));


        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);


        if (data.getPhoto()!= null) {
            mImageView.setVisibility(View.VISIBLE);
            Glide.with(DetailTaskDetailActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.getPhoto()))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);


            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.getPhoto());
                    startActivity(intent);
                }
            });
        } else{
            mImageView.setVisibility(View.GONE);
        }

        try {
            if (data.getLatitude() != null && data.getLongitude() != null) {
                latitude = Double.parseDouble(data.getLatitude());
                longitude = Double.parseDouble(data.getLongitude());


                if (data.getAddress()!= null){

                    if (!data.getAddress().equals("")){

                        txtAlamat.setText(data.getAddress());

                    } else {

                        getAddress(this, latitude, longitude);

                    }


                } else {

                    getAddress(this, latitude, longitude);
                }


                mLoadMaps();
            }
        } catch (Exception e){

        }



    }



    void getAddress(Context context, double LATITUDE, double LONGITUDE) {

        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {



                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                txtAlamat.setText(address);
                Log.d(TAG, "getAddress:  address" + address);
                Log.d(TAG, "getAddress:  city" + city);
                Log.d(TAG, "getAddress:  state" + state);
                Log.d(TAG, "getAddress:  postalCode" + postalCode);
                Log.d(TAG, "getAddress:  knownName" + knownName);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void voiceCall(String no_Telp){


        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(DetailTaskDetailActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(DetailAktivitasActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailTaskDetailActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}