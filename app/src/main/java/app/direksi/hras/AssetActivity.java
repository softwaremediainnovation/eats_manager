package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.CreateAssetFragment;
import app.direksi.hras.fragment.CreateCutiFragment;
import app.direksi.hras.model.DataAset;
import app.direksi.hras.model.ResponseAset;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.fragment.FilterAsetFragment;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 27/02/2019.
 */

public class AssetActivity extends AppCompatActivity implements Serializable, View.OnClickListener, AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ListCutiActivity.class.getSimpleName();

    private List<DataAset> results = new ArrayList<>();
    private AssetActivity.AsetAdapter cutiAdapter;

    SweetAlertDialog loading;
    TextView txtEmptyList;
    private Filter filter;
    ListView recyclerView;

    private MessageDialog messageDialog;
    FloatingActionMenu mFloat;
    FloatingActionButton fab;
    Integer pageNumber = 1;
    SwipeRefreshLayout swiperefresh;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(AssetActivity.this);
        setContentView(R.layout.activity_aset);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //change font app
      //  TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.aset));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        //remove line in bar
        // getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading
        //  getSupportActionBar().setTitle(getResources().getString(R.string.title_list_order));

        messageDialog = new MessageDialog();

        swiperefresh = findViewById(R.id.swiperefresh);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setOnScrollListener(this);
        fab = findViewById(R.id.fab);
        txtEmptyList = findViewById(R.id.txtEmptyList);

        mFloat = findViewById(R.id.material_design_android_floating_action_menu);

        mFloat.setOnClickListener(this);
        fab.setOnClickListener(this);
        swiperefresh.setOnRefreshListener(this);

        loading = new SweetAlertDialog(AssetActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        // fetchDetailSummary();


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_aseet, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(AssetActivity.this);

        String tag = (PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                getResources().getString(R.string.pref_dayoff_flag), ""));

        if (tag.equals("0")){
            loading = new SweetAlertDialog(AssetActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
            loading.setTitleText(getResources().getString(R.string.loading));
            loading.setCancelable(false);

            loading.show();
            pageNumber = 1;

            results = new ArrayList<>();

            cutiAdapter = new AssetActivity.AsetAdapter(getApplicationContext(), R.layout.item_cuti, R.id.txtName, results);
            cutiAdapter.setNotifyOnChange(true);
            recyclerView.setAdapter(cutiAdapter);

            cutiAdapter.clear();
            results.clear();

            fetchDetailSummary();

        } else {
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                    .apply();

        }



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterAsetFragment dialogFragment = new FilterAsetFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;
            case R.id.action_asset:
                Intent mIntent = new Intent(getApplicationContext(), MyAssetsActivity.class);
                startActivity(mIntent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        loading.show();
        pageNumber = 1;
        cutiAdapter.clear();
        results.clear();
        fetchDetailSummary();
        swiperefresh.setRefreshing(false);

    }

    private class AsetAdapter extends ArrayAdapter<DataAset> {
        AssetActivity.AsetAdapter.ViewHolder holder = null;
        private Context context;


        public AsetAdapter(Context context, int resource, int textViewResourceId, List<DataAset> objects) {
            super(context, resource, textViewResourceId, objects);
            this.context = context;
        }

        private class ViewHolder {
            public TextView mTxtJudul, txtDateCreated, txtLastLocation;

            public ConstraintLayout mMainLayout;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final DataAset data = getItem(position);

            convertView = getLayoutInflater().inflate(R.layout.item_aset, null);
            holder = new AssetActivity.AsetAdapter.ViewHolder();
            convertView.setTag(holder);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                            .apply();
                    Intent mIntent = new Intent(getApplicationContext(), TrackingActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);

                }
            });


            holder.mTxtJudul = convertView.findViewById(R.id.mTxtJudul);
            holder.txtDateCreated = convertView.findViewById(R.id.txtDateCreated);
            holder.txtLastLocation = convertView.findViewById(R.id.txtLastLocation);
            holder.mMainLayout = convertView.findViewById(R.id.mMainLayout);



            holder.mTxtJudul.setText(data.getName());
           // holder.txtDateCreated.setText(data.getDateCreated()!= null ? data.getDateCreated(): "");
            holder.txtLastLocation.setText(data.getLastLocation() != null ? data.getLastLocation():"");
            if (data.getDateCreated() != null) {
                holder.txtDateCreated.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getDateCreated()));
            } else {
                holder.txtDateCreated.setText("");
            }

            holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                            .apply();
                    Intent mIntent = new Intent(getApplicationContext(), TrackingActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);
                }
            });



            return convertView;
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        super.onBackPressed();
    }


    private void fetchDetailSummary() {
        try {

            //  recyclerView.setVisibility(View.GONE);

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("page", pageNumber.toString());
            data.put("size", "50");

            if (!PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty())
                data.put("start", PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));

            if (!PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_end), "").isEmpty())
                data.put("end", PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_end), ""));

            if (!PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_search), "").isEmpty())
                data.put("search", PreferenceManager.getDefaultSharedPreferences(AssetActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_search), ""));

            Log.v(TAG, data.toString());


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseAset> call = api.getAset(Authorization);


            call.enqueue(new Callback<ResponseAset>() {
                @Override
                public void onResponse(Call<ResponseAset> call, Response<ResponseAset> response) {


                    if (response.isSuccessful()) {
                        recyclerView.setVisibility(View.VISIBLE);
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            try {
                                results = response.body().getData();

                                if (results.size() > 0) {

                                    cutiAdapter.addAll(results);
                                    cutiAdapter.notifyDataSetChanged();
                                    recyclerView.invalidateViews();
                                    pageNumber++;
                                    txtEmptyList.setVisibility(cutiAdapter.isEmpty() ? View.VISIBLE : View.GONE);
                                    loading.dismiss();


                                }

                                cutiAdapter.notifyDataSetChanged();
                                txtEmptyList.setVisibility(cutiAdapter.isEmpty() ? View.VISIBLE : View.GONE);
                                loading.dismiss();
                            } catch (Exception e) {
                                messageDialog.mShowMessageError(AssetActivity.this,"GAGAL"
                                        , response.body().getErrMessage());
                                loading.dismiss();

                            }

                        } else {

                            messageDialog.mShowMessageError(AssetActivity.this,"GAGAL"
                                    , response.body().getErrMessage());
                            loading.dismiss();


                        }
                    }
                    else {
                        loading.dismiss();
                        Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponseAset> call, Throwable t) {
                    loading.dismiss();
                    recyclerView.setVisibility(View.VISIBLE);
                    messageDialog.mShowMessageError(AssetActivity.this, "GAGAL"
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            recyclerView.setVisibility(View.VISIBLE);
            messageDialog.mShowMessageError(AssetActivity.this, "GAGAL"
                    , getResources().getString(R.string.loading_error));
        }

    }


    private boolean currentVisibleItemCount;
    private boolean loadmoreProcessComplete = true;
    private int currentScrollState;


    public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
        currentVisibleItemCount = firstVisible + visibleCount >= totalCount - 2;
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        currentScrollState = scrollState;
        isScrollCompleted();
    }

    private void isScrollCompleted() {
        try {
            if (loadmoreProcessComplete && currentVisibleItemCount && currentScrollState == SCROLL_STATE_IDLE) {
                loadmoreProcessComplete = true;
                fetchDetailSummary();
            }
        } catch (Exception e) {
        }

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.material_design_android_floating_action_menu) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateCutiFragment dialogFragment = new CreateCutiFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        } else if (v.getId() == R.id.fab) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateAssetFragment dialogFragment = new CreateAssetFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        }
    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }



}



