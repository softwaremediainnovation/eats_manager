package app.direksi.hras;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.gson.Gson;

import app.direksi.hras.fragment.CertificateEmployeFragment;
import app.direksi.hras.fragment.EducationEmployeFragment;
import app.direksi.hras.fragment.JobEmployeFragment;
import app.direksi.hras.fragment.OrganizationEmployeFragment;
import app.direksi.hras.fragment.OrganizationFragment;
import app.direksi.hras.fragment.ProfileSelfFragment;
import app.direksi.hras.fragment.RelativeEmployeeFragment;
import app.direksi.hras.fragment.RuleEmployeFragment;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class ProfileActivity extends AppCompatActivity
        implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;

    private ImageView mProfileImage;
    private int mMaxScrollSize;
    DataEmployee dataEmploye;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(ProfileActivity.this);
        setContentView(R.layout.activity_detail_employe);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
        //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.profile));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);

        Gson gson = new Gson();
        dataEmploye = gson.fromJson(getIntent().getStringExtra("detail"), DataEmployee.class);

        String id = (PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString(
                getResources().getString(R.string.pref_employeeid), ""));


        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                edit().putString(getResources().getString(R.string.pref_employeeid_view), id)
                .apply();

        //viewPager.setOffscreenPageLimit(8);


        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        viewPager.setAdapter(new ProfileActivity.TabsAdapter(getSupportFragmentManager(), dataEmploye,ProfileActivity.this));
        tabLayout.setupWithViewPager(viewPager);
    }

    public static void start(Context c) {
        c.startActivity(new Intent(c, DetailEmployeActivity.class));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
    }

    private static class TabsAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 8;
        private DataEmployee data;
        private Context context;

        TabsAdapter(FragmentManager fm, DataEmployee a, Context cont) {
            super(fm);
            this.data = a;
            context = cont;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int i) {

            final Fragment result;
            switch (i) {
                case 0:
                    // First Tab
                    result = ProfileSelfFragment.newInstance();
                    break;
                case 1:
                    // Second Tab
                    result = OrganizationFragment.newInstance();
                    break;
                case 2:
                    // Second Tab
                    result = RuleEmployeFragment.newInstance();
                    break;
                case 3:
                    // Second Tab
                    result = RelativeEmployeeFragment.newInstance();
                    break;
                case 4:
                    // Third Tab
                    result = OrganizationEmployeFragment.newInstance();
                    break;
                case 5:
                    // Third Tab
                    result = JobEmployeFragment.newInstance();
                    break;
                case 6:
                    // Third Tab
                    result = EducationEmployeFragment.newInstance();
                    break;
                case 7:
                    // Third Tab
                    result = CertificateEmployeFragment.newInstance();
                    break;
                default:
                    result = FakePageFragment.newInstance();
                    break;
            }
            return result;



        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title ="";
            if (position == 0){
                title = context.getResources().getString(R.string.title_profil);
            }
            if (position == 1){
                title = context.getResources().getString(R.string.title_organisasi);
            }
            if (position == 2){
                title = context.getResources().getString(R.string.title_peraturan);
            }
            if (position == 3){
                title = context.getResources().getString(R.string.title_kerabat);
            }
            if (position == 4){
                title = context.getResources().getString(R.string.title_pengalaman_organisasi);
            }
            if (position == 5){
                title = context.getResources().getString(R.string.title_pekerjaan);
            }
            if (position == 6){
                title = context.getResources().getString(R.string.title_pendidikan);
            }
            if (position == 7){
                title = context.getResources().getString(R.string.title_sertifikast);
            }
            return title;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_setting:
                Intent intent = new Intent(ProfileActivity.this, SettingActivity.class);
                startActivity(intent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(ProfileActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
