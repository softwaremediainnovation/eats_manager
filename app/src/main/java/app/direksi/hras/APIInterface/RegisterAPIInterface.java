package app.direksi.hras.APIInterface;


import java.util.Map;

import app.direksi.hras.model.FetchVersion;
import app.direksi.hras.model.FormPenilaian;
import app.direksi.hras.model.PostTask;
import app.direksi.hras.model.PostTaskSchedule;
import app.direksi.hras.model.ResponseAset;
import app.direksi.hras.model.ResponseCertificateEmploye;
import app.direksi.hras.model.ResponseCheckIn;
import app.direksi.hras.model.ResponseClaim;
import app.direksi.hras.model.ResponseCompanies;
import app.direksi.hras.model.ResponseConstanta;
import app.direksi.hras.model.ResponseCountToday;
import app.direksi.hras.model.ResponseCounter;
import app.direksi.hras.model.ResponseCreateAsset;
import app.direksi.hras.model.ResponseCreateCuti;
import app.direksi.hras.model.ResponseCreateOvertimes;
import app.direksi.hras.model.ResponseCreateReimbursement;
import app.direksi.hras.model.ResponseCreateSnap;
import app.direksi.hras.model.ResponseCreateTeguran;
import app.direksi.hras.model.ResponseCuti;
import app.direksi.hras.model.ResponseDashboard;
import app.direksi.hras.model.ResponseDataAktivitas;
import app.direksi.hras.model.ResponseDataAttendance;
import app.direksi.hras.model.ResponseDataBirthday;
import app.direksi.hras.model.ResponseDataChangePassword;
import app.direksi.hras.model.ResponseDataCheckAbsenIN;
import app.direksi.hras.model.ResponseDataCompany;
import app.direksi.hras.model.ResponseDataContractExpired;
import app.direksi.hras.model.ResponseDataDetailEmployeeInfo;
import app.direksi.hras.model.ResponseDataEmployee;
import app.direksi.hras.model.ResponseDataEmployeeID;
import app.direksi.hras.model.ResponseDataEmployeeResign;
import app.direksi.hras.model.ResponseDataGetSummary;
import app.direksi.hras.model.ResponseDataHoliday;
import app.direksi.hras.model.ResponseDataKategoriAset;
import app.direksi.hras.model.ResponseDataKeyXendit;
import app.direksi.hras.model.ResponseDataLastReprimand;
import app.direksi.hras.model.ResponseDataListCompanies;
import app.direksi.hras.model.ResponseDataListOrgz;
import app.direksi.hras.model.ResponseDataMembership;
import app.direksi.hras.model.ResponseDataOvertimeAttendance;
import app.direksi.hras.model.ResponseDataRemu;
import app.direksi.hras.model.ResponseDataReprimand;
import app.direksi.hras.model.ResponseDataRequestAttendance;
import app.direksi.hras.model.ResponseDataSalary;
import app.direksi.hras.model.ResponseDataTask;
import app.direksi.hras.model.ResponseDataTaskIDDetail;
import app.direksi.hras.model.ResponseDataTaskIDDetailSchedule;
import app.direksi.hras.model.ResponseDataTrackingAset;
import app.direksi.hras.model.ResponseDetailActivities;
import app.direksi.hras.model.ResponseDetailAset;
import app.direksi.hras.model.ResponseDetailAsetGeneral;
import app.direksi.hras.model.ResponseDetailAttendance;
import app.direksi.hras.model.ResponseDetailClaim;
import app.direksi.hras.model.ResponseDetailCuti;
import app.direksi.hras.model.ResponseDetailEmployeeShare;
import app.direksi.hras.model.ResponseDetailLembur;
import app.direksi.hras.model.ResponseDetailLoan;
import app.direksi.hras.model.ResponseDetailLoanWithDetail;
import app.direksi.hras.model.ResponseDetailNews;
import app.direksi.hras.model.ResponseDetailReprimand;
import app.direksi.hras.model.ResponseDetailRequestAttendance;
import app.direksi.hras.model.ResponseDetailTeguran;
import app.direksi.hras.model.ResponseDisbursement;
import app.direksi.hras.model.ResponseEducationEmploye;
import app.direksi.hras.model.ResponseEmploye;
import app.direksi.hras.model.ResponseFetchEmployee;
import app.direksi.hras.model.ResponseForgetPassword;
import app.direksi.hras.model.ResponseGaji;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.model.ResponseGeneralRequestAttendance;
import app.direksi.hras.model.ResponseGetActivityForReimburse;
import app.direksi.hras.model.ResponseGetKasbon;
import app.direksi.hras.model.ResponseGetServerTimePost;
import app.direksi.hras.model.ResponseInformations;
import app.direksi.hras.model.ResponseJobEmploye;
import app.direksi.hras.model.ResponseKategori;
import app.direksi.hras.model.ResponseLastOvertime;
import app.direksi.hras.model.ResponseLastSummaryAttendance;
import app.direksi.hras.model.ResponseLembur;
import app.direksi.hras.model.ResponseLoan;
import app.direksi.hras.model.ResponseLoginPengguna;
import app.direksi.hras.model.ResponseMyAssetList;
import app.direksi.hras.model.ResponseNews;
import app.direksi.hras.model.ResponseOrganizationEmploye;
import app.direksi.hras.model.ResponsePostLoan;
import app.direksi.hras.model.ResponseReimbursCategory;
import app.direksi.hras.model.ResponseRelativeEmploye;
import app.direksi.hras.model.ResponseRiwayatPenilaian;
import app.direksi.hras.model.ResponseSaldoXendit;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.model.ResponseSnap;
import app.direksi.hras.model.ResponseTracking;
import app.direksi.hras.model.ResponseUploadPhoto;
import app.direksi.hras.model.ResultDisbursement;
import app.direksi.hras.model.ResultDisbursementLoan;
import app.direksi.hras.model.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

public interface RegisterAPIInterface {

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/Login")
    Call<ResponseLoginPengguna> mLoginUser(@Header("Authorization") String Authorization, @Body User user);

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/Evaluations")
    Call<ResponseGeneral> createEvaluation(@Header("Authorization") String Authorization, @Body FormPenilaian formPenilaian);

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/disbursements")
    Call<ResponseDisbursement> requestDisbursement(@Header("Authorization") String Authorization, @Body ResponseDisbursement disbursement);

    @GET("/balance")
    Call<ResponseSaldoXendit> getSaldo(@Header("Authorization") String Authorization);

    @GET("/disbursements/{id}")
    Call<ResponseDisbursement> getDetailTransfer(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Xendits/GetKeyXendit")
    Call<ResponseDataKeyXendit> getKeyXendit(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @Multipart
    @POST("/api/attendances")
    Call<ResponseCheckIn> CheckIn(@Header("Authorization") String Authorization,
                                  @Part("Latitude") RequestBody Latitude,
                                  @Part("Longitude") RequestBody Longitude
    );


    @Multipart
    @POST("/api/Attendances/PostAttendanceBiometric")
    Call<ResponseCheckIn> mPostAttendancesInOutWorkingPatternBiometric (@Header("Authorization") String Authorization,
                                                               @Part("Longitude") RequestBody Longitude,
                                                               @Part("Latitude") RequestBody Latitude,
                                                               @Part("InOut") RequestBody InOut,
                                                               @Part("Address") RequestBody Address,
                                                               @Part("GMT") RequestBody GMT,
                                                               @Part MultipartBody.Part File);

    @Multipart
    @POST("/api/Attendances/PostAttendanceBiometric")
    Call<ResponseCheckIn> mPostAttendancesInOutDateWorkingPatternBiometric (@Header("Authorization") String Authorization,
                                                                   @Part("Longitude") RequestBody Longitude,
                                                                   @Part("Latitude") RequestBody Latitude,
                                                                   @Part("InOut") RequestBody InOut,
                                                                   @Part("Address") RequestBody Address,
                                                                   @Part("IsYesterday") RequestBody IsYesterday,
                                                                   @Part("GMT") RequestBody GMT,
                                                                   @Part MultipartBody.Part File);


    @Multipart
    @POST("/api/Attendances/PostAttendancePhoto")
    Call<ResponseCheckIn> mPostAttendancesInOutWorkingPatternPhoto (@Header("Authorization") String Authorization,
                                                               @Part("Longitude") RequestBody Longitude,
                                                               @Part("Latitude") RequestBody Latitude,
                                                               @Part("InOut") RequestBody InOut,
                                                               @Part("Address") RequestBody Address,
                                                               @Part("GMT") RequestBody GMT,
                                                               @Part MultipartBody.Part File);

    @Multipart
    @POST("/api/Attendances/PostAttendancePhoto")
    Call<ResponseCheckIn> mPostAttendancesInOutDateWorkingPatternPhoto (@Header("Authorization") String Authorization,
                                                                   @Part("Longitude") RequestBody Longitude,
                                                                   @Part("Latitude") RequestBody Latitude,
                                                                   @Part("InOut") RequestBody InOut,
                                                                   @Part("Address") RequestBody Address,
                                                                   @Part("IsYesterday") RequestBody IsYesterday,
                                                                   @Part("GMT") RequestBody GMT,
                                                                   @Part MultipartBody.Part File);

    @GET("/api/Companies")
    Call<ResponseDataCompany> getCompany(@Header("Authorization") String Authorization);

    @GET("/api/Dashboards")
    Call<ResponseDashboard> mGetDashboard(@Header("Authorization") String Authorization);

    @GET("/api/Versions")
    Call<FetchVersion> getVersion(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Reprimands/filter")
    Call<ResponseDataReprimand> getTeguran(@Header("Authorization") String Authorization , @QueryMap Map<String, String> options);

    @GET("/api/Reprimands/GetByEmployee")
    Call<ResponseDataReprimand> getTeguranSelected(@Header("Authorization") String Authorization , @QueryMap Map<String, String> options);

    @GET("/api/overtimes/filter")
    Call<ResponseLembur> getLembur(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/overtimes/GetByEmployee")
    Call<ResponseLembur> getLemburSelected(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/reimbursements/filter")
    Call<ResponseClaim> getClaim(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/reimbursements/GetByEmployee")
    Call<ResponseClaim> getClaimSelected(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/employees/filterActivity")
    Call<ResponseDataEmployee> getEmployeeActivity(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Overtimes/GetAttendance")
    Call<ResponseDataOvertimeAttendance> getOvertimeAttendance(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);


    @GET("/api/employees/filter")
    Call<ResponseDataEmployee> getEmployee(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Memberships/filter")
    Call<ResponseDataMembership> getMemberships(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Evaluations/filter")
    Call<ResponseRiwayatPenilaian> getRiwayatPenilaian(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Remunerations/RemunerationFilter")
    Call<ResponseDataSalary> getSalary(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/employees/{id}")
    Call<ResponseDataEmployeeID> getEmployeeID(@Header("Authorization") String Authorization,  @Path("id") String id);

    @GET("api/Employees/GetEmployeeShare/{id}")
    Call<ResponseDetailEmployeeShare> getEmployeeIDShare(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/EmployeeRelatives/Employee")
    Call<ResponseRelativeEmploye> getRelativeEmployee(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/EmployeeEmployments/Employee")
    Call<ResponseJobEmploye> getJobEmploye(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/EmployeeCertificates/Employee")
    Call<ResponseCertificateEmploye> getCertificateEmploye(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/EmployeeEducations/Employee")
    Call<ResponseEducationEmploye> getEducationEmploye(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/EmployeeOrganizations/Employee")
    Call<ResponseOrganizationEmploye> getOrganizationEmploye(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/attendances")
    Call<ResponseDataAttendance> getAttendance(@Header("Authorization") String Authorization);

    @GET("/api/Employees/GetCounterManager")
    Call<ResponseCounter> getCounter(@Header("Authorization") String Authorization);

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/Employees/Logout")
    Call<ResponseGeneral> Logout(@Header("Authorization") String Authorization);

    @GET("/api/attendances/GetAttendanceCalendar")
    Call<ResponseDataAttendance> getAttendanceHariIni(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/attendances/GetAttendanceCalendarLastMonth")
    Call<ResponseDataAttendance> getAttendanceLastMonth(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/attendances/GetAttendanceCalendarLateToday")
    Call<ResponseDataAttendance> getAttendanceTelatHariIni(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Attendances/GetAttendanceCountToday")
    Call<ResponseCountToday> getCountToday(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/attendances/GetAttendanceCalendarNotLateToday")
    Call<ResponseDataAttendance> getAttendanceTidakTelatHariIni(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/getlatestsummarybyemployeeid")
    Call<ResponseDataGetSummary> getSummaryEmployee(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Employees/GetEmployeeInfo")
    Call<ResponseDataDetailEmployeeInfo> getEmployeeInfo(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/loans/filter")
    Call<ResponseLoan> getLoan(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Attendances/RequestAttendanceFilter")
    Call<ResponseDataRequestAttendance> getRequestAttendance(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/articles/filter")
    Call<ResponseNews> getNews(@Header("Authorization") String Authorization , @QueryMap Map<String, String> options);

    @GET("/api/Employees/GetEmployeeBirthday")
    Call<ResponseDataBirthday> getBirthday(@Header("Authorization") String Authorization);

    @GET("/api/Employees/GetEmployeeKontrak3MonthEnd")
    Call<ResponseDataContractExpired> getContractExpired(@Header("Authorization") String Authorization);

    @GET("/api/Employees/GetEmployeeResign")
    Call<ResponseDataEmployeeResign> getEmployeeResign(@Header("Authorization") String Authorization , @QueryMap Map<String, String> options);

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/ChangePassword")
    Call<ResponseDataChangePassword> changePassword(@Header("Authorization") String Authorization , @QueryMap Map<String, String> options);

    @Multipart
    @POST("/api/LupaPassword")
    Call<ResponseForgetPassword> forgetPassword(@Part("email") RequestBody email);

    @GET("/api/DayOffs/filter")
    Call<ResponseCuti> mGetListCuti(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Activities/FilterEmployee")
    Call<ResponseDataAktivitas> getAktivitasByEmployee(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Activities/filter")
    Call<ResponseDataAktivitas> getAktivitas(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/DayOffs/filter")
    Call<ResponseCuti> mGetListCutiList(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/TaskHeaderSchedules/TaskFilter")
    Call<ResponseDataTask> taskFilter(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/DayOffs/GetByEmployee")
    Call<ResponseCuti> mGetListCutiListSelected(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Assets/filter")
    Call<ResponseDataTrackingAset> getTrackingAset(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Assets/Employee")
    Call<ResponseMyAssetList> mGetAssetByEmp(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Overtimes/GetLatestOvertimeByEmployeeID")
    Call<ResponseLastOvertime> getLastOvertime(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Reprimands/GetLatestReprimandByEmployeeID")
    Call<ResponseDataLastReprimand> getLastReprimand(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/DayOffs/GetLatestDayoffByEmployeeID")
    Call<ResponseDetailCuti> getLastDayOff(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Attendances/RequestAttendanceByDate")
    Call<ResponseLastSummaryAttendance> getLastSummaryAttendance(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Loans/GetLatestLoanByEmployeeID")
    Call<ResponseDetailLoan> getLastLoan(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Reimbursements/GetLatestReimbursementByEmployeeID")
    Call<ResponseDetailClaim> getLastReimbursment(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Reimbursements/GetListApprovedReimbursementByEmployeeID")
    Call<ResponseClaim> GetListApprovedReimbursementByEmployeeID(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/DayOffs/Constanta")
    Call<ResponseConstanta> mGetConstanta(@Header("Authorization") String Authorization);

    @GET("/api/Assets/GetAssetCategory")
    Call<ResponseDataKategoriAset> mGetConstantaAset(@Header("Authorization") String Authorization);

    @GET("/api/Attendances/CheckAlreadyIN")
    Call<ResponseDataCheckAbsenIN> getDataCheckAbsenIN(@Header("Authorization") String Authorization);

    @GET("/api/Attendances/CheckAlreadyOUT")
    Call<ResponseDataCheckAbsenIN> getDataCheckAbsenOUT(@Header("Authorization") String Authorization);

    @GET("/api/getservertime")
    Call<ResponseServerTime> getServerTime(@Header("Authorization") String Authorization);
    
    @GET("/api/employees")
    Call<ResponseEmploye> getEmploye(@Header("Authorization") String Authorization);

    @GET("/api/companies")
    Call<ResponseCompanies> mGetCompanies(@Header("Authorization") String Authorization);

    @GET("/api/Companies/GetCompanyByRole")
    Call<ResponseDataListCompanies> getCompanies( @QueryMap Map<String, String> options, @Header("Authorization") String Authorization);

    @GET("/api/Companies/GetOrganizationByRole")
    Call<ResponseDataListOrgz> getOrg( @QueryMap Map<String, String> options, @Header("Authorization") String Authorization);

    @GET("/api/assets")
    Call<ResponseAset> getAset(@Header("Authorization") String Authorization);

    @GET("/api/assets/Employee")
    Call<ResponseAset> getAsetEmployee(@Header("Authorization") String Authorization);

    @GET("/api/assetTrackings")
    Call<ResponseTracking> getTracking(@Header("Authorization") String Authorization);

    @GET("/api/remunerations/filter")
    Call<ResponseGaji> getGaji(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/reprimands/category")
    Call<ResponseKategori> getCategory(@Header("Authorization") String Authorization);

    @GET("/api/DayOffs/{id}")
    Call<ResponseDetailCuti> getCutiDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Evaluations/GetInformation")
    Call<ResponseInformations> getInformation(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/reprimands/{id}")
    Call<ResponseDetailTeguran> getTeguranDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/reprimands/{id}")
    Call<ResponseDetailReprimand> getTeguranDetailReprimand(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/TaskHeaders/{id}")
    Call<ResponseDataTaskIDDetail> getTaskDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/TaskHeaderSchedules/{id}")
    Call<ResponseDataTaskIDDetailSchedule> getTaskDetailSchedule(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Overtimes/{id}")
    Call<ResponseDetailLembur> getLemburDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Remunerations/{id}")
    Call<ResponseDataRemu> getRemuDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Employees/GetEmployeeHolidayCalendar")
    Call<ResponseDataHoliday> getListHoliday(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @GET("/api/Reimbursements/{id}")
    Call<ResponseDetailClaim> getClaimDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Assets/Tracking/{id}")
    Call<ResponseDetailAset> getDetailAset(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Assets/{id}")
    Call<ResponseDetailAsetGeneral> getDetailAsetGeneral(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Loans/{id}")
    Call<ResponseDetailLoanWithDetail> getLoansDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Memberships/{id}")
    Call<ResponseDataMembership> getMembershipDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Articles/{id}")
    Call<ResponseDetailNews> getDetailNews(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Attendances/{id}")
    Call<ResponseDetailAttendance> getAttendanceDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/Attendances/{id}")
    Call<ResponseDetailRequestAttendance> getRequestAttendanceDetail(@Header("Authorization") String Authorization, @Path("id") String id);

    @GET("/api/searchEmployee")
    Call<ResponseFetchEmployee> mGetListEmp(@Header("Authorization") String Authorization,
                                            @QueryMap Map<String, String> options);

    @GET("/api/Reprimands/searchEmployee")
    Call<ResponseFetchEmployee> mGetListEmpReprimands(@Header("Authorization") String Authorization,
                                         @QueryMap Map<String, String> options);

    @GET("/api/TaskHeaders/SearchEmployee")
    Call<ResponseFetchEmployee> mGetListEmpTask(@Header("Authorization") String Authorization,
                                                   @QueryMap Map<String, String> options);

    @GET("/api/activities/{id}")
    Call<ResponseDetailActivities> mGetDetailActivities(@Path("id") String id, @Header("Authorization") String Authorization);

    @Multipart
    @POST("/api/DayOffs")
    Call<ResponseCreateCuti> mCreateDayOffS(@Header("Authorization") String Authorization,
                                           @Part("ConstantaID") RequestBody ConstantaID,
                                           @Part("EmployeeID") RequestBody EmployeeID,
                                           @Part("HowManyDays") RequestBody HowManyDays,
                                           @Part("Date") RequestBody Date,
                                           @Part("Note") RequestBody Note,
                                           @Part MultipartBody.Part file
                                           //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/reprimands")
    Call<ResponseCreateTeguran> createTeguran(@Header("Authorization") String Authorization,
                                              @Part("ReprimandCategoryID") RequestBody ConstantaID,
                                              @Part("EmployeeID") RequestBody EmployeeID,
                                              @Part("Date") RequestBody Date,
                                              @Part("Note") RequestBody Note,
                                              @Part MultipartBody.Part reprimandfile
                                              //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/Assets/Handover")
    Call<ResponseGeneral> createHandOver(@Header("Authorization") String Authorization,
                                           @Part("AssetID") RequestBody assetID,
                                           @Part("DestinationHolderID") RequestBody DestinationHolderID,
                                           @Part("Note") RequestBody Note
                                           //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/employees/uploadimage")
    Call<ResponseUploadPhoto> UploadImages(@Header("Authorization") String Authorization,
                                           @Part("Id") RequestBody EmployeeID,
                                           @Part MultipartBody.Part File
                                           //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/DayOffs/Approve")
    Call<ResponseGeneral> ApproveCuti(@Header("Authorization") String Authorization,
                                      @Part("id") RequestBody id,
                                      @Part("isApprove") RequestBody isApprove,
                                      @Part("comment") RequestBody comment
    );

    @Multipart
    @POST("/api/Attendances/Approve")
    Call<ResponseGeneral> ApproveRequestAttendance(@Header("Authorization") String Authorization,
                                      @Part("id") RequestBody id,
                                      @Part("isApprove") RequestBody isApprove,
                                      @Part("comment") RequestBody comment,
                                      @Part("GMT") RequestBody GMT
    );

    @Multipart
    @POST("/api/Assets/Approve")
    Call<ResponseGeneral> ApproveAset(@Header("Authorization") String Authorization,
                                   @Part("id") RequestBody id,
                                   @Part("isApprove") RequestBody isApprove,
                                   @Part("comment") RequestBody comment
    );

    @Multipart
    @POST("/api/Overtimes/Approve")
    Call<ResponseGeneral> ApproveLembur(@Header("Authorization") String Authorization,
                                      @Part("id") RequestBody id,
                                      @Part("isApprove") RequestBody isApprove,
                                      @Part("comment") RequestBody comment
    );

    @Multipart
    @POST("/api/Reimbursements/Approve")
    Call<ResponseGeneral> ApproveKlaim(@Header("Authorization") String Authorization,
                                      @Part("id") RequestBody id,
                                      @Part("isApprove") RequestBody isApprove,
                                      @Part("comment") RequestBody comment,
                                       @Part("amount") RequestBody amount
    );



    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/Reimbursements/MultipleCair")
    Call<ResponseGeneral> CairClaim(@Header("Authorization") String Authorization,
                                    @Body ResultDisbursement resultDisbursement


     );

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/TaskHeaders/PostAddTaskHeader")
    Call<ResponseGeneral> PostTask(@Header("Authorization") String Authorization,
                                 @Body PostTask PostTask


    );

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/TaskHeaderSchedules/PostAddTaskHeaderSchedule")
    Call<ResponseGeneral> PostTaskSchedule(@Header("Authorization") String Authorization,
                                   @Body PostTaskSchedule PostTaskSchedule


    );

    @Headers({"Content-Type: application/json", "X-Requested-With: XMLHttpRequest"})
    @POST("/api/Loans/Cair")
    Call<ResponseGeneral> CairLoan(@Header("Authorization") String Authorization,
                                   @Body ResultDisbursementLoan resultDisbursement

    );

    @Multipart
    @POST("/api/Loans/Approve")
    Call<ResponseGeneral> ApproveLoans(@Header("Authorization") String Authorization,
                                      @Part("id") RequestBody id,
                                      @Part("isApprove") RequestBody isApprove,
                                      @Part("comment") RequestBody comment,
                                      @Part("amount") RequestBody amount,
                                      @Part("installment") RequestBody installment,
                                      @Part("loanStartDate") RequestBody loanStartDate
    );


    @Multipart
    @POST("/api/Assets")
    Call<ResponseCreateAsset> mCreateAsset(@Header("Authorization") String Authorization,
                                           @Part("Name") RequestBody Name,
                                           @Part("LastLocation") RequestBody EmployeeID,
                                           @Part("Note") RequestBody HowManyDays,
                                           @Part("ExpiredDate") RequestBody ExpiredDate,
                                           @Part("ConstantaID") RequestBody ConstantaID,
                                           @Part MultipartBody.Part file);

    @Multipart
    @POST("/api/Assets/EditExpiryDate")
    Call<ResponseGeneral> editAset(@Header("Authorization") String Authorization,
                                           @Part("id") RequestBody Id,
                                           @Part("expiryDate") RequestBody expiryDate
                                         );

    @Multipart
    @POST("/api/Employees/UploadImage")
    Call<ResponseUploadPhoto> uploadImage(@Header("Authorization") String Authorization,
                                          @Part("Id") RequestBody ids,
                                          @Part MultipartBody.Part file);

    @Multipart
    @POST("/api/DayOffs")
    Call<ResponseGeneralRequestAttendance> mCreateDayOff(@Header("Authorization") String Authorization,
                                              @Part("ConstantaID") RequestBody ConstantaID,
                                              @Part("EmployeeID") RequestBody EmployeeID,
                                              @Part("HowManyDays") RequestBody HowManyDays,
                                              @Part("Date") RequestBody Date,
                                              @Part("Note") RequestBody Note,
                                              @Part("IsDisease") RequestBody IsDisease,
                                              @Part MultipartBody.Part file
                                              //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/Loans")
    Call<ResponsePostLoan> mPostLoans(@Header("Authorization") String Authorization,
                                      @Part("Name") RequestBody Name,
                                      @Part("Date") RequestBody Date,
                                      @Part("Amount") RequestBody Amount,
                                      @Part("Installment") RequestBody Installment,
                                      @Part("Note") RequestBody Note,
                                      @Part("IsPbonMingguan") RequestBody IsPbonMingguan,
                                      @Part("EmployeeID") RequestBody EmployeeID,
                                      @Part("DatePaid") RequestBody DatePaid
    );

    @Multipart
    @POST("/api/Overtimes")
    Call<ResponseCreateOvertimes> mCreateOvertime(@Header("Authorization") String Authorization,
                                                  @Part("DateStart") RequestBody DateStart,
                                                  @Part("DateEnd") RequestBody DateEnd,
                                                  @Part("Note") RequestBody Note,
                                                  @Part("EmployeeID") RequestBody EmployeeID,
                                                  @Part MultipartBody.Part file
                                                  //@Part MultipartBody.Part file

    );

    @GET("/api/Reimbursements/Category")
    Call<ResponseReimbursCategory> mGetCategoryReimburse(@Header("Authorization") String Authorization);


    @GET("/api/Reimbursements/GetKasbonByUser")
    Call<ResponseGetKasbon> mGetKasbonForReimburse(@Header("Authorization") String Authorization);

    @GET("/api/Activities/GetActivityByDate")
    Call<ResponseGetActivityForReimburse> mGetActivityForReimburse(@Header("Authorization") String Authorization, @QueryMap Map<String, String> options);

    @Multipart
    @POST("/api/Reimbursements")
    Call<ResponseCreateReimbursement> mCreateReimburse(@Header("Authorization") String Authorization,
                                                       @Part("ReimbursementCategoryID") RequestBody ReimbursementCategoryID,
                                                       @Part("Date") RequestBody Date,
                                                       @Part("Amount") RequestBody Amount,
                                                       @Part("Note") RequestBody Note,
                                                       @Part("employeeID") RequestBody employeeID,
                                                       @Part("KasbonID") RequestBody KasbonID,
                                                       @Part("ActivityID") RequestBody ActivityID,
                                                       @Part MultipartBody.Part file
                                                       //@Part MultipartBody.Part file

    );

 @Multipart
 @POST("/api/Activities")
 Call<ResponseGeneral> mCreateActivites(@Header("Authorization") String Authorization,
                                        @Part("EmployeeID") RequestBody ConstantaID,
                                        @Part("Date") RequestBody EmployeeID,
                                        @Part("Description") RequestBody HowManyDays,
                                        @Part("Latitude") RequestBody Date,
                                        @Part("Longitude") RequestBody Note,
                                        @Part("Address") RequestBody Address,
                                        @Part MultipartBody.Part file
                                        //@Part MultipartBody.Part file

 );

 @Multipart
 @POST("/api/TaskHeaders/EditDetailHeaderTask")
 Call<ResponseGeneral> updateTask(@Header("Authorization") String Authorization,
                                        @Part("TaskDetailID") RequestBody TaskDetailID,
                                        @Part("Note") RequestBody Note,
                                        @Part("Latitude") RequestBody Latitude,
                                        @Part("Longitude") RequestBody Longitude,
                                        @Part("Address") RequestBody Address,
                                        @Part MultipartBody.Part file
                                        //@Part MultipartBody.Part file

 );

    @Multipart
    @POST("/api/Attendances/RequestAttendanceBiometric")
    Call<ResponseGeneralRequestAttendance> mCreateRequestAttendanceBiometric(@Header("Authorization") String Authorization,
                                                                    @Part("InOut") RequestBody EmployeeID,
                                                                    @Part("Note") RequestBody HowManyDays,
                                                                    @Part("Latitude") RequestBody Date,
                                                                    @Part("Longitude") RequestBody Note,
                                                                    @Part("Address") RequestBody Address,
                                                                    @Part MultipartBody.Part file
                                                                    //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/Attendances/RequestAttendanceBiometric")
    Call<ResponseGeneralRequestAttendance> mCreateRequestAttendanceBiometric(@Header("Authorization") String Authorization,
                                                                    @Part("InOut") RequestBody EmployeeID,
                                                                    @Part("Note") RequestBody HowManyDays,
                                                                    @Part("Latitude") RequestBody Date,
                                                                    @Part("Longitude") RequestBody Note,
                                                                    @Part("Address") RequestBody Address,
                                                                    @Part MultipartBody.Part file,
                                                                    @Part("IsYesterday") RequestBody IsYesterday

    );

    @Multipart
    @POST("/api/Attendances/RequestAttendancePhoto")
    Call<ResponseGeneralRequestAttendance> mCreateRequestAttendancePhoto(@Header("Authorization") String Authorization,
                                                                    @Part("InOut") RequestBody EmployeeID,
                                                                    @Part("Note") RequestBody HowManyDays,
                                                                    @Part("Latitude") RequestBody Date,
                                                                    @Part("Longitude") RequestBody Note,
                                                                    @Part("Address") RequestBody Address,
                                                                    @Part MultipartBody.Part file
                                                                    //@Part MultipartBody.Part file

    );

    @Multipart
    @POST("/api/Attendances/RequestAttendancePhoto")
    Call<ResponseGeneralRequestAttendance> mCreateRequestAttendancePhoto(@Header("Authorization") String Authorization,
                                                                    @Part("InOut") RequestBody EmployeeID,
                                                                    @Part("Note") RequestBody HowManyDays,
                                                                    @Part("Latitude") RequestBody Date,
                                                                    @Part("Longitude") RequestBody Note,
                                                                    @Part("Address") RequestBody Address,
                                                                    @Part MultipartBody.Part file,
                                                                    @Part("IsYesterday") RequestBody IsYesterday

    );

    @Multipart
    @POST("/api/postservertime")
    Call<ResponseGetServerTimePost> getServerPost(
                                                             @Part("EmployeeID") RequestBody EmployeeID,
                                                             @Part("Latitude") RequestBody Date,
                                                             @Part("Longitude") RequestBody Note,
                                                             @Part MultipartBody.Part file
                                                             //@Part MultipartBody.Part file

    );



    @Multipart
    @POST("api/Snap")
    Call<ResponseCreateSnap> mCreateSnap(@Part("email") RequestBody Note,
                                      @Part MultipartBody.Part file
                                      //@Part MultipartBody.Part file

    );


    @DELETE("/api/Dayoffs/{id}")
    Call<ResponseGeneral> deleteCuti(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/TaskHeaders/{id}")
    Call<ResponseGeneral> deleteTask(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/TaskHeaderSchedules/{id}")
    Call<ResponseGeneral> deleteTaskDaily(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/Reprimands/{id}")
    Call<ResponseGeneral> deleteTeguran(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/Overtimes/{id}")
    Call<ResponseGeneral> deleteLembur(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/Reimbursements/{id}")
    Call<ResponseGeneral> deleteClaim(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/Loans/{id}")
    Call<ResponseGeneral> deletePinjaman(@Path("id") String id, @Header("Authorization") String Authorization);

    @DELETE("/api/Attendances/DeleteRequestAttendance/{id}")
    Call<ResponseGeneral> deleteRequestAttendance(@Path("id") String id, @Header("Authorization") String Authorization);

    @GET("api/Snap/GetListFile")
    Call<ResponseSnap> mGetSnap(@QueryMap Map<String, String> options);




}
