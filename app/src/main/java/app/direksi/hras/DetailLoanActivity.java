package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.CairLoanFragment;
import app.direksi.hras.model.DetailLoanWithDetailData;
import app.direksi.hras.model.ResponseDetailLoanWithDetail;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.fragment.ApproveLoanFragment;
import app.direksi.hras.model.DataLoan;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.CanApproveVoid;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 26/02/2019.
 */

public class DetailLoanActivity extends AppCompatActivity {

    DataLoan dataLoan;
    DetailLoanWithDetailData temp;
    TableLayout tbApprover, tbCicilan, tbApprover2, tbisCair, tbDetailCair;
    public TextView mTxtStart,
            txtJumlah,
            mTxtStatus,
            mTxtNote,
            txtApprover,
            txtComment,
            txtDateApprove,
            txtInstallment,
            txtInstallmentPayOut,
            txtInstallmentAmount,
            txtPaid,
            txtBon,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtNamaPinjaman,
            txtDateApprove2,
            txtApprover2,
            txtComment2,
            txtOrganisasi,
            txtstatusCair,
            txtDateCair,
            txtCairer,
            txtCairComment,
            txtCek,
            txtPaymentRemaining,
            txtTransfer,
            txtPotong;
    private CheckBox item_check;
    private ImageView mIconStatus, profile_image, mIconStatusCair;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;
    private Long id;
    private Button prosesDelete, prosesRequest, prosesCair, cekSaldo;

    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;
    private static final int REQUEST_PHONE_CALL = 1;

    private TableRow trPotong;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailLoanActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
     //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_pinjaman));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_loan);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        dataLoan = gson.fromJson(getIntent().getStringExtra("detail"), DataLoan.class);
        txtPotong = findViewById(R.id.txtPotong);
        trPotong = findViewById(R.id.trPotong);
        txtDateApprove = findViewById(R.id.txtDateApprove);
        item_check = findViewById(R.id.item_check);
        tbCicilan = findViewById(R.id.tbCicilan);
        tbCicilan.setVisibility(View.GONE);
        tbApprover = findViewById(R.id.tbApprover);
        tbApprover.setVisibility(View.GONE);
        tbApprover2 = findViewById(R.id.tbApprover2);
        tbApprover2.setVisibility(View.GONE);
        tbisCair = findViewById(R.id.tbisCair);
        tbisCair.setVisibility(View.GONE);
        tbDetailCair = findViewById(R.id.tbDetailCair);
        tbDetailCair.setVisibility(View.GONE);
        txtApprover = findViewById(R.id.txtApprover);
        txtComment = findViewById(R.id.txtComment);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        mTxtNote = findViewById(R.id.mTxtNote);
        txtJumlah = findViewById(R.id.txtJumlah);
        mTxtStart = findViewById(R.id.mTxtStart);
        //  mTxtJudul = findViewById(R.id.mTxtJudul);
        mLayoutImage = findViewById(R.id.mLayoutImage);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        mIconStatus = findViewById(R.id.mIconStatus);
        mIconStatusCair = findViewById(R.id.mIconStatusCair);
        mImageView = findViewById(R.id.mImageView);
        txtInstallment = findViewById(R.id.txtInstallment);
        txtInstallmentPayOut = findViewById(R.id.txtInstallmentPayOut);
        txtInstallmentAmount = findViewById(R.id.txtInstallmentAmount);
        txtPaid = findViewById(R.id.txtPaid);
        txtBon = findViewById(R.id.txtBon);
        txtNamaPinjaman = findViewById(R.id.txtNamaPinjaman);
        prosesCair = findViewById(R.id.prosesCair);
        cekSaldo = findViewById(R.id.cekSaldo);
        prosesDelete = findViewById(R.id.prosesDelete);
        prosesRequest = findViewById(R.id.prosesRequest);

        txtDateApprove2 = findViewById(R.id.txtDateApprove2);
        txtApprover2 = findViewById(R.id.txtApprover2);
        txtComment2 = findViewById(R.id.txtComment2);


        txtstatusCair = findViewById(R.id.txtstatusCair);
        txtDateCair = findViewById(R.id.txtDateCair);
        txtCairer = findViewById(R.id.txtCairer);
        txtCairComment = findViewById(R.id.txtCairComment);
        txtTransfer = findViewById(R.id.txtTransfer);
        txtTransfer.setVisibility(View.GONE);

        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();

        loading = new SweetAlertDialog(DetailLoanActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        profile_image = findViewById(R.id.profile_image);
        profil_nama = findViewById(R.id.profil_nama);
        profil_role = findViewById(R.id.profil_role);
        profil_email = findViewById(R.id.profil_email);
        profil_phone = findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);
        txtPaymentRemaining = (TextView) findViewById(R.id.txtPaymentRemaining);

        txtCek = (TextView) findViewById(R.id.txtCek);
        txtCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailLoanActivity.this, DetailLoanPaymentActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(temp);
                intent.putExtra("detailLoan", myJson);
                startActivity(intent);
            }
        });


        if (dataLoan != null) {
            DetailLoan(dataLoan.getLoanID().intValue());
        }else {
            onNewIntent(getIntent());
        }

    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataLoan = new DataLoan();
                        dataLoan.setLoanID(Long.parseLong(String.valueOf(idx)));
                        DetailLoan(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /*public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApproveKlaim();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  ProsesUpdate();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }*/
    public void prosesRequest(View v) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataLoan.getLoanID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        ApproveLoanFragment dialogFragment = new ApproveLoanFragment();
        Bundle bundle = new Bundle();
        bundle.putString("isApprove", temp.getResultHeader().isIsApproved()== null? "N" : temp.getResultHeader().isIsApproved()== true ? "T" : "F");
        bundle.putString("loanStart", temp.getResultHeader().getLoanStartDate()!= null? "T" : "F");
        bundle.putString("amount", temp.getResultHeader().getAmount().toString());
        bundle.putString("installment", temp.getResultHeader().getInstallment().toString() + "x");
        bundle.putString("installmentpayout", temp.getResultHeader().getInstallmentAmount().toString());
        bundle.putString("ismingguan", temp.getResultHeader().isIsPbonMingguan() == true ? "T" : "F");
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");


    }

    public void prosesCair(View v) {

            PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).
                    edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataLoan.getLoanID().toString())
                    .apply();

            android.app.FragmentManager fm = getFragmentManager();
            CairLoanFragment dialogFragment = new CairLoanFragment();
            Bundle bundle = new Bundle();
            bundle.putString("amount", temp.getResultHeader().getAmount().toString());
            bundle.putString("id", String.valueOf(temp.getResultHeader().getLoanID()));
            bundle.putString("email", temp.getResultHeader().getEmployee().getEmail());
            bundle.putString("holder", temp.getResultHeader().getBankAccountName());
            bundle.putString("bank", temp.getResultHeader().getBankName());
            bundle.putString("norek", temp.getResultHeader().getBankNumber());
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");


    }

    public void prosesDelete(View v) {
        new android.app.AlertDialog.Builder(DetailLoanActivity.this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteData();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void cekSaldo(View v) {
        Intent intent = new Intent(DetailLoanActivity.this,DetailSaldoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    public void DeleteData() {

        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String idx = String.valueOf(id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.deletePinjaman(idx, Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.dismiss();

                            new SweetAlertDialog(DetailLoanActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getResources().getString(R.string.title_sukses))
                                    .setContentText(getResources().getString(R.string.title_berhasil_hapus_data))
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(DetailLoanActivity.this, LoanActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);

                                        }
                                    })
                                    .show();

                        }
                        else {
                            loading.dismiss();
                            try {
                            messageDialog.mShowMessageError(DetailLoanActivity.this,getResources().getString(R.string.title_gagal)
                                    , "");
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                            try {
                        messageDialog.mShowMessageError(DetailLoanActivity.this,getResources().getString(R.string.title_gagal)
                                , "");
                            }catch (Exception r){

                            }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.dismiss();
                            try {
                    messageDialog.mShowMessageError(DetailLoanActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
                        try {
            messageDialog.mShowMessageError(DetailLoanActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
        }

    }

    public void DetailLoan(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailLoanWithDetail> call = api.getLoansDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDetailLoanWithDetail>() {
                @Override
                public void onResponse(Call<ResponseDetailLoanWithDetail> call, Response<ResponseDetailLoanWithDetail> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_idProses_employee), String.valueOf(response.body().getData().getResultHeader().getEmployee().getEmployeeID()))
                                    .apply();
                            ShowData(response.body().getData());
                            temp = response.body().getData();


                        }
                        else {
                            loading.dismiss();
                            try {
                            messageDialog.mShowMessageError(DetailLoanActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                            try {
                        messageDialog.mShowMessageError(DetailLoanActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailLoanWithDetail> call, Throwable t) {
                    loading.dismiss();
                            try {
                    messageDialog.mShowMessageError(DetailLoanActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
                        try {
            messageDialog.mShowMessageError(DetailLoanActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
        }
    }

    private void ShowData(DetailLoanWithDetailData data){
        id = data.getResultHeader().getLoanID();
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        txtOrganisasi.setText(data.getResultHeader().getOrganization()!=null ? data.getResultHeader().getOrganization() : "");
        txtNamaPinjaman.setText(String.valueOf(data.getResultHeader().getName() != null ?data.getResultHeader().getName()  : ""));
        txtJumlah.setText(String.valueOf(data.getResultHeader().getAmount() != null ? getResources().getString(R.string.title_rp) +  " " + formatter.format(data.getResultHeader().getAmount()) : ""));
        txtInstallment.setText(String.valueOf(data.getResultHeader().getInstallment() != null ?data.getResultHeader().getInstallment() + "x" : ""));
        txtInstallmentPayOut.setText(String.valueOf(data.getResultHeader().getInstallmentPayOut() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(data.getResultHeader().getInstallmentPayOut()) : ""));
        txtInstallmentAmount.setText(String.valueOf(data.getResultHeader().getInstallmentAmount() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(data.getResultHeader().getInstallmentAmount()) : ""));
        txtPaid.setText(String.valueOf(data.getResultHeader().isIsPaid() == true ? getResources().getString(R.string.title_lunas) : getResources().getString(R.string.title_belum_lunas)));
        txtPaid.setBackgroundResource(data.getResultHeader().isIsPaid() == true ? R.drawable.rounded_green : R.drawable.rounded_orange);
        txtBon.setText(String.valueOf(data.getResultHeader().isIsPbonMingguan() == true ? getResources().getString(R.string.title_ya) : getResources().getString(R.string.title_tidak)));
        item_check.setChecked(data.getResultHeader().isIsPbonMingguan()== true ? true:false);

        if (data.getResultHeader().getInstallmentPayOut() != null && data.getResultHeader().getAmount() != null){
            txtPaymentRemaining.setText("Rp " +formatter.format(data.getResultHeader().getAmount() - data.getResultHeader().getInstallmentPayOut() ));
        }


        mTxtNote.setText(data.getResultHeader().getNote()!=null ? data.getResultHeader().getNote() : "");
        if (data.getResultHeader().getDate() != null)
            mTxtStart.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getResultHeader().getDate()));

        // status pending
        if (data.getResultHeader().isIsApproved() == null && data.getResultHeader().isIsApproved2() == null ||
                data.getResultHeader().isIsApproved() == true && data.getResultHeader().isIsApproved2() == null ){
            mIconStatus.setBackgroundResource(R.drawable.status_pending);

            if (data.getResultHeader().isIsApproved() != null){

                try {
                    trPotong.setVisibility(View.VISIBLE);
                    if (data.getResultHeader().getLoanStartDate() != null) {
                        txtPotong.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getResultHeader().getLoanStartDate()));
                    }
                    tbApprover.setVisibility(View.VISIBLE);
                    txtApprover.setText(data.getResultHeader().getApprover().getFirstName() + " " + data.getResultHeader().getApprover().getLastName());
                    txtComment.setText(data.getResultHeader().getComment());
                    if (data.getResultHeader().getDateApproved() != null)
                        txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getResultHeader().getDateApproved()));

                } catch (Exception e){

                }

            }
        // status 2 approved
        } else  if (data.getResultHeader().isIsApproved() == true && data.getResultHeader().isIsApproved2() == true){
            mIconStatus.setBackgroundResource(R.drawable.status_accept);


            try {
                trPotong.setVisibility(View.VISIBLE);
                if (data.getResultHeader().getLoanStartDate() != null) {
                    txtPotong.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getResultHeader().getLoanStartDate()));
                }
                tbApprover.setVisibility(View.VISIBLE);
                txtApprover.setText(data.getResultHeader().getApprover().getFirstName() + " " + data.getResultHeader().getApprover().getLastName());
                txtComment.setText(data.getResultHeader().getComment());
                if (data.getResultHeader().getDateApproved() != null)
                    txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getResultHeader().getDateApproved()));


                tbApprover2.setVisibility(View.VISIBLE);
                txtApprover2.setText(data.getResultHeader().getApprover2());
                txtComment2.setText(data.getResultHeader().getComment2());
                if (data.getResultHeader().getDateApproved2() != null)
                    txtDateApprove2.setText(DefaultFormatter.changeFormatDate(data.getResultHeader().getDateApproved2()));

            } catch (Exception e){

            }

            tbisCair.setVisibility(View.VISIBLE);


           if (data.getResultHeader().isIsCair()!=null) {
               if (data.getResultHeader().isIsCair()) {
                   tbDetailCair.setVisibility(View.VISIBLE);
                   tbCicilan.setVisibility(View.VISIBLE);

                   mIconStatusCair.setBackgroundResource(R.drawable.ic_cairtrue);
                   txtstatusCair.setText(getResources().getString(R.string.title_sudah_dicairkan));
                   txtstatusCair.setBackgroundResource(R.drawable.rounded_green);

                   txtCairComment.setText(data.getResultHeader().getMethodCair());
                   txtCairer.setText(data.getResultHeader().getUserCair());
                   if (data.getResultHeader().getDateCair() != null)
                       txtDateCair.setText(DefaultFormatter.changeFormatDate(data.getResultHeader().getDateCair()));

                   if (data.getResultHeader().getXendit() != null) {
                       if (data.getResultHeader().getXendit().size() > 0) {
                           txtTransfer.setVisibility(View.VISIBLE);
                           txtTransfer.setOnClickListener(new View.OnClickListener() {
                               @Override
                               public void onClick(View v) {

                                   if (data.getResultHeader().getKeyXendit() != null) {
                                       Intent intent = new Intent(DetailLoanActivity.this, TransferDetailActivity.class);
                                       intent.putExtra("detail", data.getResultHeader().getXendit().get(data.getResultHeader().getXendit().size() - 1).getId());
                                       startActivity(intent);
                                   } else {
                                       MDToast.makeText(DetailLoanActivity.this, getResources().getString(R.string.title_tidak_punya_token_xendit),
                                               MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                   }
                               }
                           });
                       }
                   }

               } else {
                   mIconStatusCair.setBackgroundResource(R.drawable.ic_cairfalse);
                   txtstatusCair.setText(getResources().getString(R.string.title_belum_dicairkan));
                   txtstatusCair.setBackgroundResource(R.drawable.rounded_orange);
               }
           }else{
               mIconStatusCair.setBackgroundResource(R.drawable.ic_cairfalse);
               txtstatusCair.setText(getResources().getString(R.string.title_belum_dicairkan));
               txtstatusCair.setBackgroundResource(R.drawable.rounded_orange);
           }

        }
        // reject
        else {
            mIconStatus.setBackgroundResource(R.drawable.status_reject);
            if (data.getResultHeader().isIsApproved() != null){

                try {
                    tbApprover.setVisibility(View.VISIBLE);
                    txtApprover.setText(data.getResultHeader().getApprover().getFirstName() + " " + data.getResultHeader().getApprover().getLastName());
                    txtComment.setText(data.getResultHeader().getComment());
                    if (data.getResultHeader().getDateApproved() != null)
                        txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getResultHeader().getDateApproved()));

                } catch (Exception e){

                }

            }
            if (data.getResultHeader().isIsApproved2() != null){

                try {
                    tbApprover2.setVisibility(View.VISIBLE);
                    txtApprover2.setText(data.getResultHeader().getApprover2());
                    txtComment2.setText(data.getResultHeader().getComment2());
                    if (data.getResultHeader().getDateApproved2() != null)
                        txtDateApprove2.setText(DefaultFormatter.changeFormatDate(data.getResultHeader().getDateApproved2()));

                } catch (Exception e){

                }

            }

        }

        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);

        if (data.getResultHeader().getEmployee() != null) {
            if (data.getResultHeader().getEmployee().getFirstName() != null) {
                profil_nama.setText(data.getResultHeader().getEmployee().getFirstName() + " " + data.getResultHeader().getEmployee().getLastName());
            }
            if (data.getResultHeader().getEmployee().getNik() != null) {
                profil_role.setText(data.getResultHeader().getEmployee().getNik());
            }
            if (data.getResultHeader().getEmployee().getEmail() != null) {
                profil_email.setText(data.getResultHeader().getEmployee().getEmail());
            }
            if (data.getResultHeader().getEmployee().getPhone() != null) {
                profil_phone.setText(data.getResultHeader().getEmployee().getPhone());
            }

            if (data.getResultHeader().getEmployee().getPhone().equals("") || data.getResultHeader().getEmployee().getPhone() == null) {

            } else{
                mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                    @Override
                    public void onClick(View v) {

                        final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                        CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(getResources().getString(R.string.title_aksi));
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // Check Permissions Now
                                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(intent);
                                    }
                                } else if (which == 1) {
                                    Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                    Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                    mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(mSendSms);
                                } else if (which == 2) {
                                    String name = data.getResultHeader().getEmployee().getFirstName() + " " + data.getResultHeader().getEmployee().getLastName();
                                    String phone = data.getResultHeader().getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                    contactIntent
                                            .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                            .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                    ((Activity) context).startActivityForResult(contactIntent, 1);

                                } else if (which == 3) {

                                    voiceCall(profil_phone.getText().toString().trim());
                                }
                            }
                        });
                        builder.show();

                    }
                });
            }

            if (data.getResultHeader().getEmployee().getPhotoUrl() != null) {

                Glide.with(DetailLoanActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getResultHeader().getEmployee().getPhotoUrl()))
                        .apply(requestOptions)
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);
                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getResultHeader().getEmployee().getPhotoUrl());
                        startActivity(intent);
                    }
                });

            }
            else {
                // profile_image.setVisibility(View.GONE);
            }

            String organisasi = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_namaOrganization), ""));

            String approval = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_CanApproveLoan), ""));
            String id = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_employeeid), ""));
            String idAspet = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_id), ""));

            String keyxendit = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_key_xendit), ""));

            String kasir = (PreferenceManager.getDefaultSharedPreferences(DetailLoanActivity.this).getString(
                    getResources().getString(R.string.pref_kasir), ""));

            if (!kasir.equals("true")) {
                if (approval.equals("true")) {

                    if (String.valueOf(data.getResultHeader().getEmployee().getEmployeeID()).equals(id)) {
                        prosesRequest.setVisibility(View.GONE);
                    } else {
                        if (CanApproveVoid) {
                            prosesRequest.setVisibility(View.VISIBLE);
                        } else {

                            if (data.getResultHeader().isIsApproved() == null && data.getResultHeader().isIsApproved2() == null ||
                                    data.getResultHeader().isIsApproved() == true && data.getResultHeader().isIsApproved2() == null) {

                                if (data.getResultHeader().isIsApproved()!= null ){

                                    if (data.getResultHeader().getApprover().getId().equals(idAspet)){
                                        prosesRequest.setVisibility(View.GONE);
                                    } else {
                                        prosesRequest.setVisibility(View.VISIBLE);
                                    }
                                } else{
                                    prosesRequest.setVisibility(View.VISIBLE);
                                }


                            } else {
                                prosesRequest.setVisibility(View.GONE);
                            }

                        }
                    }
                } else {
                    prosesRequest.setVisibility(View.GONE);
                }
            } else {
                prosesRequest.setVisibility(View.GONE);

                if (data.getResultHeader().isIsApproved() == null && data.getResultHeader().isIsApproved2() == null ||
                        data.getResultHeader().isIsApproved() == true && data.getResultHeader().isIsApproved2() == null ){

                }else if(data.getResultHeader().isIsApproved()== true && data.getResultHeader().isIsApproved2() == true){

                    if (data.getResultHeader().isIsCair()!=null) {
                        if (!data.getResultHeader().isIsCair()) {
                            prosesCair.setVisibility(View.VISIBLE);

                            if (!keyxendit.equals("")){
                                cekSaldo.setVisibility(View.VISIBLE);
                            }

                        } else {
                            if (data.getResultHeader().getXendit() != null) {
                                if (data.getResultHeader().getXendit().size() > 0) {

                                    if (data.getResultHeader().getXendit().get(data.getResultHeader().getXendit().size() - 1).getStatus() != null) {

                                        if (data.getResultHeader().getXendit().get(data.getResultHeader().getXendit().size() - 1).getStatus().equals("FAILED")) {
                                            prosesCair.setVisibility(View.VISIBLE);
                                            if (!keyxendit.equals("")){
                                                cekSaldo.setVisibility(View.VISIBLE);
                                            }
                                        }

                                    }


                                }
                            }
                        }
                    }
                }
            }

        }
    }


    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){

            MDToast.makeText(DetailLoanActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(DetailLoanActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailLoanActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}