package app.direksi.hras;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.ResponseCheckIn;
import app.direksi.hras.model.ResponseDataCompany;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 10/05/2019.
 */

public class CheckinActivity extends AppCompatActivity implements LocationAssistant.Listener, OnMapReadyCallback {
    private TextView errorText;
    LocationAssistant assistant;
    private int Mock = 0;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private String mAddress = "";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(CheckinActivity.this);
        setContentView(R.layout.activity_checkin);
        errorText = (TextView) findViewById(R.id.errorText);
        assistant = new LocationAssistant(this, this, LocationAssistant.Accuracy.HIGH, 1000, false);


        new CountDownTimer(2000, 1000) {
            public void onFinish() {
                // When timer is finished
                // Execute your code here

                if (Mock == 0 ) {
                    Intent intent = new Intent(CheckinActivity.this, GPSTrackerActivity.class);
                    startActivityForResult(intent,1);

                } else {
                    errorText.setText("Mock Detected");
                    gagal("Mock Detected");
                }
            }

            public void onTick(long millisUntilFinished) {
                // millisUntilFinished    The amount of time until finished.
            }
        }.start();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){

            try {
                Bundle extras = data.getExtras();
                Double longitude = extras.getDouble("Longitude");
                Double latitude = extras.getDouble("Latitude");
                //  errorText.setText("Latitude = " + latitude + " , Longitude = " + longitude);


                if (longitude.intValue() != 0 && latitude.intValue() != 0) {
                    errorText.setText(getResources().getString(R.string.title_mendapatkan_lokasi));

                    Location loc1 = new Location("");
                    loc1.setLatitude(latitude);
                    loc1.setLongitude(longitude);

                    try {

                        Double lat = Double.valueOf(loc1.getLatitude());
                        Double longi = Double.valueOf(loc1.getLongitude());

                        int height = SIZE_MARKER;
                        int width = SIZE_MARKER;
                        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.marker_user);
                        Bitmap b = bitmapdraw.getBitmap();
                        Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth() * width / 100, bitmapdraw.getBitmap().getHeight() * height / 100, false);

                        mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat, longi))
                                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                                .title("Lokasi Ku"));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                                .zoom(15)                   // Sets the zoom
                                .bearing(0)                // Sets the orientation of the camera to east
                                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                                .build();                   // Creates a CameraPosition from the builder
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    } catch (Exception e) {

                    }
                    // CheckLatLong(loc1);
                    CheckRadius(loc1);
                } else {

                    errorText.setText(getResources().getString(R.string.title_gagal));
                    SweetAlertDialog alertDialog = new SweetAlertDialog(CheckinActivity.this, SweetAlertDialog.ERROR_TYPE);
                    alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
                    alertDialog.setContentText(getResources().getString(R.string.title_gagal_mendapatkan_lokasi_only));
                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                       /* Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                        intent.setAction(Intent.ACTION_VIEW);
                        startActivity(intent);
                        finish();*/
                            Intent intent = new Intent(CheckinActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        }
                    });
                    alertDialog.show();

                    Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                    btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    btn.setTextColor(getResources().getColor(R.color.colorWhite));

                }
            } catch (Exception e){
                Intent intent = new Intent(CheckinActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }


        }
    }


    private void CheckRadius(Location locc){

        try {

            int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).getString(
                    getResources().getString(R.string.pref_radius), ""));

            String latt = (PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).getString(
                    getResources().getString(R.string.pref_latitude), ""));

            String longi = (PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).getString(
                    getResources().getString(R.string.pref_longitude), ""));

            LatLngBounds.Builder builder = new LatLngBounds.Builder();

         /*   CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(locc.getLatitude(), locc.getLongitude()))      // Sets the center of the map to location user
                    .zoom(18)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

            if ((latt.equals("") && longi.equals(""))) {

                new CountDownTimer(1000, 1000) {
                    public void onFinish() {
                        // When timer is finished
                        // Execute your code here
                        errorText.setText(getResources().getString(R.string.title_mencoba_absen));
                        CheckIn(locc);

                    }

                    public void onTick(long millisUntilFinished) {


                        // millisUntilFinished    The amount of time until finished.
                    }
                }.start();


            } else {

                try {
                    int height = SIZE_MARKER;
                    int width = SIZE_MARKER;
                    BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_kantor);
                    Bitmap b=bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()* width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);

             /*   mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(latt), Double.parseDouble(longi)))
                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                        .title("Lokasi kantor"));*/

                    MarkerOptions marker = new MarkerOptions()  .position(new LatLng(Double.parseDouble(latt), Double.parseDouble(longi)))
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                            .title(getResources().getString(R.string.title_lokasi_kantor));
                    mMap.addMarker(marker);
                    builder.include(marker.getPosition());
                }
                catch (Exception e){

                }

                try {
                    int height = SIZE_MARKER;
                    int width = SIZE_MARKER;
                    BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
                    Bitmap b=bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);

                    MarkerOptions marke = new MarkerOptions()   .position(new LatLng(locc.getLatitude(), locc.getLongitude()))
                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                            .title(getResources().getString(R.string.title_lokasi_ku));
                    mMap.addMarker(marke);
                    builder.include(marke.getPosition());
                }
                catch (Exception e){

                }

                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(CheckinActivity.this, Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(locc.getLatitude(), locc.getLongitude(), 1);
                    mAddress = addresses.get(0).getAddressLine(0);

                } catch (IOException e) {
                    e.printStackTrace();
                }




                int swidth = getResources().getDisplayMetrics().widthPixels;
                int sheight = getResources().getDisplayMetrics().heightPixels;
                int spadding = (int) (swidth * 0.10);


                Circle circle = mMap.addCircle(new CircleOptions()
                        .center(new LatLng(Double.parseDouble(latt), Double.parseDouble(longi)))
                        .radius(radius)
                        .strokeColor(Color.parseColor("#20ff5252"))
                        .strokeWidth(3)
                        .fillColor(Color.parseColor("#50ff5252")));


                Location loc2 = new Location("");
                loc2.setLatitude(Double.parseDouble(latt));
                loc2.setLongitude(Double.parseDouble(longi));
                float distance = locc.distanceTo(loc2);


                if (distance <= radius) {


                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(locc.getLatitude(), locc.getLongitude()))      // Sets the center of the map to location user
                            .zoom(18)                   // Sets the zoom
                            .bearing(0)                // Sets the orientation of the camera to east
                            .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                            .build();                   // Creates a CameraPosition from the builder
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                    new CountDownTimer(1000, 1000) {
                        public void onFinish() {
                            // When timer is finished
                            // Execute your code here
                            errorText.setText(getResources().getString(R.string.title_mencoba_absen));
                            CheckIn(locc);

                        }

                        public void onTick(long millisUntilFinished) {


                        }
                    }.start();


                } else {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), swidth, sheight, spadding));
                    String mTextDistance = "";

                    int jarak = Math.round(distance) - radius;

                    if (distance < 1000) {
                        mTextDistance = DefaultFormatter.formatDefaultCurrencyRupiah(Math.round(jarak)) + " m ";
                    } else {
                        mTextDistance = DefaultFormatter.formatDefaultCurrencyRupiah(Math.round((jarak / 1000))) + " km ";
                    }

                    errorText.setText(getResources().getString(R.string.title_lokasi_diluar_area_absen));
                    SweetAlertDialog alertDialog = new SweetAlertDialog(CheckinActivity.this, SweetAlertDialog.ERROR_TYPE);
                    alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
                    alertDialog.setContentText( getResources().getString(R.string.title_jarak_telalu_jauh_dari_kantor)  + " " +
                            getResources().getString(R.string.title_saat_ini_anda_berjarak) +  " " + mTextDistance + " " + getResources().getString(R.string.title_dari_kantor));
                    alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                            intent.setAction(Intent.ACTION_VIEW);
                            startActivity(intent);
                            finish();

                        }
                    });
                    alertDialog.show();

                    Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                    btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    btn.setTextColor(getResources().getColor(R.color.colorWhite));
                }


            }
        } catch (Exception e){

            gagal("");

        }



    }



    private void CheckIn(Location locc) {


        try {

            RequestBody Address = RequestBody.create(MediaType.parse("text/plain"), mAddress);
            RequestBody Latitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(locc.getLatitude()));
            RequestBody Longitude = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(locc.getLongitude()));
            // PostCheckIN up = new PostCheckIN( String.valueOf(locc.getLatitude()), String.valueOf(locc.getLongitude()));
            RequestBody InOut = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                    getResources().getString(R.string.pref_in_out), ""));
            RequestBody GMT = RequestBody.create(MediaType.parse("text/plain"), PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                    getResources().getString(R.string.pref_gmt), "0"));
            RequestBody IsYesterday = RequestBody.create(MediaType.parse("text/plain"),
                    Boolean.toString(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(
                            getResources().getString(R.string.pref_out_date), false)));
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            // simplified call to request the news with already initialized service
            Call<ResponseCheckIn> call;

            if (!PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getBoolean(
                    getResources().getString(R.string.pref_out_date), false))
                call = api.mPostAttendancesInOutWorkingPatternBiometric(Authorization, Longitude, Latitude, InOut, Address, GMT, null);
            else
                call = api.mPostAttendancesInOutDateWorkingPatternBiometric(Authorization, Longitude, Latitude, InOut, Address, IsYesterday, GMT, null);


            call.enqueue(new Callback<ResponseCheckIn>() {
                @Override
                public void onResponse(Call<ResponseCheckIn> call, Response<ResponseCheckIn> response) {

                    if (response.isSuccessful()) {

                        if (response.body().getData().getStatus() == null) {

                            DecimalFormat formatter = new DecimalFormat("#,###,###");
                            SweetAlertDialog alertDialog = new SweetAlertDialog(CheckinActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            // String mes = "ID : " + response.body().getData().getAttendanceID().toString() + "\n" + "Waktu : " + DefaultFormatter.changeFormatDateWithDayNameWithSecond(response.body().getData().getDateTime());
                            alertDialog.setContentText( getResources().getString(R.string.title_absen_berhasil_pada) + " " + DefaultFormatter.changeFormatDateWithDayName(response.body().getData().getDateTime()) + " " + getResources().getString(R.string.title_dengan_id)  + " "+ formatter.format(response.body().getData().getAttendanceID()));
                            alertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    redirected();
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));

                        } else {

                            errorText.setText(getResources().getString(R.string.title_gagal));
                            gagal(response.body().getData().getResult());

                        }

                    } else if (response.code() == 201) {

                    } else if (response.code() == 400) {
                        errorText.setText(getResources().getString(R.string.title_gagal));
                        gagal("Bad Request");
                    } else if (response.code() == 404) {
                        errorText.setText(getResources().getString(R.string.title_gagal));
                        gagal("Jam kerja tidak ada");
                    } else if (response.code() == 401) {
                        errorText.setText(getResources().getString(R.string.title_gagal));
                        gagal("Unauthorized");
                    } else if (response.code() == 415) {
                        errorText.setText(getResources().getString(R.string.title_gagal));
                        gagal("Unsupported Media Type");
                    } else if (response.code() == 500) {
                        errorText.setText(getResources().getString(R.string.title_gagal));
                        gagal("Internal Server Error");
                    } else {
                        errorText.setText(getResources().getString(R.string.title_gagal));
                        gagal("");
                    }
                }

                @Override
                public void onFailure(Call<ResponseCheckIn> call, Throwable t) {

                    String a = t.toString();

                    gagal("");
                    errorText.setText("failure");

                }
            });

        } catch (Exception e) {
            gagal("");
            MDToast.makeText(CheckinActivity.this, e.toString(),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            // Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            errorText.setText("exception");


        }


    }

    private void CheckLatLong(Location locc) {


        try {



            String Authorization = (PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataCompany> call = api.getCompany(Authorization );


            call.enqueue(new Callback<ResponseDataCompany>() {
                @Override
                public void onResponse(Call<ResponseDataCompany> call, Response<ResponseDataCompany> response) {



                    if (response.isSuccessful()) {

                        int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).getString(
                                getResources().getString(R.string.pref_radius), ""));


                        for ( int i = 0; i < response.body().getData().size(); i++) {
                            Location loc2 = new Location("");
                            loc2.setLatitude(Double.parseDouble(response.body().getData().get(i).getLatitude()));
                            loc2.setLongitude(Double.parseDouble(response.body().getData().get(i).getLongitude()));
                            float distance = locc.distanceTo(loc2);


                            try {

                                Double lat = Double.valueOf(loc2.getLatitude());
                                Double longi = Double.valueOf(loc2.getLongitude());
                                mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(lat, longi))
                                        .title(response.body().getData().get(i).getName()));
                                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
                                CameraPosition cameraPosition = new CameraPosition.Builder()
                                        .target(new LatLng(locc.getLatitude(), locc.getLongitude()))      // Sets the center of the map to location user
                                        .zoom(17)                   // Sets the zoom
                                        .bearing(0)                // Sets the orientation of the camera to east
                                        .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                                        .build();                   // Creates a CameraPosition from the builder
                                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


                                Circle circle = mMap.addCircle(new CircleOptions()
                                        .center(new LatLng(lat, longi))
                                        .radius(radius)
                                        .strokeColor(Color.parseColor("#27F57F17"))
                                        .strokeWidth(3)
                                        .fillColor(Color.parseColor("#0FF57F17")));

                            } catch (Exception e){

                            }


                            if (distance <= radius) {


                                new CountDownTimer(1000, 1000) {
                                    public void onFinish() {
                                        // When timer is finished
                                        // Execute your code here
                                        errorText.setText(getResources().getString(R.string.title_mencoba_absen));
                                        CheckIn(locc);

                                    }

                                    public void onTick(long millisUntilFinished) {


                                        // millisUntilFinished    The amount of time until finished.
                                    }
                                }.start();

                                /* CheckIn(locc);*/
                                break;

                            } else {
                                if ( i + 1 == response.body().getData().size()) {
                                    errorText.setText(getResources().getString(R.string.title_lokasi_diluar_area_absen));
                                    SweetAlertDialog alertDialog = new SweetAlertDialog(CheckinActivity.this,SweetAlertDialog.ERROR_TYPE);
                                    alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
                                    alertDialog.setContentText(getResources().getString(R.string.title_lokasi_diluar_area_absen));
                                    alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {




                                            Intent intent = getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                                            intent.setAction(Intent.ACTION_VIEW);
                                            startActivity(intent);
                                            finish();

                                        }
                                    });
                                    alertDialog.show();

                                    Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                    btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                    btn.setTextColor(getResources().getColor(R.color.colorWhite));


                                }
                            }
                        }

                    } else {


                    }
                    if (response.code() == 400) {
                        gagal(response.message());
                        errorText.setText(response.message());

                    }
                    if (response.code() == 401) {
                        gagal(response.message());
                        errorText.setText(response.message());

                    }
                    if (response.code() == 415) {
                        gagal(response.message());
                        errorText.setText(response.message());

                    }
                    if (response.code() == 500) {
                        gagal(response.message());
                        errorText.setText(response.message());

                    }
                }


                @Override
                public void onFailure(Call<ResponseDataCompany> call, Throwable t) {
                    gagal("");
                    String a = t.toString();


                    errorText.setText("failure");

                }
            });

        } catch (Exception e) {
            gagal("");
            MDToast.makeText(CheckinActivity.this, e.toString(),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
            //  Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            errorText.setText("exception");


        }


    }

    private void gagal(String message){

        SweetAlertDialog alertDialog = new SweetAlertDialog(CheckinActivity.this,SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
        alertDialog.setContentText(message);
        alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                redirected ();
            }
        });
        alertDialog.show();

        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btn.setTextColor(getResources().getColor(R.color.colorWhite));

    }


    @Override
    protected void onResume() {
        super.onResume();
        MyContextWrapper.refreshBahasa(CheckinActivity.this);
        assistant.start();
    }

    @Override
    protected void onPause() {
        assistant.stop();
        super.onPause();
    }

    @Override
    public void onExplainLocationPermission() {
        Log.v("Maps", "onExplainLocationPermission");
     /*   new AlertDialog.Builder(this)
                .setMessage("Permission")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        assistant.requestLocationPermission();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       *//* tvLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                assistant.requestLocationPermission();
                            }
                        });*//*
                    }
                })
                .show();*/
    }

    @Override
    public void onNeedLocationPermission() {
        Log.v("Maps", "onNeedLocationPermission");
        /* tvLocation.setText("Need\nPermission");*/
      /*  tvLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assistant.requestLocationPermission();
            }
        });*/
        assistant.requestAndPossiblyExplainLocationPermission();
    }

    @Override
    public void onNeedLocationSettingsChange() {
        Log.v("Maps", "onNeedLocationSettingsChange");
      /*  new AlertDialog.Builder(this)
                .setMessage("SWITCH")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        assistant.changeLocationSettings();
                    }
                })
                .show();*/
    }

    @Override
    public void onFallBackToSystemSettings(View.OnClickListener fromView, DialogInterface.OnClickListener fromDialog) {
        Log.v("Maps", "onFallBackToSystemSettings");
     /*   new AlertDialog.Builder(this)
                .setMessage("SWITCH")
                .setPositiveButton("OK", fromDialog)
                .show();*/
    }

    @Override
    public void onNewLocationAvailable(Location location) {
        Log.v("Maps", "onNewLocationAvailable");
        if (location == null) return;
     /*   tvLocation.setOnClickListener(null);
        tvLocation.setText(location.getLongitude() + "\n" + location.getLatitude());
        tvLocation.setAlpha(1.0f);
        tvLocation.animate().alpha(0.5f).setDuration(400);*/
    }

    @Override
    public void onMockLocationsDetected(View.OnClickListener fromView, DialogInterface.OnClickListener fromDialog) {
        Mock = 1;
    }

    @Override
    public void onError(LocationAssistant.ErrorType type, String message) {
        Log.v("Maps", "onError");
        /* tvLocation.setText(getString(R.string.error));*/
    }

    @Override
    public void onLocationPermissionPermanentlyDeclined(View.OnClickListener fromView,
                                                        DialogInterface.OnClickListener fromDialog) {
        Log.v("Maps", "onLocationPermissionPermanentlyDeclined");
     /*   new AlertDialog.Builder(this)
                .setMessage("DECLINE")
                .setPositiveButton("OK", fromDialog)
                .show();*/
    }

    void redirected (){

        PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).
                edit()
                .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                .apply();

        Intent login = new Intent(CheckinActivity.this, AttendanceActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
        startActivity(login);
    }

  /*  @Override
    public void onBackPressed() {
        super.onBackPressed();

      *//*  Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();*//*


     *//*  PreferenceManager.getDefaultSharedPreferences(CheckinActivity.this).
                edit()
                .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                .apply();

        Intent login = new Intent(CheckinActivity.this, AttendanceActivity.class);
        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(login);*//*


    }*/

    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try{
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_json_maps));

            if (!isSuccess)
            {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        }
        catch (Resources.NotFoundException ex)
        {
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        else
        {
            mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);
        }

      /*  Double lat = Double.valueOf(DataAttendance.getLatitude().replace(",",""));
        Double longi = Double.valueOf(DataAttendance.getLongitude().replace(",",""));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, longi))
                .title("Lokasi Check in"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                .zoom(15)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

        LatLng currentLatLng = new LatLng(-7.265465,
                112.745543);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(currentLatLng,
                3);
        mMap.moveCamera(update);
    }


    private void getAddress(Context context, double LATITUDE, double LONGITUDE) {


        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {



                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL




            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }



}
