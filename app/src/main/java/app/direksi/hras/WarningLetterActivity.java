package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.ImageView;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.ListTeguranFragment;
import app.direksi.hras.fragment.MyTeguranFragment;
import app.direksi.hras.model.DataKategori;
import app.direksi.hras.model.ResponseKategori;
import app.direksi.hras.fragment.FilterTeguranFragment;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class WarningLetterActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;
    SubMenu subMenu;
    private ImageView mProfileImage;
    private int mMaxScrollSize;


    public FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private FragmentRefreshListener1 fragmentRefreshListener1;




    public FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private FragmentRefreshListener2 fragmentRefreshListener2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(WarningLetterActivity.this);
        setContentView(R.layout.activity_asset_new);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
       // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.title_letter));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);

        PreferenceManager.getDefaultSharedPreferences(WarningLetterActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_status), "")
                .apply();



        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        viewPager.setAdapter(new WarningLetterActivity.TabsAdapter(getSupportFragmentManager(), WarningLetterActivity.this));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                //  Toast.makeText(CutiActivity.this, String.valueOf(position), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }



    public static void start(Context c) {
        c.startActivity(new Intent(c, CutiActivity.class));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
    }

    private static class TabsAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 2;
        private Context context;

        TabsAdapter(FragmentManager fm, Context cont) {
            super(fm);
            context = cont;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int i) {

            final Fragment result;
            switch (i) {
                case 0:
                    // First Tab
                    result = ListTeguranFragment.newInstance();
                    break;
                case 1:
                    // Second Tab
                    result = MyTeguranFragment.newInstance();
                    break;

                default:
                    result = ListTeguranFragment.newInstance();
                    break;
            }
            return result;



        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title ="";
            if (position == 0){
                title = context.getResources().getString(R.string.title_terima);
            }
            if (position == 1){
                title = context.getResources().getString(R.string.title_buat);
            }
            return title;
        }
    }







    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {


        getMenuInflater().inflate(R.menu.menu_reprimand, menu);

        MenuItem menuItem = menu.findItem(R.id.menubutton_submenuButton);
        subMenu = menuItem.getSubMenu();

        mGetConstanta();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterTeguranFragment dialogFragment = new FilterTeguranFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;
            case R.id.menubutton_submenuButton:


                for (int i = 0; i < item.getSubMenu().size(); i++) {
                    item.getSubMenu().getItem(i).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {

                            String mTextSearch = "";

                            if (!item.getTitle().toString().contains("Semua"))
                                mTextSearch = item.getTitle().toString();

                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_status), mTextSearch)
                                    .apply();

                            if(getFragmentRefreshListener1()!=null){
                                getFragmentRefreshListener1().onRefresh();
                            }
                            if(getFragmentRefreshListener2()!=null){
                                getFragmentRefreshListener2().onRefresh();
                            }

                            return false;
                        }
                    });


                }
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        // Toast.makeText(getBaseContext(), ""+ selectedValue, Toast.LENGTH_LONG).show();
        if(getFragmentRefreshListener1()!=null){
            getFragmentRefreshListener1().onRefresh();
        }
        if(getFragmentRefreshListener2()!=null){
            getFragmentRefreshListener2().onRefresh();
        }
    }


    public void mGetConstanta() {


        String Authorization = (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                getString(getResources().getString(R.string.pref_token), ""));




        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

        // simplified call to request the news with already initialized service
        Call<ResponseKategori> call = api.getCategory(Authorization);

        call.enqueue(new Callback<ResponseKategori>() {
            @Override
            public void onResponse(Call<ResponseKategori> call, Response<ResponseKategori> response) {


                if (response.isSuccessful()) {
                    Long statusCode = response.body().getErrCode();

                    if (statusCode == 0) {


                        subMenu.add(getResources().getString(R.string.title_semua_kategori));

                        for (DataKategori dataCategoryReprimandsItem : response.body().getData()) {
                            subMenu.add(dataCategoryReprimandsItem.getName());
                        }


                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseKategori> call, Throwable t) {
            }
        });

    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(WarningLetterActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}
