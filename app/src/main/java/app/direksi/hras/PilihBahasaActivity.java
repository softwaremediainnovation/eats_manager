package app.direksi.hras;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Locale;

import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.app.AppConstants.LogoutPermission;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class PilihBahasaActivity extends AppCompatActivity {

    private ImageView img_indonesia, img_english;
    private RelativeLayout rlIndonesia, rlEnglish;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(PilihBahasaActivity.this);
        setContentView(R.layout.activity_pilih_bahasa);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
        //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.title_pilih_bahasa));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        rlIndonesia = (RelativeLayout) findViewById(R.id.rlIndonesia);
        rlEnglish = (RelativeLayout) findViewById(R.id.rlEnglish);

        img_indonesia = (ImageView) findViewById(R.id.img_indonesia);
        img_indonesia.setVisibility(View.GONE);
        img_english = (ImageView) findViewById(R.id.img_english);
        img_english.setVisibility(View.GONE);

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(PilihBahasaActivity.this).getString(
                getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("in")){

            img_indonesia.setVisibility(View.VISIBLE);

        } else if (bahasa.equals("en")){

            img_english.setVisibility(View.VISIBLE);

        } else {

        }



        rlIndonesia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bahasa.equals("in")){


                }  else {

                    ShowDialogUpdate("in");
                }

            }
        });

        rlEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bahasa.equals("en")){


                }  else {

                    ShowDialogUpdate("en");
                }

            }
        });

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }


    private void ShowDialogUpdate( String bahasa){

        new android.app.AlertDialog.Builder(PilihBahasaActivity.this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       Update(bahasa);
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();
    }

    private void Update(String bahasa){


        try {

            MyContextWrapper.UpdateLanguage(PilihBahasaActivity.this, bahasa);

            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                    edit().putString(getResources().getString(R.string.pref_bahasa), bahasa)
                    .apply();


            Intent intent = new Intent(getApplicationContext(), SplashScreenActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        } catch (Exception e){

        }

    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(PilihBahasaActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}
