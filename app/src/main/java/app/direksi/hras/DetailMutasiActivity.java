package app.direksi.hras;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.DataMembership;
import app.direksi.hras.model.ResponseDataMembership;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class DetailMutasiActivity extends AppCompatActivity {

    DataMembership dataMembership;
    SweetAlertDialog loading;
    private ScrollView scrollView;
    private MessageDialog messageDialog;
    private TextView txtOldOrganization, txtOldCompany, txtNewOrganization, txtNewCompany, txtCreator, txtDate, txtNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailMutasiActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_mutasi));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_mutasi);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        Gson gson = new Gson();
        dataMembership = gson.fromJson(getIntent().getStringExtra("detail"), DataMembership.class);

        txtOldOrganization = findViewById(R.id.txtOldOrganization);
        txtOldCompany = findViewById(R.id.txtOldCompany);
        txtNewOrganization = findViewById(R.id.txtNewOrganization);
        txtNewCompany = findViewById(R.id.txtNewCompany);
        txtCreator = findViewById(R.id.txtCreator);
        txtDate = findViewById(R.id.txtDate);
        txtNote = findViewById(R.id.txtNote);


        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();

        loading = new SweetAlertDialog(DetailMutasiActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        if (dataMembership != null) {
            DetailMembership(dataMembership.getMembershipHistoryID());
        }else {
           // onNewIntent(getIntent());
        }


    }

    public void DetailMembership(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailMutasiActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataMembership> call = api.getMembershipDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDataMembership>() {
                @Override
                public void onResponse(Call<ResponseDataMembership> call, Response<ResponseDataMembership> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);

                            if (response.body().getData().size()> 0){

                                ShowData(response.body().getData());

                            } else {
                                
                                try {
                                    messageDialog.mShowMessageError(DetailMutasiActivity.this,getResources().getString(R.string.title_gagal)
                                            , getResources().getString(R.string.loading_error));
                                }catch (Exception r){

                                }
                            }




                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailMutasiActivity.this,getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(DetailMutasiActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataMembership> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(DetailMutasiActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }catch (Exception r){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(DetailMutasiActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            }catch (Exception r){

            }
        }
    }

    private void ShowData(List<DataMembership> data){

        txtOldOrganization.setText(data.get(0).getOrganizationOld());
        txtOldCompany.setText(data.get(0).getCompanyOld());
        txtNewOrganization.setText(data.get(0).getOrganizationNew());
        txtNewCompany.setText(data.get(0).getCompanyNew());
        txtCreator.setText(data.get(0).getCreator());
        txtNote.setText(data.get(0).getNote());

        if (data.get(0).getDateCreated() != null)
            txtDate.setText(DefaultFormatter.changeFormatDate(data.get(0).getDateCreated()));

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailMutasiActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}
