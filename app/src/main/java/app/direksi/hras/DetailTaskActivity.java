package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.DetailTaskAdapter;
import app.direksi.hras.model.DataTask;
import app.direksi.hras.model.DataTaskIDDetail;
import app.direksi.hras.model.DataTaskIDDetailItem;
import app.direksi.hras.model.ResponseDataTaskIDDetail;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class DetailTaskActivity extends AppCompatActivity {


    DataTask dataTask;

    public TextView txtTanggal, txtCreator, txtCatatan,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtOrganisasi,
            txtTanggalKadaluarsa,
            mTxtNote,
            mTxtStatus,
            txtSum;
    private ImageView profile_image;
    private ImageView mIconStatus;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;

    private int id;
    private Button prosesDelete;
    private TableRow trExpiredDate;

    private static final int REQUEST_PHONE_CALL = 1;
    RecyclerView recyclerView;
    private DetailTaskAdapter cAdapter;
    private List<DataTaskIDDetailItem> dataList = new ArrayList<>();

    private ProgressBar progressBar;
    private TextView txtProgress;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailTaskActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.task_detail));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_task);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        Gson gson = new Gson();
        dataTask = gson.fromJson(getIntent().getStringExtra("detail"), DataTask.class);
        //dataCutiItem = getIntent().getParcelableExtra("detail");
        trExpiredDate = findViewById(R.id.trExpiredDate);
        trExpiredDate.setVisibility(View.GONE);
        txtTanggalKadaluarsa = findViewById(R.id.txtTanggalKadaluarsa);
        mTxtNote = findViewById(R.id.mTxtNote);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        txtTanggal = findViewById(R.id.txtTanggal);
        txtCreator = findViewById(R.id.txtCreator);
        txtCatatan = findViewById(R.id.txtCatatan);
        mImageView = findViewById(R.id.mImageView);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();
        prosesDelete = findViewById(R.id.prosesDelete);
        prosesDelete.setVisibility(View.GONE);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        loading = new SweetAlertDialog(DetailTaskActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);

        progressBar = findViewById(R.id.progress);
        txtProgress = findViewById(R.id.txtProgress);
        progressBar.setProgress(0); //Set Progress Dimulai Dari O


        recyclerView = findViewById(R.id.recyclerView);
        txtSum = findViewById(R.id.txtSum);


        cAdapter = new DetailTaskAdapter(dataList, DetailTaskActivity.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DetailTaskActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(DetailTaskActivity.this, DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(DetailTaskActivity.this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(DetailTaskActivity.this, R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(cAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(DetailTaskActivity.this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {


                DataTaskIDDetailItem data = dataList.get(position);

                if (data.isIsFinish() == true){

                    Intent mIntent = new Intent(DetailTaskActivity.this, DetailTaskDetailActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);

                }
                else {
                    MDToast.makeText(DetailTaskActivity.this, getResources().getString(R.string.title_item_belum_selesai),
                            MDToast.LENGTH_LONG, MDToast.TYPE_INFO).show();
                }



            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prosesDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new android.app.AlertDialog.Builder(DetailTaskActivity.this)
                        .setTitle(getResources().getString(R.string.string_confirmation))
                        .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                        .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                DeleteData();
                            }

                        })
                        .setNegativeButton(getResources().getString(R.string.txt_no), null)
                        .show();

            }
        });

        if (dataTask != null) {
            DetailTask(dataTask.getTaskHeaderID());
        } else {
            onNewIntent(getIntent());
        }





    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataTask = new DataTask();
                        dataTask.setTaskHeaderID(Integer.parseInt(String.valueOf(idx)));
                        DetailTask(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailTaskActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public void DetailTask(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTaskActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataTaskIDDetail> call = api.getTaskDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDataTaskIDDetail>() {
                @Override
                public void onResponse(Call<ResponseDataTaskIDDetail> call, Response<ResponseDataTaskIDDetail> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            ShowData(response.body().getData());



                        }
                        else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(DetailTaskActivity.this,getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.dismiss();
                        messageDialog.mShowMessageError(DetailTaskActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataTaskIDDetail> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(DetailTaskActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(DetailTaskActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }
    }


    public void DeleteData() {

        loading.show();

        try {




            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTaskActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String idx = String.valueOf(id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.deleteTask(idx, Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(DetailTaskActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.dismiss();



                            SweetAlertDialog alertDialog = new SweetAlertDialog(DetailTaskActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {


                                    Intent login = new Intent(DetailTaskActivity.this, TaskActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));


                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailTaskActivity.this,getResources().getString(R.string.title_gagal)
                                        , "");
                            } catch (Exception e){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(DetailTaskActivity.this,getResources().getString(R.string.title_gagal)
                                    , "");
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(DetailTaskActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(DetailTaskActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            } catch (Exception d){

            }
        }

    }



    private void ShowData(List<DataTaskIDDetail> data){
        id = data.get(0).getHeader().getTaskHeaderID();
        if (data.get(0).getHeader().getDateCreated() != null)
            txtTanggal.setText(DefaultFormatter.changeFormatDate(data.get(0).getHeader().getDateCreated()));
        if (!data.get(0).getHeader().getDueDate().contains("0001")) {
            txtTanggalKadaluarsa.setText(DefaultFormatter.changeFormatDateWithOutHour(data.get(0).getHeader().getDueDate()));
            trExpiredDate.setVisibility(View.VISIBLE);
        }

       // txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization() : "");
        txtCreator.setText(data.get(0).getCreator().get(0).getFirstName() + " " + data.get(0).getCreator().get(0).getLastName());
        txtCatatan.setText(data.get(0).getHeader().getTaskName());
        mTxtNote.setText(data.get(0).getHeader().getNote());



        if (!data.get(0).getIsExpired()) {
            if (data.get(0).getHeader().isIsFinish()) {

                mTxtStatus.setText(getResources().getString(R.string.title_selesai));
                mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                mTxtStatus.setTextColor(ContextCompat.getColor(DetailTaskActivity.this, R.color.colorWhite));

            } else {

                mTxtStatus.setText(getResources().getString(R.string.title_belum_selesai));
                mTxtStatus.setBackgroundResource(R.drawable.rounded_orange);
                mTxtStatus.setTextColor(ContextCompat.getColor(DetailTaskActivity.this, R.color.colorWhite));

            }
        } else {

            mTxtStatus.setText(getResources().getString(R.string.title_kadaluarsa));
            mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
            mTxtStatus.setTextColor(ContextCompat.getColor(DetailTaskActivity.this, R.color.colorWhite));
        }

        int temp = 0;
        for ( int i = 0; i < data.get(0).getDetail().size(); i ++){

            if (data.get(0).getDetail().get(i).isIsFinish() == true){
                temp = temp + 1;
            }
        }
        txtSum.setText(String.valueOf(temp)+ "/" + String.valueOf(data.get(0).getDetail().size()));

        float percentTemp =  temp * 100 / data.get(0).getDetail().size();

        txtProgress.setText( math(percentTemp) + "%");
        progressBar.setProgress(math(percentTemp));

        String idEmployee = (PreferenceManager.getDefaultSharedPreferences(DetailTaskActivity.this).getString(
                getResources().getString(R.string.pref_employeeid), ""));


        if (temp == 0 & data.get(0).getHeader().getCreatorID() == Integer.valueOf(idEmployee) ){
            prosesDelete.setVisibility(View.VISIBLE);
        }


        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);


        profil_nama.setText(data.get(0).getEmployees().get(0).getFirstName()!= null? data.get(0).getEmployees().get(0).getFirstName() + " " + data.get(0).getEmployees().get(0).getLastName() : " ");
        profil_role.setText(data.get(0).getEmployees().get(0).getNik() !=null ? data.get(0).getEmployees().get(0).getNik() : " ");
        profil_email.setText(data.get(0).getEmployees().get(0).getEmail() != null? data.get(0).getEmployees().get(0).getEmail() : " ");
        profil_phone.setText(data.get(0).getEmployees().get(0).getPhone()!=null? data.get(0).getEmployees().get(0).getPhone() : " ");
        txtOrganisasi.setText(data.get(0).getOrganizations()!=null? data.get(0).getOrganizations() : " ");

        if (data.get(0).getEmployees().get(0).getPhone().equals("") || data.get(0).getEmployees().get(0).getPhone() == null) {

        } else{
            mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                @Override
                public void onClick(View v) {
                    final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                    CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(getResources().getString(R.string.title_aksi));
                    builder.setItems(colors, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    // Check Permissions Now
                                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                } else {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(intent);
                                }
                            } else if (which == 1) {
                                Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.getApplicationContext().startActivity(mSendSms);
                            } else if (which == 2) {
                                String name = data.get(0).getEmployees().get(0).getFirstName() + " "+ data.get(0).getEmployees().get(0).getLastName();
                                String phone = data.get(0).getEmployees().get(0).getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                contactIntent
                                        .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                        .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                ((Activity) context).startActivityForResult(contactIntent, 1);

                            } else if (which == 3) {

                                voiceCall(profil_phone.getText().toString().trim());
                            }
                        }
                    });
                    builder.show();
                }
            });
        }

        if (data.get(0).getEmployees().get(0).getPhotoUrl() != null) {

            Glide.with(DetailTaskActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.get(0).getEmployees().get(0).getPhotoUrl()))
                    .apply(requestOptions)
                    .error(Glide.with(profile_image).load(R.drawable.profile))
                    .into(profile_image);
            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.get(0).getEmployees().get(0).getPhotoUrl());
                    startActivity(intent);
                }
            });

        }
        else {
            // profile_image.setVisibility(View.GONE);
        }












        dataList.addAll(data.get(0).getDetail());
        cAdapter.notifyDataSetChanged();







    }

    public static int math(float f) {
        int c = (int) ((f) + 0.5f);
        float n = f + 0.5f;
        return (n - c) % 2 == 0 ? (int) f : c;
    }


    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){

            MDToast.makeText(DetailTaskActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(DetailCutiActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailTaskActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}

