package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.CairClaimFragment;
import app.direksi.hras.model.DataAktivitas;
import app.direksi.hras.model.ResponseDetailClaim;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.fragment.ApproveKlaimFragment;
import app.direksi.hras.model.DataClaim;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.CanApproveVoid;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 26/02/2019.
 */

public class DetailClaimActivity  extends AppCompatActivity {

    DataClaim dataClaim;
    DataClaim temp;
    TableLayout tbApprover, tbisCair, tbDetailCair;
    public TextView mTxtStart,
            txtJumlah,
            mTxtStatus,
            mTxtNote,
            txtApprover,
            txtComment,
            txtKategori,
            txtDateApprove,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtCek,
            txtOrganisasi,
            txtstatusCair,
            txtDateCair,
            txtCairer,
            txtCairComment,
            txtTransfer;
    private ImageView mIconStatus, profile_image, mIconStatusCair;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;

    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;
    private Long id;
    private Button prosesDelete, prosesRequest, prosesCair, cekSaldo;
    private static final int REQUEST_PHONE_CALL = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailClaimActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
       // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_claim));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_claim);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        dataClaim = gson.fromJson(getIntent().getStringExtra("detail"), DataClaim.class);
        txtDateApprove = findViewById(R.id.txtDateApprove);
        tbApprover = findViewById(R.id.tbApprover);
        tbApprover.setVisibility(View.GONE);
        tbisCair = findViewById(R.id.tbisCair);
        tbisCair.setVisibility(View.GONE);
        tbDetailCair = findViewById(R.id.tbDetailCair);
        tbDetailCair.setVisibility(View.GONE);
        txtApprover = findViewById(R.id.txtApprover);
        txtKategori = findViewById(R.id.txtKategori);
        txtComment = findViewById(R.id.txtComment);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        mTxtNote = findViewById(R.id.mTxtNote);
        txtJumlah = findViewById(R.id.txtJumlah);
        mTxtStart = findViewById(R.id.mTxtStart);
        //  mTxtJudul = findViewById(R.id.mTxtJudul);
        mLayoutImage = findViewById(R.id.mLayoutImage);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        mImageView = findViewById(R.id.mImageView);
        mIconStatus = findViewById(R.id.mIconStatus);
        mIconStatusCair = findViewById(R.id.mIconStatusCair);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();
        prosesCair = findViewById(R.id.prosesCair);
        cekSaldo = findViewById(R.id.cekSaldo);
        prosesDelete = findViewById(R.id.prosesDelete);
        prosesRequest = findViewById(R.id.prosesRequest);
        txtTransfer = findViewById(R.id.txtTransfer);
        txtTransfer.setVisibility(View.GONE);
        txtCek = findViewById(R.id.txtCek);
        txtCek.setVisibility(View.GONE);

        txtstatusCair = findViewById(R.id.txtstatusCair);
        txtDateCair = findViewById(R.id.txtDateCair);
        txtCairer = findViewById(R.id.txtCairer);
        txtCairComment = findViewById(R.id.txtCairComment);


        loading = new SweetAlertDialog(DetailClaimActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);

        if (dataClaim != null) {
            DetailClaim(dataClaim.getReimbursementID().intValue());
        }else {
            onNewIntent(getIntent());
        }



    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataClaim = new DataClaim();
                        dataClaim.setReimbursementID(Long.parseLong(String.valueOf(idx)));
                        DetailClaim(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
   /* public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApproveKlaim();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  ProsesUpdate();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }*/

    public void prosesRequest(View v) {
        PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataClaim.getReimbursementID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        ApproveKlaimFragment dialogFragment = new ApproveKlaimFragment();
        Bundle bundle = new Bundle();
        bundle.putString("amount", temp.getAmount().toString());
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");




    }


    public void prosesCair(View v) {



            PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).
                    edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataClaim.getReimbursementID().toString())
                    .apply();

            android.app.FragmentManager fm = getFragmentManager();
            CairClaimFragment dialogFragment = new CairClaimFragment();
            Bundle bundle = new Bundle();
            bundle.putString("amount", temp.getAmount().toString());
            bundle.putString("id", temp.getReimbursementID().toString());
            bundle.putString("idEmployee", temp.getEmployee().getEmployeeID().toString());
            bundle.putString("email", temp.getEmployee().getEmail());
            bundle.putString("holder", temp.getBankAccountName());
            bundle.putString("norek", temp.getBankNumber());
            bundle.putString("bank", temp.getBankName());
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");


    }

    public void prosesDelete(View v) {
        new android.app.AlertDialog.Builder(DetailClaimActivity.this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteData();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void cekSaldo(View v) {
        Intent intent = new Intent(DetailClaimActivity.this,DetailSaldoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);


    }

    public void DeleteData() {

        loading.show();

        try {




            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String idx = String.valueOf(id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.deleteClaim(idx, Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.dismiss();

                            new SweetAlertDialog(DetailClaimActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getResources().getString(R.string.title_sukses))
                                    .setContentText(getResources().getString(R.string.title_berhasil_hapus_data))
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(DetailClaimActivity.this, ClaimActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);



                                        }
                                    })
                                    .show();


                        }
                        else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(DetailClaimActivity.this,getResources().getString(R.string.title_gagal)
                                    , "");
                        }

                    } else {
                        loading.dismiss();
                        messageDialog.mShowMessageError(DetailClaimActivity.this,getResources().getString(R.string.title_gagal)
                                , "");
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(DetailClaimActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(DetailClaimActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }

    }

    public void DetailClaim(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailClaim> call = api.getClaimDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDetailClaim>() {
                @Override
                public void onResponse(Call<ResponseDetailClaim> call, Response<ResponseDetailClaim> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_idProses_employee), response.body().getData().getEmployee().getEmployeeID().toString())
                                    .apply();
                            ShowData(response.body().getData());
                            temp = response.body().getData();



                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailClaimActivity.this, getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                            } catch (Exception e){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                        messageDialog.mShowMessageError(DetailClaimActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailClaim> call, Throwable t) {
                    loading.dismiss();
                        try {
                    messageDialog.mShowMessageError(DetailClaimActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                        } catch (Exception e){

                        }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
                    try {
            messageDialog.mShowMessageError(DetailClaimActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
                    } catch (Exception d){

                    }
        }
    }

    private void ShowData(DataClaim data){


        try {
            if (data.getActivityID() != null & data.getKasbonID() != null) {

                if (data.getReimbursementCategoryID() == 7 & data.getKasbonID() != 0) {
                    txtCek.setVisibility(View.VISIBLE);
                    txtCek.setText(getResources().getString(R.string.title_lihat_kasbon));
                    txtCek.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(DetailClaimActivity.this, DetailClaimActivity.class);
                            DataClaim dataReimbursementItem = new DataClaim();
                            dataReimbursementItem.setReimbursementID(Long.valueOf(data.getKasbonID()));
                            Gson gson = new Gson();
                            String myJson = gson.toJson(dataReimbursementItem);
                            intent.putExtra("detail", myJson);
                            startActivity(intent);
                        }
                    });
                } else {

                    if (data.getActivityID() != 0) {
                        txtCek.setVisibility(View.VISIBLE);
                        txtCek.setText(getResources().getString(R.string.title_lihat_aktivitas));
                        txtCek.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(DetailClaimActivity.this, DetailAktivitasActivity.class);
                                DataAktivitas dataAsetItem = new DataAktivitas();
                                dataAsetItem.setActivityID(Long.valueOf(data.getActivityID()));
                                Gson gson = new Gson();
                                String myJson = gson.toJson(dataAsetItem);
                                intent.putExtra("detail", myJson);
                                startActivity(intent);
                            }
                        });
                    } else {
                        txtCek.setVisibility(View.GONE);
                    }

                }

            } else {
                txtCek.setVisibility(View.GONE);
            }
        }catch (Exception ex){

        }




        id = data.getReimbursementID();
        prosesDelete.setVisibility(View.GONE);
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        txtJumlah.setText(String.valueOf(data.getAmount() != null ? getResources().getString(R.string.title_rp) + " " + formatter.format(data.getAmount()) : "-"));

        mTxtNote.setText(data.getNote()!=null ? data.getNote() : "");
        txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization() : "");
        txtKategori.setText(data.getReimbursementCategory().getName()!=null ? data.getReimbursementCategory().getName() : "");
        if (data.getDate() != null)
            mTxtStart.setText(DefaultFormatter.changeFormatDate(data.getDate()));


        if (data.getIsApproved() == null){
            mIconStatus.setBackgroundResource(R.drawable.status_pending);
            mTxtStatus.setText(getResources().getString(R.string.title_menunggu));
            mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            mTxtStatus.setTextColor( ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

        }
        else {
            try {
                tbApprover.setVisibility(View.VISIBLE);
                txtApprover.setText(data.getApprover().getFirstName() + " " + data.getApprover().getLastName());
                txtComment.setText(data.getComment());
                if (data.getDateApproved() != null)
                    txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getDateApproved()));


                if (data.getIsApproved()) {
                    mIconStatus.setBackgroundResource(R.drawable.status_accept);
                    mTxtStatus.setText(getResources().getString(R.string.title_disetujui));
                    mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                    mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

                    tbisCair.setVisibility(View.VISIBLE);


                    if (data.getIsCair()!=null) {
                        if (data.getIsCair()) {
                            tbDetailCair.setVisibility(View.VISIBLE);

                            mIconStatusCair.setBackgroundResource(R.drawable.ic_cairtrue);
                            txtstatusCair.setText(getResources().getString(R.string.title_sudah_dicairkan));
                            txtstatusCair.setBackgroundResource(R.drawable.rounded_green);

                            txtCairComment.setText(data.getMethodCair());
                            txtCairer.setText(data.getUserCair());
                            if (data.getDateCair() != null)
                                txtDateCair.setText(DefaultFormatter.changeFormatDate(data.getDateCair()));


                            if (data.getXendit() != null) {
                                if (data.getXendit().size() > 0) {
                                    txtTransfer.setVisibility(View.VISIBLE);
                                    txtTransfer.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                            if (data.getKeyXendit() != null) {
                                                Intent intent = new Intent(DetailClaimActivity.this, TransferDetailActivity.class);
                                                intent.putExtra("detail", data.getXendit().get(data.getXendit().size() - 1).getId());
                                                startActivity(intent);
                                            } else {
                                                MDToast.makeText(DetailClaimActivity.this, getResources().getString(R.string.title_tidak_punya_token_xendit),
                                                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                            }
                                        }
                                    });
                                }
                            }


                        } else {
                            mIconStatusCair.setBackgroundResource(R.drawable.ic_cairfalse);
                            txtstatusCair.setText(getResources().getString(R.string.title_belum_dicairkan));
                            txtstatusCair.setBackgroundResource(R.drawable.rounded_orange);
                        }
                    }else{
                        mIconStatusCair.setBackgroundResource(R.drawable.ic_cairfalse);
                        txtstatusCair.setText(getResources().getString(R.string.title_belum_dicairkan));
                        txtstatusCair.setBackgroundResource(R.drawable.rounded_orange);
                    }

                } else {
                    mIconStatus.setBackgroundResource(R.drawable.status_reject);
                    mTxtStatus.setText(getResources().getString(R.string.title_ditolak));
                    mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                    mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

                }
            } catch (Exception e){

            }

        }



        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);

        String url = "";

        try{
            url = data.getFilePath().toString();
        } catch (Exception e){

        }

        if (!url.equals("")) {


            Glide.with(DetailClaimActivity.this)
                    .load((getResources().getString(R.string.base_url) + url))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.getFilePath());
                    startActivity(intent);
                }
            });
        }
        else {
            mImageView.setVisibility(View.GONE);
        }

        if (data.getEmployee() != null) {
            if (data.getEmployee().getFirstName() != null) {
                profil_nama.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
            }
            if (data.getEmployee().getNik() != null) {
                profil_role.setText(data.getEmployee().getNik());
            }
            if (data.getEmployee().getEmail() != null) {
                profil_email.setText(data.getEmployee().getEmail());
            }
            if (data.getEmployee().getPhone() != null) {
                profil_phone.setText(data.getEmployee().getPhone());
            }

            if (data.getEmployee().getPhone().equals("") || data.getEmployee().getPhone() == null) {

            } else{
                mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                    @Override
                    public void onClick(View v) {
                        final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                        CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(getResources().getString(R.string.title_aksi));
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // Check Permissions Now
                                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(intent);
                                    }
                                } else if (which == 1) {
                                    Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                    Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                    mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(mSendSms);
                                } else if (which == 2) {
                                    String name = data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName();
                                    String phone = data.getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                    contactIntent
                                            .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                            .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                    ((Activity) context).startActivityForResult(contactIntent, 1);

                                } else if (which == 3) {

                                    voiceCall(profil_phone.getText().toString().trim());
                                }
                            }
                        });
                        builder.show();
                    }
                });
            }

            if (data.getEmployee().getPhotoUrl() != null) {

                Glide.with(DetailClaimActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getEmployee().getPhotoUrl()))
                        .apply(requestOptions)
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);
                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getEmployee().getPhotoUrl());
                        startActivity(intent);
                    }
                });

            }
            else {
                // profile_image.setVisibility(View.GONE);
            }

            String organisasi = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_namaOrganization), ""));


            String approval = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_CanApproveReimbursement), ""));
            String id = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_employeeid), ""));

            String keyxendit = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_key_xendit), ""));

            String kasir = (PreferenceManager.getDefaultSharedPreferences(DetailClaimActivity.this).getString(
                    getResources().getString(R.string.pref_kasir), ""));

            if (!kasir.equals("true")) {
                if (approval.equals("true")) {

                    if (data.getEmployee().getEmployeeID().toString().equals(id)) {
                        prosesRequest.setVisibility(View.GONE);
                    } else {
                        if (CanApproveVoid) {
                            prosesRequest.setVisibility(View.VISIBLE);
                        } else {

                            if (data.getIsApproved() == null) {

                                prosesRequest.setVisibility(View.VISIBLE);
                            } else {
                                prosesRequest.setVisibility(View.GONE);
                            }

                        }
                    }
                } else {
                    prosesRequest.setVisibility(View.GONE);
                }
            } else {
                prosesRequest.setVisibility(View.GONE);

                if (data.getIsApproved() == null){

                    prosesRequest.setVisibility(View.VISIBLE);

                }else{
                    if(data.getIsApproved()== true){

                        if (data.getIsCair()!=null) {
                            if (!data.getIsCair()) {
                                prosesRequest.setVisibility(View.VISIBLE);
                                prosesCair.setVisibility(View.VISIBLE);
                                if (!keyxendit.equals("")){
                                    cekSaldo.setVisibility(View.VISIBLE);
                                }

                            } else {
                                if (data.getXendit() != null) {
                                    if (data.getXendit().size() > 0) {

                                        if (data.getXendit().get(data.getXendit().size() - 1).getStatus() != null) {

                                            if (data.getXendit().get(data.getXendit().size() - 1).getStatus().equals("FAILED")) {
                                                prosesRequest.setVisibility(View.VISIBLE);
                                                prosesCair.setVisibility(View.VISIBLE);
                                                if (!keyxendit.equals("")){
                                                    cekSaldo.setVisibility(View.VISIBLE);
                                                }
                                            }
                                        }


                                    }
                                }
                            }
                        }

                    }
                }


            }


        }
    }

    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(DetailClaimActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
           // Toast.makeText(DetailClaimActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailClaimActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }



}