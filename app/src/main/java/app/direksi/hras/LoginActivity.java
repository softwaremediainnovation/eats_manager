package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.lang.reflect.Method;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.Pengguna;
import app.direksi.hras.model.ResponseLoginPengguna;
import app.direksi.hras.model.User;
import app.direksi.hras.permission.PermissionsActivity;
import app.direksi.hras.permission.PermissionsChecker;
import app.direksi.hras.util.CrashReport;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.LogoutPermission;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private Button mButtonLogin;
    private SweetAlertDialog loading;
    private EditText mEdtEmail,
            mEdtPassword;

    private TextView txtVersion, txtForgetPassword;
    private Pengguna userLogin;
    private MessageDialog messageDialog;
    private CrashReport crashReport;
    private String mUid ="";
    public static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 1;

    private static final String[] PERMISSIONS_READ_STORAGE = new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE,
           android.Manifest.permission.INTERNET,
            android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.ACCESS_COARSE_LOCATION,
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.INTERNET,
            android.Manifest.permission.CAMERA,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.CALL_PHONE};

    private PermissionsChecker checker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(LoginActivity.this);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        //remove line in bar
//        getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
      //  getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading

        crashReport = new CrashReport();
        messageDialog = new MessageDialog();

        loading = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        setContentView(R.layout.activity_login);
        txtVersion = findViewById(R.id.txtVersion);
        txtForgetPassword = findViewById(R.id.txtForgetPassword);
        mButtonLogin = findViewById(R.id.mButtonLogin);
        mEdtEmail = findViewById(R.id.mEdtEmail);
        mEdtPassword = findViewById(R.id.mEdtPassword);
        mEdtPassword.setTransformationMethod(new PasswordTransformationMethod());
        mButtonLogin.setOnClickListener(this);

        checker = new PermissionsChecker(this);

        if (checker.lacksPermissions(PERMISSIONS_READ_STORAGE)) {
            startPermissionsActivity(PERMISSIONS_READ_STORAGE);
        }

        try {
            String mCurrentVersion = LoginActivity.this.getPackageManager().getPackageInfo(LoginActivity.this.getPackageName(), 0).versionName;
            txtVersion.setText(getApplicationInfo().loadLabel(getPackageManager()).toString() + " " + getResources().getString(R.string.title_versi) + " " +  mCurrentVersion);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        txtForgetPassword.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, LoginActivity.this);
    }

    //start permission
    private void startPermissionsActivity(String[] permission) {
        PermissionsActivity.startActivityForResult(this, 0, permission);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mButtonLogin) {
            mAttemptLoginUser();
        }
    }

    private void mAttemptLoginUser() {
       /* if (loading.isShowing()) {
            return;
        }*/

        mEdtEmail.setError(null);
        mEdtPassword.setError(null);

        String email = mEdtEmail.getText().toString().replace(" ","");
        String password = mEdtPassword.getText().toString();


        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEdtEmail.setError(getString(R.string.login_email_blank));
            focusView = mEdtEmail;
            cancel = true;
        } else if (!Validation.isEmailValid(email)) {
            mEdtEmail.setError(getString(R.string.login_email_failed));
            focusView = mEdtEmail;
            cancel = true;
        }

        if (password.isEmpty()) {
            mEdtPassword.setError(getString(R.string.login_password_blank));
            focusView = mEdtPassword;
            cancel = true;
        } else if (!Validation.isPasswordValid(password)) {
            mEdtPassword.setError(getString(R.string.error_invalid_password));
            focusView = mEdtPassword;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            mLoginUser();
        }
    }

    @Override
    protected void onStart() {

        //check if session is empty direct to Dashboard
        if (!PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                getResources().getString(R.string.pref_nama), "").isEmpty()) {

            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } /*else {
            mLoginUser("client@client.com", "12345678");
        }*/
        super.onStart();
    }


    private void mLoginUser() {

        try {
            loading.show();

            String Token = FirebaseInstanceId.getInstance().getToken();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).getString(
                    getResources().getString(R.string.pref_token), "tes"));

            String serialnumber = "";

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                serialnumber = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

            }
            else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                        != PackageManager.PERMISSION_GRANTED) {

                    loading.dismiss();

                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.READ_PHONE_STATE},
                            PERMISSIONS_REQUEST_READ_PHONE_STATE);


                  //  MDToast.makeText(LoginActivity.this, "Berikan hak akses agar bisa login", MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                    // TODO: Consider calling
                    return;
                }
                serialnumber = getSerialNumber();
                if(serialnumber.equals("unknown")){
                    serialnumber = Build.getSerial();

                }

            } else {
                serialnumber = getSerialNumber();
            }

            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED) {
                startPermissionsActivity(PERMISSIONS_READ_STORAGE);
            } else {
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                } else {

                    mUid = tManager.getDeviceId();
                }

            }

            if(serialnumber.equals("unknown")){
                serialnumber = mUid;

            }

            final String sn = serialnumber;



            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            User user = new User(Validation.mGetText(mEdtPassword), Validation.mGetText(mEdtEmail).replace(" ",""), Token, serialnumber);


            Gson gson = new Gson();
            String jsonString = gson.toJson(user);
            Log.v(TAG, jsonString);


            Call<ResponseLoginPengguna> call = api.mLoginUser(Authorization, user);
            call.enqueue(new Callback<ResponseLoginPengguna>() {
                @Override
                public void onResponse(Call<ResponseLoginPengguna> call, Response<ResponseLoginPengguna> response) {

                    Log.v(TAG, response.toString());

                    try {
                        //loading hide
                        loading.dismiss();

                        if (response.isSuccessful()) {
                            long statusCode = response.body().getErrCode();
                            String statusMessage = response.body().getErrMessage();
                            //status code 1 if login success
                            if (statusCode == 0) {


                                if (response.body().getData().getStatus()!= null){

                                    if (response.body().getData().getStatus().equals("Success")){


                                        if (response.body().getData().getValue() != null) {

                                            userLogin = response.body().getData().getUser();

                                            //save data profile to sharepreferences


                                            if (userLogin.getIsLoginAllowed() == true ) {
                                                Boolean temp = false;
                                                try {
                                                    if (response.body().getData().getLembur()== null){

                                                    } else {
                                                        temp = response.body().getData().getLembur();
                                                    }

                                                } catch (Exception es){

                                                }

                                                String face = "biometric";
                                                try {
                                                    if (response.body().getData().getAttendance()== null){

                                                    } else {
                                                        face = response.body().getData().getAttendance().toLowerCase();
                                                    }

                                                } catch (Exception es){

                                                }

                                                try {

                                                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                                                            edit().putString(getResources().getString(R.string.pref_nama), userLogin.getFirstName() + " " + userLogin.getLastName())
                                                            .putString(getResources().getString(R.string.pref_Logout_permission), LogoutPermission)
                                                            .putString(getResources().getString(R.string.pref_role), userLogin.getRole())
                                                            .putString(getResources().getString(R.string.pref_adress), userLogin.getAddress())
                                                            .putString(getResources().getString(R.string.pref_gender), userLogin.getGender() != null ? userLogin.getGender() : "")
                                                            .putString(getResources().getString(R.string.pref_employeeid), (userLogin.getEmployeeID().toString()))
                                                            .putString(getResources().getString(R.string.pref_nik), userLogin.getNik())
                                                            .putString(getResources().getString(R.string.pref_photourl), getResources().getString(R.string.base_url) + userLogin.getPhotoUrl())
                                                            .putString(getResources().getString(R.string.pref_phone), userLogin.getPhone())
                                                            .putString(getResources().getString(R.string.pref_dob), userLogin.getDob())
                                                            .putString(getResources().getString(R.string.pref_id), userLogin.getId())
                                                            .putString(getResources().getString(R.string.pref_email), userLogin.getEmail())
                                                            .putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                                            .putString(getResources().getString(R.string.pref_token), "Bearer " + response.body().getData().getValue())
                                                            .putString(getResources().getString(R.string.pref_CanViewActivity), userLogin.getPermission().getCanViewActivity().toString())
                                                            .putString(getResources().getString(R.string.pref_CanViewAsset), userLogin.getPermission().getCanViewAsset().toString())
                                                            .putString(getResources().getString(R.string.pref_CanApproveDayOff), userLogin.getPermission().getCanApproveDayOff().toString())
                                                            .putString(getResources().getString(R.string.pref_CanApproveOvertime), userLogin.getPermission().getCanApproveOvertime().toString())
                                                            .putString(getResources().getString(R.string.pref_CanApproveReimbursement), userLogin.getPermission().getCanApproveReimbursement().toString())
                                                            .putString(getResources().getString(R.string.pref_CanIssueReprimand), userLogin.getPermission().getCanIssueReprimand().toString())
                                                            .putString(getResources().getString(R.string.pref_CanApproveLoan), userLogin.getPermission().getCanApproveLoan().toString())
                                                            .putString(getResources().getString(R.string.pref_CanViewAttendance), userLogin.getPermission().getCanViewAttendance().toString())
                                                            .putString(getResources().getString(R.string.pref_CanApproveRequestAttendance), userLogin.getPermission().getCanApproveRequestAttendance().toString())
                                                            .putString(getResources().getString(R.string.pref_CanViewTask), userLogin.getPermission().getCanViewTask().toString())
                                                            .putString(getResources().getString(R.string.pref_CanViewEvaluation), userLogin.getPermission().getmCanViewEvaluation().toString())
                                                            .putString(getResources().getString(R.string.pref_idOrganization), userLogin.getOrganization() != null ? userLogin.getOrganization().getCompanyID().toString() : "")
                                                            .putString(getResources().getString(R.string.pref_namaOrganization), userLogin.getOrganization() != null ? userLogin.getOrganization().getName() : "")
                                                            .putString(getResources().getString(R.string.pref_contract_expired), userLogin.getContractExpiredDate())
                                                            .putString(getResources().getString(R.string.pref_overtime_option), temp.toString())
                                                            .putString(getResources().getString(R.string.pref_absen_method), face)
                                                            .putString(getResources().getString(R.string.pref_kasir), response.body().getData().getKasir())
                                                            .apply();

                                                    MDToast.makeText(LoginActivity.this, getResources().getString(R.string.login_success), MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();
                                                    //MDToast.makeText(LoginActivity.this, response.body().getData().getKasir().toString(), MDToast.LENGTH_LONG, MDToast.TYPE_SUCCESS).show();

                                                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                    startActivity(intent);
                                                    finish();


                                                } catch (Exception e){
                                                    MDToast.makeText(LoginActivity.this, getResources().getString(R.string.title_login_hak_akses_kurang), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                                }

                                            } else {

                                                MDToast.makeText(LoginActivity.this, getResources().getString(R.string.title_login_tidak_ada_hak_akses), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                                // Toast.makeText(LoginActivity.this, "Login gagal. Tidak ada hak akses", Toast.LENGTH_LONG).show();

                                            }
                                        } else {
                                            MDToast.makeText(LoginActivity.this, getResources().getString(R.string.title_login_gagal) + " " + sn, MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                            // Toast.makeText(LoginActivity.this, "Login gagal. Segera hubungi Administrator", Toast.LENGTH_LONG).show();
                                        }

                                    } else {

                                        MDToast.makeText(LoginActivity.this, response.body().getData().getResult()!= null? response.body().getData().getResult() : getResources().getString(R.string.title_login_gagal_mendapatkan_status), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                    }
                                } else {

                                    MDToast.makeText(LoginActivity.this, response.body().getData().getResult()!= null? response.body().getData().getResult() : getResources().getString(R.string.title_login_gagal_mendapatkan_status), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                }


                                //status code 2 user not yet register
                            } else {
                                MDToast.makeText(LoginActivity.this, response.body().getErrMessage(), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                              /*  messageDialog.mShowMessageError(LoginActivity.this, getResources().getString(R.string.title_login)
                                        , response.body().getErrMessage());
                                Log.v(TAG, response.toString());*/

                            }

                        } else if(response.code() == 500 )
                        {
                            MDToast.makeText(LoginActivity.this, getResources().getString(R.string.title_emailpass_salah), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                        } else {
                            MDToast.makeText(LoginActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                         /*   messageDialog.mShowMessageError(LoginActivity.this, getResources().getString(R.string.title_login)
                                    , getResources().getString(R.string.loading_error));*/
                        }
                    } catch (Exception e) {
                        Log.v(TAG, e.getMessage());
                        MDToast.makeText(LoginActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                      /*  messageDialog.mShowMessageError(LoginActivity.this, getResources().getString(R.string.title_login)
                                , getResources().getString(R.string.loading_error));*/
                    }


                }

                @Override
                public void onFailure(Call<ResponseLoginPengguna> call, Throwable t) {
                    loading.dismiss();
                    MDToast.makeText(LoginActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                   /* messageDialog.mShowMessageError(LoginActivity.this, getResources().getString(R.string.title_login)
                            , getResources().getString(R.string.loading_error));*/
                }

            });


        } catch (Exception e) {
            loading.dismiss();
            MDToast.makeText(LoginActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
        /*    messageDialog.mShowMessageError(LoginActivity.this, getResources().getString(R.string.title_login)
                    , getResources().getString(R.string.loading_error));*/
        }
    }

    public static String getSerialNumber() {
        String serialNumber;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);

            serialNumber = (String) get.invoke(c, "gsm.sn1");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ril.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ro.serialno");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "sys.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = Build.SERIAL;

            // If none of the methods above worked
            if (serialNumber.equals(""))
                serialNumber = null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber = null;
        }

        return serialNumber;
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(LoginActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }




}
