package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.LateAttendanceFragment;
import app.direksi.hras.fragment.NotLateAttendanceFragment;
import app.direksi.hras.model.ResponseDataAttendance;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.SimpleSpanBuilder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 05/09/2019.
 */

public class LateActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;

    private ImageView mProfileImage;
    private int mMaxScrollSize;
    private TextView count0, count1;
    private TabLayout tabLayout;

    public AttendanceActivity.FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(AttendanceActivity.FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private AttendanceActivity.FragmentRefreshListener1 fragmentRefreshListener1;




    public AttendanceActivity.FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(AttendanceActivity.FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private AttendanceActivity.FragmentRefreshListener2 fragmentRefreshListener2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(LateActivity.this);
        setContentView(R.layout.activity_terlambat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.checkin));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);



        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        viewPager.setAdapter(new LateActivity.TabsAdapter(getSupportFragmentManager(), LateActivity.this));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                //  Toast.makeText(CutiActivity.this, String.valueOf(position), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        NotLate();
        Late();

    }



    public static void start(Context c) {
        c.startActivity(new Intent(c, CutiActivity.class));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
    }

    private static class TabsAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 2;
        private Context context;

        TabsAdapter(FragmentManager fm, Context cont) {
            super(fm);
            context = cont;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int i) {

            final Fragment result;
            switch (i) {
                case 0:
                    // First Tab
                    result = LateAttendanceFragment.newInstance();
                    break;
                case 1:
                    // Second Tab
                    result = NotLateAttendanceFragment.newInstance();
                    break;

                default:
                    result = LateAttendanceFragment.newInstance();
                    break;
            }
            return result;



        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title ="";
            if (position == 0){
                title = context.getResources().getString(R.string.title_terlambat);
            }
            if (position == 1){
                title = context.getResources().getString(R.string.title_tidak_terlambat);
            }
            return title;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(LateActivity.this);

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                edit()
                .putString(getResources().getString(R.string.pref_dayoff_start), "")
                .putString(getResources().getString(R.string.pref_dayoff_end), "")
                .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                .putString(getResources().getString(R.string.pref_dayoff_end_shadow), "")
                .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                .apply();



    }



   /* @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_aseet, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
          /*  case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterAttendanceFragment dialogFragment = new FilterAttendanceFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;
            case R.id.action_asset:
                Intent mIntent = new Intent(getApplicationContext(), MyAssetsActivity.class);
                startActivity(mIntent);
                return true;*/

        }
        return super.onOptionsItemSelected(item);
    }

    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        // Toast.makeText(getBaseContext(), ""+ selectedValue, Toast.LENGTH_LONG).show();
        if(getFragmentRefreshListener1()!=null){
            getFragmentRefreshListener1().onRefresh();
        }
        if(getFragmentRefreshListener2()!=null){
            getFragmentRefreshListener2().onRefresh();
        }
    }

    public void NotLate() {

        try {




            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));
            String idEmployee = (PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                    getResources().getString(R.string.pref_employeeid), ""));



            Map<String, String> data = new HashMap<>();
            data.put("employeeID", idEmployee);

            if (!PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty()) {
                data.put("dateFilter", PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));
            } else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = sdf.format(c.getTime());
                data.put("dateFilter", strDate);

            }

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataAttendance> call = api.getAttendanceTidakTelatHariIni( Authorization, data);

            call.enqueue(new Callback<ResponseDataAttendance>() {
                @Override
                public void onResponse(Call<ResponseDataAttendance> call, Response<ResponseDataAttendance> response) {


                    if (response.isSuccessful()) {

                     /*   Log.v("DoneFragment", "sukses");
                        Log.v("DoneFragment", "jumlah count" +  results.size());*/
                        if (response.body().getErrCode() == 0) {
                            if (response.body().getData().size() > 0) {

                                String spasi = " ";
                                if (response.body().getData().size() > 100){
                                    spasi = "";
                                }

                                int flag = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
                                SpannableString staffTitleSpan = new SpannableString(getResources().getString(R.string.title_tidak_terlambat) + "   ");
                                SpannableString staffNameSpan = new SpannableString(spasi + String.valueOf(response.body().getData().size()) + spasi);
                                staffNameSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, staffNameSpan.length(), flag);
                                staffNameSpan.setSpan(new SimpleSpanBuilder(Color.rgb(255, 255, 255), getResources().getColor(R.color.colorPrimary), 30, 8, 8, 0), 0, staffNameSpan.length(), flag);
                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                builder.append(staffTitleSpan);
                                builder.append(staffNameSpan);
                                tabLayout.getTabAt(1).setText(builder);


                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataAttendance> call, Throwable t) {

                }
            });
        } catch (Exception e) {

        }

    }

    public void Late() {

        try {




            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));
            String idEmployee = (PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                    getResources().getString(R.string.pref_employeeid), ""));



            Map<String, String> data = new HashMap<>();
            data.put("employeeID", idEmployee);

            if (!PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty()) {
                data.put("dateFilter", PreferenceManager.getDefaultSharedPreferences(LateActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));
            } else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = sdf.format(c.getTime());
                data.put("dateFilter", strDate);

            }

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataAttendance> call = api.getAttendanceTelatHariIni( Authorization, data);

            call.enqueue(new Callback<ResponseDataAttendance>() {
                @Override
                public void onResponse(Call<ResponseDataAttendance> call, Response<ResponseDataAttendance> response) {


                    if (response.isSuccessful()) {


                        if (response.body().getErrCode() == 0) {
                            if (response.body().getData().size() > 0) {

                                String spasi = " ";
                                if (response.body().getData().size() > 100){
                                    spasi = "";
                                }

                                int flag = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
                                SpannableString staffTitleSpan = new SpannableString(getResources().getString(R.string.title_terlambat) + "   ");
                                SpannableString staffNameSpan = new SpannableString(spasi + String.valueOf(response.body().getData().size()) + spasi);
                                staffNameSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, staffNameSpan.length(), flag);
                                staffNameSpan.setSpan(new SimpleSpanBuilder(Color.rgb(255, 255, 255), getResources().getColor(R.color.colorPrimary), 30, 8, 8, 0), 0, staffNameSpan.length(), flag);
                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                builder.append(staffTitleSpan);
                                builder.append(staffNameSpan);
                                tabLayout.getTabAt(0).setText(builder);

                            }
                        }




                    }
                }

                @Override
                public void onFailure(Call<ResponseDataAttendance> call, Throwable t) {


                }
            });
        } catch (Exception e) {


        }



    }

    public void updateCount(String text){

        NotLate();
        Late();

    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
