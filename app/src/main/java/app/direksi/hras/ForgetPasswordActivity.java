package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.ResponseForgetPassword;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class ForgetPasswordActivity extends AppCompatActivity {

    private EditText mEdtEmail;
    private SweetAlertDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(ForgetPasswordActivity.this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.forget_password));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        setContentView(R.layout.activity_forget_password);
        loading = new SweetAlertDialog(ForgetPasswordActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        mEdtEmail = findViewById(R.id.mEdtEmail);
        Button mButtonLogin = findViewById(R.id.mButtonLogin);
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
              /*  Intent intent = new Intent(LoginActivity.this, OTPActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);*/

                mAttemptForgetPass();
            }
        });

    }

    
    private void mAttemptForgetPass() {
       /* if (loading.isShowing()) {
            return;
        }*/

        mEdtEmail.setError(null);
        String email = mEdtEmail.getText().toString().replace(" ","");

        boolean cancel = false;
        View focusView = null;


        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEdtEmail.setError(getString(R.string.login_email_blank));
            focusView = mEdtEmail;
            cancel = true;
        } else if (!Validation.isEmailValid(email)) {
            mEdtEmail.setError(getString(R.string.login_email_failed));
            focusView = mEdtEmail;
            cancel = true;
        }


        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            Forget();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void Forget() {

        try {
            loading.show();

            RequestBody email = RequestBody.create(MediaType.parse("text/plain"), Validation.mGetText(mEdtEmail).replace(" ",""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            Call<ResponseForgetPassword> call = api.forgetPassword(email);
            call.enqueue(new Callback<ResponseForgetPassword>() {
                @Override
                public void onResponse(Call<ResponseForgetPassword> call, Response<ResponseForgetPassword> response) {

                    if (response.isSuccessful()) {

                        loading.dismiss();
                        if (response.body().getData().getStatus().equals("Success")){

                            SweetAlertDialog alertDialog = new SweetAlertDialog(ForgetPasswordActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText(getResources().getString(R.string.title_berhasil_mengirim_email));
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    Intent login = new Intent(ForgetPasswordActivity.this, LoginActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);
                                }
                            });
                            alertDialog.show();

                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));

                        } else {

                            MDToast.makeText(ForgetPasswordActivity.this, response.body().getData().getResult(), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                        }

                    } else if(response.code() == 403 )
                    {
                        loading.dismiss();
                        MDToast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();

                    } else {
                        loading.dismiss();
                        MDToast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseForgetPassword> call, Throwable t) {
                    loading.dismiss();
                    MDToast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                }
            });

        } catch (Exception e) {
            loading.dismiss();
            MDToast.makeText(ForgetPasswordActivity.this, getResources().getString(R.string.loading_error), MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(ForgetPasswordActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }



}
