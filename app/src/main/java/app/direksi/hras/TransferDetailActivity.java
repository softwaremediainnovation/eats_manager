package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.ListClaimCairAdapter;
import app.direksi.hras.model.ResponseDataKeyXendit;
import app.direksi.hras.model.DataClaim;
import app.direksi.hras.model.ResponseDisbursement;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class TransferDetailActivity extends AppCompatActivity {

    DataClaim dataClaim;
    DataClaim temp;
    TableLayout tbApprover, tbisCair, tbDetailCair;
    public TextView txtJumlah, txtBank, txtHolder, mTxtNote, txtTransaksi, mTxtSum, txtNoBKKHeader;
    private ImageView mIconStatus, profile_image;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;

    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;
    private Long id;
    private Button prosesDelete, prosesRequest, prosesCair;
    private static final int REQUEST_PHONE_CALL = 1;

    private RelativeLayout llLast;
    private ListClaimCairAdapter cAdapter;
    private RecyclerView mRootView;
    private List<DataClaim> dataList = new ArrayList<>();
    private LinearLayout llBKKHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(TransferDetailActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_transfer));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_transfer);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        Bundle bundle = getIntent().getExtras();
        String detail = bundle.getString("detail");

        llBKKHeader = findViewById(R.id.llBKKHeader);
        txtNoBKKHeader = findViewById(R.id.txtNoBKKHeader);
        mIconStatus = findViewById(R.id.mIconStatus);
        txtTransaksi = findViewById(R.id.txtTransaksi);
        txtJumlah = findViewById(R.id.txtJumlah);
        txtBank = findViewById(R.id.txtBank);
        txtHolder = findViewById(R.id.txtHolder);
        mTxtNote = findViewById(R.id.mTxtNote);
        mTxtSum = findViewById(R.id.mTxtSum);

        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();



        loading = new SweetAlertDialog(TransferDetailActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        llLast = findViewById(R.id.llLast);
        llLast.setVisibility(View.GONE);
        mRootView = findViewById(R.id.mRootView);

        cAdapter = new ListClaimCairAdapter(dataList, TransferDetailActivity.this);
        mRootView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(TransferDetailActivity.this);
        mRootView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRootView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(TransferDetailActivity.this, DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(TransferDetailActivity.this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(TransferDetailActivity.this, R.drawable.divider_black));

        mRootView.addItemDecoration(itemDecorator);
        mRootView.setAdapter(cAdapter);

        mRootView.addOnItemTouchListener(new RecyclerTouchListener(TransferDetailActivity.this, mRootView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));




        if (detail != null) {
            GetDetailKeyXendit(detail);
        }
        onNewIntent(getIntent());



    }


    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        GetDetailKeyXendit(mBody);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getBase64(String input) {
        return Base64.encodeToString(input.getBytes(), Base64.NO_WRAP);
    }

    public void DetailTransfer(String  id, String key, List<DataClaim> reimbursement, Integer idBKK) {
        loading.show();

        try {


            String username = key;
            String password = "";
            String svcCredentials = "Basic "+ getBase64(username + ":" + password);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url_xendid))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDisbursement> call = api.getDetailTransfer(svcCredentials, id);

            call.enqueue(new Callback<ResponseDisbursement>() {
                @Override
                public void onResponse(Call<ResponseDisbursement> call, Response<ResponseDisbursement> response) {


                    if (response.isSuccessful()) {

                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);


                            if (reimbursement!= null){

                                long temp = 0;
                                for (int i = 0; i < reimbursement.size(); i++){
                                    temp = temp + reimbursement.get(i).getAmount();

                                }
                                DecimalFormat formatter = new DecimalFormat("#,###,###");
                                mTxtSum.setText(String.valueOf( getResources().getString(R.string.title_rp)  + " " + formatter.format(temp)) );


                                llLast.setVisibility(View.VISIBLE);
                                dataList.addAll(reimbursement);
                                cAdapter.notifyDataSetChanged();

                                if (idBKK!= null){
                                    llBKKHeader.setVisibility(View.VISIBLE
                                    );
                                    txtNoBKKHeader.setText(idBKK.toString());
                                }


                            }

                        scrollView.fullScroll(ScrollView.FOCUS_UP);

                        DecimalFormat formatter = new DecimalFormat("#,###,###");

                        txtJumlah.setText("Rp " + formatter.format(response.body().getAmount()));
                        txtBank.setText(response.body().getBankCode());
                        txtHolder.setText(response.body().getAccountHolderName());
                        mTxtNote.setText(response.body().getDisbursementDescription());

                        if (response.body().getStatus().equals("PENDING")){
                            mIconStatus.setBackgroundResource((R.drawable.status_pending));
                            txtTransaksi.setText(getResources().getString(R.string.title_transaksi_pending));

                        }else if (response.body().getStatus().equals("FAILED")){
                            mIconStatus.setBackgroundResource((R.drawable.status_reject));
                            txtTransaksi.setText(getResources().getString(R.string.title_transaksi_failed));

                        }else  if (response.body().getStatus().equals("COMPLETED")){
                            mIconStatus.setBackgroundResource((R.drawable.status_accept));
                            txtTransaksi.setText(getResources().getString(R.string.title_transaksi_sukses));

                        } else {
                            mIconStatus.setVisibility(View.GONE);
                            txtTransaksi.setVisibility(View.GONE);

                        }





                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(TransferDetailActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDisbursement> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(TransferDetailActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(TransferDetailActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            } catch (Exception d){

            }
        }
    }


    public void GetDetailKeyXendit(String  id) {
        loading.show();

        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(TransferDetailActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("id",id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataKeyXendit> call = api.getKeyXendit(Authorization, data);

            call.enqueue(new Callback<ResponseDataKeyXendit>() {
                @Override
                public void onResponse(Call<ResponseDataKeyXendit> call, Response<ResponseDataKeyXendit> response) {


                    if (response.isSuccessful()) {

                       if (response.body().getData().getStatus().equals("Success")){

                           DetailTransfer(id,response.body().getData().getResult(), response.body().getData().getReimbursement(), response.body().getData().getIdBuktiKasHeader());


                       }else {

                           loading.dismiss();
                           try {
                               messageDialog.mShowMessageError(TransferDetailActivity.this,getResources().getString(R.string.title_gagal)
                                       , getResources().getString(R.string.loading_error));
                           } catch (Exception e){

                           }
                       }


                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(TransferDetailActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataKeyXendit> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(TransferDetailActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(TransferDetailActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            } catch (Exception d){

            }
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(TransferDetailActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}
