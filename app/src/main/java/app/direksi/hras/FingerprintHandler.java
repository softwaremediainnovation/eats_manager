package app.direksi.hras;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.preference.PreferenceManager;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.DataCompany;
import app.direksi.hras.model.ResponseCheckIn;
import app.direksi.hras.model.ResponseDataCompany;
import app.direksi.hras.util.GetLocation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by whit3hawks on 11/16/16.
 */
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {



    private CancellationSignal cancellationSignal;
    private static final String TAG = FingerprintHandler.class.getSimpleName();
    private Context context;
    private double latitude = 0, longitude = 0;
    boolean isFragment = false;

    private LocationManager manager;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;


    RelativeLayout mLayoutLoading;
    RelativeLayout mLayoutMains;
    TextView mTxShow;

    Location myLocation;
    private List<DataCompany> dataCompanies;


    // Constructor
    public FingerprintHandler(Context mContext) {
        context = mContext;
    }

    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject, boolean isfragment, List<DataCompany> dataCompanie) {
        cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        isFragment = isfragment;
        dataCompanies = dataCompanie;


        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Kesalahan otentikasi sidik jari\n" + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Bantuan Autentikasi Sidik Jari\n" + helpString);
    }

    @Override
    public void onAuthenticationFailed() {
        this.update("Otentikasi Sidik Jari gagal.");
    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {

        ((Activity) context).finish();
        Intent mIntent = new Intent(context, CheckinActivity.class);
        mIntent.putExtra("isFragment", isFragment);
        context.startActivity(mIntent);

/*        manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        mLayoutLoading = ((Activity) context).findViewById(R.id.mLayoutLoading);
        mLayoutMains = ((Activity) context).findViewById(R.id.mLayoutMains);
        mTxShow = ((Activity) context).findViewById(R.id.mTxShow);

        mLayoutMains.setVisibility(View.GONE);
        mLayoutLoading.setVisibility(View.VISIBLE);

        mTxShow.setText("Mencari Lokasi..");

        mLocationCallback = new LocationCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    mFusedLocationClient.removeLocationUpdates(mLocationCallback);
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
            }
        };

        mGetCompanies();*/
    }


    private void update(String e) {
        try {
            TextView textView = ((Activity) context).findViewById(R.id.errorText);
            if (e != null)
                textView.setText(e);
        } catch (Exception es) {
        }
    }

    private void mShowDialog(String message, int type) {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(context.getResources().getString(R.string.string_fingerprint))
                .setContentText(message)
                .setConfirmText(context.getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                        try {
                            sweetAlertDialog.cancel();
                            if (type == 0) {
                                ((Activity) context).startActivityForResult(new Intent(Settings.ACTION_SETTINGS), 0);
                            } else if (type == 1) {
                                Intent intent = context.getPackageManager().getLaunchIntentForPackage("com.google.android.apps.maps");
                                intent.setAction(Intent.ACTION_VIEW);
                                context.startActivity(intent);
                            } else if (type == 2) {
                                context.startActivity(new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS));
                            }
                        } catch (Exception e) {
                        }

                    }
                }).show();
    }


    void getMaps() {

        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener((Activity) context, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            Bundle extras = null;
                            if (location != null)
                                extras = location.getExtras();
                            boolean isMockLocation = extras != null && extras.getBoolean(FusedLocationProviderApi.KEY_MOCK_LOCATION, false);

                            if (isMockLocation) {
                                mLayoutMains.setVisibility(View.VISIBLE);
                                mLayoutLoading.setVisibility(View.GONE);
                                mShowDialog("Anda menggunakan lokasi palsu, silahkan matikan lokasi palsu anda", 2);
                                return;
                            }

                            if (location != null) {
                                mTxShow.setText("Mendapatkan lokasi");
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();

                                if (insideRadius()) {
                                    mHandOver();
                                } else {
                                    mShowDialog("Jarak anda terlalu jauh dengan kantor", 1);
                                }

                            } else {
                                getLastLocation();
                            }


                        }


                    });
            mFusedLocationClient.getLastLocation().addOnFailureListener((Activity) context, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });


            mFusedLocationClient.getLastLocation().addOnFailureListener((Activity) context, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });

            mFusedLocationClient.getLastLocation().addOnFailureListener((Activity) context, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FingerprintHandler", e.getMessage());
                }
            });


        } catch (Exception e) {
            Log.e("FingerprintHandler", e.getMessage());
        }
    }


    private void getLastLocation() {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);


        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                mTxShow.setText("Mendapatkan lokasi");

                if (insideRadius()) {
                    mHandOver();
                } else {
                    mShowDialog("Jarak anda terlalu jauh dengan kantor", 1);
                }

            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, locationListener, null);

    }

    public boolean insideRadius() {

        boolean isInside = false;

        int radius = Integer.parseInt (PreferenceManager.getDefaultSharedPreferences(context).getString(
                context.getResources().getString(R.string.pref_radius), ""));



        myLocation =   GetLocation.getLastKnownLocation(context);

        if (latitude == 0 && longitude == 0) {
            try {
                latitude = myLocation.getLatitude();
                longitude = myLocation.getLongitude();
            } catch (Exception e) {
            }
        }

        Location locationA = new Location("");

        locationA.setLatitude(latitude);
        locationA.setLongitude(longitude);


        for (int i = 0; i < dataCompanies.size(); i++) {
            Location locationB = new Location("");

            locationB.setLatitude( Double.parseDouble(dataCompanies.get(i).getLatitude()));
            locationB.setLongitude(Double.parseDouble(dataCompanies.get(i).getLongitude()));

            float distancse = locationA.distanceTo(locationB);

            Log.e(TAG, "Lokasi saat ini = " + latitude + " --- " + longitude);
            Log.e(TAG, "Lokasi kantor satt ini = " + dataCompanies.get(i).getLatitude() + " --- " + dataCompanies.get(i).getLongitude());
            Log.e(TAG, "Jarak " + distancse + "");

            if (distancse < radius) {
                isInside = true;
            }

        }


        return isInside;
    }


    public void mHandOver() {


        RequestBody Longitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(latitude));
        RequestBody Latitude = RequestBody.create(MediaType.parse("text/plain"), Double.toString(latitude));


        String Authorization = (PreferenceManager.getDefaultSharedPreferences(context).getString(
                context.getResources().getString(R.string.pref_token), ""));


        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        // simplified call to request the news with already initialized service
        Call<ResponseCheckIn> call = api.CheckIn(Authorization, Longitude, Latitude);

        call.enqueue(new Callback<ResponseCheckIn>() {
            @Override
            public void onResponse(Call<ResponseCheckIn> call, Response<ResponseCheckIn> response) {

                try {

                    if (response.isSuccessful()) {

                        Long statusCode = response.body().getErrCode();


                        if (statusCode == 0) {


                            Toast.makeText(context, context.getResources().getString(R.string.absen_success), Toast.LENGTH_SHORT).show();
                            if (!isFragment) {
                                ((Activity) context).finish();
                            } else {
                                Intent intent = new Intent(context, HomeActivity.class);
                                context.startActivity(intent);
                                ((Activity) context).finish();
                            }

                        } else {
                            mLayoutMains.setVisibility(View.VISIBLE);
                            mLayoutLoading.setVisibility(View.GONE);
                            Toast.makeText(context, response.body().getErrMessage(), Toast.LENGTH_SHORT).show();

                        }

                    } else {
                        mLayoutMains.setVisibility(View.VISIBLE);
                        mLayoutLoading.setVisibility(View.GONE);
                        Toast.makeText(context, response.body().getErrMessage(), Toast.LENGTH_SHORT).show();

                    }

                } catch (Exception e) {
                    mLayoutMains.setVisibility(View.VISIBLE);
                    mLayoutLoading.setVisibility(View.GONE);

                    Toast.makeText(context, context.getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseCheckIn> call, Throwable t) {
                mLayoutMains.setVisibility(View.VISIBLE);
                mLayoutLoading.setVisibility(View.GONE);
                Toast.makeText(context, context.getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void mGetCompanies() {


        String Authorization = (PreferenceManager.getDefaultSharedPreferences(context).getString(context.getResources().getString(R.string.pref_token), "tes"));

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getResources().getString(R.string.base_url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


        Call<ResponseDataCompany> call = api.getCompany(Authorization);
        call.enqueue(new Callback<ResponseDataCompany>() {
            @Override
            public void onResponse(Call<ResponseDataCompany> call, Response<ResponseDataCompany> response) {

                try {


                    if (response.isSuccessful()) {
                        Long statusCode = response.body().getErrCode();
                        if (statusCode == 0) {

                            Log.e(TAG, response.toString());

                            dataCompanies = response.body().getData();

                            getMaps();

                        } else {
                            showMessageDialog();
                        }

                    } else {
                        showMessageDialog();
                    }
                } catch (Exception e) {
                    showMessageDialog();
                }

            }

            @Override
            public void onFailure(Call<ResponseDataCompany> call, Throwable t) {
                showMessageDialog();
            }

        });
    }


    void showMessageDialog() {
        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(context.getResources().getString(R.string.string_confirmation))
                .setContentText(context.getResources().getString(R.string.refresh_again))
                .setCancelText(context.getResources().getString(R.string.txt_no))
                .setConfirmText(context.getResources().getString(R.string.string_yes))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                        if (!isFragment) {
                            ((Activity) context).finish();
                        } else {
                            Intent intent = new Intent(context, HomeActivity.class);
                            context.startActivity(intent);
                            ((Activity) context).finish();
                        }
                    }
                })
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                        mGetCompanies();
                    }
                })
                .show();
    }


    public void stopFingerAuth(){
        if(cancellationSignal != null && !cancellationSignal.isCanceled()){
            cancellationSignal.cancel();
        }
    }


}
