package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.DetailTaskAdapter;
import app.direksi.hras.fragment.UpdateTaskDetailFragment;
import app.direksi.hras.model.DataTask;
import app.direksi.hras.model.DataTaskIDDetail;
import app.direksi.hras.model.DataTaskIDDetailItem;
import app.direksi.hras.model.ResponseDataTaskIDDetail;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class DetailTaskKuActivity extends AppCompatActivity {


    DataTask dataTask;

    public TextView txtTanggal, txtCreator, txtCatatan,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtOrganisasi,
            txtTanggalKadaluarsa,
            mTxtNote,
            mTxtStatus,
            txtSum;
    private ImageView profile_image;
    private ImageView mIconStatus;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;

    private int id;
    private Button prosesDelete;
    private TableRow trExpiredDate;

    RecyclerView recyclerView;
    private DetailTaskAdapter cAdapter;
    private List<DataTaskIDDetailItem> dataList = new ArrayList<>();

    private ProgressBar progressBar;
    private TextView txtProgress;
    private String dateTemp = "";
    private List<DataTaskIDDetail> datatEMP =  new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailTaskKuActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.task_detail));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_task);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        Gson gson = new Gson();
        dataTask = gson.fromJson(getIntent().getStringExtra("detail"), DataTask.class);
        //dataCutiItem = getIntent().getParcelableExtra("detail");

        trExpiredDate = findViewById(R.id.trExpiredDate);
        trExpiredDate.setVisibility(View.GONE);
        txtTanggalKadaluarsa = findViewById(R.id.txtTanggalKadaluarsa);
        mTxtNote = findViewById(R.id.mTxtNote);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        txtTanggal = findViewById(R.id.txtTanggal);
        txtCreator = findViewById(R.id.txtCreator);
        txtCatatan = findViewById(R.id.txtCatatan);
        mImageView = findViewById(R.id.mImageView);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();
        prosesDelete = findViewById(R.id.prosesDelete);
        prosesDelete.setVisibility(View.GONE);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        loading = new SweetAlertDialog(DetailTaskKuActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);

        progressBar = findViewById(R.id.progress);
        txtProgress = findViewById(R.id.txtProgress);
        progressBar.setProgress(0); //Set Progress Dimulai Dari O


        recyclerView = findViewById(R.id.recyclerView);
        txtSum = findViewById(R.id.txtSum);


        cAdapter = new DetailTaskAdapter(dataList, DetailTaskKuActivity.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(DetailTaskKuActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(DetailTaskKuActivity.this, DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(DetailTaskKuActivity.this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(DetailTaskKuActivity.this, R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(cAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(DetailTaskKuActivity.this, recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                DataTaskIDDetailItem data = dataList.get(position);


                if (data.isIsFinish() == true){

                    Intent mIntent = new Intent(DetailTaskKuActivity.this, DetailTaskDetailActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);

                }
                else {

                    //expired check
                    if (!datatEMP.get(0).getHeader().getDueDate().contains("0001")) {

                        if (!dateTemp.equals("")) {

                            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                            try {
                                Date dateStart = format.parse(dateTemp);
                                Date dateEnds = format.parse(datatEMP.get(0).getHeader().getDueDate());

                                Calendar cal = Calendar.getInstance();

                                cal.setTime(dateEnds);
                                cal.set(Calendar.HOUR_OF_DAY, 23);
                                cal.set(Calendar.MINUTE, 59);
                                Date dateEnd = cal.getTime();


                                if (dateStart.after(dateEnd)) {

                                    MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.title_tgl_kadaluarsa_sudah_lewat),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

                                } else {
                                    try {

                                        PreferenceManager.getDefaultSharedPreferences(DetailTaskKuActivity.this).
                                                edit()
                                                .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                                                .apply();

                                        FragmentManager fm = DetailTaskKuActivity.this.getSupportFragmentManager();
                                        UpdateTaskDetailFragment dialog = new UpdateTaskDetailFragment();
                                        Bundle b = new Bundle();
                                        b.putString("id", String.valueOf(data.getTaskDetailID()));
                                        b.putString("note", data.getTaskName());
                                        dialog.setArguments(b);
                                        dialog.show(fm, "confirm delete");
                                    } catch (Exception e) {
                                        MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.title_gagal),
                                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                        // Toast.makeText(getActivity(), "GAGAL", Toast.LENGTH_SHORT).show();
                                    }
                                }


                            } catch (ParseException e) {

                                MDToast.makeText(DetailTaskKuActivity.this, "Gagal convert tanggal",
                                        MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                                e.printStackTrace();
                            }

                        } else {
                            mGetServerTim();
                        }
                    } else {

                        try {

                            PreferenceManager.getDefaultSharedPreferences(DetailTaskKuActivity.this).
                                    edit()
                                    .putString(getResources().getString(R.string.pref_aktivitas_maps), "1")
                                    .apply();

                            FragmentManager fm = DetailTaskKuActivity.this.getSupportFragmentManager();
                            UpdateTaskDetailFragment dialog = new UpdateTaskDetailFragment();
                            Bundle b = new Bundle();
                            b.putString("id", String.valueOf(data.getTaskDetailID()));
                            b.putString("note", data.getTaskName());
                            dialog.setArguments(b);
                            dialog.show(fm, "confirm delete");
                        } catch (Exception e) {
                            MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.title_gagal),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(getActivity(), "GAGAL", Toast.LENGTH_SHORT).show();
                        }

                    }
                }



            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));


        mGetServerTim();


    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailTaskKuActivity.this);

        dataList.clear();

        if (dataTask != null) {
            DetailTask(dataTask.getTaskHeaderID());
        } else {
            onNewIntent(getIntent());
        }

    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataTask = new DataTask();
                        dataTask.setTaskHeaderID(Integer.parseInt(String.valueOf(idx)));
                        DetailTask(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailTaskKuActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public void DetailTask( int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTaskKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataTaskIDDetail> call = api.getTaskDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDataTaskIDDetail>() {
                @Override
                public void onResponse(Call<ResponseDataTaskIDDetail> call, Response<ResponseDataTaskIDDetail> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            datatEMP = (response.body().getData());
                            ShowData(response.body().getData());



                        }
                        else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(DetailTaskKuActivity.this,getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.dismiss();
                        messageDialog.mShowMessageError(DetailTaskKuActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataTaskIDDetail> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(DetailTaskKuActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(DetailTaskKuActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }
    }


    private void ShowData(List<DataTaskIDDetail> data){
        id = data.get(0).getHeader().getTaskHeaderID();
        if (data.get(0).getHeader().getDateCreated() != null)
            txtTanggal.setText(DefaultFormatter.changeFormatDate(data.get(0).getHeader().getDateCreated()));
        if (!data.get(0).getHeader().getDueDate().contains("0001")) {
            txtTanggalKadaluarsa.setText(DefaultFormatter.changeFormatDateWithOutHour(data.get(0).getHeader().getDueDate()));
            trExpiredDate.setVisibility(View.VISIBLE);
        }


        // txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization() : "");
        txtCreator.setText(data.get(0).getCreator().get(0).getFirstName() + " " + data.get(0).getCreator().get(0).getLastName());
        txtCatatan.setText(data.get(0).getHeader().getTaskName());
        mTxtNote.setText(data.get(0).getHeader().getNote());


        if (!data.get(0).getIsExpired()) {
            if (data.get(0).getHeader().isIsFinish()) {

                mTxtStatus.setText(getResources().getString(R.string.title_selesai));
                mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                mTxtStatus.setTextColor(ContextCompat.getColor(DetailTaskKuActivity.this, R.color.colorWhite));

            } else {

                mTxtStatus.setText(getResources().getString(R.string.title_belum_selesai));
                mTxtStatus.setBackgroundResource(R.drawable.rounded_orange);
                mTxtStatus.setTextColor(ContextCompat.getColor(DetailTaskKuActivity.this, R.color.colorWhite));

            }
        } else {

            mTxtStatus.setText(getResources().getString(R.string.title_kadaluarsa));
            mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
            mTxtStatus.setTextColor(ContextCompat.getColor(DetailTaskKuActivity.this, R.color.colorWhite));
        }

        int temp = 0;
        for ( int i = 0; i < data.get(0).getDetail().size(); i ++){

            if (data.get(0).getDetail().get(i).isIsFinish() == true){
                temp = temp + 1;
            }
        }
        txtSum.setText(String.valueOf(temp)+ "/" + String.valueOf(data.get(0).getDetail().size()));

        float percentTemp =  temp * 100 / data.get(0).getDetail().size();

        txtProgress.setText( math(percentTemp) + "%");
        progressBar.setProgress(math(percentTemp));


        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);



        if (data.get(0).getEmployees().get(0).getPhotoUrl() != null) {

            Glide.with(DetailTaskKuActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.get(0).getEmployees().get(0).getPhotoUrl()))
                    .apply(requestOptions)
                    .error(Glide.with(profile_image).load(R.drawable.profile))
                    .into(profile_image);
            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.get(0).getEmployees().get(0).getPhotoUrl());
                    startActivity(intent);
                }
            });

        }
        else {
            // profile_image.setVisibility(View.GONE);
        }



        profil_nama.setText(data.get(0).getEmployees().get(0).getFirstName()!= null? data.get(0).getEmployees().get(0).getFirstName() + " "+ data.get(0).getEmployees().get(0).getLastName() : " ");
        profil_role.setText(data.get(0).getEmployees().get(0).getNik() !=null ? data.get(0).getEmployees().get(0).getNik() : " ");
        profil_email.setText(data.get(0).getEmployees().get(0).getEmail() != null? data.get(0).getEmployees().get(0).getEmail() : " ");
        profil_phone.setText(data.get(0).getEmployees().get(0).getPhone()!=null? data.get(0).getEmployees().get(0).getPhone() : " ");
        txtOrganisasi.setText(data.get(0).getOrganizations()!=null? data.get(0).getOrganizations() : " ");


        dataList.addAll(data.get(0).getDetail());
        cAdapter.notifyDataSetChanged();







    }

    public static int math(float f) {
        int c = (int) ((f) + 0.5f);
        float n = f + 0.5f;
        return (n - c) % 2 == 0 ? (int) f : c;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void mGetServerTim() {

        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTaskKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            dateTemp = response.body().getData();




                        } else {

                            MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        }

                    } catch (Exception e) {

                        MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {

                    MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                }
            });
        } catch (Exception e) {

            MDToast.makeText(DetailTaskKuActivity.this, getResources().getString(R.string.loading_error),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
        }

    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}

