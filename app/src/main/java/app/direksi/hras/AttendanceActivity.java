package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.widget.ImageView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.ListAttendanceFragment;
import app.direksi.hras.fragment.MyAttendanceFragment;
import app.direksi.hras.model.DataEmploye;
import app.direksi.hras.fragment.FilterAttendanceFragment;
import app.direksi.hras.model.ResponseCountToday;
import app.direksi.hras.model.ResponseDataAttendance;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.SimpleSpanBuilder;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 27/02/2019.
 */

public class AttendanceActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;

    private ImageView mProfileImage;
    private int mMaxScrollSize;
    private TabLayout tabLayout;


    public FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private FragmentRefreshListener1 fragmentRefreshListener1;




    public FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private FragmentRefreshListener2 fragmentRefreshListener2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(AttendanceActivity.this);
        setContentView(R.layout.activity_asset_new);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.kehadiran));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);



        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        viewPager.setAdapter(new AttendanceActivity.TabsAdapter(getSupportFragmentManager(), AttendanceActivity.this));
        String id = (PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                getResources().getString(R.string.pref_aktivitas_maps), ""));
        viewPager.setCurrentItem(Integer.parseInt(id));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                //  Toast.makeText(CutiActivity.this, String.valueOf(position), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        Late();


    }

    public void updateCount(String text){

        Late();

    }


    public static void start(Context c) {
        c.startActivity(new Intent(c, CutiActivity.class));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
    }

    private static class TabsAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 2;
        private Context context;

        TabsAdapter(FragmentManager fm, Context cont) {
            super(fm);
            context = cont;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int i) {

            final Fragment result;
            switch (i) {
                case 0:
                    // First Tab
                    result = ListAttendanceFragment.newInstance();
                    break;
                case 1:
                    // Second Tab
                    result = MyAttendanceFragment.newInstance();
                    break;

                default:
                    result = ListAttendanceFragment.newInstance();
                    break;
            }
            return result;



        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title ="";
            if (position == 0){
                title = context.getString(R.string.title_daftar_kehadiran);
            }
            if (position == 1){
                title = context.getString(R.string.title_kehadiran_ku);
            }
            return title;
        }
    }

    public void Late() {

        try {




            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
                data.put("page", "1");
                data.put("size", "100000");
                data.put("Grab", "approval");

                String idCompany =PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_search_idCompany), "");

                if (!idCompany.equals("0")) {
                    // Toast.makeText(ListCutiActivity.this, "approved", Toast.LENGTH_LONG).show();
                    data.put("CompanyId", idCompany);
                }

                String idOrganization =PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_search_idOrganization), "");

                if (!idOrganization.equals("0")) {
                    // Toast.makeText(EmployeActivity.this, idOrganization, Toast.LENGTH_LONG).show();
                    data.put("OrganizationId", idOrganization);
                }
            /*    if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_start), "").isEmpty())
                    data.put("start", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_dayoff_start), ""));*/

              /*  if (!PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                        getResources().getString(R.string.pref_dayoff_end), "").isEmpty())
                    data.put("end", PreferenceManager.getDefaultSharedPreferences(getContext()).getString(
                            getResources().getString(R.string.pref_dayoff_end), ""));*/

                if (!PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_search), "").isEmpty())
                    data.put("search", PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                            getResources().getString(R.string.pref_dayoff_search), ""));



            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseCountToday> call = api.getCountToday( Authorization, data);

            call.enqueue(new Callback<ResponseCountToday>() {
                @Override
                public void onResponse(Call<ResponseCountToday> call, Response<ResponseCountToday> response) {


                    if (response.isSuccessful()) {


                        if (response.body().getErrCode() == 0) {

                            if (response.body().getData() > 0) {

                                String spasi = " ";
                                if (response.body().getData() > 100){
                                    spasi = "";
                                }

                                int flag = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
                                SpannableString staffTitleSpan = new SpannableString(getResources().getString(R.string.title_daftar_kehadiran) + "   ");
                                SpannableString staffNameSpan = new SpannableString(spasi + String.valueOf(response.body().getData()) + spasi);
                                staffNameSpan.setSpan(new StyleSpan(Typeface.BOLD), 0, staffNameSpan.length(), flag);
                                staffNameSpan.setSpan(new SimpleSpanBuilder(Color.rgb(255, 255, 255), getResources().getColor(R.color.colorPrimary), 30, 8, 8, 0), 0, staffNameSpan.length(), flag);
                                SpannableStringBuilder builder = new SpannableStringBuilder();
                                builder.append(staffTitleSpan);
                                builder.append(staffNameSpan);
                                tabLayout.getTabAt(0).setText(builder);

                            }
                        }




                    }
                }

                @Override
                public void onFailure(Call<ResponseCountToday> call, Throwable t) {
                    String a = t.toString();

                }
            });
        } catch (Exception e) {

            String a = e.toString();

        }



    }

    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(AttendanceActivity.this);

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                edit()
                .putString(getResources().getString(R.string.pref_dayoff_start), "")
                .putString(getResources().getString(R.string.pref_dayoff_end), "")
                .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                .putString(getResources().getString(R.string.pref_dayoff_end_shadow), "")
                .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                .apply();



    }



    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_kehadiran, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterAttendanceFragment dialogFragment = new FilterAttendanceFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;

               /* Intent intent = new Intent(AttendanceActivity.this, CalendarActivity.class);
                startActivity(intent);
                return true;*/

            case R.id.action_history:

                String id = (PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                        getResources().getString(R.string.pref_employeeid), ""));

                DataEmploye data = new DataEmploye();
                data.setEmployeeID(Long.parseLong(id));

                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(getResources().getString(R.string.pref_employeeid_view), data.getEmployeeID().toString())
                        .apply();
                Intent mIntent = new Intent(getApplicationContext(), HistoryAttendanceActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        // Toast.makeText(getBaseContext(), ""+ selectedValue, Toast.LENGTH_LONG).show();

        Late();

        if(getFragmentRefreshListener1()!=null){
            getFragmentRefreshListener1().onRefresh();
        }
        if(getFragmentRefreshListener2()!=null){
            getFragmentRefreshListener2().onRefresh();
        }
    }


       @Override
    public void onBackPressed() {


           String id = (PreferenceManager.getDefaultSharedPreferences(AttendanceActivity.this).getString(
                   getResources().getString(R.string.pref_aktivitas_maps), ""));
           if (id.equals("1")){

               Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
               intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
               startActivity(intent);
               finish();

           } else {
               super.onBackPressed();
           }


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }



}

