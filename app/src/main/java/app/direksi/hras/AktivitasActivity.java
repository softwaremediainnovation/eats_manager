package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;


import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.widget.ImageView;

import app.direksi.hras.fragment.FilterAktivitasFragment;
import app.direksi.hras.fragment.MyActivityFragment;
import app.direksi.hras.fragment.ListActivityFragment;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 11/04/2019.
 */

public class AktivitasActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;

    private ImageView mProfileImage;
    private int mMaxScrollSize;


    public AktivitasActivity.FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(AktivitasActivity.FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private AktivitasActivity.FragmentRefreshListener1 fragmentRefreshListener1;




    public AktivitasActivity.FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(AktivitasActivity.FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private AktivitasActivity.FragmentRefreshListener2 fragmentRefreshListener2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(AktivitasActivity.this);
        setContentView(R.layout.activity_asset_new);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
        //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.activity));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);






        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        viewPager.setAdapter(new AktivitasActivity.TabsAdapter(getSupportFragmentManager(), AktivitasActivity.this));
        String id = (PreferenceManager.getDefaultSharedPreferences(AktivitasActivity.this).getString(
                getResources().getString(R.string.pref_aktivitas_maps), ""));
        viewPager.setCurrentItem(Integer.parseInt(id));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                //  Toast.makeText(CutiActivity.this, String.valueOf(position), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }



    public static void start(Context c) {
        c.startActivity(new Intent(c, CutiActivity.class));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
    }

    private static class TabsAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 2;
        private Context context;

        TabsAdapter(FragmentManager fm, Context cont) {
            super(fm);
            context = cont;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int i) {

            final Fragment result;
            switch (i) {
                case 0:
                    // First Tab
                    result = ListActivityFragment.newInstance();
                    break;
                case 1:
                    // Second Tab
                    result = MyActivityFragment.newInstance();
                    break;

                default:
                    result = ListActivityFragment.newInstance();
                    break;
            }
            return result;



        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title ="";
            if (position == 0){
                title = context.getString(R.string.title_daftar_aktivitas);
            }
            if (position == 1){
                title = context.getString(R.string.title_aktivitas_ku);
            }
            return title;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_aktivitas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterAktivitasFragment dialogFragment = new FilterAktivitasFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;
            case R.id.action_employee:
                Intent mIntent = new Intent(getApplicationContext(), EmployeeAktivitasActivity.class);
                startActivity(mIntent);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }



   /* @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cari, menu);
        return true;
    }*/



    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(AktivitasActivity.this);


    }


    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        // Toast.makeText(getBaseContext(), ""+ selectedValue, Toast.LENGTH_LONG).show();
        if(getFragmentRefreshListener1()!=null){
            getFragmentRefreshListener1().onRefresh();
        }
        if(getFragmentRefreshListener2()!=null){
            getFragmentRefreshListener2().onRefresh();
        }
    }


    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }





}


