package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.EmployeeAktIvitasAdapter;
import app.direksi.hras.fragment.CreateCutiFragment;
import app.direksi.hras.fragment.FilterEmployeAktivitasFragment;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.model.ResponseDataEmployee;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class EmployeeAktivitasActivity extends AppCompatActivity implements  View.OnClickListener,  SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ListCutiActivity.class.getSimpleName();

    private List<DataEmployee> results = new ArrayList<>();
    private EmployeeAktIvitasAdapter cutiAdapter;

    SweetAlertDialog loading;
    TextView txtEmptyList;
    private Filter filter;
    RecyclerView recyclerView;

    private MessageDialog messageDialog;
    FloatingActionMenu mFloat;
    FloatingActionButton fab;
    Integer pageNumber = 1;
    SwipeRefreshLayout swiperefresh;
    boolean isLoading = false;
    int scrollPosition = 0 ;


    public EmployeActivity.FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(EmployeActivity.FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private EmployeActivity.FragmentRefreshListener1 fragmentRefreshListener1;




    public EmployeActivity.FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(EmployeActivity.FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private EmployeActivity.FragmentRefreshListener2 fragmentRefreshListener2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(EmployeeAktivitasActivity.this);
        setContentView(R.layout.activity_employe);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");

        //remove line in bar
        // getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading
        //  getSupportActionBar().setTitle(getResources().getString(R.string.title_list_order));


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.employe));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        messageDialog = new MessageDialog();

        swiperefresh = findViewById(R.id.swiperefresh);
        recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        fab = findViewById(R.id.fab);
        txtEmptyList = findViewById(R.id.txtEmptyList);

        mFloat = findViewById(R.id.material_design_android_floating_action_menu);

        mFloat.setOnClickListener(this);
        fab.setOnClickListener(this);
        swiperefresh.setOnRefreshListener(this);



        loading = new SweetAlertDialog(EmployeeAktivitasActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        loading.show();
        pageNumber = 1;

       /* results = new ArrayList<>();
        cutiAdapter = new EmployeeAdapter(results, getApplicationContext());
        recyclerView.setAdapter(cutiAdapter);
        results.clear();*/

        cutiAdapter = new EmployeeAktIvitasAdapter(results, EmployeeAktivitasActivity.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(cutiAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
               /* DataEmployee data = results.get(position);
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(getResources().getString(R.string.pref_employeeid_view), data.getEmployeeID().toString())
                        .apply();
                Intent mIntent = new Intent(getApplicationContext(), ListAktivitasActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);*/





                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();


                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        String approval = (PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).getString(
                getResources().getString(R.string.pref_CanViewActivity), ""));

        if (approval.equals("true")) {

            fetchDetailSummary();
            initScrollListener();

        } else {
            txtEmptyList.setVisibility(View.VISIBLE);
            txtEmptyList.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
            loading.dismiss();

        }




    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == results.size() - 1) {
                        //bottom of list!
                        double mod = results.size() % Integer.parseInt(getResources().getString(R.string.count_paging));
                        if ( mod > 0 ){

                        }else{
                            loadMore();
                            isLoading = true;

                        }
                    }
                }
            }
        });


    }

    private void loadMore() {

        results.add(null);
        cutiAdapter.notifyItemInserted(results.size() - 1);
        fetchDetailSummary();


      /*  Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                results.remove(results.size() - 1);
                scrollPosition = results.size();
                fetchDetailSummary();
                } catch (Exception e){

                }

               *//* while (currentSize - 1 < nextLimit) {
                    rowsArrayList.add("Item " + currentSize);
                    currentSize++;
                }*//*

         *//* cAdapter.notifyDataSetChanged();
                isLoading = false;*//*
            }
        }, 2000);*/


    }



    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(EmployeeAktivitasActivity.this);

       /* String tag = (PreferenceManager.getDefaultSharedPreferences(EmployeActivity.this).getString(
                getResources().getString(R.string.pref_dayoff_flag), ""));

        if (tag.equals("0")){
            loading = new SweetAlertDialog(EmployeActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
            loading.setTitleText(getResources().getString(R.string.loading));
            loading.setCancelable(false);

            loading.show();
            pageNumber = 1;

            results = new ArrayList<>();
            cutiAdapter = new EmployeActivity.EmployeAdapter(getApplicationContext(), R.layout.item_cuti, R.id.txtName, results);
            cutiAdapter.setNotifyOnChange(true);
            recyclerView.setAdapter(cutiAdapter);

            cutiAdapter.clear();
            results.clear();

            fetchDetailSummary();

        } else {
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                    .apply();

        }*/



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();

                PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).
                        edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                        .putString(getResources().getString(R.string.pref_dayoff_start), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                        .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                        .putString(getResources().getString(R.string.pref_dayoff_end_shadow), "")
                        /* .putString(getResources().getString(R.string.pref_dayoff_status), "0")*/
                        .apply();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterEmployeAktivitasFragment dialogFragment = new FilterEmployeAktivitasFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        //  loading.show();

        String approval = (PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).getString(
                getResources().getString(R.string.pref_CanViewActivity), ""));

        if (approval.equals("true")) {

            loading.show();
            pageNumber = 1;
            results = new ArrayList<>();
            cutiAdapter = new EmployeeAktIvitasAdapter(results, EmployeeAktivitasActivity.this);
            recyclerView.setAdapter(cutiAdapter);
            swiperefresh.setRefreshing(false);
            fetchDetailSummary();

        } else {
            swiperefresh.setRefreshing(false);
            txtEmptyList.setVisibility(View.VISIBLE);
            txtEmptyList.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
            loading.dismiss();
        }


    }

    private class EmployeAdapter extends ArrayAdapter<DataEmployee> {
        EmployeeAktivitasActivity.EmployeAdapter.ViewHolder holder = null;
        private Context context;


        public EmployeAdapter(Context context, int resource, int textViewResourceId, List<DataEmployee> objects) {
            super(context, resource, textViewResourceId, objects);
            this.context = context;
        }

        private class ViewHolder {
            public TextView mTxtJudul, txtEmail, txtPhone, profil_role;

            public ConstraintLayout mMainLayout;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final DataEmployee data = getItem(position);

            convertView = getLayoutInflater().inflate(R.layout.item_employe, null);
            holder = new EmployeeAktivitasActivity.EmployeAdapter.ViewHolder();
            convertView.setTag(holder);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                            .putString(getResources().getString(R.string.pref_employeeid_view), data.getEmployeeID().toString())
                            .apply();
                    Intent mIntent = new Intent(getApplicationContext(), DetailEmployeActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);

                }
            });

            holder.profil_role = convertView.findViewById(R.id.profil_role);
            holder.mTxtJudul = convertView.findViewById(R.id.mTxtJudul);
            holder.txtEmail = convertView.findViewById(R.id.txtEmail);
            holder.txtPhone = convertView.findViewById(R.id.txtPhone);
            holder.mMainLayout = convertView.findViewById(R.id.mMainLayout);


            holder.profil_role.setText(data.getNik() != null ? data.getNik() : " ");
            holder.mTxtJudul.setText(data.getFirstName() + " " + data.getLastName());
            holder.txtEmail.setText(data.getEmail()!= null ? data.getEmail(): "");
            holder.txtPhone.setText(data.getPhone() != null ? data.getPhone():"");

            holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                            edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                            .putString(getResources().getString(R.string.pref_employeeid_view), data.getEmployeeID().toString())
                            .apply();
                    Intent mIntent = new Intent(getApplicationContext(), DetailEmployeActivity.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(data);
                    mIntent.putExtra("detail", myJson);
                    startActivity(mIntent);
                }
            });



            return convertView;
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                .putString(getResources().getString(R.string.pref_dayoff_start), "")
                .putString(getResources().getString(R.string.pref_dayoff_end), "")
                .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                .putString(getResources().getString(R.string.pref_dayoff_end_shadow), "")
                /* .putString(getResources().getString(R.string.pref_dayoff_status), "0")*/
                .apply();
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        super.onBackPressed();
    }


    private void fetchDetailSummary() {
        try {

            //  recyclerView.setVisibility(View.GONE);

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("page", pageNumber.toString());
            data.put("size", getResources().getString(R.string.count_paging));
            data.put("Grab", "approval");

         /*   String nur = (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                    getResources().getString(R.string.pref_dayoff_search), ""));
               Toast.makeText(getApplicationContext(), nur, Toast.LENGTH_SHORT).show();*/

          /*  if (!PreferenceManager.getDefaultSharedPreferences(EmployeActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty())
                data.put("start", PreferenceManager.getDefaultSharedPreferences(EmployeActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));*/

         /*   if (!PreferenceManager.getDefaultSharedPreferences(EmployeActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_end), "").isEmpty())
                data.put("end", PreferenceManager.getDefaultSharedPreferences(EmployeActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_end), ""));*/

            if (!PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).getString(
                    getResources().getString(R.string.pref_dayoff_search), "").isEmpty())
                data.put("search", PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).getString(
                        getResources().getString(R.string.pref_dayoff_search), ""));

            Log.v(TAG, data.toString());
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployee> call = api.getEmployeeActivity(Authorization, data);


            call.enqueue(new Callback<ResponseDataEmployee>() {
                @Override
                public void onResponse(Call<ResponseDataEmployee> call, Response<ResponseDataEmployee> response) {


                    if (response.isSuccessful()) {
                        if (pageNumber != 1) {

                            try {
                                results.remove(results.size() - 1);
                                scrollPosition = results.size();
                            } catch (Exception e){

                            }
                            cutiAdapter.notifyItemRemoved(scrollPosition);
                        }
                        recyclerView.setVisibility(View.VISIBLE);
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            try {


                                if (response.body().getData().size() > 0) {

                                    results.addAll(response.body().getData());
                                    cutiAdapter.notifyDataSetChanged();
                                    pageNumber = pageNumber + 1;
                                    isLoading = false;
                                    loading.dismiss();
                                    txtEmptyList.setVisibility(View.GONE);

                                } else {
                                    if (results.size() > 0){

                                    }else {
                                        txtEmptyList.setText(getResources().getString(R.string.title_tidak_ada_data));
                                        txtEmptyList.setVisibility(View.VISIBLE);
                                    }

                                }

                                //  cutiAdapter.notifyDataSetChanged();
                                loading.dismiss();
                            } catch (Exception e) {
                                loading.dismiss();
                                try {
                                    MDToast.makeText(EmployeeAktivitasActivity.this, e.toString(),
                                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                    // Toast.makeText(EmployeActivity.this, R.string.loading_error, Toast.LENGTH_SHORT).show();
                                }
                                catch (Exception ex)
                                {

                                }

                            }

                        } else {
                            if (pageNumber != 1) {
                                try {
                                    results.remove(results.size() - 1);
                                    scrollPosition = results.size();
                                } catch (Exception e){

                                }
                                cutiAdapter.notifyItemRemoved(scrollPosition);
                            }

                            try {
                                MDToast.makeText(EmployeeAktivitasActivity.this, getResources().getString(R.string.loading_error),
                                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                                // Toast.makeText(EmployeActivity.this, R.string.loading_error, Toast.LENGTH_SHORT).show();
                            }
                            catch (Exception ex)
                            {

                            }
                            loading.dismiss();
                            isLoading = false;


                        }
                    }
                    else {
                        if (pageNumber != 1) {
                            try {
                                results.remove(results.size() - 1);
                                scrollPosition = results.size();
                            } catch (Exception e){

                            }
                            cutiAdapter.notifyItemRemoved(scrollPosition);
                        }
                        loading.dismiss();
                        try {
                            MDToast.makeText(EmployeeAktivitasActivity.this, getResources().getString(R.string.loading_error),
                                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                            // Toast.makeText(EmployeActivity.this, R.string.loading_error, Toast.LENGTH_SHORT).show();
                        }
                        catch (Exception ex)
                        {

                        }
                        isLoading = false;
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataEmployee> call, Throwable t) {
                    if (pageNumber != 1) {
                        try {
                            results.remove(results.size() - 1);
                            scrollPosition = results.size();
                        } catch (Exception e){

                        }
                        cutiAdapter.notifyItemRemoved(scrollPosition);
                    }
                    loading.dismiss();
                    recyclerView.setVisibility(View.VISIBLE);
                    try {
                        MDToast.makeText(EmployeeAktivitasActivity.this, getResources().getString(R.string.loading_error),
                                MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                        // Toast.makeText(EmployeActivity.this, R.string.loading_error, Toast.LENGTH_SHORT).show();
                    }
                    catch (Exception ex)
                    {

                    }
                    isLoading = false;
                }
            });
        } catch (Exception e) {
            if (pageNumber != 1) {
                try {
                    results.remove(results.size() - 1);
                    scrollPosition = results.size();
                } catch (Exception s){

                }
                cutiAdapter.notifyItemRemoved(scrollPosition);
            }
            loading.dismiss();
            recyclerView.setVisibility(View.VISIBLE);
            try {
                MDToast.makeText(EmployeeAktivitasActivity.this, getResources().getString(R.string.loading_error),
                        MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                // Toast.makeText(EmployeActivity.this, R.string.loading_error, Toast.LENGTH_SHORT).show();
            }
            catch (Exception ex)
            {

            }
            isLoading = false;
        }

    }





    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.material_design_android_floating_action_menu) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateCutiFragment dialogFragment = new CreateCutiFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        } else if (v.getId() == R.id.fab) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateCutiFragment dialogFragment = new CreateCutiFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        }
    }

    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        //  Toast.makeText(getBaseContext(), "search "+ selectedValue, Toast.LENGTH_LONG).show();



        String approval = (PreferenceManager.getDefaultSharedPreferences(EmployeeAktivitasActivity.this).getString(
                getResources().getString(R.string.pref_CanViewActivity), ""));

        if (approval.equals("true")) {

            loading.show();
            pageNumber = 1;
            results = new ArrayList<>();
            cutiAdapter = new EmployeeAktIvitasAdapter(results, EmployeeAktivitasActivity.this);
            recyclerView.setAdapter(cutiAdapter);
            swiperefresh.setRefreshing(false);
            fetchDetailSummary();

        } else {

            txtEmptyList.setVisibility(View.VISIBLE);
            txtEmptyList.setText(getResources().getString(R.string.title_tidak_ada_hak_akses));
            loading.dismiss();
        }
    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}
