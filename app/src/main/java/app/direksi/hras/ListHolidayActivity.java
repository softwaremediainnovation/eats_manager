package app.direksi.hras;


import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;


import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import org.threeten.bp.LocalDate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.HolidayAdapter;
import app.direksi.hras.model.DataHoliday;
import app.direksi.hras.model.ResponseDataHoliday;
import app.direksi.hras.model.ResponseServerTime;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class ListHolidayActivity extends AppCompatActivity implements OnDateSelectedListener, OnMonthChangedListener {

    private MaterialCalendarView customCalendar;
 //private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("EEE, d MMM yyyy");
    RecyclerView recyclerView;
    private HolidayAdapter holidayAdapter;
    private List<DataHoliday> results = new ArrayList<>();
    private List<DataHoliday> resultsBehind = new ArrayList<>();
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ArrayList<CalendarDay> datess = new ArrayList<>();
    private ArrayList<CalendarDay> datessDot = new ArrayList<>();
    private TextView txtEmptyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(ListHolidayActivity.this);

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.holiday_day));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        setContentView(R.layout.activity_holiday);
        txtEmptyList = (TextView)findViewById(R.id.txtEmptyList);
        txtEmptyList.setVisibility(View.GONE);
        recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
        customCalendar = (MaterialCalendarView) findViewById(R.id.calendarView);
        customCalendar.setDynamicHeightEnabled(true);
        customCalendar.setOnDateChangedListener(this);
        customCalendar.setOnMonthChangedListener(this);


     //   new ApiSimulator().executeOnExecutor(Executors.newSingleThreadExecutor());


        holidayAdapter = new HolidayAdapter(results, getApplicationContext());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));

        recyclerView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(holidayAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        messageDialog = new MessageDialog();
        loading = new SweetAlertDialog(ListHolidayActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        mGetServerTim();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDateSelected(
            @NonNull MaterialCalendarView widget,
            @NonNull CalendarDay date,
            boolean selected) {
      /*  String a = selected ? String.valueOf(date.getDate()) : "No Selection";
        Toast.makeText(getApplicationContext(), a, Toast.LENGTH_LONG).show();*/

        ShowDataDay(date.getMonth(), date.getDay());
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
       // Toast.makeText(getApplicationContext(),String.valueOf(date.getMonth()), Toast.LENGTH_LONG).show();
        ShowDataMonth (date.getMonth());
    }


    private class ApiSimulator extends AsyncTask<Void, Void, List<CalendarDay>> {

        @Override
        protected List<CalendarDay> doInBackground(@NonNull Void... voids) {
           /* try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            LocalDate temp = LocalDate.now().minusMonths(2);
            final ArrayList<CalendarDay> dates = new ArrayList<>();
            for (int i = 0; i < 30; i++) {
                final CalendarDay day = CalendarDay.from(temp);
                dates.add(day);
                temp = temp.plusDays(5);
            }

            return dates;
        }

        @Override
        protected void onPostExecute(@NonNull List<CalendarDay> calendarDays) {
            super.onPostExecute(calendarDays);

            if (isFinishing()) {
                return;
            }

            customCalendar.addDecorator(new EventDecorator(Color.GREEN, calendarDays));

        }
    }

    public class EventDecorator implements DayViewDecorator {

        private int color;
        private HashSet<CalendarDay> dates;

        public EventDecorator(int color, Collection<CalendarDay> dates) {
            this.color = color;
            this.dates = new HashSet<>(dates);
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            return dates.contains(day);
        }

        @Override
        public void decorate(DayViewFacade view) {
            view.addSpan(new DotSpan(5, color));
        }
    }


    public void DetailHolidayList(String dateServer) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(ListHolidayActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String id = (PreferenceManager.getDefaultSharedPreferences(ListHolidayActivity.this).getString(
                    getResources().getString(R.string.pref_employeeid_view), ""));

            Map<String, String> data = new HashMap<>();
            data.put("id", id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataHoliday> call = api.getListHoliday(Authorization, data);

            call.enqueue(new Callback<ResponseDataHoliday>() {
                @Override
                public void onResponse(Call<ResponseDataHoliday> call, Response<ResponseDataHoliday> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            resultsBehind.addAll(response.body().getData());



                            for (int i = 0; i < response.body().getData().size(); i++) {


                                if (response.body().getData().get(i).getIsHoliday()) {

                                    try {
                                        String dateString = DefaultFormatter.changeFormatDateYearMonthDay(response.body().getData().get(i).getTanggal());
                                        LocalDate localDate = LocalDate.parse(dateString);
                                        final CalendarDay day = CalendarDay.from(localDate);
                                        datess.add(day);
                                        /*customCalendar.setSelectedDate(day);*/

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    try {
                                        String dateString = DefaultFormatter.changeFormatDateYearMonthDay(response.body().getData().get(i).getTanggal());
                                        LocalDate localDate = LocalDate.parse(dateString);
                                        final CalendarDay day = CalendarDay.from(localDate);
                                        datessDot.add(day);
                                        /*customCalendar.setSelectedDate(day);*/

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }


                            if (response.body().getData().size()> 0){
                                String dateString = DefaultFormatter.changeFormatDateYearMonthDay(dateServer);
                                LocalDate localDate = LocalDate.parse( dateString );
                                final CalendarDay day = CalendarDay.from(localDate);
                                customCalendar.setSelectedDate(day);

                                DrawCalendar(datess, resultsBehind);
                              //  DrawDot(datessDot);

                            } else{

                            }

                        }
                        else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(ListHolidayActivity.this,getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.dismiss();
                        messageDialog.mShowMessageError(ListHolidayActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataHoliday> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(ListHolidayActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(ListHolidayActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }
    }

    private void DrawDot (ArrayList<CalendarDay> calendar){
        customCalendar.addDecorator(new EventDecorator(getResources().getColor(R.color.colorThemeGreenStatus), calendar));
    }

    private void DrawCalendar (ArrayList<CalendarDay> calendar , List<DataHoliday> holiday){


        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Calendar calStart  = Calendar.getInstance();
        Calendar calEnd  = Calendar.getInstance();
        try{
            calStart.setTime(df.parse(holiday.get(0).getTanggal()));
            calEnd.setTime(df.parse(holiday.get(holiday.size() - 1).getTanggal()));
        } catch (Exception e){

        }



        customCalendar.state().edit()

                .setMinimumDate(CalendarDay.from(calStart.get(Calendar.YEAR), calStart.get(Calendar.MONTH) + 1, calStart.get(Calendar.DAY_OF_MONTH)))
                .setMaximumDate(CalendarDay.from(calEnd.get(Calendar.YEAR), calEnd.get(Calendar.MONTH) + 1, calEnd.get(Calendar.DAY_OF_MONTH)))
                .setCalendarDisplayMode(CalendarMode.MONTHS)
                .commit();



        customCalendar.addDecorator(new DayViewDecorator() {
            @Override
            public boolean shouldDecorate(CalendarDay day) {

                if (calendar.contains(day)){
                    return true;
                }else {
                    return false;
                }

            }

            @Override
            public void decorate(DayViewFacade view) {
                // add red foreground span
                view.addSpan(new ForegroundColorSpan(
                        ContextCompat.getColor(ListHolidayActivity.this, R.color.colorThemeRedStatus)));
            }
        });


    }

    private void ShowDataMonth ( int bulan){

        results = new ArrayList<>();
        holidayAdapter = new HolidayAdapter(results, getApplicationContext());
        for (int i = 0; i < resultsBehind.size(); i++){

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Calendar cal  = Calendar.getInstance();
            try{
                cal.setTime(df.parse(resultsBehind.get(i).getTanggal()));
            } catch (Exception e){

            }

            if (cal.get(Calendar.MONTH) + 1 == bulan){
                results.add(resultsBehind.get(i));
            }


        }
        if (results.size() > 0){
            txtEmptyList.setVisibility(View.GONE);
        } else {
            txtEmptyList.setVisibility(View.VISIBLE);
        }
        recyclerView.setAdapter(holidayAdapter);
        holidayAdapter.notifyDataSetChanged();

    }

    private void ShowDataDay ( int bulan, int day){

        results = new ArrayList<>();
        holidayAdapter = new HolidayAdapter(results, getApplicationContext());
        for (int i = 0; i < resultsBehind.size(); i++){

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            Calendar cal  = Calendar.getInstance();
            try{
                cal.setTime(df.parse(resultsBehind.get(i).getTanggal()));
            } catch (Exception e){

            }

            if (cal.get(Calendar.MONTH) + 1 == bulan && cal.get(Calendar.DAY_OF_MONTH) == day){
                results.add(resultsBehind.get(i));
            }


        }
        if (results.size() > 0){
            txtEmptyList.setVisibility(View.GONE);
        } else {
            txtEmptyList.setVisibility(View.VISIBLE);
        }
        recyclerView.setAdapter(holidayAdapter);
        holidayAdapter.notifyDataSetChanged();

    }


    private void mGetServerTim() {
        loading.show();
        try {


            String Authorization = (PreferenceManager.getDefaultSharedPreferences(ListHolidayActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseServerTime> call = api.getServerTime(Authorization);


            call.enqueue(new Callback<ResponseServerTime>() {


                @Override
                public void onResponse(Call<ResponseServerTime> call, Response<ResponseServerTime> response) {

                    try {

                        Long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            DetailHolidayList(response.body().getData());




                        } else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(ListHolidayActivity.this, getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        }

                    } catch (Exception e) {
                        loading.dismiss();
                        messageDialog.mShowMessageError(ListHolidayActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }

                }

                @Override
                public void onFailure(Call<ResponseServerTime> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(ListHolidayActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(ListHolidayActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }

    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(ListHolidayActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}
