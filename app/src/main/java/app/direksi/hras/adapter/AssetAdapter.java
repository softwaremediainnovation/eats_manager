package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataAset;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class AssetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataAset> mItemList;
    private Context context;


    public AssetAdapter(List<DataAset> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_aset, parent, false);
            return new AssetAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new AssetAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof AssetAdapter.ItemViewHolder) {

            populateItemRows((AssetAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof AssetAdapter.LoadingViewHolder) {
            showLoadingView((AssetAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, txtLastLocation;
        public ImageView lineSharp, imageCarDisplay, imageDetail;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            mTxtJudul = (TextView) itemView.findViewById(R.id.mTxtJudul);
            txtLastLocation = (TextView) itemView.findViewById(R.id.txtLastLocation);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(AssetAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(AssetAdapter.ItemViewHolder holder, int position) {

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataAset data = mItemList.get(position);
        holder.mTxtJudul.setText(String.valueOf(data.getName() != null ? data.getName(): "-"));
        holder.txtLastLocation.setText(String.valueOf(data.getLastLocation() != null ? data.getLastLocation(): "-"));

    }


}