package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataHoliday;
import app.direksi.hras.util.DefaultFormatter;

public class HolidayAdapter extends RecyclerView.Adapter<HolidayAdapter.MyViewHolder> {

    private List<DataHoliday> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtNama, txtTanggal;
        public View vwdot;



        public MyViewHolder(View view) {
            super(view);

            txtTanggal = view.findViewById(R.id.txtTanggal);
            txtNama = view.findViewById(R.id.txtNama);
            vwdot = view.findViewById(R.id.vwdot);

        }
    }


    public HolidayAdapter(List<DataHoliday> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public HolidayAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_holidays, parent, false);

        return new HolidayAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HolidayAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataHoliday data = dataList.get(position);


        if (data.getTanggal() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithOutHourFullNameMonth(data.getTanggal()));

        if (data.getIsHoliday()){
            holder.txtTanggal.setTextColor( ContextCompat.getColor(context, R.color.colorRed));
            holder.txtNama.setTextColor( ContextCompat.getColor(context, R.color.colorRed));
            holder.txtNama.setText(data.getKeterangan());
            holder.vwdot.setBackgroundResource(R.drawable.circle_calendar_red);
        }else {
            holder.txtTanggal.setTextColor( ContextCompat.getColor(context, R.color.color_black));
            holder.txtNama.setTextColor( ContextCompat.getColor(context, R.color.color_black));
            holder.txtNama.setText(data.getKeterangan() + " " + data.getJamMasuk() + " " + context.getResources().getString(R.string.title_sampai_dengan) + " " + data.getJamPulang());
            holder.vwdot.setBackgroundResource(R.drawable.circle_calendar_green);
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}



