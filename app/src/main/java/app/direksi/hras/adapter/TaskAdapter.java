package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataTask;
import app.direksi.hras.util.DefaultFormatter;

public class TaskAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataTask> mItemList;
    private Context context;


    public TaskAdapter(List<DataTask> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task, parent, false);
            return new TaskAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new TaskAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof TaskAdapter.ItemViewHolder) {

            populateItemRows((TaskAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof TaskAdapter.LoadingViewHolder) {
            showLoadingView((TaskAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, txtOrganisasi, mTxtStatus, txtTgl, txtDesc, txtCreator,txtExpired ;
        public ImageView mIconStatus;
        public LinearLayout llExpired;

        public ConstraintLayout mMainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtCreator = itemView.findViewById(R.id.txtCreator);
            txtExpired = itemView.findViewById(R.id.txtExpired);
            llExpired = itemView.findViewById(R.id.llExpired);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            txtTgl = itemView.findViewById(R.id.txtTgl);
            txtDesc = itemView.findViewById(R.id.txtDesc);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(TaskAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(TaskAdapter.ItemViewHolder holder, int position) {

        DataTask data = mItemList.get(position);

        if (data.getDateCreated() != null)
            holder.txtTgl.setText(DefaultFormatter.changeFormatDate(data.getDateCreated()));

        if (!data.getDueDate().contains("0001")) {
            holder.txtExpired.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getDueDate()));
            holder.llExpired.setVisibility(View.VISIBLE);
        } else {
            holder.llExpired.setVisibility(View.GONE);
        }

        holder.mTxtJudul.setText(data.getEmployeeName());
        holder.txtOrganisasi.setText(data.getOrganization());
        holder.txtDesc.setText(data.getTaskName());
        holder.txtCreator.setText(data.getCreatorName());


        if (!data.isIsExpired()) {

            if (!data.getFlag().equals("daily")) {


                if (data.isIsFinish()) {

                    holder.mTxtStatus.setText(context.getResources().getString(R.string.title_selesai));
                    holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                    holder.mTxtStatus.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));

                } else {

                    holder.mTxtStatus.setText(context.getResources().getString(R.string.title_belum_selesai));
                    holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_orange);
                    holder.mTxtStatus.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));

                }
            } else {

                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_terjadwal));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_primary);
                holder.mTxtStatus.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
            }
        } else {


                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_kadaluarsa));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));



        }




    }


}
