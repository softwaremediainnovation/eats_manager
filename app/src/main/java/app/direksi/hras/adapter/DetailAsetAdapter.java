package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataDetailAssetsAssetTracking;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 10/04/2019.
 */

public class DetailAsetAdapter  extends RecyclerView.Adapter<DetailAsetAdapter.MyViewHolder> {

    private List<DataDetailAssetsAssetTracking> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal, txtName, txtNote, txtID;
        public ImageView profile_image;


        public MyViewHolder(View view) {
            super(view);

            txtID = (TextView) view.findViewById(R.id.txtID);
            txtNote = (TextView) view.findViewById(R.id.txtNote);
            txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
            txtName = (TextView) view.findViewById(R.id.txtName);
            profile_image = (ImageView) view.findViewById(R.id.profile_image);

        }
    }


    public DetailAsetAdapter(List<DataDetailAssetsAssetTracking> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public DetailAsetAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_flow, parent, false);

        return new DetailAsetAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailAsetAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataDetailAssetsAssetTracking data = dataList.get(position);
        holder.txtTanggal.setText(String.valueOf(data.getDate() != null ? DefaultFormatter.changeFormatDate(data.getDate()): "-"));
        holder.txtName.setText(String.valueOf(data.getDestinationHolder().getFirstName()  != null ?" " + context.getResources().getString(R.string.title_aset_di) + " " +  data.getDestinationHolder().getFirstName() + " " + data.getDestinationHolder().getLastName() : "-"));
        holder.txtNote.setText(String.valueOf(data.getNote()  != null ? data.getNote() : ""));
        holder.txtID.setText(String.valueOf(data.getAssetTrackingID()  != null ? "No. Tracking : " + data.getAssetTrackingID() : ""));


        if (data.getIsAccepted()== true){
            holder.profile_image.setBackgroundResource(R.drawable.status_accept);
        }else {
            holder.profile_image.setBackgroundResource(R.drawable.status_pending);

        }


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

