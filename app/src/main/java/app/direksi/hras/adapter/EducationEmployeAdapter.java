package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataEducationEmploye;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class EducationEmployeAdapter extends RecyclerView.Adapter<EducationEmployeAdapter.MyViewHolder> {

    private List<DataEducationEmploye> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPerusahaan, mTxtStatus, txtTingkat, txtJurusan, txtIPK, txtTanggal, txtAlamat;
        public ImageView lineSharp, imageCarDisplay, imageDetail;


        public MyViewHolder(View view) {
            super(view);

            txtPerusahaan = (TextView) view.findViewById(R.id.txtPerusahaan);
            mTxtStatus = (TextView) view.findViewById(R.id.mTxtStatus);
            txtTingkat = (TextView) view.findViewById(R.id.txtTingkat);
            txtJurusan = (TextView) view.findViewById(R.id.txtJurusan);
            txtIPK = (TextView) view.findViewById(R.id.txtIPK);
            txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
            txtAlamat = (TextView) view.findViewById(R.id.txtAlamat);


        }
    }


    public EducationEmployeAdapter(List<DataEducationEmploye> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public EducationEmployeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_education_employe, parent, false);

        return new EducationEmployeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(EducationEmployeAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataEducationEmploye data = dataList.get(position);
        holder.txtPerusahaan.setText(String.valueOf(data.getInstituteName() != null ? data.getInstituteName(): "-"));
        holder.txtTingkat.setText(String.valueOf(data.getEducationLevel().getName() != null ? data.getEducationLevel().getName(): "-"));
        holder.txtJurusan.setText(String.valueOf(data.getMajor().getName() != null ? data.getMajor().getName() : "-"));
        holder.txtIPK.setText(String.valueOf(data.getScore() != null ? data.getScore(): "-"));
        holder.txtAlamat.setText(String.valueOf(data.getAddress() != null ? data.getAddress(): "-"));

        Date sDate, eDate;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatParse = new SimpleDateFormat("dd MMM yy");

        try {

            if (data.getDateStart()!= null) {
                sDate = parser.parse(data.getDateStart());
                // holder.txtTanggal.setText(formatParse.format(sDate));

                if (data.getDateEnd()!= null) {
                    eDate = parser.parse(data.getDateEnd());
                    holder.txtTanggal.setText(formatParse.format(sDate) + " " + context.getResources().getString(R.string.title_sampai_dengan) +  " " +formatParse.format(eDate));
                }
                else
                {
                    holder.txtTanggal.setText("-");
                }
            }
            else
            {
                holder.txtTanggal.setText("-");
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (data.getIsGraduate()) {

            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_lulus));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));
        }
        else
        {
            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_tidak_lulus));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));
        }






    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

