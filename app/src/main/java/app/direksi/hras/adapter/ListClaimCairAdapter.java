package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataClaim;
import app.direksi.hras.util.DefaultFormatter;

public class ListClaimCairAdapter extends RecyclerView.Adapter<ListClaimCairAdapter.MyViewHolder> {

    private List<DataClaim> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public CheckBox cbFinish;
        public  LinearLayout llList;
        public TextView txtJumlah, txtTanggal, txtKeperluan, txtCreator, txtCatatan, txtID;
        public TableRow trNote, trID;

        public MyViewHolder(View view) {
            super(view);

            cbFinish = itemView.findViewById(R.id.cbFinish);
            llList = itemView.findViewById(R.id.llList);

            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            txtKeperluan = itemView.findViewById(R.id.txtKeperluan);
            txtCreator = itemView.findViewById(R.id.txtCreator);
            trNote = itemView.findViewById(R.id.trNote);
            txtCatatan = itemView.findViewById(R.id.txtCatatan);
            trID = itemView.findViewById(R.id.trID);
            txtID = itemView.findViewById(R.id.txtID);

        }
    }


    public ListClaimCairAdapter(List<DataClaim> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public ListClaimCairAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_claim_approved, parent, false);

        return new ListClaimCairAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ListClaimCairAdapter.MyViewHolder holder, int position) {
        DataClaim data = dataList.get(position);

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        holder.txtJumlah.setText(String.valueOf(data.getAmount() != null ? context.getResources().getString(R.string.title_rp) + " " + formatter.format(data.getAmount()) : "-"));
        holder.txtCatatan.setText(data.getNote()!=null ? data.getNote() : "");
        holder.txtID.setText(data.getBuktiKasKeluarDetailID()!=null ? data.getBuktiKasKeluarDetailID() : "");
        if (data.getDate() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDate()));

        holder.txtKeperluan.setText(data.getReimbursementCategory().getName()!=null ? data.getReimbursementCategory().getName() : "");

        holder.txtCreator.setText(data.getApprover().getFirstName() + " " + data.getApprover().getLastName());
        holder.cbFinish.setVisibility(View.GONE);
        holder.trNote.setVisibility(View.VISIBLE);
        holder.trID.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
