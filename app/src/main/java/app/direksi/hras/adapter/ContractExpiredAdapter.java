package app.direksi.hras.adapter;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import app.direksi.hras.BeriPenilaianActivity;
import app.direksi.hras.DetailEmployeActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataContractExpired;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 27/08/2019.
 */

public class ContractExpiredAdapter extends RecyclerView.Adapter<ContractExpiredAdapter.MyViewHolder> {

    private List<DataContractExpired> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtJudul , mTxtStatus, txtTanggal, txtOrganisasi, txtCompany;
        public ConstraintLayout mMainLayout;
        // public ImageView mIconStatus;
        public ImageView mIconStatus;
        public Button btnBeriPenilaian;

        public MyViewHolder(View view) {
            super(view);

            btnBeriPenilaian = view.findViewById(R.id.btnBeriPenilaian);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            txtTanggal = view.findViewById(R.id.txtTanggal);
            txtOrganisasi = view.findViewById(R.id.txtOrganisasi);
            txtCompany = view.findViewById(R.id.txtCompany);
            mMainLayout = view.findViewById(R.id.mMainLayout);



        }
    }


    public ContractExpiredAdapter(List<DataContractExpired> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public ContractExpiredAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contract_expired, parent, false);

        return new ContractExpiredAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContractExpiredAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataContractExpired data = dataList.get(position);
        holder.mTxtJudul.setText(data.getEmployeeName() != null? data.getEmployeeName() : "");
        holder.txtOrganisasi.setText(data.getOrganization() != null? data.getOrganization() : "");
        holder.txtCompany.setText(data.getCompany() != null? data.getCompany() : "");

        if (data.getContractExpiredDate() != null){

            Date orderDate = null;
            Calendar call = null, today = Calendar.getInstance();
            String dateString = "";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");

            try {
                orderDate = sdf.parse(data.getDob());
                call = Calendar.getInstance();
                call.setTime(orderDate);
                holder.mTxtStatus.setText(String.valueOf(today.get(Calendar.YEAR) - call.get(Calendar.YEAR)) + " " + context.getResources().getString(R.string.title_tahun));
                holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getContractExpiredDate()));
            } catch (ParseException e) {
                e.printStackTrace();
            }



        }

        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DataEmployee dataEmployee = new DataEmployee(data.getEmployeeId(),data.getMfirstName(),data.getMlastName());


                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(context.getResources().getString(R.string.pref_employeeid_view), Long.toString(data.getEmployeeId()))
                        .apply();
                Intent mIntent = new Intent(context, DetailEmployeActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Gson gson = new Gson();
                String myJson = gson.toJson(dataEmployee);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });

        holder.btnBeriPenilaian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DataEmployee dataEmployee = new DataEmployee(data.getEmployeeId(),data.getMfirstName(),data.getMlastName());


                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(context.getResources().getString(R.string.pref_employeeid_view), Long.toString(data.getEmployeeId()))
                        .apply();
                Intent mIntent = new Intent(context, BeriPenilaianActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Gson gson = new Gson();
                String myJson = gson.toJson(dataEmployee);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });








    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}


