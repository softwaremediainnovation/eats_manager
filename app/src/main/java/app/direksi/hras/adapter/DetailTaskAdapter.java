package app.direksi.hras.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataTaskIDDetailItem;

public class DetailTaskAdapter extends RecyclerView.Adapter<DetailTaskAdapter.MyViewHolder> {

    private List<DataTaskIDDetailItem> dataList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public CheckBox cbFinish;
        public TextView txtNomer, txtNote;

        public MyViewHolder(View view) {
            super(view);

            cbFinish = view.findViewById(R.id.cbFinish);
            txtNomer = view.findViewById(R.id.txtNomer);
            txtNote = view.findViewById(R.id.txtNote);

        }
    }


    public DetailTaskAdapter(List<DataTaskIDDetailItem> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public DetailTaskAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_task, parent, false);

        return new DetailTaskAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailTaskAdapter.MyViewHolder holder, int position) {

        DataTaskIDDetailItem data = dataList.get(position);

        holder.txtNote.setText(data.getTaskName());

        if (data.isIsFinish()==true){

            holder.cbFinish.setChecked(true);
           /* holder.txtNote.setPaintFlags(holder.txtNote.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.txtNote.setTypeface(holder.txtNote.getTypeface(), Typeface.ITALIC);*/

        }else {

            holder.cbFinish.setChecked(false);

        }

        int newwPosition = holder.getAdapterPosition();
        int temp = newwPosition + 1;
        holder.txtNomer.setText( String.valueOf(temp) + "." );

        holder.cbFinish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {

            if (isChecked){
                holder.cbFinish.setChecked(false);
                holder.cbFinish.jumpDrawablesToCurrentState();

            }else {
                holder.cbFinish.setChecked(true);
                holder.cbFinish.jumpDrawablesToCurrentState();
            }

          }
        }
        );






    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}


