package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TableRow;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataTrackingAset;

/**
 * Created by dhimaz on 23/05/2019.
 */

public class TrackingAsetAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataTrackingAset> mItemList;
    private Context context;


    public TrackingAsetAdapter(List<DataTrackingAset> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_aset, parent, false);
            return new TrackingAsetAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new TrackingAsetAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof TrackingAsetAdapter.ItemViewHolder) {

            populateItemRows((TrackingAsetAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof TrackingAsetAdapter.LoadingViewHolder) {
            showLoadingView((TrackingAsetAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, txtLastLocation, txtDate, txtKategori;
        public ImageView lineSharp, imageCarDisplay, imageDetail;
        public TableRow trExpired;
        public ImageView mIconStatus;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            mTxtJudul = (TextView) itemView.findViewById(R.id.mTxtJudul);
            txtLastLocation = (TextView) itemView.findViewById(R.id.txtLastLocation);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);
            trExpired = (TableRow) itemView.findViewById(R.id.trExpired);
            txtKategori = (TextView) itemView.findViewById(R.id.txtKategori);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(TrackingAsetAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(TrackingAsetAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataTrackingAset data = mItemList.get(position);
        holder.mTxtJudul.setText(String.valueOf(data.getName() != null ? data.getName(): ""));
        holder.txtKategori.setText(String.valueOf(data.getConstanta().getName() != null ? data.getConstanta().getName(): ""));
        holder.txtLastLocation.setText(String.valueOf(data.getLastLocation() != null ? data.getLastLocation(): ""));

        Date sDate, eDate;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatParse = new SimpleDateFormat("dd MMM yyyy");

        try {

            if (data.getExpiredDate()!= null) {
                sDate = parser.parse(data.getExpiredDate());
                holder.txtDate.setText(formatParse.format(sDate));
                holder.trExpired.setVisibility(View.VISIBLE);


            }
            else
            {
                holder.txtDate.setText("-");
                holder.trExpired.setVisibility(View.GONE);
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (data.getPending() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));


        }
        else {

            if (data.getPending()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));


            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));


            }

        }

    }


}