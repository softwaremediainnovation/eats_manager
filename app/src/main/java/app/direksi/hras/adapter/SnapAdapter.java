package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.ResultItem;

/**
 * Created by dhimaz on 02/08/2019.
 */

public class SnapAdapter extends RecyclerView.Adapter<SnapAdapter.MyViewHolder> {

    private List<ResultItem> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtName ;

        public MyViewHolder(View view) {
            super(view);

            mTxtName = view.findViewById(R.id.mTxtName);



        }
    }


    public SnapAdapter(List<ResultItem> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public SnapAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_snap, parent, false);

        return new SnapAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SnapAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        ResultItem data = dataList.get(position);


        holder.mTxtName.setText(data.getFileName());








    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

