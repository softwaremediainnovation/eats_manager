package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DetailAsetHistory;

/**
 * Created by dhimaz on 11/04/2019.
 */

public class DetailStrukturAdapter extends RecyclerView.Adapter<DetailStrukturAdapter.MyViewHolder> {

    private List<DetailAsetHistory> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal, txtName;
        public ImageView profile_image;


        public MyViewHolder(View view) {
            super(view);

           /* txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
            txtName = (TextView) view.findViewById(R.id.txtName);
            profile_image = (ImageView) view.findViewById(R.id.profile_image);*/



        }
    }


    public DetailStrukturAdapter(List<DetailAsetHistory> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public DetailStrukturAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_struktur, parent, false);

        return new DetailStrukturAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailStrukturAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DetailAsetHistory data = dataList.get(position);
      /*  holder.txtTanggal.setText(String.valueOf(data.getDate() != null ? DefaultFormatter.changeFormatDateWithOutHour(data.getDate()): "-"));
        holder.txtName.setText(String.valueOf(data.getEmployee() != null ? data.getEmployee(): "-"));

        if (position == 0){
            holder.profile_image.setBackgroundResource(R.drawable.ic_flow_1);
        }else {
            holder.profile_image.setBackgroundResource(R.drawable.ic_flow_2);

        }*/







    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

