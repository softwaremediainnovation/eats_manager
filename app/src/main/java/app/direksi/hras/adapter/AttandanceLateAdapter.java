package app.direksi.hras.adapter;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.DetailAttendancesActivity;
import app.direksi.hras.EmployeActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 05/09/2019.
 */

public class AttandanceLateAdapter extends RecyclerView.Adapter<AttandanceLateAdapter.MyViewHolder> {

    private List<DataAttendance> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal , mTxtJudul, mTxtStatus, txtID, txtTerlambat, txtVia, txtAbsen;
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            txtAbsen = view.findViewById(R.id.txtAbsen);
            txtVia = view.findViewById(R.id.txtVia);
            txtID = view.findViewById(R.id.txtID);
            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            txtTanggal = view.findViewById(R.id.txtTanggal);
            mMainLayout = view.findViewById(R.id.mMainLayout);
            mIconStatus = view.findViewById(R.id.mIconStatus);
            txtTerlambat = view.findViewById(R.id.txtTerlambat);
            llTerlambat = view.findViewById(R.id.llTerlambat);


        }
    }


    public AttandanceLateAdapter(List<DataAttendance> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public AttandanceLateAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attendance_late, parent, false);

        return new AttandanceLateAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AttandanceLateAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataAttendance data = dataList.get(position);

        if (data.getIsPhotoApproved() == null){

            holder.mTxtStatus.setVisibility(View.GONE);

        } else {

            if (data.getIsPhotoApproved()){
                holder.mTxtStatus.setVisibility(View.GONE);
            } else {
                holder.mTxtStatus.setVisibility(View.VISIBLE);
            }

        }

        holder.txtAbsen.setText(data.getInOut()!= null? data.getInOut() : "");
        holder.txtVia.setText(data.getIsMobileApp()== true? context.getResources().getString(R.string.title_aplikasi): context.getResources().getString(R.string.title_mesin_finger));
        holder.mTxtJudul.setText(data.getNama() != null? data.getNama() : "");
        holder.txtID.setText(data.getAttendanceID().toString());
        if (data.getDateTime() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithDayNameWithSecond(data.getDateTime()));


        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailAttendancesActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });

        if (data.getIsLate() == null){
            holder.llTerlambat.setVisibility(View.GONE);
          //  holder.mTxtStatus.setText("");
         //   holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
         //   holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsLate()){
                holder.llTerlambat.setVisibility(View.VISIBLE);
                try {

                    Double temp1 = data.getLateTimeInMinute() / 60;
                    Double temp2 = data.getLateTimeInMinute() % 60;
                    Double temp3 = data.getLateTimeInMinute();

                    int hour = temp1.intValue();
                    int minut = temp2.intValue();
                    int total = temp3.intValue();

                    holder.txtTerlambat.setText(hour + " " +
                            context.getResources().getString(R.string.title_jam) + " "
                            + minut + " " +
                            context.getResources().getString(R.string.title_menit)
                            +" ( " +
                            context.getResources().getString(R.string.title_total) + " "
                            + total + " " +
                            context.getResources().getString(R.string.title_menit) +" )");

                } catch (Exception e){

                }


                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
             //   holder.mTxtStatus.setText(context.getResources().getString(R.string.title_terlambat));
             //   holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
              //  holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.llTerlambat.setVisibility(View.GONE);
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
              //  holder.mTxtStatus.setText(context.getResources().getString(R.string.title_tidak_terlambat));
              //  holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
              //  holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}


