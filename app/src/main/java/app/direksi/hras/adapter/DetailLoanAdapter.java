package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DetailLoanWithDetailResultPaymentItem;
import app.direksi.hras.util.DefaultFormatter;

public class DetailLoanAdapter extends RecyclerView.Adapter<DetailLoanAdapter.MyViewHolder> {

    private List<DetailLoanWithDetailResultPaymentItem> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal, txtName, txtNote, txtCicilan, txtTerbayar, txtSisa, txtMetod;
        public ImageView profile_image;


        public MyViewHolder(View view) {
            super(view);

            txtMetod = (TextView) view.findViewById(R.id.txtMetod);
            txtTerbayar = (TextView) view.findViewById(R.id.txtTerbayar);
            txtSisa = (TextView) view.findViewById(R.id.txtSisa);
            txtCicilan = (TextView) view.findViewById(R.id.txtCicilan);
            txtNote = (TextView) view.findViewById(R.id.txtNote);
            txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
            txtName = (TextView) view.findViewById(R.id.txtName);
            profile_image = (ImageView) view.findViewById(R.id.profile_image);



        }
    }


    public DetailLoanAdapter(List<DetailLoanWithDetailResultPaymentItem> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public DetailLoanAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_loan, parent, false);

        return new DetailLoanAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailLoanAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DetailLoanWithDetailResultPaymentItem data = dataList.get(position);
        holder.txtTanggal.setText(String.valueOf(data.getDatePayment() != null ? DefaultFormatter.changeFormatDateWithOutHour(data.getDatePayment()): ""));
        holder.txtCicilan.setText(context.getResources().getString(R.string.title_cicilan_ke) + " " + String.valueOf(data.getInstallmentPayout()));
        holder.txtNote.setText( data.getNote()!= null ? data.getNote(): "" );
        holder.txtTerbayar.setText(String.valueOf(data.getAmount() != null ? context.getResources().getString(R.string.title_rp) + " " + formatter.format(data.getAmount()) : ""));
        holder.txtSisa.setText(String.valueOf(data.getRemainingLoan() != null ? context.getResources().getString(R.string.title_rp) +  " " + formatter.format(data.getRemainingLoan()) : ""));
        holder.txtMetod.setText(data.isIsPayroll() == true ? context.getResources().getString(R.string.title_potong_gaji): context.getResources().getString(R.string.title_bayar_manual));

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

