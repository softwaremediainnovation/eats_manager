package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataReprimand;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 07/05/2019.
 */

public class TeguranAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataReprimand> mItemList;
    private Context context;


    public TeguranAdapter(List<DataReprimand> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_teguran, parent, false);
            return new TeguranAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new TeguranAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof TeguranAdapter.ItemViewHolder) {

            populateItemRows((TeguranAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof TeguranAdapter.LoadingViewHolder) {
            showLoadingView((TeguranAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtNama, txtTanggal, txtKategori, txtOrganisasi;
        public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            txtNama = itemView.findViewById(R.id.txtNama);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            txtKategori = itemView.findViewById(R.id.txtKategori);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(TeguranAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(TeguranAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataReprimand data = mItemList.get(position);

        holder.txtNama.setText(data.getEmployee().getFirstName()!= null ? data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName() : "");
        if (data.getDate() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getDate()));

        holder.txtKategori.setText(data.getReprimandCategory().getName()!=null ? data.getReprimandCategory().getName(): "" );
        holder.txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization(): "" );


      /*  holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailTeguranActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });*/


    }


}