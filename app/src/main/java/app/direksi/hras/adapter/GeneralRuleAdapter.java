package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.EmployeeInfoGeneralRule;

/**
 * Created by dhimaz on 17/06/2019.
 */

public class GeneralRuleAdapter extends RecyclerView.Adapter<GeneralRuleAdapter.MyViewHolder> {

    private List<EmployeeInfoGeneralRule> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtRule;
         public ImageView mIconStatus1;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            txtRule = view.findViewById(R.id.txtRule);
            mIconStatus1 = view.findViewById(R.id.mIconStatus1);



        }
    }


    public GeneralRuleAdapter(List<EmployeeInfoGeneralRule> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public GeneralRuleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rule_general, parent, false);

        return new GeneralRuleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(GeneralRuleAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        EmployeeInfoGeneralRule data = dataList.get(position);

        holder.txtRule.setText(data.getPermission() != null? data.getPermission() : "");
        holder.mIconStatus1.setBackgroundResource(R.drawable.status_accept);

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}



