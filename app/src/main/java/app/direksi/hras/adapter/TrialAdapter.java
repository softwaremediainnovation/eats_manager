package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataCuti;

/**
 * Created by dhimaz on 13/05/2019.
 */

public class TrialAdapter extends RecyclerView.Adapter<TrialAdapter.MyViewHolder> {

    private List<DataCuti> dataList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
       /* public TextView mTxtJudul, mTxtStart,
                mTxtEnd, mTxtStatus,
                mTxtReason,
                mTxtNote;
        public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;*/


        public MyViewHolder(View view) {
            super(view);

       /*     mTxtStatus = view.findViewById(R.id.mTxtStatus);
            mTxtNote = view.findViewById(R.id.mTxtNote);
            mTxtReason = view.findViewById(R.id.mTxtReason);
            mTxtEnd = view.findViewById(R.id.mTxtEnd);
            mTxtStart = view.findViewById(R.id.mTxtStart);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            mMainLayout = view.findViewById(R.id.mMainLayout);
            mIconStatus = view.findViewById(R.id.mIconStatus);
*/


        }
    }


    public TrialAdapter(List<DataCuti> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public TrialAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_trial, parent, false);

        return new TrialAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TrialAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataCuti data = dataList.get(position);
/*


        holder.mTxtJudul.setText(data.getEmployeeName());
        if (data.getStart() != null)
            holder.mTxtStart.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getStart()));
        if (data.getStart() != null)
            holder.mTxtEnd.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getEnd()));
        holder.mTxtReason.setText(data.getConstantaName());
        holder.mTxtNote.setText(data.getNote());

        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences( context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailCutiActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });

        if (data.getIsApproved() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));
            holder.mTxtStatus.setText("MENUNGGU");
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsApproved()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mTxtStatus.setText("DISETUJUI");
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
                holder.mTxtStatus.setText("DITOLAK");
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }


*/





    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

