package app.direksi.hras.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import app.direksi.hras.R;
import app.direksi.hras.util.BitmapImage;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<String> list = new ArrayList<>();
    private Context context;

    public MyAdapter(Context context) {
        this.context = context;
    }

    public void addImage(ArrayList<String> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.image, parent, false);
        return new Holder(v);
    }

    //Adapter implementation
    //so this is the function that retieve data from the adapter
    public ArrayList<String> getAllItems() {
        return list;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //Uri imageUri = Uri.fromFile(new File(list.get(position)));// For files on device
        //Log.e("hello", "- " + imageUri.toString());
        File f = new File(list.get(position));
        Bitmap d = new BitmapDrawable(context.getResources(), f.getAbsolutePath()).getBitmap();
        //Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, com.fxn.utility.Utility.getExifCorrectedBitmap(f));
        Bitmap scaled = com.fxn.utility.Utility.getScaledBitmap(512, d);


        try {
            ((Holder) holder).iv.setImageBitmap(BitmapImage.modifyOrientation(scaled,list.get(position)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        // ((Holder) holder).iv.setImageURI(imageUri);

        if (position == 0)
            ((Holder) holder).mLayoutFoto.setVisibility(View.VISIBLE);

        ((Holder) holder).mImageRemove.setOnClickListener(v -> {
            list.remove(position);
            notifyDataSetChanged();
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public ImageView iv,mImageRemove;
        public LinearLayout mLayoutFoto;


        public Holder(View itemView) {
            super(itemView);
            iv = itemView.findViewById(R.id.iv);
            mImageRemove = itemView.findViewById(R.id.mImageRemove);
            mLayoutFoto = itemView.findViewById(R.id.mLayoutFoto);
        }
    }


}
