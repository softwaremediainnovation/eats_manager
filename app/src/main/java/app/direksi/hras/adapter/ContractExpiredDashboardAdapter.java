package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.EmployeeBirthdayDashboard;

/**
 * Created by dhimaz on 27/08/2019.
 */

public class ContractExpiredDashboardAdapter extends RecyclerView.Adapter<ContractExpiredDashboardAdapter.MyViewHolder> {

    private List<EmployeeBirthdayDashboard> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtJudul, mTxtStatus, txtCompany ;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            txtCompany = view.findViewById(R.id.txtCompany);



        }
    }


    public ContractExpiredDashboardAdapter(List<EmployeeBirthdayDashboard> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public ContractExpiredDashboardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_contract_expired_dashboard, parent, false);

        return new ContractExpiredDashboardAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContractExpiredDashboardAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        EmployeeBirthdayDashboard data = dataList.get(position);
        holder.mTxtJudul.setText(data.getEmployeeName() != null? data.getEmployeeName() : "");
        holder.txtCompany.setText(data.getOrganization() != null? data.getOrganization() + " - " + data.getCompany() : "");

        if (data.getDob() != null){

            Date orderDate = null;
            Calendar call = null, today = Calendar.getInstance();
            String dateString = "";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

            try {
                orderDate = sdf.parse(data.getDob());
                call = Calendar.getInstance();
                call.setTime(orderDate);
                holder.mTxtStatus.setText(String.valueOf(today.get(Calendar.YEAR) - call.get(Calendar.YEAR)) + " " +context.getResources().getString(R.string.title_tahun));
            } catch (ParseException e) {
                e.printStackTrace();
            }



        }



       /* holder.mTxtJudul.setText(data.getEmployee() != null? data.getEmployee() : "");
        holder.txtID.setText(data.getAttendanceID().toString());
        if (data.getDateTime() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithDayName(data.getDateTime()));


        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailAttendancesActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });

        if (data.getIsLate() == null){
            holder.mTxtStatus.setText("");
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsLate()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
                holder.mTxtStatus.setText("Telat");
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mTxtStatus.setText("Tidak Telat");
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }
*/






    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

