package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.model.DataRelativeEmploye;
import app.direksi.hras.R;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class RelativeEmployeAdapter extends RecyclerView.Adapter<RelativeEmployeAdapter.MyViewHolder> {

    private List<DataRelativeEmploye> spkadvList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHubungan,  txtNama, txtUmur, txtGender, txtPekerjaan;
        public ImageView lineSharp, imageCarDisplay, imageDetail;


        public MyViewHolder(View view) {
            super(view);

            txtHubungan = (TextView) view.findViewById(R.id.txtHubungan);
            txtNama = (TextView) view.findViewById(R.id.txtNama);
            txtUmur = (TextView) view.findViewById(R.id.txtUmur);
            txtGender = (TextView) view.findViewById(R.id.txtGender);
            txtPekerjaan = (TextView) view.findViewById(R.id.txtPekerjaan);


        }
    }


    public RelativeEmployeAdapter(List<DataRelativeEmploye> pnwrAdvList, Context context) {
        this.spkadvList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_relative_employe, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataRelativeEmploye data = spkadvList.get(position);
        holder.txtHubungan.setText(String.valueOf(data.getRelation() != null ? data.getRelation(): "-"));
        holder.txtNama.setText(String.valueOf(data.getName() != null ? data.getName(): "-"));
        holder.txtUmur.setText(String.valueOf(data.getAge() != null ? data.getAge() + " " + context.getResources().getString(R.string.title_tahun): "-"));
        holder.txtGender.setText(String.valueOf(data.getGender() != null ? data.getGender(): "-"));
        holder.txtPekerjaan.setText(String.valueOf(data.getJob() != null ? data.getJob(): "-"));







    }

    @Override
    public int getItemCount() {
        return spkadvList.size();
    }
}


