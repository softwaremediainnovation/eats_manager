package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataSalary;

public class GajiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataSalary> mItemList;
    private Context context;
    private static final int REQUEST_PHONE_CALL = 1;

    public GajiAdapter(List<DataSalary> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gaji, parent, false);
            return new GajiAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new GajiAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof GajiAdapter.ItemViewHolder) {

            populateItemRows((GajiAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof GajiAdapter.LoadingViewHolder) {
            showLoadingView((GajiAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtBulan, txtGaji, txtPhone, profil_role, txtOrganisasi;
        public ImageView profile_image;
        public ConstraintLayout mMainLayout;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);


            txtBulan = itemView.findViewById(R.id.txtBulan);
            txtGaji = itemView.findViewById(R.id.txtGaji);

        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(GajiAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(GajiAdapter.ItemViewHolder holder, int position) {

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataSalary data = mItemList.get(position);
        holder.txtBulan.setText(data.getName());
        holder.txtGaji.setText(String.valueOf(data.getNominal() != null ? context.getResources().getString(R.string.title_rp) + formatter.format(data.getNominal()) : context.getResources().getString(R.string.title_rp_0)));


    }




}
