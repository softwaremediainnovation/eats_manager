package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataTaskIDDetailItem;

public class DetailTaskDailyAdapter extends RecyclerView.Adapter<DetailTaskDailyAdapter.MyViewHolder> {

    private List<DataTaskIDDetailItem> dataList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;

        public TextView txtNomer, txtNote;

        public MyViewHolder(View view) {
            super(view);


            txtNomer = view.findViewById(R.id.txtNomer);
            txtNote = view.findViewById(R.id.txtNote);

        }
    }


    public DetailTaskDailyAdapter(List<DataTaskIDDetailItem> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public DetailTaskDailyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail_task_daily, parent, false);

        return new DetailTaskDailyAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DetailTaskDailyAdapter.MyViewHolder holder, int position) {

        DataTaskIDDetailItem data = dataList.get(position);

        holder.txtNote.setText(data.getTaskName());

        int newwPosition = holder.getAdapterPosition();
        int temp = newwPosition + 1;
        holder.txtNomer.setText( String.valueOf(temp) + "." );








    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}


