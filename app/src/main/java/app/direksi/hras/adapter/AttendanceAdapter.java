package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.util.DefaultFormatter;

import static app.direksi.hras.util.DefaultFormatter.isNegative;

/**
 * Created by dhimaz on 08/05/2019.
 */

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private List<DataAttendance> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal , mTxtJudul, mTxtStatus, txtID, txtTerlambat,txtVia,txtAbsen;
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            txtAbsen = view.findViewById(R.id.txtAbsen);
            txtVia = view.findViewById(R.id.txtVia);
            txtID = view.findViewById(R.id.txtID);
            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            txtTanggal = view.findViewById(R.id.txtTanggal);
            mMainLayout = view.findViewById(R.id.mMainLayout);
            mIconStatus = view.findViewById(R.id.mIconStatus);
            txtTerlambat = view.findViewById(R.id.txtTerlambat);
            llTerlambat = view.findViewById(R.id.llTerlambat);


        }
    }


    public AttendanceAdapter(List<DataAttendance> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public AttendanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attendance, parent, false);

        return new AttendanceAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AttendanceAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataAttendance data = dataList.get(position);

        if (data.getIsPhotoApproved() == null){

            holder.mTxtStatus.setVisibility(View.GONE);

        } else {

            if (data.getIsPhotoApproved()){
                holder.mTxtStatus.setVisibility(View.GONE);
            } else {
                holder.mTxtStatus.setVisibility(View.VISIBLE);
            }

        }


        String method = "";

        if (data.getIsApproved()== null){

            method = context.getResources().getString(R.string.title_pengajuan_kehadiran);

        } else {

            if (data.getIsApproved().toLowerCase().equals("true")){

                if (data.getApprover() == null){
                    String row = data.getIsMobileApp()== true? context.getResources().getString(R.string.title_aplikasi): context.getResources().getString(R.string.title_mesin_finger);
                    method = row ;

                } else {
                    method = context.getResources().getString(R.string.title_pengajuan_kehadiran);
                }

            } else{
                method = context.getResources().getString(R.string.title_pengajuan_kehadiran);
            }

        }

        holder.txtVia.setText(method);

        holder.txtAbsen.setText(data.getInOut()!= null? data.getInOut() : "");
        holder.mTxtJudul.setText(data.getNama() != null? data.getNama() : "");
        holder.txtID.setText( formatter.format (data.getAttendanceID()));

        String isnegative = "+";
        if (isNegative(data.getDateGMT())){
            isnegative = "";
        }

        if (data.getDateTime() != null)
            holder.txtTanggal.setText( (DefaultFormatter.changeFormatDateWithDayName(data.getDateTime())) + " GMT" + isnegative + String.valueOf(data.getDateGMT()));


        if (data.getIsLate() == null){
            holder.llTerlambat.setVisibility(View.GONE);
         //   holder.mTxtStatus.setText("");
         //   holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
         //   holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsLate()){
                holder.llTerlambat.setVisibility(View.VISIBLE);
                try {

                    Double temp1 = data.getLateTimeInMinute() / 60;
                    Double temp2 = data.getLateTimeInMinute() % 60;
                    Double temp3 = data.getLateTimeInMinute();

                    int hour = temp1.intValue();
                    int minut = temp2.intValue();
                    int total = temp3.intValue();

                    holder.txtTerlambat.setText(hour + " " +
                                    context.getResources().getString(R.string.title_jam) + " "
                            + minut + " " +
                            context.getResources().getString(R.string.title_menit)
                            +" ( " +
                            context.getResources().getString(R.string.title_total) + " "
                            + total + " " +
                            context.getResources().getString(R.string.title_menit) +" )");
                } catch (Exception e){

                }


                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
             //   holder.mTxtStatus.setText(context.getResources().getString(R.string.title_terlambat));
              //  holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
             //   holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.llTerlambat.setVisibility(View.GONE);
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
             //   holder.mTxtStatus.setText(context.getResources().getString(R.string.title_tidak_terlambat));
             //   holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
             //   holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}


