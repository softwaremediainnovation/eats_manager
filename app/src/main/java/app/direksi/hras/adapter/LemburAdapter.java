package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataLembur;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 07/05/2019.
 */

public class LemburAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataLembur> mItemList;
    private Context context;


    public LemburAdapter(List<DataLembur> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lembur, parent, false);
            return new LemburAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LemburAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof LemburAdapter.ItemViewHolder) {

            populateItemRows((LemburAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LemburAdapter.LoadingViewHolder) {
            showLoadingView((LemburAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, mTxtStart,
                mTxtEnd, mTxtStatus,
                mTxtNote, txtDurasi, txtOrganisasi;
        public ImageView mIconStatus;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            txtDurasi = itemView.findViewById(R.id.txtDurasi);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            mTxtNote = itemView.findViewById(R.id.mTxtNote);
            mTxtEnd = itemView.findViewById(R.id.mTxtEnd);
            mTxtStart = itemView.findViewById(R.id.mTxtStart);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LemburAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(LemburAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataLembur data = mItemList.get(position);
        double differenceDates = 0L;

        if (data.getDateStart() != null && data.getDateEnd() != null) {
            try {
                //Dates to compare
                String CurrentDate = data.getDateStart();
                String FinalDate = data.getDateEnd();

                Date date1;
                Date date2;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                //Setting dates
                date1 = dates.parse(CurrentDate);
                date2 = dates.parse(FinalDate);

                //Comparing dates
                double difference = Math.abs(date1.getTime() - date2.getTime());
                differenceDates = difference / (60 * 1000);

                //Convert long to String
               // String dayDifference = Long.toString(differenceDates);

                //Log.e("HERE", "HERE: " + dayDifference);

                Double temp1 = differenceDates/60;
                Double temp2 = differenceDates % 60;
                Double temp3 =  differenceDates;

                int hour =  temp1.intValue();
                int minut = temp2.intValue();
                int total = temp3.intValue();

                //tbLast.setVisibility(View.VISIBLE);
               // String value = ("Jumlah lembur dalam 1 bulan adalah  <b>" + hour +  " jam " +  minut + " menit ( Total " + total+ " menit )</b>");
                //txtLastTitle.setText(Html.fromHtml(value));


                holder.txtDurasi.setText(hour + " " +
                        context.getResources().getString(R.string.title_jam) + " "
                        + minut + " " +
                        context.getResources().getString(R.string.title_menit)
                        +" ( " +
                        context.getResources().getString(R.string.title_total) + " "
                        + total + " " +
                        context.getResources().getString(R.string.title_menit) +" )");

            } catch (Exception exception) {
                holder.txtDurasi.setText("");
               // Log.e("DIDN'T WORK", "exception " + exception);
            }
        } else {
            holder.txtDurasi.setText("");
        }



        holder.mTxtJudul.setText(data.getEmployee().getFirstName() + " " +data.getEmployee().getLastName());
        if (data.getDateStart() != null)
            holder.mTxtStart.setText(DefaultFormatter.changeFormatDate(data.getDateStart()));
        if (data.getDateEnd() != null)
            holder.mTxtEnd.setText(DefaultFormatter.changeFormatDate(data.getDateEnd()));

        holder.mTxtNote.setText(data.getNote()!= null ? data.getNote() : "");
        holder.txtOrganisasi.setText(data.getOrganization()!= null ? data.getOrganization() : "");

        /*holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailLemburActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });*/

        if (data.getIsApproved() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));
            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_menunggu));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsApproved()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_disetujui));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_ditolak));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }

    }


}