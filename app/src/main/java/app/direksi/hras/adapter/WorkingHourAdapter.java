package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.EmployeeInfoWorkingHour;

/**
 * Created by dhimaz on 19/06/2019.
 */

public class WorkingHourAdapter extends RecyclerView.Adapter<WorkingHourAdapter.MyViewHolder> {

    private List<EmployeeInfoWorkingHour> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtHari , txtJam;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            txtHari = view.findViewById(R.id.txtHari);
            txtJam = view.findViewById(R.id.txtJam);



        }
    }


    public WorkingHourAdapter(List<EmployeeInfoWorkingHour> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public WorkingHourAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_holiday, parent, false);

        return new WorkingHourAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(WorkingHourAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        EmployeeInfoWorkingHour data = dataList.get(position);

        holder.txtHari.setText(data.getHari() != null? data.getHari() : "");
        holder.txtJam.setText(data.getJamMasuk() != null ? data.getJamMasuk() + " " + context.getResources().getString(R.string.title_sampai_dengan) +  " " + data.getJamPulang() : "");








    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
