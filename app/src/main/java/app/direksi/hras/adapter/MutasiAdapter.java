package app.direksi.hras.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataMembership;
import app.direksi.hras.util.DefaultFormatter;

public class  MutasiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataMembership> mItemList;
    private Context context;
    private static final int REQUEST_PHONE_CALL = 1;

    public MutasiAdapter(List<DataMembership> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_mutasi, parent, false);
            return new MutasiAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new MutasiAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof MutasiAdapter.ItemViewHolder) {

            populateItemRows((MutasiAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof MutasiAdapter.LoadingViewHolder) {
            showLoadingView((MutasiAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTanggal, txtFrom, txtTo;



        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);


            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            txtFrom = itemView.findViewById(R.id.txtFrom);
            txtTo = itemView.findViewById(R.id.txtTo);

        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(MutasiAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(MutasiAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataMembership data = mItemList.get(position);

        if (data.getDateCreated() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDateCreated()));

        holder.txtFrom.setText(data.getOrganizationOld());
        holder.txtTo.setText(data.getOrganizationNew());





    }

    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(context, context.getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            // Toast.makeText(DetailClaimActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }


}