package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataClaim;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 07/05/2019.
 */

public class ClaimAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataClaim> mItemList;
    private Context context;


    public ClaimAdapter(List<DataClaim> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_claim, parent, false);
            return new ClaimAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new ClaimAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ClaimAdapter.ItemViewHolder) {

            populateItemRows((ClaimAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof ClaimAdapter.LoadingViewHolder) {
            showLoadingView((ClaimAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTanggal, txtJumlah, mTxtNote, mTxtJudul, mTxtStatus, txtKategori, txtOrganisasi;
        // public ImageView mIconStatus;
        public ImageView mIconStatus, mIconStatusCair;

        public ConstraintLayout mMainLayout;


        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            txtKategori = itemView.findViewById(R.id.txtKategori);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            mTxtNote = itemView.findViewById(R.id.mTxtNote);
            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);
            mIconStatusCair = itemView.findViewById(R.id.mIconStatusCair);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(ClaimAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ClaimAdapter.ItemViewHolder holder, int position) {

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataClaim data = mItemList.get(position);


        holder.mTxtJudul.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
        holder.txtKategori.setText(data.getReimbursementCategory().getName());
        holder.txtOrganisasi.setText(data.getOrganization()!= null ? data.getOrganization():"");
        if (data.getDate() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDate()));
        holder.txtJumlah.setText(String.valueOf(data.getAmount() != null ? "Rp " + formatter.format(data.getAmount()) : "-"));
        holder.mTxtNote.setText(data.getNote());
        holder.mIconStatusCair.setVisibility(View.VISIBLE);

      /*  holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailClaimActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });*/

        if (data.getIsApproved() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));
            holder.mIconStatusCair.setVisibility(View.GONE);
            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_menunggu));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsApproved()){

                if (data.getIsCair()){

                    holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                    holder.mIconStatusCair.setBackgroundResource((R.drawable.ic_cairtrue));
                    holder.mTxtStatus.setText(context.getResources().getString(R.string.title_disetujui));
                    holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                    holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

                } else {

                    holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                    holder.mIconStatusCair.setBackgroundResource((R.drawable.ic_cairfalse));
                    holder.mTxtStatus.setText(context.getResources().getString(R.string.title_disetujui));
                    holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                    holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

                }


            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
                holder.mIconStatusCair.setVisibility(View.GONE);
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_ditolak));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }



    }


}