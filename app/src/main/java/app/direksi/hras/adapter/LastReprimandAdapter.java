package app.direksi.hras.adapter;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.DetailAttendancesActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.DataLastReprimand;
import app.direksi.hras.util.DefaultFormatter;

public class LastReprimandAdapter extends RecyclerView.Adapter<LastReprimandAdapter.MyViewHolder> {

    private List<DataLastReprimand> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggalLast, txtKategoriLast, txtCreatorLast, txtCatatanLast;
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            txtTanggalLast = view.findViewById(R.id.txtTanggalLast);
            txtKategoriLast = view.findViewById(R.id.txtKategoriLast);
            txtCreatorLast = view.findViewById(R.id.txtCreatorLast);
            txtCatatanLast = view.findViewById(R.id.txtCatatanLast);



        }
    }


    public LastReprimandAdapter(List<DataLastReprimand> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public LastReprimandAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_last_reprimand, parent, false);

        return new LastReprimandAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LastReprimandAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataLastReprimand data = dataList.get(position);

        if (data.getDate() != null){

            holder.txtTanggalLast.setText( (DefaultFormatter.changeFormatDate(data.getDate())) );

        }
        holder.txtKategoriLast.setText(data.getCategory());
        holder.txtCreatorLast.setText(data.getIssuerID());
        holder.txtCatatanLast.setText(data.getNote());

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

