package app.direksi.hras.adapter;

import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.DetailAttendancesActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 22/05/2019.
 */

public class AttendanceKuAdapter extends RecyclerView.Adapter<AttendanceKuAdapter.MyViewHolder> {

    private List<DataAttendance> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal , mTxtJudul, mTxtStatus;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);

            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            txtTanggal = view.findViewById(R.id.txtTanggal);
            mMainLayout = view.findViewById(R.id.mMainLayout);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);


        }
    }


    public AttendanceKuAdapter(List<DataAttendance> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public AttendanceKuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_attendanceku, parent, false);

        return new AttendanceKuAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AttendanceKuAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataAttendance data = dataList.get(position);

        if (data.getIsPhotoApproved() == null){

            holder.mTxtStatus.setVisibility(View.GONE);

        } else {

            if (data.getIsPhotoApproved()){
                holder.mTxtStatus.setVisibility(View.GONE);
            } else {
                holder.mTxtStatus.setVisibility(View.VISIBLE);
            }

        }

        holder.mTxtJudul.setText(data.getAttendanceID().toString());
        if (data.getDateTime() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDateTime()));


        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailAttendancesActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });

        if (data.getIsLate() == null){
            holder.mTxtStatus.setText("");
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsLate()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_terlambat));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_tidak_terlambat));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }







    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}



