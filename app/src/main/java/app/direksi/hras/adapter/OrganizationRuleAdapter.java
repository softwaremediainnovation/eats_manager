package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.EmployeeInfoOrganizationRule;

/**
 * Created by dhimaz on 01/07/2019.
 */

public class OrganizationRuleAdapter  extends RecyclerView.Adapter<OrganizationRuleAdapter.MyViewHolder> {

    private List<EmployeeInfoOrganizationRule> dataList;
    private Context context;
    private String temp_nama = "";
    private Boolean status;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtRule, txtOrg, txtCompany ;
         public ImageView mIconStatus;
        public ConstraintLayout mMainLayout;
        public LinearLayout llHead;

        public MyViewHolder(View view) {
            super(view);
            txtRule = view.findViewById(R.id.txtRule);
            txtOrg = view.findViewById(R.id.txtOrg);
            txtCompany = view.findViewById(R.id.txtCompany);
            mIconStatus = view.findViewById(R.id.mIconStatus);
            llHead = view.findViewById(R.id.llHead);



        }
    }


    public OrganizationRuleAdapter(List<EmployeeInfoOrganizationRule> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public OrganizationRuleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rule_organization, parent, false);

        return new OrganizationRuleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrganizationRuleAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        EmployeeInfoOrganizationRule data = dataList.get(position);

        holder.txtRule.setText(data.getPermission() != null? data.getPermission() : "");
        holder.txtOrg.setText(data.getOrganization() != null? data.getOrganization() : "");
        holder.txtCompany.setText(data.getCompany() != null? data.getCompany() : "");

        if (data.getIsAllow()!= true){
            holder.mIconStatus.setBackgroundResource(R.drawable.status_reject);
        }else {
            holder.mIconStatus.setBackgroundResource(R.drawable.status_accept);
        }



        /*if (position == 0)
        {
            temp_nama = data.getPermission();
            status = data.getIsAllow();
        }
        else
        {
            if ( temp_nama.equals(data.getPermission()) && status == data.getIsAllow())
            {
                holder.llHead.setVisibility(View.GONE);
            }
            else
            {
                holder.llHead.setVisibility(View.VISIBLE);

                temp_nama = data.getPermission();
                status = data.getIsAllow();
            }
        }*/



    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}



