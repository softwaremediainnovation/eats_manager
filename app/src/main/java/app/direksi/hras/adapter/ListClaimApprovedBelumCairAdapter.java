package app.direksi.hras.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataClaim;
import app.direksi.hras.util.DefaultFormatter;

public class ListClaimApprovedBelumCairAdapter extends RecyclerView.Adapter<ListClaimApprovedBelumCairAdapter.MyViewHolder> {

    private List<DataClaim> dataList;
    private Context context;
    private String idRem;

    public class MyViewHolder extends RecyclerView.ViewHolder {

      public CheckBox cbFinish;
      public  LinearLayout llList;
      public TextView txtJumlah, txtTanggal, txtKeperluan, txtCreator;


        public MyViewHolder(View view) {
            super(view);

            cbFinish = itemView.findViewById(R.id.cbFinish);
            llList = itemView.findViewById(R.id.llList);

            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            txtKeperluan = itemView.findViewById(R.id.txtKeperluan);
            txtCreator = itemView.findViewById(R.id.txtCreator);


        }
    }


    public ListClaimApprovedBelumCairAdapter(List<DataClaim> pnwrAdvList, Context context, String idRem) {
        this.dataList = pnwrAdvList;
        this.context = context;
        this.idRem = idRem;
    }

    @Override
    public ListClaimApprovedBelumCairAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_claim_approved, parent, false);

        return new ListClaimApprovedBelumCairAdapter.MyViewHolder(itemView);
    }

    public interface onItemClickListner{
        void onClick(String str);//pass your object types.
    }

    onItemClickListner onItemClickListner;

    public void setOnItemClickListner(ListClaimApprovedBelumCairAdapter.onItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

    @Override
    public void onBindViewHolder(ListClaimApprovedBelumCairAdapter.MyViewHolder holder, int position) {
        DataClaim data = dataList.get(position);

        DecimalFormat formatter = new DecimalFormat("#,###,###");
        holder.txtJumlah.setText(String.valueOf(data.getAmount() != null ? context.getResources().getString(R.string.title_rp) + " " + formatter.format(data.getAmount()) : "-"));

        if (data.getDate() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDate()));

        holder.txtKeperluan.setText(data.getReimbursementCategory().getName()!=null ? data.getReimbursementCategory().getName() : "");

        holder.txtCreator.setText(data.getApprover().getFirstName() + " " + data.getApprover().getLastName());

        if (data.getReimbursementID().toString().equals(idRem)){
            holder.cbFinish.setChecked(true);
         //   holder.cbFinish.setEnabled(false);
        }

        holder.llList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (holder.cbFinish.isChecked()){
                    if (data.getReimbursementID().toString().equals(idRem)){
                        holder.cbFinish.setChecked(true);
                        data.setChecked(true);

                    } else {
                        holder.cbFinish.setChecked(false);
                        data.setChecked(false);
                    }


                }else {

                    if (data.getReimbursementID().toString().equals(idRem)){
                        holder.cbFinish.setChecked(true);
                        data.setChecked(true);

                    } else {
                        holder.cbFinish.setChecked(true);
                        data.setChecked(true);
                    }

                }

                onItemClickListner.onClick("");

            }
        });


        holder.cbFinish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {



                if (isChecked){
                    if (data.getReimbursementID().toString().equals(idRem)){
                        holder.cbFinish.setChecked(true);
                        data.setChecked(true);
                        holder.cbFinish.jumpDrawablesToCurrentState();

                    } else {
                        holder.cbFinish.setChecked(true);
                        data.setChecked(true);
                    }

                }else {
                    if (data.getReimbursementID().toString().equals(idRem)){
                        holder.cbFinish.setChecked(true);
                        data.setChecked(true);
                        holder.cbFinish.jumpDrawablesToCurrentState();

                    } else {
                        holder.cbFinish.setChecked(false);
                        data.setChecked(false);
                    }
                }

                onItemClickListner.onClick("");

            }
        });


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}



