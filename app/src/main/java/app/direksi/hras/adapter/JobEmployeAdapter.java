package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataJobEmploye;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class JobEmployeAdapter extends RecyclerView.Adapter<JobEmployeAdapter.MyViewHolder> {

    private List<DataJobEmploye> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPerusahaan, txtJabatan, txtNamaAtasan, txtPhoneAtasan, txtTanggal, txtDesc;
        public ImageView lineSharp, imageCarDisplay, imageDetail;


        public MyViewHolder(View view) {
            super(view);

            txtPerusahaan = (TextView) view.findViewById(R.id.txtPerusahaan);
            txtJabatan = (TextView) view.findViewById(R.id.txtJabatan);
            txtNamaAtasan = (TextView) view.findViewById(R.id.txtNamaAtasan);
            txtPhoneAtasan = (TextView) view.findViewById(R.id.txtPhoneAtasan);
            txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
            txtDesc = (TextView) view.findViewById(R.id.txtDesc);


        }
    }


    public JobEmployeAdapter(List<DataJobEmploye> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_job_employe, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataJobEmploye data = dataList.get(position);
        holder.txtPerusahaan.setText(String.valueOf(data.getCompany() != null ? data.getCompany(): "-"));
        holder.txtJabatan.setText(String.valueOf(data.getPosition() != null ? data.getPosition(): "-"));
        holder.txtNamaAtasan.setText(String.valueOf(data.getHeadBoss() != null ? data.getHeadBoss() : "-"));
        holder.txtPhoneAtasan.setText(String.valueOf(data.getContactNumber() != null ? data.getContactNumber(): "-"));
        holder.txtDesc.setText(String.valueOf(data.getJobDescription() != null ? data.getJobDescription(): "-"));

        Date sDate, eDate;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatParse = new SimpleDateFormat("dd MMM yy");

        try {

            if (data.getDateStart()!= null) {
                sDate = parser.parse(data.getDateStart());
               // holder.txtTanggal.setText(formatParse.format(sDate));

                if (data.getDateEnd()!= null) {
                    eDate = parser.parse(data.getDateEnd());
                    holder.txtTanggal.setText(formatParse.format(sDate) + " " + context.getResources().getString(R.string.title_sampai_dengan) +  " " +formatParse.format(eDate));
                }
                else
                {
                    holder.txtTanggal.setText("-");
                }
            }
            else
            {
                holder.txtTanggal.setText("-");
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }







    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}


