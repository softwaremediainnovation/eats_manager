package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.direksi.hras.model.DataCertificateEmploye;
import app.direksi.hras.R;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class CertificateEmployeAdapter extends RecyclerView.Adapter<CertificateEmployeAdapter.MyViewHolder> {

    private List<DataCertificateEmploye> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPerusahaan, mTxtStatus, txtTanggal, txtDesc;
        public ImageView lineSharp, imageCarDisplay, imageDetail;


        public MyViewHolder(View view) {
            super(view);

            txtPerusahaan = (TextView) view.findViewById(R.id.txtPerusahaan);
            mTxtStatus = (TextView) view.findViewById(R.id.mTxtStatus);
            txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);
            txtDesc = (TextView) view.findViewById(R.id.txtDesc);



        }
    }


    public CertificateEmployeAdapter(List<DataCertificateEmploye> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public CertificateEmployeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_certificate_employe, parent, false);

        return new CertificateEmployeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CertificateEmployeAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataCertificateEmploye data = dataList.get(position);
        holder.txtPerusahaan.setText(String.valueOf(data.getName() != null ? data.getName(): "-"));
        holder.txtDesc.setText(String.valueOf(data.getDescription() != null ? data.getDescription(): "-"));

        Date sDate, eDate;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatParse = new SimpleDateFormat("dd MMM yy");

        try {

            if (data.getDateFrom()!= null) {
                sDate = parser.parse(data.getDateFrom());
                // holder.txtTanggal.setText(formatParse.format(sDate));

                if (data.getDateTo()!= null) {
                    eDate = parser.parse(data.getDateTo());
                    holder.txtTanggal.setText(formatParse.format(sDate) + " s/d " +formatParse.format(eDate));
                }
                else
                {
                    holder.txtTanggal.setText("-");
                }
            }
            else
            {
                holder.txtTanggal.setText("-");
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (data.getIsGraduate()) {

            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_lulus));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));
        }
        else
        {
            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_tidak_lulus));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));
        }






    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}



