package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataBirthday;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 08/07/2019.
 */

public class BirthdayAdapter extends RecyclerView.Adapter<BirthdayAdapter.MyViewHolder> {

    private List<DataBirthday> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtJudul , mTxtStatus, txtTanggal, txtOrganisasi, txtCompany;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            txtTanggal = view.findViewById(R.id.txtTanggal);
            txtOrganisasi = view.findViewById(R.id.txtOrganisasi);
            txtCompany = view.findViewById(R.id.txtCompany);



        }
    }


    public BirthdayAdapter(List<DataBirthday> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public BirthdayAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_birthday, parent, false);

        return new BirthdayAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BirthdayAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataBirthday data = dataList.get(position);
        holder.mTxtJudul.setText(data.getEmployeeName() != null? data.getEmployeeName() : "");
        holder.txtOrganisasi.setText(data.getOrganization() != null? data.getOrganization() : "");
        holder.txtCompany.setText(data.getCompany() != null? data.getCompany() : "");

        if (data.getDob() != null){

            Date orderDate = null;
            Calendar call = null, today = Calendar.getInstance();
            String dateString = "";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM");

            try {
                orderDate = sdf.parse(data.getDob());
                call = Calendar.getInstance();
                call.setTime(orderDate);
                holder.mTxtStatus.setText(String.valueOf(today.get(Calendar.YEAR) - call.get(Calendar.YEAR)) + " " + context.getResources().getString(R.string.title_tahun));
                holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithOutYear(data.getDob()));
            } catch (ParseException e) {
                e.printStackTrace();
            }



        }





    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}


