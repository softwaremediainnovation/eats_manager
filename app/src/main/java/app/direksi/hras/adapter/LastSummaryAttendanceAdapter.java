package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataLastSummaryAttendance;
import app.direksi.hras.util.DefaultFormatter;

public class LastSummaryAttendanceAdapter extends RecyclerView.Adapter<LastSummaryAttendanceAdapter.MyViewHolder> {

    private List<DataLastSummaryAttendance> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtTanggal, txtName;
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView mIconStatus;

        public MyViewHolder(View view) {
            super(view);
            txtTanggal = view.findViewById(R.id.txtTanggal);
            txtName = view.findViewById(R.id.txtName);



        }
    }


    public LastSummaryAttendanceAdapter(List<DataLastSummaryAttendance> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public LastSummaryAttendanceAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_last_summary_attendance, parent, false);

        return new LastSummaryAttendanceAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LastSummaryAttendanceAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataLastSummaryAttendance data = dataList.get(position);
        String temp = data.getInOut() != null? " ( " + data.getInOut() + " )": "";

        String method = "";

       if (data.getIsApproved()== null){

           method = context.getResources().getString(R.string.title_via) + " " + context.getResources().getString(R.string.title_pengajuan_kehadiran) +  " - <font color=#FFA500><b> " +  context.getResources().getString(R.string.title_menunggu) + "</b></font>";

       } else {

           if (data.getIsApproved().toLowerCase().equals("true")){

               if (data.getApprover() == null){
                    String row = data.isIsMobileApp()== true? context.getResources().getString(R.string.title_aplikasi): context.getResources().getString(R.string.title_mesin_finger);
                    method =  context.getResources().getString(R.string.title_via) + " " + row ;

               } else {
                   method = context.getResources().getString(R.string.title_via) + " " + context.getResources().getString(R.string.title_pengajuan_kehadiran) +  " - <font color=#02be73><b> "+ context.getResources().getString(R.string.title_disetujui) + "</b></font>";
               }

           } else{
                   method = context.getResources().getString(R.string.title_via) + " " + context.getResources().getString(R.string.title_pengajuan_kehadiran) + " - <font color=#ff4100><b> " +context.getResources().getString(R.string.title_ditolak) + "</b></font>";
           }

       }
        holder.txtName.setText(Html.fromHtml(method));

        if (data.getDateTime() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDateTime()) + temp);

      //  holder.txtName.setText(data.getInOut() != null? data.getInOut(): "");

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}

