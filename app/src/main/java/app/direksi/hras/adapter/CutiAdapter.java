package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataCuti;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 07/05/2019.
 */

public class CutiAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataCuti> mItemList;
    private Context context;


    public CutiAdapter(List<DataCuti> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cuti, parent, false);
            return new CutiAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new CutiAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof CutiAdapter.ItemViewHolder) {

            populateItemRows((CutiAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof CutiAdapter.LoadingViewHolder) {
            showLoadingView((CutiAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public  TextView mTxtJudul, mTxtStart,
                mTxtEnd, mTxtStatus,
                mTxtReason,
                mTxtNote,
                txtOrganisasi,
                txtJumlahHari;
        public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtJumlahHari = itemView.findViewById(R.id.txtJumlahHari);
            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            mTxtNote = itemView.findViewById(R.id.mTxtNote);
            mTxtReason = itemView.findViewById(R.id.mTxtReason);
            mTxtEnd = itemView.findViewById(R.id.mTxtEnd);
            mTxtStart = itemView.findViewById(R.id.mTxtStart);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(CutiAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(CutiAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataCuti data = mItemList.get(position);


        holder.mTxtJudul.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
        if (data.getStart() != null)
            holder.mTxtStart.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getStart()));
        if (data.getStart() != null)
            holder.mTxtEnd.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getEnd()));
        holder.mTxtReason.setText(data.getConstanta().getName());
        holder.mTxtNote.setText(data.getNote());
        holder.txtOrganisasi.setText(data.getOrganization().getName());
        holder.txtJumlahHari.setText(data.getHowManyDays().toString() + " " + context.getResources().getString(R.string.title_hari));

      /*  holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences( context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailCutiActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });
*/
        if (data.getIsApproved() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));
            holder.mTxtStatus.setText(context.getResources().getString(R.string.title_menunggu));
            holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsApproved()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_disetujui));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
                holder.mTxtStatus.setText(context.getResources().getString(R.string.title_ditolak));
                holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }


    }


}