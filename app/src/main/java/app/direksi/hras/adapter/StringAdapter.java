package app.direksi.hras.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.StringClass;

public class StringAdapter extends RecyclerView.Adapter<StringAdapter.MyViewHolder> {

    private List<StringClass> dataList;
    private Context context;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout llTerlambat;
        // public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;
        public ImageView imgClose, imgMoving;
        public EditText editText1;
        public MyCustomEditTextListener myCustomEditTextListener;
        public TextView txtNum;

        public MyViewHolder(View view , MyCustomEditTextListener myCustomEditTextListeners) {
            super(view);

            imgClose = view.findViewById(R.id.imgClose);
            imgMoving = view.findViewById(R.id.imgMoving);
            txtNum = view.findViewById(R.id.txtNum);
            editText1 = view.findViewById(R.id.editText1);


            myCustomEditTextListener = myCustomEditTextListeners;
            editText1.addTextChangedListener(myCustomEditTextListener);


        }
    }


    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
          //  dataList.get(position).setString(charSequence.toString());
        }

        @Override
        public void afterTextChanged(Editable editable) {
            dataList.get(position).setString(editable.toString());
        }
    }


    public StringAdapter(List<StringClass> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public StringAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_add_task, parent, false);

        MyViewHolder vh = new MyViewHolder(itemView, new MyCustomEditTextListener());

        return (vh);
    }

    @Override
    public void onBindViewHolder(StringAdapter.MyViewHolder holder, int position) {

        StringClass data = dataList.get(position);
        int newwPosition = holder.getAdapterPosition();
        int temp = newwPosition + 1;
        holder.txtNum.setText( temp  + ".");
        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
        holder.editText1.setText(data.getString());
       // holder.txtHapus.setText("HAPUS " + position);
        holder.imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dataList.size()> 0){

                    int newPosition = holder.getAdapterPosition();
                    dataList.remove(newPosition);
                   // notifyItemRemoved(newPosition);
                    /*notifyItemRangeChanged(newPosition, dataList.size());*/
                    notifyDataSetChanged();



                }else {

                }

            }
        });

        holder.imgMoving.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {


                    MDToast.makeText(v.getContext(), context.getResources().getString(R.string.title_klik_dan_tahan_pindah_item),
                            MDToast.LENGTH_LONG, MDToast.TYPE_INFO).show();
                    // Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.loading_error), Toast.LENGTH_SHORT).show();
                } catch (Exception e){

                }

            }
        });






        /*final TextWatcher mPriceWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {




            }

            @Override
            public void afterTextChanged(Editable s) {

                String enteredString = s.toString();
                int newPosition = holder.getAdapterPosition();
                dataList.get(newPosition).setString(enteredString);



            }


        };
        holder.editText1.addTextChangedListener(mPriceWatcher);*/


        if (holder.editText1.requestFocus()) {


          //  window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            InputMethodManager inputMethodManager = (InputMethodManager) holder.editText1.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(holder.editText1, InputMethodManager.SHOW_IMPLICIT);
        }

      //  holder.editText1.setSelection(dataList.size() - 1);

      /*  holder.editText1.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput( holder.editText1, InputMethodManager.SHOW_IMPLICIT);*/







    }





    @Override
    public int getItemCount() {
        return dataList.size();
    }

   /* @Override
    public int getItemViewType(int position) {
        return position;
    }*/
}


