package app.direksi.hras.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataRequestAttendance;
import app.direksi.hras.util.DefaultFormatter;

import static app.direksi.hras.util.DefaultFormatter.isNegative;

public class RequestAttendanceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataRequestAttendance> mItemList;
    private Context context;


    public RequestAttendanceAdapter(List<DataRequestAttendance> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_request_attendance, parent, false);
            return new RequestAttendanceAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new RequestAttendanceAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof RequestAttendanceAdapter.ItemViewHolder) {

            populateItemRows((RequestAttendanceAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof RequestAttendanceAdapter.LoadingViewHolder) {
            showLoadingView((RequestAttendanceAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTanggal, txtJumlah, mTxtNote, mTxtJudul, mTxtStatus, txtOrganisasi, txtAbsen;

        public ImageView mIconStatus;
        public ConstraintLayout mMainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtAbsen = itemView.findViewById(R.id.txtAbsen);
            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            mTxtNote = itemView.findViewById(R.id.mTxtNote);
            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(RequestAttendanceAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(RequestAttendanceAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataRequestAttendance data = mItemList.get(position);

        if (data.getIsPhotoApproved() == null){

            holder.mTxtStatus.setVisibility(View.GONE);

        } else {

            if (data.getIsPhotoApproved()){
                holder.mTxtStatus.setVisibility(View.GONE);
            } else {
                holder.mTxtStatus.setVisibility(View.VISIBLE);
            }

        }

        holder.mTxtJudul.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());

        String isnegative = "+";
        if (isNegative(data.getDateGMT())){
            isnegative = "";
        }

        if (data.getDateTime() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDateTime()) + " " + context.getResources().getString(R.string.title_gmt) + isnegative + String.valueOf(data.getDateGMT()));

       // holder.mTxtNote.setText(data.getn());
        holder.txtOrganisasi.setText(data.getOrganization());
        holder.txtAbsen.setText(data.getInOut()!= null ? data.getInOut():"");



        if (data.getIsApproved() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));
          //  holder.mTxtStatus.setText(context.getResources().getString(R.string.title_menunggu));
          //  holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
          //  holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

        }
        else {

            if (data.getIsApproved()){
                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
              //  holder.mTxtStatus.setText(context.getResources().getString(R.string.title_disetujui));
             //   holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
             //   holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }
            else {
                holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
             //   holder.mTxtStatus.setText(context.getResources().getString(R.string.title_ditolak));
             //   holder.mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
            //    holder.mTxtStatus.setTextColor( ContextCompat.getColor(context, R.color.colorWhite));

            }

        }


    }
}


