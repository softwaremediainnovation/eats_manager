package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataAktivitas;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 26/06/2019.
 */

public class ActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataAktivitas> mItemList;
    private Context context;


    public ActivityAdapter(List<DataAktivitas> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_aktivitas, parent, false);
            return new ActivityAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new ActivityAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ActivityAdapter.ItemViewHolder) {

            populateItemRows((ActivityAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof ActivityAdapter.LoadingViewHolder) {
            showLoadingView((ActivityAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, mTxtStatus, txtDesc, txtOrganisasi;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            txtDesc = itemView.findViewById(R.id.txtDesc);

        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(ActivityAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(ActivityAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataAktivitas data = mItemList.get(position);

        if (data.getDate() != null)
            holder.mTxtStatus.setText(DefaultFormatter.changeFormatDate(data.getDate()));

        holder.txtOrganisasi.setText(data.getOrganization() );
        holder.mTxtJudul.setText(data.getEmployeeName() );
        holder.txtDesc.setText(data.getDescription());

    }


}
