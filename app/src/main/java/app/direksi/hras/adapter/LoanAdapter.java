package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataLoan;
import app.direksi.hras.util.DefaultFormatter;

/**
 * Created by dhimaz on 07/05/2019.
 */

public class LoanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataLoan> mItemList;
    private Context context;


    public LoanAdapter(List<DataLoan> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loan, parent, false);
            return new LoanAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new LoanAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof LoanAdapter.ItemViewHolder) {

            populateItemRows((LoanAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LoanAdapter.LoadingViewHolder) {
            showLoadingView((LoanAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView txtTanggal, txtJumlah, mTxtNote, mTxtJudul, mTxtStatus, txtOrganisasi;

        public ImageView mIconStatus, mIconStatusCair;
        public ConstraintLayout mMainLayout;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            mTxtStatus = itemView.findViewById(R.id.mTxtStatus);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            txtTanggal = itemView.findViewById(R.id.txtTanggal);
            mTxtNote = itemView.findViewById(R.id.mTxtNote);
            txtJumlah = itemView.findViewById(R.id.txtJumlah);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
            mIconStatus = itemView.findViewById(R.id.mIconStatus);
            mIconStatusCair = itemView.findViewById(R.id.mIconStatusCair);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(LoanAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(LoanAdapter.ItemViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataLoan data = mItemList.get(position);

        holder.mTxtJudul.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
        if (data.getDate() != null)
            holder.txtTanggal.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getDate()));
        holder.txtJumlah.setText(String.valueOf(data.getAmount() != null ? context.getResources().getString(R.string.title_rp) + " " + formatter.format(data.getAmount()) : "-"));
        holder.mTxtNote.setText(data.getNote());
        holder.txtOrganisasi.setText(data.getOrganization());
        holder.mIconStatusCair.setVisibility(View.VISIBLE);


        if (data.getIsApproved() == null && data.getIsApproved2() == null ||
                data.getIsApproved() == true && data.getIsApproved2() == null){
            holder.mIconStatus.setBackgroundResource((R.drawable.status_pending));
            holder.mIconStatusCair.setVisibility(View.GONE);


        }
        else  if (data.getIsApproved() == true && data.getIsApproved2() == true){


            if (data.getIsCair()){

                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mIconStatusCair.setBackgroundResource((R.drawable.ic_cairtrue));

            } else {

                holder.mIconStatus.setBackgroundResource((R.drawable.status_accept));
                holder.mIconStatusCair.setBackgroundResource((R.drawable.ic_cairfalse));
            }

        } else {
            holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
            holder.mIconStatusCair.setVisibility(View.GONE);
        }



    }


}