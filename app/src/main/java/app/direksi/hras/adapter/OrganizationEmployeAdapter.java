package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataOrganizationEmploye;

/**
 * Created by dhimaz on 01/04/2019.
 */

public class OrganizationEmployeAdapter  extends RecyclerView.Adapter<OrganizationEmployeAdapter.MyViewHolder> {

    private List<DataOrganizationEmploye> dataList;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPerusahaan,  txtTanggal;
        public ImageView lineSharp, imageCarDisplay, imageDetail;


        public MyViewHolder(View view) {
            super(view);

            txtPerusahaan = (TextView) view.findViewById(R.id.txtPerusahaan);
            txtTanggal = (TextView) view.findViewById(R.id.txtTanggal);



        }
    }


    public OrganizationEmployeAdapter(List<DataOrganizationEmploye> pnwrAdvList, Context context) {
        this.dataList = pnwrAdvList;
        this.context = context;
    }

    @Override
    public OrganizationEmployeAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_organization_employe, parent, false);

        return new OrganizationEmployeAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrganizationEmployeAdapter.MyViewHolder holder, int position) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataOrganizationEmploye data = dataList.get(position);
        holder.txtPerusahaan.setText(String.valueOf(data.getName() != null ? data.getName(): "-"));


        Date sDate, eDate;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatParse = new SimpleDateFormat("dd MMM yy");

        try {

            if (data.getDateStart()!= null) {
                sDate = parser.parse(data.getDateStart());
                // holder.txtTanggal.setText(formatParse.format(sDate));

                if (data.getDateEnd()!= null) {
                    eDate = parser.parse(data.getDateEnd());
                    holder.txtTanggal.setText(formatParse.format(sDate) + " " + context.getResources().getString(R.string.title_sampai_dengan) +  " " +formatParse.format(eDate));
                }
                else
                {
                    holder.txtTanggal.setText("-");
                }
            }
            else
            {
                holder.txtTanggal.setText("-");
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }







    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}


