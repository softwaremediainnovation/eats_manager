package app.direksi.hras.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.DetailListAttendanceActivity;
import app.direksi.hras.R;
import app.direksi.hras.model.DataEmployee;

/**
 * Created by dhimaz on 27/05/2019.
 */

public class EmployeeAttendanceAdapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataEmployee> mItemList;
    private Context context;
    private static final int REQUEST_PHONE_CALL = 1;

    public EmployeeAttendanceAdapter(List<DataEmployee> itemList, Context context) {

        mItemList = itemList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_employe, parent, false);
            return new EmployeeAttendanceAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new EmployeeAttendanceAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof EmployeeAttendanceAdapter.ItemViewHolder) {

            populateItemRows((EmployeeAttendanceAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof EmployeeAttendanceAdapter.LoadingViewHolder) {
            showLoadingView((EmployeeAttendanceAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, txtEmail, txtPhone, profil_role, txtOrganisasi;
        public ImageView profile_image;
        public ConstraintLayout mMainLayout;
        public LinearLayout llItem;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            llItem = itemView.findViewById(R.id.llItem);
            txtOrganisasi = itemView.findViewById(R.id.txtOrganisasi);
            profil_role = itemView.findViewById(R.id.profil_role);
            mTxtJudul = itemView.findViewById(R.id.mTxtJudul);
            txtEmail = itemView.findViewById(R.id.txtEmail);
            txtPhone = itemView.findViewById(R.id.txtPhone);
            mMainLayout = itemView.findViewById(R.id.mMainLayout);
            profile_image = (ImageView) itemView.findViewById(R.id.profile_image);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(EmployeeAttendanceAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(EmployeeAttendanceAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataEmployee data = mItemList.get(position);


        holder.profil_role.setText(data.getNik() != null ? data.getNik() : " ");
        holder.mTxtJudul.setText(data.getFirstName() + " " + data.getLastName());
        holder.txtEmail.setText(data.getEmail() != null ? data.getEmail() : "");
        holder.txtPhone.setText(data.getPhone() != null ? data.getPhone() : "");
        holder.txtOrganisasi.setText(data.getOrganization().getName() != null ? data.getOrganization().getName() : "");

        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(context, DetailListAttendanceActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                context.startActivity(mIntent);
            }
        });

        holder.txtPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (data.getPhone().equals("") || data.getPhone() == null) {

                } else{
                    final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                    CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(context.getResources().getString(R.string.title_aksi));
                    builder.setItems(colors, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    // Check Permissions Now
                                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                } else {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + data.getPhone().trim()));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(intent);
                                }
                            } else if (which == 1) {
                                Uri uri = Uri.parse("smsto:" + data.getPhone().trim());
                                Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.getApplicationContext().startActivity(mSendSms);
                            } else if (which == 2) {
                                String name = data.getFirstName() + " " + data.getLastName();
                                String phone = data.getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                contactIntent
                                        .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                        .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                ((Activity) context).startActivityForResult(contactIntent, 1);

                            } else if (which == 3) {

                                voiceCall(data.getPhone().trim());
                            }
                        }
                    });
                    builder.show();
                }

            }
        });

        if (data.getPhotoUrl() == null || data.getPhotoUrl().equals("http://hras.warnawarni.co.id")) {
            Glide.with(context)
                    .load(context.getResources().getDrawable(R.drawable.profile))
                    .error(Glide.with(holder.profile_image).load(R.drawable.profile))
                    .into(holder.profile_image);


        } else {
            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);

            Glide.with(context)
                    .load(( data.getPhotoUrl()))
                    .error(Glide.with(holder.profile_image).load(R.drawable.profile))
                    .into(holder.profile_image);
        }


    }

    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(context, context.getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            // Toast.makeText(DetailClaimActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }
}


