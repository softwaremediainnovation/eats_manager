package app.direksi.hras.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.List;

import app.direksi.hras.R;
import app.direksi.hras.model.DataTrackingAset;

/**
 * Created by dhimaz on 23/05/2019.
 */

public class TrackingAsetKuAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public List<DataTrackingAset> mItemList;
    private Context context;
    private String id;



    public TrackingAsetKuAdapter(List<DataTrackingAset> itemList, Context context, String id) {

        mItemList = itemList;
        this.context = context;
        this.id = id;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_asetku, parent, false);
            return new TrackingAsetKuAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
            return new TrackingAsetKuAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof TrackingAsetKuAdapter.ItemViewHolder) {

            populateItemRows((TrackingAsetKuAdapter.ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof TrackingAsetKuAdapter.LoadingViewHolder) {
            showLoadingView((TrackingAsetKuAdapter.LoadingViewHolder) viewHolder, position);
        }

    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    /**
     * The following method decides the type of ViewHolder to display in the RecyclerView
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mItemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, txtLastLocation;
        public ImageView lineSharp, imageCarDisplay, imageDetail;
        public ImageView mIconStatus;
        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);

            mTxtJudul = (TextView) itemView.findViewById(R.id.mTxtJudul);
            txtLastLocation = (TextView) itemView.findViewById(R.id.txtLastLocation);
            mIconStatus = (ImageView) itemView.findViewById(R.id.mIconStatus);
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private void showLoadingView(TrackingAsetKuAdapter.LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed

    }

    private void populateItemRows(TrackingAsetKuAdapter.ItemViewHolder holder, int position) {


        DecimalFormat formatter = new DecimalFormat("#,###,###");
        DataTrackingAset data = mItemList.get(position);
        holder.mTxtJudul.setText(String.valueOf(data.getName() != null ? data.getName(): "-"));
        holder.txtLastLocation.setText(String.valueOf(data.getLastLocation() != null ? data.getLastLocation(): "-"));





       /* SharedPreferences pref = PreferenceManager
                .getDefaultSharedPreferences(context);
        String Authorization = pref.getString("pref_employeeid", null);

        TrackingAsset ta = data.getAssetTrackings().get(data.getAssetTrackings().size() - 1);
        if (ta.getIsAccepted() == false && ta.getDestinationHolder().getEmployeeID().equals(id)) {

            holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
        } else {
          //  holder.mIconStatus.setBackgroundResource((R.drawable.status_reject));
            holder.mIconStatus.setVisibility(View.GONE);
        }*/
       // holder.txtLastLocation.setText(Authorization + " " + ta.getIsAccepted().toString());

    }


}
