package app.direksi.hras;

/**
 * Created by warnawarni on 4/10/2017.
 */


import android.content.Context;
import android.os.Build;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.direksi.hras.model.DataCuti;
import app.direksi.hras.util.DefaultFormatter;


public class RecyclerViewAdapterList extends RecyclerView.Adapter<RecyclerViewAdapterList.ViewHolder> {

    private Context context;
    private List<DataCuti> results;

    public RecyclerViewAdapterList(Context context, List<DataCuti> results) {
        this.context = context;
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cuti, parent, false);
        ViewHolder holder = new ViewHolder(v);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final DataCuti data = results.get(position);

        holder.mTxtJudul.setText(data.getComment());
        if (data.getStart() != null)
            holder.mTxtStart.setText(DefaultFormatter.changeFormatDate(data.getStart()));
        if (data.getStart() != null)
            holder.mTxtEnd.setText(DefaultFormatter.changeFormatDate(data.getEnd()));
        holder.mTxtReason.setText(data.getComment());
        holder.mTxtNote.setText(data.getNote());

        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Intent mIntent = new Intent(context, DetailDayOff.class);
                Bundle mBundle = new Bundle();
                mBundle.putParcelable("detail", data);
                mIntent.putExtras(mBundle);
                context.startActivity(mIntent);*/
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            holder.mIconStatus.setBackground(data.getIsApproved()
                    ? context.getDrawable(R.drawable.icon_play) :
                    context.getDrawable(R.drawable.icon_stop));
        else
            holder.mIconStatus.setBackgroundDrawable(data.getIsApproved() ?
                    context.getResources().getDrawable(R.drawable.icon_play) :
                    context.getResources().getDrawable(R.drawable.icon_stop));


        holder.mTxtStatus.setText(data.getIsApproved() ?
                "Disetujui" : "Menunggu"
        );


    }


    @Override
    public int getItemCount() {
        return results.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTxtJudul, mTxtStart,
                mTxtEnd, mTxtStatus,
                mTxtReason,
                mTxtNote;
        public ImageView mIconStatus;

        public ConstraintLayout mMainLayout;

        public ViewHolder(View view) {
            super(view);
            mTxtStatus = view.findViewById(R.id.mTxtStatus);
            mTxtNote = view.findViewById(R.id.mTxtNote);
            mTxtReason = view.findViewById(R.id.mTxtReason);
            mTxtEnd = view.findViewById(R.id.mTxtEnd);
            mTxtStart = view.findViewById(R.id.mTxtStart);
            mTxtJudul = view.findViewById(R.id.mTxtJudul);
            mMainLayout = view.findViewById(R.id.mMainLayout);
          //  mIconStatus = view.findViewById(R.id.mIconStatus);
        }

    }

}
