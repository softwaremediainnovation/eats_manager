package app.direksi.hras;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;

import com.github.clans.fab.FloatingActionButton;

import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class CutiActivity extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton floatingActionButton, mFloatIzin,mFloatOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(CutiActivity.this);
        //remove line in bar
        getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading




        setContentView(R.layout.activity_cuti);

        /*floatingActionButton = findViewById(R.id.mFloatCuti);
        mFloatIzin = findViewById(R.id.mFloatIzin);
        mFloatOut = findViewById(R.id.mFloatOut);*/

        floatingActionButton.setOnClickListener(this);
        mFloatIzin.setOnClickListener(this);
        mFloatOut.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
       /* if (v.getId() == R.id.mFloatCuti) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateCutiFragment dialogFragment = new CreateCutiFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        } else if (v.getId() == R.id.mFloatIzin) {
            android.app.FragmentManager fm = getFragmentManager();
            CreatePermissionFragment dialogFragment = new CreatePermissionFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        }else if (v.getId() == R.id.mFloatOut) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateOutFragment dialogFragment = new CreateOutFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        }*/
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(CutiActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
