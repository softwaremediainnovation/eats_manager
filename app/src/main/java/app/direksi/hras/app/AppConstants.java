package app.direksi.hras.app;

import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.CalendarScopes;

import java.util.Collections;
import java.util.List;

public class AppConstants {

    public static int GPS_REQUEST = 155;
    public static int TIMEOUT_REQUEST = 60;
    public static String RANGE_DISTANCE_NULL = "40";
    public static int SIZE_NUMBER = 10;
    public static int SIZE_COMPRESS_1 = 28;
    public static int SIZE_COMPRESS_2 = 50;
    public static int SIZE_COMPRESS_3 = 70;
    public static boolean CanApproveVoid = false;
    public static int SIZE_MARKER = 40;
    public static int addMindaysCuti = -7;
    public static int addMaxdaysCuti = 7;
    public static int addMaxdaysManual = 30;
    public static int addMaxdaysApproveLoan = 180;
    public static int addMindaysReimburse = -7;
    public static int addMaxdaysReimburse = 7;
    public static int addMindaysLembur = -3;
    public static int addMaxdaysLembur = 3;
    public static int addMindaysZero = 0;
    public static int addMindays1Day = 1;
    public static int addMaxdays1Year = 365;
    public static int RANGE_DISTANCE_BOUNDS = 100;

    public static String NOWACS = "089602595133";
    /*public static String NOWACS = "085731068819";*/
    public static String urlShare = "http://eats.mediainovasi.id/employeedetail?id=";

   /* //XENDIT SURABAYA
    public static String tokenXendid = "xnd_production_Bc0EqmKpMej1l6M1ElzIAaPA3V9h3QVpNQZV1cLuDQ2ok0vAk7A6HaNf4oAFZ9k";
   // public static String tokenXendid = "xnd_development_86jpjTeHO3SFrVjh2qWa3THbeoGiGDmjD1CUiyTKXJtyuQdzoeEZgXNQVhp87";*/


    public static String LogoutPermission = "4";


    public static final String APPLICATION_NAME = "Google Calendar API Java Quickstart";
    public static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    public static final String TOKENS_DIRECTORY_PATH = "tokens";
    public static final List<String> SCOPES = Collections.singletonList(CalendarScopes.CALENDAR_READONLY);
    public static final String CREDENTIALS_FILE_PATH = "/credentials.json";
    public static boolean AllowMock = false;
    public static int addLemburOption = 10;

}
