package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.DataDetailRequestAttendance;
import app.direksi.hras.model.ResponseDetailRequestAttendance;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.util.DefaultFormatter.isNegative;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 29/03/2019.
 */

public class DetailAttendancesActivity extends AppCompatActivity implements OnMapReadyCallback {

    DataAttendance DataAttendance;
    public TextView mTxtStart,
            mTxtStatus,
            txtLatitude,
            txtLongitude,
            txtVia,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtAlamat,
            txtTerlambat,
            txtOrganisasi,
            txtAbsen,
            txtApprover,
            txtComment,
            txtDateApprove,
            txtNote,
            rejecter,
            txtTglReject,
            txtNoteReject;

    private ImageView mIconStatus, profile_image;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private ScrollView scrollView;
    private static final int REQUEST_PHONE_CALL = 1;
    private TableRow trTerlambat, trNote;
    private TableLayout tbApprover;

    private RelativeLayout rlwarning;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailAttendancesActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
       // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_attendances));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        setContentView(R.layout.activity_detail_attendances);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        DataAttendance = gson.fromJson(getIntent().getStringExtra("detail"), DataAttendance.class);
        txtLatitude = findViewById(R.id.txtLatitude);
        txtLongitude = findViewById(R.id.txtLongitude);
        txtVia = findViewById(R.id.txtVia);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        mTxtStart = findViewById(R.id.mTxtStart);
        txtAlamat = findViewById(R.id.txtAlamat);
        txtTerlambat = findViewById(R.id.txtTerlambat);
        trTerlambat = findViewById(R.id.trTerlambat);
        txtAbsen = findViewById(R.id.txtAbsen);

        mLayoutImages = findViewById(R.id.mLayoutImages);
        mLayoutImage = findViewById(R.id.mLayoutImage);
        mImageView = findViewById(R.id.mImageView);
        mIconStatus = findViewById(R.id.mIconStatus);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);

        tbApprover = findViewById(R.id.tbApprover);
        tbApprover.setVisibility(View.GONE);

        txtApprover = findViewById(R.id.txtApprover);
        txtComment = findViewById(R.id.txtComment);
        txtDateApprove = findViewById(R.id.txtDateApprove);
        txtNote = findViewById(R.id.txtNote);

        trNote = findViewById(R.id.trNote);
        trNote.setVisibility(View.GONE);

        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);


        rlwarning = (RelativeLayout) findViewById(R.id.rlwarning);
        rlwarning.setVisibility(View.GONE);
        rejecter = (TextView) findViewById(R.id.rejecter);
        txtTglReject = (TextView) findViewById(R.id.txtTglReject);
        txtNoteReject = (TextView) findViewById(R.id.txtNoteReject);





        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);







        //  Glide.with(getApplicationContext()).load(pemeliharaanClass.getGambarPJ()).thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon)).apply(requestOptions).into(imagePhoto1);
       /* Glide.with(DetailDayOff.this)
                .load((getResources().getString(R.string.base_url) + dataCutiItem.getPath()))
                .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                .apply(requestOptions)
                .into(mImageView);*/


        loading = new SweetAlertDialog(DetailAttendancesActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        messageDialog = new MessageDialog();

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        DetailLoan();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
   /* public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApproveKlaim();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  ProsesUpdate();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }*/

    public void prosesRequest(View v) {
      /*  PreferenceManager.getDefaultSharedPreferences(DetailAttendancesActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataClaim.getReimbursementID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        ApproveKlaimFragment dialogFragment = new ApproveKlaimFragment();
        Bundle bundle = new Bundle();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");*/


    }

    public void DetailLoan() {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailAttendancesActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailRequestAttendance> call = api.getRequestAttendanceDetail(Authorization, DataAttendance.getAttendanceID().toString());

            call.enqueue(new Callback<ResponseDetailRequestAttendance>() {
                @Override
                public void onResponse(Call<ResponseDetailRequestAttendance> call, Response<ResponseDetailRequestAttendance> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            if (response.body().getData().size() > 0){
                                ShowData(response.body().getData().get(0));
                            } else {

                                try {
                                    messageDialog.mShowMessageError(DetailAttendancesActivity.this,getResources().getString(R.string.title_gagal)
                                            , getResources().getString(R.string.loading_error));
                                }catch (Exception r){

                                }
                            }




                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailAttendancesActivity.this,getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(DetailAttendancesActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailRequestAttendance> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(DetailAttendancesActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }catch (Exception r){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(DetailAttendancesActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            }catch (Exception r){

            }
        }
    }

    private void ShowData(DataDetailRequestAttendance data){
        Date orderDate = null;
        String dateString = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");

        txtAbsen.setText(data.getInOut()!= null? data.getInOut() : "");
        txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization() : "");
        txtLatitude.setText(data.getLatitude());
        txtLongitude.setText(data.getLongitude());


        if (data.getIsPhotoApproved()==null){

        } else {

            if(!data.getIsPhotoApproved()){

                try {
                    rlwarning.setVisibility(View.VISIBLE);
                    txtNoteReject.setText(data.getCommentPhoto());
                    rejecter.setText(data.getRejecter());
                    txtTglReject.setText(DefaultFormatter.changeFormatDate(data.getDatePhotoRejected()));
                } catch (Exception e){

                }

            }

        }

        if (data.getFile()== null){

            mImageView.setVisibility(View.GONE);

        } else {

            if (data.getFile().equals("")){

                mImageView.setVisibility(View.GONE);

            } else {

                RequestOptions requestOptions = new RequestOptions();
                //  requestOptions.placeholder(R.drawable.warnawarni);
                requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
                requestOptions.skipMemoryCache(true);

                mImageView.setVisibility(View.VISIBLE);
                Glide.with(DetailAttendancesActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getFile()))
                        .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                        .apply(requestOptions)
                        .error(Glide.with(mImageView).load(R.drawable.no_picture))
                        .into(mImageView);

                mImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getFile());
                        startActivity(intent);
                    }
                });


            }
        }


        String method = "";

        if (data.isIsApproved()== null){

            method = getResources().getString(R.string.title_pengajuan_kehadiran);
            tbApprover.setVisibility(View.VISIBLE);
            trNote.setVisibility(View.VISIBLE);

            try {
                txtApprover.setText(data.getApprover());
                txtComment.setText(data.getComment()!= null? data.getComment() : "");
                txtNote.setText(data.getNote()!= null? data.getNote() : "");
                if (data.getDateApproved() != null) {

                    try {

                        String isnegative = "+";
                        if (isNegative(data.getDateApprovedGMT())) {
                            isnegative = "";
                        }


                        txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getDateApproved()) + " GMT" + isnegative + String.valueOf(data.getDateApprovedGMT()));
                    } catch (Exception es) {
                        es.printStackTrace();
                    }
                }


            } catch (Exception e){

            }

        } else {

            if (data.isIsApproved().toLowerCase().equals("true")){

                if (data.getApprover() == null){
                    String row = data.isIsMobileApp()== true? getResources().getString(R.string.title_aplikasi): getResources().getString(R.string.title_mesin_finger);
                    method = row ;

                } else {
                    method = getResources().getString(R.string.title_pengajuan_kehadiran);
                    tbApprover.setVisibility(View.VISIBLE);
                    trNote.setVisibility(View.VISIBLE);

                    try {
                        txtApprover.setText(data.getApprover());
                        txtComment.setText(data.getComment()!= null? data.getComment() : "");
                        txtNote.setText(data.getNote()!= null? data.getNote() : "");
                        if (data.getDateApproved() != null) {

                            try {

                                String isnegative = "+";
                                if (isNegative(data.getDateApprovedGMT())) {
                                    isnegative = "";
                                }


                                txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getDateApproved()) + " GMT" + isnegative + String.valueOf(data.getDateApprovedGMT()));
                            } catch (Exception es) {
                                es.printStackTrace();
                            }
                        }

                    } catch (Exception e){

                    }
                }

            } else{
                method = getResources().getString(R.string.title_pengajuan_kehadiran);
                tbApprover.setVisibility(View.VISIBLE);
                trNote.setVisibility(View.VISIBLE);

                try {
                    txtApprover.setText(data.getApprover());
                    txtComment.setText(data.getComment()!= null? data.getComment() : "");
                    txtNote.setText(data.getNote()!= null? data.getNote() : "");
                    if (data.getDateApproved() != null) {

                        try {

                            String isnegative = "+";
                            if (isNegative(data.getDateApprovedGMT())) {
                                isnegative = "";
                            }


                            txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getDateApproved()) + " GMT" + isnegative + String.valueOf(data.getDateApprovedGMT()));
                        } catch (Exception es) {
                            es.printStackTrace();
                        }
                    }

                } catch (Exception e){

                }
            }

        }


        txtVia.setText(method);

        if (data.getDateTime() != null) {
            try {

                String isnegative = "+";
                if (isNegative(data.getDateGMT())){
                    isnegative = "";
                }

                orderDate = sdf.parse(data.getDateTime());
                CharSequence days = android.text.format.DateFormat.format("EEEE", orderDate);
                dateString =  days + ", " + dateFormat.format(orderDate) + " GMT" + isnegative + String.valueOf(data.getDateGMT());
                mTxtStart.setText(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }



            if (!data.isIsLate()) {
                trTerlambat.setVisibility(View.GONE);
                mIconStatus.setBackgroundResource(R.drawable.status_accept);
                mTxtStatus.setText(getResources().getString(R.string.title_tidak_terlambat));
                mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

            } else {
                trTerlambat.setVisibility(View.VISIBLE);

                try {

                    Double temp1 = data.getLateTimeInMinute() / 60;
                    Double temp2 = data.getLateTimeInMinute() % 60;
                    Double temp3 = data.getLateTimeInMinute();

                    int hour = temp1.intValue();
                    int minut = temp2.intValue();
                    int total = temp3.intValue();

                    txtTerlambat.setText(hour + " " +
                            getResources().getString(R.string.title_jam) + " "
                            + minut + " " +
                            getResources().getString(R.string.title_menit)
                            +" ( " +
                            getResources().getString(R.string.title_total) + " "
                            + total + " " +
                            getResources().getString(R.string.title_menit) +" )");
                } catch (Exception e){

                }

                mIconStatus.setBackgroundResource(R.drawable.status_reject);
                mTxtStatus.setText(getResources().getString(R.string.title_terlambat));
                mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

            }
        }


        try {

            int height = SIZE_MARKER;
            int width = SIZE_MARKER;
            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
            Bitmap b=bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);


            Double lat = Double.valueOf(data.getLatitude().replace(",",""));
            Double longi = Double.valueOf(data.getLongitude().replace(",",""));
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, longi))
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                    .title(getResources().getString(R.string.title_lokasi_absen)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                    .zoom(15)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        } catch (Exception e){

        }

        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);

        try {

            if (data.getLatitude() != null && data.getLongitude() != null) {

                double latitude = Double.parseDouble(data.getLatitude());
                double longitude = Double.parseDouble(data.getLongitude());

                if (data.getAddress()!= null){

                    if (!data.getAddress().equals("")){

                        txtAlamat.setText(data.getAddress());

                    } else {

                        getAddress(this, latitude, longitude);

                    }

                } else {

                    getAddress(this, latitude, longitude);
                }


            }


            if (data.getLatitude() != null && data.getLongitude() != null) {
                double latitude = Double.parseDouble(data.getLatitude());
                double longitude = Double.parseDouble(data.getLongitude());
                getAddress(this, latitude, longitude);

            }
        } catch (Exception e){

        }


        if (data.getEmployee() != null) {
            if (data.getEmployee().getFirstName() != null) {
                profil_nama.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName() );
            }
            if (data.getEmployee().getNik() != null) {
                profil_role.setText(data.getEmployee().getNik());
            }
            if (data.getEmployee().getEmail() != null) {
                profil_email.setText(data.getEmployee().getEmail());
            }
            if (data.getEmployee().getPhone() != null) {
                profil_phone.setText(data.getEmployee().getPhone());
            }

            if (data.getEmployee().getPhone().equals("") || data.getEmployee().getPhone() == null) {

            } else{

                String id = (PreferenceManager.getDefaultSharedPreferences(DetailAttendancesActivity.this).getString(
                        getResources().getString(R.string.pref_employeeid), ""));

                if(!String.valueOf(data.getEmployee().getEmployeeID()).equals(id)) {

                    mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                        @Override
                        public void onClick(View v) {
                            final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                            CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(getResources().getString(R.string.title_aksi));
                            builder.setItems(colors, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                            // Check Permissions Now
                                            ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                        } else {
                                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.getApplicationContext().startActivity(intent);
                                        }
                                    } else if (which == 1) {
                                        Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                        Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                        mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(mSendSms);
                                    } else if (which == 2) {
                                        String name = data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName();
                                        String phone = data.getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                        contactIntent
                                                .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                                .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                        ((Activity) context).startActivityForResult(contactIntent, 1);

                                    } else if (which == 3) {

                                        voiceCall(profil_phone.getText().toString().trim());
                                    }
                                }
                            });
                            builder.show();
                        }
                    });
                }
            }

            if (data.getEmployee().getPhotoUrl() != null) {


                Glide.with(DetailAttendancesActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getEmployee().getPhotoUrl()))
                        .apply(requestOptions)
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);
                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getEmployee().getPhotoUrl());
                        startActivity(intent);
                    }
                });

            }
            else {
                // profile_image.setVisibility(View.GONE);
            }
        }




    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try{
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_json_maps));

            if (!isSuccess)
            {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        }
        catch (Resources.NotFoundException ex)
        {
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        else
        {
            mMap.getUiSettings().setMapToolbarEnabled(false);
          //  mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);
        }

      /*  Double lat = Double.valueOf(DataAttendance.getLatitude().replace(",",""));
        Double longi = Double.valueOf(DataAttendance.getLongitude().replace(",",""));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(lat, longi))
                .title("Lokasi Check in"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longi), 10));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(lat, longi))      // Sets the center of the map to location user
                .zoom(15)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/

    }

    void getAddress(Context context, double LATITUDE, double LONGITUDE) {

        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {



                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                txtAlamat.setText(address);
              /*  Log.d(TAG, "getAddress:  address" + address);
                Log.d(TAG, "getAddress:  city" + city);
                Log.d(TAG, "getAddress:  state" + state);
                Log.d(TAG, "getAddress:  postalCode" + postalCode);
                Log.d(TAG, "getAddress:  knownName" + knownName);*/

            }
        } catch (IOException e) {
            e.printStackTrace();
            txtAlamat.setText("-");
        }

    }

    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(DetailAttendancesActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(DetailAttendancesActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailAttendancesActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}