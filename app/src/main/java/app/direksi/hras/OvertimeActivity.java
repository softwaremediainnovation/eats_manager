package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.widget.ImageView;

import app.direksi.hras.fragment.ListLemburFragment;
import app.direksi.hras.fragment.MyLemburFragment;
import app.direksi.hras.fragment.FilterLemburFragment;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class OvertimeActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {

    private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
    private boolean mIsAvatarShown = true;

    private ImageView mProfileImage;
    private int mMaxScrollSize;


    public FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private FragmentRefreshListener1 fragmentRefreshListener1;




    public FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private FragmentRefreshListener2 fragmentRefreshListener2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(OvertimeActivity.this);
        setContentView(R.layout.activity_asset_new);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.title_lembur));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
        ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
        AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);



        appbarLayout.addOnOffsetChangedListener(this);
        mMaxScrollSize = appbarLayout.getTotalScrollRange();

        viewPager.setAdapter(new OvertimeActivity.TabsAdapter(getSupportFragmentManager(), OvertimeActivity.this));
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                //  Toast.makeText(CutiActivity.this, String.valueOf(position), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }



    public static void start(Context c) {
        c.startActivity(new Intent(c, CutiActivity.class));
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
    }

    private static class TabsAdapter extends FragmentPagerAdapter {
        private static final int TAB_COUNT = 2;
        private Context context;

        TabsAdapter(FragmentManager fm, Context cont) {
            super(fm);
            context = cont;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public Fragment getItem(int i) {

            final Fragment result;
            switch (i) {
                case 0:
                    // First Tab
                    result = ListLemburFragment.newInstance();
                    break;
                case 1:
                    // Second Tab
                    result = MyLemburFragment.newInstance();
                    break;

                default:
                    result = ListLemburFragment.newInstance();
                    break;
            }
            return result;



        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title ="";
            if (position == 0){
                title = context.getString(R.string.title_list_lembur);
            }
            if (position == 1){
                title = context.getString(R.string.title_lembur_ku);
            }
            return title;
        }
    }



    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cari, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                android.app.FragmentManager fm = getFragmentManager();
                FilterLemburFragment dialogFragment = new FilterLemburFragment();
                Bundle bundle = new Bundle();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, "Image Dialog");
                return true;
            case R.id.action_all:

                PreferenceManager.getDefaultSharedPreferences(OvertimeActivity.this).
                        edit().putString(getResources().getString(R.string.pref_dayoff_status), "0")
                        .apply();

                /*Intent intent = new Intent(OvertimeActivity.this, OvertimeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);*/
                if(getFragmentRefreshListener1()!=null){
                    getFragmentRefreshListener1().onRefresh();
                }
                if(getFragmentRefreshListener2()!=null){
                    getFragmentRefreshListener2().onRefresh();
                }


                return true;

            case R.id.action_pending:

                PreferenceManager.getDefaultSharedPreferences(OvertimeActivity.this).
                        edit().putString(getResources().getString(R.string.pref_dayoff_status), "1")
                        .apply();

               /* Intent intents = new Intent(OvertimeActivity.this, OvertimeActivity.class);
                intents.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intents);*/
                if(getFragmentRefreshListener1()!=null){
                    getFragmentRefreshListener1().onRefresh();
                }
                if(getFragmentRefreshListener2()!=null){
                    getFragmentRefreshListener2().onRefresh();
                }

                return true;
            case R.id.action_approve:

                PreferenceManager.getDefaultSharedPreferences(OvertimeActivity.this).
                        edit().putString(getResources().getString(R.string.pref_dayoff_status), "2")
                        .apply();

              /*  Intent intentss = new Intent(OvertimeActivity.this, OvertimeActivity.class);
                intentss.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentss);*/
                if(getFragmentRefreshListener1()!=null){
                    getFragmentRefreshListener1().onRefresh();
                }
                if(getFragmentRefreshListener2()!=null){
                    getFragmentRefreshListener2().onRefresh();
                }


                return true;
            case R.id.action_reject:

                PreferenceManager.getDefaultSharedPreferences(OvertimeActivity.this).
                        edit().putString(getResources().getString(R.string.pref_dayoff_status), "3")
                        .apply();

                /*Intent intentsss = new Intent(OvertimeActivity.this, OvertimeActivity.class);
                intentsss.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentsss);*/
                if(getFragmentRefreshListener1()!=null){
                    getFragmentRefreshListener1().onRefresh();
                }
                if(getFragmentRefreshListener2()!=null){
                    getFragmentRefreshListener2().onRefresh();
                }

                return true;

            case R.id.action_myresponse:

                PreferenceManager.getDefaultSharedPreferences(OvertimeActivity.this).
                        edit().putString(getResources().getString(R.string.pref_dayoff_status), "4")
                        .apply();

                /*Intent intentsss = new Intent(OvertimeActivity.this, OvertimeActivity.class);
                intentsss.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentsss);*/
                if(getFragmentRefreshListener1()!=null){
                    getFragmentRefreshListener1().onRefresh();
                }
               /* if(getFragmentRefreshListener2()!=null){
                    getFragmentRefreshListener2().onRefresh();
                }*/

                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        // Toast.makeText(getBaseContext(), ""+ selectedValue, Toast.LENGTH_LONG).show();
        if(getFragmentRefreshListener1()!=null){
            getFragmentRefreshListener1().onRefresh();
        }
        if(getFragmentRefreshListener2()!=null){
            getFragmentRefreshListener2().onRefresh();
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(OvertimeActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}
