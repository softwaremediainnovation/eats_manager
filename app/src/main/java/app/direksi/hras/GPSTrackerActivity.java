package app.direksi.hras;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * Created by dhimaz on 30/03/2019.
 */
public class GPSTrackerActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

    }

    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(Bundle bundle) {
        try {

            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                Intent intent = new Intent();
                intent.putExtra("Longitude", mLastLocation.getLongitude());
                intent.putExtra("Latitude", mLastLocation.getLatitude());
                setResult(1,intent);
                finish();






            }
        } catch (SecurityException e) {

            Intent intent = new Intent();
            intent.putExtra("Longitude", "0.0");
            intent.putExtra("Latitude", "0.0");
            setResult(1,intent);
            finish();

        }

    }

    @Override
    public void onConnectionSuspended(int i) {

        Intent intent = new Intent();
        intent.putExtra("Longitude", "0.0");
        intent.putExtra("Latitude", "0.0");
        setResult(1,intent);
        finish();

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

        Intent intent = new Intent();
        intent.putExtra("Longitude", "0.0");
        intent.putExtra("Latitude", "0.0");
        setResult(1,intent);
        finish();

    }
}