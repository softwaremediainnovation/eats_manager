package app.direksi.hras;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.ApproveLemburFragment;
import app.direksi.hras.model.DataLembur;
import app.direksi.hras.model.ResponseDetailLembur;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 14/05/2019.
 */

public class DetailLemburKuActivity extends AppCompatActivity {

    DataLembur dataLembur;
    TableLayout tbApprover;
    public TextView mTxtStart,
            mTxtEnd,
            mTxtStatus,
            mTxtNote,
            txtApprover,
            txtComment,
            txtDateApprove,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtDurasi,
            txtOrganisasi;
    private ImageView mIconStatus, profile_image;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;

    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;
    private Long id;
    private Button prosesDelete, prosesRequest;
    private static final int REQUEST_PHONE_CALL = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailLemburKuActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_lembur));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_lembur);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        dataLembur = gson.fromJson(getIntent().getStringExtra("detail"), DataLembur.class);
        txtDateApprove = findViewById(R.id.txtDateApprove);
        tbApprover = findViewById(R.id.tbApprover);
        tbApprover.setVisibility(View.GONE);
        txtApprover = findViewById(R.id.txtApprover);
        txtComment = findViewById(R.id.txtComment);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        mTxtNote = findViewById(R.id.mTxtNote);
        mTxtEnd = findViewById(R.id.mTxtEnd);
        mTxtStart = findViewById(R.id.mTxtStart);
        txtDurasi = findViewById(R.id.txtDurasi);
        //  mTxtJudul = findViewById(R.id.mTxtJudul);
        mLayoutImage = findViewById(R.id.mLayoutImage);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        mImageView = findViewById(R.id.mImageView);
        mIconStatus = findViewById(R.id.mIconStatus);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();
        prosesDelete = findViewById(R.id.prosesDelete);
        prosesRequest = findViewById(R.id.prosesRequest);
        prosesRequest.setVisibility(View.GONE);


        loading = new SweetAlertDialog(DetailLemburKuActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);

        if (dataLembur != null) {
            DetailLembur(dataLembur.getOvertimeID().intValue());
        }


        onNewIntent(getIntent());




    }
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        DetailLembur(idx);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /*public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApproveLembur();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  ProsesUpdate();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }*/
    public void prosesRequest(View v) {
        PreferenceManager.getDefaultSharedPreferences(DetailLemburKuActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataLembur.getOvertimeID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        ApproveLemburFragment dialogFragment = new ApproveLemburFragment();
        Bundle bundle = new Bundle();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");


    }

    public void prosesDelete(View v) {
        new android.app.AlertDialog.Builder(DetailLemburKuActivity.this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteData();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void DeleteData() {

        loading.show();

        try {




            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailLemburKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String idx = String.valueOf(id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.deleteLembur(idx, Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(DetailLemburKuActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.dismiss();

                           /* new SweetAlertDialog(DetailLemburKuActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("SUKSES")
                                    .setContentText("Berhasil hapus data")
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(DetailLemburKuActivity.this, OvertimeActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);



                                        }
                                    })
                                    .show();*/

                            SweetAlertDialog alertDialog = new SweetAlertDialog(DetailLemburKuActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                            alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                            alertDialog.setContentText("");
                            alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    Intent login = new Intent(DetailLemburKuActivity.this, OvertimeActivity.class);
                                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(login);


                                }
                            });
                            alertDialog.show();
                            Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                            btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                            btn.setTextColor(getResources().getColor(R.color.colorWhite));


                        }
                        else {
                            loading.dismiss();
                            try {
                            messageDialog.mShowMessageError(DetailLemburKuActivity.this,getResources().getString(R.string.title_gagal)
                                    , "");
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                            try {
                        messageDialog.mShowMessageError(DetailLemburKuActivity.this,getResources().getString(R.string.title_gagal)
                                , "");
                            }catch (Exception r){

                            }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.dismiss();
                            try {
                    messageDialog.mShowMessageError(DetailLemburKuActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
                        try {
            messageDialog.mShowMessageError(DetailLemburKuActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
        }

    }


    public void DetailLembur(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailLemburKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailLembur> call = api.getLemburDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDetailLembur>() {
                @Override
                public void onResponse(Call<ResponseDetailLembur> call, Response<ResponseDetailLembur> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            ShowData(response.body().getData());



                        }
                        else {
                            loading.dismiss();
                            try {
                            messageDialog.mShowMessageError(DetailLemburKuActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                            try {
                        messageDialog.mShowMessageError(DetailLemburKuActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailLembur> call, Throwable t) {
                    loading.dismiss();
                            try {
                    messageDialog.mShowMessageError(DetailLemburKuActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
                        try {
            messageDialog.mShowMessageError(DetailLemburKuActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
        }
    }

    private void ShowData(DataLembur data){
        id = data.getOvertimeID();
        prosesDelete.setVisibility(View.GONE);
        mTxtNote.setText(data.getNote()!=null ? data.getNote() : "");
        txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization() : "");
        if (data.getDateStart() != null)
            mTxtStart.setText(DefaultFormatter.changeFormatDate(data.getDateStart()));
        if (data.getDateEnd() != null)
            mTxtEnd.setText(DefaultFormatter.changeFormatDate(data.getDateEnd()));

        if (data.getIsApproved() == null){
            prosesDelete.setVisibility(View.VISIBLE);
            mIconStatus.setBackgroundResource(R.drawable.status_pending);
            mTxtStatus.setText("MENUNGGU");
            mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            mTxtStatus.setTextColor( ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

        }
        else {
            try {
                tbApprover.setVisibility(View.VISIBLE);
                txtApprover.setText(data.getApprover().getFirstName() + " " + data.getApprover().getLastName());
                txtComment.setText(data.getComment());
                if (data.getDateApproved() != null)
                    txtDateApprove.setText(DefaultFormatter.changeFormatDate(data.getDateApproved()));

                if (data.getIsApproved()) {
                    mIconStatus.setBackgroundResource(R.drawable.status_accept);
                    mTxtStatus.setText("DISETUJUI");
                    mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                    mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

                } else {
                    mIconStatus.setBackgroundResource(R.drawable.status_reject);
                    mTxtStatus.setText("DITOLAK");
                    mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                    mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

                }
            } catch (Exception e){

            }

        }




        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);



        if (data.getFilePath() != null) {


            Glide.with(DetailLemburKuActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.getFilePath()))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.getFilePath());
                    startActivity(intent);
                }
            });
        }
        else {
            mImageView.setVisibility(View.GONE);
        }

        if (data.getEmployee() != null) {
            if (data.getEmployee().getFirstName() != null) {
                profil_nama.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
            }
            if (data.getEmployee().getNik() != null) {
                profil_role.setText(data.getEmployee().getNik());
            }
            if (data.getEmployee().getEmail() != null) {
                profil_email.setText(data.getEmployee().getEmail());
            }
            if (data.getEmployee().getPhone() != null) {
                profil_phone.setText(data.getEmployee().getPhone());
            }

            /*if (data.getEmployee().getPhone().equals("") || data.getEmployee().getPhone() == null) {

            } else{
                profil_phone.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                    @Override
                    public void onClick(View v) {
                        final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                        CharSequence colors[] = new CharSequence[]{"Panggil", "Kirim SMS", "Simpan Kontak"};

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle("Aksi");
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // Check Permissions Now
                                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(intent);
                                    }
                                } else if (which == 1) {
                                    Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                    Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                    mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(mSendSms);
                                } else if (which == 2) {
                                    String name = data.getEmployee().getFullName();
                                    String phone = data.getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                    contactIntent
                                            .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                            .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                    ((Activity) context).startActivityForResult(contactIntent, 1);

                                } //else if (which == 3) {
//                                PackageManager pm = context.getPackageManager();
//                                try {
//                                    PackageInfo info = pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
//
//                                    Uri uri = Uri.parse("smsto:" + viewHolder.tv_tlp.getText().toString().trim());
//                                    Intent i = new Intent(Intent.ACTION_SENDTO, uri);
//                                    i.setPackage("com.whatsapp");
//                                    context.getApplicationContext().startActivity(Intent.createChooser(i, ""));
////                                    context.getApplicationContext().startActivity(i);
//
//                                } catch (PackageManager.NameNotFoundException e) {
//                                    Toast.makeText((Activity) context, "WhatsApp not Installed", Toast.LENGTH_SHORT).show();
//                                }
//                            }
                            }
                        });
                        builder.show();
                    }
                });
            }*/

            if (data.getEmployee().getPhotoUrl() != null) {

                Glide.with(DetailLemburKuActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getEmployee().getPhotoUrl()))
                        .apply(requestOptions)
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);
                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getEmployee().getPhotoUrl());
                        startActivity(intent);
                    }
                });

            }
            else {
                // profile_image.setVisibility(View.GONE);
            }
        }

        if (data.getDateStart() != null && data.getDateEnd() != null) {
            try {
                //Dates to compare
                String CurrentDate = data.getDateStart();
                String FinalDate = data.getDateEnd();

                Date date1;
                Date date2;

                SimpleDateFormat dates = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

                //Setting dates
                date1 = dates.parse(CurrentDate);
                date2 = dates.parse(FinalDate);

                //Comparing dates
                double difference = Math.abs(date1.getTime() - date2.getTime());
                double differenceDates = difference / (60 * 1000);

                //Convert long to String
                // String dayDifference = Long.toString(differenceDates);

                //Log.e("HERE", "HERE: " + dayDifference);

                Double temp1 = differenceDates/60;
                Double temp2 = differenceDates % 60;
                Double temp3 =  differenceDates;

                int hour =  temp1.intValue();
                int minut = temp2.intValue();
                int total = temp3.intValue();

                //tbLast.setVisibility(View.VISIBLE);
                // String value = ("Jumlah lembur dalam 1 bulan adalah  <b>" + hour +  " jam " +  minut + " menit ( Total " + total+ " menit )</b>");
                //txtLastTitle.setText(Html.fromHtml(value));


                txtDurasi.setText(hour + " " +
                        getResources().getString(R.string.title_jam) + " "
                        + minut + " " +
                        getResources().getString(R.string.title_menit)
                        +" ( " +
                        getResources().getString(R.string.title_total) + " "
                        + total + " " +
                        getResources().getString(R.string.title_menit) +" )");

            } catch (Exception exception) {
               txtDurasi.setText("");
                // Log.e("DIDN'T WORK", "exception " + exception);
            }
        } else {
           txtDurasi.setText("");
        }
    }


    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailLemburKuActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}
