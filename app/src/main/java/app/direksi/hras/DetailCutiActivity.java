package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.Iterator;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.ApproveCutiFragment;
import app.direksi.hras.model.DataCuti;
import app.direksi.hras.model.ResponseDetailCuti;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.CanApproveVoid;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

public class DetailCutiActivity extends AppCompatActivity {

    DataCuti dataCutiItem;
    TableLayout tbApprover;


    public TextView  mTxtStart,
            mTxtEnd,
            mTxtReason, mTxtStatus,
            mTxtNote,
            mTxtTgl1,
            mTxtHowManyDay,
            txtApprover,
            txtComment,
            txtDateApprove,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtOrganisasi,
            txtisDesease;
    private ImageView mIconStatus, profile_image;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;
    private Long id;
    private Button prosesDelete, prosesRequest;
    private static final int REQUEST_PHONE_CALL = 1;
    private TableRow trDesease;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailCutiActivity.this);

        //remove line in bar
      //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_dayoff));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_day_off);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


       // dataCutiItem = getIntent().getParcelableExtra("detail");
        Gson gson = new Gson();
        dataCutiItem = gson.fromJson(getIntent().getStringExtra("detail"), DataCuti.class);
        txtDateApprove = findViewById(R.id.txtDateApprove);
        tbApprover = findViewById(R.id.tbApprover);
        tbApprover.setVisibility(View.GONE);
        txtApprover = findViewById(R.id.txtApprover);
        txtComment = findViewById(R.id.txtComment);
        mTxtStatus = findViewById(R.id.mTxtStatus);
        mTxtNote = findViewById(R.id.mTxtNote);
        mTxtReason = findViewById(R.id.mTxtReason);
        mTxtEnd = findViewById(R.id.mTxtEnd);
        mTxtStart = findViewById(R.id.mTxtStart);
      //  mTxtJudul = findViewById(R.id.mTxtJudul);
        mLayoutImage = findViewById(R.id.mLayoutImage);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        mImageView = findViewById(R.id.mImageView);
        mIconStatus = findViewById(R.id.mIconStatus);
        mTxtTgl1 = findViewById(R.id.mTxtTgl1);
        mTxtHowManyDay = findViewById(R.id.mTxtHowManyDay);
        prosesDelete = findViewById(R.id.prosesDelete);
        prosesRequest = findViewById(R.id.prosesRequest);

        trDesease = findViewById(R.id.trDesease);
        txtisDesease = findViewById(R.id.txtisDesease);

        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();

        loading = new SweetAlertDialog(DetailCutiActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        profile_image = findViewById(R.id.profile_image);
        profil_nama = findViewById(R.id.profil_nama);
        profil_role = findViewById(R.id.profil_role);
        profil_email = findViewById(R.id.profil_email);
        profil_phone = findViewById(R.id.profil_phone);
        txtOrganisasi = findViewById(R.id.txtOrganisasi);

        if (dataCutiItem != null) {
            DetailCuti(dataCutiItem.getDayOffID().intValue());
        } else {
            onNewIntent(getIntent());
        }

    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataCutiItem = new DataCuti();
                        dataCutiItem.setDayOffID(Long.parseLong(String.valueOf(idx)));
                        DetailCuti(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    /*public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       *//* ApproveCuti();*//*
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                      //  ProsesUpdate();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }
*/
    public void prosesRequest(View v) {
        PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataCutiItem.getDayOffID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        ApproveCutiFragment dialogFragment = new ApproveCutiFragment();
        Bundle bundle = new Bundle();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");


    }

    public void prosesDelete(View v) {
        new android.app.AlertDialog.Builder(DetailCutiActivity.this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteData();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void DeleteData() {

        loading.show();

        try {




            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String idx = String.valueOf(id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.deleteCuti(idx, Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.dismiss();

                            new SweetAlertDialog(DetailCutiActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getResources().getString(R.string.title_sukses))
                                    .setContentText(getResources().getString(R.string.title_berhasil_hapus_data))
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(DetailCutiActivity.this, ListCutiActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);



                                        }
                                    })
                                    .show();


                        }
                        else {
                            loading.dismiss();
                            try {
                            messageDialog.mShowMessageError(DetailCutiActivity.this,getResources().getString(R.string.title_gagal)
                                    , "");
                        } catch (Exception e){

                        }
                        }

                    } else {
                        loading.dismiss();
                        try {
                        messageDialog.mShowMessageError(DetailCutiActivity.this,getResources().getString(R.string.title_gagal)
                                , "");
                } catch (Exception e){

                }
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.dismiss();
                    try {
                    messageDialog.mShowMessageError(DetailCutiActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
        } catch (Exception e){

        }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
            messageDialog.mShowMessageError(DetailCutiActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        } catch (Exception d){

        }
        }

    }


    public void DetailCuti( int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailCuti> call = api.getCutiDetail(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDetailCuti>() {
                @Override
                public void onResponse(Call<ResponseDetailCuti> call, Response<ResponseDetailCuti> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_idProses_employee), response.body().getData().getEmployee().getEmployeeID().toString())
                                    .apply();
                            ShowData(response.body().getData());



                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailCutiActivity.this, getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                            } catch ( Exception e){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                        messageDialog.mShowMessageError(DetailCutiActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                        } catch ( Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailCuti> call, Throwable t) {
                    loading.dismiss();
                    try {
                    messageDialog.mShowMessageError(DetailCutiActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                    } catch ( Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
            messageDialog.mShowMessageError(DetailCutiActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
            } catch ( Exception f){

            }
        }
    }

    private void ShowData(DataCuti detailCuti){
        id = detailCuti.getDayOffID();
        if (detailCuti.getStart() != null)
            mTxtStart.setText(DefaultFormatter.changeFormatDateWithOutHour(detailCuti.getStart()));
        if (detailCuti.getStart() != null)
            mTxtEnd.setText(DefaultFormatter.changeFormatDateWithOutHour(detailCuti.getEnd()));
        mTxtReason.setText(detailCuti.getConstanta().getName());
        mTxtNote.setText(detailCuti.getNote());
        prosesDelete.setVisibility(View.GONE);
        if (detailCuti.getIsApproved() == null){
            mIconStatus.setBackgroundResource(R.drawable.status_pending);
            mTxtStatus.setText(getResources().getString(R.string.title_menunggu));
            mTxtStatus.setBackgroundResource(R.drawable.rounded_gray);
            mTxtStatus.setTextColor( ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

        }
        else {
            try {
                tbApprover.setVisibility(View.VISIBLE);
                txtApprover.setText(detailCuti.getApprover().getFirstName() + " " + detailCuti.getApprover().getLastName());
                txtComment.setText(detailCuti.getComment());
                if (detailCuti.getDateApproved() != null)
                    txtDateApprove.setText(DefaultFormatter.changeFormatDate(detailCuti.getDateApproved()));

                if (detailCuti.getIsApproved()) {
                    mIconStatus.setBackgroundResource(R.drawable.status_accept);
                    mTxtStatus.setText(getResources().getString(R.string.title_disetujui));
                    mTxtStatus.setBackgroundResource(R.drawable.rounded_green);
                    mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

                } else {
                    mIconStatus.setBackgroundResource(R.drawable.status_reject);
                    mTxtStatus.setText(getResources().getString(R.string.title_ditolak));
                    mTxtStatus.setBackgroundResource(R.drawable.rounded_red);
                    mTxtStatus.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorWhite));

                }
            } catch (Exception e){

            }

        }

        if (detailCuti.getDateCreated() != null)
            mTxtTgl1.setText(DefaultFormatter.changeFormatDate(detailCuti.getDateCreated()));


        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);


        if (detailCuti.getConstanta().getName().equals("Sakit") || detailCuti.getConstanta().getName().toLowerCase().equals("sakit") || detailCuti.getConstanta().getName().contains("Sakit")){

            trDesease.setVisibility(View.VISIBLE);

            if (detailCuti.getIsDisease()!= null){

                txtisDesease.setText( detailCuti.getIsDisease() == true? getResources().getString(R.string.title_ada) : getResources().getString(R.string.title_tidak_ada));

            } else {

                txtisDesease.setText(getResources().getString(R.string.title_tidak_ada));
            }
        } else {

            trDesease.setVisibility(View.GONE);

        }

        if (detailCuti.getPath() != null) {

            if (detailCuti.getPath() != "") {

                Glide.with(DetailCutiActivity.this)
                        .load((getResources().getString(R.string.base_url) + detailCuti.getPath()))
                        .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                        .apply(requestOptions)
                        .error(Glide.with(mImageView).load(R.drawable.no_picture))
                        .into(mImageView);
                mImageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", detailCuti.getPath());
                        startActivity(intent);
                    }
                });
            } else {
                mImageView.setVisibility(View.GONE);
            }

        }
        else {
            mImageView.setVisibility(View.GONE);
        }

        mTxtHowManyDay.setText(DefaultFormatter.formatDefaultDouble(detailCuti.getHowManyDays()) + " " + getResources().getString(R.string.title_hari));

        profil_nama.setText(detailCuti.getEmployee().getFirstName()!= null? detailCuti.getEmployee().getFirstName() + " " + detailCuti.getEmployee().getLastName() : " ");
        profil_role.setText(detailCuti.getEmployee().getNik() !=null ? detailCuti.getEmployee().getNik() : " ");
        profil_email.setText(detailCuti.getEmployee().getEmail() != null? detailCuti.getEmployee().getEmail() : " ");
        profil_phone.setText(detailCuti.getEmployee().getPhone()!=null? detailCuti.getEmployee().getPhone() : " ");
        txtOrganisasi.setText(detailCuti.getOrganization().getName()!=null? detailCuti.getOrganization().getName() : " ");

        if (detailCuti.getEmployee().getPhone().equals("") || detailCuti.getEmployee().getPhone() == null) {

        } else{
            mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                @Override
                public void onClick(View v) {
                    final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                    CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(getResources().getString(R.string.title_aksi));
                    builder.setItems(colors, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (which == 0) {
                                if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    // Check Permissions Now
                                    ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                } else {
                                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(intent);
                                }
                            } else if (which == 1) {
                                Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.getApplicationContext().startActivity(mSendSms);
                            } else if (which == 2) {
                                String name = detailCuti.getEmployee().getFirstName() + " "+ detailCuti.getEmployee().getLastName();
                                String phone = detailCuti.getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                contactIntent
                                        .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                        .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                ((Activity) context).startActivityForResult(contactIntent, 1);

                            } else if (which == 3) {

                                voiceCall(profil_phone.getText().toString().trim());
                            }
                        }
                    });
                    builder.show();
                }
            });
        }

        if (detailCuti.getEmployee().getPhotoUrl() != null) {

            Glide.with(DetailCutiActivity.this)
                    .load((getResources().getString(R.string.base_url) + detailCuti.getEmployee().getPhotoUrl()))
                    .apply(requestOptions)
                    .error(Glide.with(profile_image).load(R.drawable.profile))
                    .into(profile_image);
            profile_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", detailCuti.getEmployee().getPhotoUrl());
                    startActivity(intent);
                }
            });

        }
        else {
           // profile_image.setVisibility(View.GONE);
        }


        String approval = (PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).getString(
                getResources().getString(R.string.pref_CanApproveDayOff), ""));
        String id = (PreferenceManager.getDefaultSharedPreferences(DetailCutiActivity.this).getString(
                getResources().getString(R.string.pref_employeeid), ""));

        if (approval.equals("true")){

            if (detailCuti.getEmployee().getEmployeeID().toString().equals(id)){
                prosesRequest.setVisibility(View.GONE);
            } else {

                if (CanApproveVoid){
                    prosesRequest.setVisibility(View.VISIBLE);
                }else{

                    if (detailCuti.getIsApproved() == null){

                        prosesRequest.setVisibility(View.VISIBLE);
                    }else {
                        prosesRequest.setVisibility(View.GONE);
                    }

                }

            }
        } else {
            prosesRequest.setVisibility(View.GONE);
        }





    }




    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){

            MDToast.makeText(DetailCutiActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(DetailCutiActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailCutiActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
