package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

import android.preference.PreferenceManager;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import app.direksi.hras.util.MyContextWrapper;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 13/03/2019.
 */

public class ListLaporanActivity extends AppCompatActivity {

    RelativeLayout rl1, rl2, rl3, rl4, rl5, rl6, rl7, rl8, rl9, rl10, rl11, rl12, rl13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(ListLaporanActivity.this);
        setContentView(R.layout.activity_list_laporan);
       /* setTitle("Detail Staff");*/



        // getSupportActionBar().setTitle("Daftar Laporan");
     //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        rl1 = (RelativeLayout) findViewById(R.id.rl1);
        rl1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /*   GoTo("com.mii.erp.FilterAActivity");*/
              //  Toast.makeText(getApplicationContext(), "Laporan Komparasi Penjualan", Toast.LENGTH_SHORT).show();
            }
        });
        rl2 = (RelativeLayout) findViewById(R.id.rl2);
        rl2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // GoTo("com.mii.erp.FilterAActivity");
                // Toast.makeText(getApplicationContext(), "Laporan Periodik Kontrak Titik Lokasi", Toast.LENGTH_SHORT).show();
            }
        });
        rl3 = (RelativeLayout) findViewById(R.id.rl3);
        rl3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  GoTo("com.mii.erp.FilterAActivity");*/
               // Toast.makeText(getApplicationContext(), "Laporan Periodik Listrik", Toast.LENGTH_SHORT).show();
            }
        });
        rl4 = (RelativeLayout) findViewById(R.id.rl4);
        rl4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  GoTo("com.mii.erp.FilterAActivity");*/
              //  Toast.makeText(getApplicationContext(), "Laporan Periodik Penawaran", Toast.LENGTH_SHORT).show();
            }
        });
        rl5 = (RelativeLayout) findViewById(R.id.rl5);
        rl5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* GoTo("com.mii.erp.FilterAActivity");*/
               // Toast.makeText(getApplicationContext(), "Laporan Periodik Penjualan", Toast.LENGTH_SHORT).show();
            }
        });
        rl6 = (RelativeLayout) findViewById(R.id.rl6);
        rl6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              /*  GoTo("com.mii.erp.FilterAActivity");*/
               // Toast.makeText(getApplicationContext(), "Laporan Periodik Periode", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void GoTo(String page){
        Intent i = new Intent().setClassName(getApplicationContext(), page);
        startActivity(i);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*moveTaskToBack(true);
        finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(ListLaporanActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }
}
