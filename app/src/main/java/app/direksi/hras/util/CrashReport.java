package app.direksi.hras.util;

import android.content.Context;
import android.content.Intent;
import com.google.android.material.snackbar.Snackbar;
import android.view.View;
import android.widget.Toast;


import app.direksi.hras.R;


/**
 * Created by warnawarni on 9/15/2017.
 */

public class CrashReport {

    private Snackbar snackbar;

    public void mSendCrashReport(final Context context, final String mTag, final String mBody, View view) {

        try {
            snackbar = Snackbar
                    .make(view, context.getResources().getString(R.string.loading_error), Snackbar.LENGTH_LONG)
                    .setAction(context.getResources().getString(R.string.txt_report), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                            i.putExtra(Intent.EXTRA_EMAIL, new String[]{context.getResources().getString(R.string.txt_email_mbakmu)});
                            i.putExtra(Intent.EXTRA_SUBJECT, "Crash Report " + mTag);
                            i.putExtra(Intent.EXTRA_TEXT, mBody);
                            try {
                                context.startActivity(Intent.createChooser(i, context.getResources().getString(R.string.txt_send_email)));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(context, context.getResources().getString(R.string.txt_no_email), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

            snackbar.show();
        } catch (Exception e) {
        }


    }


}
