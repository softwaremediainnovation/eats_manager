package app.direksi.hras.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.preference.PreferenceManager;

import java.util.Locale;

import app.direksi.hras.R;
import app.direksi.hras.RequestAttendanceActivity;
import app.direksi.hras.SplashScreenActivity;

public class MyContextWrapper extends ContextWrapper {

    public MyContextWrapper(Context base) {
        super(base);
    }

    @SuppressWarnings("deprecation")
    public static ContextWrapper wrap(Context context, String language) {
        Configuration config = context.getResources().getConfiguration();
        Locale sysLocale = null;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
            sysLocale = getSystemLocale(config);
        } else {
            sysLocale = getSystemLocaleLegacy(config);
        }
        if (!language.equals("") && !sysLocale.getLanguage().equals(language)) {
            Locale locale = new Locale(language);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setSystemLocale(config, locale);
            } else {
                setSystemLocaleLegacy(config, locale);
            }

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context = context.createConfigurationContext(config);
        } else {
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
        return new MyContextWrapper(context);
    }

    @SuppressWarnings("deprecation")
    public static Locale getSystemLocaleLegacy(Configuration config){
        return config.locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static Locale getSystemLocale(Configuration config){
        return config.getLocales().get(0);
    }

    @SuppressWarnings("deprecation")
    public static void setSystemLocaleLegacy(Configuration config, Locale locale){
        config.locale = locale;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public static void setSystemLocale(Configuration config, Locale locale){
        config.setLocale(locale);
    }


    public static ContextWrapper changeLang(Context context, String lang_code){
        Locale sysLocale;

        Resources rs = context.getResources();
        Configuration config = rs.getConfiguration();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            sysLocale = config.getLocales().get(0);
        } else {
            sysLocale = config.locale;
        }
        if (!lang_code.equals("") && !sysLocale.getLanguage().equals(lang_code)) {
            Locale locale = new Locale(lang_code);
            Locale.setDefault(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                config.setLocale(locale);
            } else {
                config.locale = locale;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                context = context.createConfigurationContext(config);
            } else {
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
        }

        return new ContextWrapper(context);
    }


    public static void refreshBahasa(Context context)
    {

        try {
            String bahasa = (PreferenceManager.getDefaultSharedPreferences(context).getString(
                   context.getResources().getString(R.string.pref_bahasa), ""));

            if (bahasa.equals("") || bahasa.equals(null)) {

                bahasa = "en";
            }

            Locale locale = new Locale(bahasa);
            Configuration config = new Configuration(context.getResources().getConfiguration());
            Locale.setDefault(locale);
            config.setLocale(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                context.createConfigurationContext(config);
            } else {
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
        } catch (Exception e){

        }

    }

    public static void refreshBahasaIndonesia(Context context)
    {

        try {
            String bahasa = (PreferenceManager.getDefaultSharedPreferences(context).getString(
                    context.getResources().getString(R.string.pref_bahasa), ""));

            if (bahasa.equals("") || bahasa.equals(null)) {

                bahasa = "in";

                PreferenceManager.getDefaultSharedPreferences(context).
                        edit().putString(context.getResources().getString(R.string.pref_bahasa), "in")
                        .apply();
            }

            Locale locale = new Locale(bahasa);
            Configuration config = new Configuration(context.getResources().getConfiguration());
            Locale.setDefault(locale);
            config.setLocale(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                context.createConfigurationContext(config);
            } else {
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }
        } catch (Exception e){

        }

    }

    public static void refreshBahasaDefault(Context context)
    {

        try {
            String bahasa = (PreferenceManager.getDefaultSharedPreferences(context).getString(
                    context.getResources().getString(R.string.pref_bahasa), ""));

            if (bahasa.equals("") || bahasa.equals(null)) {
                bahasa = Locale.getDefault().getLanguage();
                if (bahasa.equals("en")){

                    UpdateLanguage(context, "en");

                    PreferenceManager.getDefaultSharedPreferences(context).
                            edit().putString(context.getResources().getString(R.string.pref_bahasa), "en")
                            .apply();

                    // Toast.makeText(SplashScreenActivity.this, "en1", Toast.LENGTH_LONG).show();

                } else if (bahasa.equals("in")){

                    UpdateLanguage(context, "in");

                    PreferenceManager.getDefaultSharedPreferences(context).
                            edit().putString(context.getResources().getString(R.string.pref_bahasa), "in")
                            .apply();

                    //  Toast.makeText(SplashScreenActivity.this, "in", Toast.LENGTH_LONG).show();

                } else {

                    UpdateLanguage(context, "en");
                    PreferenceManager.getDefaultSharedPreferences(context).
                            edit().putString(context.getResources().getString(R.string.pref_bahasa), "en")
                            .apply();

                    //  Toast.makeText(SplashScreenActivity.this, "en2", Toast.LENGTH_LONG).show();

                }

                //  Toast.makeText(SplashScreenActivity.this, bahasa, Toast.LENGTH_LONG).show();

            } else {
                UpdateLanguage(context, bahasa);
            }

        } catch (Exception e){

            // Toast.makeText(SplashScreenActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }

    }

    public static void UpdateLanguage(Context context, String bahasa){


        try {
            Locale locale = new Locale(bahasa);
            Configuration config = new Configuration(context.getResources().getConfiguration());
            Locale.setDefault(locale);
            config.setLocale(locale);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                context.createConfigurationContext(config);
            } else {
                context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            }

        } catch (Exception e){

        }

    }


}