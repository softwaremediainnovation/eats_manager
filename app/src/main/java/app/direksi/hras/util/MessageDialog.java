package app.direksi.hras.util;

import android.app.Activity;
import android.widget.Button;

import app.direksi.hras.R;
import cn.pedant.SweetAlert.SweetAlertDialog;


public class MessageDialog {

    public void mShowMessageError(Activity activity, String title, String message) {
     /*   new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(title)
                .setContentText(message)
                .show();*/



        SweetAlertDialog alertDialog = new SweetAlertDialog(activity,SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(title);
        alertDialog.setContentText(message);
        alertDialog.show();

        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(activity.getResources().getColor(R.color.colorPrimary));
        btn.setTextColor(activity.getResources().getColor(R.color.colorWhite));
    }

}
