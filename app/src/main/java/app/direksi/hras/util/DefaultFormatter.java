package app.direksi.hras.util;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Created by warnawarni on 4/8/2017.
 */

public class DefaultFormatter {
    public static String formatDefaultDouble(double n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        DecimalFormat df = new DecimalFormat("#.###########################", dfs);
        return df.format(n);
    }

    public static String formatDefaultLong(long n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("#,###", dfs);
        return df.format(n);
    }

    public static String formatDefaultCurrency(double n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("#,###", dfs);

        return df.format(n);
    }

    public static String formatDefaultCurrencyRupiah(double n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("###,###,###.##", dfs);

        return df.format(n);
    }


    public static String formatSpesifikasi(int luastanah, int luasbangunan, String kt, String km) {

        return "LT : " + luastanah + " LB : " + luasbangunan + " KT : " + kt + " KM : " + km;

    }

    public static String formatCurrencyDoku(double n) {
        NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
        DecimalFormat formatter = (DecimalFormat) nf;
        formatter.applyPattern("####.00");

        return formatter.format(n);
    }

    public static String formatDefaultRupiah(double n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("Rp ###,###,###.##", dfs);

        return df.format(n);
    }

    public static String formatDefaultIDR (double n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator('.');
        DecimalFormat df = new DecimalFormat("IDR ###,###,###.##", dfs);

        return df.format(n);
    }


    public static String formatTime(String hourOfDay, String minuteOfDay) {

        int hour = Integer.parseInt(hourOfDay);
        int minute = Integer.parseInt(minuteOfDay);
        String hours = "", minutes = "";

        if (minute > 45) {
            minute = 0;
            if (hour == 23) {
                hour = 0;
            } else {
                hour += 1;
            }
        } else {
            if (minute > 30) {
                minute = 45;
            } else if (minute > 15) {
                minute = 30;
            } else if (minute > 0) {
                minute = 15;
            }
        }

        if (hour < 10)
            hours = "0" + hour;
        else
            hours = Integer.toString(hour);


        if (minute < 10)
            minutes = "0" + minute;
        else
            minutes = Integer.toString(minute);


        return hours + ":" + minutes;
    }

    public static String mGetHourMinute(double late) {

        String mDate = "", mShowDate = "", mStringTotal = "";


        int diffHours = (int) (late / 60);
        double modMinutes = late % 60;

        Log.v("DefaultFormatter", diffHours + " diffHours");
        Log.v("DefaultFormatter", modMinutes + " modMinutes");

        if (diffHours > 0) {
            mDate = DefaultFormatter.formatDefaultDouble(diffHours) + " jam";
            mStringTotal = " ( Total " + DefaultFormatter.formatDefaultTwoDigit(late) + " menit )";
        }

        if (modMinutes > 0) {
            mShowDate = " " + DefaultFormatter.formatDefaultDouble(modMinutes) + " menit";
        } else {

            if (!mDate.isEmpty()) {
                mShowDate = "";
            } else {
                mShowDate = "0 menit";
            }

        }


        return mDate + "" + mShowDate + "" + mStringTotal;


        // Log.e("toyBornTime", "" + toyBornTime);

    }

    public static String formatDefaultTwoDigit(double n) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("###,###,###.##", dfs);

        return df.format((int) Math.ceil(n));
    }

    public static String formatDateRelative(Context context, Date time) {
        String resultDate = "";
        try {
            Calendar c = Calendar.getInstance();
            if (time.getTime() >= c.getTimeInMillis() - DateUtils.MINUTE_IN_MILLIS) {// && time.getTime() <= c.getTimeInMillis()) {
//				return context.getResources().getString(R.string.news_feed_just_now);
                Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
                calendar.setTime(time);   // assigns calendar to given date
//				calendar.get(Calendar.HOUR_OF_DAY); // gets hour in 24h format
//				calendar.get(Calendar.HOUR);        // gets hour in 12h format
//				calendar.get(Calendar.MONTH);
                resultDate = (String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)).length() == 1 ? ("0" + String.valueOf(calendar.get(Calendar.HOUR_OF_DAY))) : String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)))
                        + ":" + (String.valueOf(calendar.get(Calendar.MINUTE)).length() == 1 ? ("0" + String.valueOf(calendar.get(Calendar.MINUTE))) : String.valueOf(calendar.get(Calendar.MINUTE)));
            } else {
                resultDate = (String) DateUtils.getRelativeTimeSpanString(time.getTime(), Calendar.getInstance().getTimeInMillis(),
                        DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
            }
        } catch (Exception e) {
            try {
                Calendar c = Calendar.getInstance();
                c.setTime(time);
                Locale locale = new Locale("en");
                resultDate = c.get(Calendar.DATE) + " " + (new SimpleDateFormat("MMM", locale)).format(c.getTime());
            } catch (Exception e2) {
                resultDate = "";
            }

        }

        String[] temp = resultDate.replace(",", "").split(" ");
        if (resultDate.contains("hours") || resultDate.contains("hour")) {
            return temp[0] + " jam lalu";
        } else if (resultDate.contains("days")) {
            return temp[0] + " hari lalu";
        } else if (resultDate.contains("min") || resultDate.contains("mins")) {
            return temp[0] + " menit lalu";
        } else if (resultDate.contains("yesterday") || resultDate.contains("Yesterday")) {
            return "1 hari lalu";
        } else {

            if (temp[0].contains("January")) {
                return temp[1] + " " + "Januari";
            } else if (temp[0].contains("February")) {
                return temp[1] + " " + "Februari";
            } else if (temp[0].contains("March")) {
                return temp[1] + " " + "Maret";
            } else if (temp[0].contains("April")) {
                return temp[1] + " " + "April";
            } else if (temp[0].contains("May")) {
                return temp[1] + " " + "Mei";
            } else if (temp[0].contains("June")) {
                return temp[1] + " " + "Juni";
            } else if (temp[0].contains("July")) {
                return temp[1] + " " + "Juli";
            } else if (temp[0].contains("August")) {
                return temp[1] + " " + "Agustus";
            } else if (temp[0].contains("September")) {
                return temp[1] + " " + "September";
            } else if (temp[0].contains("October")) {
                return temp[1] + " " + "Oktober";
            } else if (temp[0].contains("November")) {
                return temp[1] + " " + "November";
            } else if (temp[0].contains("December")) {
                return temp[1] + " " + "Desember";
            }
            try {
                if (temp[1].contains("January")) {
                    return temp[0] + " " + "Januari";
                } else if (temp[1].contains("February")) {
                    return temp[0] + " " + "Februai";
                } else if (temp[1].contains("March")) {
                    return temp[0] + " " + "Maret";
                } else if (temp[1].contains("April")) {
                    return temp[0] + " " + "April";
                } else if (temp[1].contains("May")) {
                    return temp[0] + " " + "Mei";
                } else if (temp[1].contains("June")) {
                    return temp[0] + " " + "Juni";
                } else if (temp[1].contains("July")) {
                    return temp[0] + " " + "Juli";
                } else if (temp[1].contains("August")) {
                    return temp[0] + " " + "Agustus";
                } else if (temp[1].contains("September")) {
                    return temp[0] + " " + "September";
                } else if (temp[1].contains("October")) {
                    return temp[0] + " " + "Oktober";
                } else if (temp[1].contains("November")) {
                    return temp[0] + " " + "November";
                } else if (temp[1].contains("December")) {
                    return temp[0] + " " + "Desember";
                }
            } catch (Exception e) {

            }


            return resultDate;
        }

    }


    public static String changeFormatDate(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String getMonthAndYear(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateWithDayName(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");

        try {
            orderDate = sdf.parse(date);
            CharSequence days = android.text.format.DateFormat.format("EEEE", orderDate);
            dateString =  days + ", " + dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateWithDayNameWithSecond(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");

        try {
            orderDate = sdf.parse(date);
            CharSequence days = android.text.format.DateFormat.format("EEEE", orderDate);
            dateString =  days + ", " + dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }


    public static String changeFormatDateWithOutHour(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateHourOnly(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDOnlyMonthYear(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateOnlyDateNumber(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateOnlyMonthNumber(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateWithOutHourFullNameMonth(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateYearMonthDay(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateWithOutHourFull(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");

        try {
            orderDate = sdf.parse(date);
            CharSequence days = android.text.format.DateFormat.format("EEEE", orderDate);
            dateString =  days + ", " + dateFormat.format(orderDate);
            //dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateWithOutYear(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static String changeFormatDateWithOutDay(String date) {
        Date orderDate = null;
        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM yyyy");

        try {
            orderDate = sdf.parse(date);
            dateString = dateFormat.format(orderDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return dateString;
    }

    public static Calendar changeFormatToDate(String date) {
        Calendar cal = Calendar.getInstance();
        Date orderDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        try {
            orderDate = sdf.parse(date);
            cal.setTime(orderDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return cal;
    }

    public static Calendar changeFormatToDateWithoutSecond(String date) {
        Calendar cal = Calendar.getInstance();
        Date orderDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");

        try {
            orderDate = sdf.parse(date);
            cal.setTime(orderDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return cal;
    }

    public static boolean isSameDateTime(Calendar cal1, Calendar cal2) {
        // compare if is the same ERA, YEAR, DAY, HOUR, MINUTE and SECOND
        return (cal1.get(Calendar.HOUR_OF_DAY) == cal2.get(Calendar.HOUR_OF_DAY)
                && cal1.get(Calendar.MINUTE) == cal2.get(Calendar.MINUTE)
                );
    }

    public static String changeFormatDateToString(Date date) {

        String dateString = "";

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");

        try {

            dateString = dateFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return dateString;
    }



    public static int calculateNumberOfDaysExcludeWeekends(Date startDate, Date endDate) {
        if (startDate.after(endDate)) {
            throw new IllegalArgumentException("End date should be grater or equals to start date");
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);

        int numOfDays = 0;
        while(!calendar.getTime().after(endDate)) {
            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
            if ( (dayOfWeek>1) && (dayOfWeek<7) ) {
                numOfDays++;
            }
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        return numOfDays;
    }

    public static boolean isNegative(double d) {
        return Double.compare(d, 0.0) < 0;
    }



}
