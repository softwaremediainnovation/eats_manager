package app.direksi.hras.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.util.Patterns;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Validation {

    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isEmptyEditext(String text) {
        //TODO: Replace this with your own logic
        return text.isEmpty();
    }

    public static boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 5;
    }

    public static String mGetText(EditText editText) {
        return editText.getText().toString().trim();
    }


    public static String mGetTextReplaceComma(EditText editText) {
        return editText.getText().toString().trim().replaceAll(",", "");
    }

    public static String mReplaceComma(String string) {
        return string.replaceAll(",", "");
    }

    public static boolean isPhoneValid(String handphone1) {
        //TODO: Replace this with your own logic
        return Patterns.PHONE.matcher(handphone1).matches();
    }

    public static boolean isPhoneValid2(String handphone1) {
        char first = '0';

        if (!handphone1.isEmpty())
            first = handphone1.charAt(0);

        if (handphone1.isEmpty() || first != '0') {
            return false;
        }

        return true;

    }

    public Boolean isEmpty(EditText edt) {
        boolean result = GetText(edt).isEmpty();
        return result;
    }

    public String GetText(EditText edt) {
        String result = edt.getText().toString().trim();
        return result;
    }


    public static boolean mIsValidDateyyMMdd(String mStartDate, String mEndDate) {

        Date starDate = null, endDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            starDate = sdf.parse(mStartDate);
            endDate = sdf.parse(mEndDate);


            if (mStartDate.equals(mEndDate)) {
                return true;
            } else if (endDate.after(starDate)) {
                return true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return false;

    }
    public static boolean mIsValidDateyyMMddAddDays(String mStartDate, String mEndDate) {

        Date starDate = null, endDate = null;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {
            starDate = sdf.parse(mStartDate);
            endDate = sdf.parse(mEndDate);
            Calendar c = Calendar.getInstance();
            c.setTime(starDate);
          /*  c.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) + 1);*/
            c.add(Calendar.DATE, 1);
            Date newDate = c.getTime();

            if (newDate.equals(mEndDate)) {
                return true;
            } else if (endDate.after(newDate)) {
                return true;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


        return false;

    }

    public static String getFirstString(String input) {
        String[] parts = input.split("@");
        return parts[0];
    }

    public static String replaceNull(String input) {
        return input == null ? "" : input;
    }


    public static void openWhatsAppConversationUsingUri(Context context, String numberWithCountryCode, String message) {

        Uri uri = Uri.parse("https://api.whatsapp.com/send?phone=" + numberWithCountryCode + "&text=" + message);

        Intent sendIntent = new Intent(Intent.ACTION_VIEW, uri);

        context.startActivity(sendIntent);
    }


    public static boolean KnowisAppInstalled(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

}
