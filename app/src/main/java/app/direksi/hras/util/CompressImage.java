package app.direksi.hras.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static app.direksi.hras.util.BitmapImage.modifyOrientation;

/**
 * Created by warnawarni on 11/7/2017.
 */

public class CompressImage {

    public static int SIZE_COMPRESS_1 = 28;
    public static int SIZE_COMPRESS_2 = 50;
    public static int SIZE_COMPRESS_3 = 70;
    public static String TAG = CompressImage.class.getSimpleName();


    public static File compressImage(File file, Context context) throws IOException {


        Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());

        bmp = modifyOrientation(bmp,file.getAbsolutePath());


        int width = 0;
        int height = 0;

        try {

            if (bmp.getWidth() > 2000 || bmp.getHeight() > 2000) {
                width = bmp.getWidth() * SIZE_COMPRESS_1 / 100;
                height = bmp.getHeight() * SIZE_COMPRESS_1 / 100;
            } else if (bmp.getWidth() > 1000 || bmp.getHeight() > 1000) {
                width = bmp.getWidth() * SIZE_COMPRESS_2 / 100;
                height = bmp.getHeight() * SIZE_COMPRESS_2 / 100;
            } else if (bmp.getWidth() > 500 || bmp.getHeight() > 500) {
                width = bmp.getWidth() * SIZE_COMPRESS_3 / 100;
                height = bmp.getHeight() * SIZE_COMPRESS_3 / 100;
            } else {
                width = bmp.getWidth();
                height = bmp.getHeight();
            }


        } catch (Exception e) {

        }

        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bmp, width, height, true);

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("ddMMMyyyyHHmmss");
        String formattedDate = df.format(c);

        CapturePhotoUtils.insertImage(context.getContentResolver(), scaledBitmap, "IMG_" + formattedDate, "tes");


        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos);

        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return file;
    }

    public static String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    public static Bitmap loadBitmap(String url) {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Nullable
    public static String createCopyAndReturnRealPath( @NonNull Context context, @NonNull Uri uri) {
        final ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null)
            return null;

        // Create file path inside app's data dir
        String filePath = context.getApplicationInfo().dataDir + File.separator
                + System.currentTimeMillis();

        File file = new File(filePath);
        try {
            InputStream inputStream = contentResolver.openInputStream(uri);
            if (inputStream == null)
                return null;

            OutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = inputStream.read(buf)) > 0)
                outputStream.write(buf, 0, len);

            outputStream.close();
            inputStream.close();
        } catch (IOException ignore) {
            return null;
        }

        return file.getAbsolutePath();
    }


    public static String makeBitmapPath(Context inContext, Bitmap bmp){

        // TimeStamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOut = null;
        File file = new File(inContext.getExternalFilesDir(Environment.DIRECTORY_DCIM), "Photo"+ timeStamp +".jpg"); // the File to save to
        try {
            fOut = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close(); // do not forget to close the stream

            MediaStore.Images.Media.insertImage(inContext.getContentResolver(),file.getAbsolutePath(),file.getName(),file.getName());
        } catch (IOException e){
            String a = e.toString();
            // whatever
        }
        return file.getPath();
    }





}
