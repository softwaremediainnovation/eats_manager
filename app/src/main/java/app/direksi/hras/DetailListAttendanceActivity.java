package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.content.ContextCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.AttendanceAdapter;
import app.direksi.hras.fragment.CreateCutiFragment;
import app.direksi.hras.model.DataAttendance;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.model.ResponseDataAttendance;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 27/05/2019.
 */

public class DetailListAttendanceActivity extends AppCompatActivity implements  View.OnClickListener,  SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = ListCutiActivity.class.getSimpleName();

    private List<DataAttendance> results = new ArrayList<>();
    private AttendanceAdapter cutiAdapter;

    SweetAlertDialog loading;
    TextView txtEmptyList;
    private Filter filter;
    RecyclerView recyclerView;

    private MessageDialog messageDialog;
    //FloatingActionMenu mFloat;
    //FloatingActionButton fab;
    Integer pageNumber = 1;
    SwipeRefreshLayout swiperefresh;
    boolean isLoading = false;
    DataEmployee dataEmployee;
    private ProgressBar load;
    private LinearLayout llFilterDate, llPreviousMonth;
    private TextView txtLate;
    private TextView txtTitleTelat;






    public EmployeActivity.FragmentRefreshListener1 getFragmentRefreshListener1() {
        return fragmentRefreshListener1;
    }

    public void setFragmentRefreshListener(EmployeActivity.FragmentRefreshListener1 fragmentRefreshListener) {
        this.fragmentRefreshListener1 = fragmentRefreshListener;
    }

    private EmployeActivity.FragmentRefreshListener1 fragmentRefreshListener1;




    public EmployeActivity.FragmentRefreshListener2 getFragmentRefreshListener2() {
        return fragmentRefreshListener2;
    }

    public void setFragmentRefreshListener(EmployeActivity.FragmentRefreshListener2 fragmentRefreshListener) {
        this.fragmentRefreshListener2 = fragmentRefreshListener;
    }

    private EmployeActivity.FragmentRefreshListener2 fragmentRefreshListener2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailListAttendanceActivity.this);
        setContentView(R.layout.activity_history_attendance);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");

        //remove line in bar
        // getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading
        //  getSupportActionBar().setTitle(getResources().getString(R.string.title_list_order));

        Gson gson = new Gson();
        dataEmployee = gson.fromJson(getIntent().getStringExtra("detail"), DataEmployee.class);

        SpannableStringBuilder ss = new SpannableStringBuilder(dataEmployee.getFirstName() + " " + dataEmployee.getLastName());
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        messageDialog = new MessageDialog();

        swiperefresh = findViewById(R.id.swiperefresh);
        recyclerView = (RecyclerView)findViewById(R.id.my_recycler_view);
       // fab = findViewById(R.id.fab);
        txtTitleTelat = findViewById(R.id.txtTitleTelat);
        txtTitleTelat.setText(getResources().getString(R.string.title_terlambat_bulan_berjalan));
        txtLate = findViewById(R.id.txtLate);
        txtEmptyList = findViewById(R.id.textNoInternet);
        llPreviousMonth = findViewById(R.id.llPreviousMonth);
        llFilterDate = findViewById(R.id.llFilterDate);
        llFilterDate.setVisibility(View.GONE);
        load = findViewById(R.id.loading);
        load.setVisibility(View.GONE);

        llPreviousMonth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(getResources().getString(R.string.pref_employeeid_view), dataEmployee.getEmployeeID().toString())
                        .apply();
                Intent mIntent = new Intent(getApplicationContext(), HistoryAttendanceActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(dataEmployee);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);
            }
        });

        //mFloat = findViewById(R.id.material_design_android_floating_action_menu);

        //mFloat.setOnClickListener(this);
        //fab.setOnClickListener(this);
        swiperefresh.setOnRefreshListener(this);



        loading = new SweetAlertDialog(DetailListAttendanceActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        loading.show();
        pageNumber = 1;

       /* results = new ArrayList<>();
        cutiAdapter = new EmployeeAdapter(results, getApplicationContext());
        recyclerView.setAdapter(cutiAdapter);
        results.clear();*/

        cutiAdapter = new AttendanceAdapter(results, DetailListAttendanceActivity.this);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.divider));

        //   mRootView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(cutiAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                DataAttendance data = results.get(position);
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .apply();
                Intent mIntent = new Intent(getApplicationContext(), DetailAttendancesActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);


               /* PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(getResources().getString(R.string.pref_employeeid_view), data.getEmployeeID().toString())
                        .apply();
                Intent mIntent = new Intent(getApplicationContext(), DetailEmployeActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);*/


                //  Toast.makeText(getApplicationContext(), "selected", Toast.LENGTH_SHORT).show();


                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        fetchDetailSummary();
      /*  initScrollListener();*/


    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == results.size() - 1) {
                        //bottom of list!
                        double mod = results.size() % Integer.parseInt(getResources().getString(R.string.count_paging));
                        if ( mod > 0 ){

                        }else{
                            loadMore();
                            isLoading = true;

                        }
                    }
                }
            }
        });


    }

    private void loadMore() {
        results.add(null);
        cutiAdapter.notifyItemInserted(results.size() - 1);


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                results.remove(results.size() - 1);
                int scrollPosition = results.size();
                cutiAdapter.notifyItemRemoved(scrollPosition);
                int currentSize = scrollPosition;
                int nextLimit = currentSize + 10;

                fetchDetailSummary();

               /* while (currentSize - 1 < nextLimit) {
                    rowsArrayList.add("Item " + currentSize);
                    currentSize++;
                }*/

               /* cAdapter.notifyDataSetChanged();
                isLoading = false;*/
            }
        }, 2000);


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_check_profil, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailListAttendanceActivity.this);

       /* String tag = (PreferenceManager.getDefaultSharedPreferences(EmployeActivity.this).getString(
                getResources().getString(R.string.pref_dayoff_flag), ""));

        if (tag.equals("0")){
            loading = new SweetAlertDialog(EmployeActivity.this, SweetAlertDialog.PROGRESS_TYPE);
            loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
            loading.setTitleText(getResources().getString(R.string.loading));
            loading.setCancelable(false);

            loading.show();
            pageNumber = 1;

            results = new ArrayList<>();
            cutiAdapter = new EmployeActivity.EmployeAdapter(getApplicationContext(), R.layout.item_cuti, R.id.txtName, results);
            cutiAdapter.setNotifyOnChange(true);
            recyclerView.setAdapter(cutiAdapter);

            cutiAdapter.clear();
            results.clear();

            fetchDetailSummary();

        } else {
            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                    .apply();

        }*/



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
            case R.id.action_search:
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                        edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
                        .putString(getResources().getString(R.string.pref_employeeid_view), dataEmployee.getEmployeeID().toString())
                        .apply();

                DataEmployee data = new DataEmployee(dataEmployee.getEmployeeID(), dataEmployee.getFirstName(), dataEmployee.getLastName());
                Intent mIntent = new Intent(getApplicationContext(), DetailEmployeActivity.class);
                Gson gson = new Gson();
                String myJson = gson.toJson(data);
                mIntent.putExtra("detail", myJson);
                startActivity(mIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        //  loading.show();
        llFilterDate.setVisibility(View.GONE);
        loading.show();
        pageNumber = 1;
        results = new ArrayList<>();
        cutiAdapter = new AttendanceAdapter(results, DetailListAttendanceActivity.this);
        recyclerView.setAdapter(cutiAdapter);
        swiperefresh.setRefreshing(false);
        fetchDetailSummary();

    }



    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        super.onBackPressed();
    }


    private void fetchDetailSummary() {
        try {

            //  recyclerView.setVisibility(View.GONE);



            //authorization JWT pref_token berdasarkan string yg disimpan di preferenceManager pada class login.
            String Authorization = (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                    getResources().getString(R.string.pref_token), ""));


            Map<String, String> data = new HashMap<>();
            data.put("employeeID", dataEmployee.getEmployeeID().toString());

            if (!PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                    getResources().getString(R.string.pref_dayoff_start), "").isEmpty()) {
                data.put("dateFilter", PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                        getResources().getString(R.string.pref_dayoff_start), ""));
            } else {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String strDate = sdf.format(c.getTime());
                data.put("dateFilter", strDate);

            }


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataAttendance> call = api.getAttendanceHariIni(Authorization, data);


            call.enqueue(new Callback<ResponseDataAttendance>() {
                @Override
                public void onResponse(Call<ResponseDataAttendance> call, Response<ResponseDataAttendance> response) {


                    if (response.isSuccessful()) {
                        recyclerView.setVisibility(View.VISIBLE);
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            try {


                                if (response.body().getData().size() > 0) {


                                    double temp = 0;
                                    for (int i = 0; i < response.body().getData().size(); i ++){
                                        if (response.body().getData().get(i).getIsLate()) {
                                            temp = temp + response.body().getData().get(i).getLateTimeInMinute();
                                        }
                                    }


                                    Double temp1 = temp/60;
                                    Double temp2 = temp % 60;
                                    Double temp3 =  temp;

                                    int hour =  temp1.intValue();
                                    int minut = temp2.intValue();
                                    int total = temp3.intValue();
                                    txtLate.setText(hour + " " +
                                            getResources().getString(R.string.title_jam) + " "
                                            + minut + " " +
                                            getResources().getString(R.string.title_menit)
                                            +" ( " +
                                            getResources().getString(R.string.title_total) + " "
                                            + total + " " +
                                            getResources().getString(R.string.title_menit) +" )");

                                    llFilterDate.setVisibility(View.VISIBLE);
                                    results.addAll(response.body().getData());
                                    cutiAdapter.notifyDataSetChanged();
                                    pageNumber = pageNumber + 1;
                                    isLoading = false;
                                    loading.dismiss();
                                    txtEmptyList.setVisibility(View.GONE);

                                } else {
                                    txtEmptyList.setVisibility(View.VISIBLE);
                                }

                                //  cutiAdapter.notifyDataSetChanged();
                                loading.dismiss();
                            } catch (Exception e) {
                                messageDialog.mShowMessageError(DetailListAttendanceActivity.this,getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                                loading.dismiss();

                            }

                        } else {

                            messageDialog.mShowMessageError(DetailListAttendanceActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                            loading.dismiss();


                        }
                    }
                    else {
                        loading.dismiss();
                        Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<ResponseDataAttendance> call, Throwable t) {
                    loading.dismiss();
                    recyclerView.setVisibility(View.VISIBLE);
                    messageDialog.mShowMessageError(DetailListAttendanceActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            recyclerView.setVisibility(View.VISIBLE);
            messageDialog.mShowMessageError(DetailListAttendanceActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }

    }





    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.material_design_android_floating_action_menu) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateCutiFragment dialogFragment = new CreateCutiFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        } else if (v.getId() == R.id.fab) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateCutiFragment dialogFragment = new CreateCutiFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        }
    }

    public interface FragmentRefreshListener1{
        void onRefresh();
    }

    public interface FragmentRefreshListener2{
        void onRefresh();
    }

    public void onUserSelectValue(String selectedValue) {

        // TODO add your implementation.
        //  Toast.makeText(getBaseContext(), "search "+ selectedValue, Toast.LENGTH_LONG).show();
        loading.show();
        pageNumber = 1;
        results = new ArrayList<>();
        cutiAdapter = new AttendanceAdapter(results, DetailListAttendanceActivity.this);
        recyclerView.setAdapter(cutiAdapter);
        swiperefresh.setRefreshing(false);
        fetchDetailSummary();
    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }



}

