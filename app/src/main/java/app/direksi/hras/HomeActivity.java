package app.direksi.hras;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;

import android.os.Bundle;

import android.provider.Settings;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;


import android.graphics.drawable.ColorDrawable;

import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.AkunFragment;
import app.direksi.hras.fragment.CheckInFragment;
import app.direksi.hras.fragment.DashboardFragment;
import app.direksi.hras.fragment.UpdateAppFragment;
import app.direksi.hras.model.FetchVersion;
import app.direksi.hras.model.ResponseDashboard;
import app.direksi.hras.model.ResponseDataEmployeeResign;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.AllowMock;
import static app.direksi.hras.app.AppConstants.LogoutPermission;
import static app.direksi.hras.app.AppConstants.RANGE_DISTANCE_NULL;
import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class
HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private static final String TAG = HomeActivity.class.getSimpleName();

    private Calendar calendar;
    private String mCurrentVersion = "", mServerVersion = "";
    private String datenow;
    private DateFormat dateFormat;
    private Date dateDismissUpdate = null, dateNow = null;


    private TextView title;
    private Toolbar toolbar;
    private LinearLayout llHome, llAtendance, llCuti, llPinjaman, llTeguran;

    private TextView txtEmailNav, txtNameNav, txtHaiName;
    private TextView mTitleNews, mTxtNews, mTxtApprove, mTxtWait, mTxtTotalSurat, mTxtSurat, mTxtReject, mTxtLembur1, mTxtLembur2,
            mTxtLembur3, mTxtTotalGaji, mTxtPinjaman,
            mTxtKlaim,
            mTxtNotif;
    private TextView mTxtCutiDiterima, mTxtCutiMenunggu, mTxtCutiDitolak, mTxtCutiTotal,
            mTxtLemburDiterima, mTxtLemburMenunggu, mTxtLemburDitolak, mTxtLemburTotal,
            mTxtKlaimDiterima, mTxtKlaimMenunggu, mTxtKlaimDitolak, mTxtKlaimTotal,
            mTxtLoanDiterima, mTxtLoanMenunggu, mTxtLoanDitolak, mTxtLoanTotal;
    private NavigationView navigationView;
    private LinearLayout mLayoutNews, mLayoutCuti, mLayoutWarning, mLayoutOvertime, mLayoutSalary, mLayoutClaim, mLayoutPinjaman;
    private SweetAlertDialog loading;
    private MessageDialog messageDialog;
    ImageView imageViewNav;
    private MenuItem menu_checkin;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {


            displaySelectedScreen(item.getItemId());
            return true;
        }
    };

    private void displaySelectedScreen(int itemId) {
        //creating fragment object
        Fragment fragment = null;
        //initializing the fragment object which is selected



        switch (itemId) {
            case R.id.navigation_akun:
                fragment = new AkunFragment();
                break;
            case R.id.navigation_kehadiran:
                fragment = new DashboardFragment();
                break;
            case R.id.navigation_home:
                fragment = new CheckInFragment();
                break;
//            case R.id.nav_rekap:
//                fragment = new RekapOrtuActivity();
//                break;

        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(HomeActivity.this);

        //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setBackgroundResource(R.drawable.gradient_color);
        changeStatusBarColor();


      //  getSupportActionBar().setElevation(0);//value 0 to remove line

        messageDialog = new MessageDialog();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);


        txtNameNav = (TextView) header.findViewById(R.id.txtNameNav);
        txtEmailNav = (TextView) header.findViewById(R.id.txtEmailNav);

        txtHaiName = findViewById(R.id.txtHaiName);
        mLayoutNews = findViewById(R.id.mLayoutNews);
        mLayoutPinjaman = findViewById(R.id.mLayoutPinjaman);
        mLayoutCuti = findViewById(R.id.mLayoutCuti);
        mLayoutWarning = findViewById(R.id.mLayoutWarning);
        mLayoutOvertime = findViewById(R.id.mLayoutOvertime);
        mLayoutSalary = findViewById(R.id.mLayoutSalary);
        mLayoutClaim = findViewById(R.id.mLayoutClaim);
        mTitleNews = findViewById(R.id.mTitleNews);
        mTxtNews = findViewById(R.id.mTxtNews);
        llHome = findViewById(R.id.llHome);

        llAtendance = findViewById(R.id.llAtendance);
        llAtendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AttendanceActivity.class);
                startActivity(intent);
            }
        });
        llCuti = findViewById(R.id.llCuti);
        llCuti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListCutiActivity.class);
                startActivity(intent);
            }
        });
        llPinjaman = findViewById(R.id.llPinjaman);
        llPinjaman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoanActivity.class);
                startActivity(intent);
            }
        });
        llTeguran = findViewById(R.id.llTeguran);
        llTeguran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), WarningLetterActivity.class);
                startActivity(intent);
            }
        });

        txtHaiName.setText("Hai, " + PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                getResources().getString(R.string.pref_nama), ""));


        mTxtApprove = findViewById(R.id.mTxtApprove);
        mTxtWait = findViewById(R.id.mTxtWait);
        mTxtReject = findViewById(R.id.mTxtReject);
        mTxtTotalSurat = findViewById(R.id.mTxtTotalSurat);
        mTxtSurat = findViewById(R.id.mTxtSurat);

        mTxtLembur1 = findViewById(R.id.mTxtLembur1);
        mTxtLembur2 = findViewById(R.id.mTxtLembur2);
        mTxtLembur3 = findViewById(R.id.mTxtLembur3);
        mTxtTotalGaji = findViewById(R.id.mTxtTotalGaji);
        mTxtNotif = findViewById(R.id.mTxtNotif);


        mTxtPinjaman = findViewById(R.id.mTxtPinjaman);
        mTxtKlaim = findViewById(R.id.mTxtKlaim);


        mTxtCutiDiterima = findViewById(R.id.mTxtCutiDiterima);
        mTxtCutiMenunggu = findViewById(R.id.mTxtCutiMenunggu);
        mTxtCutiDitolak = findViewById(R.id.mTxtCutiDitolak);
        mTxtCutiTotal = findViewById(R.id.mTxtCutiTotal);

        mTxtLemburDiterima = findViewById(R.id.mTxtLemburDiterima);
        mTxtLemburMenunggu = findViewById(R.id.mTxtLemburMenunggu);
        mTxtLemburDitolak = findViewById(R.id.mTxtLemburDitolak);
        mTxtLemburTotal = findViewById(R.id.mTxtLemburTotal);

        mTxtKlaimDiterima = findViewById(R.id.mTxtKlaimDiterima);
        mTxtKlaimMenunggu = findViewById(R.id.mTxtKlaimMenunggu);
        mTxtKlaimDitolak = findViewById(R.id.mTxtKlaimDitolak);
        mTxtKlaimTotal = findViewById(R.id.mTxtKlaimTotal);

        mTxtLoanDiterima = findViewById(R.id.mTxtLoanDiterima);
        mTxtLoanMenunggu = findViewById(R.id.mTxtLoanMenunggu);
        mTxtLoanDitolak = findViewById(R.id.mTxtLoanDitolak);
        mTxtLoanTotal = findViewById(R.id.mTxtLoanTotal);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

       /* if(savedInstanceState == null) {
            getSupportFragmentManager().
                    beginTransaction().replace(R.id.content_frame,new FingerPrintFragmentNew()).commit();
        }*/


        imageViewNav = (ImageView) header.findViewById(R.id.imageViewNav);
        imageViewNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });
        String url = PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(getResources().getString(R.string.pref_photourl), "");
        if (url == null || url.equals("http://hras.warnawarni.co.id") )
        {

            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);

            Glide.with(HomeActivity.this)
                    .load(getResources().getDrawable(R.drawable.hras_logo))
                    .apply(requestOptions)
                    .into(imageViewNav);

        } else {
            RequestOptions requestOptions = new RequestOptions();
            //  requestOptions.placeholder(R.drawable.warnawarni);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.skipMemoryCache(true);
            Glide.with(HomeActivity.this)
                    .load( PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(getResources().getString(R.string.pref_photourl), ""))
                    .apply(requestOptions)
                    .into(imageViewNav);

          /*  Glide.with(HomeActivity.this)
                    .load((getResources().getString(R.string.base_url) +  PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(getResources().getString(R.string.pref_photourl), "")))
                            .apply(requestOptions)
                            .into(imageViewNav);*/
        }






       /* Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_create_user).setVisible(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                getResources().getString(R.string.pref_role), "").contains("Admin"));


        nav_Menu.findItem(R.id.nav_list_user).setVisible(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                getResources().getString(R.string.pref_role), "").contains("Admin"));

        nav_Menu.findItem(R.id.nav_logout).setVisible(!PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
                getResources().getString(R.string.pref_role), "").equals("Client"));*/


        loading = new SweetAlertDialog(HomeActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        txtNameNav.setText(getResources().getString(R.string.text_hello) + PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(getResources().getString(R.string.pref_nama), ""));
        txtEmailNav.setText(PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                getResources().getString(R.string.pref_email), ""));



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Cabin.otf");
        title = new TextView(getApplicationContext());
        title.setText(getResources().getString(R.string.action_sign_in));
        //title.setTypeface(face);
        title.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        title.setTextSize(20);
        title.setTextColor(getResources().getColor(R.color.color_white));
        getSupportActionBar().setCustomView(title);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));


        getSupportActionBar().setElevation(0);

        mLayoutNews.setOnClickListener(this);
        mLayoutCuti.setOnClickListener(this);
        mLayoutWarning.setOnClickListener(this);
        mLayoutOvertime.setOnClickListener(this);
        mLayoutSalary.setOnClickListener(this);
        mLayoutClaim.setOnClickListener(this);
        mLayoutPinjaman.setOnClickListener(this);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbars);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Beranda");
        mToolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

        displaySelectedScreen(R.id.navigation_home);

        //mGetDashboard();
        try {
            mCurrentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

       /* mGetUpdateApp();*/
        if (AllowMock){
                MDToast.makeText(HomeActivity.this, "Allow Mock Location",
                MDToast.LENGTH_LONG, MDToast.TYPE_INFO).show();
        }




    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
       /* DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
      /*  MDToast.makeText(HomeActivity.this, "Tekan sekali lagi untuk keluar",
                MDToast.LENGTH_LONG, MDToast.TYPE_INFO).show();*/
        Toast.makeText(this, getResources().getString(R.string.title_tekan_sekali_untuk_keluar), Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(HomeActivity.this);

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                .putString(getResources().getString(R.string.pref_dayoff_start), "")
                .putString(getResources().getString(R.string.pref_dayoff_end), "")
                .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                .putString(getResources().getString(R.string.pref_dayoff_end_shadow), "")
                .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                .putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                .putString(getResources().getString(R.string.pref_dayoff_idProses), "0")
                .putString(getResources().getString(R.string.pref_dayoff_status), "1")
                .putString(getResources().getString(R.string.pref_dayoff_notif), "0")
                .putString(getResources().getString(R.string.pref_aktivitas_maps), "0")
                .putString(getResources().getString(R.string.pref_dayoff_search_idCompany), "0")
                .putString(getResources().getString(R.string.pref_dayoff_search_idOrganization), "0")
                .putString(getResources().getString(R.string.pref_dayoff_search_idCategory), "0")
                .apply();

        checkResign();
        CheckLogoutPermission ();

      /*  TimeZone ();*/

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_logout) {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getResources().getString(R.string.string_confirmation))
                    .setContentText(getResources().getString(R.string.txt_message_log_out))
                    .setCancelText(getResources().getString(R.string.txt_no))
                    .setConfirmText(getResources().getString(R.string.string_yes))
                    .showCancelButton(true)
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit()
                                    .putString(getResources().getString(R.string.pref_nama), "")
                                    .putString(getResources().getString(R.string.pref_email), "")
                                    .putString(getResources().getString(R.string.pref_token), "")
                                    .putString(getResources().getString(R.string.pref_role), "")
                                    .putString(getResources().getString(R.string.pref_phone), "")
                                    .apply();


                            Intent login = new Intent(HomeActivity.this, MainActivity.class);
                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(login);
                            finish();
                        }
                    })
                    .show();
        } else if (id == R.id.nav_news) {
            Intent intent = new Intent(getApplicationContext(), NewsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_cuti) {

           /* PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                    edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                    .putString(getResources().getString(R.string.pref_dayoff_start), "")
                    .putString(getResources().getString(R.string.pref_dayoff_end), "")
                    .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                    .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                    .apply();*/


            Intent intent = new Intent(getApplicationContext(), ListCutiActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_letter) {
            Intent intent = new Intent(getApplicationContext(), WarningLetterActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_overtime) {
            Intent intent = new Intent(getApplicationContext(), OvertimeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_loan) {
            Intent intent = new Intent(getApplicationContext(), LoanActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_request) {
            Intent intent = new Intent(getApplicationContext(), ClaimActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_aset) {
            Intent intent = new Intent(getApplicationContext(), AssetNewActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_employe) {
            Intent intent = new Intent(getApplicationContext(), EmployeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_attendance) {
            Intent intent = new Intent(getApplicationContext(), AttendanceActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_laporan) {
            Intent intent = new Intent(getApplicationContext(), ListLaporanActivity.class);
            startActivity(intent);
        }  else if (id == R.id.nav_activity) {
            Intent intent = new Intent(getApplicationContext(), AktivitasActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_home) {
            Intent intent = new Intent(getApplication(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.mLayoutNews) {
            Intent intent = new Intent(getApplicationContext(), NewsActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutCuti) {

          /*  PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
                    edit().putString(getResources().getString(R.string.pref_dayoff_search), "")
                    .putString(getResources().getString(R.string.pref_dayoff_start), "")
                    .putString(getResources().getString(R.string.pref_dayoff_end), "")
                    .putString(getResources().getString(R.string.pref_dayoff_end1), "")
                    .putString(getResources().getString(R.string.pref_dayoff_start1), "")
                    .apply();*/


            Intent intent = new Intent(getApplicationContext(), ListCutiActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutWarning) {
            Intent intent = new Intent(getApplicationContext(), WarningLetterActivity.class);
            startActivity(intent);
        } else if (v.getId() == R.id.mLayoutOvertime) {
            Intent intent = new Intent(getApplicationContext(), OvertimeActivity.class);
            startActivity(intent);
        }  else if (v.getId() == R.id.mLayoutClaim) {
            Intent intent = new Intent(getApplicationContext(), ClaimActivity.class);
            startActivity(intent);
        }  else if (v.getId() == R.id.mLayoutPinjaman) {
            Intent intent = new Intent(getApplicationContext(), LoanActivity.class);
            startActivity(intent);
        }
    }


    private void mGetDashboard() {

        try {
            loading.show();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                    getResources().getString(R.string.pref_token), "tes"));

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);


            Call<ResponseDashboard> call = api.mGetDashboard(Authorization);
            call.enqueue(new Callback<ResponseDashboard>() {
                @Override
                public void onResponse(Call<ResponseDashboard> call, Response<ResponseDashboard> response) {

                    try {
                        //loading hide
                        loading.dismiss();

                        if (response.isSuccessful()) {
                            long statusCode = response.body().getErrCode();
                            String statusMessage = response.body().getErrMessage();
                            //status code 1 if login success
                            if (statusCode == 0) {

                                if (response.body().getDataLuar().getDataDalam().getNews()!=null) {
                                    mTitleNews.setText(response.body().getDataLuar().getDataDalam().getNews().getTitle() != null ? response.body().getDataLuar().getDataDalam().getNews().getTitle() : "");
                                    mTxtNews.setText(response.body().getDataLuar().getDataDalam().getNews().getNews() != null ? response.body().getDataLuar().getDataDalam().getNews().getNews() : "");
                                } else{
                                    mTitleNews.setText("Tidak ada berita");
                                    mTxtNews.setText("");
                                }
                                llHome.setVisibility(View.VISIBLE);
                                DecimalFormat formatter = new DecimalFormat("#,###,###");

                                mTxtApprove.setText("Disetujui : " + response.body().getDataLuar().getDataDalam().getCuti().getDisetujui());
                                mTxtWait.setText("Menunggu : " + response.body().getDataLuar().getDataDalam().getCuti().getMenunggu());
                                mTxtReject.setText("Ditolak : " + response.body().getDataLuar().getDataDalam().getCuti().getDitolak());


                                mTxtLembur1.setText("Disetujui : " + response.body().getDataLuar().getDataDalam().getLembur().getDisetujui());
                                mTxtLembur2.setText("Menunggu : " + response.body().getDataLuar().getDataDalam().getLembur().getMenunggu());
                                mTxtLembur3.setText("Ditolak : " + response.body().getDataLuar().getDataDalam().getLembur().getDitolak());


                                mTxtTotalSurat.setText(response.body().getDataLuar().getDataDalam().getSurat().getTotal().toString());
                                mTxtSurat.setText(response.body().getDataLuar().getDataDalam().getSurat().getSurat());

                                mTxtTotalGaji.setText("Total Gaji " + "Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getGaji().getRemunerationNominalID()));
                                mTxtKlaim.setText("Pengajuan Klaim " + "Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtPinjaman.setText("Pengajuan Pinjaman " + "Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtNotif.setText(response.body().getDataLuar().getDataDalam().getGaji().getNotifikasi());

                                mTxtCutiDiterima.setText(response.body().getDataLuar().getDataDalam().getCuti().getDisetujui().toString());
                                mTxtCutiMenunggu.setText(response.body().getDataLuar().getDataDalam().getCuti().getMenunggu().toString());
                                mTxtCutiDitolak.setText(response.body().getDataLuar().getDataDalam().getCuti().getDitolak().toString());
                                mTxtCutiTotal.setText(String.valueOf(response.body().getDataLuar().getDataDalam().getCuti().getDitolak() + response.body().getDataLuar().getDataDalam().getCuti().getDisetujui() + response.body().getDataLuar().getDataDalam().getCuti().getMenunggu()));

                                mTxtLemburDiterima.setText(response.body().getDataLuar().getDataDalam().getLembur().getDisetujui().toString());
                                mTxtLemburMenunggu.setText(response.body().getDataLuar().getDataDalam().getLembur().getMenunggu().toString());
                                mTxtLemburDitolak.setText(response.body().getDataLuar().getDataDalam().getLembur().getDitolak().toString());
                                mTxtLemburTotal.setText(String.valueOf(response.body().getDataLuar().getDataDalam().getLembur().getDitolak() + response.body().getDataLuar().getDataDalam().getLembur().getDisetujui() + response.body().getDataLuar().getDataDalam().getLembur().getMenunggu()));

                                mTxtKlaimDiterima.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtKlaimMenunggu.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtKlaimDitolak.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));
                                mTxtKlaimTotal.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim() + response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim() + response.body().getDataLuar().getDataDalam().getPengajuan().getKlaim()));

                                mTxtLoanDiterima.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtLoanMenunggu.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtLoanDitolak.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));
                                mTxtLoanTotal.setText("Rp " + formatter.format(response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman() + response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman() + response.body().getDataLuar().getDataDalam().getPengajuan().getPinjaman()));








                               /* mTxtCutiDiterima, mTxtCutiMenunggu, mTxtCutiDitolak, mTxtCutiTotal,
                                        mTxtLemburDiterima, mTxtLemburMenunggu, mTxtLemburDitolak, mTxtLemburTotal,
                                        mTxtKlaimDiterima, mTxtKlaimMenunggu, mTxtKlaimDitolak, mTxtKlaimTotal,
                                        mTxtLoanDiterima, mTxtLoanMenunggu, mTxtLoanDitolak, mTxtLoanTotal;*/


                            } else {
                                messageDialog.mShowMessageError(HomeActivity.this,"GAGAL"
                                        , "");
                            }

                        } else {
                            messageDialog.mShowMessageError(HomeActivity.this, "GAGAL"
                                    , "");
                        }
                    } catch (Exception e) {
                        messageDialog.mShowMessageError(HomeActivity.this, "GAGAL"
                                , "");
                    }


                }

                @Override
                public void onFailure(Call<ResponseDashboard> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(HomeActivity.this, "GAGAL"
                            ,"");
                }

            });


        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(HomeActivity.this,"GAGAL"
                    , "");
        }
    }


    private void changeStatusBarColor(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
        }
    }


    void mGetUpdateApp() {
       /* if (!PreferenceManager.getDefaultSharedPreferences(this).getString(
                getResources().getString(R.string.pref_update_application), "0").equals("1")) {
            mGetVersionApp();
        } else {

            try {
                calendar = Calendar.getInstance();
                dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                datenow = dateFormat.format(calendar.getTime());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                try {
                    dateDismissUpdate = sdf.parse(PreferenceManager.getDefaultSharedPreferences(this).getString(
                            getResources().getString(R.string.pref_count_date_application), datenow));

                    dateNow = sdf.parse(datenow);

                    if (dateNow.after(dateDismissUpdate)) {

                        mGetVersionApp();

                    }
                } catch (ParseException e) {
                }

            } catch (Exception e) {

            }

        }*/
        mGetVersionApp();
    }

    public void mGetVersionApp() {

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            Map<String, String> data = new HashMap<>();
            data.put("id", String.valueOf(2));

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<FetchVersion> call = api.getVersion(Authorization, data);

            call.enqueue(new Callback<FetchVersion>() {
                @Override
                public void onResponse(Call<FetchVersion> call, Response<FetchVersion> response) {
                    if (response.isSuccessful()) {

                        if(response.body().getErrCode() == 0) {


                            if (response.body().getData()!= null) {
                                mServerVersion = response.body().getData().getVersions();
                               // mServerVersion = "2.0.0";

                                String[] parts = mCurrentVersion.split("\\.");
                                String[] parts2 = mServerVersion.split("\\.");
                                Integer curversion1 = Integer.parseInt(parts[0].trim());
                                Integer curversion2 = Integer.parseInt(parts[1].trim());
                                Integer curversion3 = Integer.parseInt(parts[2].trim());

                                Integer newversion1 = Integer.parseInt(parts2[0].trim());
                                Integer newversion2 = Integer.parseInt(parts2[1].trim());
                                Integer newversion3 = Integer.parseInt(parts2[2].trim());

                                if (newversion1 > curversion1) {
                                    mDialogUpdateVersion("force", mServerVersion);
                                    return;
                                } else if (newversion2 > curversion2) {

                                    if (newversion1 > curversion1){
                                        mDialogUpdateVersion("force", mServerVersion);
                                        return;
                                    }
                                    if (newversion1.toString().equals( curversion1.toString()) ){
                                        mDialogUpdateVersion("force", mServerVersion);
                                        return;
                                    }


                                } else if (newversion3 > curversion3) {
                                    if (newversion2 > curversion2){
                                        mDialogUpdateVersion("later", mServerVersion);
                                        return;
                                    }
                                    if (newversion2.toString().equals( curversion2.toString()) ){
                                        mDialogUpdateVersion("later", mServerVersion);
                                        return;
                                    }

                                }
                            }
                        }
                    } else {
                      //  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<FetchVersion> call, Throwable t) {
                   // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
           // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
        }
    }

    public void checkResign() {

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String id = (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                    getResources().getString(R.string.pref_employeeid), ""));

            Map<String, String> data = new HashMap<>();
            data.put("employeeId", id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployeeResign> call = api.getEmployeeResign(Authorization, data);

            call.enqueue(new Callback<ResponseDataEmployeeResign>() {
                @Override
                public void onResponse(Call<ResponseDataEmployeeResign> call, Response<ResponseDataEmployeeResign> response) {
                    if (response.isSuccessful()) {

                        if (response.body().getData() != null) {

                            boolean isTrue = response.body().getData().getIsActive();

                            if (!isTrue) {


                                PostLogout(getResources().getString(R.string.title_anda_sudah_resign));


                            } else {

                                PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).edit()
                                        .putString(getResources().getString(R.string.pref_latitude), response.body().getData().getLatitude() != null ? response.body().getData().getLatitude() : "")
                                        .putString(getResources().getString(R.string.pref_longitude), response.body().getData().getLongitude() != null ? response.body().getData().getLongitude() : "")
                                        .putString(getResources().getString(R.string.pref_radius), response.body().getData().getDistance() != null ? response.body().getData().getDistance() : RANGE_DISTANCE_NULL)
                                        .putString(getResources().getString(R.string.pref_absen_method), response.body().getData().getAttendanceMethod() != null ? response.body().getData().getAttendanceMethod().toLowerCase() : "biometric")
                                        .apply();

                                String serialnumber = "";
                                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                                    serialnumber = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);

                                }
                                else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    if (ActivityCompat.checkSelfPermission(HomeActivity.this, android.Manifest.permission.READ_PHONE_STATE)
                                            != PackageManager.PERMISSION_GRANTED) {
                                        // TODO: Consider calling
                                        return;
                                    }
                                    serialnumber = getSerialNumber();
                                    if(serialnumber.equals("unknown")){
                                        serialnumber = Build.getSerial();

                                    }

                                } else {
                                    serialnumber = getSerialNumber();
                                }

                                if (response.body().getData().getSerialNumber()!= null) {

                                    if (!serialnumber.equals(response.body().getData().getSerialNumber())) {


                                        PostLogout(getResources().getString(R.string.title_hanya_boleh_satu_device));



                                    }
                                }

                            }
                        }
                        mGetUpdateApp();


                    } else {
                        mGetUpdateApp();
                        //  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataEmployeeResign> call, Throwable t) {
                    mGetUpdateApp();
                    // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            mGetUpdateApp();
            // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
        }
    }

    void mDialogUpdateVersion(String action, String newVersion) {
        android.app.FragmentManager fm = getFragmentManager();
        UpdateAppFragment dialogFragment = new UpdateAppFragment();
        Bundle bundle = new Bundle();
        bundle.putString("action", action);
        bundle.putString("version", newVersion);
        bundle.putString("datenow", datenow);
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");
    }


    void CheckLogoutPermission (){

        try {
            String logout = (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                    getResources().getString(R.string.pref_Logout_permission), ""));

            if (!logout.equals(LogoutPermission)) {

                PostLogout(getResources().getString(R.string.title_silahkan_login_ulang));

            }
        } catch (Exception e){

        }
    }

    public static String getSerialNumber() {
        String serialNumber;

        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class);

            serialNumber = (String) get.invoke(c, "gsm.sn1");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ril.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "ro.serialno");
            if (serialNumber.equals(""))
                serialNumber = (String) get.invoke(c, "sys.serialnumber");
            if (serialNumber.equals(""))
                serialNumber = Build.SERIAL;

            // If none of the methods above worked
            if (serialNumber.equals(""))
                serialNumber = null;
        } catch (Exception e) {
            e.printStackTrace();
            serialNumber = null;
        }

        return serialNumber;
    }


    public void PostLogout( String message ) {

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));






            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.Logout(Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {
                    if (response.isSuccessful()) {

                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            try {
                                NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                                notificationManager.cancelAll();

                            } catch (Exception e){

                            }

                            PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).edit()
                                    .putString(getResources().getString(R.string.pref_nama), "")
                                    .putString(getResources().getString(R.string.pref_email), "")
                                    .putString(getResources().getString(R.string.pref_token), "")
                                    .putString(getResources().getString(R.string.pref_role), "")
                                    .putString(getResources().getString(R.string.pref_phone), "")
                                    .putString(getResources().getString(R.string.pref_key_xendit), "")
                                    .apply();

                            MDToast.makeText(HomeActivity.this, message,
                                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
                            // Toast.makeText(HomeActivity.this, "Anda sudah resign", Toast.LENGTH_LONG).show();
                            Intent login = new Intent(HomeActivity.this, LoginActivity.class);
                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(login);
                            finish();


                        }
                        else {

                        }

                    } else {

                        //  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {

                    // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {

            // Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
            // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
        }
    }

    private void TimeZone (){

        try {

            Calendar calendartIZ = Calendar.getInstance(TimeZone.getDefault());

            Log.d("LOG_TAGA", String.valueOf(TimeZone.getDefault()));
            Log.d("LOG_TAGB", TimeZone.getDefault().getID());
            Log.d("LOG_TAGC", String.valueOf(Locale.getDefault()));
            Log.d("LOG_TAGD", String.valueOf(calendartIZ.getTimeZone()));
            Log.d("LOG_TAGE", calendartIZ.getTimeZone().getDisplayName());
            Log.d("LOG_TAGF", calendartIZ.getTimeZone().getDisplayName(false, TimeZone.SHORT));
            Log.d("LOG_TAGG", calendartIZ.getTimeZone().getDisplayName(false, TimeZone.LONG));


            TimeZone tz = TimeZone.getDefault();
            Calendar cal = GregorianCalendar.getInstance(tz);
            int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

            String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
            offset = "GMT"+(offsetInMillis >= 0 ? "+" : "-") + offset;

            Log.d("LOG_TAGH", offset);


            Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
            String   timeZone = new SimpleDateFormat("Z").format(calendar.getTime());


            Log.d("LOG_TAGI", timeZone.substring(0, 3) + ":"+ timeZone.substring(3, 5));





        } catch (Exception e){

        }
    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

       if (bahasa.equals("") || bahasa.equals(null)){

           bahasa = "en";
       }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
