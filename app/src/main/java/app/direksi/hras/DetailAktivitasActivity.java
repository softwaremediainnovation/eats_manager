package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.DataActivities;
import app.direksi.hras.model.DataAktivitas;
import app.direksi.hras.model.ResponseDetailActivities;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 26/06/2019.
 */

public class DetailAktivitasActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = DetailAktivitasActivity.class.getSimpleName();

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private TextView mTxtDate,
            mTxtNote;
    private ImageView mImageView;
    private MessageDialog messageDialog;
    DataAktivitas dataActivitiesItem;
    private SweetAlertDialog loading;
    private ScrollView scrollView;
    private double latitude = 0, longitude = 0;
    private static final int REQUEST_PHONE_CALL = 1;
    public TextView profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtAlamat,
            txtOrganisasi;
    private ImageView  profile_image;
    private CardView  mLayoutImages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailAktivitasActivity.this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading

        // getSupportActionBar().setElevation(0);



        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_activity));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        setContentView(R.layout.activity_detail_aktivitas);


        txtAlamat = findViewById(R.id.txtAlamat);
        mTxtDate = findViewById(R.id.mTxtDate);
        mTxtNote = findViewById(R.id.mTxtNote);
        mImageView = findViewById(R.id.mImageView);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);



        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        messageDialog = new MessageDialog();
        loading = new SweetAlertDialog(DetailAktivitasActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);


        mLayoutImages = findViewById(R.id.mLayoutImages);


        Gson gson = new Gson();
        dataActivitiesItem = gson.fromJson(getIntent().getStringExtra("detail"), DataAktivitas.class);

        // Log.v(TAG, dataActivitiesItem.getActivityID() + "");


        if (dataActivitiesItem != null) {
            mGetDetailDayOff(dataActivitiesItem.getActivityID().intValue());
        } else {
            onNewIntent(getIntent());
        }





    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        mGetDetailDayOff(idx);
                    }
                }
            }
        } catch (Exception e) {
        }
    }


    public void mLoadMaps() {


        int height = SIZE_MARKER;
        int width = SIZE_MARKER;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);


        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                .title(getResources().getString(R.string.title_lokasi_aktivitas)));

        Log.v(TAG, latitude + " --- " + longitude);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                .zoom(16)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


     /*   Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(RANGE_DISTANCE)
                .strokeColor(Color.parseColor("#20ff5252"))
                .strokeWidth(3)
                .fillColor(Color.parseColor("#50ff5252")));*/


    }


    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try {
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_json_maps));

            if (!isSuccess) {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        } catch (Resources.NotFoundException ex) {

            Log.v(TAG, ex.toString());
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            mMap.getUiSettings().setMapToolbarEnabled(false);
            // mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);
          /*  mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(-7.257472, 112.752090)));*/
        }


    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void mGetDetailDayOff(int id) {

        try {

            loading.show();

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailAktivitasActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();


            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailActivities> call = api.mGetDetailActivities(String.valueOf(id), Authorization);


            call.enqueue(new Callback<ResponseDetailActivities>() {
                @Override
                public void onResponse(Call<ResponseDetailActivities> call, Response<ResponseDetailActivities> response) {

                    Log.v(TAG, response.toString());
                    try {

                        int statusCode = response.body().getErrCode();

                        if (statusCode == 0) {

                            ShowData(response.body().getDataActivities());


                        }

                    } catch (Exception e) {
                        messageDialog.mShowMessageError(DetailAktivitasActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }

                    loading.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseDetailActivities> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(DetailAktivitasActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            Log.v(TAG, e.getMessage());
            loading.dismiss();
            messageDialog.mShowMessageError(DetailAktivitasActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }


    }



    private void ShowData(DataActivities data){

        scrollView.setVisibility(View.VISIBLE);

        txtOrganisasi.setText(data.getName());
        mTxtNote.setText(data.getDescription());
        mTxtDate.setText(DefaultFormatter.changeFormatDate(data.getDate()));


        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);


        if (data.getPhotoUrl()!= null) {
            mImageView.setVisibility(View.VISIBLE);
            Glide.with(DetailAktivitasActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.getPhotoUrl()))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);


            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.getPhotoUrl());
                    startActivity(intent);
                }
            });
        } else{
            mImageView.setVisibility(View.GONE);
        }

        try {

            if (data.getLatitude() != null && data.getLongitude() != null) {

                latitude = Double.parseDouble(data.getLatitude());
                longitude = Double.parseDouble(data.getLongitude());

                if (data.getAddress()!= null){

                     if (!data.getAddress().equals("")){

                         txtAlamat.setText(data.getAddress());

                     } else {

                         getAddress(this, latitude, longitude);

                     }


                } else {

                    getAddress(this, latitude, longitude);
                }

                mLoadMaps();
            }


        } catch (Exception e){

        }


        if (data.getEmployee() != null) {
            if (data.getEmployee().getFirstName() != null) {
                profil_nama.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
            }
            if (data.getEmployee().getNik() != null) {
                profil_role.setText(data.getEmployee().getNik());
            }
            if (data.getEmployee().getEmail() != null) {
                profil_email.setText(data.getEmployee().getEmail());
            }
            if (data.getEmployee().getPhone() != null) {
                profil_phone.setText(data.getEmployee().getPhone());
            }

            if (data.getEmployee().getPhone().equals("") || data.getEmployee().getPhone() == null) {

            } else {
                String id = (PreferenceManager.getDefaultSharedPreferences(DetailAktivitasActivity.this).getString(
                        getResources().getString(R.string.pref_employeeid), ""));

                if (!data.getEmployee().getEmployeeID().toString().equals(id)) {


                    mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                        @Override
                        public void onClick(View v) {
                            final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                            CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setTitle(getResources().getString(R.string.title_aksi));
                            builder.setItems(colors, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == 0) {
                                        if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                            // Check Permissions Now
                                            ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                        } else {
                                            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            context.getApplicationContext().startActivity(intent);
                                        }
                                    } else if (which == 1) {
                                        Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                        Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                        mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(mSendSms);
                                    } else if (which == 2) {
                                        String name = data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName();
                                        String phone = data.getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                        Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                        contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                        contactIntent
                                                .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                                .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                        ((Activity) context).startActivityForResult(contactIntent, 1);

                                    } else if (which == 3) {

                                        voiceCall(profil_phone.getText().toString().trim());
                                    }
                                }
                            });
                            builder.show();
                        }
                    });
                }
            }

            if (data.getEmployee().getPhotoUrl() != null) {

                Glide.with(DetailAktivitasActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getEmployee().getPhotoUrl()))
                        .apply(requestOptions)
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);
                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getEmployee().getPhotoUrl());
                        startActivity(intent);
                    }
                });

            }
            else {
                // profile_image.setVisibility(View.GONE);
            }







        }
    }



    void getAddress(Context context, double LATITUDE, double LONGITUDE) {

        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {



                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                txtAlamat.setText(address);
                Log.d(TAG, "getAddress:  address" + address);
                Log.d(TAG, "getAddress:  city" + city);
                Log.d(TAG, "getAddress:  state" + state);
                Log.d(TAG, "getAddress:  postalCode" + postalCode);
                Log.d(TAG, "getAddress:  knownName" + knownName);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public void voiceCall(String no_Telp){


        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(DetailAktivitasActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            //Toast.makeText(DetailAktivitasActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailAktivitasActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}

