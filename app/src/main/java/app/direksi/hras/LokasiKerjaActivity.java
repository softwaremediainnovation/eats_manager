package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.app.AppConstants;
import app.direksi.hras.model.DataActivities;
import app.direksi.hras.model.DataAktivitas;
import app.direksi.hras.model.DataEmployeeResign;
import app.direksi.hras.model.ResponseDataEmployeeResign;
import app.direksi.hras.model.ResponseDetailActivities;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.RANGE_DISTANCE_NULL;
import static app.direksi.hras.app.AppConstants.SIZE_MARKER;
import static app.direksi.hras.app.AppConstants.TIMEOUT_REQUEST;
import static app.direksi.hras.util.MyContextWrapper.changeLang;
import static app.direksi.hras.util.Validation.replaceNull;

/**
 * Created by dhimaz on 26/06/2019.
 */

public class LokasiKerjaActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = DetailAktivitasActivity.class.getSimpleName();

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private TextView mTxtDate,
            mTxtNote;
    private ImageView mImageView;
    private MessageDialog messageDialog;
    DataAktivitas dataActivitiesItem;
    private SweetAlertDialog loading;
    private ScrollView scrollView;
    private double latitude = 0, longitude = 0;
    private static final int REQUEST_PHONE_CALL = 1;
    public TextView profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtAlamat,
            txtOrganisasi, txtLatitude, txtLongitude;
    private ImageView  profile_image;
    private CardView  mLayoutImages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(LokasiKerjaActivity.this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading

        // getSupportActionBar().setElevation(0);



        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.title_lokasi_kantor));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }


        setContentView(R.layout.activity_lokasi_kerja);


        txtAlamat = findViewById(R.id.txtAlamat);
        txtLatitude = findViewById(R.id.txtLatitude);
        txtLongitude = findViewById(R.id.txtLongitude);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);



        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        messageDialog = new MessageDialog();
        loading = new SweetAlertDialog(LokasiKerjaActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        mGetActiveUser();



    }




    public void mLoadMaps() {


        int height = SIZE_MARKER;
        int width = SIZE_MARKER;
        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_user);
        Bitmap b=bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, bitmapdraw.getBitmap().getWidth()*width/100, bitmapdraw.getBitmap().getHeight()* height/100, false);


        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker))
                .title(getResources().getString(R.string.title_lokasi_kantor)));

        Log.v(TAG, latitude + " --- " + longitude);

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 10));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))      // Sets the center of the map to location user
                .zoom(16)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));


     /*   Circle circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latitude, longitude))
                .radius(RANGE_DISTANCE)
                .strokeColor(Color.parseColor("#20ff5252"))
                .strokeWidth(3)
                .fillColor(Color.parseColor("#50ff5252")));*/


    }


    @Override
    public void onMapReady(GoogleMap map) {
        map.clear();
        try {
            boolean isSuccess = map.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.my_json_maps));

            if (!isSuccess) {
                Log.e("ERRORR", "MAP LOAD FAILED !!!!");
            }
        } catch (Resources.NotFoundException ex) {

            Log.v(TAG, ex.toString());
            ex.printStackTrace();
        }
        mMap = map;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        } else {
            mMap.getUiSettings().setMapToolbarEnabled(false);
            // mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setIndoorLevelPickerEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setCompassEnabled(true);
          /*  mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(-7.257472, 112.752090)));*/
        }


    }


    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }





    private void ShowData(DataEmployeeResign data){

        scrollView.setVisibility(View.VISIBLE);


        try {

            if (data.getLatitude() != null && data.getLongitude() != null) {

                latitude = Double.parseDouble(data.getLatitude());
                longitude = Double.parseDouble(data.getLongitude());
                txtLatitude.setText(data.getLatitude());
                txtLongitude.setText(data.getLongitude());


                getAddress(this, latitude, longitude);

                mLoadMaps();
            }


        } catch (Exception e){

        }


    }



    void getAddress(Context context, double LATITUDE, double LONGITUDE) {

        //Set Address
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null && addresses.size() > 0) {



                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                txtAlamat.setText(address);
                Log.d(TAG, "getAddress:  address" + address);
                Log.d(TAG, "getAddress:  city" + city);
                Log.d(TAG, "getAddress:  state" + state);
                Log.d(TAG, "getAddress:  postalCode" + postalCode);
                Log.d(TAG, "getAddress:  knownName" + knownName);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(LokasiKerjaActivity.this);


    }



    public void mGetActiveUser() {

        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(LokasiKerjaActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Map<String, String> data = new HashMap<>();

            data.put("employeeid", PreferenceManager.getDefaultSharedPreferences(LokasiKerjaActivity.this)
                    .getString(getResources().getString(R.string.pref_employeeid_view), ""));


            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDataEmployeeResign> call = api.getEmployeeResign(Authorization, data);

            call.enqueue(new Callback<ResponseDataEmployeeResign>() {
                @Override
                public void onResponse(Call<ResponseDataEmployeeResign> call, Response<ResponseDataEmployeeResign> response) {
                    try {
                        loading.dismiss();
                        if (response.isSuccessful()) {
                            ShowData(response.body().getData());

                        }

                    } catch (Exception e) {
                        loading.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDataEmployeeResign> call, Throwable t) {
                    loading.dismiss();
                }
            });
        } catch (Exception e) {
            loading.dismiss();
        }
    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}

