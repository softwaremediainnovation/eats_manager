/*
 * Copyright (C) 2017
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.valdesekamdem.library.mdtoast.MDToast;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.fragment.CertificateEmployeFragment;
import app.direksi.hras.fragment.EducationEmployeFragment;
import app.direksi.hras.fragment.JobEmployeFragment;
import app.direksi.hras.fragment.OrganizationEmployeFragment;
import app.direksi.hras.fragment.OrganizationFragment;
import app.direksi.hras.fragment.ProfilFragmentActivity;
import app.direksi.hras.fragment.RelativeEmployeeFragment;
import app.direksi.hras.fragment.RuleEmployeFragment;
import app.direksi.hras.fragment.SummaryFragment;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.model.DataItemEmployeeShare;
import app.direksi.hras.model.ResponseDetailEmployeeShare;
import app.direksi.hras.util.AESUtils;
import app.direksi.hras.util.HrsApp;
import app.direksi.hras.util.MyContextWrapper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.app.AppConstants.urlShare;
import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class DetailEmployeActivity extends AppCompatActivity
		implements AppBarLayout.OnOffsetChangedListener {

	private static final int PERCENTAGE_TO_ANIMATE_AVATAR = 20;
	private boolean mIsAvatarShown = true;

	private ImageView mProfileImage;
	private int mMaxScrollSize;
	DataEmployee dataEmploye;

	private Intent shareIntent;
	private String shareValue;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MyContextWrapper.refreshBahasa(DetailEmployeActivity.this);
		setContentView(R.layout.activity_detail_employe);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setElevation(0);//value 0 to remove line

		//change transition open and close layout
		//overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);

		SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_employe));
		ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
		getSupportActionBar().setTitle(ss);

		if (getSupportActionBar() != null) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				getSupportActionBar().setDisplayHomeAsUpEnabled(true);
				getSupportActionBar().setHomeButtonEnabled(true);

				getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
			}
			getSupportActionBar().setDisplayShowCustomEnabled(true);
			getSupportActionBar().setDisplayShowTitleEnabled(true);
		}

		TabLayout tabLayout = (TabLayout) findViewById(R.id.materialup_tabs);
		ViewPager viewPager  = (ViewPager) findViewById(R.id.materialup_viewpager);
		AppBarLayout appbarLayout = (AppBarLayout) findViewById(R.id.materialup_appbar);
		mProfileImage = (ImageView) findViewById(R.id.profile_image);

		Gson gson = new Gson();
		dataEmploye = gson.fromJson(getIntent().getStringExtra("detail"), DataEmployee.class);


		PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
				edit().putString(getResources().getString(R.string.pref_attendance_id), String.valueOf(dataEmploye.getEmployeeID()))
				.putString(getResources().getString(R.string.pref_attendance_firstname),  dataEmploye.getFirstName())
				.putString(getResources().getString(R.string.pref_attendance_lastname),  dataEmploye.getLastName())
				.apply();

		//viewPager.setOffscreenPageLimit(9);


		appbarLayout.addOnOffsetChangedListener(this);
		mMaxScrollSize = appbarLayout.getTotalScrollRange();

		viewPager.setAdapter(new TabsAdapter(getSupportFragmentManager(), dataEmploye, DetailEmployeActivity.this));
		tabLayout.setupWithViewPager(viewPager);
	}

	public static void start(Context c) {
		c.startActivity(new Intent(c, DetailEmployeActivity.class));
	}

	@Override
	public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
		/*if (mMaxScrollSize == 0)
			mMaxScrollSize = appBarLayout.getTotalScrollRange();

		int percentage = (Math.abs(i)) * 100 / mMaxScrollSize;

		if (percentage >= PERCENTAGE_TO_ANIMATE_AVATAR && mIsAvatarShown) {
			mIsAvatarShown = false;

			mProfileImage.animate()
					.scaleY(0).scaleX(0)
					.setDuration(200)
					.start();
		}

		if (percentage <= PERCENTAGE_TO_ANIMATE_AVATAR && !mIsAvatarShown) {
			mIsAvatarShown = true;

			mProfileImage.animate()
					.scaleY(1).scaleX(1)
					.start();
		}*/
	}

	private static class TabsAdapter extends FragmentPagerAdapter {
		private static final int TAB_COUNT = 9;
		private DataEmployee data;
		private Context context;

		TabsAdapter(FragmentManager fm, DataEmployee a, Context cont) {
			super(fm);
			this.data = a;
			context = cont;
		}

		@Override
		public int getCount() {
			return TAB_COUNT;
		}

		@Override
		public Fragment getItem(int i) {

			final Fragment result;
			switch (i) {
				case 0:
					// First Tab
				/*	String nama = data.getFirstName()==null ? "-":data.getFirstName() + " " + data.getLastName();
					String role = data.getGender()== null? "-": data.getGender();
					String email = data.getEmail()== null? "-":data.getEmail();
					String phone = data.getPhone()== null? "-": data.getPhone();
					String alamat = data.getAddress()== null? "-": data.getAddress();
					String nik = data.getNik()==null? "-": data.getNik();
					String url = data.getPhotoUrl() == null ? "-" : data.getPhotoUrl();
					String dob = data.getDob() == null ? "-" : data.getDob();
					result = ProfilFragmentActivity.newInstance(nama,role, email, phone, alamat, nik, url, dob);*/
                    result = ProfilFragmentActivity.newInstance();
					break;
				case 1:
					// Second Tab
					result = SummaryFragment.newInstance();
					break;
				case 2:
					// Second Tab
					result = OrganizationFragment.newInstance();
					break;
				case 3:
					// Second Tab
					result = RuleEmployeFragment.newInstance();
					break;
				case 4:
					// Second Tab
					result = RelativeEmployeeFragment.newInstance();
					break;
				case 5:
					// Third Tab
					result = OrganizationEmployeFragment.newInstance();
					break;
				case 6:
					// Third Tab
					result = JobEmployeFragment.newInstance();
					break;
				case 7:
					// Third Tab
					result = EducationEmployeFragment.newInstance();
					break;
				case 8:
					// Third Tab
					result = CertificateEmployeFragment.newInstance();
					break;
				default:
					result = FakePageFragment.newInstance();
					break;
			}
			return result;



		}

		@Override
		public CharSequence getPageTitle(int position) {
			String title ="";
			if (position == 0){
				title = context.getResources().getString(R.string.title_profil);
			}
			if (position == 1){
				title = context.getResources().getString(R.string.title_ringkasan);
			}
			if (position == 2){
				title = context.getResources().getString(R.string.title_organisasi);
			}
			if (position == 3){
				title = context.getResources().getString(R.string.title_peraturan);
			}
			if (position == 4){
				title = context.getResources().getString(R.string.title_kerabat);
			}
			if (position == 5){
				title = context.getResources().getString(R.string.title_pengalaman_organisasi);
			}
			if (position == 6){
				title = context.getResources().getString(R.string.title_pekerjaan);
			}
			if (position == 7){
				title = context.getResources().getString(R.string.title_pendidikan);
			}
			if (position == 8){
				title = context.getResources().getString(R.string.title_sertifikast);
			}
			return title;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				//overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
				return true;
			case R.id.action_sharing:
				loadApi();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_sharing, menu);

		try {
			MenuItem share = (MenuItem) menu.findItem(R.id.action_sharing).getActionView();
			share.setVisible(true);

		}catch(Exception e){e.printStackTrace();}

		return super.onCreateOptionsMenu(menu);
	}

	private void loadApi() {

		try {

			String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailEmployeActivity.this).getString(
					getResources().getString(R.string.pref_token), ""));

			String id = (PreferenceManager.getDefaultSharedPreferences(DetailEmployeActivity.this).getString(
					getResources().getString(R.string.pref_employeeid_view), ""));




			Retrofit retrofit = new Retrofit.Builder()
					.baseUrl(getResources().getString(R.string.base_url))
					.addConverterFactory(GsonConverterFactory.create())
					.build();
			RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

			// simplified call to request the news with already initialized service
			Call<ResponseDetailEmployeeShare> call = api.getEmployeeIDShare(Authorization, id);

			call.enqueue(new Callback<ResponseDetailEmployeeShare>() {
				@Override
				public void onResponse(Call<ResponseDetailEmployeeShare> call, Response<ResponseDetailEmployeeShare> response) {
					if (response.isSuccessful()) {
						if (response.body().getData().size()>0) {
							handleMenuShare(response.body().getData().get(0));
						} else {
							MDToast.makeText(DetailEmployeActivity.this, getResources().getString(R.string.loading_error),
									MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
						}


					} else {
						try {
							MDToast.makeText(DetailEmployeActivity.this, getResources().getString(R.string.loading_error),
									MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
							// Toast.makeText(getActivity(), R.string.loading_error, Toast.LENGTH_SHORT).show();
						}
						catch (Exception e)
						{
							MDToast.makeText(DetailEmployeActivity.this, getResources().getString(R.string.loading_error),
									MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
						}
						//  Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
					}
				}

				@Override
				public void onFailure(Call<ResponseDetailEmployeeShare> call, Throwable t) {
					MDToast.makeText(DetailEmployeActivity.this, getResources().getString(R.string.loading_error),
							MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
					// Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
				}
			});
		} catch (Exception e) {
			MDToast.makeText(DetailEmployeActivity.this, getResources().getString(R.string.loading_error),
					MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
			// Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_LONG).show();
			// Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
		}

	}

	private void handleMenuShare(DataItemEmployeeShare data) {


		try {
			if (data.getProfile().getPhotoUrl() == null || data.getProfile().getPhotoUrl().equals(DetailEmployeActivity.this.getResources().getString(R.string.base_url))){

				String value = method(data);
				Toast.makeText(getApplicationContext(), "Tidak ada gambar tersedia. ", Toast.LENGTH_LONG).show();

				// Toast.makeText(getApplicationContext(), "Gambar pagi dekat tidak tersedia. ", Toast.LENGTH_SHORT).show();
				shareIntent = new Intent(android.content.Intent.ACTION_SEND);
				shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				shareIntent.setType("text/plain");
				shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				shareIntent.putExtra(Intent.EXTRA_TEXT,"*<< DETAIL PEGAWAI EATS >>* \n\n" + value);
				startActivity(Intent.createChooser(shareIntent,"Kirim ke"));
			}else if(data.getProfile().getPhotoUrl() == ""){


				String value = method(data);
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.title_tidak_ada_gambar_tersedia), Toast.LENGTH_LONG).show();

				// Toast.makeText(getApplicationContext(), "Gambar pagi dekat tidak tersedia. ", Toast.LENGTH_SHORT).show();
				shareIntent = new Intent(android.content.Intent.ACTION_SEND);
				shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
				shareIntent.setType("text/plain");
				shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				shareIntent.putExtra(Intent.EXTRA_TEXT,"*<< DETAIL PEGAWAI EATS >>* \n\n" + value);
				startActivity(Intent.createChooser(shareIntent,"Kirim ke"));
			} else {


				Picasso.with(this)
						.load(this.getResources().getString(R.string.base_url) +data.getProfile().getPhotoUrl())
						.into(new Target() {
							@Override
							public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {

								String value = method(data);

								shareIntent = new Intent(android.content.Intent.ACTION_SEND);
								shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
								shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								shareIntent.putExtra(Intent.EXTRA_STREAM,  getLocalBitmapUri(bitmap));
								shareIntent.putExtra(Intent.EXTRA_TEXT,"*<< DETAIL PEGAWAI EATS >>*\n" + value);
								shareIntent.setType("image/jpeg");
								startActivity(Intent.createChooser(shareIntent,"Kirim ke"));

							}

							@Override
							public void onBitmapFailed(Drawable errorDrawable) {
								Snackbar.make(getWindow().getDecorView(), getResources().getString(R.string.title_gagal_memuat_gambar), Snackbar.LENGTH_LONG).show();
							}

							@Override
							public void onPrepareLoad(Drawable placeHolderDrawable) {

							}
						});
			}

		}catch (Exception e){
			Log.e("ErrorShare", "handleMenuShare: "+e.toString());
		}

	}



	public Uri getLocalBitmapUri(Bitmap bmp) {
		final String appPackageName = "." + DetailEmployeActivity.this.getPackageName();
		Uri bmpUri = null;
		try {
			File file =  new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".jpeg");
			FileOutputStream out = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.JPEG, 90, out);
			out.close();
//            bmpUri = Uri.fromFile(file);
			bmpUri = FileProvider.getUriForFile(getApplicationContext(), getApplicationContext().getPackageName() + appPackageName, file);
		} catch (IOException e) {
			Log.e("ErrorShare", "getLocalBitmapUri: "+e.toString());
			e.printStackTrace();
		}
		return bmpUri;
	}

	private  String method(DataItemEmployeeShare data) {

		String catatan = data.getProfile().getNote() == null? "":data.getProfile().getNote();
		String encrypted = "";
		String sourceStr = String.valueOf(data.getProfile().getEmployeeID());
		try {
			encrypted = AESUtils.encrypt(sourceStr);
			Log.d("TEST", "encrypted:" + encrypted);
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.title_gagal_membuat_link), Toast.LENGTH_LONG).show();
		}

		String dob = "", join = "",end = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date dateOfBirth = sdf.parse(String.valueOf(data.getProfile().getDob()));
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
			dob = formatter.format(dateOfBirth);


		}catch (Exception e){
			Log.d("Error date = ", e.toString());
//                                    Toast.makeText(getApplicationContext(), "Format Waktu Salah !", Toast.LENGTH_LONG).show();
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date dateOfBirth = sdf.parse(String.valueOf(data.getProfile().getJoinDate()));
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
			join = formatter.format(dateOfBirth);


		}catch (Exception e){
			Log.d("Error date = ", e.toString());
//                                    Toast.makeText(getApplicationContext(), "Format Waktu Salah !", Toast.LENGTH_LONG).show();
		}
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date dateOfBirth = sdf.parse(String.valueOf(data.getProfile().getContractExpiredDate()));
			SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
			end = formatter.format(dateOfBirth);


		}catch (Exception e){
			Log.d("Error date = ", e.toString());
//                                    Toast.makeText(getApplicationContext(), "Format Waktu Salah !", Toast.LENGTH_LONG).show();
		}


		String  relative = "",  organization = "",  employment = "", education = "", certificate = "";

		if (data.getRelative().size() > 0) {

			for (int i = 0; i < data.getRelative().size(); i++) {

				relative = relative + "\n*HUBUNGAN"+"* : " + data.getRelative().get(i).getRelation() +
				"\n*NAMA* : " +data.getRelative().get(i).getName() +
				"\n*UMUR* : " +data.getRelative().get(i).getAge() + " Tahun" +
				"\n*JENIS KELAMIN* : " +data.getRelative().get(i).getGender() +
				"\n*PEKERJAAN* : " +data.getRelative().get(i).getJob()+"\n";

			}

		} else {
			relative =   "\n*No Result*";
		}

		if (data.getOrganization().size() > 0) {

			for (int i = 0; i < data.getOrganization().size(); i++) {

				String startt = "", endd = "";

				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date dDateFrom = sdf.parse(data.getOrganization().get(i).getDateStart());
					Date dDateTo = sdf.parse(data.getOrganization().get(i).getDateEnd());
					SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
					startt = formatter.format(dDateFrom);
					endd = formatter.format(dDateTo);

				}catch (Exception e){
					Log.d("Error = ", e.toString());
				}


				organization = organization + "\n*NAMA"+"* : " + data.getOrganization().get(i).getName() +
						"\n*MULAI* : " +startt +
						"\n*AKHIR* : " +endd + "\n";

			}

		} else {
			organization =   "\n*No Result*";
		}

		if (data.getEmployment().size() > 0) {

			for (int i = 0; i < data.getEmployment().size(); i++) {

				String startt = "", endd = "";

				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date dDateFrom = sdf.parse(data.getEmployment().get(i).getDateStart());
					Date dDateTo = sdf.parse(data.getEmployment().get(i).getDateEnd());
					SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
					startt = formatter.format(dDateFrom);
					endd = formatter.format(dDateTo);

				}catch (Exception e){
					Log.d("Error = ", e.toString());
				}


				employment = employment + "\n*NAMA"+"* : " + data.getEmployment().get(i).getCompany() +
						"\n*JABATAN* : " +data.getEmployment().get(i).getPosition() +
						"\n*MULAI* : " +startt +
						"\n*AKHIR* : " +endd + "\n";

			}

		} else {
			employment =   "\n*No Result*";
		}

		if (data.getEducation().size() > 0) {

			for (int i = 0; i < data.getEducation().size(); i++) {

				String startt = "", endd = "";

				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date dDateFrom = sdf.parse(data.getEducation().get(i).getC().getDateStart());
					Date dDateTo = sdf.parse(data.getEducation().get(i).getC().getDateEnd());
					SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
					startt = formatter.format(dDateFrom);
					endd = formatter.format(dDateTo);

				}catch (Exception e){
					Log.d("Error = ", e.toString());
				}


				education = education + "\n*NAMA"+"* : " + data.getEducation().get(i).getC().getInstituteName() +
						"\n*TINGKAT* : " +data.getEducation().get(i).getM().getName() +
						"\n*MULAI* : " +startt +
						"\n*AKHIR* : " +endd + "\n";

			}

		} else {
			education =   "\n*No Result*";
		}

		if (data.getCertificate().size() > 0) {

			for (int i = 0; i < data.getCertificate().size(); i++) {

				String startt = "", endd = "";

				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date dDateFrom = sdf.parse(data.getCertificate().get(i).getDateFrom());
					Date dDateTo = sdf.parse(data.getCertificate().get(i).getDateTo());
					SimpleDateFormat formatter = new SimpleDateFormat("dd MMMM yyyy");
					startt = formatter.format(dDateFrom);
					endd = formatter.format(dDateTo);

				}catch (Exception e){
					Log.d("Error = ", e.toString());
				}


				certificate = certificate + "\n*NAMA"+"* : " + data.getCertificate().get(i).getName() +
						"\n*MULAI* : " +startt +
						"\n*AKHIR* : " +endd + "\n";

			}

		} else {
			certificate =   "\n*No Result*";
		}


		shareValue =
						"*ID PEGAWAI* : "+ String.valueOf(data.getProfile().getEmployeeID()) +
						"\n*NIK* : "+data.getProfile().getNik()+
						"\n*NAMA* : "+ data.getProfile().getFirstName()+ " " + data.getProfile().getLastName() +
                        "\n*ORGANISASI* : "+data.getOrganisasi()+
						"\n*JENIS KELAMIN* : "+data.getProfile().getGender()+
						"\n*EMAIL* : "+data.getProfile().getEmail()+
						"\n*TELEPON* : "+data.getProfile().getPhone()+
						"\n*ALAMAT* : "+data.getProfile().getAddress()+
						"\n*TANGGAL LAHIR* : "+dob+
						"\n*TANGGAL BERGABUNG* : "+join+
						"\n*KONTRAK BERAKHIR* : "+end+
						"\n*CATATAN* : "+catatan+   "\n" +

						"\n\n*<< KERABAT >>*" + relative +
						"\n\n*<< PENGALAMAN ORGANISASI >>*" + organization +
						"\n\n*<< PEKERJAAN >>*" + employment +
						"\n\n*<< PENDIDIKAN >>*" + education +
						"\n\n*<< SERTIFIKAT >>*" + certificate +

                        "\n\n"+
						urlShare+ encrypted.toLowerCase() +
                        "\n";



		return shareValue;
	}

	@Override
	public void onResume() {

		MyContextWrapper.refreshBahasa(DetailEmployeActivity.this);
        try {
        	int UserID = 0;
            String foo = "";
            super.onResume();
            Intent intent = getIntent();
            Uri uri = intent.getData();

            if (uri == null){

			}else if (uri.toString().contains(urlShare)){

				foo = uri.getQueryParameter("id");
				String encrypted = foo.toUpperCase();
				String decrypted = "";
				try {
					decrypted = AESUtils.decrypt(encrypted);
					Log.d("TEST", "decrypted:" + decrypted);

					try {

						UserID = Integer.parseInt(decrypted);

						if (PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(
								getResources().getString(R.string.pref_nama), "").isEmpty()) {

							Intent intentz = new Intent(getApplicationContext(), LoginActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
							startActivity(intentz);
							finish();
						}


						if (!foo.equals("")) {
							PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).
									edit().putString(getResources().getString(R.string.pref_dayoff_flag), "1")
									.putString(getResources().getString(R.string.pref_employeeid_view), String.valueOf(UserID))
									.apply();
							//Toast.makeText(getApplicationContext(), decrypted, Toast.LENGTH_LONG).show();
						}

					}
					catch (Exception s){
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.title_link_tidak_valid), Toast.LENGTH_LONG).show();
						finish();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.title_link_tidak_valid), Toast.LENGTH_LONG).show();
					finish();
				}

			} else {
				Toast.makeText(getApplicationContext(), getResources().getString(R.string.title_link_tidak_valid), Toast.LENGTH_LONG).show();
				finish();
			}

        } catch (Exception e) {
			e.printStackTrace();
        }
    }

	protected void attachBaseContext(Context newBase) {

		String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
				newBase.getResources().getString(R.string.pref_bahasa), ""));

		if (bahasa.equals("") || bahasa.equals(null)){

			bahasa = "en";
		}
		Context context = changeLang(newBase, bahasa);
		super.attachBaseContext(context);

	}


}
