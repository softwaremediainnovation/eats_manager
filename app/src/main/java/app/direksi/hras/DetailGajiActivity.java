package app.direksi.hras;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.valdesekamdem.library.mdtoast.MDToast;

import app.direksi.hras.model.RemuData;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 15/03/2019.
 */

public class DetailGajiActivity extends AppCompatActivity {

    private  WebView web1;
    SweetAlertDialog loading;
    RemuData remuData;
    private MessageDialog messageDialog;
    private String idPayroll ="";
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailGajiActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //   overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_gaji));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        setContentView(R.layout.activity_detail_gaji);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        messageDialog = new MessageDialog();
        loading = new SweetAlertDialog(DetailGajiActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        loading.show();

        Intent i = getIntent();
        idPayroll = i.getStringExtra("detail");


       /* if (idPayroll != null) {
            DetailLembur(idPayroll);
        } else {
            onNewIntent(getIntent());
        }*/

        web1 = findViewById(R.id.web1);
        web1.getSettings().setJavaScriptEnabled(true); // enable javascript
        web1.getSettings().setBuiltInZoomControls(true);
        web1.setInitialScale(100);




        web1.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // Log.i(TAG, "Processing webview url click...");
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                // Log.i(TAG, "Finished loading URL: " + url);
                if (loading.isShowing()) {
                    loading.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

                try {
                    messageDialog.mShowMessageError(DetailGajiActivity.this, "GAGAL"
                            , getResources().getString(R.string.loading_error));
                }catch (Exception r){

                }
            }
        });
        String url = getResources().getString(R.string.base_url)+ "/Payrolls/CetakSlipGajiMobile/" + idPayroll;
        web1 .loadUrl(url );


    }



    private void createWebPrintJob(WebView webView) {

        try {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                PrintManager printManager = (PrintManager) this
                        .getSystemService(Context.PRINT_SERVICE);

                PrintDocumentAdapter printAdapter =
                        webView.createPrintDocumentAdapter();

                String jobName = "gaji";

                if (printManager != null) {
                    printManager.print(jobName, printAdapter,
                            new PrintAttributes.Builder().build());
                }
            } else {
                MDToast.makeText(DetailGajiActivity.this, "Gagal download, minimum Android kitkat", MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
            }
        } catch (Exception e){

            MDToast.makeText(DetailGajiActivity.this, e.toString(), MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();

        }

    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.menu_download, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;

            case R.id.action_search:

                createWebPrintJob(web1);

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailGajiActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
