
package app.direksi.hras.service;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import app.direksi.hras.DetailAktivitasActivity;
import app.direksi.hras.DetailClaimActivity;
import app.direksi.hras.DetailClaimKuActivity;
import app.direksi.hras.DetailCutiActivity;
import app.direksi.hras.DetailCutiKuActivity;
import app.direksi.hras.DetailLemburActivity;
import app.direksi.hras.DetailLemburKuActivity;
import app.direksi.hras.DetailLoanActivity;
import app.direksi.hras.DetailLoanKuActivity;
import app.direksi.hras.DetailNewsActivity;
import app.direksi.hras.DetailRequestAttendanceActivity;
import app.direksi.hras.DetailRequestAttendanceKuActivity;
import app.direksi.hras.DetailTaskActivity;
import app.direksi.hras.DetailTaskDailyActivity;
import app.direksi.hras.DetailTaskKuActivity;
import app.direksi.hras.DetailTeguranKuActivity;
import app.direksi.hras.DetailTrackingAsetKuActivity;
import app.direksi.hras.R;
import app.direksi.hras.SplashScreenActivity;
import app.direksi.hras.TransferDetailActivity;
import app.direksi.hras.model.AssetMyAssetList;
import app.direksi.hras.model.DataAktivitas;
import app.direksi.hras.model.DataClaim;
import app.direksi.hras.model.DataCuti;
import app.direksi.hras.model.DataLembur;
import app.direksi.hras.model.DataLoan;
import app.direksi.hras.model.DataNews;
import app.direksi.hras.model.DataReprimand;
import app.direksi.hras.model.DataRequestAttendance;
import app.direksi.hras.model.DataTask;
import app.direksi.hras.model.JsonNotif;
import app.direksi.hras.model.JsonNotifString;
import app.direksi.hras.util.NotificationUtils;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private String mTitle, mMessage;
    private Integer mId;
    private Intent resultIntent;
    private String idXendid;


   /* @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "DataOrder Payload: " + remoteMessage.getData().toString());
            try {
                // JSONObject json = new JSONObject(remoteMessage.getData().toString());

                JSONObject json = new JSONObject(String.valueOf(remoteMessage.getData().toString()));

                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }

    }*/

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {


        try {
            Log.e(TAG, "From: " + remoteMessage.getFrom());

            if (remoteMessage == null)
                return;

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "DataOrder Payload: " + remoteMessage.getData().toString());
                try {
                    // JSONObject json = new JSONObject(remoteMessage.getData().toString());
                    String pesan =   remoteMessage.getNotification().getBody();


                    JSONObject json = new JSONObject(String.valueOf(remoteMessage.getData().toString()));

                    handleDataMessage(json, pesan);

                } catch (Exception e) {

                    String pesan =   remoteMessage.getNotification().getBody();
                    Map<String, String> params = remoteMessage.getData();
                    JSONObject object = new JSONObject(params);

                    String id = "";
                    String title = "";
                    String message = "";
                    try {
                        id = object.getString("body");
                        title = object.getString("click_action");
                        message = object.getString("title");
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }

                    handleErrorDataMessage(id, title, message, pesan);

                }

                notificationUtils = new NotificationUtils(getApplicationContext());
                notificationUtils.playNotificationSound();

            }
        } catch (Exception e) {

        }
    }

    private void handleDataMessage(JSONObject json, String pesan) {

        try {

            Gson gson = new Gson();
            JsonNotif responseNotif = new JsonNotif();


            Object aObj = json.get("body");
            if(aObj instanceof Integer){
                responseNotif = gson.fromJson(json.toString(), JsonNotif.class);
            } else {
                JsonNotifString temp = gson.fromJson(json.toString(), JsonNotifString.class);
                responseNotif.setBody(19);
                responseNotif.setIcon(temp.getIcon());
                responseNotif.setSound(temp.getSound());
                responseNotif.setTitle(temp.getTitle());
                responseNotif.setClickAction(temp.getClickAction());
                responseNotif.setShowInForeground(temp.getShowInForeground());
                idXendid = temp.getBody();
            }





            mTitle = responseNotif.getClickAction();
            mMessage = responseNotif.getTitle();
            mId = responseNotif.getBody();

            if (mTitle.equalsIgnoreCase("Article") || mTitle.contains("Article")) {
                resultIntent = new Intent(getApplicationContext(), DetailNewsActivity.class);
                DataNews dataNewsItem = new DataNews();
                dataNewsItem.setArticleID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataNewsItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("Activity") || mTitle.contains("Activity")) {
                resultIntent = new Intent(getApplicationContext(), DetailAktivitasActivity.class);
                DataAktivitas dataAsetItem = new DataAktivitas();
                dataAsetItem.setActivityID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }else if (mTitle.equalsIgnoreCase("HandoverAsset") || mTitle.contains("HandoverAsset")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("AssetTracking") || mTitle.contains("AssetTracking")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("RejectAsset") || mTitle.contains("RejectAsset")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("Asset") || mTitle.contains("Asset")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }else if (mTitle.equalsIgnoreCase("RequestDayOff") || mTitle.contains("RequestDayOff")) {
                DataCuti dataCutiItem = new DataCuti();
                dataCutiItem.setDayOffID(Long.valueOf(mId));
                resultIntent = new Intent(getApplicationContext(), DetailCutiActivity.class);
                gson = new Gson();
                String myJson = gson.toJson(dataCutiItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveDayOff") || mTitle.contains("ApproveDayOff")) {
                DataCuti dataCutiItem = new DataCuti();
                dataCutiItem.setDayOffID(Long.valueOf(mId));
                resultIntent = new Intent(getApplicationContext(), DetailCutiKuActivity.class);
                gson = new Gson();
                String myJson = gson.toJson(dataCutiItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestLoan") || mTitle.contains("RequestLoan")) {
                resultIntent = new Intent(getApplicationContext(), DetailLoanActivity.class);
                DataLoan dataLoanItem = new DataLoan();
                dataLoanItem.setLoanID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataLoanItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveLoan") || mTitle.contains("ApproveLoan")) {
                resultIntent = new Intent(getApplicationContext(), DetailLoanKuActivity.class);
                DataLoan dataLoanItem = new DataLoan();
                dataLoanItem.setLoanID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataLoanItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestOvertime") || mTitle.contains("RequestOvertime")) {
                resultIntent = new Intent(getApplicationContext(), DetailLemburActivity.class);
                DataLembur dataOvertimeItem = new DataLembur();
                dataOvertimeItem.setOvertimeID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataOvertimeItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveOvertime") || mTitle.contains("ApproveOvertime")) {
                resultIntent = new Intent(getApplicationContext(), DetailLemburKuActivity.class);
                DataLembur dataOvertimeItem = new DataLembur();
                dataOvertimeItem.setOvertimeID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataOvertimeItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestReimbursement") || mTitle.contains("RequestReimbursement")) {
                resultIntent = new Intent(getApplicationContext(), DetailClaimActivity.class);
                DataClaim dataReimbursementItem = new DataClaim();
                dataReimbursementItem.setReimbursementID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveReimbursement") || mTitle.contains("ApproveReimbursement")) {
                resultIntent = new Intent(getApplicationContext(), DetailClaimKuActivity.class);
                DataClaim dataReimbursementItem = new DataClaim();
                dataReimbursementItem.setReimbursementID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("Reprimand") || mTitle.contains("Reprimand")) {
                resultIntent = new Intent(getApplicationContext(), DetailTeguranKuActivity.class);
                DataReprimand dataReprimandsItem = new DataReprimand();
                dataReprimandsItem.setReprimandID(Long.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataReprimandsItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestAttendance") || mTitle.contains("RequestAttendance")) {
                resultIntent = new Intent(getApplicationContext(), DetailRequestAttendanceActivity.class);
                DataRequestAttendance dataReimbursementItem = new DataRequestAttendance();
                dataReimbursementItem.setAttendanceID(Integer.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveAttendance") || mTitle.contains("ApproveAttendance")) {
                resultIntent = new Intent(getApplicationContext(), DetailRequestAttendanceKuActivity.class);
                DataRequestAttendance dataReimbursementItem = new DataRequestAttendance();
                dataReimbursementItem.setAttendanceID(Integer.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("NewTask") || mTitle.contains("NewTask")) {
                resultIntent = new Intent(getApplicationContext(), DetailTaskKuActivity.class);
                DataTask dataTask = new DataTask();
                dataTask.setTaskHeaderID(Integer.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataTask);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("TaskFinish") || mTitle.contains("TaskFinish")) {
                resultIntent = new Intent(getApplicationContext(), DetailTaskActivity.class);
                DataTask dataTask = new DataTask();
                dataTask.setTaskHeaderID(Integer.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataTask);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("Schedule") || mTitle.contains("Schedule")) {
                resultIntent = new Intent(getApplicationContext(), DetailTaskDailyActivity.class);
                DataTask dataTask = new DataTask();
                dataTask.setTaskHeaderID(Integer.valueOf(mId));
                gson = new Gson();
                String myJson = gson.toJson(dataTask);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ProcessReimbursement") || mTitle.contains("ProcessReimbursement")) {
                resultIntent = new Intent(getApplicationContext(), TransferDetailActivity.class);
                resultIntent.putExtra("detail", idXendid);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ProcessLoan") || mTitle.contains("ProcessLoan")) {
                resultIntent = new Intent(getApplicationContext(), TransferDetailActivity.class);
                resultIntent.putExtra("detail", idXendid);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else {
                resultIntent = new Intent(getApplicationContext(), SplashScreenActivity.class);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);
            }



            // resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK );
            resultIntent.setAction("dummy_action_" + mId);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);
            //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);

            String channelId = "Default";
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(mMessage)
                    .setContentText(pesan).setAutoCancel(true).setContentIntent(pendingIntent);

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
                manager.createNotificationChannel(channel);
            }
            manager.notify(mId, builder.build());
            // playNotificationSound();

        } catch (Exception e) {
            String a = e.toString();
            String b = a;
        }


    }

    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + getApplicationContext().getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }


    private void handleErrorDataMessage(String mId, String mTitle, String mMessage, String pesan) {

        Integer id = 19;
        String temp = mId;


        try {
            int num = Integer.parseInt(mId);
            id = num;

        } catch (NumberFormatException e) {
            temp = mId;
        }


        Gson gson = new Gson();

        try {

            if (mTitle.equalsIgnoreCase("Article") || mTitle.contains("Article")) {
                resultIntent = new Intent(getApplicationContext(), DetailNewsActivity.class);
                DataNews dataNewsItem = new DataNews();
                dataNewsItem.setArticleID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataNewsItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("Activity") || mTitle.contains("Activity")) {
                resultIntent = new Intent(getApplicationContext(), DetailAktivitasActivity.class);
                DataAktivitas dataAsetItem = new DataAktivitas();
                dataAsetItem.setActivityID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }else if (mTitle.equalsIgnoreCase("HandoverAsset") || mTitle.contains("HandoverAsset")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("AssetTracking") || mTitle.contains("AssetTracking")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("RejectAsset") || mTitle.contains("RejectAsset")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("Asset") || mTitle.contains("Asset")) {
                resultIntent = new Intent(getApplicationContext(), DetailTrackingAsetKuActivity.class);
                AssetMyAssetList dataAsetItem = new AssetMyAssetList();
                dataAsetItem.setAssetID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataAsetItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }else if (mTitle.equalsIgnoreCase("RequestDayOff") || mTitle.contains("RequestDayOff")) {
                DataCuti dataCutiItem = new DataCuti();
                dataCutiItem.setDayOffID(Long.valueOf(id));
                resultIntent = new Intent(getApplicationContext(), DetailCutiActivity.class);
                gson = new Gson();
                String myJson = gson.toJson(dataCutiItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveDayOff") || mTitle.contains("ApproveDayOff")) {
                DataCuti dataCutiItem = new DataCuti();
                dataCutiItem.setDayOffID(Long.valueOf(id));
                resultIntent = new Intent(getApplicationContext(), DetailCutiKuActivity.class);
                gson = new Gson();
                String myJson = gson.toJson(dataCutiItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestLoan") || mTitle.contains("RequestLoan")) {
                resultIntent = new Intent(getApplicationContext(), DetailLoanActivity.class);
                DataLoan dataLoanItem = new DataLoan();
                dataLoanItem.setLoanID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataLoanItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveLoan") || mTitle.contains("ApproveLoan")) {
                resultIntent = new Intent(getApplicationContext(), DetailLoanKuActivity.class);
                DataLoan dataLoanItem = new DataLoan();
                dataLoanItem.setLoanID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataLoanItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestOvertime") || mTitle.contains("RequestOvertime")) {
                resultIntent = new Intent(getApplicationContext(), DetailLemburActivity.class);
                DataLembur dataOvertimeItem = new DataLembur();
                dataOvertimeItem.setOvertimeID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataOvertimeItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveOvertime") || mTitle.contains("ApproveOvertime")) {
                resultIntent = new Intent(getApplicationContext(), DetailLemburKuActivity.class);
                DataLembur dataOvertimeItem = new DataLembur();
                dataOvertimeItem.setOvertimeID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataOvertimeItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestReimbursement") || mTitle.contains("RequestReimbursement")) {
                resultIntent = new Intent(getApplicationContext(), DetailClaimActivity.class);
                DataClaim dataReimbursementItem = new DataClaim();
                dataReimbursementItem.setReimbursementID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveReimbursement") || mTitle.contains("ApproveReimbursement")) {
                resultIntent = new Intent(getApplicationContext(), DetailClaimKuActivity.class);
                DataClaim dataReimbursementItem = new DataClaim();
                dataReimbursementItem.setReimbursementID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("Reprimand") || mTitle.contains("Reprimand")) {
                resultIntent = new Intent(getApplicationContext(), DetailTeguranKuActivity.class);
                DataReprimand dataReprimandsItem = new DataReprimand();
                dataReprimandsItem.setReprimandID(Long.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataReprimandsItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("RequestAttendance") || mTitle.contains("RequestAttendance")) {
                resultIntent = new Intent(getApplicationContext(), DetailRequestAttendanceActivity.class);
                DataRequestAttendance dataReimbursementItem = new DataRequestAttendance();
                dataReimbursementItem.setAttendanceID(Integer.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ApproveAttendance") || mTitle.contains("ApproveAttendance")) {
                resultIntent = new Intent(getApplicationContext(), DetailRequestAttendanceKuActivity.class);
                DataRequestAttendance dataReimbursementItem = new DataRequestAttendance();
                dataReimbursementItem.setAttendanceID(Integer.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataReimbursementItem);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("NewTask") || mTitle.contains("NewTask")) {
                resultIntent = new Intent(getApplicationContext(), DetailTaskKuActivity.class);
                DataTask dataTask = new DataTask();
                dataTask.setTaskHeaderID(Integer.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataTask);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("TaskFinish") || mTitle.contains("TaskFinish")) {
                resultIntent = new Intent(getApplicationContext(), DetailTaskActivity.class);
                DataTask dataTask = new DataTask();
                dataTask.setTaskHeaderID(Integer.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataTask);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("Schedule") || mTitle.contains("Schedule")) {
                resultIntent = new Intent(getApplicationContext(), DetailTaskDailyActivity.class);
                DataTask dataTask = new DataTask();
                dataTask.setTaskHeaderID(Integer.valueOf(id));
                gson = new Gson();
                String myJson = gson.toJson(dataTask);
                resultIntent.putExtra("detail", myJson);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            }  else if (mTitle.equalsIgnoreCase("ProcessReimbursement") || mTitle.contains("ProcessReimbursement")) {
                resultIntent = new Intent(getApplicationContext(), TransferDetailActivity.class);
                resultIntent.putExtra("detail", temp);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else if (mTitle.equalsIgnoreCase("ProcessLoan") || mTitle.contains("ProcessLoan")) {
                resultIntent = new Intent(getApplicationContext(), TransferDetailActivity.class);
                resultIntent.putExtra("detail", temp);
                // showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);

            } else {
                resultIntent = new Intent(getApplicationContext(), SplashScreenActivity.class);
                //  showNotificationMessage(getApplicationContext(), mTitle, mMessage, "", resultIntent);
            }

            resultIntent.setAction("dummy_action_" + id);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_ONE_SHOT);
            // PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, 0);
            String channelId = "Default";
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, channelId)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(mMessage)
                    .setContentText(pesan).setAutoCancel(true).setContentIntent(pendingIntent);

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_DEFAULT);
                manager.createNotificationChannel(channel);
            }
            manager.notify(id, builder.build());
            // playNotificationSound();


        } catch (Exception e) {

        }


    }




}
