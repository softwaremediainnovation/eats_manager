package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.app.AppConstants;
import app.direksi.hras.fragment.CreateReimbursementFragment;
import app.direksi.hras.fragment.CreateSnapFragment;
import app.direksi.hras.model.ResponseSnap;
import app.direksi.hras.model.ResultItem;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;


public class ListSnapActivity extends AppCompatActivity implements View.OnClickListener, AbsListView.OnScrollListener, SwipeRefreshLayout.OnRefreshListener {



    private List<ResultItem> results = new ArrayList<>();
    private List<ResultItem> mArrayList = new ArrayList<>();
    private List<ResultItem> mFilteredList = new ArrayList<>();
    private SnapAdapter snapAdapter;

    TextView txtEmptyList;
    private Filter filter;
    ListView recyclerView;

    private MessageDialog messageDialog;
    Integer pageNumber = 1;
    SwipeRefreshLayout swiperefresh;
    private boolean isScroll = false;
    private LinearLayout mLayoutAnimation;
    FloatingActionButton floatingActionButton;
    SweetAlertDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(ListSnapActivity.this);

        //remove line in bar
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //animation loading

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.snap));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_snap);

        messageDialog = new MessageDialog();

        swiperefresh = findViewById(R.id.swiperefresh);
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setDividerHeight(0);
        recyclerView.setOnScrollListener(this);
        txtEmptyList = findViewById(R.id.txtEmptyList);
        mLayoutAnimation = findViewById(R.id.mLayoutAnimation);
        floatingActionButton = findViewById(R.id.fab);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateSnapFragment dialog = new CreateSnapFragment();
                dialog.show(getFragmentManager(), "");
            }
        });

        swiperefresh.setOnRefreshListener(this);


        loading = new SweetAlertDialog(ListSnapActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        loading.show();


        pageNumber = 1;

        results = new ArrayList<>();

        snapAdapter = new SnapAdapter(getApplicationContext(), R.layout.item_snap, R.id.mTxtName, results);
        snapAdapter.setNotifyOnChange(true);
        recyclerView.setAdapter(snapAdapter);

        snapAdapter.clear();
        mArrayList.clear();
        results.clear();
        mFilteredList.clear();

        mGetNews();
        mGetAllResult();
        isScroll = true;

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news, menu);
        MenuItem search = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();

        MyContextWrapper.refreshBahasa(ListSnapActivity.this);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                try {
                    mFilterCategory(query);
                } catch (Exception e) {
                    loadmoreProcessComplete = true;
                    Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    mFilterCategory("all");
                }
                return true;
            }
        });
    }


    void mFilterCategory(String filter) {
        isScroll = false;
        pageNumber = 1;

        mFilteredList = new ArrayList<>();

        snapAdapter = new SnapAdapter(getApplicationContext(), R.layout.item_snap, R.id.mTxtName, mFilteredList);
        snapAdapter.setNotifyOnChange(true);
        recyclerView.setAdapter(snapAdapter);

        snapAdapter.clear();
        mFilteredList.clear();

        mFilter(filter);
    }


     public void mFilter(String charSequence) {


        if (charSequence.equals("all")) {
            mFilteredList = mArrayList;
        } else {
            ArrayList<ResultItem> filteredList = new ArrayList<>();
            for (ResultItem resultItem : mArrayList) {
                if (Validation.replaceNull(resultItem.getFilePath()).toLowerCase().contains(charSequence)) {
                    filteredList.add(resultItem);
                }
            }

            mFilteredList = filteredList;
        }


        if (mFilteredList.size() > 0) {

            snapAdapter.addAll(mFilteredList);
            snapAdapter.notifyDataSetChanged();
            recyclerView.invalidateViews();
            pageNumber++;
            txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);

        }

        snapAdapter.notifyDataSetChanged();
        txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);


    }

    @Override
    public void onRefresh() {

        pageNumber = 1;
        snapAdapter.clear();
        results.clear();
        mArrayList.clear();
        mFilteredList.clear();
        mGetNews();
        mGetAllResult();
        isScroll = true;
        swiperefresh.setRefreshing(false);
        loading.show();
    }

    private class SnapAdapter extends ArrayAdapter<ResultItem> {
        ViewHolder holder = null;


        public SnapAdapter(Context context, int resource, int textViewResourceId, List<ResultItem> objects) {
            super(context, resource, textViewResourceId, objects);
        }

        private class ViewHolder {
            public TextView
                    mTxtName;

            public ConstraintLayout mMainLayout;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ResultItem resultItem = getItem(position);

            convertView = getLayoutInflater().inflate(R.layout.item_snap, null);
            holder = new ViewHolder();
            convertView.setTag(holder);


            holder.mMainLayout = convertView.findViewById(R.id.mMainLayout);
            holder.mTxtName = convertView.findViewById(R.id.mTxtName);

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String newData = resultItem.getFilePath().replace("www.", "http://");

                    Intent mIntent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("image", newData.replace("\\", "/"));
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);

                }
            });

            holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newData = resultItem.getFilePath().replace("www.", "http://");

                    Intent mIntent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    Bundle mBundle = new Bundle();
                    mBundle.putString("image", newData.replace("\\", "/"));
                    mIntent.putExtras(mBundle);
                    startActivity(mIntent);
                }
            });


            holder.mTxtName.setText(resultItem.getFileName());


            return convertView;
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        super.onBackPressed();
    }


    private void mGetNews() {
        try {

            //  recyclerView.setVisibility(View.GONE);

            if (pageNumber == 1)
                mLayoutAnimation.setVisibility(View.VISIBLE);

            Map<String, String> data = new HashMap<>();
            data.put("email", Validation.getFirstString(PreferenceManager.getDefaultSharedPreferences(ListSnapActivity.this).
                    getString(getResources().getString(R.string.pref_email), "")));

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url_snap))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseSnap> call = api.mGetSnap(data);


            call.enqueue(new Callback<ResponseSnap>() {
                @Override
                public void onResponse(Call<ResponseSnap> call, Response<ResponseSnap> response) {

                    try {
                        if (pageNumber == 1)
                            mLayoutAnimation.setVisibility(View.GONE);

                        recyclerView.setVisibility(View.VISIBLE);

                        if (response.isSuccessful()) {
                            loading.dismiss();

                            String status = response.body().getStatus();

                            if (status.equals(getResources().getString(R.string.response_success))) {

                                try {
                                    results = response.body().getResult();

                                    if (results.size() > 0) {

                                        snapAdapter.addAll(results);
                                        snapAdapter.notifyDataSetChanged();
                                        recyclerView.invalidateViews();
                                        pageNumber++;
                                        txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);

                                    }

                                    snapAdapter.notifyDataSetChanged();
                                    txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);
                                } catch (Exception e) {

                                }

                            } else {

                                txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);

                            }

                        } else {
                            loading.dismiss();
                            txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);
                        }

                    } catch (Exception e) {
                        loading.dismiss();
                        txtEmptyList.setVisibility(snapAdapter.isEmpty() ? View.VISIBLE : View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ResponseSnap> call, Throwable t) {
                    loading.dismiss();
                    if (pageNumber == 1)
                        mLayoutAnimation.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    MDToast.makeText(ListSnapActivity.this, getResources().getString(R.string.loading_error),
                            MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
                   // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            if (pageNumber == 1)
                mLayoutAnimation.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            MDToast.makeText(ListSnapActivity.this, getResources().getString(R.string.loading_error),
                    MDToast.LENGTH_LONG, MDToast.TYPE_ERROR).show();
           // Snackbar.make(getWindow().getDecorView(), R.string.loading_error, Snackbar.LENGTH_LONG).show();
        }

    }


    private boolean currentVisibleItemCount;
    private boolean loadmoreProcessComplete = false;
    private int currentScrollState;


    public void onScroll(AbsListView view, int firstVisible, int visibleCount, int totalCount) {
        currentVisibleItemCount = firstVisible + visibleCount >= totalCount - 2;
    }

    public void onScrollStateChanged(AbsListView view, int scrollState) {
        currentScrollState = scrollState;
        isScrollCompleted();
    }

    private void isScrollCompleted() {
        try {
            if (loadmoreProcessComplete && currentVisibleItemCount && currentScrollState == SCROLL_STATE_IDLE && isScroll) {
                loadmoreProcessComplete = false;
                mGetNews();
            }
        } catch (Exception e) {
        }

    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.material_design_android_floating_action_menu) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateReimbursementFragment dialogFragment = new CreateReimbursementFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        } else if (v.getId() == R.id.fab) {
            android.app.FragmentManager fm = getFragmentManager();
            CreateReimbursementFragment dialogFragment = new CreateReimbursementFragment();
            Bundle bundle = new Bundle();
            dialogFragment.setArguments(bundle);
            dialogFragment.show(fm, "Image Dialog");
        }
    }


    private void mGetAllResult() {
        try {

            if (pageNumber == 1)
                mLayoutAnimation.setVisibility(View.VISIBLE);

            Map<String, String> data = new HashMap<>();
            data.put("email", Validation.getFirstString(PreferenceManager.getDefaultSharedPreferences(ListSnapActivity.this).
                    getString(getResources().getString(R.string.pref_email), "")));

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .readTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .writeTimeout(AppConstants.TIMEOUT_REQUEST, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url_snap))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseSnap> call = api.mGetSnap(data);


            call.enqueue(new Callback<ResponseSnap>() {
                @Override
                public void onResponse(Call<ResponseSnap> call, Response<ResponseSnap> response) {

                    try {
                        if (response.isSuccessful()) {

                            String status = response.body().getStatus();

                            if (status.equals(getResources().getString(R.string.response_success))) {

                                mArrayList = response.body().getResult();
                            }

                        } else {

                        }
                    } catch (Exception e) {
                    }
                }

                @Override
                public void onFailure(Call<ResponseSnap> call, Throwable t) {
                }
            });
        } catch (Exception e) {
        }

    }

    public void onUserSelectValue(String selectedValue) {

        loading.show();
        pageNumber = 1;

        results = new ArrayList<>();

        snapAdapter = new SnapAdapter(getApplicationContext(), R.layout.item_snap, R.id.mTxtName, results);
        snapAdapter.setNotifyOnChange(true);
        recyclerView.setAdapter(snapAdapter);

        snapAdapter.clear();
        mArrayList.clear();
        results.clear();
        mFilteredList.clear();

        mGetNews();
        mGetAllResult();
        isScroll = true;


        // TODO add your implementation.
        //  Toast.makeText(getBaseContext(), "search "+ selectedValue, Toast.LENGTH_LONG).show();

    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }




}


