package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableRow;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.adapter.DetailAsetAdapter;
import app.direksi.hras.fragment.ApproveAssetFragment;
import app.direksi.hras.fragment.EditKadaluarsaFragment;
import app.direksi.hras.fragment.HandoverFragment;
import app.direksi.hras.model.AssetMyAssetList;
import app.direksi.hras.model.DataDetailAsetGeneral;
import app.direksi.hras.model.DataDetailAssetsAssetTracking;
import app.direksi.hras.model.ResponseDetailAsetGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 23/05/2019.
 */

public class DetailTrackingAsetKuActivity extends AppCompatActivity {


    AssetMyAssetList dataAset;
    public TextView txtNama,
            txtLocation,
            txtCreator,
            txtDateCreate,
            txtNote,
            txtDate,
            txtKategori;
    TableRow trExpired;
    private ImageView mIconStatus;
    private ImageView mImageView;
    private RecyclerView recyclerView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private DetailAsetAdapter cAdapter;
    private List<DataDetailAssetsAssetTracking> data = new ArrayList<>();
    private ScrollView scroolview1;
    private CardView rlTracking;
    private Button prosesRequest, btnUpdate, btnKadaluarsa;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailTrackingAsetKuActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        //   TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_aset));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);



        setContentView(R.layout.activity_detail_tracking);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        dataAset = gson.fromJson(getIntent().getStringExtra("detail"), AssetMyAssetList.class);
        txtNama = findViewById(R.id.txtNama);
        txtKategori = findViewById(R.id.txtKategori);
        txtLocation = findViewById(R.id.txtLocation);
        txtCreator = findViewById(R.id.txtCreator);
        txtDateCreate = findViewById(R.id.txtDateCreate);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mImageView = findViewById(R.id.mImageView);
        rlTracking = findViewById(R.id.rlTracking);
        prosesRequest = findViewById(R.id.prosesRequest);
        btnUpdate = findViewById(R.id.btnUpdate);
        btnKadaluarsa = findViewById(R.id.btnKadaluarsa);
        prosesRequest.setVisibility(View.GONE);
        btnUpdate.setVisibility(View.GONE);
        btnKadaluarsa.setVisibility(View.GONE);
        scroolview1 = findViewById(R.id.scroolview1);
        scroolview1.setVisibility(View.GONE);
        txtNote = findViewById(R.id.txtNote);

        txtDate = (TextView) findViewById(R.id.txtDate);
        trExpired = (TableRow) findViewById(R.id.trExpired);

        String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).getString(
                getResources().getString(R.string.pref_token), ""));


        cAdapter = new DetailAsetAdapter(data, getApplicationContext());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        // recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        RecyclerView.ItemDecoration itemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);

        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider));

        //  recyclerView.addItemDecoration(itemDecorator);
        recyclerView.setAdapter(cAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {

                //kirim id campaign
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));





        messageDialog = new MessageDialog();
        loading = new SweetAlertDialog(DetailTrackingAsetKuActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


        if (dataAset != null) {
            DetailAsetGeneral(dataAset.getAssetID().intValue());
        }else {
            onNewIntent(getIntent());
        }




    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataAset = new AssetMyAssetList();
                        dataAset.setAssetID(Long.parseLong(String.valueOf(idx)));
                        DetailAsetGeneral(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
               // overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
   /* public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ApproveKlaim();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle("Konfirmasi")
                .setMessage("Apakah Anda yakin ?")
                .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  ProsesUpdate();
                    }

                })
                .setNegativeButton("Tidak", null)
                .show();


    }*/


   /* public void DetailAset() {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailAset> call = api.getDetailAset(Authorization, dataAset.getAssetID().toString());

            call.enqueue(new Callback<ResponseDetailAset>() {
                @Override
                public void onResponse(Call<ResponseDetailAset> call, Response<ResponseDetailAset> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.hide();
                            data.addAll(response.body().getData().getHistory());
                            cAdapter.notifyDataSetChanged();
                            scroolview1.setVisibility(View.VISIBLE);

                            if (data.size() > 0){

                            }else {
                                rlTracking.setVisibility(View.GONE);
                            }



                        }
                        else {
                            loading.hide();
                            try {
                                messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this, "GAGAL"
                                        , response.body().getErrMessage());
                            } catch (Exception e){

                            }
                        }

                    } else {
                        loading.hide();
                        try {
                            messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this,"GAGAL"
                                    , response.body().getErrMessage());
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailAset> call, Throwable t) {
                    loading.hide();
                    try {
                        messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this, "GAGAL"
                                , getResources().getString(R.string.loading_error));
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.hide();
            try {
                messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this, "GAGAL"
                        , getResources().getString(R.string.loading_error));
            } catch (Exception d){

            }
        }
    }*/

    public void DetailAsetGeneral(int ids) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailAsetGeneral> call = api.getDetailAsetGeneral(Authorization, String.valueOf(ids));

            call.enqueue(new Callback<ResponseDetailAsetGeneral>() {
                @Override
                public void onResponse(Call<ResponseDetailAsetGeneral> call, Response<ResponseDetailAsetGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            data.addAll(response.body().getData().getAssetTrackings());
                            if (response.body().getData().getAssetTrackings().get(0).getDestinationHolder().getEmployeeID().toString()
                                    .equals(response.body().getData().getAssetTrackings().get(0).getOriginHolder().getEmployeeID().toString())){

                                Collections.reverse(data);

                            }

                            cAdapter.notifyDataSetChanged();
                            scroolview1.setVisibility(View.VISIBLE);
                            ShowData(response.body().getData());
                            ShowHideButton(response.body().getData());
                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this, getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                            } catch (Exception e){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        } catch (Exception e){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailAsetGeneral> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    } catch (Exception e){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(DetailTrackingAsetKuActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            } catch (Exception d){

            }
        }
    }

    private void ShowHideButton ( DataDetailAsetGeneral data){


        String idEmployee = (PreferenceManager.getDefaultSharedPreferences(this).getString(
                getResources().getString(R.string.pref_employeeid), ""));

        int last = data.getAssetTrackings().size() - 1 ;



            if (data.getAssetTrackings().get(last).getIsAccepted() == true){

                if (data.getAssetTrackings().get(data.getAssetTrackings().size() - 1).getDestinationHolder().getEmployeeID().toString().equals(idEmployee)){

                    btnUpdate.setVisibility(View.VISIBLE);

                    if (data.getExpiredDate()!= null){
                        btnKadaluarsa.setVisibility(View.VISIBLE);
                    }

                }



            } else{

                if (data.getAssetTrackings().get(last).getDestinationHolder().getEmployeeID().toString().equals(idEmployee)){

                    prosesRequest.setVisibility(View.VISIBLE);

                }


            }

    }

    private void ShowData(DataDetailAsetGeneral data){

        txtNote.setText(data.getNote());
        txtNama.setText(data.getName());
        txtKategori.setText(data.getConstanta().getName());
        txtLocation.setText(data.getLastLocation());
        txtCreator.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
        if (data.getDateCreated() != null)
            txtDateCreate.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getDateCreated()));





        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);





        if (data.getFilePath() != null) {


            Glide.with(DetailTrackingAsetKuActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.getFilePath()))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.getFilePath());
                    startActivity(intent);
                }
            });
        }
        else {
            mImageView.setVisibility(View.GONE);
        }

        Date sDate, eDate;
        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat formatParse = new SimpleDateFormat("dd MMM yyyy");

        try {

            if (data.getExpiredDate()!= null) {
                sDate = parser.parse(data.getExpiredDate());
                txtDate.setText(formatParse.format(sDate));
                trExpired.setVisibility(View.VISIBLE);


            }
            else
            {
                txtDate.setText("-");
                trExpired.setVisibility(View.GONE);
            }




        } catch (ParseException e) {
            e.printStackTrace();
        }




    }

    public void prosesRequest(View v) {
        PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataAset.getAssetID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        ApproveAssetFragment dialogFragment = new ApproveAssetFragment();
        Bundle bundle = new Bundle();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");


    }

    public void terimaCuti(View v) {
        PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataAset.getAssetID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        HandoverFragment dialogFragment = new HandoverFragment();
        Bundle bundle = new Bundle();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");


    }

    public void editKadaluarsa(View v) {
        PreferenceManager.getDefaultSharedPreferences(DetailTrackingAsetKuActivity.this).
                edit().putString(getResources().getString(R.string.pref_dayoff_idProses), dataAset.getAssetID().toString())
                .apply();

        android.app.FragmentManager fm = getFragmentManager();
        EditKadaluarsaFragment dialogFragment = new EditKadaluarsaFragment();
        Bundle bundle = new Bundle();
        dialogFragment.setArguments(bundle);
        dialogFragment.show(fm, "Image Dialog");


    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailTrackingAsetKuActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}
