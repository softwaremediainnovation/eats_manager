package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import app.direksi.hras.fragment.FilterEmployeFragment;
import app.direksi.hras.model.DataEmployee;
import app.direksi.hras.util.MyContextWrapper;

public class SettingActivity extends AppCompatActivity {

    RelativeLayout rlPilihBahasa, rlUbahPassword, rlAkurasiGPS;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(SettingActivity.this);
        setContentView(R.layout.activity_list_setting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);//value 0 to remove line

        //change transition open and close layout
        //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.settings));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        rlPilihBahasa = (RelativeLayout) findViewById(R.id.rlPilihBahasa);
        rlPilihBahasa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SettingActivity.this, PilihBahasaActivity.class);
                startActivity(intent);
            }
        });
        rlUbahPassword = (RelativeLayout) findViewById(R.id.rlUbahPassword);
        rlUbahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(SettingActivity.this, ChangePasswordActivity.class);
                startActivity(intent);
            }
        });
        rlAkurasiGPS = (RelativeLayout) findViewById(R.id.rlAkurasiGPS);
        rlAkurasiGPS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(viewIntent);
                    
                } catch (Exception e){

                }
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;


        }
        return super.onOptionsItemSelected(item);
    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }

        super.attachBaseContext(MyContextWrapper.wrap(newBase, bahasa));
    }

    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(SettingActivity.this);


    }


}
