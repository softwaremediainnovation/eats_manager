
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataCounter {

    @SerializedName("assets")
    private Integer mAssets;
    @SerializedName("dayoffs")
    private Integer mDayoffs;
    @SerializedName("loans")
    private Integer mLoans;
    @SerializedName("overtimes")
    private Integer mOvertimes;
    @SerializedName("reimbursements")
    private Integer mReimbursements;
    @SerializedName("kontrak")
    private Integer kontrak;
    @SerializedName("attendances")
    private Integer requestAttendance;
    @SerializedName("taskheader")
    private Integer taskheader;

    public Integer getTaskheader() {
        return taskheader;
    }

    public void setTaskheader(Integer assets) {
        taskheader = assets;
    }

    public Integer getAssets() {
        return mAssets;
    }

    public void setAssets(Integer assets) {
        mAssets = assets;
    }

    public Integer getDayoffs() {
        return mDayoffs;
    }

    public void setDayoffs(Integer dayoffs) {
        mDayoffs = dayoffs;
    }

    public Integer getLoans() {
        return mLoans;
    }

    public void setLoans(Integer loans) {
        mLoans = loans;
    }

    public Integer getOvertimes() {
        return mOvertimes;
    }

    public void setOvertimes(Integer overtimes) {
        mOvertimes = overtimes;
    }

    public Integer getReimbursements() {
        return mReimbursements;
    }

    public void setReimbursements(Integer reimbursements) {
        mReimbursements = reimbursements;
    }

    public Integer getKontrak() {
        return kontrak;
    }

    public void setKontrak(Integer reimbursements) {
        kontrak = reimbursements;
    }

    public Integer getRequestAttendance() {
        return requestAttendance;
    }

    public void setRequestAttendance(Integer reimbursements) {
        requestAttendance = reimbursements;
    }

}
