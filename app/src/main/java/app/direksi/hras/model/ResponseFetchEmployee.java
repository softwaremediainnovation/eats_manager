package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseFetchEmployee{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	@SerializedName("data")
	@Expose
	private List<DataFetchEmployeeItem> dataFetchEmployee;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	public void setDataFetchEmployee(List<DataFetchEmployeeItem> dataFetchEmployee){
		this.dataFetchEmployee = dataFetchEmployee;
	}

	public List<DataFetchEmployeeItem> getDataFetchEmployee(){
		return dataFetchEmployee;
	}

	@Override
 	public String toString(){
		return 
			"ResponseFetchEmployee{" + 
			"errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			",dataFetchEmployee = '" + dataFetchEmployee + '\'' + 
			"}";
		}
}