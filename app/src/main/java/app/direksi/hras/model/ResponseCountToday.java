package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseCountToday{

	@SerializedName("data")
	private int data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public int getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}