package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseReimbursCategory{

	@SerializedName("data")
	@Expose
	private List<DataReimbursCategoryItem> dataReimbursCategory;

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setDataReimbursCategory(List<DataReimbursCategoryItem> dataReimbursCategory){
		this.dataReimbursCategory = dataReimbursCategory;
	}

	public List<DataReimbursCategoryItem> getDataReimbursCategory(){
		return dataReimbursCategory;
	}

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseReimbursCategory{" + 
			"dataReimbursCategory = '" + dataReimbursCategory + '\'' + 
			",errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}