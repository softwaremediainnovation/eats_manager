
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Pengguna {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("id")
    private String mId;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("nik")
    private String mNik;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("role")
    private String mRole;
    @SerializedName("signinToMobile")
    private Boolean mSigninToMobile;
    @SerializedName("permissions")
    private Permissions permissionss;
    @SerializedName("isLoginAllowed")
    private Boolean mIsLoginAllowed;
    @SerializedName("organization")
    private DataLoginOrganizations mOrganization;
    @SerializedName("contractExpiredDate")
    private String mContractExpiredDate;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getNik() {
        return mNik;
    }

    public void setNik(String nik) {
        mNik = nik;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public String getRole() {
        return mRole;
    }

    public void setRole(String role) {
        mRole = role;
    }

    public Boolean getSigninToMobile() {
        return mSigninToMobile;
    }

    public void setSigninToMobile(Boolean signinToMobile) {
        mSigninToMobile = signinToMobile;
    }

    public Permissions getPermission() {
        return permissionss;
    }

    public void setPermission(Permissions permission) {
        permissionss = permission;
    }

    public DataLoginOrganizations getOrganization() {
        return mOrganization;
    }

    public void setOrganization(DataLoginOrganizations organization) {
        mOrganization = organization;
    }

    public Boolean getIsLoginAllowed() {
        return mIsLoginAllowed;
    }

    public void setIsLoginAllowed(Boolean isLoginAllowed) {
        mIsLoginAllowed = isLoginAllowed;
    }

    public String getContractExpiredDate() {
        return mContractExpiredDate;
    }

    public void setContractExpiredDate(String contractExpiredDate) {
        mContractExpiredDate = contractExpiredDate;
    }

}
