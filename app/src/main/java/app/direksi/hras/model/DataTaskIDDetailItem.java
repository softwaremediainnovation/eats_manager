package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataTaskIDDetailItem {

	@SerializedName("note")
	private String note;

	@SerializedName("address")
	private String address;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("photo")
	private String photo;

	@SerializedName("isFinish")
	private boolean isFinish;

	@SerializedName("dateFinish")
	private String dateFinish;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("updatedByID")
	private int updatedByID;

	@SerializedName("taskDetailID")
	private int taskDetailID;

	@SerializedName("dateUpdated")
	private String dateUpdated;

	@SerializedName("taskHeaderID")
	private int taskHeaderID;

	@SerializedName("taskName")
	private String taskName;

	@SerializedName("longitude")
	private String longitude;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setIsFinish(boolean isFinish){
		this.isFinish = isFinish;
	}

	public boolean isIsFinish(){
		return isFinish;
	}

	public void setDateFinish(String dateFinish){
		this.dateFinish = dateFinish;
	}

	public String getDateFinish(){
		return dateFinish;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUpdatedByID(int updatedByID){
		this.updatedByID = updatedByID;
	}

	public int getUpdatedByID(){
		return updatedByID;
	}

	public void setTaskDetailID(int taskDetailID){
		this.taskDetailID = taskDetailID;
	}

	public int getTaskDetailID(){
		return taskDetailID;
	}

	public void setDateUpdated(String dateUpdated){
		this.dateUpdated = dateUpdated;
	}

	public String getDateUpdated(){
		return dateUpdated;
	}

	public void setTaskHeaderID(int taskHeaderID){
		this.taskHeaderID = taskHeaderID;
	}

	public int getTaskHeaderID(){
		return taskHeaderID;
	}

	public void setTaskName(String taskName){
		this.taskName = taskName;
	}

	public String getTaskName(){
		return taskName;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return 
			"DetailItem{" + 
			"note = '" + note + '\'' + 
			",address = '" + address + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",photo = '" + photo + '\'' + 
			",isFinish = '" + isFinish + '\'' + 
			",dateFinish = '" + dateFinish + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",updatedByID = '" + updatedByID + '\'' + 
			",taskDetailID = '" + taskDetailID + '\'' + 
			",dateUpdated = '" + dateUpdated + '\'' + 
			",taskHeaderID = '" + taskHeaderID + '\'' + 
			",taskName = '" + taskName + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}