package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataDetailRequestAttendance {

	@SerializedName("dateTime")
	private String dateTime;

	@SerializedName("approver")
	private String approver;

	@SerializedName("note")
	private String note;

	@SerializedName("approverID")
	private String approverID;

	@SerializedName("isMobileApp")
	private boolean isMobileApp;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("employee")
	private DataDetailRequestAttedanceEmployee employee;

	@SerializedName("attendanceID")
	private int attendanceID;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("lateTimeInMinute")
	private Double lateTimeInMinute;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("inOut")
	private String inOut;

	@SerializedName("nama")
	private String nama;

	@SerializedName("isLate")
	private boolean isLate;

	@SerializedName("organization")
	private String organization;

	@SerializedName("isApproved")
	private String isApproved;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("dateApproved")
	private String mDateApproved;

	@SerializedName("comment")
	private String mComment;

	@SerializedName("isYesterday")
	private Boolean isYesterday;

	@SerializedName("address")
	private String address;

	@SerializedName("dateGMT")
	private int dateGMT;

	@SerializedName("file")
	private String file;

	@SerializedName("isPhotoApproved")
	private Boolean isPhotoApproved;

	@SerializedName("datePhotoRejected")
	private String datePhotoRejected;

	@SerializedName("rejecter")
	private String rejecter;

	@SerializedName("commentPhoto")
	private String commentPhoto;

	@SerializedName("dateApprovedGMT")
	private int dateApprovedGMT;


	public String getCommentPhoto() {
		return commentPhoto;
	}

	public void setCommentPhoto(String address) {
		commentPhoto = address;
	}

	public String getRejecter() {
		return rejecter;
	}

	public void setRejecter(String address) {
		rejecter = address;
	}

	public String getDatePhotoRejected() {
		return datePhotoRejected;
	}

	public void setDatePhotoRejected(String address) {
		datePhotoRejected = address;
	}

	public Boolean getIsPhotoApproved() {
		return isPhotoApproved;
	}

	public void setIsPhotoApproved(Boolean comment) {
		isPhotoApproved = comment;
	}



	public String getFile() {
		return file;
	}

	public void setFile(String address) {
		file = address;
	}

	public void setDateTime(String dateTime){
		this.dateTime = dateTime;
	}

	public String getDateTime(){
		return dateTime;
	}

	public void setApprover(String approver){
		this.approver = approver;
	}

	public String getApprover(){
		return approver;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setApproverID(String approverID){
		this.approverID = approverID;
	}

	public String getApproverID(){
		return approverID;
	}

	public void setIsMobileApp(boolean isMobileApp){
		this.isMobileApp = isMobileApp;
	}

	public boolean isIsMobileApp(){
		return isMobileApp;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setEmployee(DataDetailRequestAttedanceEmployee employee){
		this.employee = employee;
	}

	public DataDetailRequestAttedanceEmployee getEmployee(){
		return employee;
	}

	public void setAttendanceID(int attendanceID){
		this.attendanceID = attendanceID;
	}

	public int getAttendanceID(){
		return attendanceID;
	}

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setLateTimeInMinute(Double lateTimeInMinute){
		this.lateTimeInMinute = lateTimeInMinute;
	}

	public Double getLateTimeInMinute(){
		return lateTimeInMinute;
	}

	public void setDateCreated(String dateCreated){
		this.dateCreated = dateCreated;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public void setInOut(String inOut){
		this.inOut = inOut;
	}

	public String getInOut(){
		return inOut;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setIsLate(boolean isLate){
		this.isLate = isLate;
	}

	public boolean isIsLate(){
		return isLate;
	}

	public void setOrganization(String organization){
		this.organization = organization;
	}

	public String getOrganization(){
		return organization;
	}

	public void setIsApproved(String isApproved){
		this.isApproved = isApproved;
	}

	public String isIsApproved(){
		return isApproved;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public String getDateApproved() {
		return mDateApproved;
	}

	public void setDateApproved(String dateApproved) {
		mDateApproved = dateApproved;
	}

	public String getComment() {
		return mComment;
	}

	public void setComment(String comment) {
		mComment = comment;
	}

	public Boolean getIsYesterday() {
		return isYesterday;
	}

	public void setIsYesterday(Boolean comment) {
		isYesterday = comment;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		address = address;
	}

	public int getDateGMT() {
		return dateGMT;
	}

	public void setDateGMT(int dateGMT) {
		dateGMT = dateGMT;
	}

	public int getDateApprovedGMT() {
		return dateApprovedGMT;
	}

	public void setDateApprovedGMT(int dateApprovedGMT) {
		dateApprovedGMT = dateApprovedGMT;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"dateTime = '" + dateTime + '\'' + 
			",approver = '" + approver + '\'' + 
			",note = '" + note + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",isMobileApp = '" + isMobileApp + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",employee = '" + employee + '\'' + 
			",attendanceID = '" + attendanceID + '\'' + 
			",organizationID = '" + organizationID + '\'' + 
			",lateTimeInMinute = '" + lateTimeInMinute + '\'' + 
			",dateCreated = '" + dateCreated + '\'' + 
			",inOut = '" + inOut + '\'' + 
			",nama = '" + nama + '\'' + 
			",isLate = '" + isLate + '\'' + 
			",organization = '" + organization + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}