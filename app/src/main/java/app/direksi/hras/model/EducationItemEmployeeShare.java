package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class EducationItemEmployeeShare {

	@SerializedName("c")
	private CEmployeeShare C;

	@SerializedName("m")
	private MEmployeeShare M;

	public void setC(CEmployeeShare C){
		this.C = C;
	}

	public CEmployeeShare getC(){
		return C;
	}

	public void setM(MEmployeeShare M){
		this.M = M;
	}

	public MEmployeeShare getM(){
		return M;
	}

	@Override
	public String toString(){
		return
				"EducationItem{" +
						"c = '" + C + '\'' +
						",m = '" + M + '\'' +
						"}";
	}
}