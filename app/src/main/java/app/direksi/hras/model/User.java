package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class User{

	public User(String password, String email, String deviceId, String serial) {
		this.password = password;
		this.email = email;
		this.deviceId = deviceId;
		this.serialNumber = serial;
	}

	@SerializedName("password")
	@Expose
	private String password;

	@SerializedName("email")
	@Expose
	private String email;

	@SerializedName("deviceId")
	@Expose
	private String deviceId;

	@SerializedName("serialNumber")
	@Expose
	private String serialNumber;

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setDeviceId(String email){
		this.deviceId = email;
	}

	public String getDeviceId(){
		return deviceId;
	}

	@Override
 	public String toString(){
		return 
			"User{" + 
			"password = '" + password + '\'' + 
			",email = '" + email + '\'' +
					",deviceId = '" + deviceId + '\'' +
					"}";
		}
}