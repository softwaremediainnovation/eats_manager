package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FormPenilaian {


	public FormPenilaian( int disiplinKerja, int minatTerhadapPekerjaan, int caraKerja, int tempoKerja, int tanggapTerhadapTugasPekerjaanA, int kesimpulanA,
						  int dasarPemikiran, int inisiatif, int kepemimpinan, int pertanggunganJawabAdministrasi, int tanggapTerhadapTugasPekerjaanB, int kesimpulanB, int kesimpulanHasilKondite,
						  int pengabdian, int kejujuran, int rasaTanggungJawab, int keinginanUntukMaju, int penguasaanEmosi, int penampilanDiri, int komunikasi, int kesimpulanHasilKepribadian,
						  int loyalitasDanRasaMemiliki, String hubunganKeluargaDenganPenilai, String penghargaanYangDiberikan, String peringatanYangDiberikan,
						  String kenaikanGajiPokok, String kenaikanJabatan, String kategoriKenaikanGajiSesuaiGrade, String catatanPenilai, String keterangan,
						  int employeeID, int evaluatorID,
						  String perincianAbsensiM,String perincianAbsensiSI,String perincianAbsensiSK,String perincianAbsensiSD,String perincianAbsensiDis,String perincianAbsensiCH,String perincianAbsensiCT,
						  String start, String end,  Integer gaji) {


		this.disiplinKerja = disiplinKerja;
		this.minatTerhadapPekerjaan = minatTerhadapPekerjaan;
		this.caraKerja = caraKerja;
		this.tempoKerja = tempoKerja;
		this.tanggapTerhadapTugasPekerjaanA = tanggapTerhadapTugasPekerjaanA;
		this.kesimpulanA = kesimpulanA;
		this.dasarPemikiran = dasarPemikiran;
		this.inisiatif = inisiatif;
		this.kepemimpinan = kepemimpinan;
		this.pertanggunganJawabAdministrasi = pertanggunganJawabAdministrasi;
		this.tanggapTerhadapTugasPekerjaanB = tanggapTerhadapTugasPekerjaanB;
		this.kesimpulanB = kesimpulanB;
		this.kesimpulanHasilKondite = kesimpulanHasilKondite;

		this.pengabdian = pengabdian;
		this.kejujuran = kejujuran;
		this.rasaTanggungJawab = rasaTanggungJawab;
		this.keinginanUntukMaju = keinginanUntukMaju;
		this.penguasaanEmosi = penguasaanEmosi;
		this.penampilanDiri = penampilanDiri;
		this.komunikasi = komunikasi;
		this.kesimpulanHasilKepribadian = kesimpulanHasilKepribadian;

		this.loyalitasDanRasaMemiliki = loyalitasDanRasaMemiliki;

		this.mulaiBekerja = start;
		this.kontrakHabis = end;
		this.gajiSekarang = gaji;

		this.perincianAbsensiM = perincianAbsensiM;
		this.perincianAbsensiSI = perincianAbsensiSI;
		this.perincianAbsensiSK = perincianAbsensiSK;
		this.perincianAbsensiSD = perincianAbsensiSD;
		this.perincianAbsensiDis = perincianAbsensiDis;
		this.perincianAbsensiCH = perincianAbsensiCH;
		this.perincianAbsensiCT = perincianAbsensiCT;


		this.hubunganKeluargaDenganPenilai = hubunganKeluargaDenganPenilai;
		this.penghargaanYangDiberikan = penghargaanYangDiberikan;
		this.peringatanYangDiberikan = peringatanYangDiberikan;

		this.kenaikanGajiPokok = kenaikanGajiPokok;
		this.kenaikanJabatan = kenaikanJabatan;
		this.kategoriKenaikanGajiSesuaiGrade = kategoriKenaikanGajiSesuaiGrade;
		this.catatanPenilai = catatanPenilai;
		this.keterangan = keterangan;

		this.employeeID = employeeID;
		this.evaluatorID = evaluatorID;


	}


	@SerializedName("kpI3Grade")
	private String kpI3Grade;

	@SerializedName("tanggapTerhadapTugasPekerjaanB")
	private int tanggapTerhadapTugasPekerjaanB;

	@SerializedName("kpI1FokusKerja")
	private String kpI1FokusKerja;

	@SerializedName("tanggapTerhadapTugasPekerjaanA")
	private int tanggapTerhadapTugasPekerjaanA;

	@SerializedName("kpI4Desc")
	private String kpI4Desc;

	@SerializedName("kenaikanJabatan")
	private String kenaikanJabatan;

	@SerializedName("hubunganKeluargaDenganPenilai")
	private String hubunganKeluargaDenganPenilai;

	@SerializedName("keinginanUntukMaju")
	private int keinginanUntukMaju;

	@SerializedName("kpI5FokusKerja")
	private String kpI5FokusKerja;

	@SerializedName("kpI5Point")
	private int kpI5Point;

	@SerializedName("evaluationID")
	private int evaluationID;

	@SerializedName("kpI1Bobot")
	private int kpI1Bobot;

	@SerializedName("kpI2Desc")
	private String kpI2Desc;

	@SerializedName("kejujuran")
	private int kejujuran;

	@SerializedName("kpI5Desc")
	private String kpI5Desc;

	@SerializedName("kpI4FokusKerja")
	private String kpI4FokusKerja;

	@SerializedName("penghargaanYangDiberikan")
	private String penghargaanYangDiberikan;

	@SerializedName("kpI4Grade")
	private String kpI4Grade;

	@SerializedName("pericianAbsensi")
	private String pericianAbsensi;

	@SerializedName("kpI5Total")
	private int kpI5Total;

	@SerializedName("kpI1Point")
	private int kpI1Point;

	@SerializedName("pengabdian")
	private int pengabdian;

	@SerializedName("kesimpulanHasilKondite")
	private int kesimpulanHasilKondite;

	@SerializedName("kpI4Total")
	private int kpI4Total;

	@SerializedName("kpI1Total")
	private int kpI1Total;

	@SerializedName("kpI2Bobot")
	private int kpI2Bobot;

	@SerializedName("kpI5Bobot")
	private int kpI5Bobot;

	@SerializedName("kpI2FokusKerja")
	private String kpI2FokusKerja;

	@SerializedName("caraKerja")
	private int caraKerja;

	@SerializedName("penampilanDiri")
	private int penampilanDiri;

	@SerializedName("kpI1Grade")
	private String kpI1Grade;

	@SerializedName("kategoriKenaikanGajiSesuaiGrade")
	private String kategoriKenaikanGajiSesuaiGrade;

	@SerializedName("tempoKerja")
	private int tempoKerja;

	@SerializedName("kpI2Point")
	private int kpI2Point;

	@SerializedName("totalPoint")
	private int totalPoint;

	@SerializedName("kesimpulanA")
	private int kesimpulanA;

	@SerializedName("kpI3Desc")
	private String kpI3Desc;

	@SerializedName("inisiatif")
	private int inisiatif;

	@SerializedName("kesimpulanB")
	private int kesimpulanB;

	@SerializedName("kesimpulanHasilKepribadian")
	private int kesimpulanHasilKepribadian;

	@SerializedName("dasarPemikiran")
	private int dasarPemikiran;

	@SerializedName("loyalitasDanRasaMemiliki")
	private int loyalitasDanRasaMemiliki;

	@SerializedName("keterangan")
	private String keterangan;

	@SerializedName("bobot")
	private int bobot;

	@SerializedName("kpI3Point")
	private int kpI3Point;

	@SerializedName("kpI3Bobot")
	private int kpI3Bobot;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("kpI3Total")
	private int kpI3Total;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("komunikasi")
	private int komunikasi;

	@SerializedName("kpI4Bobot")
	private int kpI4Bobot;

	@SerializedName("kpI4Point")
	private int kpI4Point;

	@SerializedName("evaluatorID")
	private int evaluatorID;

	@SerializedName("catatanPenilai")
	private String catatanPenilai;

	@SerializedName("kpI2Total")
	private int kpI2Total;

	@SerializedName("peringatanYangDiberikan")
	private String peringatanYangDiberikan;

	@SerializedName("kpI1Desc")
	private String kpI1Desc;

	@SerializedName("kepemimpinan")
	private int kepemimpinan;

	@SerializedName("kenaikanGajiPokok")
	private String kenaikanGajiPokok;

	@SerializedName("catatan")
	private String catatan;

	@SerializedName("rasaTanggungJawab")
	private int rasaTanggungJawab;

	@SerializedName("pertanggunganJawabAdministrasi")
	private int pertanggunganJawabAdministrasi;

	@SerializedName("kpI5Grade")
	private String kpI5Grade;

	@SerializedName("penguasaanEmosi")
	private int penguasaanEmosi;

	@SerializedName("disiplinKerja")
	private int disiplinKerja;

	@SerializedName("minatTerhadapPekerjaan")
	private int minatTerhadapPekerjaan;

	@SerializedName("kpI2Grade")
	private String kpI2Grade;

	@SerializedName("kpI3FokusKerja")
	private String kpI3FokusKerja;

	@SerializedName("mengetahui2")
	private String mengetahui2;

	@SerializedName("mengetahui1")
	private String mengetahui1;

	@SerializedName("mulaiBekerja")
	private String mulaiBekerja;

	@SerializedName("kontrakHabis")
	private String kontrakHabis;

	@SerializedName("gajiSekarang")
	private Integer gajiSekarang;

	@SerializedName("perincianAbsensiCT")
	private String perincianAbsensiCT;

	@SerializedName("perincianAbsensiCH")
	private String perincianAbsensiCH;

	@SerializedName("perincianAbsensiDis")
	private String perincianAbsensiDis;

	@SerializedName("perincianAbsensiSD")
	private String perincianAbsensiSD;

	@SerializedName("perincianAbsensiSK")
	private String perincianAbsensiSK;

	@SerializedName("perincianAbsensiSI")
	private String perincianAbsensiSI;

	@SerializedName("perincianAbsensiM")
	private String perincianAbsensiM;


	public String getPerincianAbsensiCT(){
		return perincianAbsensiCT;
	}
	public String getPerincianAbsensiCH(){
		return perincianAbsensiCH;
	}
	public String getPerincianAbsensiDis(){
		return perincianAbsensiDis;
	}
	public String getPerincianAbsensiSD(){
		return perincianAbsensiSD;
	}
	public String getPerincianAbsensiSK(){
		return perincianAbsensiSK;
	}
	public String getPerincianAbsensiSI(){
		return perincianAbsensiSI;
	}
	public String getPerincianAbsensiM(){
		return perincianAbsensiM;
	}

	public String getMulaiBekerja(){
		return mulaiBekerja;
	}
	public String getKontrakHabis(){
		return kontrakHabis;
	}
	public Integer getGajiSekarang(){
		return gajiSekarang;
	}

	public String getKpI3Grade(){
		return kpI3Grade;
	}

	public int getTanggapTerhadapTugasPekerjaanB(){
		return tanggapTerhadapTugasPekerjaanB;
	}

	public String getKpI1FokusKerja(){
		return kpI1FokusKerja;
	}

	public int getTanggapTerhadapTugasPekerjaanA(){
		return tanggapTerhadapTugasPekerjaanA;
	}

	public String getKpI4Desc(){
		return kpI4Desc;
	}

	public String getKenaikanJabatan(){
		return kenaikanJabatan;
	}

	public String getHubunganKeluargaDenganPenilai(){
		return hubunganKeluargaDenganPenilai;
	}

	public int getKeinginanUntukMaju(){
		return keinginanUntukMaju;
	}

	public String getKpI5FokusKerja(){
		return kpI5FokusKerja;
	}

	public int getKpI5Point(){
		return kpI5Point;
	}

	public int getEvaluationID(){
		return evaluationID;
	}

	public int getKpI1Bobot(){
		return kpI1Bobot;
	}

	public String getKpI2Desc(){
		return kpI2Desc;
	}

	public int getKejujuran(){
		return kejujuran;
	}

	public String getKpI5Desc(){
		return kpI5Desc;
	}

	public String getKpI4FokusKerja(){
		return kpI4FokusKerja;
	}

	public String getPenghargaanYangDiberikan(){
		return penghargaanYangDiberikan;
	}

	public String getKpI4Grade(){
		return kpI4Grade;
	}

	public String getPericianAbsensi(){
		return pericianAbsensi;
	}

	public int getKpI5Total(){
		return kpI5Total;
	}

	public int getKpI1Point(){
		return kpI1Point;
	}

	public int getPengabdian(){
		return pengabdian;
	}

	public int getKesimpulanHasilKondite(){
		return kesimpulanHasilKondite;
	}

	public int getKpI4Total(){
		return kpI4Total;
	}

	public int getKpI1Total(){
		return kpI1Total;
	}

	public int getKpI2Bobot(){
		return kpI2Bobot;
	}

	public int getKpI5Bobot(){
		return kpI5Bobot;
	}

	public String getKpI2FokusKerja(){
		return kpI2FokusKerja;
	}

	public int getCaraKerja(){
		return caraKerja;
	}

	public int getPenampilanDiri(){
		return penampilanDiri;
	}

	public String getKpI1Grade(){
		return kpI1Grade;
	}

	public String getKategoriKenaikanGajiSesuaiGrade(){
		return kategoriKenaikanGajiSesuaiGrade;
	}

	public int getTempoKerja(){
		return tempoKerja;
	}

	public int getKpI2Point(){
		return kpI2Point;
	}

	public int getTotalPoint(){
		return totalPoint;
	}

	public int getKesimpulanA(){
		return kesimpulanA;
	}

	public String getKpI3Desc(){
		return kpI3Desc;
	}

	public int getInisiatif(){
		return inisiatif;
	}

	public int getKesimpulanB(){
		return kesimpulanB;
	}

	public int getKesimpulanHasilKepribadian(){
		return kesimpulanHasilKepribadian;
	}

	public int getDasarPemikiran(){
		return dasarPemikiran;
	}

	public int getLoyalitasDanRasaMemiliki(){
		return loyalitasDanRasaMemiliki;
	}

	public String getKeterangan(){
		return keterangan;
	}

	public int getBobot(){
		return bobot;
	}

	public int getKpI3Point(){
		return kpI3Point;
	}

	public int getKpI3Bobot(){
		return kpI3Bobot;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public int getKpI3Total(){
		return kpI3Total;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public int getKomunikasi(){
		return komunikasi;
	}

	public int getKpI4Bobot(){
		return kpI4Bobot;
	}

	public int getKpI4Point(){
		return kpI4Point;
	}

	public int getEvaluatorID(){
		return evaluatorID;
	}

	public String getCatatanPenilai(){
		return catatanPenilai;
	}

	public int getKpI2Total(){
		return kpI2Total;
	}

	public String getPeringatanYangDiberikan(){
		return peringatanYangDiberikan;
	}

	public String getKpI1Desc(){
		return kpI1Desc;
	}

	public int getKepemimpinan(){
		return kepemimpinan;
	}

	public String getKenaikanGajiPokok(){
		return kenaikanGajiPokok;
	}

	public String getCatatan(){
		return catatan;
	}

	public int getRasaTanggungJawab(){
		return rasaTanggungJawab;
	}

	public int getPertanggunganJawabAdministrasi(){
		return pertanggunganJawabAdministrasi;
	}

	public String getKpI5Grade(){
		return kpI5Grade;
	}

	public int getPenguasaanEmosi(){
		return penguasaanEmosi;
	}

	public int getDisiplinKerja(){
		return disiplinKerja;
	}

	public int getMinatTerhadapPekerjaan(){
		return minatTerhadapPekerjaan;
	}

	public String getKpI2Grade(){
		return kpI2Grade;
	}

	public String getKpI3FokusKerja(){
		return kpI3FokusKerja;
	}

	public String getMengetahui2(){
		return mengetahui2;
	}

	public String getMengetahui1(){
		return mengetahui1;
	}
}