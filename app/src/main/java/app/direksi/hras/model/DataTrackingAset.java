
package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataTrackingAset {

    @SerializedName("assetID")
    private Long mAssetID;
    @SerializedName("assetTrackings")
    private List<TrackingAsset> mAssetTrackings;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("employee")
    private Object mEmployee;
    @SerializedName("filePath")
    private Object mFilePath;
    @SerializedName("lastLocation")
    private String mLastLocation;
    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("expiredDate")
    private String mExpiredDate;
    @SerializedName("pending")
    private Boolean mPending;
    @SerializedName("constanta")
    private DataConstantaAsset constanta;

    public Long getAssetID() {
        return mAssetID;
    }

    public void setAssetID(Long assetID) {
        mAssetID = assetID;
    }

    public List<TrackingAsset> getAssetTrackings() {
        return mAssetTrackings;
    }

    public void setAssetTrackings(List<TrackingAsset> assetTrackings) {
        mAssetTrackings = assetTrackings;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public Object getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Object employee) {
        mEmployee = employee;
    }

    public Object getFilePath() {
        return mFilePath;
    }

    public void setFilePath(Object filePath) {
        mFilePath = filePath;
    }

    public String getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(String lastLocation) {
        mLastLocation = lastLocation;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getExpiredDate() {
        return mExpiredDate;
    }

    public void setExpiredDate(String note) {
        mExpiredDate = note;
    }

    public Boolean getPending() {
        return mPending;
    }

    public void setPending(Boolean note) {
        mPending = note;
    }

    public DataConstantaAsset getConstanta() {
        return constanta;
    }

    public void setConstanta(DataConstantaAsset note) {
        constanta = note;
    }

}
