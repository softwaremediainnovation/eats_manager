package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataTaskIDHeaderSchedule {

	@SerializedName("note")
	private String note;

	@SerializedName("taskHeaderScheduleID")
	private int taskHeaderScheduleID;

	@SerializedName("senin")
	private boolean senin;

	@SerializedName("creatorID")
	private int creatorID;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("selasa")
	private boolean selasa;

	@SerializedName("dateEnd")
	private String dateEnd;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("updatedByID")
	private int updatedByID;

	@SerializedName("dateUpdated")
	private String dateUpdated;

	@SerializedName("kamis")
	private boolean kamis;

	@SerializedName("taskType")
	private int taskType;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("dateStart")
	private String dateStart;

	@SerializedName("rabu")
	private boolean rabu;

	@SerializedName("sabtu")
	private boolean sabtu;

	@SerializedName("minggu")
	private boolean minggu;

	@SerializedName("jumat")
	private boolean jumat;

	@SerializedName("taskDetailSchedules")
	private Object taskDetailSchedules;

	@SerializedName("taskName")
	private String taskName;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setTaskHeaderScheduleID(int taskHeaderScheduleID){
		this.taskHeaderScheduleID = taskHeaderScheduleID;
	}

	public int getTaskHeaderScheduleID(){
		return taskHeaderScheduleID;
	}

	public void setSenin(boolean senin){
		this.senin = senin;
	}

	public boolean isSenin(){
		return senin;
	}

	public void setCreatorID(int creatorID){
		this.creatorID = creatorID;
	}

	public int getCreatorID(){
		return creatorID;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setSelasa(boolean selasa){
		this.selasa = selasa;
	}

	public boolean isSelasa(){
		return selasa;
	}

	public void setDateEnd(String dateEnd){
		this.dateEnd = dateEnd;
	}

	public String getDateEnd(){
		return dateEnd;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUpdatedByID(int updatedByID){
		this.updatedByID = updatedByID;
	}

	public int getUpdatedByID(){
		return updatedByID;
	}

	public void setDateUpdated(String dateUpdated){
		this.dateUpdated = dateUpdated;
	}

	public String getDateUpdated(){
		return dateUpdated;
	}

	public void setKamis(boolean kamis){
		this.kamis = kamis;
	}

	public boolean isKamis(){
		return kamis;
	}

	public void setTaskType(int taskType){
		this.taskType = taskType;
	}

	public int getTaskType(){
		return taskType;
	}

	public void setDateCreated(String dateCreated){
		this.dateCreated = dateCreated;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public void setDateStart(String dateStart){
		this.dateStart = dateStart;
	}

	public String getDateStart(){
		return dateStart;
	}

	public void setRabu(boolean rabu){
		this.rabu = rabu;
	}

	public boolean isRabu(){
		return rabu;
	}

	public void setSabtu(boolean sabtu){
		this.sabtu = sabtu;
	}

	public boolean isSabtu(){
		return sabtu;
	}

	public void setMinggu(boolean minggu){
		this.minggu = minggu;
	}

	public boolean isMinggu(){
		return minggu;
	}

	public void setJumat(boolean jumat){
		this.jumat = jumat;
	}

	public boolean isJumat(){
		return jumat;
	}

	public void setTaskDetailSchedules(Object taskDetailSchedules){
		this.taskDetailSchedules = taskDetailSchedules;
	}

	public Object getTaskDetailSchedules(){
		return taskDetailSchedules;
	}

	public void setTaskName(String taskName){
		this.taskName = taskName;
	}

	public String getTaskName(){
		return taskName;
	}

	@Override
 	public String toString(){
		return 
			"Header{" + 
			"note = '" + note + '\'' + 
			",taskHeaderScheduleID = '" + taskHeaderScheduleID + '\'' + 
			",senin = '" + senin + '\'' + 
			",creatorID = '" + creatorID + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",selasa = '" + selasa + '\'' + 
			",dateEnd = '" + dateEnd + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",updatedByID = '" + updatedByID + '\'' + 
			",dateUpdated = '" + dateUpdated + '\'' + 
			",kamis = '" + kamis + '\'' + 
			",taskType = '" + taskType + '\'' + 
			",dateCreated = '" + dateCreated + '\'' + 
			",dateStart = '" + dateStart + '\'' + 
			",rabu = '" + rabu + '\'' + 
			",sabtu = '" + sabtu + '\'' + 
			",minggu = '" + minggu + '\'' + 
			",jumat = '" + jumat + '\'' + 
			",taskDetailSchedules = '" + taskDetailSchedules + '\'' + 
			",taskName = '" + taskName + '\'' + 
			"}";
		}
}