package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class EmploymentItemEmployeeShare {

	@SerializedName("dateStart")
	private String dateStart;

	@SerializedName("employeeEmploymentID")
	private int employeeEmploymentID;

	@SerializedName("contactNumber")
	private String contactNumber;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("company")
	private String company;

	@SerializedName("jobDescription")
	private String jobDescription;

	@SerializedName("dateEnd")
	private String dateEnd;

	@SerializedName("position")
	private String position;

	@SerializedName("headBoss")
	private String headBoss;

	@SerializedName("employee")
	private Object employee;

	public void setDateStart(String dateStart){
		this.dateStart = dateStart;
	}

	public String getDateStart(){
		return dateStart;
	}

	public void setEmployeeEmploymentID(int employeeEmploymentID){
		this.employeeEmploymentID = employeeEmploymentID;
	}

	public int getEmployeeEmploymentID(){
		return employeeEmploymentID;
	}

	public void setContactNumber(String contactNumber){
		this.contactNumber = contactNumber;
	}

	public String getContactNumber(){
		return contactNumber;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setCompany(String company){
		this.company = company;
	}

	public String getCompany(){
		return company;
	}

	public void setJobDescription(String jobDescription){
		this.jobDescription = jobDescription;
	}

	public String getJobDescription(){
		return jobDescription;
	}

	public void setDateEnd(String dateEnd){
		this.dateEnd = dateEnd;
	}

	public String getDateEnd(){
		return dateEnd;
	}

	public void setPosition(String position){
		this.position = position;
	}

	public String getPosition(){
		return position;
	}

	public void setHeadBoss(String headBoss){
		this.headBoss = headBoss;
	}

	public String getHeadBoss(){
		return headBoss;
	}

	public void setEmployee(Object employee){
		this.employee = employee;
	}

	public Object getEmployee(){
		return employee;
	}

	@Override
 	public String toString(){
		return 
			"EmploymentItem{" + 
			"dateStart = '" + dateStart + '\'' + 
			",employeeEmploymentID = '" + employeeEmploymentID + '\'' + 
			",contactNumber = '" + contactNumber + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",company = '" + company + '\'' + 
			",jobDescription = '" + jobDescription + '\'' + 
			",dateEnd = '" + dateEnd + '\'' + 
			",position = '" + position + '\'' + 
			",headBoss = '" + headBoss + '\'' + 
			",employee = '" + employee + '\'' + 
			"}";
		}
}