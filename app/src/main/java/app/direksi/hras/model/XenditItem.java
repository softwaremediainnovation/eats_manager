package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class XenditItem{

	@SerializedName("transaction_id")
	private Object transactionId;

	@SerializedName("bank_code")
	private String bankCode;

	@SerializedName("amount")
	private int amount;

	@SerializedName("failure_code")
	private String failureCode;

	@SerializedName("xenditID")
	private int xenditID;

	@SerializedName("account_holder_name")
	private String accountHolderName;

	@SerializedName("created")
	private String created;

	@SerializedName("disbursement_id")
	private Object disbursementId;

	@SerializedName("email_cc2")
	private Object emailCc2;

	@SerializedName("email_cc1")
	private String emailCc1;

	@SerializedName("external_id")
	private String externalId;

	@SerializedName("disbursement_description")
	private String disbursementDescription;

	@SerializedName("transaction_sequence")
	private int transactionSequence;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("is_instant")
	private boolean isInstant;

	@SerializedName("email_cc3")
	private Object emailCc3;

	@SerializedName("id")
	private String id;

	@SerializedName("email_to1")
	private String emailTo1;

	@SerializedName("email_to2")
	private Object emailTo2;

	@SerializedName("email_to3")
	private Object emailTo3;

	@SerializedName("updated")
	private String updated;

	@SerializedName("status")
	private String status;

	public void setTransactionId(Object transactionId){
		this.transactionId = transactionId;
	}

	public Object getTransactionId(){
		return transactionId;
	}

	public void setBankCode(String bankCode){
		this.bankCode = bankCode;
	}

	public String getBankCode(){
		return bankCode;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setFailureCode(String failureCode){
		this.failureCode = failureCode;
	}

	public String getFailureCode(){
		return failureCode;
	}

	public void setXenditID(int xenditID){
		this.xenditID = xenditID;
	}

	public int getXenditID(){
		return xenditID;
	}

	public void setAccountHolderName(String accountHolderName){
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderName(){
		return accountHolderName;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setDisbursementId(Object disbursementId){
		this.disbursementId = disbursementId;
	}

	public Object getDisbursementId(){
		return disbursementId;
	}

	public void setEmailCc2(Object emailCc2){
		this.emailCc2 = emailCc2;
	}

	public Object getEmailCc2(){
		return emailCc2;
	}

	public void setEmailCc1(String emailCc1){
		this.emailCc1 = emailCc1;
	}

	public String getEmailCc1(){
		return emailCc1;
	}

	public void setExternalId(String externalId){
		this.externalId = externalId;
	}

	public String getExternalId(){
		return externalId;
	}

	public void setDisbursementDescription(String disbursementDescription){
		this.disbursementDescription = disbursementDescription;
	}

	public String getDisbursementDescription(){
		return disbursementDescription;
	}

	public void setTransactionSequence(int transactionSequence){
		this.transactionSequence = transactionSequence;
	}

	public int getTransactionSequence(){
		return transactionSequence;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setIsInstant(boolean isInstant){
		this.isInstant = isInstant;
	}

	public boolean isIsInstant(){
		return isInstant;
	}

	public void setEmailCc3(Object emailCc3){
		this.emailCc3 = emailCc3;
	}

	public Object getEmailCc3(){
		return emailCc3;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setEmailTo1(String emailTo1){
		this.emailTo1 = emailTo1;
	}

	public String getEmailTo1(){
		return emailTo1;
	}

	public void setEmailTo2(Object emailTo2){
		this.emailTo2 = emailTo2;
	}

	public Object getEmailTo2(){
		return emailTo2;
	}

	public void setEmailTo3(Object emailTo3){
		this.emailTo3 = emailTo3;
	}

	public Object getEmailTo3(){
		return emailTo3;
	}

	public void setUpdated(String updated){
		this.updated = updated;
	}

	public String getUpdated(){
		return updated;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"XenditItem{" + 
			"transaction_id = '" + transactionId + '\'' + 
			",bank_code = '" + bankCode + '\'' + 
			",amount = '" + amount + '\'' + 
			",failure_code = '" + failureCode + '\'' + 
			",xenditID = '" + xenditID + '\'' + 
			",account_holder_name = '" + accountHolderName + '\'' + 
			",created = '" + created + '\'' + 
			",disbursement_id = '" + disbursementId + '\'' + 
			",email_cc2 = '" + emailCc2 + '\'' + 
			",email_cc1 = '" + emailCc1 + '\'' + 
			",external_id = '" + externalId + '\'' + 
			",disbursement_description = '" + disbursementDescription + '\'' + 
			",transaction_sequence = '" + transactionSequence + '\'' + 
			",user_id = '" + userId + '\'' + 
			",is_instant = '" + isInstant + '\'' + 
			",email_cc3 = '" + emailCc3 + '\'' + 
			",id = '" + id + '\'' + 
			",email_to1 = '" + emailTo1 + '\'' + 
			",email_to2 = '" + emailTo2 + '\'' + 
			",email_to3 = '" + emailTo3 + '\'' + 
			",updated = '" + updated + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}