
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDetailAssetsAssetTracking {

    @SerializedName("assetTrackingID")
    private Long mAssetTrackingID;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("destinationHolder")
    private DataDetailAssetsDestinationHolder mDestinationHolder;
    @SerializedName("destinationHolderID")
    private Long mDestinationHolderID;
    @SerializedName("isAccepted")
    private Boolean mIsAccepted;
    @SerializedName("isMoved")
    private Boolean mIsMoved;
    @SerializedName("note")
    private String mNote;
    @SerializedName("originHolder")
    private DataDetailAssetsOriginHolder mOriginHolder;
    @SerializedName("originHolderID")
    private Long mOriginHolderID;

    public Long getAssetTrackingID() {
        return mAssetTrackingID;
    }

    public void setAssetTrackingID(Long assetTrackingID) {
        mAssetTrackingID = assetTrackingID;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public DataDetailAssetsDestinationHolder getDestinationHolder() {
        return mDestinationHolder;
    }

    public void setDestinationHolder(DataDetailAssetsDestinationHolder destinationHolder) {
        mDestinationHolder = destinationHolder;
    }

    public Long getDestinationHolderID() {
        return mDestinationHolderID;
    }

    public void setDestinationHolderID(Long destinationHolderID) {
        mDestinationHolderID = destinationHolderID;
    }

    public Boolean getIsAccepted() {
        return mIsAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        mIsAccepted = isAccepted;
    }

    public Boolean getIsMoved() {
        return mIsMoved;
    }

    public void setIsMoved(Boolean isMoved) {
        mIsMoved = isMoved;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public DataDetailAssetsOriginHolder getOriginHolder() {
        return mOriginHolder;
    }

    public void setOriginHolder(DataDetailAssetsOriginHolder originHolder) {
        mOriginHolder = originHolder;
    }

    public Long getOriginHolderID() {
        return mOriginHolderID;
    }

    public void setOriginHolderID(Long originHolderID) {
        mOriginHolderID = originHolderID;
    }

}
