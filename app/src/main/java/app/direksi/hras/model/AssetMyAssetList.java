
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AssetMyAssetList {

    @SerializedName("assetFile")
    private Object mAssetFile;
    @SerializedName("assetID")
    private Long mAssetID;
    @SerializedName("assetTrackings")
    private Object mAssetTrackings;
    @SerializedName("creator")
    private CreatorMyAssetList mCreator;
    @SerializedName("creatorID")
    private String mCreatorID;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("expiredDate")
    private String mExpiredDate;
    @SerializedName("dateDeleted")
    private Object mDateDeleted;
    @SerializedName("disposer")
    private Object mDisposer;
    @SerializedName("disposerID")
    private Object mDisposerID;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("isDeleted")
    private Boolean mIsDeleted;
    @SerializedName("lastLocation")
    private String mLastLocation;
    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("pending")
    private Boolean mPending;
    @SerializedName("constanta")
    private DataConstantaAsset constanta;


    public Object getAssetFile() {
        return mAssetFile;
    }

    public void setAssetFile(Object assetFile) {
        mAssetFile = assetFile;
    }

    public Long getAssetID() {
        return mAssetID;
    }

    public void setAssetID(Long assetID) {
        mAssetID = assetID;
    }

    public Object getAssetTrackings() {
        return mAssetTrackings;
    }

    public void setAssetTrackings(Object assetTrackings) {
        mAssetTrackings = assetTrackings;
    }

    public CreatorMyAssetList getCreator() {
        return mCreator;
    }

    public void setCreator(CreatorMyAssetList creator) {
        mCreator = creator;
    }

    public String getCreatorID() {
        return mCreatorID;
    }

    public void setCreatorID(String creatorID) {
        mCreatorID = creatorID;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public String getExpiredDate() {
        return mExpiredDate;
    }

    public void setExpiredDate(String dateCreated) {
        mExpiredDate = dateCreated;
    }

    public Object getDateDeleted() {
        return mDateDeleted;
    }

    public void setDateDeleted(Object dateDeleted) {
        mDateDeleted = dateDeleted;
    }

    public Object getDisposer() {
        return mDisposer;
    }

    public void setDisposer(Object disposer) {
        mDisposer = disposer;
    }

    public Object getDisposerID() {
        return mDisposerID;
    }

    public void setDisposerID(Object disposerID) {
        mDisposerID = disposerID;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public Boolean getIsDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        mIsDeleted = isDeleted;
    }

    public String getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(String lastLocation) {
        mLastLocation = lastLocation;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Boolean getPending() {
        return mPending;
    }

    public void setPending(Boolean note) {
        mPending = note;
    }

    public DataConstantaAsset getConstanta() {
        return constanta;
    }

    public void setConstanta(DataConstantaAsset note) {
        constanta = note;
    }

}
