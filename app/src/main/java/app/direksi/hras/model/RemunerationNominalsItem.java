package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemunerationNominalsItem{

	@SerializedName("date")
	private String date;

	@SerializedName("lembur3")
	private int lembur3;

	@SerializedName("ijinKeluarKantorTime")
	private double ijinKeluarKantorTime;

	@SerializedName("terlambatTime")
	private double terlambatTime;

	@SerializedName("premi1")
	private int premi1;

	@SerializedName("premi2")
	private int premi2;

	@SerializedName("hariTidakMasukTotal")
	private int hariTidakMasukTotal;

	@SerializedName("lembur1")
	private int lembur1;

	@SerializedName("lembur2")
	private int lembur2;

	@SerializedName("isModified")
	private boolean isModified;

	@SerializedName("lembur3Time")
	private int lembur3Time;

	@SerializedName("hariMasukTelat")
	private int hariMasukTelat;

	@SerializedName("tunjanganAdminBank")
	private int tunjanganAdminBank;

	@SerializedName("remunerationFormulas")
	private Object remunerationFormulas;

	@SerializedName("insentifTime")
	private int insentifTime;

	@SerializedName("bpjsKesehatan")
	private int bpjsKesehatan;

	@SerializedName("hariTidakMasukIjin")
	private int hariTidakMasukIjin;

	@SerializedName("remunerationID")
	private int remunerationID;

	@SerializedName("uangMakanLemburTime")
	private int uangMakanLemburTime;

	@SerializedName("isDelivered")
	private boolean isDelivered;

	@SerializedName("payroll")
	private RemuPayroll remuPayroll;

	@SerializedName("angsuranPinjaman")
	private Object angsuranPinjaman;

	@SerializedName("hariTidakMasukTidakIjin")
	private int hariTidakMasukTidakIjin;

	@SerializedName("hariTidakMasukH2")
	private int hariTidakMasukH2;

	@SerializedName("ijinKeluarKantor")
	private int ijinKeluarKantor;

	@SerializedName("uangMakan")
	private int uangMakan;

	@SerializedName("uangMakanLembur")
	private int uangMakanLembur;

	@SerializedName("holidayMasterID")
	private int holidayMasterID;

	@SerializedName("hariTidakTelat")
	private int hariTidakTelat;

	@SerializedName("hariTidakMasukCutiPerusahaan")
	private int hariTidakMasukCutiPerusahaan;

	@SerializedName("hariTidakMasukCutiTahunan")
	private int hariTidakMasukCutiTahunan;

	@SerializedName("insentif")
	private Object insentif;

	@SerializedName("ijinTidakMasuk")
	private int ijinTidakMasuk;

	@SerializedName("hariKerja")
	private int hariKerja;

	@SerializedName("gradeInsentif")
	private String gradeInsentif;

	@SerializedName("ijinTidakMasukTime")
	private int ijinTidakMasukTime;

	@SerializedName("pbonMingguan")
	private boolean pbonMingguan;

	@SerializedName("lembur2Time")
	private int lembur2Time;

	@SerializedName("hariTidakMasukSakit")
	private int hariTidakMasukSakit;

	@SerializedName("creator")
	private Object creator;

	@SerializedName("koreksiGaji")
	private int koreksiGaji;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("terlambat")
	private int terlambat;

	@SerializedName("payrollID")
	private int payrollID;

	@SerializedName("tunjanganLainLain")
	private int tunjanganLainLain;

	@SerializedName("lembur1Time")
	private int lembur1Time;

	@SerializedName("remunerationNominalID")
	private int remunerationNominalID;

	@SerializedName("tunjanganTetap")
	private int tunjanganTetap;

	@SerializedName("holidayMaster")
	private Object holidayMaster;

	@SerializedName("gajiPokok")
	private int gajiPokok;

	@SerializedName("pph21")
	private int pph21;

	public String getDate(){
		return date;
	}

	public int getLembur3(){
		return lembur3;
	}

	public double getIjinKeluarKantorTime(){
		return ijinKeluarKantorTime;
	}

	public double getTerlambatTime(){
		return terlambatTime;
	}

	public int getPremi1(){
		return premi1;
	}

	public int getPremi2(){
		return premi2;
	}

	public int getHariTidakMasukTotal(){
		return hariTidakMasukTotal;
	}

	public int getLembur1(){
		return lembur1;
	}

	public int getLembur2(){
		return lembur2;
	}

	public boolean isIsModified(){
		return isModified;
	}

	public int getLembur3Time(){
		return lembur3Time;
	}

	public int getHariMasukTelat(){
		return hariMasukTelat;
	}

	public int getTunjanganAdminBank(){
		return tunjanganAdminBank;
	}

	public Object getRemunerationFormulas(){
		return remunerationFormulas;
	}

	public int getInsentifTime(){
		return insentifTime;
	}

	public int getBpjsKesehatan(){
		return bpjsKesehatan;
	}

	public int getHariTidakMasukIjin(){
		return hariTidakMasukIjin;
	}

	public int getRemunerationID(){
		return remunerationID;
	}

	public int getUangMakanLemburTime(){
		return uangMakanLemburTime;
	}

	public boolean isIsDelivered(){
		return isDelivered;
	}

	public RemuPayroll getPayroll(){
		return remuPayroll;
	}

	public Object getAngsuranPinjaman(){
		return angsuranPinjaman;
	}

	public int getHariTidakMasukTidakIjin(){
		return hariTidakMasukTidakIjin;
	}

	public int getHariTidakMasukH2(){
		return hariTidakMasukH2;
	}

	public int getIjinKeluarKantor(){
		return ijinKeluarKantor;
	}

	public int getUangMakan(){
		return uangMakan;
	}

	public int getUangMakanLembur(){
		return uangMakanLembur;
	}

	public int getHolidayMasterID(){
		return holidayMasterID;
	}

	public int getHariTidakTelat(){
		return hariTidakTelat;
	}

	public int getHariTidakMasukCutiPerusahaan(){
		return hariTidakMasukCutiPerusahaan;
	}

	public int getHariTidakMasukCutiTahunan(){
		return hariTidakMasukCutiTahunan;
	}

	public Object getInsentif(){
		return insentif;
	}

	public int getIjinTidakMasuk(){
		return ijinTidakMasuk;
	}

	public int getHariKerja(){
		return hariKerja;
	}

	public String getGradeInsentif(){
		return gradeInsentif;
	}

	public int getIjinTidakMasukTime(){
		return ijinTidakMasukTime;
	}

	public boolean isPbonMingguan(){
		return pbonMingguan;
	}

	public int getLembur2Time(){
		return lembur2Time;
	}

	public int getHariTidakMasukSakit(){
		return hariTidakMasukSakit;
	}

	public Object getCreator(){
		return creator;
	}

	public int getKoreksiGaji(){
		return koreksiGaji;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public int getTerlambat(){
		return terlambat;
	}

	public int getPayrollID(){
		return payrollID;
	}

	public int getTunjanganLainLain(){
		return tunjanganLainLain;
	}

	public int getLembur1Time(){
		return lembur1Time;
	}

	public int getRemunerationNominalID(){
		return remunerationNominalID;
	}

	public int getTunjanganTetap(){
		return tunjanganTetap;
	}

	public Object getHolidayMaster(){
		return holidayMaster;
	}

	public int getGajiPokok(){
		return gajiPokok;
	}

	public int getPph21(){
		return pph21;
	}
}