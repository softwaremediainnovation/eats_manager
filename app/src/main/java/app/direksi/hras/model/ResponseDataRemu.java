package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseDataRemu{

	@SerializedName("data")
	private RemuData remuData;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public RemuData getData(){
		return remuData;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}