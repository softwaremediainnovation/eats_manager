package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataCreateOvertime{

	@SerializedName("employeeName")
	@Expose
	private String employeeName;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("approverID")
	@Expose
	private String approverID;

	@SerializedName("filePath")
	@Expose
	private String filePath;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("dateEnd")
	@Expose
	private String dateEnd;

	@SerializedName("approverName")
	@Expose
	private String approverName;

	@SerializedName("overtimeID")
	@Expose
	private int overtimeID;

	@SerializedName("dateStart")
	@Expose
	private String dateStart;

	@SerializedName("dateApproved")
	@Expose
	private String dateApproved;

	@SerializedName("overtimeFile")
	@Expose
	private OvertimeFile overtimeFile;

	@SerializedName("comment")
	@Expose
	private String comment;

	@SerializedName("isApproved")
	@Expose
	private String isApproved;

	public void setEmployeeName(String employeeName){
		this.employeeName = employeeName;
	}

	public String getEmployeeName(){
		return employeeName;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setApproverID(String approverID){
		this.approverID = approverID;
	}

	public String getApproverID(){
		return approverID;
	}

	public void setFilePath(String filePath){
		this.filePath = filePath;
	}

	public String getFilePath(){
		return filePath;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setDateEnd(String dateEnd){
		this.dateEnd = dateEnd;
	}

	public String getDateEnd(){
		return dateEnd;
	}

	public void setApproverName(String approverName){
		this.approverName = approverName;
	}

	public String getApproverName(){
		return approverName;
	}

	public void setOvertimeID(int overtimeID){
		this.overtimeID = overtimeID;
	}

	public int getOvertimeID(){
		return overtimeID;
	}

	public void setDateStart(String dateStart){
		this.dateStart = dateStart;
	}

	public String getDateStart(){
		return dateStart;
	}

	public void setDateApproved(String dateApproved){
		this.dateApproved = dateApproved;
	}

	public String getDateApproved(){
		return dateApproved;
	}

	public void setOvertimeFile(OvertimeFile overtimeFile){
		this.overtimeFile = overtimeFile;
	}

	public OvertimeFile getOvertimeFile(){
		return overtimeFile;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	public void setIsApproved(String isApproved){
		this.isApproved = isApproved;
	}

	public String getIsApproved(){
		return isApproved;
	}

	@Override
 	public String toString(){
		return 
			"DataCreateOvertime{" + 
			"employeeName = '" + employeeName + '\'' + 
			",note = '" + note + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",filePath = '" + filePath + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",dateEnd = '" + dateEnd + '\'' + 
			",approverName = '" + approverName + '\'' + 
			",overtimeID = '" + overtimeID + '\'' + 
			",dateStart = '" + dateStart + '\'' + 
			",dateApproved = '" + dateApproved + '\'' + 
			",overtimeFile = '" + overtimeFile + '\'' + 
			",comment = '" + comment + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			"}";
		}
}