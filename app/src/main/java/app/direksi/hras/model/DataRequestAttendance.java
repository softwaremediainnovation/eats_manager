package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataRequestAttendance {

	@SerializedName("dateTime")
	private String dateTime;

	@SerializedName("approver")
	private String approver;

	@SerializedName("approverID")
	private String approverID;

	@SerializedName("isMobileApp")
	private boolean isMobileApp;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("employee")
	private DataRequestAttendanceEmployee employee;

	@SerializedName("attendanceID")
	private int attendanceID;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("lateTimeInMinute")
	private int lateTimeInMinute;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("nama")
	private String nama;

	@SerializedName("isLate")
	private boolean isLate;

	@SerializedName("organization")
	private String organization;

	@SerializedName("isApproved")
	private Boolean isApproved;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("inOut")
	private String inOut;

	@SerializedName("dateGMT")
	private int dateGMT;

	@SerializedName("isPhotoApproved")
	private Boolean isPhotoApproved;



	public void setIsPhotoApproved(Boolean isApproved){
		this.isPhotoApproved = isApproved;
	}

	public Boolean getIsPhotoApproved(){
		return isPhotoApproved;
	}

	public void setDateTime(String dateTime){
		this.dateTime = dateTime;
	}

	public String getDateTime(){
		return dateTime;
	}

	public void setApprover(String approver){
		this.approver = approver;
	}

	public String getApprover(){
		return approver;
	}

	public void setApproverID(String approverID){
		this.approverID = approverID;
	}

	public String getApproverID(){
		return approverID;
	}

	public void setIsMobileApp(boolean isMobileApp){
		this.isMobileApp = isMobileApp;
	}

	public boolean isIsMobileApp(){
		return isMobileApp;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setEmployee(DataRequestAttendanceEmployee employee){
		this.employee = employee;
	}

	public DataRequestAttendanceEmployee getEmployee(){
		return employee;
	}

	public void setAttendanceID(int attendanceID){
		this.attendanceID = attendanceID;
	}

	public int getAttendanceID(){
		return attendanceID;
	}

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setLateTimeInMinute(int lateTimeInMinute){
		this.lateTimeInMinute = lateTimeInMinute;
	}

	public int getLateTimeInMinute(){
		return lateTimeInMinute;
	}

	public void setDateCreated(String dateCreated){
		this.dateCreated = dateCreated;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public void setNama(String nama){
		this.nama = nama;
	}

	public String getNama(){
		return nama;
	}

	public void setIsLate(boolean isLate){
		this.isLate = isLate;
	}

	public boolean isIsLate(){
		return isLate;
	}

	public void setOrganization(String organization){
		this.organization = organization;
	}

	public String getOrganization(){
		return organization;
	}

	public void setIsApproved(Boolean isApproved){
		this.isApproved = isApproved;
	}

	public Boolean getIsApproved(){
		return isApproved;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setInOut(String longitude){
		this.inOut = longitude;
	}

	public String getInOut(){
		return inOut;
	}

	public void setDateGMT(int dateGMT){
		this.dateGMT = dateGMT;
	}

	public int getDateGMT(){
		return dateGMT;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"dateTime = '" + dateTime + '\'' + 
			",approver = '" + approver + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",isMobileApp = '" + isMobileApp + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",employee = '" + employee + '\'' + 
			",attendanceID = '" + attendanceID + '\'' + 
			",organizationID = '" + organizationID + '\'' + 
			",lateTimeInMinute = '" + lateTimeInMinute + '\'' + 
			",dateCreated = '" + dateCreated + '\'' + 
			",nama = '" + nama + '\'' + 
			",isLate = '" + isLate + '\'' + 
			",organization = '" + organization + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			",longitude = '" + longitude + '\'' + 
			"}";
		}
}