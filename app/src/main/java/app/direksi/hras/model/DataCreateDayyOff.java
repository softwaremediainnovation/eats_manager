package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataCreateDayyOff{

	@SerializedName("dayOffID")
	@Expose
	private int dayOffID;


	public void setDayOffID(int dayOffID){
		this.dayOffID = dayOffID;
	}

	public int getDayOffID(){
		return dayOffID;
	}


}