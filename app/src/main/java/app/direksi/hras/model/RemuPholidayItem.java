package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemuPholidayItem {

	@SerializedName("date")
	private String date;

	@SerializedName("isActiveHtml")
	private String isActiveHtml;

	@SerializedName("publicHolidayID")
	private int publicHolidayID;

	@SerializedName("creator")
	private Object creator;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("description")
	private String description;

	@SerializedName("isActive")
	private boolean isActive;

	public String getDate(){
		return date;
	}

	public String getIsActiveHtml(){
		return isActiveHtml;
	}

	public int getPublicHolidayID(){
		return publicHolidayID;
	}

	public Object getCreator(){
		return creator;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public String getDescription(){
		return description;
	}

	public boolean isIsActive(){
		return isActive;
	}
}