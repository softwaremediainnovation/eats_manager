package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataForgetPassword {

	@SerializedName("result")
	private String result;

	@SerializedName("value")
	private Object value;

	@SerializedName("user")
	private Object user;

	@SerializedName("status")
	private String status;

	public String getResult(){
		return result;
	}

	public Object getValue(){
		return value;
	}

	public Object getUser(){
		return user;
	}

	public String getStatus(){
		return status;
	}
}