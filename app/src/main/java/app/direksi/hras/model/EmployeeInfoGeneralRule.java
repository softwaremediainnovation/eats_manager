
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EmployeeInfoGeneralRule {

    @SerializedName("description")
    private String mDescription;
    @SerializedName("permission")
    private String mPermission;

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getPermission() {
        return mPermission;
    }

    public void setPermission(String permission) {
        mPermission = permission;
    }

}
