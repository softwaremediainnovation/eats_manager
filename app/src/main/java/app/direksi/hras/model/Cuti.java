
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Cuti {

    @SerializedName("disetujui")
    private Long mDisetujui;
    @SerializedName("ditolak")
    private Long mDitolak;
    @SerializedName("menunggu")
    private Long mMenunggu;
    @SerializedName("total")
    private Long mTotal;

    public Long getDisetujui() {
        return mDisetujui;
    }

    public void setDisetujui(Long disetujui) {
        mDisetujui = disetujui;
    }

    public Long getDitolak() {
        return mDitolak;
    }

    public void setDitolak(Long ditolak) {
        mDitolak = ditolak;
    }

    public Long getMenunggu() {
        return mMenunggu;
    }

    public void setMenunggu(Long menunggu) {
        mMenunggu = menunggu;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long menunggu) {
        mTotal = menunggu;
    }

}
