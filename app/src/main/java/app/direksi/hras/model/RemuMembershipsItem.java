package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemuMembershipsItem {

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("isLeaderHtml")
	private String isLeaderHtml;

	@SerializedName("isLeader")
	private boolean isLeader;

	@SerializedName("organization")
	private RemuOrganization remuOrganization;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("organizationCompanyHtml")
	private String organizationCompanyHtml;

	@SerializedName("membershipID")
	private int membershipID;

	public int getOrganizationID(){
		return organizationID;
	}

	public String getIsLeaderHtml(){
		return isLeaderHtml;
	}

	public boolean isIsLeader(){
		return isLeader;
	}

	public RemuOrganization getOrganization(){
		return remuOrganization;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public String getOrganizationCompanyHtml(){
		return organizationCompanyHtml;
	}

	public int getMembershipID(){
		return membershipID;
	}
}