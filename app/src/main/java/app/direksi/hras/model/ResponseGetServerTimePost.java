package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseGetServerTimePost{

	@SerializedName("data")
	private DataGetServerTimePost data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public DataGetServerTimePost getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}