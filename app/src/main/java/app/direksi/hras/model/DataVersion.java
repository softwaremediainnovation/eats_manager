
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataVersion {

    @SerializedName("versions")
    private String mVersions;

    public String getVersions() {
        return mVersions;
    }

    public void setVersions(String versions) {
        mVersions = versions;
    }

}
