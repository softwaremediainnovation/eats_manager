
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataNews {

    @SerializedName("articleID")
    private Long mArticleID;
    @SerializedName("creatorID")
    private String mCreatorID;
    @SerializedName("date")
    private String mDate;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("news")
    private String mNews;
    @SerializedName("path")
    private String mPath;
    @SerializedName("photo")
    private String mPhoto;
    @SerializedName("title")
    private String mTitle;

    public Long getArticleID() {
        return mArticleID;
    }

    public void setArticleID(Long articleID) {
        mArticleID = articleID;
    }

    public String getCreatorID() {
        return mCreatorID;
    }

    public void setCreatorID(String creatorID) {
        mCreatorID = creatorID;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public String getNews() {
        return mNews;
    }

    public void setNews(String news) {
        mNews = news;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

}
