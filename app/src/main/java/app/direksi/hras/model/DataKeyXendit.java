package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataKeyXendit {

	@SerializedName("result")
	private String result;

	@SerializedName("idBuktiKasHeader")
	private Integer idBuktiKasHeader;

	@SerializedName("reimbursement")
	private List<DataClaim> reimbursement;

	@SerializedName("status")
	private String status;

	public String getResult(){
		return result;
	}

	public Integer getIdBuktiKasHeader(){
		return idBuktiKasHeader;
	}

	public String getStatus(){
		return status;
	}

	public List<DataClaim> getReimbursement(){
		return reimbursement;
	}
}