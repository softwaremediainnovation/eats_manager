
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Employee {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("applicationUser")
    private Object mApplicationUser;
    @SerializedName("assetTrackingDestinationHolders")
    private Object mAssetTrackingDestinationHolders;
    @SerializedName("assetTrackingOriginHolders")
    private Object mAssetTrackingOriginHolders;
    @SerializedName("attendances")
    private Object mAttendances;
    @SerializedName("bloodType")
    private Object mBloodType;
    @SerializedName("city")
    private Object mCity;
    @SerializedName("cityID")
    private Long mCityID;
    @SerializedName("contactEmergencyName")
    private Object mContactEmergencyName;
    @SerializedName("contactEmergencyPhone")
    private Object mContactEmergencyPhone;
    @SerializedName("dayOffs")
    private Object mDayOffs;
    @SerializedName("deviceID")
    private String mDeviceID;
    @SerializedName("diseaseHistory")
    private Object mDiseaseHistory;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("driverLicenseType")
    private Object mDriverLicenseType;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("employeeCertificates")
    private Object mEmployeeCertificates;
    @SerializedName("employeeEducations")
    private Object mEmployeeEducations;
    @SerializedName("employeeEmployments")
    private Object mEmployeeEmployments;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("employeeOrganizations")
    private Object mEmployeeOrganizations;
    @SerializedName("employeeRelatives")
    private Object mEmployeeRelatives;
    @SerializedName("expectedSalary")
    private Object mExpectedSalary;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("fullName")
    private String mFullName;
    @SerializedName("fullNameEmail")
    private String mFullNameEmail;
    @SerializedName("fullNameNik")
    private String mFullNameNik;
    @SerializedName("fullNameNikHtml")
    private String mFullNameNikHtml;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("generalRules")
    private Object mGeneralRules;
    @SerializedName("height")
    private Object mHeight;
    @SerializedName("idCardNumber")
    private String mIdCardNumber;
    @SerializedName("imageFile")
    private Object mImageFile;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("isActiveHtml")
    private String mIsActiveHtml;
    @SerializedName("isUsingGlasses")
    private Object mIsUsingGlasses;
    @SerializedName("joinDate")
    private Object mJoinDate;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("loans")
    private Object mLoans;
    @SerializedName("maritalStatus")
    private Object mMaritalStatus;
    @SerializedName("memberships")
    private Object mMemberships;
    @SerializedName("nationality")
    private String mNationality;
    @SerializedName("nik")
    private String mNik;
    @SerializedName("note")
    private Object mNote;
    @SerializedName("numberOfChildren")
    private Object mNumberOfChildren;
    @SerializedName("organizationRules")
    private Object mOrganizationRules;
    @SerializedName("overtimes")
    private Object mOvertimes;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("photoHtml")
    private String mPhotoHtml;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("province")
    private Object mProvince;
    @SerializedName("provinceID")
    private Object mProvinceID;
    @SerializedName("reimbursements")
    private Object mReimbursements;
    @SerializedName("religion")
    private String mReligion;
    @SerializedName("remunerations")
    private Object mRemunerations;
    @SerializedName("reprimands")
    private Object mReprimands;
    @SerializedName("resignDate")
    private Object mResignDate;
    @SerializedName("roleId")
    private Object mRoleId;
    @SerializedName("statusOfResidence")
    private Object mStatusOfResidence;
    @SerializedName("strengthness")
    private Object mStrengthness;
    @SerializedName("typeOfVehicle")
    private Object mTypeOfVehicle;
    @SerializedName("userID")
    private String mUserID;
    @SerializedName("weakness")
    private Object mWeakness;
    @SerializedName("weight")
    private Object mWeight;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getApplicationUser() {
        return mApplicationUser;
    }

    public void setApplicationUser(Object applicationUser) {
        mApplicationUser = applicationUser;
    }

    public Object getAssetTrackingDestinationHolders() {
        return mAssetTrackingDestinationHolders;
    }

    public void setAssetTrackingDestinationHolders(Object assetTrackingDestinationHolders) {
        mAssetTrackingDestinationHolders = assetTrackingDestinationHolders;
    }

    public Object getAssetTrackingOriginHolders() {
        return mAssetTrackingOriginHolders;
    }

    public void setAssetTrackingOriginHolders(Object assetTrackingOriginHolders) {
        mAssetTrackingOriginHolders = assetTrackingOriginHolders;
    }

    public Object getAttendances() {
        return mAttendances;
    }

    public void setAttendances(Object attendances) {
        mAttendances = attendances;
    }

    public Object getBloodType() {
        return mBloodType;
    }

    public void setBloodType(Object bloodType) {
        mBloodType = bloodType;
    }

    public Object getCity() {
        return mCity;
    }

    public void setCity(Object city) {
        mCity = city;
    }

    public Long getCityID() {
        return mCityID;
    }

    public void setCityID(Long cityID) {
        mCityID = cityID;
    }

    public Object getContactEmergencyName() {
        return mContactEmergencyName;
    }

    public void setContactEmergencyName(Object contactEmergencyName) {
        mContactEmergencyName = contactEmergencyName;
    }

    public Object getContactEmergencyPhone() {
        return mContactEmergencyPhone;
    }

    public void setContactEmergencyPhone(Object contactEmergencyPhone) {
        mContactEmergencyPhone = contactEmergencyPhone;
    }

    public Object getDayOffs() {
        return mDayOffs;
    }

    public void setDayOffs(Object dayOffs) {
        mDayOffs = dayOffs;
    }

    public String getDeviceID() {
        return mDeviceID;
    }

    public void setDeviceID(String deviceID) {
        mDeviceID = deviceID;
    }

    public Object getDiseaseHistory() {
        return mDiseaseHistory;
    }

    public void setDiseaseHistory(Object diseaseHistory) {
        mDiseaseHistory = diseaseHistory;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public Object getDriverLicenseType() {
        return mDriverLicenseType;
    }

    public void setDriverLicenseType(Object driverLicenseType) {
        mDriverLicenseType = driverLicenseType;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Object getEmployeeCertificates() {
        return mEmployeeCertificates;
    }

    public void setEmployeeCertificates(Object employeeCertificates) {
        mEmployeeCertificates = employeeCertificates;
    }

    public Object getEmployeeEducations() {
        return mEmployeeEducations;
    }

    public void setEmployeeEducations(Object employeeEducations) {
        mEmployeeEducations = employeeEducations;
    }

    public Object getEmployeeEmployments() {
        return mEmployeeEmployments;
    }

    public void setEmployeeEmployments(Object employeeEmployments) {
        mEmployeeEmployments = employeeEmployments;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Object getEmployeeOrganizations() {
        return mEmployeeOrganizations;
    }

    public void setEmployeeOrganizations(Object employeeOrganizations) {
        mEmployeeOrganizations = employeeOrganizations;
    }

    public Object getEmployeeRelatives() {
        return mEmployeeRelatives;
    }

    public void setEmployeeRelatives(Object employeeRelatives) {
        mEmployeeRelatives = employeeRelatives;
    }

    public Object getExpectedSalary() {
        return mExpectedSalary;
    }

    public void setExpectedSalary(Object expectedSalary) {
        mExpectedSalary = expectedSalary;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getFullNameEmail() {
        return mFullNameEmail;
    }

    public void setFullNameEmail(String fullNameEmail) {
        mFullNameEmail = fullNameEmail;
    }

    public String getFullNameNik() {
        return mFullNameNik;
    }

    public void setFullNameNik(String fullNameNik) {
        mFullNameNik = fullNameNik;
    }

    public String getFullNameNikHtml() {
        return mFullNameNikHtml;
    }

    public void setFullNameNikHtml(String fullNameNikHtml) {
        mFullNameNikHtml = fullNameNikHtml;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public Object getGeneralRules() {
        return mGeneralRules;
    }

    public void setGeneralRules(Object generalRules) {
        mGeneralRules = generalRules;
    }

    public Object getHeight() {
        return mHeight;
    }

    public void setHeight(Object height) {
        mHeight = height;
    }

    public String getIdCardNumber() {
        return mIdCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        mIdCardNumber = idCardNumber;
    }

    public Object getImageFile() {
        return mImageFile;
    }

    public void setImageFile(Object imageFile) {
        mImageFile = imageFile;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public String getIsActiveHtml() {
        return mIsActiveHtml;
    }

    public void setIsActiveHtml(String isActiveHtml) {
        mIsActiveHtml = isActiveHtml;
    }

    public Object getIsUsingGlasses() {
        return mIsUsingGlasses;
    }

    public void setIsUsingGlasses(Object isUsingGlasses) {
        mIsUsingGlasses = isUsingGlasses;
    }

    public Object getJoinDate() {
        return mJoinDate;
    }

    public void setJoinDate(Object joinDate) {
        mJoinDate = joinDate;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Object getLoans() {
        return mLoans;
    }

    public void setLoans(Object loans) {
        mLoans = loans;
    }

    public Object getMaritalStatus() {
        return mMaritalStatus;
    }

    public void setMaritalStatus(Object maritalStatus) {
        mMaritalStatus = maritalStatus;
    }

    public Object getMemberships() {
        return mMemberships;
    }

    public void setMemberships(Object memberships) {
        mMemberships = memberships;
    }

    public String getNationality() {
        return mNationality;
    }

    public void setNationality(String nationality) {
        mNationality = nationality;
    }

    public String getNik() {
        return mNik;
    }

    public void setNik(String nik) {
        mNik = nik;
    }

    public Object getNote() {
        return mNote;
    }

    public void setNote(Object note) {
        mNote = note;
    }

    public Object getNumberOfChildren() {
        return mNumberOfChildren;
    }

    public void setNumberOfChildren(Object numberOfChildren) {
        mNumberOfChildren = numberOfChildren;
    }

    public Object getOrganizationRules() {
        return mOrganizationRules;
    }

    public void setOrganizationRules(Object organizationRules) {
        mOrganizationRules = organizationRules;
    }

    public Object getOvertimes() {
        return mOvertimes;
    }

    public void setOvertimes(Object overtimes) {
        mOvertimes = overtimes;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPhotoHtml() {
        return mPhotoHtml;
    }

    public void setPhotoHtml(String photoHtml) {
        mPhotoHtml = photoHtml;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public Object getProvince() {
        return mProvince;
    }

    public void setProvince(Object province) {
        mProvince = province;
    }

    public Object getProvinceID() {
        return mProvinceID;
    }

    public void setProvinceID(Object provinceID) {
        mProvinceID = provinceID;
    }

    public Object getReimbursements() {
        return mReimbursements;
    }

    public void setReimbursements(Object reimbursements) {
        mReimbursements = reimbursements;
    }

    public String getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = religion;
    }

    public Object getRemunerations() {
        return mRemunerations;
    }

    public void setRemunerations(Object remunerations) {
        mRemunerations = remunerations;
    }

    public Object getReprimands() {
        return mReprimands;
    }

    public void setReprimands(Object reprimands) {
        mReprimands = reprimands;
    }

    public Object getResignDate() {
        return mResignDate;
    }

    public void setResignDate(Object resignDate) {
        mResignDate = resignDate;
    }

    public Object getRoleId() {
        return mRoleId;
    }

    public void setRoleId(Object roleId) {
        mRoleId = roleId;
    }

    public Object getStatusOfResidence() {
        return mStatusOfResidence;
    }

    public void setStatusOfResidence(Object statusOfResidence) {
        mStatusOfResidence = statusOfResidence;
    }

    public Object getStrengthness() {
        return mStrengthness;
    }

    public void setStrengthness(Object strengthness) {
        mStrengthness = strengthness;
    }

    public Object getTypeOfVehicle() {
        return mTypeOfVehicle;
    }

    public void setTypeOfVehicle(Object typeOfVehicle) {
        mTypeOfVehicle = typeOfVehicle;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public Object getWeakness() {
        return mWeakness;
    }

    public void setWeakness(Object weakness) {
        mWeakness = weakness;
    }

    public Object getWeight() {
        return mWeight;
    }

    public void setWeight(Object weight) {
        mWeight = weight;
    }

}
