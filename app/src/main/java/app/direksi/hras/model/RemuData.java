package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RemuData {

	/*@SerializedName("result")
	private List<RemuResultItem> result;*/

	@SerializedName("pholiday")
	private List<RemuPholidayItem> pholiday;

	@SerializedName("aholiday")
	private List<Object> aholiday;

	@SerializedName("hariTidakMasukTotal")
	private int hariTidakMasukTotal;

	@SerializedName("remunerationNominal")
	private RemunerationNominal remunerationNominal;

	@SerializedName("status")
	private String status;

	@SerializedName("str")
	private String str;

	@SerializedName("sisaloan")
	private RemuSisaloan remuSisaloan;

	/*public List<RemuResultItem> getResult(){
		return result;
	}*/

	public RemuSisaloan getSisaloan(){
		return remuSisaloan;
	}

	public List<RemuPholidayItem> getPholiday(){
		return pholiday;
	}

	public List<Object> getAholiday(){
		return aholiday;
	}

	public int getHariTidakMasukTotal(){
		return hariTidakMasukTotal;
	}

	public RemunerationNominal getRemunerationNominal(){
		return remunerationNominal;
	}

	public String getStatus(){
		return status;
	}

	public String getStr(){
		return str;
	}
}