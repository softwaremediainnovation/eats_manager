package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class EmployeeRiwayatPenilaian {

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("note")
	private Object note;

	@SerializedName("contractExpiredDate")
	private String contractExpiredDate;

	@SerializedName("gender")
	private String gender;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("userID")
	private String userID;

	@SerializedName("nik")
	private String nik;

	@SerializedName("photoUrl")
	private String photoUrl;

	@SerializedName("applicationUser")
	private Object applicationUser;

	@SerializedName("companyID")
	private int companyID;

	@SerializedName("joinDate")
	private String joinDate;

	@SerializedName("imageFile")
	private Object imageFile;

	@SerializedName("fullNameNik")
	private String fullNameNik;

	@SerializedName("email")
	private String email;

	@SerializedName("address")
	private String address;

	@SerializedName("organizationName")
	private Object organizationName;

	@SerializedName("isLeader")
	private Object isLeader;

	@SerializedName("deviceID")
	private String deviceID;

	@SerializedName("religion")
	private String religion;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("phone")
	private String phone;

	@SerializedName("dob")
	private String dob;

	@SerializedName("organization")
	private Object organization;

	@SerializedName("idCardNumber")
	private String idCardNumber;

	public String getLastName(){
		return lastName;
	}

	public Object getNote(){
		return note;
	}

	public String getContractExpiredDate(){
		return contractExpiredDate;
	}

	public String getGender(){
		return gender;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public String getUserID(){
		return userID;
	}

	public String getNik(){
		return nik;
	}

	public String getPhotoUrl(){
		return photoUrl;
	}

	public Object getApplicationUser(){
		return applicationUser;
	}

	public int getCompanyID(){
		return companyID;
	}

	public String getJoinDate(){
		return joinDate;
	}

	public Object getImageFile(){
		return imageFile;
	}

	public String getFullNameNik(){
		return fullNameNik;
	}

	public String getEmail(){
		return email;
	}

	public String getAddress(){
		return address;
	}

	public Object getOrganizationName(){
		return organizationName;
	}

	public Object getIsLeader(){
		return isLeader;
	}

	public String getDeviceID(){
		return deviceID;
	}

	public String getReligion(){
		return religion;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public String getFirstName(){
		return firstName;
	}

	public String getPhone(){
		return phone;
	}

	public String getDob(){
		return dob;
	}

	public Object getOrganization(){
		return organization;
	}

	public String getIdCardNumber(){
		return idCardNumber;
	}
}