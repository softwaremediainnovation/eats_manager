
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataTracking {

    @SerializedName("asset")
    private String mAsset;
    @SerializedName("assetID")
    private Long mAssetID;
    @SerializedName("assetTrackingID")
    private Long mAssetTrackingID;
    @SerializedName("date")
    private String mDate;
    @SerializedName("destinationHolder")
    private String mDestinationHolder;
    @SerializedName("destinationHolderID")
    private Long mDestinationHolderID;
    @SerializedName("isAccepted")
    private Boolean mIsAccepted;
    @SerializedName("isMoved")
    private Boolean mIsMoved;
    @SerializedName("note")
    private String mNote;
    @SerializedName("originHolder")
    private String mOriginHolder;
    @SerializedName("originHolderID")
    private Long mOriginHolderID;

    public String getAsset() {
        return mAsset;
    }

    public void setAsset(String asset) {
        mAsset = asset;
    }

    public Long getAssetID() {
        return mAssetID;
    }

    public void setAssetID(Long assetID) {
        mAssetID = assetID;
    }

    public Long getAssetTrackingID() {
        return mAssetTrackingID;
    }

    public void setAssetTrackingID(Long assetTrackingID) {
        mAssetTrackingID = assetTrackingID;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDestinationHolder() {
        return mDestinationHolder;
    }

    public void setDestinationHolder(String destinationHolder) {
        mDestinationHolder = destinationHolder;
    }

    public Long getDestinationHolderID() {
        return mDestinationHolderID;
    }

    public void setDestinationHolderID(Long destinationHolderID) {
        mDestinationHolderID = destinationHolderID;
    }

    public Boolean getIsAccepted() {
        return mIsAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        mIsAccepted = isAccepted;
    }

    public Boolean getIsMoved() {
        return mIsMoved;
    }

    public void setIsMoved(Boolean isMoved) {
        mIsMoved = isMoved;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getOriginHolder() {
        return mOriginHolder;
    }

    public void setOriginHolder(String originHolder) {
        mOriginHolder = originHolder;
    }

    public Long getOriginHolderID() {
        return mOriginHolderID;
    }

    public void setOriginHolderID(Long originHolderID) {
        mOriginHolderID = originHolderID;
    }

}
