
package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EmployeeInfoData {

    @SerializedName("membership")
    private List<EmployeeInfoMembership> mMembership;
    @SerializedName("generalRules")
    private List<EmployeeInfoGeneralRule> mRules;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("workingHour")
    private List<EmployeeInfoWorkingHour> mWorkingHour;
    @SerializedName("orgRules")
    private List<EmployeeInfoOrganizationRule> mOrgRules;

    public List<EmployeeInfoMembership> getMembership() {
        return mMembership;
    }

    public void setMembership(List<EmployeeInfoMembership> membership) {
        mMembership = membership;
    }

    public List<EmployeeInfoGeneralRule> getRules() {
        return mRules;
    }

    public void setRules(List<EmployeeInfoGeneralRule> rules) {
        mRules = rules;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public List<EmployeeInfoWorkingHour> getWorkingHour() {
        return mWorkingHour;
    }

    public void setWorkingHour(List<EmployeeInfoWorkingHour> workingHour) {
        mWorkingHour = workingHour;
    }

    public List<EmployeeInfoOrganizationRule> getOrgRules() {
        return mOrgRules;
    }

    public void setOrgRules(List<EmployeeInfoOrganizationRule> orgRules) {
        mOrgRules = orgRules;
    }

}
