
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDetailClaimReimburstmentCategory {

    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reimbursementCategoryID")
    private Long mReimbursementCategoryID;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Long getReimbursementCategoryID() {
        return mReimbursementCategoryID;
    }

    public void setReimbursementCategoryID(Long reimbursementCategoryID) {
        mReimbursementCategoryID = reimbursementCategoryID;
    }

}
