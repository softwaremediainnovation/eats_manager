package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataMembership {

	@SerializedName("note")
	private String note;

	@SerializedName("creator")
	private String creator;

	@SerializedName("membershipHistoryID")
	private int membershipHistoryID;

	@SerializedName("companyOld")
	private String companyOld;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("organizationOld")
	private String organizationOld;

	@SerializedName("companyNew")
	private String companyNew;

	@SerializedName("organizationNew")
	private String organizationNew;

	public String getNote(){
		return note;
	}

	public String getCreator(){
		return creator;
	}

	public int getMembershipHistoryID(){
		return membershipHistoryID;
	}

	public String getCompanyOld(){
		return companyOld;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public String getOrganizationOld(){
		return organizationOld;
	}

	public String getCompanyNew(){
		return companyNew;
	}

	public String getOrganizationNew(){
		return organizationNew;
	}
}