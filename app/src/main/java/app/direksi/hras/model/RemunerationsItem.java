package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RemunerationsItem{

	@SerializedName("date")
	private String date;

	@SerializedName("gajiPokokBpjs")
	private int gajiPokokBpjs;

	@SerializedName("premi1")
	private int premi1;

	@SerializedName("premi2")
	private int premi2;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("uangMakan")
	private int uangMakan;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("nomorNpwp")
	private String nomorNpwp;

	@SerializedName("penghasilanNetto")
	private Object penghasilanNetto;

	@SerializedName("insentif")
	private int insentif;

	@SerializedName("dateCreated")
	private Object dateCreated;

	@SerializedName("tunjanganAdminBank")
	private int tunjanganAdminBank;

	@SerializedName("bpjsKesehatan")
	private boolean bpjsKesehatan;

	@SerializedName("haveNpwp")
	private boolean haveNpwp;

	@SerializedName("bpjsTambahan")
	private int bpjsTambahan;

	@SerializedName("remunerationNominals")
	private List<RemunerationNominalsItem> remunerationNominals;

	@SerializedName("remunerationID")
	private int remunerationID;

	@SerializedName("bpjsKetenagakerjaanJht")
	private boolean bpjsKetenagakerjaanJht;

	@SerializedName("formulaResult")
	private Object formulaResult;

	@SerializedName("tunjanganLainLain")
	private int tunjanganLainLain;

	@SerializedName("bpjsKetenagakerjaanJp")
	private boolean bpjsKetenagakerjaanJp;

	@SerializedName("tunjanganTetap")
	private int tunjanganTetap;

	@SerializedName("pph21BuktiPotong")
	private Object pph21BuktiPotong;

	@SerializedName("gajiPokok")
	private int gajiPokok;

	@SerializedName("statusPtkp")
	private String statusPtkp;

	public String getDate(){
		return date;
	}

	public int getGajiPokokBpjs(){
		return gajiPokokBpjs;
	}

	public int getPremi1(){
		return premi1;
	}

	public int getPremi2(){
		return premi2;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public int getUangMakan(){
		return uangMakan;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public String getNomorNpwp(){
		return nomorNpwp;
	}

	public Object getPenghasilanNetto(){
		return penghasilanNetto;
	}

	public int getInsentif(){
		return insentif;
	}

	public Object getDateCreated(){
		return dateCreated;
	}

	public int getTunjanganAdminBank(){
		return tunjanganAdminBank;
	}

	public boolean isBpjsKesehatan(){
		return bpjsKesehatan;
	}

	public boolean isHaveNpwp(){
		return haveNpwp;
	}

	public int getBpjsTambahan(){
		return bpjsTambahan;
	}

	public List<RemunerationNominalsItem> getRemunerationNominals(){
		return remunerationNominals;
	}

	public int getRemunerationID(){
		return remunerationID;
	}

	public boolean isBpjsKetenagakerjaanJht(){
		return bpjsKetenagakerjaanJht;
	}

	public Object getFormulaResult(){
		return formulaResult;
	}

	public int getTunjanganLainLain(){
		return tunjanganLainLain;
	}

	public boolean isBpjsKetenagakerjaanJp(){
		return bpjsKetenagakerjaanJp;
	}

	public int getTunjanganTetap(){
		return tunjanganTetap;
	}

	public Object getPph21BuktiPotong(){
		return pph21BuktiPotong;
	}

	public int getGajiPokok(){
		return gajiPokok;
	}

	public String getStatusPtkp(){
		return statusPtkp;
	}
}