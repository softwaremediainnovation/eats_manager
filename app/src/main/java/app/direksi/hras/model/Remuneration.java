package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Remuneration{

	@SerializedName("date")
	private String date;

	@SerializedName("gajiPokokBpjs")
	private Double gajiPokokBpjs;

	@SerializedName("premi1")
	private Double premi1;

	@SerializedName("premi2")
	private Double premi2;

	@SerializedName("employeeID")
	private Double employeeID;

	@SerializedName("uangMakan")
	private Double uangMakan;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("nomorNpwp")
	private String nomorNpwp;

	@SerializedName("employee")
	private RemuEmployee remuEmployee;

	@SerializedName("penghasilanNetto")
	private Double penghasilanNetto;

	@SerializedName("insentif")
	private Double insentif;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("tunjanganAdminBank")
	private Double tunjanganAdminBank;

	@SerializedName("bpjsKesehatan")
	private boolean bpjsKesehatan;

	@SerializedName("haveNpwp")
	private boolean haveNpwp;

	@SerializedName("bpjsTambahan")
	private Double bpjsTambahan;

	@SerializedName("remunerationNominals")
	private List<Object> remunerationNominals;

	@SerializedName("remunerationID")
	private Double remunerationID;

	@SerializedName("bpjsKetenagakerjaanJht")
	private boolean bpjsKetenagakerjaanJht;

	@SerializedName("formulaResult")
	private Object formulaResult;

	@SerializedName("tunjanganLainLain")
	private Double tunjanganLainLain;

	@SerializedName("bpjsKetenagakerjaanJp")
	private boolean bpjsKetenagakerjaanJp;

	@SerializedName("tunjanganTetap")
	private Double tunjanganTetap;

	@SerializedName("pph21BuktiPotong")
	private Double pph21BuktiPotong;

	@SerializedName("gajiPokok")
	private Double gajiPokok;

	@SerializedName("statusPtkp")
	private String statusPtkp;

	public String getDate(){
		return date;
	}

	public Double getGajiPokokBpjs(){
		return gajiPokokBpjs;
	}

	public Double getPremi1(){
		return premi1;
	}

	public Double getPremi2(){
		return premi2;
	}

	public Double getEmployeeID(){
		return employeeID;
	}

	public Double getUangMakan(){
		return uangMakan;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public String getNomorNpwp(){
		return nomorNpwp;
	}

	public RemuEmployee getEmployee(){
		return remuEmployee;
	}

	public Double getPenghasilanNetto(){
		return penghasilanNetto;
	}

	public Double getInsentif(){
		return insentif;
	}

	public Object getDateCreated(){
		return dateCreated;
	}

	public Double getTunjanganAdminBank(){
		return tunjanganAdminBank;
	}

	public boolean isBpjsKesehatan(){
		return bpjsKesehatan;
	}

	public boolean isHaveNpwp(){
		return haveNpwp;
	}

	public Double getBpjsTambahan(){
		return bpjsTambahan;
	}

	public List<Object> getRemunerationNominals(){
		return remunerationNominals;
	}

	public Double getRemunerationID(){
		return remunerationID;
	}

	public boolean isBpjsKetenagakerjaanJht(){
		return bpjsKetenagakerjaanJht;
	}

	public Object getFormulaResult(){
		return formulaResult;
	}

	public Double getTunjanganLainLain(){
		return tunjanganLainLain;
	}

	public boolean isBpjsKetenagakerjaanJp(){
		return bpjsKetenagakerjaanJp;
	}

	public Double getTunjanganTetap(){
		return tunjanganTetap;
	}

	public Double getPph21BuktiPotong(){
		return pph21BuktiPotong;
	}

	public Double getGajiPokok(){
		return gajiPokok;
	}

	public String getStatusPtkp(){
		return statusPtkp;
	}
}