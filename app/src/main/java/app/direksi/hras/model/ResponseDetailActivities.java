package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDetailActivities{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private DataActivities dataActivities;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataActivities(DataActivities dataActivities){
		this.dataActivities = dataActivities;
	}

	public DataActivities getDataActivities(){
		return dataActivities;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDetailActivities{" + 
			"errCode = '" + errCode + '\'' + 
			",dataActivities = '" + dataActivities + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}