
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Gaji {

    @SerializedName("notifikasi")
    private String mNotifikasi;
    @SerializedName("remunerationNominalID")
    private Double mRemunerationNominalID;

    public String getNotifikasi() {
        return mNotifikasi;
    }

    public void setNotifikasi(String notifikasi) {
        mNotifikasi = notifikasi;
    }

    public Double getRemunerationNominalID() {
        return mRemunerationNominalID;
    }

    public void setRemunerationNominalID(Double remunerationNominalID) {
        mRemunerationNominalID = remunerationNominalID;
    }

}
