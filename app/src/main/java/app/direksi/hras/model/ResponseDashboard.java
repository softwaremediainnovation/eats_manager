
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseDashboard {

    @SerializedName("data")
    private DataLuarDashboard mDataLuar;
    @SerializedName("errCode")
    private Long mErrCode;
    @SerializedName("errMessage")
    private String mErrMessage;

    public DataLuarDashboard getDataLuar() {
        return mDataLuar;
    }

    public void setDataLuar(DataLuarDashboard dataLuar) {
        mDataLuar = dataLuar;
    }

    public Long getErrCode() {
        return mErrCode;
    }

    public void setErrCode(Long errCode) {
        mErrCode = errCode;
    }

    public String getErrMessage() {
        return mErrMessage;
    }

    public void setErrMessage(String errMessage) {
        mErrMessage = errMessage;
    }

}
