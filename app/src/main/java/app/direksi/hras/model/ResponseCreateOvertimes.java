package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCreateOvertimes{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private DataCreateOvertime dataCreateOvertime;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataCreateOvertime(DataCreateOvertime dataCreateOvertime){
		this.dataCreateOvertime = dataCreateOvertime;
	}

	public DataCreateOvertime getDataCreateOvertime(){
		return dataCreateOvertime;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseCreateOvertimes{" + 
			"errCode = '" + errCode + '\'' + 
			",dataCreateOvertime = '" + dataCreateOvertime + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}