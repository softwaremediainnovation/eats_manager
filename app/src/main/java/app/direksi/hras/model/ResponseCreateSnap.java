package app.direksi.hras.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCreateSnap implements Parcelable {

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@SerializedName("result")
	@Expose
	private String result;


	@SerializedName("status")
	@Expose
	private String status;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.result);
		dest.writeString(this.status);
	}

	public ResponseCreateSnap() {
	}

	protected ResponseCreateSnap(Parcel in) {
		this.result = in.readString();
		this.status = in.readString();
	}

	public static final Creator<ResponseCreateSnap> CREATOR = new Creator<ResponseCreateSnap>() {
		@Override
		public ResponseCreateSnap createFromParcel(Parcel source) {
			return new ResponseCreateSnap(source);
		}

		@Override
		public ResponseCreateSnap[] newArray(int size) {
			return new ResponseCreateSnap[size];
		}
	};
}