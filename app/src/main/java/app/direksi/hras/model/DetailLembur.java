
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailLembur {

    @SerializedName("approverName")
    private String mApprover;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("dateEnd")
    private String mDateEnd;
    @SerializedName("dateStart")
    private String mDateStart;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("filePathHtml")
    private String mFilePathHtml;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("isApprovedHtml")
    private String mIsApprovedHtml;
    @SerializedName("note")
    private String mNote;
    @SerializedName("overtimeFile")
    private String mOvertimeFile;
    @SerializedName("overtimeID")
    private Long mOvertimeID;
    @SerializedName("employee")
    private Employee mEmployee;

    public String getApprover() {
        return mApprover;
    }

    public void setApprover(String approver) {
        mApprover = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getDateEnd() {
        return mDateEnd;
    }

    public void setDateEnd(String dateEnd) {
        mDateEnd = dateEnd;
    }

    public String getDateStart() {
        return mDateStart;
    }

    public void setDateStart(String dateStart) {
        mDateStart = dateStart;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employee) {
        mEmployeeName = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getFilePathHtml() {
        return mFilePathHtml;
    }

    public void setFilePathHtml(String filePathHtml) {
        mFilePathHtml = filePathHtml;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public String getIsApprovedHtml() {
        return mIsApprovedHtml;
    }

    public void setIsApprovedHtml(String isApprovedHtml) {
        mIsApprovedHtml = isApprovedHtml;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getOvertimeFile() {
        return mOvertimeFile;
    }

    public void setOvertimeFile(String overtimeFile) {
        mOvertimeFile = overtimeFile;
    }

    public Long getOvertimeID() {
        return mOvertimeID;
    }

    public void setOvertimeID(Long overtimeID) {
        mOvertimeID = overtimeID;
    }

    public Employee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Employee employee) {
        mEmployee = employee;
    }

}
