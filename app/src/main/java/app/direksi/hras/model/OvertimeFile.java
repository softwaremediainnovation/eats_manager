package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OvertimeFile{

	@SerializedName("headers")
	@Expose
	private Headers headers;

	@SerializedName("fileName")
	@Expose
	private String fileName;

	@SerializedName("contentDisposition")
	@Expose
	private String contentDisposition;

	@SerializedName("length")
	@Expose
	private int length;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("contentType")
	@Expose
	private String contentType;

	public void setHeaders(Headers headers){
		this.headers = headers;
	}

	public Headers getHeaders(){
		return headers;
	}

	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	public String getFileName(){
		return fileName;
	}

	public void setContentDisposition(String contentDisposition){
		this.contentDisposition = contentDisposition;
	}

	public String getContentDisposition(){
		return contentDisposition;
	}

	public void setLength(int length){
		this.length = length;
	}

	public int getLength(){
		return length;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setContentType(String contentType){
		this.contentType = contentType;
	}

	public String getContentType(){
		return contentType;
	}

	@Override
 	public String toString(){
		return 
			"OvertimeFile{" + 
			"headers = '" + headers + '\'' + 
			",fileName = '" + fileName + '\'' + 
			",contentDisposition = '" + contentDisposition + '\'' + 
			",length = '" + length + '\'' + 
			",name = '" + name + '\'' + 
			",contentType = '" + contentType + '\'' + 
			"}";
		}
}