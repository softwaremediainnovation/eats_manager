
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataAttendance {

    @SerializedName("attendanceID")
    private Long mAttendanceID;
    @SerializedName("attendanceOverview")
    private String mAttendanceOverview;
    @SerializedName("attendanceOverviewID")
    private Long mAttendanceOverviewID;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("dateTime")
    private String mDateTime;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("isLate")
    private Boolean mIsLate;
    @SerializedName("isMobileApp")
    private Boolean mIsMobileApp;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("lateTimeInMinute")
    private Double mLateTimeInMinute;
    @SerializedName("nama")
    private String mNama;
    @SerializedName("organization")
    private String organization;
    @SerializedName("inOut")
    private String inOut;
    @SerializedName("approver")
    private String approver;
    @SerializedName("isApproved")
    private String isApproved;
    @SerializedName("employeeName")
    private String employeeName;
    @SerializedName("approverID")
    private String approverID;
    @SerializedName("dateGMT")
    private int dateGMT;
    @SerializedName("isPhotoApproved")
    private Boolean isPhotoApproved;


    public Boolean getIsPhotoApproved() {
        return isPhotoApproved;
    }

    public void setIsPhotoApproved(Boolean isLate) {
        isPhotoApproved = isLate;
    }

    public Long getAttendanceID() {
        return mAttendanceID;
    }

    public void setAttendanceID(Long attendanceID) {
        mAttendanceID = attendanceID;
    }

    public String getAttendanceOverview() {
        return mAttendanceOverview;
    }

    public void setAttendanceOverview(String attendanceOverview) {
        mAttendanceOverview = attendanceOverview;
    }

    public Long getAttendanceOverviewID() {
        return mAttendanceOverviewID;
    }

    public void setAttendanceOverviewID(Long attendanceOverviewID) {
        mAttendanceOverviewID = attendanceOverviewID;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String dateTime) {
        mDateTime = dateTime;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Boolean getIsLate() {
        return mIsLate;
    }

    public void setIsLate(Boolean isLate) {
        mIsLate = isLate;
    }

    public Boolean getIsMobileApp() {
        return mIsMobileApp;
    }

    public void setIsMobileApp(Boolean isMobileApp) {
        mIsMobileApp = isMobileApp;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public Double getLateTimeInMinute() {
        return mLateTimeInMinute;
    }

    public void setLateTimeInMinute(Double longitude) {
        mLateTimeInMinute = longitude;
    }

    public String getNama() {
        return mNama;
    }

    public void setNama(String longitude) {
        mNama = longitude;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String longitude) {
        organization = longitude;
    }

    public void setInOut(String inOut){
        this.inOut = inOut;
    }

    public String getInOut(){
        return inOut;
    }

    public void setApprover(String approver){
        this.approver = approver;
    }

    public String getApprover(){
        return approver;
    }

    public void setIsApproved(String isApproved){
        this.isApproved = isApproved;
    }

    public String getIsApproved(){
        return isApproved;
    }

    public void setEmployeeName(String isApproved){
        this.employeeName = isApproved;
    }

    public String getEmployeeName(){
        return employeeName;
    }

    public void setApproverID(String isApproved){
        this.approverID = isApproved;
    }

    public String getApproverID(){
        return approverID;
    }

    public void setDateGMT(int isApproved){
        this.dateGMT = isApproved;
    }

    public int getDateGMT(){
        return dateGMT;
    }
}
