
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Surat {

    @SerializedName("surat")
    private String mSurat;
    @SerializedName("total")
    private Long mTotal;

    public String getSurat() {
        return mSurat;
    }

    public void setSurat(String surat) {
        mSurat = surat;
    }

    public Long getTotal() {
        return mTotal;
    }

    public void setTotal(Long total) {
        mTotal = total;
    }

}
