package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCreateAsset {

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}


	@Override
 	public String toString(){
		return 
			"ResponseCreateDayyOff{" + 
			"errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' +
			"}";
		}
}