package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCreateCuti{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private DataCreateCuti dataCreateCuti;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataCreateCuti(DataCreateCuti dataCreateCuti){
		this.dataCreateCuti = dataCreateCuti;
	}

	public DataCreateCuti getDataCreateCuti(){
		return dataCreateCuti;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseCreateCuti{" + 
			"errCode = '" + errCode + '\'' + 
			",dataCreateCuti = '" + dataCreateCuti + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}