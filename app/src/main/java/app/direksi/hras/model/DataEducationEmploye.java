
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataEducationEmploye {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("dateEnd")
    private String mDateEnd;
    @SerializedName("dateStart")
    private String mDateStart;
    @SerializedName("educationLevel")
    private EducationLevel mEducationLevel;
    @SerializedName("educationLevelID")
    private Double mEducationLevelID;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("employeeEducationID")
    private Long mEmployeeEducationID;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("instituteName")
    private String mInstituteName;
    @SerializedName("isGraduate")
    private Boolean mIsGraduate;
    @SerializedName("major")
    private Major mMajor;
    @SerializedName("majorID")
    private Long mMajorID;
    @SerializedName("score")
    private Double mScore;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getDateEnd() {
        return mDateEnd;
    }

    public void setDateEnd(String dateEnd) {
        mDateEnd = dateEnd;
    }

    public String getDateStart() {
        return mDateStart;
    }

    public void setDateStart(String dateStart) {
        mDateStart = dateStart;
    }

    public EducationLevel getEducationLevel() {
        return mEducationLevel;
    }

    public void setEducationLevel(EducationLevel educationLevel) {
        mEducationLevel = educationLevel;
    }

    public Double getEducationLevelID() {
        return mEducationLevelID;
    }

    public void setEducationLevelID(Double educationLevelID) {
        mEducationLevelID = educationLevelID;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public Long getEmployeeEducationID() {
        return mEmployeeEducationID;
    }

    public void setEmployeeEducationID(Long employeeEducationID) {
        mEmployeeEducationID = employeeEducationID;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getInstituteName() {
        return mInstituteName;
    }

    public void setInstituteName(String instituteName) {
        mInstituteName = instituteName;
    }

    public Boolean getIsGraduate() {
        return mIsGraduate;
    }

    public void setIsGraduate(Boolean isGraduate) {
        mIsGraduate = isGraduate;
    }

    public Major getMajor() {
        return mMajor;
    }

    public void setMajor(Major major) {
        mMajor = major;
    }

    public Long getMajorID() {
        return mMajorID;
    }

    public void setMajorID(Long majorID) {
        mMajorID = majorID;
    }

    public Double getScore() {
        return mScore;
    }

    public void setScore(Double score) {
        mScore = score;
    }

}
