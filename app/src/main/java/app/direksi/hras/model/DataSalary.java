package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataSalary {

	@SerializedName("date")
	private String date;

	@SerializedName("nominal")
	private Double nominal;

	@SerializedName("remunerationNominalID")
	private int remunerationNominalID;

	@SerializedName("name")
	private String name;

	@SerializedName("payrollID")
	private int payrollID;

	@SerializedName("remunerationID")
	private int remunerationID;

	public String getDate(){
		return date;
	}

	public Double getNominal(){
		return nominal;
	}

	public int getRemunerationNominalID(){
		return remunerationNominalID;
	}

	public String getName(){
		return name;
	}

	public int getPayrollID(){
		return payrollID;
	}

	public int getRemunerationID(){
		return remunerationID;
	}


}