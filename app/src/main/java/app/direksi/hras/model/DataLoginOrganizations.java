
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataLoginOrganizations {

    @SerializedName("companyID")
    private Long mCompanyID;
    @SerializedName("holidayMasterID")
    private Long mHolidayMasterID;
    @SerializedName("name")
    private String mName;
    @SerializedName("organizationID")
    private Long mOrganizationID;

    public Long getCompanyID() {
        return mCompanyID;
    }

    public void setCompanyID(Long companyID) {
        mCompanyID = companyID;
    }

    public Long getHolidayMasterID() {
        return mHolidayMasterID;
    }

    public void setHolidayMasterID(Long holidayMasterID) {
        mHolidayMasterID = holidayMasterID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Long getOrganizationID() {
        return mOrganizationID;
    }

    public void setOrganizationID(Long organizationID) {
        mOrganizationID = organizationID;
    }

}
