
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataTeguran {

    @SerializedName("date")
    private String mDate;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("issuerID")
    private String mIssuerID;
    @SerializedName("issuerName")
    private String mIssuerName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reprimandCategoryID")
    private Long mReprimandCategoryID;
    @SerializedName("reprimandCategoryName")
    private String mReprimandCategoryName;
    @SerializedName("reprimandFile")
    private Object mReprimandFile;
    @SerializedName("reprimandID")
    private Long mReprimandID;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getIssuerID() {
        return mIssuerID;
    }

    public void setIssuerID(String issuerID) {
        mIssuerID = issuerID;
    }

    public String getIssuerName() {
        return mIssuerName;
    }

    public void setIssuerName(String issuerName) {
        mIssuerName = issuerName;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Long getReprimandCategoryID() {
        return mReprimandCategoryID;
    }

    public void setReprimandCategoryID(Long reprimandCategoryID) {
        mReprimandCategoryID = reprimandCategoryID;
    }

    public String getReprimandCategoryName() {
        return mReprimandCategoryName;
    }

    public void setReprimandCategoryName(String reprimandCategoryName) {
        mReprimandCategoryName = reprimandCategoryName;
    }

    public Object getReprimandFile() {
        return mReprimandFile;
    }

    public void setReprimandFile(Object reprimandFile) {
        mReprimandFile = reprimandFile;
    }

    public Long getReprimandID() {
        return mReprimandID;
    }

    public void setReprimandID(Long reprimandID) {
        mReprimandID = reprimandID;
    }

}
