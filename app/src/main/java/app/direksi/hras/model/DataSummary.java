
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataSummary {

    @SerializedName("dayOffs")
    private Long mDayOffs;
    @SerializedName("loan")
    private Double mLoan;
    @SerializedName("overtime")
    private Long mOvertime;
    @SerializedName("payout")
    private Double mPayout;
    @SerializedName("reimbursement")
    private Double mReimbursement;
    @SerializedName("status")
    private String mStatus;
    @SerializedName("reprimand")
    private Long mReprimand;
    @SerializedName("late")
    private Double mLate;


    public Long getDayOffs() {
        return mDayOffs;
    }

    public void setDayOffs(Long dayOffs) {
        mDayOffs = dayOffs;
    }

    public Double getLoan() {
        return mLoan;
    }

    public void setLoan(Double loan) {
        mLoan = loan;
    }

    public Long getOvertime() {
        return mOvertime;
    }

    public void setOvertime(Long overtime) {
        mOvertime = overtime;
    }

    public Double getPayout() {
        return mPayout;
    }

    public void setPayout(Double payout) {
        mPayout = payout;
    }

    public Double getReimbursement() {
        return mReimbursement;
    }

    public void setReimbursement(Double reimbursement) {
        mReimbursement = reimbursement;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public Long getReprimand() {
        return mReprimand;
    }

    public void setReprimand(Long status) {
        mReprimand = status;
    }

    public Double getLate() {
        return mLate;
    }

    public void setLate(Double status) {
        mLate = status;
    }

}
