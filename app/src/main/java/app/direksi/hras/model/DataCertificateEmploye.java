
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataCertificateEmploye {

    @SerializedName("dateFrom")
    private String mDateFrom;
    @SerializedName("dateTo")
    private String mDateTo;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("employeeCertificateID")
    private Long mEmployeeCertificateID;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("isGraduate")
    private Boolean mIsGraduate;
    @SerializedName("name")
    private String mName;

    public String getDateFrom() {
        return mDateFrom;
    }

    public void setDateFrom(String dateFrom) {
        mDateFrom = dateFrom;
    }

    public String getDateTo() {
        return mDateTo;
    }

    public void setDateTo(String dateTo) {
        mDateTo = dateTo;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public Long getEmployeeCertificateID() {
        return mEmployeeCertificateID;
    }

    public void setEmployeeCertificateID(Long employeeCertificateID) {
        mEmployeeCertificateID = employeeCertificateID;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Boolean getIsGraduate() {
        return mIsGraduate;
    }

    public void setIsGraduate(Boolean isGraduate) {
        mIsGraduate = isGraduate;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
