
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailTeguran {

    @SerializedName("date")
    private String mDate;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("filePathHtml")
    private String mFilePathHtml;
    @SerializedName("issuer")
    private String mIssuer;
    @SerializedName("issuerID")
    private String mIssuerID;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reprimandCategory")
    private String mReprimandCategory;
    @SerializedName("reprimandCategoryID")
    private Long mReprimandCategoryID;
    @SerializedName("reprimandFile")
    private String mReprimandFile;
    @SerializedName("reprimandID")
    private Long mReprimandID;
    @SerializedName("employee")
    private Employee mEmployee;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employee) {
        mEmployeeName = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getFilePathHtml() {
        return mFilePathHtml;
    }

    public void setFilePathHtml(String filePathHtml) {
        mFilePathHtml = filePathHtml;
    }

    public String getIssuer() {
        return mIssuer;
    }

    public void setIssuer(String issuer) {
        mIssuer = issuer;
    }

    public String getIssuerID() {
        return mIssuerID;
    }

    public void setIssuerID(String issuerID) {
        mIssuerID = issuerID;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getReprimandCategory() {
        return mReprimandCategory;
    }

    public void setReprimandCategory(String reprimandCategory) {
        mReprimandCategory = reprimandCategory;
    }

    public Long getReprimandCategoryID() {
        return mReprimandCategoryID;
    }

    public void setReprimandCategoryID(Long reprimandCategoryID) {
        mReprimandCategoryID = reprimandCategoryID;
    }

    public String getReprimandFile() {
        return mReprimandFile;
    }

    public void setReprimandFile(String reprimandFile) {
        mReprimandFile = reprimandFile;
    }

    public Long getReprimandID() {
        return mReprimandID;
    }

    public void setReprimandID(Long reprimandID) {
        mReprimandID = reprimandID;
    }

    public Employee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Employee employee) {
        mEmployee = employee;
    }

}
