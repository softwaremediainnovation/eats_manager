
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;



@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EducationLevel {

    @SerializedName("educationLevelID")
    private Long mEducationLevelID;

    @SerializedName("employeeRelatives")
    private Object mEmployeeRelatives;
    @SerializedName("name")
    private String mName;
    @SerializedName("orderNumber")
    private Long mOrderNumber;

    public Long getEducationLevelID() {
        return mEducationLevelID;
    }

    public void setEducationLevelID(Long educationLevelID) {
        mEducationLevelID = educationLevelID;
    }



    public Object getEmployeeRelatives() {
        return mEmployeeRelatives;
    }

    public void setEmployeeRelatives(Object employeeRelatives) {
        mEmployeeRelatives = employeeRelatives;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Long getOrderNumber() {
        return mOrderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        mOrderNumber = orderNumber;
    }

}
