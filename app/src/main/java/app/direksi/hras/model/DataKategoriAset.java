package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataKategoriAset {

	@SerializedName("unit")
	private String unit;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("name")
	private String name;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("constantaID")
	private int constantaID;

	@SerializedName("constantaCategoryID")
	private int constantaCategoryID;

	@SerializedName("value")
	private String value;

	public String getUnit(){
		return unit;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public String getName(){
		return name;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public int getConstantaID(){
		return constantaID;
	}

	public int getConstantaCategoryID(){
		return constantaCategoryID;
	}

	public String getValue(){
		return value;
	}
}