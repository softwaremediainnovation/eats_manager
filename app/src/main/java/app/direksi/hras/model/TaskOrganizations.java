package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class TaskOrganizations {

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("isLeaderHtml")
	private String isLeaderHtml;

	@SerializedName("isLeader")
	private boolean isLeader;

	@SerializedName("organization")
	private Object organization;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("organizationCompanyHtml")
	private String organizationCompanyHtml;

	@SerializedName("membershipID")
	private int membershipID;

	@SerializedName("employee")
	private Object employee;

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setIsLeaderHtml(String isLeaderHtml){
		this.isLeaderHtml = isLeaderHtml;
	}

	public String getIsLeaderHtml(){
		return isLeaderHtml;
	}

	public void setIsLeader(boolean isLeader){
		this.isLeader = isLeader;
	}

	public boolean isIsLeader(){
		return isLeader;
	}

	public void setOrganization(Object organization){
		this.organization = organization;
	}

	public Object getOrganization(){
		return organization;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setOrganizationCompanyHtml(String organizationCompanyHtml){
		this.organizationCompanyHtml = organizationCompanyHtml;
	}

	public String getOrganizationCompanyHtml(){
		return organizationCompanyHtml;
	}

	public void setMembershipID(int membershipID){
		this.membershipID = membershipID;
	}

	public int getMembershipID(){
		return membershipID;
	}

	public void setEmployee(Object employee){
		this.employee = employee;
	}

	public Object getEmployee(){
		return employee;
	}

	@Override
 	public String toString(){
		return 
			"OrganizationsItem{" + 
			"organizationID = '" + organizationID + '\'' + 
			",isLeaderHtml = '" + isLeaderHtml + '\'' + 
			",isLeader = '" + isLeader + '\'' + 
			",organization = '" + organization + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",organizationCompanyHtml = '" + organizationCompanyHtml + '\'' + 
			",membershipID = '" + membershipID + '\'' + 
			",employee = '" + employee + '\'' + 
			"}";
		}
}