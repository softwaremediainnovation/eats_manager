package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCreateDayyOff{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	@SerializedName("data")
	@Expose
	private DataCreateDayyOff dataCreateDayyOff;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	public void setDataCreateDayyOff(DataCreateDayyOff dataCreateDayyOff){
		this.dataCreateDayyOff = dataCreateDayyOff;
	}

	public DataCreateDayyOff getDataCreateDayyOff(){
		return dataCreateDayyOff;
	}

	@Override
 	public String toString(){
		return 
			"ResponseCreateDayyOff{" + 
			"errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' +
			",dataCreateDayyOff = '" + dataCreateDayyOff + '\'' +
			"}";
		}
}