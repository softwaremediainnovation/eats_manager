
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataCuti {

    @SerializedName("approver")
    private DataDetailCutiApprover mApprover;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("constanta")
    private DataDetailCutiConstanta mConstanta;
    @SerializedName("constantaID")
    private Long mConstantaID;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("dayOffID")
    private Long mDayOffID;
    @SerializedName("employee")
    private DataDetailCutiEmployee mEmployee;
    @SerializedName("employeeApprover")
    private DataDetailCutiEmployeeApprover mEmployeeApprover;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("end")
    private String mEnd;
    @SerializedName("howManyDays")
    private Long mHowManyDays;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("isDisease")
    private Boolean mIsDisease;
    @SerializedName("note")
    private String mNote;
    @SerializedName("filePath")
    private String mPath;
    @SerializedName("start")
    private String mStart;
    @SerializedName("organization")
    private OrganizationEmployee organization;

    public DataDetailCutiApprover getApprover() {
        return mApprover;
    }

    public void setApprover(DataDetailCutiApprover approver) {
        mApprover = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public DataDetailCutiConstanta getConstanta() {
        return mConstanta;
    }

    public void setConstanta(DataDetailCutiConstanta constanta) {
        mConstanta = constanta;
    }

    public Long getConstantaID() {
        return mConstantaID;
    }

    public void setConstantaID(Long constantaID) {
        mConstantaID = constantaID;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public Long getDayOffID() {
        return mDayOffID;
    }

    public void setDayOffID(Long dayOffID) {
        mDayOffID = dayOffID;
    }

    public DataDetailCutiEmployee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataDetailCutiEmployee employee) {
        mEmployee = employee;
    }

    public DataDetailCutiEmployeeApprover getEmployeeApprover() {
        return mEmployeeApprover;
    }

    public void setEmployeeApprover(DataDetailCutiEmployeeApprover employeeApprover) {
        mEmployeeApprover = employeeApprover;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getEnd() {
        return mEnd;
    }

    public void setEnd(String end) {
        mEnd = end;
    }

    public Long getHowManyDays() {
        return mHowManyDays;
    }

    public void setHowManyDays(Long howManyDays) {
        mHowManyDays = howManyDays;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public Boolean getIsDisease() {
        return mIsDisease;
    }

    public void setIsDisease(Boolean isDisease) {
        mIsDisease = isDisease;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getStart() {
        return mStart;
    }

    public void setStart(String start) {
        mStart = start;
    }

    public void setOrganization(OrganizationEmployee organization){
        this.organization = organization;
    }

    public OrganizationEmployee getOrganization(){
        return organization;
    }

}
