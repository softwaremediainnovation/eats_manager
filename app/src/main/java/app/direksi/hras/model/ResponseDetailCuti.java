
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseDetailCuti {

    @SerializedName("data")
    private DataCuti mData;
    @SerializedName("errCode")
    private Long mErrCode;
    @SerializedName("errMessage")
    private String mErrMessage;

    public DataCuti getData() {
        return mData;
    }

    public void setData(DataCuti data) {
        mData = data;
    }

    public Long getErrCode() {
        return mErrCode;
    }

    public void setErrCode(Long errCode) {
        mErrCode = errCode;
    }

    public String getErrMessage() {
        return mErrMessage;
    }

    public void setErrMessage(String errMessage) {
        mErrMessage = errMessage;
    }

}
