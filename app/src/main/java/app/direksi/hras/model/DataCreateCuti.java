package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataCreateCuti{

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("approver")
	@Expose
	private String approver;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("howManyDays")
	@Expose
	private int howManyDays;

	@SerializedName("isDisease")
	@Expose
	private String isDisease;

	@SerializedName("approverID")
	@Expose
	private Object approverID;

	@SerializedName("filePath")
	@Expose
	private String filePath;

	@SerializedName("constanta")
	@Expose
	private String constanta;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("dayOffFile")
	@Expose
	private String dayOffFile;

	@SerializedName("employee")
	@Expose
	private String employee;

	@SerializedName("constantaID")
	@Expose
	private int constantaID;

	@SerializedName("dateApproved")
	@Expose
	private String dateApproved;

	@SerializedName("isApprovedHtml")
	@Expose
	private String isApprovedHtml;

	@SerializedName("dayOffID")
	@Expose
	private int dayOffID;

	@SerializedName("isApproved")
	@Expose
	private String isApproved;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setApprover(String approver){
		this.approver = approver;
	}

	public String getApprover(){
		return approver;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setHowManyDays(int howManyDays){
		this.howManyDays = howManyDays;
	}

	public int getHowManyDays(){
		return howManyDays;
	}

	public void setIsDisease(String isDisease){
		this.isDisease = isDisease;
	}

	public String getIsDisease(){
		return isDisease;
	}

	public void setApproverID(Object approverID){
		this.approverID = approverID;
	}

	public Object getApproverID(){
		return approverID;
	}

	public void setFilePath(String filePath){
		this.filePath = filePath;
	}

	public String getFilePath(){
		return filePath;
	}

	public void setConstanta(String constanta){
		this.constanta = constanta;
	}

	public String getConstanta(){
		return constanta;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setDayOffFile(String dayOffFile){
		this.dayOffFile = dayOffFile;
	}

	public String getDayOffFile(){
		return dayOffFile;
	}

	public void setEmployee(String employee){
		this.employee = employee;
	}

	public String getEmployee(){
		return employee;
	}

	public void setConstantaID(int constantaID){
		this.constantaID = constantaID;
	}

	public int getConstantaID(){
		return constantaID;
	}

	public void setDateApproved(String dateApproved){
		this.dateApproved = dateApproved;
	}

	public String getDateApproved(){
		return dateApproved;
	}

	public void setIsApprovedHtml(String isApprovedHtml){
		this.isApprovedHtml = isApprovedHtml;
	}

	public String getIsApprovedHtml(){
		return isApprovedHtml;
	}

	public void setDayOffID(int dayOffID){
		this.dayOffID = dayOffID;
	}

	public int getDayOffID(){
		return dayOffID;
	}

	public void setIsApproved(String isApproved){
		this.isApproved = isApproved;
	}

	public String getIsApproved(){
		return isApproved;
	}

	@Override
 	public String toString(){
		return 
			"DataCreateCuti{" + 
			"date = '" + date + '\'' + 
			",approver = '" + approver + '\'' + 
			",note = '" + note + '\'' + 
			",howManyDays = '" + howManyDays + '\'' + 
			",isDisease = '" + isDisease + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",filePath = '" + filePath + '\'' + 
			",constanta = '" + constanta + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",dayOffFile = '" + dayOffFile + '\'' + 
			",employee = '" + employee + '\'' + 
			",constantaID = '" + constantaID + '\'' + 
			",dateApproved = '" + dateApproved + '\'' + 
			",isApprovedHtml = '" + isApprovedHtml + '\'' + 
			",dayOffID = '" + dayOffID + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			"}";
		}
}