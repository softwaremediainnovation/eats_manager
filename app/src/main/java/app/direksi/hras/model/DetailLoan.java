
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailLoan {

    @SerializedName("amount")
    private Double mAmount;
    @SerializedName("approverName")
    private String mApprover;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("datePaid")
    private String mDatePaid;
    @SerializedName("employeeName")
    private String employeeName;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("installment")
    private Long mInstallment;
    @SerializedName("installmentAmount")
    private Double mInstallmentAmount;
    @SerializedName("installmentPayOut")
    private Double mInstallmentPayOut;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("isApprovedHtml")
    private String mIsApprovedHtml;
    @SerializedName("isPaid")
    private Boolean mIsPaid;
    @SerializedName("isPbonMingguan")
    private Boolean mIsPbonMingguan;
    @SerializedName("isPbonMingguanHtml")
    private String mIsPbonMingguanHtml;
    @SerializedName("loanHtml")
    private String mLoanHtml;
    @SerializedName("loanID")
    private Long mLoanID;
    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("employee")
    private Employee mEmployee;

    public Double getAmount() {
        return mAmount;
    }

    public void setAmount(Double amount) {
        mAmount = amount;
    }

    public String getApprover() {
        return mApprover;
    }

    public void setApprover(String approver) {
        mApprover = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getDatePaid() {
        return mDatePaid;
    }

    public void setDatePaid(String datePaid) {
        mDatePaid = datePaid;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employee) {
        employeeName = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Long getInstallment() {
        return mInstallment;
    }

    public void setInstallment(Long installment) {
        mInstallment = installment;
    }

    public Double getInstallmentAmount() {
        return mInstallmentAmount;
    }

    public void setInstallmentAmount(Double installmentAmount) {
        mInstallmentAmount = installmentAmount;
    }

    public Double getInstallmentPayOut() {
        return mInstallmentPayOut;
    }

    public void setInstallmentPayOut(Double installmentPayOut) {
        mInstallmentPayOut = installmentPayOut;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public String getIsApprovedHtml() {
        return mIsApprovedHtml;
    }

    public void setIsApprovedHtml(String isApprovedHtml) {
        mIsApprovedHtml = isApprovedHtml;
    }

    public Boolean getIsPaid() {
        return mIsPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        mIsPaid = isPaid;
    }

    public Boolean getIsPbonMingguan() {
        return mIsPbonMingguan;
    }

    public void setIsPbonMingguan(Boolean isPbonMingguan) {
        mIsPbonMingguan = isPbonMingguan;
    }

    public String getIsPbonMingguanHtml() {
        return mIsPbonMingguanHtml;
    }

    public void setIsPbonMingguanHtml(String isPbonMingguanHtml) {
        mIsPbonMingguanHtml = isPbonMingguanHtml;
    }

    public String getLoanHtml() {
        return mLoanHtml;
    }

    public void setLoanHtml(String loanHtml) {
        mLoanHtml = loanHtml;
    }

    public Long getLoanID() {
        return mLoanID;
    }

    public void setLoanID(Long loanID) {
        mLoanID = loanID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Employee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Employee employee) {
        mEmployee = employee;
    }

}
