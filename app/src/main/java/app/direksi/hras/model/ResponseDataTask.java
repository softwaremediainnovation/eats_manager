package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDataTask{

	@SerializedName("data")
	private DataTaskSummary data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public DataTaskSummary getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}