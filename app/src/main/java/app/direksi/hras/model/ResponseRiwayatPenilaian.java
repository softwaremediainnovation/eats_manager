package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseRiwayatPenilaian {

	@SerializedName("data")
	private List<DataRiwayatPenilaian> data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public List<DataRiwayatPenilaian> getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}