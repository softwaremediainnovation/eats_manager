package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RemuConstanta {

	@SerializedName("unit")
	private String unit;

	@SerializedName("creator")
	private Object creator;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("dayOffs")
	private List<Object> dayOffs;

	@SerializedName("constantaCategory")
	private Object constantaCategory;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("name")
	private String name;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("constantaID")
	private int constantaID;

	@SerializedName("constantaCategoryID")
	private int constantaCategoryID;

	@SerializedName("value")
	private String value;

	public String getUnit(){
		return unit;
	}

	public Object getCreator(){
		return creator;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public List<Object> getDayOffs(){
		return dayOffs;
	}

	public Object getConstantaCategory(){
		return constantaCategory;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public String getName(){
		return name;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public int getConstantaID(){
		return constantaID;
	}

	public int getConstantaCategoryID(){
		return constantaCategoryID;
	}

	public String getValue(){
		return value;
	}
}