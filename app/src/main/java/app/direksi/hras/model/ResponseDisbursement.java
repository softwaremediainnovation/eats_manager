package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDisbursement{

	public ResponseDisbursement(String external_id, long amount, String bank_code, String accountHolderName, String account_number, String description, List<String> email_to, List<String> email_cc) {
		this.externalId = external_id;
		this.amount = amount;
		this.bankCode = bank_code;
		this.accountHolderName = accountHolderName;
		this.accountNumber = account_number;
		this.description = description;
		this.email_to = email_to;
        this.email_cc = email_cc;
	}




	@SerializedName("email_to")
	private List<String> email_to;

    @SerializedName("email_cc")
    private List<String> email_cc;

	@SerializedName("bank_code")
	private String bankCode;

	@SerializedName("account_number")
	private String accountNumber;

	@SerializedName("amount")
	private long amount;

	@SerializedName("user_id")
	private String userId;

	@SerializedName("account_holder_name")
	private String accountHolderName;

	@SerializedName("external_id")
	private String externalId;

	@SerializedName("error_code")
	private String errorCode;

	@SerializedName("id")
	private String id;

	@SerializedName("disbursement_description")
	private String disbursementDescription;

	@SerializedName("description")
	private String description;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private String status;



	public void setEmailTo(List<String> emailTo){
		this.email_to = emailTo;
	}

	public List<String> getEmailTo(){
		return email_to;
	}

    public void setEmail_cc(List<String> emailTo){
        this.email_cc = emailTo;
    }

    public List<String> getEmailCc(){
        return email_cc;
    }

	public void setBankCode(String bankCode){
		this.bankCode = bankCode;
	}

	public String getBankCode(){
		return bankCode;
	}

	public void setAccountNumber(String bankCode){
		this.accountNumber = bankCode;
	}

	public String getAccountNumber(){
		return accountNumber;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public long getAmount(){
		return amount;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setAccountHolderName(String accountHolderName){
		this.accountHolderName = accountHolderName;
	}

	public String getAccountHolderName(){
		return accountHolderName;
	}

	public void setExternalId(String externalId){
		this.externalId = externalId;
	}

	public String getExternalId(){
		return externalId;
	}

	public void setErrorCode(String errorCode){
		this.errorCode = errorCode;
	}

	public String getErrorCode(){
		return errorCode;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDisbursementDescription(String disbursementDescription){
		this.disbursementDescription = disbursementDescription;
	}

	public String getDisbursementDescription(){
		return disbursementDescription;
	}

	public void setDescription(String disbursementDescription){
		this.description = disbursementDescription;
	}

	public String getDescription(){
		return description;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDisbursement{" + 
			"bank_code = '" + bankCode + '\'' + 
			",amount = '" + amount + '\'' + 
			",user_id = '" + userId + '\'' + 
			",account_holder_name = '" + accountHolderName + '\'' + 
			",external_id = '" + externalId + '\'' + 
			",error_code = '" + errorCode + '\'' + 
			",id = '" + id + '\'' + 
			",disbursement_description = '" + disbursementDescription + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}