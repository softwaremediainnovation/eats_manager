
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseDataEmployeeIDData {

    @SerializedName("address")
    private String mAddress;
    @SerializedName("applicationUser")
    private ResponseDataEmployeeIDApplicationUser mApplicationUser;
    @SerializedName("deviceID")
    private String mDeviceID;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("fullNameNik")
    private String mFullNameNik;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("idCardNumber")
    private String mIdCardNumber;
    @SerializedName("imageFile")
    private Object mImageFile;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("isLeader")
    private Boolean mIsLeader;
    @SerializedName("joinDate")
    private String mJoinDate;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("nik")
    private String mNik;
    @SerializedName("organization")
    private ResponseDataEmployeeIDOrganization mOrganization;
    @SerializedName("organizationID")
    private Long mOrganizationID;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("religion")
    private String mReligion;
    @SerializedName("userID")
    private String mUserID;
    @SerializedName("contractExpiredDate")
    private String mContractExpiredDate;
    @SerializedName("note")
    private String mNotes;
    @SerializedName("sisaCuti")
    private Long sisaCuti;
    @SerializedName("bankName")
    private String mBankName;
    @SerializedName("bankNumber")
    private String mBankNumber;
    @SerializedName("bankAccountName")
    private String mBankAccountName;
    @SerializedName("keyXendit")
    private String mkeyXendit;

    public String getKeyXendit() {
        return mkeyXendit;
    }

    public void setKeyXendit(String address) {
        mkeyXendit = address;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public ResponseDataEmployeeIDApplicationUser getApplicationUser() {
        return mApplicationUser;
    }

    public void setApplicationUser(ResponseDataEmployeeIDApplicationUser applicationUser) {
        mApplicationUser = applicationUser;
    }

    public String getDeviceID() {
        return mDeviceID;
    }

    public void setDeviceID(String deviceID) {
        mDeviceID = deviceID;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getFullNameNik() {
        return mFullNameNik;
    }

    public void setFullNameNik(String fullNameNik) {
        mFullNameNik = fullNameNik;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getIdCardNumber() {
        return mIdCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        mIdCardNumber = idCardNumber;
    }

    public Object getImageFile() {
        return mImageFile;
    }

    public void setImageFile(Object imageFile) {
        mImageFile = imageFile;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public Boolean getIsLeader() {
        return mIsLeader;
    }

    public void setIsLeader(Boolean isLeader) {
        mIsLeader = isLeader;
    }

    public String getJoinDate() {
        return mJoinDate;
    }

    public void setJoinDate(String joinDate) {
        mJoinDate = joinDate;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getNik() {
        return mNik;
    }

    public void setNik(String nik) {
        mNik = nik;
    }

    public ResponseDataEmployeeIDOrganization getOrganization() {
        return mOrganization;
    }

    public void setOrganization(ResponseDataEmployeeIDOrganization organization) {
        mOrganization = organization;
    }

    public Long getOrganizationID() {
        return mOrganizationID;
    }

    public void setOrganizationID(Long organizationID) {
        mOrganizationID = organizationID;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public String getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = religion;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public String getContractExpiredDate() {
        return mContractExpiredDate;
    }

    public void setContractExpiredDate(String contractExpiredDate) {
        mContractExpiredDate = contractExpiredDate;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String contractExpiredDate) {
        mNotes = contractExpiredDate;
    }

    public Long getSisaCuti() {
        return sisaCuti;
    }

    public void setSisaCuti(Long employeeID) {
        sisaCuti = employeeID;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String contractExpiredDate) {
        mBankName = contractExpiredDate;
    }

    public String getBankNumber() {
        return mBankNumber;
    }

    public void setBankNumber(String contractExpiredDate) {
        mBankNumber = contractExpiredDate;
    }

    public String getBankAccountName() {
        return mBankAccountName;
    }

    public void setBankAccountName(String contractExpiredDate) {
        mBankAccountName = contractExpiredDate;
    }

}
