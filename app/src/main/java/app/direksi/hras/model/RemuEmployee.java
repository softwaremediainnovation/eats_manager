package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RemuEmployee {

	@SerializedName("remunerations")
	private List<RemunerationsItem> remunerations;

	@SerializedName("contractExpiredDate")
	private Object contractExpiredDate;

	@SerializedName("resignDate")
	private Object resignDate;

	@SerializedName("statusOfResidence")
	private Object statusOfResidence;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("organizationRules")
	private Object organizationRules;

	@SerializedName("userID")
	private String userID;

	@SerializedName("provinceID")
	private int provinceID;

	@SerializedName("isUsingGlasses")
	private boolean isUsingGlasses;

	@SerializedName("nik")
	private String nik;

	@SerializedName("toleransi")
	private int toleransi;

	@SerializedName("applicationUser")
	private Object applicationUser;

	@SerializedName("photoHtml")
	private String photoHtml;

	@SerializedName("joinDate")
	private Object joinDate;

	@SerializedName("province")
	private Object province;

	@SerializedName("isShift")
	private boolean isShift;

	@SerializedName("fullNameNikHtml")
	private String fullNameNikHtml;

	@SerializedName("overtimes")
	private Object overtimes;

	@SerializedName("expectedSalary")
	private Object expectedSalary;

	@SerializedName("fullNameNik")
	private String fullNameNik;

	@SerializedName("height")
	private Object height;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("contactEmergencyPhone")
	private Object contactEmergencyPhone;

	@SerializedName("isActiveHtml")
	private String isActiveHtml;

	@SerializedName("serialNumber")
	private Object serialNumber;

	@SerializedName("generalRules")
	private Object generalRules;

	@SerializedName("loans")
	private Object loans;

	@SerializedName("weight")
	private Object weight;

	@SerializedName("cityID")
	private int cityID;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("diseaseHistory")
	private Object diseaseHistory;

	@SerializedName("employeeRelatives")
	private Object employeeRelatives;

	@SerializedName("numberOfChildren")
	private String numberOfChildren;

	@SerializedName("nationality")
	private String nationality;

	@SerializedName("phone")
	private String phone;

	@SerializedName("dob")
	private String dob;

	@SerializedName("idCardNumber")
	private String idCardNumber;

	@SerializedName("canEditProfile")
	private boolean canEditProfile;

	@SerializedName("fullNameNikHtmlWithDetail")
	private String fullNameNikHtmlWithDetail;

	@SerializedName("maritalStatus")
	private String maritalStatus;

	@SerializedName("employeeEmployments")
	private Object employeeEmployments;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("note")
	private String note;

	@SerializedName("gender")
	private String gender;

	@SerializedName("assetTrackingOriginHolders")
	private Object assetTrackingOriginHolders;

	@SerializedName("city")
	private Object city;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("bankName")
	private String bankName;

	@SerializedName("bloodType")
	private String bloodType;

	@SerializedName("reprimands")
	private Object reprimands;

	@SerializedName("memberships")
	private List<RemuMembershipsItem> memberships;

	@SerializedName("attendances")
	private Object attendances;

	@SerializedName("cutOffEmployees")
	private List<RemuCutOffEmployeesItem> cutOffEmployees;

	@SerializedName("weakness")
	private Object weakness;

	@SerializedName("photoUrl")
	private String photoUrl;

	@SerializedName("contactEmergencyName")
	private Object contactEmergencyName;

	@SerializedName("imageFile")
	private Object imageFile;

	@SerializedName("employeeCertificates")
	private Object employeeCertificates;

	@SerializedName("driverLicenseType")
	private String driverLicenseType;

	@SerializedName("employeeEducations")
	private Object employeeEducations;

	@SerializedName("email")
	private String email;

	@SerializedName("bankAccountName")
	private String bankAccountName;

	@SerializedName("bankNumber")
	private String bankNumber;

	@SerializedName("address")
	private String address;

	@SerializedName("dayOffs")
	private List<Object> dayOffs;

	@SerializedName("roleId")
	private Object roleId;

	@SerializedName("fullName")
	private String fullName;

	@SerializedName("fullNameEmail")
	private String fullNameEmail;

	@SerializedName("reimbursements")
	private Object reimbursements;

	@SerializedName("typeOfVehicle")
	private String typeOfVehicle;

	@SerializedName("employeeOrganizations")
	private Object employeeOrganizations;

	@SerializedName("deviceID")
	private String deviceID;

	@SerializedName("religion")
	private String religion;

	@SerializedName("assetTrackingDestinationHolders")
	private Object assetTrackingDestinationHolders;

	@SerializedName("activities")
	private Object activities;

	@SerializedName("strengthness")
	private Object strengthness;

	public List<RemunerationsItem> getRemunerations(){
		return remunerations;
	}

	public Object getContractExpiredDate(){
		return contractExpiredDate;
	}

	public Object getResignDate(){
		return resignDate;
	}

	public Object getStatusOfResidence(){
		return statusOfResidence;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public Object getOrganizationRules(){
		return organizationRules;
	}

	public String getUserID(){
		return userID;
	}

	public int getProvinceID(){
		return provinceID;
	}

	public boolean isIsUsingGlasses(){
		return isUsingGlasses;
	}

	public String getNik(){
		return nik;
	}

	public int getToleransi(){
		return toleransi;
	}

	public Object getApplicationUser(){
		return applicationUser;
	}

	public String getPhotoHtml(){
		return photoHtml;
	}

	public Object getJoinDate(){
		return joinDate;
	}

	public Object getProvince(){
		return province;
	}

	public boolean isIsShift(){
		return isShift;
	}

	public String getFullNameNikHtml(){
		return fullNameNikHtml;
	}

	public Object getOvertimes(){
		return overtimes;
	}

	public Object getExpectedSalary(){
		return expectedSalary;
	}

	public String getFullNameNik(){
		return fullNameNik;
	}

	public Object getHeight(){
		return height;
	}

	public String getLongitude(){
		return longitude;
	}

	public Object getContactEmergencyPhone(){
		return contactEmergencyPhone;
	}

	public String getIsActiveHtml(){
		return isActiveHtml;
	}

	public Object getSerialNumber(){
		return serialNumber;
	}

	public Object getGeneralRules(){
		return generalRules;
	}

	public Object getLoans(){
		return loans;
	}

	public Object getWeight(){
		return weight;
	}

	public int getCityID(){
		return cityID;
	}

	public String getFirstName(){
		return firstName;
	}

	public Object getDiseaseHistory(){
		return diseaseHistory;
	}

	public Object getEmployeeRelatives(){
		return employeeRelatives;
	}

	public String getNumberOfChildren(){
		return numberOfChildren;
	}

	public String getNationality(){
		return nationality;
	}

	public String getPhone(){
		return phone;
	}

	public String getDob(){
		return dob;
	}

	public String getIdCardNumber(){
		return idCardNumber;
	}

	public boolean isCanEditProfile(){
		return canEditProfile;
	}

	public String getFullNameNikHtmlWithDetail(){
		return fullNameNikHtmlWithDetail;
	}

	public String getMaritalStatus(){
		return maritalStatus;
	}

	public Object getEmployeeEmployments(){
		return employeeEmployments;
	}

	public String getLastName(){
		return lastName;
	}

	public String getNote(){
		return note;
	}

	public String getGender(){
		return gender;
	}

	public Object getAssetTrackingOriginHolders(){
		return assetTrackingOriginHolders;
	}

	public Object getCity(){
		return city;
	}

	public String getLatitude(){
		return latitude;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public String getBankName(){
		return bankName;
	}

	public String getBloodType(){
		return bloodType;
	}

	public Object getReprimands(){
		return reprimands;
	}

	public List<RemuMembershipsItem> getMemberships(){
		return memberships;
	}

	public Object getAttendances(){
		return attendances;
	}

	public List<RemuCutOffEmployeesItem> getCutOffEmployees(){
		return cutOffEmployees;
	}

	public Object getWeakness(){
		return weakness;
	}

	public String getPhotoUrl(){
		return photoUrl;
	}

	public Object getContactEmergencyName(){
		return contactEmergencyName;
	}

	public Object getImageFile(){
		return imageFile;
	}

	public Object getEmployeeCertificates(){
		return employeeCertificates;
	}

	public String getDriverLicenseType(){
		return driverLicenseType;
	}

	public Object getEmployeeEducations(){
		return employeeEducations;
	}

	public String getEmail(){
		return email;
	}

	public String getBankAccountName(){
		return bankAccountName;
	}

	public String getBankNumber(){
		return bankNumber;
	}

	public String getAddress(){
		return address;
	}

	public List<Object> getDayOffs(){
		return dayOffs;
	}

	public Object getRoleId(){
		return roleId;
	}

	public String getFullName(){
		return fullName;
	}

	public String getFullNameEmail(){
		return fullNameEmail;
	}

	public Object getReimbursements(){
		return reimbursements;
	}

	public String getTypeOfVehicle(){
		return typeOfVehicle;
	}

	public Object getEmployeeOrganizations(){
		return employeeOrganizations;
	}

	public String getDeviceID(){
		return deviceID;
	}

	public String getReligion(){
		return religion;
	}

	public Object getAssetTrackingDestinationHolders(){
		return assetTrackingDestinationHolders;
	}

	public Object getActivities(){
		return activities;
	}

	public Object getStrengthness(){
		return strengthness;
	}
}