package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDetailRequestAttendance{

	@SerializedName("data")
	private List<DataDetailRequestAttendance> data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public void setData(List<DataDetailRequestAttendance> data){
		this.data = data;
	}

	public List<DataDetailRequestAttendance> getData(){
		return data;
	}

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDetailRequestAttendance{" + 
			"data = '" + data + '\'' + 
			",errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}