
package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class CreatorMyAssetList {

    @SerializedName("accessFailedCount")
    private Long mAccessFailedCount;
    @SerializedName("additionalHolidayCreators")
    private Object mAdditionalHolidayCreators;
    @SerializedName("articleCreators")
    private Object mArticleCreators;
    @SerializedName("assetCreators")
    private List<AssetCreatorMyAssetList> mAssetCreators;
    @SerializedName("assetDisposers")
    private Object mAssetDisposers;
    @SerializedName("concurrencyStamp")
    private String mConcurrencyStamp;
    @SerializedName("constantaCreators")
    private Object mConstantaCreators;
    @SerializedName("dayOffApprovers")
    private Object mDayOffApprovers;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("emailConfirmed")
    private Boolean mEmailConfirmed;
    @SerializedName("emailConfirmedHtml")
    private String mEmailConfirmedHtml;
    @SerializedName("employee")
    private Object mEmployee;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("fullName")
    private String mFullName;
    @SerializedName("fullNameEmailPhoneHtml")
    private String mFullNameEmailPhoneHtml;
    @SerializedName("generalRuleSetterPermissions")
    private Object mGeneralRuleSetterPermissions;
    @SerializedName("holidayMasterCreators")
    private Object mHolidayMasterCreators;
    @SerializedName("id")
    private String mId;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("loanApprovers")
    private Object mLoanApprovers;
    @SerializedName("lockoutEnabled")
    private Boolean mLockoutEnabled;
    @SerializedName("lockoutEnd")
    private Object mLockoutEnd;
    @SerializedName("normalizedEmail")
    private String mNormalizedEmail;
    @SerializedName("normalizedUserName")
    private String mNormalizedUserName;
    @SerializedName("organizationRuleSetterPermissions")
    private Object mOrganizationRuleSetterPermissions;
    @SerializedName("overtimeApprovers")
    private Object mOvertimeApprovers;
    @SerializedName("passwordHash")
    private String mPasswordHash;
    @SerializedName("payrollCheckers")
    private Object mPayrollCheckers;
    @SerializedName("phoneNumber")
    private String mPhoneNumber;
    @SerializedName("phoneNumberConfirmed")
    private Boolean mPhoneNumberConfirmed;
    @SerializedName("photoHtml")
    private String mPhotoHtml;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("position")
    private Object mPosition;
    @SerializedName("positionID")
    private Long mPositionID;
    @SerializedName("publicHolidayCreators")
    private Object mPublicHolidayCreators;
    @SerializedName("queryBuilderCreators")
    private Object mQueryBuilderCreators;
    @SerializedName("reimbursemenApprovers")
    private Object mReimbursemenApprovers;
    @SerializedName("remunerationFormulaCreators")
    private Object mRemunerationFormulaCreators;
    @SerializedName("remunerationNominalCreators")
    private Object mRemunerationNominalCreators;
    @SerializedName("reprimandIssuers")
    private Object mReprimandIssuers;
    @SerializedName("securityStamp")
    private String mSecurityStamp;
    @SerializedName("signinToMobile")
    private Boolean mSigninToMobile;
    @SerializedName("twoFactorEnabled")
    private Boolean mTwoFactorEnabled;
    @SerializedName("userName")
    private String mUserName;
    @SerializedName("workingHourCreators")
    private Object mWorkingHourCreators;

    public Long getAccessFailedCount() {
        return mAccessFailedCount;
    }

    public void setAccessFailedCount(Long accessFailedCount) {
        mAccessFailedCount = accessFailedCount;
    }

    public Object getAdditionalHolidayCreators() {
        return mAdditionalHolidayCreators;
    }

    public void setAdditionalHolidayCreators(Object additionalHolidayCreators) {
        mAdditionalHolidayCreators = additionalHolidayCreators;
    }

    public Object getArticleCreators() {
        return mArticleCreators;
    }

    public void setArticleCreators(Object articleCreators) {
        mArticleCreators = articleCreators;
    }

    public List<AssetCreatorMyAssetList> getAssetCreators() {
        return mAssetCreators;
    }

    public void setAssetCreators(List<AssetCreatorMyAssetList> assetCreators) {
        mAssetCreators = assetCreators;
    }

    public Object getAssetDisposers() {
        return mAssetDisposers;
    }

    public void setAssetDisposers(Object assetDisposers) {
        mAssetDisposers = assetDisposers;
    }

    public String getConcurrencyStamp() {
        return mConcurrencyStamp;
    }

    public void setConcurrencyStamp(String concurrencyStamp) {
        mConcurrencyStamp = concurrencyStamp;
    }

    public Object getConstantaCreators() {
        return mConstantaCreators;
    }

    public void setConstantaCreators(Object constantaCreators) {
        mConstantaCreators = constantaCreators;
    }

    public Object getDayOffApprovers() {
        return mDayOffApprovers;
    }

    public void setDayOffApprovers(Object dayOffApprovers) {
        mDayOffApprovers = dayOffApprovers;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Boolean getEmailConfirmed() {
        return mEmailConfirmed;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        mEmailConfirmed = emailConfirmed;
    }

    public String getEmailConfirmedHtml() {
        return mEmailConfirmedHtml;
    }

    public void setEmailConfirmedHtml(String emailConfirmedHtml) {
        mEmailConfirmedHtml = emailConfirmedHtml;
    }

    public Object getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Object employee) {
        mEmployee = employee;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String fullName) {
        mFullName = fullName;
    }

    public String getFullNameEmailPhoneHtml() {
        return mFullNameEmailPhoneHtml;
    }

    public void setFullNameEmailPhoneHtml(String fullNameEmailPhoneHtml) {
        mFullNameEmailPhoneHtml = fullNameEmailPhoneHtml;
    }

    public Object getGeneralRuleSetterPermissions() {
        return mGeneralRuleSetterPermissions;
    }

    public void setGeneralRuleSetterPermissions(Object generalRuleSetterPermissions) {
        mGeneralRuleSetterPermissions = generalRuleSetterPermissions;
    }

    public Object getHolidayMasterCreators() {
        return mHolidayMasterCreators;
    }

    public void setHolidayMasterCreators(Object holidayMasterCreators) {
        mHolidayMasterCreators = holidayMasterCreators;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Object getLoanApprovers() {
        return mLoanApprovers;
    }

    public void setLoanApprovers(Object loanApprovers) {
        mLoanApprovers = loanApprovers;
    }

    public Boolean getLockoutEnabled() {
        return mLockoutEnabled;
    }

    public void setLockoutEnabled(Boolean lockoutEnabled) {
        mLockoutEnabled = lockoutEnabled;
    }

    public Object getLockoutEnd() {
        return mLockoutEnd;
    }

    public void setLockoutEnd(Object lockoutEnd) {
        mLockoutEnd = lockoutEnd;
    }

    public String getNormalizedEmail() {
        return mNormalizedEmail;
    }

    public void setNormalizedEmail(String normalizedEmail) {
        mNormalizedEmail = normalizedEmail;
    }

    public String getNormalizedUserName() {
        return mNormalizedUserName;
    }

    public void setNormalizedUserName(String normalizedUserName) {
        mNormalizedUserName = normalizedUserName;
    }

    public Object getOrganizationRuleSetterPermissions() {
        return mOrganizationRuleSetterPermissions;
    }

    public void setOrganizationRuleSetterPermissions(Object organizationRuleSetterPermissions) {
        mOrganizationRuleSetterPermissions = organizationRuleSetterPermissions;
    }

    public Object getOvertimeApprovers() {
        return mOvertimeApprovers;
    }

    public void setOvertimeApprovers(Object overtimeApprovers) {
        mOvertimeApprovers = overtimeApprovers;
    }

    public String getPasswordHash() {
        return mPasswordHash;
    }

    public void setPasswordHash(String passwordHash) {
        mPasswordHash = passwordHash;
    }

    public Object getPayrollCheckers() {
        return mPayrollCheckers;
    }

    public void setPayrollCheckers(Object payrollCheckers) {
        mPayrollCheckers = payrollCheckers;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public Boolean getPhoneNumberConfirmed() {
        return mPhoneNumberConfirmed;
    }

    public void setPhoneNumberConfirmed(Boolean phoneNumberConfirmed) {
        mPhoneNumberConfirmed = phoneNumberConfirmed;
    }

    public String getPhotoHtml() {
        return mPhotoHtml;
    }

    public void setPhotoHtml(String photoHtml) {
        mPhotoHtml = photoHtml;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public Object getPosition() {
        return mPosition;
    }

    public void setPosition(Object position) {
        mPosition = position;
    }

    public Long getPositionID() {
        return mPositionID;
    }

    public void setPositionID(Long positionID) {
        mPositionID = positionID;
    }

    public Object getPublicHolidayCreators() {
        return mPublicHolidayCreators;
    }

    public void setPublicHolidayCreators(Object publicHolidayCreators) {
        mPublicHolidayCreators = publicHolidayCreators;
    }

    public Object getQueryBuilderCreators() {
        return mQueryBuilderCreators;
    }

    public void setQueryBuilderCreators(Object queryBuilderCreators) {
        mQueryBuilderCreators = queryBuilderCreators;
    }

    public Object getReimbursemenApprovers() {
        return mReimbursemenApprovers;
    }

    public void setReimbursemenApprovers(Object reimbursemenApprovers) {
        mReimbursemenApprovers = reimbursemenApprovers;
    }

    public Object getRemunerationFormulaCreators() {
        return mRemunerationFormulaCreators;
    }

    public void setRemunerationFormulaCreators(Object remunerationFormulaCreators) {
        mRemunerationFormulaCreators = remunerationFormulaCreators;
    }

    public Object getRemunerationNominalCreators() {
        return mRemunerationNominalCreators;
    }

    public void setRemunerationNominalCreators(Object remunerationNominalCreators) {
        mRemunerationNominalCreators = remunerationNominalCreators;
    }

    public Object getReprimandIssuers() {
        return mReprimandIssuers;
    }

    public void setReprimandIssuers(Object reprimandIssuers) {
        mReprimandIssuers = reprimandIssuers;
    }

    public String getSecurityStamp() {
        return mSecurityStamp;
    }

    public void setSecurityStamp(String securityStamp) {
        mSecurityStamp = securityStamp;
    }

    public Boolean getSigninToMobile() {
        return mSigninToMobile;
    }

    public void setSigninToMobile(Boolean signinToMobile) {
        mSigninToMobile = signinToMobile;
    }

    public Boolean getTwoFactorEnabled() {
        return mTwoFactorEnabled;
    }

    public void setTwoFactorEnabled(Boolean twoFactorEnabled) {
        mTwoFactorEnabled = twoFactorEnabled;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public Object getWorkingHourCreators() {
        return mWorkingHourCreators;
    }

    public void setWorkingHourCreators(Object workingHourCreators) {
        mWorkingHourCreators = workingHourCreators;
    }

}
