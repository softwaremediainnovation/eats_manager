package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseDataKeyXendit {

	@SerializedName("data")
	private DataKeyXendit dataKeyXendit;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public DataKeyXendit getData(){
		return dataKeyXendit;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}