
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataClaim {

    @SerializedName("buktiKasKeluarDetailID")
    private String buktiKasKeluarDetailID;
    @SerializedName("keyXendit")
    private String mKeyXendit;
    @SerializedName("amount")
    private Long mAmount;
    @SerializedName("approver")
    private DataDetailClaimApprover mApprover;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("employee")
    private DataDetailClaimEmployee mEmployee;
    @SerializedName("employeeApprover")
    private DataDetailClaimEmmployeeApprover mEmployeeApprover;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reimbursementCategory")
    private DataDetailClaimReimburstmentCategory mReimbursementCategory;
    @SerializedName("reimbursementCategoryID")
    private Long mReimbursementCategoryID;
    @SerializedName("reimbursementID")
    private Long mReimbursementID;
    @SerializedName("kasbonID")
    private Long mKasbonID;
    @SerializedName("activityID")
    private Long mActivityID;
    @SerializedName("organization")
    private String organization;
    @SerializedName("isCair")
    private Boolean isCair;
    @SerializedName("dateCair")
    private String dateCair;
    @SerializedName("userCair")
    private String userCair;
    @SerializedName("methodCair")
    private String methodCair;
    @SerializedName("bankName")
    private String mBankName;
    @SerializedName("bankNumber")
    private String mBankNumber;
    @SerializedName("bankAccountName")
    private String mBankAccountName;
    @SerializedName("xendit")
    private List<XenditItem> xendit;
    @SerializedName("checked")
    private Boolean mChecked;


    public void setXendit(List<XenditItem> xendit){
        this.xendit = xendit;
    }

    public List<XenditItem> getXendit(){
        return xendit;
    }

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long amount) {
        mAmount = amount;
    }

    public DataDetailClaimApprover getApprover() {
        return mApprover;
    }

    public void setApprover(DataDetailClaimApprover approver) {
        mApprover = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public DataDetailClaimEmployee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataDetailClaimEmployee employee) {
        mEmployee = employee;
    }

    public DataDetailClaimEmmployeeApprover getEmployeeApprover() {
        return mEmployeeApprover;
    }

    public void setEmployeeApprover(DataDetailClaimEmmployeeApprover employeeApprover) {
        mEmployeeApprover = employeeApprover;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public DataDetailClaimReimburstmentCategory getReimbursementCategory() {
        return mReimbursementCategory;
    }

    public void setReimbursementCategory(DataDetailClaimReimburstmentCategory reimbursementCategory) {
        mReimbursementCategory = reimbursementCategory;
    }

    public Long getReimbursementCategoryID() {
        return mReimbursementCategoryID;
    }

    public void setReimbursementCategoryID(Long reimbursementCategoryID) {
        mReimbursementCategoryID = reimbursementCategoryID;
    }

    public Long getReimbursementID() {
        return mReimbursementID;
    }

    public void setReimbursementID(Long reimbursementID) {
        mReimbursementID = reimbursementID;
    }

    public Long getKasbonID() {
        return mKasbonID;
    }

    public void setKasbonID(Long reimbursementID) {
        mKasbonID = reimbursementID;
    }

    public Long getActivityID() {
        return mActivityID;
    }

    public void setActivityID(Long reimbursementID) {
        mActivityID = reimbursementID;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String note) {
        organization = note;
    }

    public void setIsCair(Boolean isPaid) {
        isCair = isPaid;
    }

    public Boolean getIsCair() {
        return isCair;
    }

    public String getDateCair() {
        return dateCair;
    }

    public void setDateCair(String note) {
        dateCair = note;
    }

    public String getUserCair() {
        return userCair;
    }

    public void setUserCair(String note) {
        userCair = note;
    }

    public String getMethodCair() {
        return methodCair;
    }

    public void setMethodCair(String note) {
        methodCair = note;
    }

    public String getBankName() {
        return mBankName;
    }

    public void setBankName(String contractExpiredDate) {
        mBankName = contractExpiredDate;
    }

    public String getBankNumber() {
        return mBankNumber;
    }

    public void setBankNumber(String contractExpiredDate) {
        mBankNumber = contractExpiredDate;
    }

    public String getBankAccountName() {
        return mBankAccountName;
    }

    public void setBankAccountName(String contractExpiredDate) {
        mBankAccountName = contractExpiredDate;
    }

    public String getKeyXendit() {
        return mKeyXendit;
    }

    public void setKeyXendit(String xendit) {
        mKeyXendit = xendit;
    }

    public Boolean getChecked() {
        return mChecked;
    }

    public void setChecked(Boolean contractExpiredDate) {
        mChecked = contractExpiredDate;
    }

    public String getBuktiKasKeluarDetailID() {
        return buktiKasKeluarDetailID;
    }

    public void setBuktiKasKeluarDetailID(String bkk) {
        buktiKasKeluarDetailID = bkk;
    }

}
