package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDetailLoanWithDetail{

	@SerializedName("data")
	private DetailLoanWithDetailData data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public void setData(DetailLoanWithDetailData data){
		this.data = data;
	}

	public DetailLoanWithDetailData getData(){
		return data;
	}

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDetailLoanWithDetail{" + 
			"data = '" + data + '\'' + 
			",errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}