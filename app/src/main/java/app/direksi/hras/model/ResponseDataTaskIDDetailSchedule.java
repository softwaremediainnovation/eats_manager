package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class ResponseDataTaskIDDetailSchedule {

	@SerializedName("data")
	private List<DataTaskIDDetailSchedule> data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public void setData(List<DataTaskIDDetailSchedule> data){
		this.data = data;
	}

	public List<DataTaskIDDetailSchedule> getData(){
		return data;
	}

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseDataTaskIDDetail{" + 
			"data = '" + data + '\'' + 
			",errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}