package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataTaskIDHeader {

	@SerializedName("note")
	private String note;

	@SerializedName("dueDate")
	private String dueDate;

	@SerializedName("creatorID")
	private int creatorID;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("isFinish")
	private boolean isFinish;

	@SerializedName("dateFinish")
	private String dateFinish;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("updatedByID")
	private int updatedByID;

	@SerializedName("dateUpdated")
	private String dateUpdated;

	@SerializedName("taskHeaderID")
	private int taskHeaderID;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("taskName")
	private String taskName;

	@SerializedName("taskDetails")
	private Object taskDetails;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setDueDate(String dueDate){
		this.dueDate = dueDate;
	}

	public String getDueDate(){
		return dueDate;
	}

	public void setCreatorID(int creatorID){
		this.creatorID = creatorID;
	}

	public int getCreatorID(){
		return creatorID;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setIsFinish(boolean isFinish){
		this.isFinish = isFinish;
	}

	public boolean isIsFinish(){
		return isFinish;
	}

	public void setDateFinish(String dateFinish){
		this.dateFinish = dateFinish;
	}

	public String getDateFinish(){
		return dateFinish;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUpdatedByID(int updatedByID){
		this.updatedByID = updatedByID;
	}

	public int getUpdatedByID(){
		return updatedByID;
	}

	public void setDateUpdated(String dateUpdated){
		this.dateUpdated = dateUpdated;
	}

	public String getDateUpdated(){
		return dateUpdated;
	}

	public void setTaskHeaderID(int taskHeaderID){
		this.taskHeaderID = taskHeaderID;
	}

	public int getTaskHeaderID(){
		return taskHeaderID;
	}

	public void setDateCreated(String dateCreated){
		this.dateCreated = dateCreated;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public void setTaskName(String taskName){
		this.taskName = taskName;
	}

	public String getTaskName(){
		return taskName;
	}

	public void setTaskDetails(Object taskDetails){
		this.taskDetails = taskDetails;
	}

	public Object getTaskDetails(){
		return taskDetails;
	}

	@Override
 	public String toString(){
		return 
			"Header{" + 
			"note = '" + note + '\'' + 
			",dueDate = '" + dueDate + '\'' + 
			",creatorID = '" + creatorID + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",isFinish = '" + isFinish + '\'' + 
			",dateFinish = '" + dateFinish + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",updatedByID = '" + updatedByID + '\'' + 
			",dateUpdated = '" + dateUpdated + '\'' + 
			",taskHeaderID = '" + taskHeaderID + '\'' + 
			",dateCreated = '" + dateCreated + '\'' + 
			",taskName = '" + taskName + '\'' + 
			",taskDetails = '" + taskDetails + '\'' + 
			"}";
		}
}