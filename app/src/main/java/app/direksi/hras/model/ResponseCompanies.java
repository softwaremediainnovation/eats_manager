package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCompanies{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	@SerializedName("data")
	@Expose
	private List<DataCompaniesItem> dataCompanies;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	public void setDataCompanies(List<DataCompaniesItem> dataCompanies){
		this.dataCompanies = dataCompanies;
	}

	public List<DataCompaniesItem> getDataCompanies(){
		return dataCompanies;
	}

	@Override
 	public String toString(){
		return 
			"ResponseCompanies{" + 
			"errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			",dataCompanies = '" + dataCompanies + '\'' + 
			"}";
		}
}