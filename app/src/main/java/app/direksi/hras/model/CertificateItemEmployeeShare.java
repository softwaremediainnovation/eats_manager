package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CertificateItemEmployeeShare {

	@SerializedName("isGraduate")
	private boolean isGraduate;

	@SerializedName("employeeCertificateID")
	private int employeeCertificateID;

	@SerializedName("dateTo")
	private String dateTo;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("dateFrom")
	private String dateFrom;

	@SerializedName("employee")
	private Object employee;

	public void setIsGraduate(boolean isGraduate){
		this.isGraduate = isGraduate;
	}

	public boolean isIsGraduate(){
		return isGraduate;
	}

	public void setEmployeeCertificateID(int employeeCertificateID){
		this.employeeCertificateID = employeeCertificateID;
	}

	public int getEmployeeCertificateID(){
		return employeeCertificateID;
	}

	public void setDateTo(String dateTo){
		this.dateTo = dateTo;
	}

	public String getDateTo(){
		return dateTo;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setDateFrom(String dateFrom){
		this.dateFrom = dateFrom;
	}

	public String getDateFrom(){
		return dateFrom;
	}

	public void setEmployee(Object employee){
		this.employee = employee;
	}

	public Object getEmployee(){
		return employee;
	}

	@Override
 	public String toString(){
		return 
			"CertificateItem{" + 
			"isGraduate = '" + isGraduate + '\'' + 
			",employeeCertificateID = '" + employeeCertificateID + '\'' + 
			",dateTo = '" + dateTo + '\'' + 
			",name = '" + name + '\'' + 
			",description = '" + description + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",dateFrom = '" + dateFrom + '\'' + 
			",employee = '" + employee + '\'' + 
			"}";
		}
}