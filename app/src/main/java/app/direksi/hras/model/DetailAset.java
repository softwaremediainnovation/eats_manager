
package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailAset {

    @SerializedName("history")
    private List<DetailAsetHistory> mHistory;
    @SerializedName("id")
    private Long mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("origin")
    private String mOrigin;

    public List<DetailAsetHistory> getHistory() {
        return mHistory;
    }

    public void setHistory(List<DetailAsetHistory> history) {
        mHistory = history;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getOrigin() {
        return mOrigin;
    }

    public void setOrigin(String origin) {
        mOrigin = origin;
    }

}
