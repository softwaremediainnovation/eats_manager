
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailCuti {

    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("approverName")
    private String mApproverName;
    @SerializedName("constantaID")
    private Long mConstantaID;
    @SerializedName("constantaName")
    private String mConstantaName;
    @SerializedName("date")
    private String mDate;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("dayOffID")
    private Long mDayOffID;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("end")
    private String mEnd;
    @SerializedName("file")
    private String mFile;
    @SerializedName("howManyDays")
    private Long mHowManyDays;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("isDisease")
    private String mIsDisease;
    @SerializedName("note")
    private String mNote;
    @SerializedName("path")
    private String mPath;
    @SerializedName("start")
    private String mStart;
    @SerializedName("employee")
    private Employee mEmployee;

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getApproverName() {
        return mApproverName;
    }

    public void setApproverName(String approverName) {
        mApproverName = approverName;
    }

    public Long getConstantaID() {
        return mConstantaID;
    }

    public void setConstantaID(Long constantaID) {
        mConstantaID = constantaID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getConstantaName() {
        return mConstantaName;
    }

    public void setConstantaName(String constantaName) {
        mConstantaName = constantaName;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public Long getDayOffID() {
        return mDayOffID;
    }

    public void setDayOffID(Long dayOffID) {
        mDayOffID = dayOffID;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        mEmployeeName = employeeName;
    }

    public String getEnd() {
        return mEnd;
    }

    public void setEnd(String end) {
        mEnd = end;
    }

    public String getFile() {
        return mFile;
    }

    public void setFile(String file) {
        mFile = file;
    }

    public Long getHowManyDays() {
        return mHowManyDays;
    }

    public void setHowManyDays(Long howManyDays) {
        mHowManyDays = howManyDays;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public String getIsDisease() {
        return mIsDisease;
    }

    public void setIsDisease(String isDisease) {
        mIsDisease = isDisease;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public String getStart() {
        return mStart;
    }

    public void setStart(String start) {
        mStart = start;
    }

    public Employee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Employee employee) {
        mEmployee = employee;
    }

}
