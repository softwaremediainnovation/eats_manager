
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Permissions {

    @SerializedName("CanApproveDayOff")
    private Boolean mCanApproveDayOff;
    @SerializedName("CanApproveLoan")
    private Boolean mCanApproveLoan;
    @SerializedName("CanApproveOvertime")
    private Boolean mCanApproveOvertime;
    @SerializedName("CanApproveReimbursement")
    private Boolean mCanApproveReimbursement;
    @SerializedName("CanGeneratePayroll")
    private Boolean mCanGeneratePayroll;
    @SerializedName("CanIssueReprimand")
    private Boolean mCanIssueReprimand;
    @SerializedName("CanViewAttendance")
    private Boolean mCanViewAttendance;
    @SerializedName("CanViewReport")
    private Boolean mCanViewReport;
    @SerializedName("CanViewAsset")
    private Boolean mCanViewAsset;
    @SerializedName("CanViewActivity")
    private Boolean mCanViewActivity;
    @SerializedName("CanApproveRequestAttendance")
    private Boolean mCanApproveRequestAttendance;
    @SerializedName("CanViewTask")
    private Boolean mCanViewTask;
    @SerializedName("CanViewEvaluation")
    private Boolean mCanViewEvaluation;
    @SerializedName("mTest")
    private Boolean mTest;


    public Boolean getmTest() {
        return mTest;
    }

    public void setmTest(Boolean CanApproveDayOff) {
        mTest = CanApproveDayOff;
    }

    public Boolean getmCanViewEvaluation() {
        return mCanViewEvaluation;
    }

    public void setmCanViewEvaluation(Boolean CanApproveDayOff) {
        mCanViewEvaluation = CanApproveDayOff;
    }

    public Boolean getCanViewTask() {
        return mCanViewTask;
    }

    public void setCanViewTask(Boolean CanApproveDayOff) {
        mCanViewTask = CanApproveDayOff;
    }

    public Boolean getCanApproveDayOff() {
        return mCanApproveDayOff;
    }

    public void setCanApproveDayOff(Boolean CanApproveDayOff) {
        mCanApproveDayOff = CanApproveDayOff;
    }

    public Boolean getCanApproveLoan() {
        return mCanApproveLoan;
    }

    public void setCanApproveLoan(Boolean CanApproveLoan) {
        mCanApproveLoan = CanApproveLoan;
    }

    public Boolean getCanApproveOvertime() {
        return mCanApproveOvertime;
    }

    public void setCanApproveOvertime(Boolean CanApproveOvertime) {
        mCanApproveOvertime = CanApproveOvertime;
    }

    public Boolean getCanApproveReimbursement() {
        return mCanApproveReimbursement;
    }

    public void setCanApproveReimbursement(Boolean CanApproveReimbursement) {
        mCanApproveReimbursement = CanApproveReimbursement;
    }

    public Boolean getCanGeneratePayroll() {
        return mCanGeneratePayroll;
    }

    public void setCanGeneratePayroll(Boolean CanGeneratePayroll) {
        mCanGeneratePayroll = CanGeneratePayroll;
    }

    public Boolean getCanIssueReprimand() {
        return mCanIssueReprimand;
    }

    public void setCanIssueReprimand(Boolean CanIssueReprimand) {
        mCanIssueReprimand = CanIssueReprimand;
    }

    public Boolean getCanViewAttendance() {
        return mCanViewAttendance;
    }

    public void setCanViewAttendance(Boolean CanViewAttendance) {
        mCanViewAttendance = CanViewAttendance;
    }

    public Boolean getCanViewReport() {
        return mCanViewReport;
    }

    public void setCanViewReport(Boolean CanViewReport) {
        mCanViewReport = CanViewReport;
    }

    public Boolean getCanViewAsset() {
        return mCanViewAsset;
    }

    public void setCanViewAsset(Boolean CanViewReport) {
        mCanViewAsset = CanViewReport;
    }

    public Boolean getCanViewActivity() {
        return mCanViewActivity;
    }

    public void setCanViewActivity(Boolean CanViewReport) {
        mCanViewActivity = CanViewReport;
    }

    public Boolean getCanApproveRequestAttendance() {
        return mCanApproveRequestAttendance;
    }

    public void setCanApproveRequestAttendance(Boolean CanViewReport) {
        mCanApproveRequestAttendance = CanViewReport;
    }



}
