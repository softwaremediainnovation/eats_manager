package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemuResultItem {

	@SerializedName("date")
	private String date;

	@SerializedName("approver")
	private Object approver;

	@SerializedName("note")
	private String note;

	@SerializedName("howManyDays")
	private int howManyDays;

	@SerializedName("isDisease")
	private Object isDisease;

	@SerializedName("approverID")
	private String approverID;

	@SerializedName("filePath")
	private Object filePath;

	@SerializedName("constanta")
	private RemuConstanta remuConstanta;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("dayOffFile")
	private Object dayOffFile;

	@SerializedName("organizationsID")
	private Object organizationsID;

	@SerializedName("employee")
	private RemuEmployee remuEmployee;

	@SerializedName("constantaID")
	private int constantaID;

	@SerializedName("companiesID")
	private Object companiesID;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("filePathHtml")
	private String filePathHtml;

	@SerializedName("dateApproved")
	private String dateApproved;

	@SerializedName("comment")
	private String comment;

	@SerializedName("isApprovedHtml")
	private String isApprovedHtml;

	@SerializedName("dayOffID")
	private int dayOffID;

	@SerializedName("isApproved")
	private boolean isApproved;

	@SerializedName("employeesID")
	private Object employeesID;

	public String getDate(){
		return date;
	}

	public Object getApprover(){
		return approver;
	}

	public String getNote(){
		return note;
	}

	public int getHowManyDays(){
		return howManyDays;
	}

	public Object getIsDisease(){
		return isDisease;
	}

	public String getApproverID(){
		return approverID;
	}

	public Object getFilePath(){
		return filePath;
	}

	public RemuConstanta getRemuConstanta(){
		return remuConstanta;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public Object getDayOffFile(){
		return dayOffFile;
	}

	public Object getOrganizationsID(){
		return organizationsID;
	}

	public RemuEmployee getEmployee(){
		return remuEmployee;
	}

	public int getConstantaID(){
		return constantaID;
	}

	public Object getCompaniesID(){
		return companiesID;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public String getFilePathHtml(){
		return filePathHtml;
	}

	public String getDateApproved(){
		return dateApproved;
	}

	public String getComment(){
		return comment;
	}

	public String getIsApprovedHtml(){
		return isApprovedHtml;
	}

	public int getDayOffID(){
		return dayOffID;
	}

	public boolean isIsApproved(){
		return isApproved;
	}

	public Object getEmployeesID(){
		return employeesID;
	}
}