
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDetailAttendance {

    @SerializedName("attendanceID")
    private Long mAttendanceID;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("dateTime")
    private String mDateTime;
    @SerializedName("employee")
    private DataEmployeeAttendance mEmployee;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("isLate")
    private Boolean mIsLate;
    @SerializedName("isMobileApp")
    private Boolean mIsMobileApp;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("lateTimeInMinute")
    private Double mLateTimeInMinute;
    @SerializedName("organization")
    private String organization;
    @SerializedName("inOut")
    private String inOut;

    public Long getAttendanceID() {
        return mAttendanceID;
    }

    public void setAttendanceID(Long attendanceID) {
        mAttendanceID = attendanceID;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String dateTime) {
        mDateTime = dateTime;
    }

    public DataEmployeeAttendance getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataEmployeeAttendance employee) {
        mEmployee = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Boolean getIsLate() {
        return mIsLate;
    }

    public void setIsLate(Boolean isLate) {
        mIsLate = isLate;
    }

    public Boolean getIsMobileApp() {
        return mIsMobileApp;
    }

    public void setIsMobileApp(Boolean isMobileApp) {
        mIsMobileApp = isMobileApp;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public Double getLateTimeInMinute() {
        return mLateTimeInMinute;
    }

    public void setLateTimeInMinute(Double longitude) {
        mLateTimeInMinute = longitude;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String longitude) {
        organization = longitude;
    }

    public void setInOut(String inOut){
        this.inOut = inOut;
    }

    public String getInOut(){
        return inOut;
    }

}
