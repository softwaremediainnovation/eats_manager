
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataLoan {

    @SerializedName("totalPembayaran")
    private Double mTotalPembayaran;
    @SerializedName("totalPinjaman")
    private Double mTotalPinjaman;
    @SerializedName("amount")
    private Long mAmount;
    @SerializedName("approver")
    private DataDetailLoanApprover mApprover;
    @SerializedName("approver2")
    private String mApprover2;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("approverID2")
    private String mApproverID2;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("comment2")
    private String mComment2;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("dateApproved2")
    private String mDateApproved2;
    @SerializedName("datePaid")
    private String mDatePaid;
    @SerializedName("employee")
    private DataDetailLoanEmployee mEmployee;
    @SerializedName("employeeApprover")
    private DataDetailLoanEmployeeApprover mEmployeeApprover;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("installment")
    private Long mInstallment;
    @SerializedName("installmentAmount")
    private Double mInstallmentAmount;
    @SerializedName("installmentPayOut")
    private Double mInstallmentPayOut;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("isApproved2")
    private Boolean mIsApproved2;
    @SerializedName("isPaid")
    private Boolean mIsPaid;
    @SerializedName("isPbonMingguan")
    private Boolean mIsPbonMingguan;
    @SerializedName("loanID")
    private Long mLoanID;
    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("organization")
    private String organization;
    @SerializedName("isCair")
    private Boolean isCair;
    @SerializedName("dateCair")
    private String dateCair;
    @SerializedName("userCair")
    private String userCair;
    @SerializedName("methodCair")
    private String methodCair;

    public Double getTotalPembayaran() {
        return mTotalPembayaran;
    }

    public void setTotalPembayaran(Double totalPembayaran) {
        mTotalPembayaran = totalPembayaran;
    }

    public Double getTotalPinjaman() {
        return mTotalPinjaman;
    }

    public void setTotalPinjaman(Double totalPinjaman) {
        mTotalPinjaman = totalPinjaman;
    }

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long amount) {
        mAmount = amount;
    }

    public DataDetailLoanApprover getApprover() {
        return mApprover;
    }

    public void setApprover(DataDetailLoanApprover approver) {
        mApprover = approver;
    }

    public String getApprover2() {
        return mApprover2;
    }

    public void setApprover2(String approver) {
        mApprover2 = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getApproverID2() {
        return mApproverID2;
    }

    public void setApproverID2(String approverID) {
        mApproverID2 = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getComment2() {
        return mComment2;
    }

    public void setComment2(String comment) {
        mComment2 = comment;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getDateApproved2() {
        return mDateApproved2;
    }

    public void setDateApproved2(String dateApproved) {
        mDateApproved2 = dateApproved;
    }

    public String getDatePaid() {
        return mDatePaid;
    }

    public void setDatePaid(String datePaid) {
        mDatePaid = datePaid;
    }

    public DataDetailLoanEmployee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataDetailLoanEmployee employee) {
        mEmployee = employee;
    }

    public DataDetailLoanEmployeeApprover getEmployeeApprover() {
        return mEmployeeApprover;
    }

    public void setEmployeeApprover(DataDetailLoanEmployeeApprover employeeApprover) {
        mEmployeeApprover = employeeApprover;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Long getInstallment() {
        return mInstallment;
    }

    public void setInstallment(Long installment) {
        mInstallment = installment;
    }

    public Double getInstallmentAmount() {
        return mInstallmentAmount;
    }

    public void setInstallmentAmount(Double installmentAmount) {
        mInstallmentAmount = installmentAmount;
    }

    public Double getInstallmentPayOut() {
        return mInstallmentPayOut;
    }

    public void setInstallmentPayOut(Double installmentPayOut) {
        mInstallmentPayOut = installmentPayOut;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public Boolean getIsApproved2() {
        return mIsApproved2;
    }

    public void setIsApproved2(Boolean isApproved) {
        mIsApproved2 = isApproved;
    }

    public Boolean getIsPaid() {
        return mIsPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        mIsPaid = isPaid;
    }

    public Boolean getIsPbonMingguan() {
        return mIsPbonMingguan;
    }

    public void setIsPbonMingguan(Boolean isPbonMingguan) {
        mIsPbonMingguan = isPbonMingguan;
    }

    public Long getLoanID() {
        return mLoanID;
    }

    public void setLoanID(Long loanID) {
        mLoanID = loanID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String note) {
        organization = note;
    }

    public void setIsCair(Boolean isPaid) {
        isCair = isPaid;
    }

    public Boolean getIsCair() {
        return isCair;
    }

    public String getDateCair() {
        return dateCair;
    }

    public void setDateCair(String note) {
        dateCair = note;
    }

    public String getUserCair() {
        return userCair;
    }

    public void setUserCair(String note) {
        userCair = note;
    }

    public String getMethodCair() {
        return methodCair;
    }

    public void setMethodCair(String note) {
        methodCair = note;
    }

}
