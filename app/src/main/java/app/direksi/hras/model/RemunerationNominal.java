package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemunerationNominal{

	@SerializedName("date")
	private String date;

	@SerializedName("lembur3")
	private Double lembur3;

	@SerializedName("ijinKeluarKantorTime")
	private Double ijinKeluarKantorTime;

	@SerializedName("terlambatTime")
	private Double terlambatTime;

	@SerializedName("premi1")
	private Double premi1;

	@SerializedName("premi2")
	private Double premi2;

	@SerializedName("hariTidakMasukTotal")
	private Double hariTidakMasukTotal;

	@SerializedName("lembur1")
	private Double lembur1;

	@SerializedName("lembur2")
	private Double lembur2;

	@SerializedName("isModified")
	private boolean isModified;

	@SerializedName("lembur3Time")
	private Double lembur3Time;

	@SerializedName("hariMasukTelat")
	private Double hariMasukTelat;

	@SerializedName("tunjanganAdminBank")
	private Double tunjanganAdminBank;

	@SerializedName("remunerationFormulas")
	private Object remunerationFormulas;

	@SerializedName("insentifTime")
	private Double insentifTime;

	@SerializedName("bpjsKesehatan")
	private Double bpjsKesehatan;

	@SerializedName("hariTidakMasukIjin")
	private Double hariTidakMasukIjin;

	@SerializedName("remunerationID")
	private Double remunerationID;

	@SerializedName("uangMakanLemburTime")
	private Double uangMakanLemburTime;

	@SerializedName("isDelivered")
	private boolean isDelivered;

	@SerializedName("payroll")
	private RemuPayroll remuPayroll;

	@SerializedName("angsuranPinjaman")
	private Double angsuranPinjaman;

	@SerializedName("hariTidakMasukTidakIjin")
	private Double hariTidakMasukTidakIjin;

	@SerializedName("hariTidakMasukH2")
	private Double hariTidakMasukH2;

	@SerializedName("ijinKeluarKantor")
	private Double ijinKeluarKantor;

	@SerializedName("uangMakan")
	private Double uangMakan;

	@SerializedName("uangMakanLembur")
	private Double uangMakanLembur;

	@SerializedName("holidayMasterID")
	private Double holidayMasterID;

	@SerializedName("hariTidakTelat")
	private Double hariTidakTelat;

	@SerializedName("hariTidakMasukCutiPerusahaan")
	private Double hariTidakMasukCutiPerusahaan;

	@SerializedName("hariTidakMasukCutiTahunan")
	private Double hariTidakMasukCutiTahunan;

	@SerializedName("insentif")
	private Double insentif;

	@SerializedName("ijinTidakMasuk")
	private Double ijinTidakMasuk;

	@SerializedName("remuneration")
	private Remuneration remuneration;

	@SerializedName("hariKerja")
	private Double hariKerja;

	@SerializedName("gradeInsentif")
	private String gradeInsentif;

	@SerializedName("ijinTidakMasukTime")
	private Double ijinTidakMasukTime;

	@SerializedName("pbonMingguan")
	private boolean pbonMingguan;

	@SerializedName("lembur2Time")
	private Double lembur2Time;

	@SerializedName("hariTidakMasukSakit")
	private Double hariTidakMasukSakit;

	@SerializedName("creator")
	private Object creator;

	@SerializedName("koreksiGaji")
	private Double koreksiGaji;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("terlambat")
	private Double terlambat;

	@SerializedName("payrollID")
	private Double payrollID;

	@SerializedName("tunjanganLainLain")
	private Double tunjanganLainLain;

	@SerializedName("lembur1Time")
	private Double lembur1Time;

	@SerializedName("remunerationNominalID")
	private Double remunerationNominalID;

	@SerializedName("tunjanganTetap")
	private Double tunjanganTetap;

	@SerializedName("holidayMaster")
	private Object holidayMaster;

	@SerializedName("gajiPokok")
	private Double gajiPokok;

	@SerializedName("pph21")
	private Double pph21;

	public String getDate(){
		return date;
	}

	public Double getLembur3(){
		return lembur3;
	}

	public Double getIjinKeluarKantorTime(){
		return ijinKeluarKantorTime;
	}

	public Double getTerlambatTime(){
		return terlambatTime;
	}

	public Double getPremi1(){
		return premi1;
	}

	public Double getPremi2(){
		return premi2;
	}

	public Double getHariTidakMasukTotal(){
		return hariTidakMasukTotal;
	}

	public Double getLembur1(){
		return lembur1;
	}

	public Double getLembur2(){
		return lembur2;
	}

	public boolean isIsModified(){
		return isModified;
	}

	public Double getLembur3Time(){
		return lembur3Time;
	}

	public Double getHariMasukTelat(){
		return hariMasukTelat;
	}

	public Double getTunjanganAdminBank(){
		return tunjanganAdminBank;
	}

	public Object getRemunerationFormulas(){
		return remunerationFormulas;
	}

	public Double getInsentifTime(){
		return insentifTime;
	}

	public Double getBpjsKesehatan(){
		return bpjsKesehatan;
	}

	public Double getHariTidakMasukIjin(){
		return hariTidakMasukIjin;
	}

	public Double getRemunerationID(){
		return remunerationID;
	}

	public Double getUangMakanLemburTime(){
		return uangMakanLemburTime;
	}

	public boolean isIsDelivered(){
		return isDelivered;
	}

	public RemuPayroll getPayroll(){
		return remuPayroll;
	}

	public Object getAngsuranPinjaman(){
		return angsuranPinjaman;
	}

	public Double getHariTidakMasukTidakIjin(){
		return hariTidakMasukTidakIjin;
	}

	public Double getHariTidakMasukH2(){
		return hariTidakMasukH2;
	}

	public Double getIjinKeluarKantor(){
		return ijinKeluarKantor;
	}

	public Double getUangMakan(){
		return uangMakan;
	}

	public Double getUangMakanLembur(){
		return uangMakanLembur;
	}

	public Double getHolidayMasterID(){
		return holidayMasterID;
	}

	public Double getHariTidakTelat(){
		return hariTidakTelat;
	}

	public Double getHariTidakMasukCutiPerusahaan(){
		return hariTidakMasukCutiPerusahaan;
	}

	public Double getHariTidakMasukCutiTahunan(){
		return hariTidakMasukCutiTahunan;
	}

	public Double getInsentif(){
		return insentif;
	}

	public Double getIjinTidakMasuk(){
		return ijinTidakMasuk;
	}

	public Remuneration getRemuneration(){
		return remuneration;
	}

	public Double getHariKerja(){
		return hariKerja;
	}

	public String getGradeInsentif(){
		return gradeInsentif;
	}

	public Double getIjinTidakMasukTime(){
		return ijinTidakMasukTime;
	}

	public boolean isPbonMingguan(){
		return pbonMingguan;
	}

	public Double getLembur2Time(){
		return lembur2Time;
	}

	public Double getHariTidakMasukSakit(){
		return hariTidakMasukSakit;
	}

	public Object getCreator(){
		return creator;
	}

	public Double getKoreksiGaji(){
		return koreksiGaji;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public Double getTerlambat(){
		return terlambat;
	}

	public Double getPayrollID(){
		return payrollID;
	}

	public Double getTunjanganLainLain(){
		return tunjanganLainLain;
	}

	public Double getLembur1Time(){
		return lembur1Time;
	}

	public Double getRemunerationNominalID(){
		return remunerationNominalID;
	}

	public Double getTunjanganTetap(){
		return tunjanganTetap;
	}

	public Object getHolidayMaster(){
		return holidayMaster;
	}

	public Double getGajiPokok(){
		return gajiPokok;
	}

	public Double getPph21(){
		return pph21;
	}
}