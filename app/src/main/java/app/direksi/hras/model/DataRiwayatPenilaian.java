package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataRiwayatPenilaian {

	@SerializedName("evaluationID")
	private int evaluationID;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("companyID")
	private int companyID;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("organizations")
	private String organizations;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("employee")
	private EmployeeRiwayatPenilaian employee;

	@SerializedName("evaluatorID")
	private int evaluatorID;

	@SerializedName("catatan")
	private String catatan;

	@SerializedName("encrypted")
	private String encrypted;

	public String getEncrypted(){
		return encrypted;
	}

	public String getCatatan(){
		return catatan;
	}

	public int getEvaluationID(){
		return evaluationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public int getCompanyID(){
		return companyID;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public String getOrganizations(){
		return organizations;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public EmployeeRiwayatPenilaian getEmployee(){
		return employee;
	}

	public int getEvaluatorID(){
		return evaluatorID;
	}
}