package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataConstantaItem{

	@SerializedName("unit")
	@Expose
	private String unit;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("constantaID")
	@Expose
	private int constantaID;

	@SerializedName("value")
	@Expose
	private String value;

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setConstantaID(int constantaID){
		this.constantaID = constantaID;
	}

	public int getConstantaID(){
		return constantaID;
	}

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	@Override
 	public String toString(){
		return 
			"DataConstantaItem{" + 
			"unit = '" + unit + '\'' + 
			",name = '" + name + '\'' + 
			",constantaID = '" + constantaID + '\'' + 
			",value = '" + value + '\'' + 
			"}";
		}
}