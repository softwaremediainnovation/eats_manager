package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrganizationItemEmployeeShare {

	@SerializedName("employeeOrganizationID")
	private int employeeOrganizationID;

	@SerializedName("dateStart")
	private String dateStart;

	@SerializedName("name")
	private String name;

	@SerializedName("description")
	private String description;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("dateEnd")
	private String dateEnd;

	@SerializedName("employee")
	private Object employee;

	public void setEmployeeOrganizationID(int employeeOrganizationID){
		this.employeeOrganizationID = employeeOrganizationID;
	}

	public int getEmployeeOrganizationID(){
		return employeeOrganizationID;
	}

	public void setDateStart(String dateStart){
		this.dateStart = dateStart;
	}

	public String getDateStart(){
		return dateStart;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setDateEnd(String dateEnd){
		this.dateEnd = dateEnd;
	}

	public String getDateEnd(){
		return dateEnd;
	}

	public void setEmployee(Object employee){
		this.employee = employee;
	}

	public Object getEmployee(){
		return employee;
	}

	@Override
 	public String toString(){
		return 
			"OrganizationItem{" + 
			"employeeOrganizationID = '" + employeeOrganizationID + '\'' + 
			",dateStart = '" + dateStart + '\'' + 
			",name = '" + name + '\'' + 
			",description = '" + description + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",dateEnd = '" + dateEnd + '\'' + 
			",employee = '" + employee + '\'' + 
			"}";
		}
}