package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DetailLoanWithDetailEmployee {

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("address")
	private String address;

	@SerializedName("gender")
	private String gender;

	@SerializedName("isLeader")
	private Object isLeader;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("userID")
	private String userID;

	@SerializedName("deviceID")
	private String deviceID;

	@SerializedName("religion")
	private String religion;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("nik")
	private String nik;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("photoUrl")
	private String photoUrl;

	@SerializedName("applicationUser")
	private Object applicationUser;

	@SerializedName("joinDate")
	private String joinDate;

	@SerializedName("phone")
	private String phone;

	@SerializedName("dob")
	private String dob;

	@SerializedName("imageFile")
	private Object imageFile;

	@SerializedName("organization")
	private Object organization;

	@SerializedName("idCardNumber")
	private String idCardNumber;

	@SerializedName("company")
	private Object company;

	@SerializedName("fullNameNik")
	private String fullNameNik;

	@SerializedName("email")
	private String email;

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setIsLeader(Object isLeader){
		this.isLeader = isLeader;
	}

	public Object getIsLeader(){
		return isLeader;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUserID(String userID){
		this.userID = userID;
	}

	public String getUserID(){
		return userID;
	}

	public void setDeviceID(String deviceID){
		this.deviceID = deviceID;
	}

	public String getDeviceID(){
		return deviceID;
	}

	public void setReligion(String religion){
		this.religion = religion;
	}

	public String getReligion(){
		return religion;
	}

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setNik(String nik){
		this.nik = nik;
	}

	public String getNik(){
		return nik;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setPhotoUrl(String photoUrl){
		this.photoUrl = photoUrl;
	}

	public String getPhotoUrl(){
		return photoUrl;
	}

	public void setApplicationUser(Object applicationUser){
		this.applicationUser = applicationUser;
	}

	public Object getApplicationUser(){
		return applicationUser;
	}

	public void setJoinDate(String joinDate){
		this.joinDate = joinDate;
	}

	public String getJoinDate(){
		return joinDate;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setImageFile(Object imageFile){
		this.imageFile = imageFile;
	}

	public Object getImageFile(){
		return imageFile;
	}

	public void setOrganization(Object organization){
		this.organization = organization;
	}

	public Object getOrganization(){
		return organization;
	}

	public void setIdCardNumber(String idCardNumber){
		this.idCardNumber = idCardNumber;
	}

	public String getIdCardNumber(){
		return idCardNumber;
	}

	public void setCompany(Object company){
		this.company = company;
	}

	public Object getCompany(){
		return company;
	}

	public void setFullNameNik(String fullNameNik){
		this.fullNameNik = fullNameNik;
	}

	public String getFullNameNik(){
		return fullNameNik;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"Employee{" + 
			"lastName = '" + lastName + '\'' + 
			",address = '" + address + '\'' + 
			",gender = '" + gender + '\'' + 
			",isLeader = '" + isLeader + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",userID = '" + userID + '\'' + 
			",deviceID = '" + deviceID + '\'' + 
			",religion = '" + religion + '\'' + 
			",organizationID = '" + organizationID + '\'' + 
			",nik = '" + nik + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",photoUrl = '" + photoUrl + '\'' + 
			",applicationUser = '" + applicationUser + '\'' + 
			",joinDate = '" + joinDate + '\'' + 
			",phone = '" + phone + '\'' + 
			",dob = '" + dob + '\'' + 
			",imageFile = '" + imageFile + '\'' + 
			",organization = '" + organization + '\'' + 
			",idCardNumber = '" + idCardNumber + '\'' + 
			",company = '" + company + '\'' + 
			",fullNameNik = '" + fullNameNik + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}