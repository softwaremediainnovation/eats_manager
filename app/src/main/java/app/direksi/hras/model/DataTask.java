package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataTask {

	@SerializedName("employeeName")
	private String employeeName;

	@SerializedName("note")
	private String note;

	@SerializedName("dueDate")
	private String dueDate;

	@SerializedName("creatorID")
	private int creatorID;

	@SerializedName("creatorName")
	private String creatorName;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("isFinish")
	private boolean isFinish;

	@SerializedName("dateFinish")
	private String dateFinish;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("isExpired")
	private boolean isExpired;

	@SerializedName("updatedByID")
	private int updatedByID;

	@SerializedName("dateUpdated")
	private String dateUpdated;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("taskHeaderID")
	private int taskHeaderID;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("organization")
	private String organization;

	@SerializedName("taskName")
	private String taskName;

	@SerializedName("flag")
	private String flag;

	public void setFlag(String note){
		this.flag = note;
	}

	public String getFlag(){
		return flag;
	}

	public void setEmployeeName(String employeeName){
		this.employeeName = employeeName;
	}

	public String getEmployeeName(){
		return employeeName;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setDueDate(String dueDate){
		this.dueDate = dueDate;
	}

	public String getDueDate(){
		return dueDate;
	}

	public void setCreatorID(int creatorID){
		this.creatorID = creatorID;
	}

	public int getCreatorID(){
		return creatorID;
	}

	public void setCreatorName(String creatorName){
		this.creatorName = creatorName;
	}

	public String getCreatorName(){
		return creatorName;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setIsFinish(boolean isFinish){
		this.isFinish = isFinish;
	}

	public boolean isIsFinish(){
		return isFinish;
	}

	public void setExpired(boolean isFinish){
		this.isExpired = isFinish;
	}

	public boolean isIsExpired(){
		return isExpired;
	}

	public void setDateFinish(String dateFinish){
		this.dateFinish = dateFinish;
	}

	public String getDateFinish(){
		return dateFinish;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUpdatedByID(int updatedByID){
		this.updatedByID = updatedByID;
	}

	public int getUpdatedByID(){
		return updatedByID;
	}

	public void setDateUpdated(String dateUpdated){
		this.dateUpdated = dateUpdated;
	}

	public String getDateUpdated(){
		return dateUpdated;
	}

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setTaskHeaderID(int taskHeaderID){
		this.taskHeaderID = taskHeaderID;
	}

	public int getTaskHeaderID(){
		return taskHeaderID;
	}

	public void setDateCreated(String dateCreated){
		this.dateCreated = dateCreated;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public void setOrganization(String organization){
		this.organization = organization;
	}

	public String getOrganization(){
		return organization;
	}

	public void setTaskName(String taskName){
		this.taskName = taskName;
	}

	public String getTaskName(){
		return taskName;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"employeeName = '" + employeeName + '\'' + 
			",note = '" + note + '\'' + 
			",dueDate = '" + dueDate + '\'' + 
			",creatorID = '" + creatorID + '\'' + 
			",creatorName = '" + creatorName + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",isFinish = '" + isFinish + '\'' + 
			",dateFinish = '" + dateFinish + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",updatedByID = '" + updatedByID + '\'' + 
			",dateUpdated = '" + dateUpdated + '\'' + 
			",organizationID = '" + organizationID + '\'' + 
			",taskHeaderID = '" + taskHeaderID + '\'' + 
			",dateCreated = '" + dateCreated + '\'' + 
			",organization = '" + organization + '\'' + 
			",taskName = '" + taskName + '\'' + 
			"}";
		}
}