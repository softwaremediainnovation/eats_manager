package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataListCompanies {

	@SerializedName("companyID")
	private int companyID;

	@SerializedName("companyName")
	private String companyName;

	public void setCompanyID(int companyID){
		this.companyID = companyID;
	}

	public int getCompanyID(){
		return companyID;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"companyID = '" + companyID + '\'' + 
			",companyName = '" + companyName + '\'' + 
			"}";
		}
}