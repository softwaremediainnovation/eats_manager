
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDetailCutiApprover {

    @SerializedName("address")
    private Object mAddress;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("gender")
    private Object mGender;
    @SerializedName("id")
    private String mId;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("nik")
    private Object mNik;
    @SerializedName("permissions")
    private Object mPermissions;
    @SerializedName("phone")
    private Object mPhone;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("role")
    private Object mRole;
    @SerializedName("signinToMobile")
    private Boolean mSigninToMobile;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object address) {
        mAddress = address;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public Object getGender() {
        return mGender;
    }

    public void setGender(Object gender) {
        mGender = gender;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Object getNik() {
        return mNik;
    }

    public void setNik(Object nik) {
        mNik = nik;
    }

    public Object getPermissions() {
        return mPermissions;
    }

    public void setPermissions(Object permissions) {
        mPermissions = permissions;
    }

    public Object getPhone() {
        return mPhone;
    }

    public void setPhone(Object phone) {
        mPhone = phone;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public Object getRole() {
        return mRole;
    }

    public void setRole(Object role) {
        mRole = role;
    }

    public Boolean getSigninToMobile() {
        return mSigninToMobile;
    }

    public void setSigninToMobile(Boolean signinToMobile) {
        mSigninToMobile = signinToMobile;
    }

}
