package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ResponseDataKategoriAset{

	@SerializedName("data")
	private List<DataKategoriAset> data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public List<DataKategoriAset> getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}