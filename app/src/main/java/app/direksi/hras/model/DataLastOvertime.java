
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataLastOvertime {

    @SerializedName("approver")
    private Object mApprover;
    @SerializedName("approverID")
    private Object mApproverID;
    @SerializedName("comment")
    private Object mComment;
    @SerializedName("dateApproved")
    private Object mDateApproved;
    @SerializedName("dateEnd")
    private Object mDateEnd;
    @SerializedName("dateStart")
    private Object mDateStart;
    @SerializedName("employee")
    private Object mEmployee;
    @SerializedName("employeeApprover")
    private Object mEmployeeApprover;
    @SerializedName("employeeID")
    private Object mEmployeeID;
    @SerializedName("filePath")
    private Object mFilePath;
    @SerializedName("howManyMinutes")
    private Double mHowManyHour;
    @SerializedName("isApproved")
    private Object mIsApproved;
    @SerializedName("note")
    private Object mNote;
    @SerializedName("overtimeID")
    private Long mOvertimeID;

    public Object getApprover() {
        return mApprover;
    }

    public void setApprover(Object approver) {
        mApprover = approver;
    }

    public Object getApproverID() {
        return mApproverID;
    }

    public void setApproverID(Object approverID) {
        mApproverID = approverID;
    }

    public Object getComment() {
        return mComment;
    }

    public void setComment(Object comment) {
        mComment = comment;
    }

    public Object getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(Object dateApproved) {
        mDateApproved = dateApproved;
    }

    public Object getDateEnd() {
        return mDateEnd;
    }

    public void setDateEnd(Object dateEnd) {
        mDateEnd = dateEnd;
    }

    public Object getDateStart() {
        return mDateStart;
    }

    public void setDateStart(Object dateStart) {
        mDateStart = dateStart;
    }

    public Object getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Object employee) {
        mEmployee = employee;
    }

    public Object getEmployeeApprover() {
        return mEmployeeApprover;
    }

    public void setEmployeeApprover(Object employeeApprover) {
        mEmployeeApprover = employeeApprover;
    }

    public Object getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Object employeeID) {
        mEmployeeID = employeeID;
    }

    public Object getFilePath() {
        return mFilePath;
    }

    public void setFilePath(Object filePath) {
        mFilePath = filePath;
    }

    public Double getHowManyHour() {
        return mHowManyHour;
    }

    public void setHowManyHour(Double howManyHour) {
        mHowManyHour = howManyHour;
    }

    public Object getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Object isApproved) {
        mIsApproved = isApproved;
    }

    public Object getNote() {
        return mNote;
    }

    public void setNote(Object note) {
        mNote = note;
    }

    public Long getOvertimeID() {
        return mOvertimeID;
    }

    public void setOvertimeID(Long overtimeID) {
        mOvertimeID = overtimeID;
    }

}
