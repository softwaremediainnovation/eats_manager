package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataActivities{

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPhotoFile() {
		return photoFile;
	}

	public void setPhotoFile(String photoFile) {
		this.photoFile = photoFile;
	}

	public int getActivityID() {
		return activityID;
	}

	public void setActivityID(int activityID) {
		this.activityID = activityID;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public void setPhotoUrl(String photoUrl) {
		this.photoUrl = photoUrl;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String getName() {
		return name;
	}

	public void setName(String longitude) {
		this.name = longitude;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("photoFile")
	@Expose
	private String photoFile;

	@SerializedName("activityID")
	@Expose
	private int activityID;

	@SerializedName("photoUrl")
	@Expose
	private String photoUrl;

	@SerializedName("latitude")
	@Expose
	private String latitude;

	@SerializedName("description")
	@Expose
	private String description;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("longitude")
	@Expose
	private String longitude;

	@SerializedName("employee")
	@Expose
	private Employee employee;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("address")
	@Expose
	private String address;


}