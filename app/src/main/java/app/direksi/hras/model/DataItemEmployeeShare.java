package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataItemEmployeeShare {

	@SerializedName("education")
	private List<EducationItemEmployeeShare> education;

	@SerializedName("organisasi")
	private String organisasi;

	@SerializedName("profile")
	private ProfileEmployeeShare profile;

	@SerializedName("organization")
	private List<OrganizationItemEmployeeShare> organization;

	@SerializedName("certificate")
	private List<CertificateItemEmployeeShare> certificate;

	@SerializedName("company")
	private String company;

	@SerializedName("employment")
	private List<EmploymentItemEmployeeShare> employment;

	@SerializedName("relative")
	private List<RelativeItemEmployeeShare> relative;

	public void setEducation(List<EducationItemEmployeeShare> education){
		this.education = education;
	}

	public List<EducationItemEmployeeShare> getEducation(){
		return education;
	}

	public void setOrganisasi(String organisasi){
		this.organisasi = organisasi;
	}

	public String getOrganisasi(){
		return organisasi;
	}

	public void setProfile(ProfileEmployeeShare profile){
		this.profile = profile;
	}

	public ProfileEmployeeShare getProfile(){
		return profile;
	}

	public void setOrganization(List<OrganizationItemEmployeeShare> organization){
		this.organization = organization;
	}

	public List<OrganizationItemEmployeeShare> getOrganization(){
		return organization;
	}

	public void setCertificate(List<CertificateItemEmployeeShare> certificate){
		this.certificate = certificate;
	}

	public List<CertificateItemEmployeeShare> getCertificate(){
		return certificate;
	}

	public void setCompany(String company){
		this.company = company;
	}

	public String getCompany(){
		return company;
	}

	public void setEmployment(List<EmploymentItemEmployeeShare> employment){
		this.employment = employment;
	}

	public List<EmploymentItemEmployeeShare> getEmployment(){
		return employment;
	}

	public void setRelative(List<RelativeItemEmployeeShare> relative){
		this.relative = relative;
	}

	public List<RelativeItemEmployeeShare> getRelative(){
		return relative;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"education = '" + education + '\'' + 
			",organisasi = '" + organisasi + '\'' + 
			",profile = '" + profile + '\'' + 
			",organization = '" + organization + '\'' + 
			",certificate = '" + certificate + '\'' + 
			",company = '" + company + '\'' + 
			",employment = '" + employment + '\'' + 
			",relative = '" + relative + '\'' + 
			"}";
		}
}