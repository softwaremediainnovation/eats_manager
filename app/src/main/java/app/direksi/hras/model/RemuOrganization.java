package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RemuOrganization {

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("companyID")
	private int companyID;

	@SerializedName("organizationWorkingHours")
	private Object organizationWorkingHours;

	@SerializedName("holidayMaster")
	private Object holidayMaster;

	@SerializedName("name")
	private String name;

	@SerializedName("organizationQueryBuilders")
	private Object organizationQueryBuilders;

	@SerializedName("company")
	private Object company;

	@SerializedName("organizationCompanyHtml")
	private String organizationCompanyHtml;

	@SerializedName("holidayMasterID")
	private int holidayMasterID;

	@SerializedName("organizationRules")
	private Object organizationRules;

	@SerializedName("memberships")
	private List<Object> memberships;

	public int getOrganizationID(){
		return organizationID;
	}

	public int getCompanyID(){
		return companyID;
	}

	public Object getOrganizationWorkingHours(){
		return organizationWorkingHours;
	}

	public Object getHolidayMaster(){
		return holidayMaster;
	}

	public String getName(){
		return name;
	}

	public Object getOrganizationQueryBuilders(){
		return organizationQueryBuilders;
	}

	public Object getCompany(){
		return company;
	}

	public String getOrganizationCompanyHtml(){
		return organizationCompanyHtml;
	}

	public int getHolidayMasterID(){
		return holidayMasterID;
	}

	public Object getOrganizationRules(){
		return organizationRules;
	}

	public List<Object> getMemberships(){
		return memberships;
	}
}