package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrganizationEmployee {

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("companyID")
	private int companyID;

	@SerializedName("name")
	private String name;

	@SerializedName("holidayMasterID")
	private int holidayMasterID;

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setCompanyID(int companyID){
		this.companyID = companyID;
	}

	public int getCompanyID(){
		return companyID;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setHolidayMasterID(int holidayMasterID){
		this.holidayMasterID = holidayMasterID;
	}

	public int getHolidayMasterID(){
		return holidayMasterID;
	}

	@Override
 	public String toString(){
		return 
			"OrganizationEmployee{" +
			"organizationID = '" + organizationID + '\'' + 
			",companyID = '" + companyID + '\'' + 
			",name = '" + name + '\'' + 
			",holidayMasterID = '" + holidayMasterID + '\'' + 
			"}";
		}
}