package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseForgetPassword{

	@SerializedName("data")
	private DataForgetPassword data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public DataForgetPassword getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}