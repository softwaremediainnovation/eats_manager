package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class ResultDisbursementLoan{

    public ResultDisbursementLoan( String status, String user_id, String external_id, long amount, String bank_code, String account_holder_name,
                               String disbursement_description, Long loanID, String method, String id,
                               List<String> email_to, List<String> email_cc) {


        this.status = status;
        this.user_id = user_id;
        this.external_id = external_id;
        this.amount = amount;
        this.bank_code = bank_code;
        this.account_holder_name = account_holder_name;
        this.disbursement_description = disbursement_description;
        this.loanID = loanID;
        this.method = method;
        this.id = id;
        this.email_to = email_to;
        this.email_cc = email_cc;





    }




    @SerializedName("email_to")
    private List<String> email_to;

    @SerializedName("email_cc")
    private List<String> email_cc;

    @SerializedName("bank_code")
    private String bank_code;

    @SerializedName("account_number")
    private String accountNumber;

    @SerializedName("amount")
    private long amount;

    @SerializedName("user_id")
    private String user_id;

    @SerializedName("account_holder_name")
    private String account_holder_name;

    @SerializedName("external_id")
    private String external_id;

    @SerializedName("error_code")
    private String errorCode;

    @SerializedName("id")
    private String id;

    @SerializedName("disbursement_description")
    private String disbursement_description;

    @SerializedName("description")
    private String description;

    @SerializedName("message")
    private String message;

    @SerializedName("status")
    private String status;

    @SerializedName("method")
    private String method;

    @SerializedName("reimbursementID")
    private Long reimbursementID;

    @SerializedName("loanID")
    private Long loanID;



    public void setEmailTo(List<String> emailTo){
        this.email_to = emailTo;
    }

    public List<String> getEmailTo(){
        return email_to;
    }

    public void setEmail_cc(List<String> emailTo){
        this.email_cc = emailTo;
    }

    public List<String> getEmailCc(){
        return email_cc;
    }

    public void setBankCode(String bankCode){
        this.bank_code = bankCode;
    }

    public String getBankCode(){
        return bank_code;
    }

    public void setAccountNumber(String bankCode){
        this.accountNumber = bankCode;
    }

    public String getAccountNumber(){
        return accountNumber;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public long getAmount(){
        return amount;
    }

    public void setUserId(String userId){
        this.user_id = userId;
    }

    public String getUserId(){
        return user_id;
    }

    public void setAccountHolderName(String accountHolderName){
        this.account_holder_name = accountHolderName;
    }

    public String getAccountHolderName(){
        return account_holder_name;
    }

    public void setExternalId(String externalId){
        this.external_id = externalId;
    }

    public String getExternalId(){
        return external_id;
    }

    public void setErrorCode(String errorCode){
        this.errorCode = errorCode;
    }

    public String getErrorCode(){
        return errorCode;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setDisbursementDescription(String disbursementDescription){
        this.disbursement_description = disbursementDescription;
    }

    public String getDisbursementDescription(){
        return disbursement_description;
    }

    public void setDescription(String disbursementDescription){
        this.description = disbursementDescription;
    }

    public String getDescription(){
        return description;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setStatus(String status){
        this.status = status;
    }

    public String getStatus(){
        return status;
    }

    @Override
    public String toString(){
        return
                "ResponseDisbursement{" +
                        "status = '" + status + '\'' +
                        ",user_id = '" + user_id + '\'' +
                        ",external_id = '" + external_id + '\'' +
                        ",amount = '" + amount + '\'' +
                        ",bank_code = '" + bank_code + '\'' +
                        ",account_holder_name = '" + account_holder_name + '\'' +
                        ",disbursement_description = '" + disbursement_description + '\'' +
                        ",reimbursementID = '" + reimbursementID + '\'' +
                        ",method = '" + method + '\'' +
                        ",id = '" + id + '\'' +
                        "}";
    }


}
