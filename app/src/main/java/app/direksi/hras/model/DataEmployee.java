
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataEmployee {


    public DataEmployee(Long id, String first, String last) {
        this.mEmployeeID = id;
        this.mFirstName = first;
        this.mLastName = last;

    }

    @SerializedName("address")
    private String mAddress;
    @SerializedName("applicationUser")
    private DataEmployeeApplicationUser mApplicationUser;
    @SerializedName("deviceID")
    private Object mDeviceID;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("fullNameNik")
    private String mFullNameNik;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("idCardNumber")
    private String mIdCardNumber;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("isLeader")
    private Boolean mIsLeader;
    @SerializedName("joinDate")
    private Object mJoinDate;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("nik")
    private String mNik;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("religion")
    private String mReligion;
    @SerializedName("userID")
    private String mUserID;
    @SerializedName("organization")
    private OrganizationEmployee organization;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public DataEmployeeApplicationUser getApplicationUser() {
        return mApplicationUser;
    }

    public void setApplicationUser(DataEmployeeApplicationUser applicationUser) {
        mApplicationUser = applicationUser;
    }

    public Object getDeviceID() {
        return mDeviceID;
    }

    public void setDeviceID(Object deviceID) {
        mDeviceID = deviceID;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getFullNameNik() {
        return mFullNameNik;
    }

    public void setFullNameNik(String fullNameNik) {
        mFullNameNik = fullNameNik;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getIdCardNumber() {
        return mIdCardNumber;
    }

    public void setIdCardNumber(String idCardNumber) {
        mIdCardNumber = idCardNumber;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public Boolean getIsLeader() {
        return mIsLeader;
    }

    public void setIsLeader(Boolean isLeader) {
        mIsLeader = isLeader;
    }

    public Object getJoinDate() {
        return mJoinDate;
    }

    public void setJoinDate(Object joinDate) {
        mJoinDate = joinDate;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getNik() {
        return mNik;
    }

    public void setNik(String nik) {
        mNik = nik;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public String getReligion() {
        return mReligion;
    }

    public void setReligion(String religion) {
        mReligion = religion;
    }

    public String getUserID() {
        return mUserID;
    }

    public void setUserID(String userID) {
        mUserID = userID;
    }

    public void setOrganization(OrganizationEmployee organization){
        this.organization = organization;
    }

    public OrganizationEmployee getOrganization(){
        return organization;
    }

}
