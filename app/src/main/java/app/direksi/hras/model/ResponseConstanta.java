package app.direksi.hras.model;

import java.util.List;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseConstanta {

    @SerializedName("errCode")
    @Expose
    private int errCode;

    @SerializedName("data")
    @Expose
    private List<DataConstantaItem> dataConstanta;

    @SerializedName("errMessage")
    @Expose
    private String errMessage;

    public void setErrCode(int errCode) {
        this.errCode = errCode;
    }

    public int getErrCode() {
        return errCode;
    }

    public void setDataConstanta(List<DataConstantaItem> dataConstanta) {
        this.dataConstanta = dataConstanta;
    }

    public List<DataConstantaItem> getDataConstanta() {
        return dataConstanta;
    }

    public void setErrMessage(String errMessage) {
        this.errMessage = errMessage;
    }

    public String getErrMessage() {
        return errMessage;
    }

    @Override
    public String toString() {
        return
                "ResponseConstanta{" +
                        "errCode = '" + errCode + '\'' +
                        ",dataConstanta = '" + dataConstanta + '\'' +
                        ",errMessage = '" + errMessage + '\'' +
                        "}";
    }
}