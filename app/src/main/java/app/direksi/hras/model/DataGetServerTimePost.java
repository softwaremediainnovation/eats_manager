package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataGetServerTimePost {

	@SerializedName("dateNow")
	private String dateNow;

	@SerializedName("gmt")
	private int gmt;

	public String getDateNow(){
		return dateNow;
	}

	public int getGmt(){
		return gmt;
	}
}