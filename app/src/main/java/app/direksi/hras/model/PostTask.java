package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PostTask{


	public PostTask( String note, String dueDate, int creatorID, int employeeID,String taskName,
					 List<StringClass> addTaskDetailInputs ) {


		this.note = note;
		this.dueDate = dueDate;
		this.creatorID = creatorID;
		this.employeeID = employeeID;
		this.taskName = taskName;
		this.addTaskDetailInputs = addTaskDetailInputs;

	}


	@SerializedName("note")
	private String note;

	@SerializedName("dueDate")
	private String dueDate;

	@SerializedName("creatorID")
	private int creatorID;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("taskName")
	private String taskName;

	@SerializedName("addTaskDetailInputs")
	private List<StringClass> addTaskDetailInputs;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setDueDate(String dueDate){
		this.dueDate = dueDate;
	}

	public String getDueDate(){
		return dueDate;
	}

	public void setCreatorID(int creatorID){
		this.creatorID = creatorID;
	}

	public int getCreatorID(){
		return creatorID;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setTaskName(String taskName){
		this.taskName = taskName;
	}

	public String getTaskName(){
		return taskName;
	}

	public void setAddTaskDetailInputs(List<StringClass> addTaskDetailInputs){
		this.addTaskDetailInputs = addTaskDetailInputs;
	}

	public List<StringClass> getAddTaskDetailInputs(){
		return addTaskDetailInputs;
	}

	@Override
 	public String toString(){
		return 
			"PostTask{" + 
			"note = '" + note + '\'' + 
			",dueDate = '" + dueDate + '\'' + 
			",creatorID = '" + creatorID + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",taskName = '" + taskName + '\'' + 
			",addTaskDetailInputs = '" + addTaskDetailInputs + '\'' + 
			"}";
		}
}