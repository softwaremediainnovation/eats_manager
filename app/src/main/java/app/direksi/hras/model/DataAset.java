
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataAset {

    @SerializedName("assetFile")
    private String mAssetFile;
    @SerializedName("assetID")
    private Long mAssetID;
    @SerializedName("assetTrackings")
    private String mAssetTrackings;
    @SerializedName("creator")
    private String mCreator;
    @SerializedName("creatorID")
    private String mCreatorID;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("dateDeleted")
    private String mDateDeleted;
    @SerializedName("disposer")
    private String mDisposer;
    @SerializedName("disposerID")
    private String mDisposerID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("isDeleted")
    private Boolean mIsDeleted;
    @SerializedName("lastLocation")
    private String mLastLocation;
    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;

    public String getAssetFile() {
        return mAssetFile;
    }

    public void setAssetFile(String assetFile) {
        mAssetFile = assetFile;
    }

    public Long getAssetID() {
        return mAssetID;
    }

    public void setAssetID(Long assetID) {
        mAssetID = assetID;
    }

    public String getAssetTrackings() {
        return mAssetTrackings;
    }

    public void setAssetTrackings(String assetTrackings) {
        mAssetTrackings = assetTrackings;
    }

    public String getCreator() {
        return mCreator;
    }

    public void setCreator(String creator) {
        mCreator = creator;
    }

    public String getCreatorID() {
        return mCreatorID;
    }

    public void setCreatorID(String creatorID) {
        mCreatorID = creatorID;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public String getDateDeleted() {
        return mDateDeleted;
    }

    public void setDateDeleted(String dateDeleted) {
        mDateDeleted = dateDeleted;
    }

    public String getDisposer() {
        return mDisposer;
    }

    public void setDisposer(String disposer) {
        mDisposer = disposer;
    }

    public String getDisposerID() {
        return mDisposerID;
    }

    public void setDisposerID(String disposerID) {
        mDisposerID = disposerID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public Boolean getIsDeleted() {
        return mIsDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        mIsDeleted = isDeleted;
    }

    public String getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(String lastLocation) {
        mLastLocation = lastLocation;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

}
