package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class News{

	@SerializedName("news")
	@Expose
	private String news;

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("path")
	@Expose
	private String path;

	@SerializedName("articleID")
	@Expose
	private int articleID;

	@SerializedName("creatorID")
	@Expose
	private String creatorID;

	@SerializedName("photo")
	@Expose
	private String photo;

	@SerializedName("title")
	@Expose
	private String title;

	@SerializedName("isActive")
	@Expose
	private boolean isActive;

	public void setNews(String news){
		this.news = news;
	}

	public String getNews(){
		return news;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setPath(String path){
		this.path = path;
	}

	public String getPath(){
		return path;
	}

	public void setArticleID(int articleID){
		this.articleID = articleID;
	}

	public int getArticleID(){
		return articleID;
	}

	public void setCreatorID(String creatorID){
		this.creatorID = creatorID;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public void setPhoto(String photo){
		this.photo = photo;
	}

	public String getPhoto(){
		return photo;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	@Override
 	public String toString(){
		return 
			"News{" + 
			"news = '" + news + '\'' + 
			",date = '" + date + '\'' + 
			",path = '" + path + '\'' +
			",articleID = '" + articleID + '\'' + 
			",creatorID = '" + creatorID + '\'' +
			",photo = '" + photo + '\'' +
			",title = '" + title + '\'' + 
			",isActive = '" + isActive + '\'' + 
			"}";
		}
}