package app.direksi.hras.model;

import com.google.gson.annotations.Expose;

import javax.annotation.Generated;

import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataCompaniesItem {

    @SerializedName("companyID")
    @Expose
    private int companyID;

    @SerializedName("website")
    @Expose
    private String website;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("imageFile")
    @Expose
    private String imageFile;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("organizations")
    @Expose
    private String organizations;

    @SerializedName("logo")
    @Expose
    private String logo;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setOrganizations(String organizations) {
        this.organizations = organizations;
    }

    public String getOrganizations() {
        return organizations;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogo() {
        return logo;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return
                "DataCompaniesItem{" +
                        "companyID = '" + companyID + '\'' +
                        ",website = '" + website + '\'' +
                        ",address = '" + address + '\'' +
                        ",phone = '" + phone + '\'' +
                        ",imageFile = '" + imageFile + '\'' +
                        ",latitude = '" + latitude + '\'' +
                        ",name = '" + name + '\'' +
                        ",organizations = '" + organizations + '\'' +
                        ",logo = '" + logo + '\'' +
                        ",email = '" + email + '\'' +
                        ",longitude = '" + longitude + '\'' +
                        "}";
    }
}