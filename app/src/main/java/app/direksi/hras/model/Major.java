
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;



@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Major {


    @SerializedName("majorID")
    private Long mMajorID;
    @SerializedName("name")
    private String mName;



    public Long getMajorID() {
        return mMajorID;
    }

    public void setMajorID(Long majorID) {
        mMajorID = majorID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
