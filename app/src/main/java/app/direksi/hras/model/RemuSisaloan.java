package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemuSisaloan {

	@SerializedName("sisa")
	private Double sisa;

	@SerializedName("viewSisaLoanID")
	private int viewSisaLoanID;

	@SerializedName("employeeid")
	private int employeeid;

	public Double getSisa(){
		return sisa;
	}

	public int getViewSisaLoanID(){
		return viewSisaLoanID;
	}

	public int getEmployeeid(){
		return employeeid;
	}
}