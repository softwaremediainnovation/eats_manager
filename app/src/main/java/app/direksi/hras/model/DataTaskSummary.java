package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import app.direksi.hras.model.DataTask;

public class DataTaskSummary {

	@SerializedName("blmSelesai")
	private int blmSelesai;

	@SerializedName("expired")
	private int expired;

	@SerializedName("data")
	private List<DataTask> data;

	@SerializedName("sdhSelesai")
	private int sdhSelesai;

	public int getBlmSelesai(){
		return blmSelesai;
	}

	public int getExpired(){
		return expired;
	}

	public List<DataTask> getData(){
		return data;
	}

	public int getSdhSelesai(){
		return sdhSelesai;
	}
}