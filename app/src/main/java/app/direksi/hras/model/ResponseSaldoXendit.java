package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseSaldoXendit{

	@SerializedName("balance")
	private Integer balance;

	public void setBalance(Integer balance){
		this.balance = balance;
	}

	public Integer getBalance(){
		return balance;
	}

	@Override
 	public String toString(){
		return 
			"ResponseSaldoXendit{" + 
			"balance = '" + balance + '\'' + 
			"}";
		}
}