
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EmployeeInfoWorkingHour {

    @SerializedName("hari")
    private String mHari;
    @SerializedName("jamMasuk")
    private String mJamMasuk;
    @SerializedName("jamPulang")
    private String mJamPulang;

    public String getHari() {
        return mHari;
    }

    public void setHari(String hari) {
        mHari = hari;
    }

    public String getJamMasuk() {
        return mJamMasuk;
    }

    public void setJamMasuk(String jamMasuk) {
        mJamMasuk = jamMasuk;
    }

    public String getJamPulang() {
        return mJamPulang;
    }

    public void setJamPulang(String jamPulang) {
        mJamPulang = jamPulang;
    }

}
