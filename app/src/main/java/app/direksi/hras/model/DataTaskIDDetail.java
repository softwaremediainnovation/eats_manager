package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataTaskIDDetail {

	@SerializedName("header")
	private DataTaskIDHeader header;

	@SerializedName("detail")
	private List<DataTaskIDDetailItem> detail;

	@SerializedName("creator")
	private List<TaskCreator> creator;

	@SerializedName("organizations")
	private String organizations;

	@SerializedName("employees")
	private List<TaskEmployee> employees;

	@SerializedName("isExpired")
	private boolean isExpired;

	public void setIsExpired(boolean creator){
		this.isExpired = creator;
	}

	public boolean getIsExpired(){
		return isExpired;
	}

	public void setCreator(List<TaskCreator> creator){
		this.creator = creator;
	}

	public List<TaskCreator> getCreator(){
		return creator;
	}

	public void setOrganizations(String organizations){
		this.organizations = organizations;
	}

	public String getOrganizations(){
		return organizations;
	}

	public void setEmployees(List<TaskEmployee> employees){
		this.employees = employees;
	}

	public List<TaskEmployee> getEmployees(){
		return employees;
	}


	public void setHeader(DataTaskIDHeader header){
		this.header = header;
	}

	public DataTaskIDHeader getHeader(){
		return header;
	}

	public void setDetail(List<DataTaskIDDetailItem> detail){
		this.detail = detail;
	}

	public List<DataTaskIDDetailItem> getDetail(){
		return detail;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"header = '" + header + '\'' + 
			",detail = '" + detail + '\'' + 
			"}";
		}
}