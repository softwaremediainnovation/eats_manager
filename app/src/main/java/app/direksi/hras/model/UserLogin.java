package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class UserLogin {

	@SerializedName("permissions")
	@Expose
	private Permissions permissions;


	@SerializedName("lastName")
	@Expose
	private String lastName;

	@SerializedName("role")
	@Expose
	private String role;

	@SerializedName("address")
	@Expose
	private String address;

	@SerializedName("gender")
	@Expose
	private String gender;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("isActive")
	@Expose
	private boolean isActive;

	@SerializedName("nik")
	@Expose
	private String nik;

	@SerializedName("firstName")
	@Expose
	private String firstName;

	@SerializedName("photoUrl")
	@Expose
	private String photoUrl;

	@SerializedName("phone")
	@Expose
	private String phone;

	@SerializedName("signinToMobile")
	@Expose
	private boolean signinToMobile;

	@SerializedName("dob")
	@Expose
	private String dob;

	@SerializedName("id")
	@Expose
	private String id;

	@SerializedName("email")
	@Expose
	private String email;

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setRole(String role){
		this.role = role;
	}

	public String getRole(){
		return role;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setNik(String nik){
		this.nik = nik;
	}

	public String getNik(){
		return nik;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setPhotoUrl(String photoUrl){
		this.photoUrl = photoUrl;
	}

	public String getPhotoUrl(){
		return photoUrl;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setSigninToMobile(boolean signinToMobile){
		this.signinToMobile = signinToMobile;
	}

	public boolean isSigninToMobile(){
		return signinToMobile;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setPermissions(Permissions permissions){
		this.permissions = permissions;
	}

	public Permissions getPermissions(){
		return permissions;
	}

	@Override
 	public String toString(){
		return 
			"UserLogin{" +
			"lastName = '" + lastName + '\'' + 
			",role = '" + role + '\'' + 
			",address = '" + address + '\'' + 
			",gender = '" + gender + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",nik = '" + nik + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",photoUrl = '" + photoUrl + '\'' + 
			",phone = '" + phone + '\'' + 
			",signinToMobile = '" + signinToMobile + '\'' + 
			",dob = '" + dob + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}