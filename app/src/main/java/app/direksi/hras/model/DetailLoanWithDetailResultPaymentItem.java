package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DetailLoanWithDetailResultPaymentItem {

	@SerializedName("note")
	private String note;

	@SerializedName("amount")
	private Double amount;

	@SerializedName("isPayroll")
	private boolean isPayroll;

	@SerializedName("installmentPayout")
	private int installmentPayout;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("loanPaymentID")
	private int loanPaymentID;

	@SerializedName("loanID")
	private int loanID;

	@SerializedName("remainingLoan")
	private Double remainingLoan;

	@SerializedName("datePayment")
	private String datePayment;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setAmount(Double amount){
		this.amount = amount;
	}

	public Double getAmount(){
		return amount;
	}

	public void setIsPayroll(boolean isPayroll){
		this.isPayroll = isPayroll;
	}

	public boolean isIsPayroll(){
		return isPayroll;
	}

	public void setInstallmentPayout(int installmentPayout){
		this.installmentPayout = installmentPayout;
	}

	public int getInstallmentPayout(){
		return installmentPayout;
	}

	public void setCreatorID(String creatorID){
		this.creatorID = creatorID;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public void setLoanPaymentID(int loanPaymentID){
		this.loanPaymentID = loanPaymentID;
	}

	public int getLoanPaymentID(){
		return loanPaymentID;
	}

	public void setLoanID(int loanID){
		this.loanID = loanID;
	}

	public int getLoanID(){
		return loanID;
	}

	public void setRemainingLoan(Double remainingLoan){
		this.remainingLoan = remainingLoan;
	}

	public Double getRemainingLoan(){
		return remainingLoan;
	}

	public void setDatePayment(String datePayment){
		this.datePayment = datePayment;
	}

	public String getDatePayment(){
		return datePayment;
	}

	@Override
 	public String toString(){
		return 
			"ResultPaymentItem{" + 
			"note = '" + note + '\'' + 
			",amount = '" + amount + '\'' + 
			",isPayroll = '" + isPayroll + '\'' + 
			",installmentPayout = '" + installmentPayout + '\'' + 
			",creatorID = '" + creatorID + '\'' + 
			",loanPaymentID = '" + loanPaymentID + '\'' + 
			",loanID = '" + loanID + '\'' + 
			",remainingLoan = '" + remainingLoan + '\'' + 
			",datePayment = '" + datePayment + '\'' + 
			"}";
		}
}