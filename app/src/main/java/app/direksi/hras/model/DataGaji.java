
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataGaji {

    @SerializedName("angsuranPinjaman")
    private Double mAngsuranPinjaman;
    @SerializedName("bpjsKesehatan")
    private Double mBpjsKesehatan;
    @SerializedName("creatorID")
    private Object mCreatorID;
    @SerializedName("creatorName")
    private Object mCreatorName;
    @SerializedName("date")
    private String mDate;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("gajiPokok")
    private Double mGajiPokok;
    @SerializedName("ijinKeluarKantor")
    private Double mIjinKeluarKantor;
    @SerializedName("ijinKeluarKantorTime")
    private Double mIjinKeluarKantorTime;
    @SerializedName("ijinTidakMasuk")
    private Double mIjinTidakMasuk;
    @SerializedName("ijinTidakMasukTime")
    private Double mIjinTidakMasukTime;
    @SerializedName("insentif")
    private Double mInsentif;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("isDelivered")
    private Boolean mIsDelivered;
    @SerializedName("isModified")
    private Boolean mIsModified;
    @SerializedName("jaminanPensiun")
    private Double mJaminanPensiun;
    @SerializedName("jamsostek")
    private Double mJamsostek;
    @SerializedName("koreksiGaji")
    private Double mKoreksiGaji;
    @SerializedName("lembur1")
    private Double mLembur1;
    @SerializedName("lembur1Time")
    private Double mLembur1Time;
    @SerializedName("lembur2")
    private Double mLembur2;
    @SerializedName("lembur2Time")
    private Double mLembur2Time;
    @SerializedName("lembur3")
    private Double mLembur3;
    @SerializedName("lembur3Time")
    private Double mLembur3Time;
    @SerializedName("notes")
    private String mNotes;
    @SerializedName("payroll")
    private DataGajiPayroll mPayroll;
    @SerializedName("payrollID")
    private Long mPayrollID;
    @SerializedName("pbonMingguan")
    private Boolean mPbonMingguan;
    @SerializedName("pph21")
    private Double mPph21;
    @SerializedName("premi1")
    private Double mPremi1;
    @SerializedName("premi2")
    private Double mPremi2;
    @SerializedName("ptkp")
    private Double mPtkp;
    @SerializedName("remainingLoan")
    private Double mRemainingLoan;
    @SerializedName("remunerationID")
    private Long mRemunerationID;
    @SerializedName("remunerationNominalID")
    private Long mRemunerationNominalID;
    @SerializedName("terlambat")
    private Double mTerlambat;
    @SerializedName("terlambatTime")
    private Double mTerlambatTime;
    @SerializedName("totalLoan")
    private Double mTotalLoan;
    @SerializedName("tunjanganAdminBank")
    private Double mTunjanganAdminBank;
    @SerializedName("tunjanganLainLain")
    private Double mTunjanganLainLain;
    @SerializedName("tunjanganTetap")
    private Double mTunjanganTetap;
    @SerializedName("uangMakan")
    private Double mUangMakan;
    @SerializedName("uangMakanLembur")
    private Double mUangMakanLembur;
    @SerializedName("uangMakanLemburTime")
    private Double mUangMakanLemburTime;

    public Double getAngsuranPinjaman() {
        return mAngsuranPinjaman;
    }

    public void setAngsuranPinjaman(Double angsuranPinjaman) {
        mAngsuranPinjaman = angsuranPinjaman;
    }

    public Double getBpjsKesehatan() {
        return mBpjsKesehatan;
    }

    public void setBpjsKesehatan(Double bpjsKesehatan) {
        mBpjsKesehatan = bpjsKesehatan;
    }

    public Object getCreatorID() {
        return mCreatorID;
    }

    public void setCreatorID(Object creatorID) {
        mCreatorID = creatorID;
    }

    public Object getCreatorName() {
        return mCreatorName;
    }

    public void setCreatorName(Object creatorName) {
        mCreatorName = creatorName;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Double getGajiPokok() {
        return mGajiPokok;
    }

    public void setGajiPokok(Double gajiPokok) {
        mGajiPokok = gajiPokok;
    }

    public Double getIjinKeluarKantor() {
        return mIjinKeluarKantor;
    }

    public void setIjinKeluarKantor(Double ijinKeluarKantor) {
        mIjinKeluarKantor = ijinKeluarKantor;
    }

    public Double getIjinKeluarKantorTime() {
        return mIjinKeluarKantorTime;
    }

    public void setIjinKeluarKantorTime(Double ijinKeluarKantorTime) {
        mIjinKeluarKantorTime = ijinKeluarKantorTime;
    }

    public Double getIjinTidakMasuk() {
        return mIjinTidakMasuk;
    }

    public void setIjinTidakMasuk(Double ijinTidakMasuk) {
        mIjinTidakMasuk = ijinTidakMasuk;
    }

    public Double getIjinTidakMasukTime() {
        return mIjinTidakMasukTime;
    }

    public void setIjinTidakMasukTime(Double ijinTidakMasukTime) {
        mIjinTidakMasukTime = ijinTidakMasukTime;
    }

    public Double getInsentif() {
        return mInsentif;
    }

    public void setInsentif(Double insentif) {
        mInsentif = insentif;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public Boolean getIsDelivered() {
        return mIsDelivered;
    }

    public void setIsDelivered(Boolean isDelivered) {
        mIsDelivered = isDelivered;
    }

    public Boolean getIsModified() {
        return mIsModified;
    }

    public void setIsModified(Boolean isModified) {
        mIsModified = isModified;
    }

    public Double getJaminanPensiun() {
        return mJaminanPensiun;
    }

    public void setJaminanPensiun(Double jaminanPensiun) {
        mJaminanPensiun = jaminanPensiun;
    }

    public Double getJamsostek() {
        return mJamsostek;
    }

    public void setJamsostek(Double jamsostek) {
        mJamsostek = jamsostek;
    }

    public Double getKoreksiGaji() {
        return mKoreksiGaji;
    }

    public void setKoreksiGaji(Double koreksiGaji) {
        mKoreksiGaji = koreksiGaji;
    }

    public Double getLembur1() {
        return mLembur1;
    }

    public void setLembur1(Double lembur1) {
        mLembur1 = lembur1;
    }

    public Double getLembur1Time() {
        return mLembur1Time;
    }

    public void setLembur1Time(Double lembur1Time) {
        mLembur1Time = lembur1Time;
    }

    public Double getLembur2() {
        return mLembur2;
    }

    public void setLembur2(Double lembur2) {
        mLembur2 = lembur2;
    }

    public Double getLembur2Time() {
        return mLembur2Time;
    }

    public void setLembur2Time(Double lembur2Time) {
        mLembur2Time = lembur2Time;
    }

    public Double getLembur3() {
        return mLembur3;
    }

    public void setLembur3(Double lembur3) {
        mLembur3 = lembur3;
    }

    public Double getLembur3Time() {
        return mLembur3Time;
    }

    public void setLembur3Time(Double lembur3Time) {
        mLembur3Time = lembur3Time;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String notes) {
        mNotes = notes;
    }

    public DataGajiPayroll getPayroll() {
        return mPayroll;
    }

    public void setPayroll(DataGajiPayroll payroll) {
        mPayroll = payroll;
    }

    public Long getPayrollID() {
        return mPayrollID;
    }

    public void setPayrollID(Long payrollID) {
        mPayrollID = payrollID;
    }

    public Boolean getPbonMingguan() {
        return mPbonMingguan;
    }

    public void setPbonMingguan(Boolean pbonMingguan) {
        mPbonMingguan = pbonMingguan;
    }

    public Double getPph21() {
        return mPph21;
    }

    public void setPph21(Double pph21) {
        mPph21 = pph21;
    }

    public Double getPremi1() {
        return mPremi1;
    }

    public void setPremi1(Double premi1) {
        mPremi1 = premi1;
    }

    public Double getPremi2() {
        return mPremi2;
    }

    public void setPremi2(Double premi2) {
        mPremi2 = premi2;
    }

    public Double getPtkp() {
        return mPtkp;
    }

    public void setPtkp(Double ptkp) {
        mPtkp = ptkp;
    }

    public Double getRemainingLoan() {
        return mRemainingLoan;
    }

    public void setRemainingLoan(Double remainingLoan) {
        mRemainingLoan = remainingLoan;
    }

    public Long getRemunerationID() {
        return mRemunerationID;
    }

    public void setRemunerationID(Long remunerationID) {
        mRemunerationID = remunerationID;
    }

    public Long getRemunerationNominalID() {
        return mRemunerationNominalID;
    }

    public void setRemunerationNominalID(Long remunerationNominalID) {
        mRemunerationNominalID = remunerationNominalID;
    }

    public Double getTerlambat() {
        return mTerlambat;
    }

    public void setTerlambat(Double terlambat) {
        mTerlambat = terlambat;
    }

    public Double getTerlambatTime() {
        return mTerlambatTime;
    }

    public void setTerlambatTime(Double terlambatTime) {
        mTerlambatTime = terlambatTime;
    }

    public Double getTotalLoan() {
        return mTotalLoan;
    }

    public void setTotalLoan(Double totalLoan) {
        mTotalLoan = totalLoan;
    }

    public Double getTunjanganAdminBank() {
        return mTunjanganAdminBank;
    }

    public void setTunjanganAdminBank(Double tunjanganAdminBank) {
        mTunjanganAdminBank = tunjanganAdminBank;
    }

    public Double getTunjanganLainLain() {
        return mTunjanganLainLain;
    }

    public void setTunjanganLainLain(Double tunjanganLainLain) {
        mTunjanganLainLain = tunjanganLainLain;
    }

    public Double getTunjanganTetap() {
        return mTunjanganTetap;
    }

    public void setTunjanganTetap(Double tunjanganTetap) {
        mTunjanganTetap = tunjanganTetap;
    }

    public Double getUangMakan() {
        return mUangMakan;
    }

    public void setUangMakan(Double uangMakan) {
        mUangMakan = uangMakan;
    }

    public Double getUangMakanLembur() {
        return mUangMakanLembur;
    }

    public void setUangMakanLembur(Double uangMakanLembur) {
        mUangMakanLembur = uangMakanLembur;
    }

    public Double getUangMakanLemburTime() {
        return mUangMakanLemburTime;
    }

    public void setUangMakanLemburTime(Double uangMakanLemburTime) {
        mUangMakanLemburTime = uangMakanLemburTime;
    }

}
