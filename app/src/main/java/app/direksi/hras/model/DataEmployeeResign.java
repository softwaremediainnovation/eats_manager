
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataEmployeeResign {

    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("serialNumber")
    private String mSerialNumber;
    @SerializedName("distance")
    private String mDistance;
    @SerializedName("attendanceMethod")
    private String attendanceMethod;

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public void setSerialNumber(String longitude) {
        mSerialNumber = longitude;
    }

    public String getDistance() {
        return mDistance;
    }

    public void setDistance(String longitude) {
        mDistance = longitude;
    }

    public String getAttendanceMethod() {
        return attendanceMethod;
    }

    public void setAttendanceMethod(String attendanceMethods) {
        attendanceMethod = attendanceMethods;
    }

}
