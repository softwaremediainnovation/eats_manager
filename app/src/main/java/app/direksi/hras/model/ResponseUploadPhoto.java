
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseUploadPhoto {

    @SerializedName("data")
    private DataUploadPhoto mData;
    @SerializedName("errCode")
    private Long mErrCode;
    @SerializedName("errMessage")
    private String mErrMessage;

    public DataUploadPhoto getData() {
        return mData;
    }

    public void setData(DataUploadPhoto data) {
        mData = data;
    }

    public Long getErrCode() {
        return mErrCode;
    }

    public void setErrCode(Long errCode) {
        mErrCode = errCode;
    }

    public String getErrMessage() {
        return mErrMessage;
    }

    public void setErrMessage(String errMessage) {
        mErrMessage = errMessage;
    }

}
