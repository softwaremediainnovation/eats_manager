package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataReimbursCategoryItem{

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("reimbursementCategoryID")
	@Expose
	private int reimbursementCategoryID;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setReimbursementCategoryID(int reimbursementCategoryID){
		this.reimbursementCategoryID = reimbursementCategoryID;
	}

	public int getReimbursementCategoryID(){
		return reimbursementCategoryID;
	}

	@Override
 	public String toString(){
		return 
			"DataReimbursCategoryItem{" + 
			"note = '" + note + '\'' + 
			",name = '" + name + '\'' + 
			",reimbursementCategoryID = '" + reimbursementCategoryID + '\'' + 
			"}";
		}
}