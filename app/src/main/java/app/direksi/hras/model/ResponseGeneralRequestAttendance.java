package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseGeneralRequestAttendance{

	@SerializedName("data")
	private GeneralRequestAttendance data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public void setData(GeneralRequestAttendance data){
		this.data = data;
	}

	public GeneralRequestAttendance getData(){
		return data;
	}

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseGeneralRequestAttendance{" + 
			"data = '" + data + '\'' + 
			",errCode = '" + errCode + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}