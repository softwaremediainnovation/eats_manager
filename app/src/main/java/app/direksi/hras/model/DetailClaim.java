
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailClaim {

    @SerializedName("amount")
    private Long mAmount;
    @SerializedName("approverName")
    private String mApprover;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("filePathHtml")
    private String mFilePathHtml;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("isApprovedHtml")
    private String mIsApprovedHtml;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reimbursementCategoryName")
    private String mReimbursementCategory;
    @SerializedName("reimbursementCategoryID")
    private Long mReimbursementCategoryID;
    @SerializedName("reimbursementFile")
    private String mReimbursementFile;
    @SerializedName("reimbursementID")
    private Long mReimbursementID;
    @SerializedName("employee")
    private Employee mEmployee;

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long amount) {
        mAmount = amount;
    }

    public String getApprover() {
        return mApprover;
    }

    public void setApprover(String approver) {
        mApprover = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employee) {
        mEmployeeName = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getFilePathHtml() {
        return mFilePathHtml;
    }

    public void setFilePathHtml(String filePathHtml) {
        mFilePathHtml = filePathHtml;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public String getIsApprovedHtml() {
        return mIsApprovedHtml;
    }

    public void setIsApprovedHtml(String isApprovedHtml) {
        mIsApprovedHtml = isApprovedHtml;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getReimbursementCategory() {
        return mReimbursementCategory;
    }

    public void setReimbursementCategory(String reimbursementCategory) {
        mReimbursementCategory = reimbursementCategory;
    }

    public Long getReimbursementCategoryID() {
        return mReimbursementCategoryID;
    }

    public void setReimbursementCategoryID(Long reimbursementCategoryID) {
        mReimbursementCategoryID = reimbursementCategoryID;
    }

    public String getReimbursementFile() {
        return mReimbursementFile;
    }

    public void setReimbursementFile(String reimbursementFile) {
        mReimbursementFile = reimbursementFile;
    }

    public Long getReimbursementID() {
        return mReimbursementID;
    }

    public void setReimbursementID(Long reimbursementID) {
        mReimbursementID = reimbursementID;
    }

    public Employee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Employee employee) {
        mEmployee = employee;
    }

}
