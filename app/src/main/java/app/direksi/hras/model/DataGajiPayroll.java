
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataGajiPayroll {

    @SerializedName("checkerID")
    private Object mCheckerID;
    @SerializedName("checkerName")
    private Object mCheckerName;
    @SerializedName("date")
    private String mDate;
    @SerializedName("payrollID")
    private Long mPayrollID;
    @SerializedName("totalSalary")
    private Double mTotalSalary;

    public Object getCheckerID() {
        return mCheckerID;
    }

    public void setCheckerID(Object checkerID) {
        mCheckerID = checkerID;
    }

    public Object getCheckerName() {
        return mCheckerName;
    }

    public void setCheckerName(Object checkerName) {
        mCheckerName = checkerName;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Long getPayrollID() {
        return mPayrollID;
    }

    public void setPayrollID(Long payrollID) {
        mPayrollID = payrollID;
    }

    public Double getTotalSalary() {
        return mTotalSalary;
    }

    public void setTotalSalary(Double totalSalary) {
        mTotalSalary = totalSalary;
    }

}
