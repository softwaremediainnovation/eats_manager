package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class RelativeItemEmployeeShare {

	@SerializedName("gender")
	private String gender;

	@SerializedName("educationLevel")
	private Object educationLevel;

	@SerializedName("name")
	private String name;

	@SerializedName("employeeRelativeID")
	private int employeeRelativeID;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("ageHtml")
	private String ageHtml;

	@SerializedName("job")
	private String job;

	@SerializedName("employee")
	private Object employee;

	@SerializedName("educationLevelID")
	private int educationLevelID;

	@SerializedName("age")
	private int age;

	@SerializedName("relation")
	private String relation;

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setEducationLevel(Object educationLevel){
		this.educationLevel = educationLevel;
	}

	public Object getEducationLevel(){
		return educationLevel;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmployeeRelativeID(int employeeRelativeID){
		this.employeeRelativeID = employeeRelativeID;
	}

	public int getEmployeeRelativeID(){
		return employeeRelativeID;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setAgeHtml(String ageHtml){
		this.ageHtml = ageHtml;
	}

	public String getAgeHtml(){
		return ageHtml;
	}

	public void setJob(String job){
		this.job = job;
	}

	public String getJob(){
		return job;
	}

	public void setEmployee(Object employee){
		this.employee = employee;
	}

	public Object getEmployee(){
		return employee;
	}

	public void setEducationLevelID(int educationLevelID){
		this.educationLevelID = educationLevelID;
	}

	public int getEducationLevelID(){
		return educationLevelID;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}

	public void setRelation(String relation){
		this.relation = relation;
	}

	public String getRelation(){
		return relation;
	}

	@Override
 	public String toString(){
		return 
			"RelativeItem{" + 
			"gender = '" + gender + '\'' + 
			",educationLevel = '" + educationLevel + '\'' + 
			",name = '" + name + '\'' + 
			",employeeRelativeID = '" + employeeRelativeID + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",ageHtml = '" + ageHtml + '\'' + 
			",job = '" + job + '\'' + 
			",employee = '" + employee + '\'' + 
			",educationLevelID = '" + educationLevelID + '\'' + 
			",age = '" + age + '\'' + 
			",relation = '" + relation + '\'' + 
			"}";
		}
}