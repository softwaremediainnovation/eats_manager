package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataListOrgz {

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("name")
	private String name;

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"organizationID = '" + organizationID + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}
}