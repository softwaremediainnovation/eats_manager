package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Headers{

	@SerializedName("Content-Disposition")
	@Expose
	private List<String> contentDisposition;

	@SerializedName("Content-Type")
	@Expose
	private List<String> contentType;

	public void setContentDisposition(List<String> contentDisposition){
		this.contentDisposition = contentDisposition;
	}

	public List<String> getContentDisposition(){
		return contentDisposition;
	}

	public void setContentType(List<String> contentType){
		this.contentType = contentType;
	}

	public List<String> getContentType(){
		return contentType;
	}

	@Override
 	public String toString(){
		return 
			"Headers{" + 
			"content-Disposition = '" + contentDisposition + '\'' + 
			",content-Type = '" + contentType + '\'' + 
			"}";
		}
}