package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseReimbursement{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private List<DataReimbursementItem> dataReimbursement;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataReimbursement(List<DataReimbursementItem> dataReimbursement){
		this.dataReimbursement = dataReimbursement;
	}

	public List<DataReimbursementItem> getDataReimbursement(){
		return dataReimbursement;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseReimbursement{" + 
			"errCode = '" + errCode + '\'' + 
			",dataReimbursement = '" + dataReimbursement + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}