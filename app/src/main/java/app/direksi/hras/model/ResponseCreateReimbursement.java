package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseCreateReimbursement{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private DataCreateReimbursement dataCreateReimbursement;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataCreateReimbursement(DataCreateReimbursement dataCreateReimbursement){
		this.dataCreateReimbursement = dataCreateReimbursement;
	}

	public DataCreateReimbursement getDataCreateReimbursement(){
		return dataCreateReimbursement;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponseCreateReimbursement{" + 
			"errCode = '" + errCode + '\'' + 
			",dataCreateReimbursement = '" + dataCreateReimbursement + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}