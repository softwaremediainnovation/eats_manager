package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PostTaskDetailInputsItem {

	@SerializedName("note")
	private String note;

	@SerializedName("taskName")
	private String taskName;

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setTaskName(String taskName){
		this.taskName = taskName;
	}

	public String getTaskName(){
		return taskName;
	}

	@Override
 	public String toString(){
		return 
			"AddTaskDetailInputsItem{" + 
			"note = '" + note + '\'' + 
			",taskName = '" + taskName + '\'' + 
			"}";
		}
}