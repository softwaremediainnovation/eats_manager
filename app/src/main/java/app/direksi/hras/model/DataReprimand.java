
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataReprimand {

    @SerializedName("date")
    private String mDate;
    @SerializedName("employee")
    private DataReprimandEmployee mEmployee;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("issuer")
    private DataReprimandIssuer mIssuer;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reprimandCategory")
    private DataReprimandCategory mReprimandCategory;
    @SerializedName("reprimandID")
    private Long mReprimandID;
    @SerializedName("organization")
    private String organization;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public DataReprimandEmployee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataReprimandEmployee employee) {
        mEmployee = employee;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public DataReprimandIssuer getIssuer() {
        return mIssuer;
    }

    public void setIssuer(DataReprimandIssuer issuer) {
        mIssuer = issuer;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public DataReprimandCategory getReprimandCategory() {
        return mReprimandCategory;
    }

    public void setReprimandCategory(DataReprimandCategory reprimandCategory) {
        mReprimandCategory = reprimandCategory;
    }

    public Long getReprimandID() {
        return mReprimandID;
    }

    public void setReprimandID(Long reprimandID) {
        mReprimandID = reprimandID;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String note) {
        organization = note;
    }

}
