package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataPostreimbursement{

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("approver")
	@Expose
	private String approver;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("amount")
	@Expose
	private int amount;

	@SerializedName("approverID")
	@Expose
	private String approverID;

	@SerializedName("filePath")
	@Expose
	private String filePath;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("reimbursementCategory")
	@Expose
	private String reimbursementCategory;

	@SerializedName("employee")
	@Expose
	private String employee;

	@SerializedName("reimbursementFile")
	@Expose
	private String reimbursementFile;

	@SerializedName("filePathHtml")
	@Expose
	private String filePathHtml;

	@SerializedName("dateApproved")
	@Expose
	private String dateApproved;

	@SerializedName("isApprovedHtml")
	@Expose
	private String isApprovedHtml;

	@SerializedName("isApproved")
	@Expose
	private String isApproved;

	@SerializedName("reimbursementID")
	@Expose
	private int reimbursementID;

	@SerializedName("reimbursementCategoryID")
	@Expose
	private int reimbursementCategoryID;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setApprover(String approver){
		this.approver = approver;
	}

	public String getApprover(){
		return approver;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setApproverID(String approverID){
		this.approverID = approverID;
	}

	public String getApproverID(){
		return approverID;
	}

	public void setFilePath(String filePath){
		this.filePath = filePath;
	}

	public String getFilePath(){
		return filePath;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setReimbursementCategory(String reimbursementCategory){
		this.reimbursementCategory = reimbursementCategory;
	}

	public String getReimbursementCategory(){
		return reimbursementCategory;
	}

	public void setEmployee(String employee){
		this.employee = employee;
	}

	public String getEmployee(){
		return employee;
	}

	public void setReimbursementFile(String reimbursementFile){
		this.reimbursementFile = reimbursementFile;
	}

	public String getReimbursementFile(){
		return reimbursementFile;
	}

	public void setFilePathHtml(String filePathHtml){
		this.filePathHtml = filePathHtml;
	}

	public String getFilePathHtml(){
		return filePathHtml;
	}

	public void setDateApproved(String dateApproved){
		this.dateApproved = dateApproved;
	}

	public String getDateApproved(){
		return dateApproved;
	}

	public void setIsApprovedHtml(String isApprovedHtml){
		this.isApprovedHtml = isApprovedHtml;
	}

	public String getIsApprovedHtml(){
		return isApprovedHtml;
	}

	public void setIsApproved(String isApproved){
		this.isApproved = isApproved;
	}

	public String getIsApproved(){
		return isApproved;
	}

	public void setReimbursementID(int reimbursementID){
		this.reimbursementID = reimbursementID;
	}

	public int getReimbursementID(){
		return reimbursementID;
	}

	public void setReimbursementCategoryID(int reimbursementCategoryID){
		this.reimbursementCategoryID = reimbursementCategoryID;
	}

	public int getReimbursementCategoryID(){
		return reimbursementCategoryID;
	}

	@Override
 	public String toString(){
		return 
			"DataPostreimbursement{" + 
			"date = '" + date + '\'' + 
			",approver = '" + approver + '\'' + 
			",note = '" + note + '\'' + 
			",amount = '" + amount + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",filePath = '" + filePath + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",reimbursementCategory = '" + reimbursementCategory + '\'' + 
			",employee = '" + employee + '\'' + 
			",reimbursementFile = '" + reimbursementFile + '\'' + 
			",filePathHtml = '" + filePathHtml + '\'' + 
			",dateApproved = '" + dateApproved + '\'' + 
			",isApprovedHtml = '" + isApprovedHtml + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			",reimbursementID = '" + reimbursementID + '\'' + 
			",reimbursementCategoryID = '" + reimbursementCategoryID + '\'' + 
			"}";
		}
}