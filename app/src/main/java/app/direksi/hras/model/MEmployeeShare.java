package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class MEmployeeShare {

	@SerializedName("majorID")
	private int majorID;

	@SerializedName("name")
	private String name;

	@SerializedName("employeeEducations")
	private Object employeeEducations;

	public void setMajorID(int majorID){
		this.majorID = majorID;
	}

	public int getMajorID(){
		return majorID;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setEmployeeEducations(Object employeeEducations){
		this.employeeEducations = employeeEducations;
	}

	public Object getEmployeeEducations(){
		return employeeEducations;
	}

	@Override
 	public String toString(){
		return 
			"M{" + 
			"majorID = '" + majorID + '\'' + 
			",name = '" + name + '\'' + 
			",employeeEducations = '" + employeeEducations + '\'' + 
			"}";
		}
}