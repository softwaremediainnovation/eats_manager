
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDetailAsetGeneral {

    @SerializedName("assetID")
    private Long mAssetID;
    @SerializedName("assetTrackings")
    private List<DataDetailAssetsAssetTracking> mAssetTrackings;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("employee")
    private DataDetailAssetsEmployee mEmployee;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("lastLocation")
    private String mLastLocation;
    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("expiredDate")
    private String mExpiredDate;
    @SerializedName("constantApi")
    private DataConstantaAsset constanta;

    public Long getAssetID() {
        return mAssetID;
    }

    public void setAssetID(Long assetID) {
        mAssetID = assetID;
    }

    public List<DataDetailAssetsAssetTracking> getAssetTrackings() {
        return mAssetTrackings;
    }

    public void setAssetTrackings(List<DataDetailAssetsAssetTracking> assetTrackings) {
        mAssetTrackings = assetTrackings;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public DataDetailAssetsEmployee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataDetailAssetsEmployee employee) {
        mEmployee = employee;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public String getLastLocation() {
        return mLastLocation;
    }

    public void setLastLocation(String lastLocation) {
        mLastLocation = lastLocation;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getExpiredDate() {
        return mExpiredDate;
    }

    public void setExpiredDate(String note) {
        mExpiredDate = note;
    }

    public DataConstantaAsset getConstanta() {
        return constanta;
    }

    public void setConstanta(DataConstantaAsset note) {
        constanta = note;
    }

}
