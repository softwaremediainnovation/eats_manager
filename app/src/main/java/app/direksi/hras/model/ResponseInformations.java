package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseInformations{

	@SerializedName("data")
	private DataInformations data;

	@SerializedName("errCode")
	private int errCode;

	@SerializedName("errMessage")
	private String errMessage;

	public DataInformations getData(){
		return data;
	}

	public int getErrCode(){
		return errCode;
	}

	public String getErrMessage(){
		return errMessage;
	}
}