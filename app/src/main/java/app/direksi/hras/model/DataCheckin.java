
package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataCheckin {

    @SerializedName("attendanceID")
    private Integer mAttendanceID;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("dateTime")
    private String mDateTime;
    @SerializedName("employeeID")
    private Integer mEmployeeID;
    @SerializedName("isLate")
    private Boolean mIsLate;
    @SerializedName("isMobileApp")
    private Boolean mIsMobileApp;
    @SerializedName("latitude")
    private String mLatitude;
    @SerializedName("longitude")
    private String mLongitude;
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private String result;

    public DataCheckin(String latlong) {

        this.mLatitude = latlong;
        this.mLongitude = latlong;

    }

    public Integer getAttendanceID() {
        return mAttendanceID;
    }

    public void setAttendanceID(Integer attendanceID) {
        mAttendanceID = attendanceID;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public String getDateTime() {
        return mDateTime;
    }

    public void setDateTime(String dateTime) {
        mDateTime = dateTime;
    }

    public Integer getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Integer employeeID) {
        mEmployeeID = employeeID;
    }

    public Boolean getIsLate() {
        return mIsLate;
    }

    public void setIsLate(Boolean isLate) {
        mIsLate = isLate;
    }

    public Boolean getIsMobileApp() {
        return mIsMobileApp;
    }

    public void setIsMobileApp(Boolean isMobileApp) {
        mIsMobileApp = isMobileApp;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public void setLatitude(String latitude) {
        mLatitude = latitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public void setLongitude(String longitude) {
        mLongitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String longitude) {
        status = longitude;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String longitude) {
        result = longitude;
    }

}
