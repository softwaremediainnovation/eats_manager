package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataOvertimeAttendance {

    @SerializedName("date")
    private String date;

    @SerializedName("employeeID")
    private int employeeID;

    @SerializedName("attendanceOverviewID")
    private int attendanceOverviewID;

    @SerializedName("employee")
    private String employee;

    @SerializedName("timeIn")
    private String timeIn;

    @SerializedName("timeOut")
    private String timeOut;

    @SerializedName("wpIn")
    private String wpIn;

    @SerializedName("wpOut")
    private String wpOut;

    public String getDate(){
        return date;
    }

    public int getEmployeeID(){
        return employeeID;
    }

    public int getAttendanceOverviewID(){
        return attendanceOverviewID;
    }

    public String getEmployee(){
        return employee;
    }

    public String getTimeIn(){
        return timeIn;
    }

    public String getTimeOut(){
        return timeOut;
    }

    public String getWpIn(){
        return wpIn;
    }

    public String getWpOut(){
        return wpOut;
    }
}