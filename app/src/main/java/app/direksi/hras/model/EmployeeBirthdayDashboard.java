
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EmployeeBirthdayDashboard {

    @SerializedName("dob")
    private String mDob;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("organization")
    private String mOrganization;
    @SerializedName("company")
    private String mCompany;

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        mEmployeeName = employeeName;
    }

    public String getOrganization() {
        return mOrganization;
    }

    public void setOrganization(String dob) {
        mOrganization = dob;
    }

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String employeeName) {
        mCompany = employeeName;
    }

}
