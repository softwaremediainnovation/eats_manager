package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class ResponseDataOvertimeAttendance{

    @SerializedName("data")
    private DataOvertimeAttendance data;

    @SerializedName("errCode")
    private int errCode;

    @SerializedName("errMessage")
    private String errMessage;

    public DataOvertimeAttendance getData(){
        return data;
    }

    public int getErrCode(){
        return errCode;
    }

    public String getErrMessage(){
        return errMessage;
    }
}