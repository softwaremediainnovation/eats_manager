package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataLastReprimand {

	@SerializedName("date")
	private String date;

	@SerializedName("note")
	private String note;

	@SerializedName("filePath")
	private String filePath;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("employee")
	private String employee;

	@SerializedName("issuer")
	private String issuer;

	@SerializedName("organizationID")
	private String organizationID;

	@SerializedName("employeeIssuer")
	private String employeeIssuer;

	@SerializedName("issuerID")
	private String issuerID;

	@SerializedName("companyID")
	private String companyID;

	@SerializedName("organization")
	private String organization;

	@SerializedName("reprimandID")
	private int reprimandID;

	@SerializedName("category")
	private String category;

	@SerializedName("reprimandCategoryID")
	private int reprimandCategoryID;

	@SerializedName("reprimandCategory")
	private String reprimandCategory;

	public String getDate(){
		return date;
	}

	public String getNote(){
		return note;
	}

	public String getFilePath(){
		return filePath;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public String getEmployee(){
		return employee;
	}

	public String getIssuer(){
		return issuer;
	}

	public String getOrganizationID(){
		return organizationID;
	}

	public String getEmployeeIssuer(){
		return employeeIssuer;
	}

	public String getIssuerID(){
		return issuerID;
	}

	public String getCompanyID(){
		return companyID;
	}

	public String getOrganization(){
		return organization;
	}

	public int getReprimandID(){
		return reprimandID;
	}

	public String getCategory(){
		return category;
	}

	public int getReprimandCategoryID(){
		return reprimandCategoryID;
	}

	public String getReprimandCategory(){
		return reprimandCategory;
	}
}