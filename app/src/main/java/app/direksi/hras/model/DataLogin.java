package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataLogin{

	@SerializedName("value")
	@Expose
	private String value;

	@SerializedName("user")
	@Expose
	private UserLogin user;

	public void setValue(String value){
		this.value = value;
	}

	public String getValue(){
		return value;
	}

	public void setUser(UserLogin user){
		this.user = user;
	}

	public UserLogin getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"DataLogin{" + 
			"value = '" + value + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}