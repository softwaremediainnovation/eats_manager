
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataOrganizationEmploye {

    @SerializedName("dateEnd")
    private String mDateEnd;
    @SerializedName("dateStart")
    private String mDateStart;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("employeeOrganizationID")
    private Long mEmployeeOrganizationID;
    @SerializedName("name")
    private String mName;

    public String getDateEnd() {
        return mDateEnd;
    }

    public void setDateEnd(String dateEnd) {
        mDateEnd = dateEnd;
    }

    public String getDateStart() {
        return mDateStart;
    }

    public void setDateStart(String dateStart) {
        mDateStart = dateStart;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Long getEmployeeOrganizationID() {
        return mEmployeeOrganizationID;
    }

    public void setEmployeeOrganizationID(Long employeeOrganizationID) {
        mEmployeeOrganizationID = employeeOrganizationID;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

}
