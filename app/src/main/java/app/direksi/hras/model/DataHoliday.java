package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataHoliday {

	@SerializedName("keterangan")
	private String keterangan;

	@SerializedName("tanggal")
	private String tanggal;

	@SerializedName("jamMasuk")
	private String jamMasuk;

	@SerializedName("jamPulang")
	private String jamPulang;

	@SerializedName("isHoliday")
	private Boolean isHoliday;

	public void setKeterangan(String keterangan){
		this.keterangan = keterangan;
	}

	public String getKeterangan(){
		return keterangan;
	}

	public void setTanggal(String tanggal){
		this.tanggal = tanggal;
	}

	public String getTanggal(){
		return tanggal;
	}

	public void setJamMasuk(String tanggal){
		this.jamMasuk = tanggal;
	}

	public String getJamMasuk(){
		return jamMasuk;
	}

	public void setJamPulang(String tanggal){
		this.jamPulang = tanggal;
	}

	public String getJamPulang(){
		return jamPulang;
	}

	public void setIsHoliday(Boolean tanggal){
		this.isHoliday = tanggal;
	}

	public Boolean getIsHoliday(){
		return isHoliday;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"keterangan = '" + keterangan + '\'' + 
			",tanggal = '" + tanggal + '\'' + 
			"}";
		}
}