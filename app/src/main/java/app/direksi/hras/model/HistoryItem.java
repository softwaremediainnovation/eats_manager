package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class HistoryItem{

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("employee")
	@Expose
	private String employee;

	@SerializedName("status")
	@Expose
	private String status;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setEmployee(String employee){
		this.employee = employee;
	}

	public String getEmployee(){
		return employee;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"HistoryItem{" + 
			"date = '" + date + '\'' + 
			",note = '" + note + '\'' + 
			",employee = '" + employee + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}