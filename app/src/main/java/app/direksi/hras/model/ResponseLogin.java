package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseLogin{

	public DataLogin getDataLogin() {
		return dataLogin;
	}

	public void setDataLogin(DataLogin dataLogin) {
		this.dataLogin = dataLogin;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public int getErrCode() {
		return errCode;
	}

	public void setErrCode(int errCode) {
		this.errCode = errCode;
	}

	@SerializedName("data")
	@Expose
	private DataLogin dataLogin;

	public ResponseLogin(DataLogin dataLogin, String errMessage, int errCode) {
		this.dataLogin = dataLogin;
		this.errMessage = errMessage;
		this.errCode = errCode;
	}

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	@SerializedName("errCode")
	@Expose
	private int errCode;


}