
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataCreateTeguran {

    @SerializedName("date")
    private String mDate;
    @SerializedName("employee")
    private Object mEmployee;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private Object mFilePath;
    @SerializedName("filePathHtml")
    private String mFilePathHtml;
    @SerializedName("issuer")
    private Object mIssuer;
    @SerializedName("issuerID")
    private String mIssuerID;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reprimandCategory")
    private Object mReprimandCategory;
    @SerializedName("reprimandCategoryID")
    private Long mReprimandCategoryID;
    @SerializedName("reprimandFile")
    private Object mReprimandFile;
    @SerializedName("reprimandID")
    private Long mReprimandID;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Object getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Object employee) {
        mEmployee = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Object getFilePath() {
        return mFilePath;
    }

    public void setFilePath(Object filePath) {
        mFilePath = filePath;
    }

    public String getFilePathHtml() {
        return mFilePathHtml;
    }

    public void setFilePathHtml(String filePathHtml) {
        mFilePathHtml = filePathHtml;
    }

    public Object getIssuer() {
        return mIssuer;
    }

    public void setIssuer(Object issuer) {
        mIssuer = issuer;
    }

    public String getIssuerID() {
        return mIssuerID;
    }

    public void setIssuerID(String issuerID) {
        mIssuerID = issuerID;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Object getReprimandCategory() {
        return mReprimandCategory;
    }

    public void setReprimandCategory(Object reprimandCategory) {
        mReprimandCategory = reprimandCategory;
    }

    public Long getReprimandCategoryID() {
        return mReprimandCategoryID;
    }

    public void setReprimandCategoryID(Long reprimandCategoryID) {
        mReprimandCategoryID = reprimandCategoryID;
    }

    public Object getReprimandFile() {
        return mReprimandFile;
    }

    public void setReprimandFile(Object reprimandFile) {
        mReprimandFile = reprimandFile;
    }

    public Long getReprimandID() {
        return mReprimandID;
    }

    public void setReprimandID(Long reprimandID) {
        mReprimandID = reprimandID;
    }

}
