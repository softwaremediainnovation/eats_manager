package app.direksi.hras.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RemuPayroll {

	@SerializedName("date")
	private String date;

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("totalSalary")
	private Double totalSalary;

	@SerializedName("totalSalaryRound")
	private Double totalSalaryRound;

	@SerializedName("checkerID")
	private String checkerID;

	@SerializedName("payrollID")
	private Integer payrollID;

	@SerializedName("remunerationNominals")
	private List<Object> remunerationNominals;

	@SerializedName("checker")
	private Object checker;

	public String getDate(){
		return date;
	}

	public String getDateCreated(){
		return dateCreated;
	}

	public Double getTotalSalary(){
		return totalSalary;
	}

	public Double getTotalSalaryRound(){
		return totalSalaryRound;
	}

	public String getCheckerID(){
		return checkerID;
	}

	public Integer getPayrollID(){
		return payrollID;
	}

	public List<Object> getRemunerationNominals(){
		return remunerationNominals;
	}

	public Object getChecker(){
		return checker;
	}
}