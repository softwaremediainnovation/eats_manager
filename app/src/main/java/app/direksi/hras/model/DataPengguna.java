
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataPengguna {

    @SerializedName("user")
    private Pengguna mUser;
    @SerializedName("value")
    private String mValue;
    @SerializedName("result")
    private String result;
    @SerializedName("status")
    private String status;
    @SerializedName("lembur")
    private Boolean lembur;
    @SerializedName("attendance")
    private String attendance;
    @SerializedName("kasir")
    private String kasir;

    public Pengguna getUser() {
        return mUser;
    }

    public void setUser(Pengguna user) {
        mUser = user;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String value) {
        result = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        status = value;
    }

    public Boolean getLembur() {
        return lembur;
    }

    public void setLembur(Boolean value) {
        lembur = value;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendances) {
        attendance = attendances;
    }

    public String getKasir() {
        return kasir;
    }

    public void setKasir(String attendances) {
        kasir = attendances;
    }

}
