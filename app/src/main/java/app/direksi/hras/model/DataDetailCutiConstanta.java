
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDetailCutiConstanta {

    @SerializedName("constantaCategoryID")
    private Long mConstantaCategoryID;
    @SerializedName("constantaID")
    private Long mConstantaID;
    @SerializedName("creatorID")
    private String mCreatorID;
    @SerializedName("dateCreated")
    private String mDateCreated;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("name")
    private String mName;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("value")
    private String mValue;

    public Long getConstantaCategoryID() {
        return mConstantaCategoryID;
    }

    public void setConstantaCategoryID(Long constantaCategoryID) {
        mConstantaCategoryID = constantaCategoryID;
    }

    public Long getConstantaID() {
        return mConstantaID;
    }

    public void setConstantaID(Long constantaID) {
        mConstantaID = constantaID;
    }

    public String getCreatorID() {
        return mCreatorID;
    }

    public void setCreatorID(String creatorID) {
        mCreatorID = creatorID;
    }

    public String getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(String dateCreated) {
        mDateCreated = dateCreated;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public String getValue() {
        return mValue;
    }

    public void setValue(String value) {
        mValue = value;
    }

}
