package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponsePostLoan{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private DataPostLoan dataPostLoan;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataPostLoan(DataPostLoan dataPostLoan){
		this.dataPostLoan = dataPostLoan;
	}

	public DataPostLoan getDataPostLoan(){
		return dataPostLoan;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponsePostLoan{" + 
			"errCode = '" + errCode + '\'' + 
			",dataPostLoan = '" + dataPostLoan + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}