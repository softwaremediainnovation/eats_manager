
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataAktivitas {

    @SerializedName("activityID")
    private Long mActivityID;
    @SerializedName("date")
    private String mDate;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("organization")
    private String organization;

    public Long getActivityID() {
        return mActivityID;
    }

    public void setActivityID(Long activityID) {
        mActivityID = activityID;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        mEmployeeName = employeeName;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String employeeName) {
        organization = employeeName;
    }

}
