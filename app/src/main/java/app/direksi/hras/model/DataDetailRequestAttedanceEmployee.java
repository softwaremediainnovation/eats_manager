package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataDetailRequestAttedanceEmployee {

	@SerializedName("remunerations")
	private Object remunerations;

	@SerializedName("contractExpiredDate")
	private String contractExpiredDate;

	@SerializedName("resignDate")
	private Object resignDate;

	@SerializedName("statusOfResidence")
	private String statusOfResidence;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("organizationRules")
	private Object organizationRules;

	@SerializedName("userID")
	private String userID;

	@SerializedName("provinceID")
	private int provinceID;

	@SerializedName("isUsingGlasses")
	private boolean isUsingGlasses;

	@SerializedName("nik")
	private String nik;

	@SerializedName("toleransi")
	private int toleransi;

	@SerializedName("applicationUser")
	private Object applicationUser;

	@SerializedName("photoHtml")
	private String photoHtml;

	@SerializedName("joinDate")
	private String joinDate;

	@SerializedName("province")
	private Object province;

	@SerializedName("fullNameNikHtml")
	private String fullNameNikHtml;

	@SerializedName("overtimes")
	private Object overtimes;

	@SerializedName("expectedSalary")
	private Object expectedSalary;

	@SerializedName("fullNameNik")
	private String fullNameNik;

	@SerializedName("height")
	private int height;

	@SerializedName("longitude")
	private String longitude;

	@SerializedName("contactEmergencyPhone")
	private String contactEmergencyPhone;

	@SerializedName("isActiveHtml")
	private String isActiveHtml;

	@SerializedName("serialNumber")
	private String serialNumber;

	@SerializedName("generalRules")
	private Object generalRules;

	@SerializedName("loans")
	private Object loans;

	@SerializedName("weight")
	private int weight;

	@SerializedName("cityID")
	private int cityID;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("diseaseHistory")
	private String diseaseHistory;

	@SerializedName("employeeRelatives")
	private Object employeeRelatives;

	@SerializedName("numberOfChildren")
	private String numberOfChildren;

	@SerializedName("nationality")
	private String nationality;

	@SerializedName("phone")
	private String phone;

	@SerializedName("dob")
	private String dob;

	@SerializedName("idCardNumber")
	private String idCardNumber;

	@SerializedName("canEditProfile")
	private boolean canEditProfile;

	@SerializedName("fullNameNikHtmlWithDetail")
	private String fullNameNikHtmlWithDetail;

	@SerializedName("maritalStatus")
	private String maritalStatus;

	@SerializedName("employeeEmployments")
	private Object employeeEmployments;

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("note")
	private String note;

	@SerializedName("gender")
	private String gender;

	@SerializedName("assetTrackingOriginHolders")
	private Object assetTrackingOriginHolders;

	@SerializedName("city")
	private Object city;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("bloodType")
	private String bloodType;

	@SerializedName("reprimands")
	private Object reprimands;

	@SerializedName("memberships")
	private Object memberships;

	@SerializedName("attendances")
	private Object attendances;

	@SerializedName("weakness")
	private String weakness;

	@SerializedName("photoUrl")
	private String photoUrl;

	@SerializedName("contactEmergencyName")
	private String contactEmergencyName;

	@SerializedName("imageFile")
	private Object imageFile;

	@SerializedName("employeeCertificates")
	private Object employeeCertificates;

	@SerializedName("driverLicenseType")
	private String driverLicenseType;

	@SerializedName("employeeEducations")
	private Object employeeEducations;

	@SerializedName("email")
	private String email;

	@SerializedName("address")
	private String address;

	@SerializedName("dayOffs")
	private Object dayOffs;

	@SerializedName("roleId")
	private Object roleId;

	@SerializedName("fullName")
	private String fullName;

	@SerializedName("fullNameEmail")
	private String fullNameEmail;

	@SerializedName("reimbursements")
	private Object reimbursements;

	@SerializedName("typeOfVehicle")
	private String typeOfVehicle;

	@SerializedName("employeeOrganizations")
	private Object employeeOrganizations;

	@SerializedName("deviceID")
	private Object deviceID;

	@SerializedName("religion")
	private String religion;

	@SerializedName("assetTrackingDestinationHolders")
	private Object assetTrackingDestinationHolders;

	@SerializedName("activities")
	private Object activities;

	@SerializedName("strengthness")
	private String strengthness;

	public void setRemunerations(Object remunerations){
		this.remunerations = remunerations;
	}

	public Object getRemunerations(){
		return remunerations;
	}

	public void setContractExpiredDate(String contractExpiredDate){
		this.contractExpiredDate = contractExpiredDate;
	}

	public String getContractExpiredDate(){
		return contractExpiredDate;
	}

	public void setResignDate(Object resignDate){
		this.resignDate = resignDate;
	}

	public Object getResignDate(){
		return resignDate;
	}

	public void setStatusOfResidence(String statusOfResidence){
		this.statusOfResidence = statusOfResidence;
	}

	public String getStatusOfResidence(){
		return statusOfResidence;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setOrganizationRules(Object organizationRules){
		this.organizationRules = organizationRules;
	}

	public Object getOrganizationRules(){
		return organizationRules;
	}

	public void setUserID(String userID){
		this.userID = userID;
	}

	public String getUserID(){
		return userID;
	}

	public void setProvinceID(int provinceID){
		this.provinceID = provinceID;
	}

	public int getProvinceID(){
		return provinceID;
	}

	public void setIsUsingGlasses(boolean isUsingGlasses){
		this.isUsingGlasses = isUsingGlasses;
	}

	public boolean isIsUsingGlasses(){
		return isUsingGlasses;
	}

	public void setNik(String nik){
		this.nik = nik;
	}

	public String getNik(){
		return nik;
	}

	public void setToleransi(int toleransi){
		this.toleransi = toleransi;
	}

	public int getToleransi(){
		return toleransi;
	}

	public void setApplicationUser(Object applicationUser){
		this.applicationUser = applicationUser;
	}

	public Object getApplicationUser(){
		return applicationUser;
	}

	public void setPhotoHtml(String photoHtml){
		this.photoHtml = photoHtml;
	}

	public String getPhotoHtml(){
		return photoHtml;
	}

	public void setJoinDate(String joinDate){
		this.joinDate = joinDate;
	}

	public String getJoinDate(){
		return joinDate;
	}

	public void setProvince(Object province){
		this.province = province;
	}

	public Object getProvince(){
		return province;
	}

	public void setFullNameNikHtml(String fullNameNikHtml){
		this.fullNameNikHtml = fullNameNikHtml;
	}

	public String getFullNameNikHtml(){
		return fullNameNikHtml;
	}

	public void setOvertimes(Object overtimes){
		this.overtimes = overtimes;
	}

	public Object getOvertimes(){
		return overtimes;
	}

	public void setExpectedSalary(Object expectedSalary){
		this.expectedSalary = expectedSalary;
	}

	public Object getExpectedSalary(){
		return expectedSalary;
	}

	public void setFullNameNik(String fullNameNik){
		this.fullNameNik = fullNameNik;
	}

	public String getFullNameNik(){
		return fullNameNik;
	}

	public void setHeight(int height){
		this.height = height;
	}

	public int getHeight(){
		return height;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setContactEmergencyPhone(String contactEmergencyPhone){
		this.contactEmergencyPhone = contactEmergencyPhone;
	}

	public String getContactEmergencyPhone(){
		return contactEmergencyPhone;
	}

	public void setIsActiveHtml(String isActiveHtml){
		this.isActiveHtml = isActiveHtml;
	}

	public String getIsActiveHtml(){
		return isActiveHtml;
	}

	public void setSerialNumber(String serialNumber){
		this.serialNumber = serialNumber;
	}

	public String getSerialNumber(){
		return serialNumber;
	}

	public void setGeneralRules(Object generalRules){
		this.generalRules = generalRules;
	}

	public Object getGeneralRules(){
		return generalRules;
	}

	public void setLoans(Object loans){
		this.loans = loans;
	}

	public Object getLoans(){
		return loans;
	}

	public void setWeight(int weight){
		this.weight = weight;
	}

	public int getWeight(){
		return weight;
	}

	public void setCityID(int cityID){
		this.cityID = cityID;
	}

	public int getCityID(){
		return cityID;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setDiseaseHistory(String diseaseHistory){
		this.diseaseHistory = diseaseHistory;
	}

	public String getDiseaseHistory(){
		return diseaseHistory;
	}

	public void setEmployeeRelatives(Object employeeRelatives){
		this.employeeRelatives = employeeRelatives;
	}

	public Object getEmployeeRelatives(){
		return employeeRelatives;
	}

	public void setNumberOfChildren(String numberOfChildren){
		this.numberOfChildren = numberOfChildren;
	}

	public String getNumberOfChildren(){
		return numberOfChildren;
	}

	public void setNationality(String nationality){
		this.nationality = nationality;
	}

	public String getNationality(){
		return nationality;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setIdCardNumber(String idCardNumber){
		this.idCardNumber = idCardNumber;
	}

	public String getIdCardNumber(){
		return idCardNumber;
	}

	public void setCanEditProfile(boolean canEditProfile){
		this.canEditProfile = canEditProfile;
	}

	public boolean isCanEditProfile(){
		return canEditProfile;
	}

	public void setFullNameNikHtmlWithDetail(String fullNameNikHtmlWithDetail){
		this.fullNameNikHtmlWithDetail = fullNameNikHtmlWithDetail;
	}

	public String getFullNameNikHtmlWithDetail(){
		return fullNameNikHtmlWithDetail;
	}

	public void setMaritalStatus(String maritalStatus){
		this.maritalStatus = maritalStatus;
	}

	public String getMaritalStatus(){
		return maritalStatus;
	}

	public void setEmployeeEmployments(Object employeeEmployments){
		this.employeeEmployments = employeeEmployments;
	}

	public Object getEmployeeEmployments(){
		return employeeEmployments;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setAssetTrackingOriginHolders(Object assetTrackingOriginHolders){
		this.assetTrackingOriginHolders = assetTrackingOriginHolders;
	}

	public Object getAssetTrackingOriginHolders(){
		return assetTrackingOriginHolders;
	}

	public void setCity(Object city){
		this.city = city;
	}

	public Object getCity(){
		return city;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setBloodType(String bloodType){
		this.bloodType = bloodType;
	}

	public String getBloodType(){
		return bloodType;
	}

	public void setReprimands(Object reprimands){
		this.reprimands = reprimands;
	}

	public Object getReprimands(){
		return reprimands;
	}

	public void setMemberships(Object memberships){
		this.memberships = memberships;
	}

	public Object getMemberships(){
		return memberships;
	}

	public void setAttendances(Object attendances){
		this.attendances = attendances;
	}

	public Object getAttendances(){
		return attendances;
	}

	public void setWeakness(String weakness){
		this.weakness = weakness;
	}

	public String getWeakness(){
		return weakness;
	}

	public void setPhotoUrl(String photoUrl){
		this.photoUrl = photoUrl;
	}

	public String getPhotoUrl(){
		return photoUrl;
	}

	public void setContactEmergencyName(String contactEmergencyName){
		this.contactEmergencyName = contactEmergencyName;
	}

	public String getContactEmergencyName(){
		return contactEmergencyName;
	}

	public void setImageFile(Object imageFile){
		this.imageFile = imageFile;
	}

	public Object getImageFile(){
		return imageFile;
	}

	public void setEmployeeCertificates(Object employeeCertificates){
		this.employeeCertificates = employeeCertificates;
	}

	public Object getEmployeeCertificates(){
		return employeeCertificates;
	}

	public void setDriverLicenseType(String driverLicenseType){
		this.driverLicenseType = driverLicenseType;
	}

	public String getDriverLicenseType(){
		return driverLicenseType;
	}

	public void setEmployeeEducations(Object employeeEducations){
		this.employeeEducations = employeeEducations;
	}

	public Object getEmployeeEducations(){
		return employeeEducations;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDayOffs(Object dayOffs){
		this.dayOffs = dayOffs;
	}

	public Object getDayOffs(){
		return dayOffs;
	}

	public void setRoleId(Object roleId){
		this.roleId = roleId;
	}

	public Object getRoleId(){
		return roleId;
	}

	public void setFullName(String fullName){
		this.fullName = fullName;
	}

	public String getFullName(){
		return fullName;
	}

	public void setFullNameEmail(String fullNameEmail){
		this.fullNameEmail = fullNameEmail;
	}

	public String getFullNameEmail(){
		return fullNameEmail;
	}

	public void setReimbursements(Object reimbursements){
		this.reimbursements = reimbursements;
	}

	public Object getReimbursements(){
		return reimbursements;
	}

	public void setTypeOfVehicle(String typeOfVehicle){
		this.typeOfVehicle = typeOfVehicle;
	}

	public String getTypeOfVehicle(){
		return typeOfVehicle;
	}

	public void setEmployeeOrganizations(Object employeeOrganizations){
		this.employeeOrganizations = employeeOrganizations;
	}

	public Object getEmployeeOrganizations(){
		return employeeOrganizations;
	}

	public void setDeviceID(Object deviceID){
		this.deviceID = deviceID;
	}

	public Object getDeviceID(){
		return deviceID;
	}

	public void setReligion(String religion){
		this.religion = religion;
	}

	public String getReligion(){
		return religion;
	}

	public void setAssetTrackingDestinationHolders(Object assetTrackingDestinationHolders){
		this.assetTrackingDestinationHolders = assetTrackingDestinationHolders;
	}

	public Object getAssetTrackingDestinationHolders(){
		return assetTrackingDestinationHolders;
	}

	public void setActivities(Object activities){
		this.activities = activities;
	}

	public Object getActivities(){
		return activities;
	}

	public void setStrengthness(String strengthness){
		this.strengthness = strengthness;
	}

	public String getStrengthness(){
		return strengthness;
	}

	@Override
 	public String toString(){
		return 
			"Employee{" + 
			"remunerations = '" + remunerations + '\'' + 
			",contractExpiredDate = '" + contractExpiredDate + '\'' + 
			",resignDate = '" + resignDate + '\'' + 
			",statusOfResidence = '" + statusOfResidence + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",organizationRules = '" + organizationRules + '\'' + 
			",userID = '" + userID + '\'' + 
			",provinceID = '" + provinceID + '\'' + 
			",isUsingGlasses = '" + isUsingGlasses + '\'' + 
			",nik = '" + nik + '\'' + 
			",toleransi = '" + toleransi + '\'' + 
			",applicationUser = '" + applicationUser + '\'' + 
			",photoHtml = '" + photoHtml + '\'' + 
			",joinDate = '" + joinDate + '\'' + 
			",province = '" + province + '\'' + 
			",fullNameNikHtml = '" + fullNameNikHtml + '\'' + 
			",overtimes = '" + overtimes + '\'' + 
			",expectedSalary = '" + expectedSalary + '\'' + 
			",fullNameNik = '" + fullNameNik + '\'' + 
			",height = '" + height + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",contactEmergencyPhone = '" + contactEmergencyPhone + '\'' + 
			",isActiveHtml = '" + isActiveHtml + '\'' + 
			",serialNumber = '" + serialNumber + '\'' + 
			",generalRules = '" + generalRules + '\'' + 
			",loans = '" + loans + '\'' + 
			",weight = '" + weight + '\'' + 
			",cityID = '" + cityID + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",diseaseHistory = '" + diseaseHistory + '\'' + 
			",employeeRelatives = '" + employeeRelatives + '\'' + 
			",numberOfChildren = '" + numberOfChildren + '\'' + 
			",nationality = '" + nationality + '\'' + 
			",phone = '" + phone + '\'' + 
			",dob = '" + dob + '\'' + 
			",idCardNumber = '" + idCardNumber + '\'' + 
			",canEditProfile = '" + canEditProfile + '\'' + 
			",fullNameNikHtmlWithDetail = '" + fullNameNikHtmlWithDetail + '\'' + 
			",maritalStatus = '" + maritalStatus + '\'' + 
			",employeeEmployments = '" + employeeEmployments + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",note = '" + note + '\'' + 
			",gender = '" + gender + '\'' + 
			",assetTrackingOriginHolders = '" + assetTrackingOriginHolders + '\'' + 
			",city = '" + city + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",bloodType = '" + bloodType + '\'' + 
			",reprimands = '" + reprimands + '\'' + 
			",memberships = '" + memberships + '\'' + 
			",attendances = '" + attendances + '\'' + 
			",weakness = '" + weakness + '\'' + 
			",photoUrl = '" + photoUrl + '\'' + 
			",contactEmergencyName = '" + contactEmergencyName + '\'' + 
			",imageFile = '" + imageFile + '\'' + 
			",employeeCertificates = '" + employeeCertificates + '\'' + 
			",driverLicenseType = '" + driverLicenseType + '\'' + 
			",employeeEducations = '" + employeeEducations + '\'' + 
			",email = '" + email + '\'' + 
			",address = '" + address + '\'' + 
			",dayOffs = '" + dayOffs + '\'' + 
			",roleId = '" + roleId + '\'' + 
			",fullName = '" + fullName + '\'' + 
			",fullNameEmail = '" + fullNameEmail + '\'' + 
			",reimbursements = '" + reimbursements + '\'' + 
			",typeOfVehicle = '" + typeOfVehicle + '\'' + 
			",employeeOrganizations = '" + employeeOrganizations + '\'' + 
			",deviceID = '" + deviceID + '\'' + 
			",religion = '" + religion + '\'' + 
			",assetTrackingDestinationHolders = '" + assetTrackingDestinationHolders + '\'' + 
			",activities = '" + activities + '\'' + 
			",strengthness = '" + strengthness + '\'' + 
			"}";
		}
}