
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EmployeeInfoMembership {

    @SerializedName("company")
    private String mCompany;
    @SerializedName("isLeader")
    private Boolean mIsLeader;
    @SerializedName("organization")
    private String mOrganization;

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public Boolean getIsLeader() {
        return mIsLeader;
    }

    public void setIsLeader(Boolean isLeader) {
        mIsLeader = isLeader;
    }

    public String getOrganization() {
        return mOrganization;
    }

    public void setOrganization(String organization) {
        mOrganization = organization;
    }

}
