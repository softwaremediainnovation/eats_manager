
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataRelativeEmploye {

    @SerializedName("age")
    private Long mAge;
    @SerializedName("educationLevel")
    private String mEducationLevel;
    @SerializedName("educationLevelID")
    private Long mEducationLevelID;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("employeeRelativeID")
    private Long mEmployeeRelativeID;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("job")
    private String mJob;
    @SerializedName("name")
    private String mName;
    @SerializedName("relation")
    private String mRelation;

    public Long getAge() {
        return mAge;
    }

    public void setAge(Long age) {
        mAge = age;
    }

    public String getEducationLevel() {
        return mEducationLevel;
    }

    public void setEducationLevel(String educationLevel) {
        mEducationLevel = educationLevel;
    }

    public Long getEducationLevelID() {
        return mEducationLevelID;
    }

    public void setEducationLevelID(Long educationLevelID) {
        mEducationLevelID = educationLevelID;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Long getEmployeeRelativeID() {
        return mEmployeeRelativeID;
    }

    public void setEmployeeRelativeID(Long employeeRelativeID) {
        mEmployeeRelativeID = employeeRelativeID;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getJob() {
        return mJob;
    }

    public void setJob(String job) {
        mJob = job;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getRelation() {
        return mRelation;
    }

    public void setRelation(String relation) {
        mRelation = relation;
    }

}
