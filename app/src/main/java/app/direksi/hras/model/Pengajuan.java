
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Pengajuan {

    @SerializedName("klaim")
    private Double mKlaim;
    @SerializedName("pinjaman")
    private Double mPinjaman;
    @SerializedName("pembayaran")
    private Double mPembayaran;

    public Double getKlaim() {
        return mKlaim;
    }

    public void setKlaim(Double klaim) {
        mKlaim = klaim;
    }

    public Double getPinjaman() {
        return mPinjaman;
    }

    public void setPinjaman(Double pinjaman) {
        mPinjaman = pinjaman;
    }

    public Double getPembayaran() {
        return mPembayaran;
    }

    public void setPembayaran(Double pinjaman) {
        mPembayaran = pinjaman;
    }

}
