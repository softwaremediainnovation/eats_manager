
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataContractExpired {

    @SerializedName("company")
    private String mCompany;

    public Long getEmployeeId() {
        return EmployeeId;
    }

    public void setEmployeeId(Long employeeId) {
        EmployeeId = employeeId;
    }

    @SerializedName("employeeId")
    private Long EmployeeId;
    @SerializedName("contractExpiredDate")
    private String mContractExpiredDate;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("employeeName")
    private String mEmployeeName;
    @SerializedName("firstName")
    private String mfirstName;

    public String getMfirstName() {
        return mfirstName;
    }

    public void setMfirstName(String mfirstName) {
        this.mfirstName = mfirstName;
    }

    public String getMlastName() {
        return mlastName;
    }

    public void setMlastName(String mlastName) {
        this.mlastName = mlastName;
    }

    @SerializedName("lastName")
    private String mlastName;
    @SerializedName("organization")
    private String mOrganization;

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getContractExpiredDate() {
        return mContractExpiredDate;
    }

    public void setContractExpiredDate(String contractExpiredDate) {
        mContractExpiredDate = contractExpiredDate;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmployeeName() {
        return mEmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        mEmployeeName = employeeName;
    }

    public String getOrganization() {
        return mOrganization;
    }

    public void setOrganization(String organization) {
        mOrganization = organization;
    }

}
