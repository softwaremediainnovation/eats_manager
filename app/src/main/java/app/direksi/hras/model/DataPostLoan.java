package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataPostLoan{

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("approver")
	@Expose
	private String approver;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("amount")
	@Expose
	private int amount;

	@SerializedName("datePaid")
	@Expose
	private String datePaid;

	@SerializedName("installmentAmount")
	@Expose
	private int installmentAmount;

	@SerializedName("approverID")
	@Expose
	private String approverID;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("employee")
	@Expose
	private String employee;

	@SerializedName("isPaid")
	@Expose
	private boolean isPaid;

	@SerializedName("loanHtml")
	@Expose
	private String loanHtml;

	@SerializedName("isPbonMingguan")
	@Expose
	private boolean isPbonMingguan;

	@SerializedName("dateApproved")
	@Expose
	private String dateApproved;

	@SerializedName("installment")
	@Expose
	private int installment;

	@SerializedName("installmentPayOut")
	@Expose
	private int installmentPayOut;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("isApprovedHtml")
	@Expose
	private String isApprovedHtml;

	@SerializedName("isApproved")
	@Expose
	private boolean isApproved;

	@SerializedName("isPbonMingguanHtml")
	@Expose
	private String isPbonMingguanHtml;

	@SerializedName("loanID")
	@Expose
	private int loanID;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setApprover(String approver){
		this.approver = approver;
	}

	public String getApprover(){
		return approver;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setAmount(int amount){
		this.amount = amount;
	}

	public int getAmount(){
		return amount;
	}

	public void setDatePaid(String datePaid){
		this.datePaid = datePaid;
	}

	public String getDatePaid(){
		return datePaid;
	}

	public void setInstallmentAmount(int installmentAmount){
		this.installmentAmount = installmentAmount;
	}

	public int getInstallmentAmount(){
		return installmentAmount;
	}

	public void setApproverID(String approverID){
		this.approverID = approverID;
	}

	public String getApproverID(){
		return approverID;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setEmployee(String employee){
		this.employee = employee;
	}

	public String getEmployee(){
		return employee;
	}

	public void setIsPaid(boolean isPaid){
		this.isPaid = isPaid;
	}

	public boolean isIsPaid(){
		return isPaid;
	}

	public void setLoanHtml(String loanHtml){
		this.loanHtml = loanHtml;
	}

	public String getLoanHtml(){
		return loanHtml;
	}

	public void setIsPbonMingguan(boolean isPbonMingguan){
		this.isPbonMingguan = isPbonMingguan;
	}

	public boolean isIsPbonMingguan(){
		return isPbonMingguan;
	}

	public void setDateApproved(String dateApproved){
		this.dateApproved = dateApproved;
	}

	public String getDateApproved(){
		return dateApproved;
	}

	public void setInstallment(int installment){
		this.installment = installment;
	}

	public int getInstallment(){
		return installment;
	}

	public void setInstallmentPayOut(int installmentPayOut){
		this.installmentPayOut = installmentPayOut;
	}

	public int getInstallmentPayOut(){
		return installmentPayOut;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setIsApprovedHtml(String isApprovedHtml){
		this.isApprovedHtml = isApprovedHtml;
	}

	public String getIsApprovedHtml(){
		return isApprovedHtml;
	}

	public void setIsApproved(boolean isApproved){
		this.isApproved = isApproved;
	}

	public boolean isIsApproved(){
		return isApproved;
	}

	public void setIsPbonMingguanHtml(String isPbonMingguanHtml){
		this.isPbonMingguanHtml = isPbonMingguanHtml;
	}

	public String getIsPbonMingguanHtml(){
		return isPbonMingguanHtml;
	}

	public void setLoanID(int loanID){
		this.loanID = loanID;
	}

	public int getLoanID(){
		return loanID;
	}

	@Override
 	public String toString(){
		return 
			"DataPostLoan{" + 
			"date = '" + date + '\'' + 
			",approver = '" + approver + '\'' + 
			",note = '" + note + '\'' + 
			",amount = '" + amount + '\'' + 
			",datePaid = '" + datePaid + '\'' + 
			",installmentAmount = '" + installmentAmount + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",employee = '" + employee + '\'' + 
			",isPaid = '" + isPaid + '\'' + 
			",loanHtml = '" + loanHtml + '\'' + 
			",isPbonMingguan = '" + isPbonMingguan + '\'' + 
			",dateApproved = '" + dateApproved + '\'' + 
			",installment = '" + installment + '\'' + 
			",installmentPayOut = '" + installmentPayOut + '\'' + 
			",name = '" + name + '\'' + 
			",isApprovedHtml = '" + isApprovedHtml + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			",isPbonMingguanHtml = '" + isPbonMingguanHtml + '\'' + 
			",loanID = '" + loanID + '\'' + 
			"}";
		}
}