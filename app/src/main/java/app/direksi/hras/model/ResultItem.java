package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResultItem{

	@SerializedName("fileName")
	@Expose
	private String fileName;

	@SerializedName("filePath")
	@Expose
	private String filePath;

	public void setFileName(String fileName){
		this.fileName = fileName;
	}

	public String getFileName(){
		return fileName;
	}

	public void setFilePath(String filePath){
		this.filePath = filePath;
	}

	public String getFilePath(){
		return filePath;
	}

	@Override
 	public String toString(){
		return 
			"ResultItem{" + 
			"fileName = '" + fileName + '\'' + 
			",filePath = '" + filePath + '\'' + 
			"}";
		}
}