package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class PostTaskSchedule{


    public PostTaskSchedule( String note, String dueDate, int creatorID, int employeeID,String taskName,
                     boolean senin, boolean selasa, boolean rabu, boolean kamis, boolean jumat, boolean sabtu, boolean minggu,
                     int taskType, String dateStart, String dateEnd,
                     List<StringClass> addTaskDetailInputs ) {


        this.senin = senin;
        this.selasa = selasa;
        this.rabu = rabu;
        this.kamis = kamis;
        this.jumat = jumat;
        this.sabtu = sabtu;
        this.minggu = minggu;

        this.taskType = taskType;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;

        this.note = note;
        this.dueDate = dueDate;
        this.creatorID = creatorID;
        this.employeeID = employeeID;
        this.taskName = taskName;
        this.addTaskDetailInputs = addTaskDetailInputs;

    }

    @SerializedName("senin")
    private boolean senin;

    @SerializedName("selasa")
    private boolean selasa;

    @SerializedName("rabu")
    private boolean rabu;

    @SerializedName("kamis")
    private boolean kamis;

    @SerializedName("jumat")
    private boolean jumat;

    @SerializedName("sabtu")
    private boolean sabtu;

    @SerializedName("minggu")
    private boolean minggu;

    @SerializedName("note")
    private String note;

    @SerializedName("dueDate")
    private String dueDate;

    @SerializedName("dateStart")
    private String dateStart;

    @SerializedName("dateEnd")
    private String dateEnd;

    @SerializedName("creatorID")
    private int creatorID;

    @SerializedName("employeeID")
    private int employeeID;

    @SerializedName("taskType")
    private int taskType;

    @SerializedName("taskName")
    private String taskName;

    @SerializedName("addTaskDetailInputs")
    private List<StringClass> addTaskDetailInputs;

    public void setTaskType(int employeeID){
        this.taskType = employeeID;
    }

    public int getTaskType(){
        return taskType;
    }

    public void setSenin(boolean note){
        this.senin = note;
    }

    public boolean getSenin(){
        return senin;
    }

    public void setSelasa(boolean note){
        this.selasa = note;
    }

    public boolean getSelasa(){
        return selasa;
    }

    public void setRabu(boolean note){
        this.rabu = note;
    }

    public boolean setRabu(){
        return rabu;
    }

    public void setKamis(boolean note){
        this.kamis = note;
    }

    public boolean getKamis(){
        return kamis;
    }

    public void setJumat(boolean note){
        this.jumat = note;
    }

    public boolean getJumat(){
        return jumat;
    }

    public void setSabtu(boolean note){
        this.sabtu = note;
    }

    public boolean getSabtu(){
        return sabtu;
    }

    public void setMinggu(boolean note){
        this.minggu = note;
    }

    public boolean getMinggu(){
        return minggu;
    }


    public void setDateStart(String note){
        this.dateStart = note;
    }

    public String getDateStart(){
        return dateStart;
    }

    public void setDateEnd(String note){
        this.dateEnd = note;
    }

    public String getDateEnd(){
        return dateEnd;
    }








    public void setNote(String note){
        this.note = note;
    }

    public String getNote(){
        return note;
    }

    public void setDueDate(String dueDate){
        this.dueDate = dueDate;
    }

    public String getDueDate(){
        return dueDate;
    }

    public void setCreatorID(int creatorID){
        this.creatorID = creatorID;
    }

    public int getCreatorID(){
        return creatorID;
    }

    public void setEmployeeID(int employeeID){
        this.employeeID = employeeID;
    }

    public int getEmployeeID(){
        return employeeID;
    }

    public void setTaskName(String taskName){
        this.taskName = taskName;
    }

    public String getTaskName(){
        return taskName;
    }

    public void setAddTaskDetailInputs(List<StringClass> addTaskDetailInputs){
        this.addTaskDetailInputs = addTaskDetailInputs;
    }

    public List<StringClass> getAddTaskDetailInputs(){
        return addTaskDetailInputs;
    }

    @Override
    public String toString(){
        return
                "PostTask{" +
                        "note = '" + note + '\'' +
                        ",dueDate = '" + dueDate + '\'' +
                        ",creatorID = '" + creatorID + '\'' +
                        ",employeeID = '" + employeeID + '\'' +
                        ",taskName = '" + taskName + '\'' +
                        ",addTaskDetailInputs = '" + addTaskDetailInputs + '\'' +
                        "}";
    }
}