package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import javax.annotation.Generated;

import app.direksi.hras.model.DataTaskIDDetailItem;
import app.direksi.hras.model.DataTaskIDHeader;
import app.direksi.hras.model.DataTaskIDHeaderSchedule;
import app.direksi.hras.model.TaskCreator;
import app.direksi.hras.model.TaskEmployee;

@Generated("com.robohorse.robopojogenerator")
public class DataTaskIDDetailSchedule {

	@SerializedName("header")
	private DataTaskIDHeaderSchedule header;

	@SerializedName("detail")
	private List<DataTaskIDDetailItem> detail;

	@SerializedName("creator")
	private List<TaskCreator> creator;

	@SerializedName("organizations")
	private String organizations;

	@SerializedName("employees")
	private List<TaskEmployee> employees;



	public void setCreator(List<TaskCreator> creator){
		this.creator = creator;
	}

	public List<TaskCreator> getCreator(){
		return creator;
	}

	public void setOrganizations(String organizations){
		this.organizations = organizations;
	}

	public String getOrganizations(){
		return organizations;
	}

	public void setEmployees(List<TaskEmployee> employees){
		this.employees = employees;
	}

	public List<TaskEmployee> getEmployees(){
		return employees;
	}


	public void setHeader(DataTaskIDHeaderSchedule header){
		this.header = header;
	}

	public DataTaskIDHeaderSchedule getHeader(){
		return header;
	}

	public void setDetail(List<DataTaskIDDetailItem> detail){
		this.detail = detail;
	}

	public List<DataTaskIDDetailItem> getDetail(){
		return detail;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"header = '" + header + '\'' + 
			",detail = '" + detail + '\'' + 
			"}";
		}
}