package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponsePostReimburse{

	@SerializedName("errCode")
	@Expose
	private int errCode;

	@SerializedName("data")
	@Expose
	private DataPostreimbursement dataPostreimbursement;

	@SerializedName("errMessage")
	@Expose
	private String errMessage;

	public void setErrCode(int errCode){
		this.errCode = errCode;
	}

	public int getErrCode(){
		return errCode;
	}

	public void setDataPostreimbursement(DataPostreimbursement dataPostreimbursement){
		this.dataPostreimbursement = dataPostreimbursement;
	}

	public DataPostreimbursement getDataPostreimbursement(){
		return dataPostreimbursement;
	}

	public void setErrMessage(String errMessage){
		this.errMessage = errMessage;
	}

	public String getErrMessage(){
		return errMessage;
	}

	@Override
 	public String toString(){
		return 
			"ResponsePostReimburse{" + 
			"errCode = '" + errCode + '\'' + 
			",dataPostreimbursement = '" + dataPostreimbursement + '\'' + 
			",errMessage = '" + errMessage + '\'' + 
			"}";
		}
}