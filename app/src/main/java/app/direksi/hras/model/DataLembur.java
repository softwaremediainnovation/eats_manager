
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataLembur {

    @SerializedName("approver")
    private DataDetailLemburApprover mApprover;
    @SerializedName("approverID")
    private String mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("dateApproved")
    private String mDateApproved;
    @SerializedName("dateEnd")
    private String mDateEnd;
    @SerializedName("dateStart")
    private String mDateStart;
    @SerializedName("employee")
    private DataDetailLemburEmployee mEmployee;
    @SerializedName("employeeApprover")
    private DataDetailLemburEmployeeApprover mEmployeeApprover;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private String mFilePath;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("note")
    private String mNote;
    @SerializedName("overtimeID")
    private Long mOvertimeID;
    @SerializedName("organization")
    private String organization;

    public DataDetailLemburApprover getApprover() {
        return mApprover;
    }

    public void setApprover(DataDetailLemburApprover approver) {
        mApprover = approver;
    }

    public String getApproverID() {
        return mApproverID;
    }

    public void setApproverID(String approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(String dateApproved) {
        mDateApproved = dateApproved;
    }

    public String getDateEnd() {
        return mDateEnd;
    }

    public void setDateEnd(String dateEnd) {
        mDateEnd = dateEnd;
    }

    public String getDateStart() {
        return mDateStart;
    }

    public void setDateStart(String dateStart) {
        mDateStart = dateStart;
    }

    public DataDetailLemburEmployee getEmployee() {
        return mEmployee;
    }

    public void setEmployee(DataDetailLemburEmployee employee) {
        mEmployee = employee;
    }

    public DataDetailLemburEmployeeApprover getEmployeeApprover() {
        return mEmployeeApprover;
    }

    public void setEmployeeApprover(DataDetailLemburEmployeeApprover employeeApprover) {
        mEmployeeApprover = employeeApprover;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFilePath() {
        return mFilePath;
    }

    public void setFilePath(String filePath) {
        mFilePath = filePath;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Long getOvertimeID() {
        return mOvertimeID;
    }

    public void setOvertimeID(Long overtimeID) {
        mOvertimeID = overtimeID;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String note) {
        organization = note;
    }

}
