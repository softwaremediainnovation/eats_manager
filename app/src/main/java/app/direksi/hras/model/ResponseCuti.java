
package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseCuti {

    @SerializedName("data")
    private List<DataCuti> mData;
    @SerializedName("errCode")
    private Long mErrCode;
    @SerializedName("errMessage")
    private String mErrMessage;

    public List<DataCuti> getData() {
        return mData;
    }

    public void setData(List<DataCuti> data) {
        mData = data;
    }

    public Long getErrCode() {
        return mErrCode;
    }

    public void setErrCode(Long errCode) {
        mErrCode = errCode;
    }

    public String getErrMessage() {
        return mErrMessage;
    }

    public void setErrMessage(String errMessage) {
        mErrMessage = errMessage;
    }

}
