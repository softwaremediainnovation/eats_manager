
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataDalamDashboard {

    @SerializedName("cuti")
    private Cuti mCuti;
    @SerializedName("gaji")
    private Gaji mGaji;
    @SerializedName("lembur")
    private Lembur mLembur;
    @SerializedName("news")
    private News mNews;
    @SerializedName("pengajuan")
    private Pengajuan mPengajuan;
    @SerializedName("surat")
    private Surat mSurat;
    @SerializedName("totalTerlambat")
    private Double mTotalTerlambat;
    @SerializedName("employees")
    private List<EmployeeBirthdayDashboard> mEmployees;


    public List<EmployeeBirthdayDashboard> getEmployees() {
        return mEmployees;
    }

    public void setEmployees(List<EmployeeBirthdayDashboard> employees) {
        mEmployees = employees;
    }

    public Cuti getCuti() {
        return mCuti;
    }

    public void setCuti(Cuti cuti) {
        mCuti = cuti;
    }

    public Gaji getGaji() {
        return mGaji;
    }

    public void setGaji(Gaji gaji) {
        mGaji = gaji;
    }

    public Lembur getLembur() {
        return mLembur;
    }

    public void setLembur(Lembur lembur) {
        mLembur = lembur;
    }

    public News getNews() {
        return mNews;
    }

    public void setNews(News news) {
        mNews = news;
    }

    public Pengajuan getPengajuan() {
        return mPengajuan;
    }

    public void setPengajuan(Pengajuan pengajuan) {
        mPengajuan = pengajuan;
    }

    public Surat getSurat() {
        return mSurat;
    }

    public void setSurat(Surat surat) {
        mSurat = surat;
    }

    public Double getTotalTerlambat() {
        return mTotalTerlambat;
    }

    public void setTotalTerlambat(Double surat) {
        mTotalTerlambat = surat;
    }

}
