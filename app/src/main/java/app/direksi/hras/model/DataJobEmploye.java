
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataJobEmploye {

    @SerializedName("company")
    private String mCompany;
    @SerializedName("contactNumber")
    private String mContactNumber;
    @SerializedName("dateEnd")
    private String mDateEnd;
    @SerializedName("dateStart")
    private String mDateStart;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("employeeEmploymentID")
    private Long mEmployeeEmploymentID;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("headBoss")
    private String mHeadBoss;
    @SerializedName("jobDescription")
    private String mJobDescription;
    @SerializedName("position")
    private String mPosition;

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public String getDateEnd() {
        return mDateEnd;
    }

    public void setDateEnd(String dateEnd) {
        mDateEnd = dateEnd;
    }

    public String getDateStart() {
        return mDateStart;
    }

    public void setDateStart(String dateStart) {
        mDateStart = dateStart;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public Long getEmployeeEmploymentID() {
        return mEmployeeEmploymentID;
    }

    public void setEmployeeEmploymentID(Long employeeEmploymentID) {
        mEmployeeEmploymentID = employeeEmploymentID;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getHeadBoss() {
        return mHeadBoss;
    }

    public void setHeadBoss(String headBoss) {
        mHeadBoss = headBoss;
    }

    public String getJobDescription() {
        return mJobDescription;
    }

    public void setJobDescription(String jobDescription) {
        mJobDescription = jobDescription;
    }

    public String getPosition() {
        return mPosition;
    }

    public void setPosition(String position) {
        mPosition = position;
    }

}
