package app.direksi.hras.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataReimbursementItem implements Parcelable {

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getApproverID() {
		return approverID;
	}

	public void setApproverID(String approverID) {
		this.approverID = approverID;
	}

	public String getDateApproved() {
		return dateApproved;
	}

	public void setDateApproved(String dateApproved) {
		this.dateApproved = dateApproved;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}

	public String getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}

	public int getReimbursementID() {
		return reimbursementID;
	}

	public void setReimbursementID(int reimbursementID) {
		this.reimbursementID = reimbursementID;
	}

	public int getReimbursementCategoryID() {
		return reimbursementCategoryID;
	}

	public void setReimbursementCategoryID(int reimbursementCategoryID) {
		this.reimbursementCategoryID = reimbursementCategoryID;
	}

	public String getApproverName() {
		return approverName;
	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getReimbursementFile() {
		return reimbursementFile;
	}

	public void setReimbursementFile(String reimbursementFile) {
		this.reimbursementFile = reimbursementFile;
	}

	@SerializedName("date")
	@Expose
	private String date;

	@SerializedName("note")
	@Expose
	private String note;

	@SerializedName("amount")
	@Expose
	private int amount;

	@SerializedName("approverID")
	@Expose
	private String approverID;

	@SerializedName("dateApproved")
	@Expose
	private String dateApproved;

	@SerializedName("filePath")
	@Expose
	private String filePath;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("isApproved")
	@Expose
	private String isApproved;

	@SerializedName("reimbursementID")
	@Expose
	private int reimbursementID;

	@SerializedName("reimbursementCategoryID")
	@Expose
	private int reimbursementCategoryID;

	@SerializedName("approverName")
	@Expose
	private String approverName;

	@SerializedName("reimbursementFile")
	@Expose
	private String reimbursementFile;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.date);
		dest.writeString(this.note);
		dest.writeInt(this.amount);
		dest.writeString(this.approverID);
		dest.writeString(this.dateApproved);
		dest.writeString(this.filePath);
		dest.writeInt(this.employeeID);
		dest.writeString(this.isApproved);
		dest.writeInt(this.reimbursementID);
		dest.writeInt(this.reimbursementCategoryID);
		dest.writeString(this.approverName);
		dest.writeString(this.reimbursementFile);
	}

	public DataReimbursementItem() {
	}

	protected DataReimbursementItem(Parcel in) {
		this.date = in.readString();
		this.note = in.readString();
		this.amount = in.readInt();
		this.approverID = in.readString();
		this.dateApproved = in.readString();
		this.filePath = in.readString();
		this.employeeID = in.readInt();
		this.isApproved = in.readString();
		this.reimbursementID = in.readInt();
		this.reimbursementCategoryID = in.readInt();
		this.approverName = in.readString();
		this.reimbursementFile = in.readString();
	}

	public static final Creator<DataReimbursementItem> CREATOR = new Creator<DataReimbursementItem>() {
		@Override
		public DataReimbursementItem createFromParcel(Parcel source) {
			return new DataReimbursementItem(source);
		}

		@Override
		public DataReimbursementItem[] newArray(int size) {
			return new DataReimbursementItem[size];
		}
	};
}