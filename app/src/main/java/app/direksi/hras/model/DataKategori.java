
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataKategori {

    @SerializedName("name")
    private String mName;
    @SerializedName("note")
    private String mNote;
    @SerializedName("reprimandCategoryID")
    private Long mReprimandCategoryID;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Long getReprimandCategoryID() {
        return mReprimandCategoryID;
    }

    public void setReprimandCategoryID(Long reprimandCategoryID) {
        mReprimandCategoryID = reprimandCategoryID;
    }

}
