package app.direksi.hras.model;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DetailLoanWithDetailData {

	@SerializedName("resultPayment")
	private List<DetailLoanWithDetailResultPaymentItem> resultPayment;

	@SerializedName("resultHeader")
	private DetailLoanWithDetailResultHeader resultHeader;

	@SerializedName("status")
	private String status;

	public void setResultPayment(List<DetailLoanWithDetailResultPaymentItem> resultPayment){
		this.resultPayment = resultPayment;
	}

	public List<DetailLoanWithDetailResultPaymentItem> getResultPayment(){
		return resultPayment;
	}

	public void setResultHeader(DetailLoanWithDetailResultHeader resultHeader){
		this.resultHeader = resultHeader;
	}

	public DetailLoanWithDetailResultHeader getResultHeader(){
		return resultHeader;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"resultPayment = '" + resultPayment + '\'' + 
			",resultHeader = '" + resultHeader + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}