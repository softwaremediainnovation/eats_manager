
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataMyAssetList {

    @SerializedName("asset")
    private AssetMyAssetList mAsset;

    public AssetMyAssetList getAsset() {
        return mAsset;
    }

    public void setAsset(AssetMyAssetList asset) {
        mAsset = asset;
    }

}
