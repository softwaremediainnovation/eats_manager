package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class RemuCutOffEmployeesItem {

	@SerializedName("dateCreated")
	private String dateCreated;

	@SerializedName("applicationUsers")
	private Object applicationUsers;

	@SerializedName("dateCutOffStart")
	private String dateCutOffStart;

	@SerializedName("creatorID")
	private String creatorID;

	@SerializedName("dateCutOffEnd")
	private String dateCutOffEnd;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("payrollMonth")
	private String payrollMonth;

	@SerializedName("organizationsID")
	private Object organizationsID;

	@SerializedName("employeesID")
	private Object employeesID;

	@SerializedName("cutOffEmployeeID")
	private int cutOffEmployeeID;

	@SerializedName("companiesID")
	private Object companiesID;

	public String getDateCreated(){
		return dateCreated;
	}

	public Object getApplicationUsers(){
		return applicationUsers;
	}

	public String getDateCutOffStart(){
		return dateCutOffStart;
	}

	public String getCreatorID(){
		return creatorID;
	}

	public String getDateCutOffEnd(){
		return dateCutOffEnd;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public String getPayrollMonth(){
		return payrollMonth;
	}

	public Object getOrganizationsID(){
		return organizationsID;
	}

	public Object getEmployeesID(){
		return employeesID;
	}

	public int getCutOffEmployeeID(){
		return cutOffEmployeeID;
	}

	public Object getCompaniesID(){
		return companiesID;
	}
}