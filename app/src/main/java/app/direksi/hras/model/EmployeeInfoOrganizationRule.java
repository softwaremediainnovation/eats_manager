
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class EmployeeInfoOrganizationRule {

    @SerializedName("company")
    private String mCompany;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("isAllow")
    private Boolean mIsAllow;
    @SerializedName("organization")
    private String mOrganization;
    @SerializedName("permission")
    private String mPermission;

    public String getCompany() {
        return mCompany;
    }

    public void setCompany(String company) {
        mCompany = company;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public Boolean getIsAllow() {
        return mIsAllow;
    }

    public void setIsAllow(Boolean isAllow) {
        mIsAllow = isAllow;
    }

    public String getOrganization() {
        return mOrganization;
    }

    public void setOrganization(String organization) {
        mOrganization = organization;
    }

    public String getPermission() {
        return mPermission;
    }

    public void setPermission(String permission) {
        mPermission = permission;
    }

}

