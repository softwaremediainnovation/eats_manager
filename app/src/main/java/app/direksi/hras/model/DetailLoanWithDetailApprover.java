package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DetailLoanWithDetailApprover {

	@SerializedName("lastName")
	private String lastName;

	@SerializedName("role")
	private Object role;

	@SerializedName("address")
	private Object address;

	@SerializedName("contractExpiredDate")
	private Object contractExpiredDate;

	@SerializedName("gender")
	private Object gender;

	@SerializedName("emailConfirmed")
	private boolean emailConfirmed;

	@SerializedName("isLeader")
	private Object isLeader;

	@SerializedName("isLoginAllowed")
	private boolean isLoginAllowed;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("userName")
	private String userName;

	@SerializedName("nik")
	private Object nik;

	@SerializedName("firstName")
	private String firstName;

	@SerializedName("photoUrl")
	private String photoUrl;

	@SerializedName("phoneNumber")
	private String phoneNumber;

	@SerializedName("phone")
	private Object phone;

	@SerializedName("signinToMobile")
	private boolean signinToMobile;

	@SerializedName("dob")
	private String dob;

	@SerializedName("permissions")
	private Object permissions;

	@SerializedName("organization")
	private Object organization;

	@SerializedName("id")
	private String id;

	@SerializedName("email")
	private String email;

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setRole(Object role){
		this.role = role;
	}

	public Object getRole(){
		return role;
	}

	public void setAddress(Object address){
		this.address = address;
	}

	public Object getAddress(){
		return address;
	}

	public void setContractExpiredDate(Object contractExpiredDate){
		this.contractExpiredDate = contractExpiredDate;
	}

	public Object getContractExpiredDate(){
		return contractExpiredDate;
	}

	public void setGender(Object gender){
		this.gender = gender;
	}

	public Object getGender(){
		return gender;
	}

	public void setEmailConfirmed(boolean emailConfirmed){
		this.emailConfirmed = emailConfirmed;
	}

	public boolean isEmailConfirmed(){
		return emailConfirmed;
	}

	public void setIsLeader(Object isLeader){
		this.isLeader = isLeader;
	}

	public Object getIsLeader(){
		return isLeader;
	}

	public void setIsLoginAllowed(boolean isLoginAllowed){
		this.isLoginAllowed = isLoginAllowed;
	}

	public boolean isIsLoginAllowed(){
		return isLoginAllowed;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setNik(Object nik){
		this.nik = nik;
	}

	public Object getNik(){
		return nik;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setPhotoUrl(String photoUrl){
		this.photoUrl = photoUrl;
	}

	public String getPhotoUrl(){
		return photoUrl;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return phoneNumber;
	}

	public void setPhone(Object phone){
		this.phone = phone;
	}

	public Object getPhone(){
		return phone;
	}

	public void setSigninToMobile(boolean signinToMobile){
		this.signinToMobile = signinToMobile;
	}

	public boolean isSigninToMobile(){
		return signinToMobile;
	}

	public void setDob(String dob){
		this.dob = dob;
	}

	public String getDob(){
		return dob;
	}

	public void setPermissions(Object permissions){
		this.permissions = permissions;
	}

	public Object getPermissions(){
		return permissions;
	}

	public void setOrganization(Object organization){
		this.organization = organization;
	}

	public Object getOrganization(){
		return organization;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	@Override
 	public String toString(){
		return 
			"Approver{" + 
			"lastName = '" + lastName + '\'' + 
			",role = '" + role + '\'' + 
			",address = '" + address + '\'' + 
			",contractExpiredDate = '" + contractExpiredDate + '\'' + 
			",gender = '" + gender + '\'' + 
			",emailConfirmed = '" + emailConfirmed + '\'' + 
			",isLeader = '" + isLeader + '\'' + 
			",isLoginAllowed = '" + isLoginAllowed + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",isActive = '" + isActive + '\'' + 
			",userName = '" + userName + '\'' + 
			",nik = '" + nik + '\'' + 
			",firstName = '" + firstName + '\'' + 
			",photoUrl = '" + photoUrl + '\'' + 
			",phoneNumber = '" + phoneNumber + '\'' + 
			",phone = '" + phone + '\'' + 
			",signinToMobile = '" + signinToMobile + '\'' + 
			",dob = '" + dob + '\'' + 
			",permissions = '" + permissions + '\'' + 
			",organization = '" + organization + '\'' + 
			",id = '" + id + '\'' + 
			",email = '" + email + '\'' + 
			"}";
		}
}