
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DetailAsetHistory {

    @SerializedName("date")
    private String mDate;
    @SerializedName("employee")
    private String mEmployee;
    @SerializedName("note")
    private String mNote;
    @SerializedName("status")
    private String mStatus;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getEmployee() {
        return mEmployee;
    }

    public void setEmployee(String employee) {
        mEmployee = employee;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
