package app.direksi.hras.model;

import com.google.gson.annotations.Expose;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class DataFetchEmployeeItem{

	@SerializedName("firstName")
	@Expose
	private String firstName;

	@SerializedName("lastName")
	@Expose
	private String lastName;

	@SerializedName("nik")
	@Expose
	private String nik;

	@SerializedName("isLeader")
	@Expose
	private boolean isLeader;

	@SerializedName("employeeID")
	@Expose
	private int employeeID;

	@SerializedName("fullNameNik")
	@Expose
	private String fullNameNik;

	@SerializedName("organizationName")
	@Expose
	private String organizationName;


	public void setOrganizationName(String firstName){
		this.organizationName = firstName;
	}

	public String getOrganizationName(){
		return organizationName;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setNik(String nik){
		this.nik = nik;
	}

	public String getNik(){
		return nik;
	}

	public void setIsLeader(boolean isLeader){
		this.isLeader = isLeader;
	}

	public boolean isIsLeader(){
		return isLeader;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setFullNameNik(String fullNameNik){
		this.fullNameNik = fullNameNik;
	}

	public String getFullNameNik(){
		return fullNameNik;
	}

	@Override
 	public String toString(){
		return 
			"DataFetchEmployeeItem{" + 
			"firstName = '" + firstName + '\'' + 
			",lastName = '" + lastName + '\'' + 
			",nik = '" + nik + '\'' + 
			",isLeader = '" + isLeader + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",fullNameNik = '" + fullNameNik + '\'' + 
			"}";
		}
}