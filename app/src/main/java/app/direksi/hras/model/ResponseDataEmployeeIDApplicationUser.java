
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ResponseDataEmployeeIDApplicationUser {

    @SerializedName("address")
    private Object mAddress;
    @SerializedName("contractExpiredDate")
    private Object mContractExpiredDate;
    @SerializedName("dob")
    private String mDob;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("emailConfirmed")
    private Boolean mEmailConfirmed;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("firstName")
    private String mFirstName;
    @SerializedName("gender")
    private Object mGender;
    @SerializedName("id")
    private String mId;
    @SerializedName("isActive")
    private Boolean mIsActive;
    @SerializedName("isLeader")
    private Object mIsLeader;
    @SerializedName("isLoginAllowed")
    private Boolean mIsLoginAllowed;
    @SerializedName("lastName")
    private String mLastName;
    @SerializedName("nik")
    private Object mNik;
    @SerializedName("organization")
    private ResponseDataEmployeeIDOrganization mOrganization;
    @SerializedName("permissions")
    private Object mPermissions;
    @SerializedName("phone")
    private String mPhone;
    @SerializedName("phoneNumber")
    private String mPhoneNumber;
    @SerializedName("photoUrl")
    private String mPhotoUrl;
    @SerializedName("role")
    private Object mRole;
    @SerializedName("signinToMobile")
    private Boolean mSigninToMobile;
    @SerializedName("userName")
    private String mUserName;

    public Object getAddress() {
        return mAddress;
    }

    public void setAddress(Object address) {
        mAddress = address;
    }

    public Object getContractExpiredDate() {
        return mContractExpiredDate;
    }

    public void setContractExpiredDate(Object contractExpiredDate) {
        mContractExpiredDate = contractExpiredDate;
    }

    public String getDob() {
        return mDob;
    }

    public void setDob(String dob) {
        mDob = dob;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Boolean getEmailConfirmed() {
        return mEmailConfirmed;
    }

    public void setEmailConfirmed(Boolean emailConfirmed) {
        mEmailConfirmed = emailConfirmed;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public Object getGender() {
        return mGender;
    }

    public void setGender(Object gender) {
        mGender = gender;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public Boolean getIsActive() {
        return mIsActive;
    }

    public void setIsActive(Boolean isActive) {
        mIsActive = isActive;
    }

    public Object getIsLeader() {
        return mIsLeader;
    }

    public void setIsLeader(Object isLeader) {
        mIsLeader = isLeader;
    }

    public Boolean getIsLoginAllowed() {
        return mIsLoginAllowed;
    }

    public void setIsLoginAllowed(Boolean isLoginAllowed) {
        mIsLoginAllowed = isLoginAllowed;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Object getNik() {
        return mNik;
    }

    public void setNik(Object nik) {
        mNik = nik;
    }

    public ResponseDataEmployeeIDOrganization getOrganization() {
        return mOrganization;
    }

    public void setOrganization(ResponseDataEmployeeIDOrganization organization) {
        mOrganization = organization;
    }

    public Object getPermissions() {
        return mPermissions;
    }

    public void setPermissions(Object permissions) {
        mPermissions = permissions;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String phone) {
        mPhone = phone;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getPhotoUrl() {
        return mPhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        mPhotoUrl = photoUrl;
    }

    public Object getRole() {
        return mRole;
    }

    public void setRole(Object role) {
        mRole = role;
    }

    public Boolean getSigninToMobile() {
        return mSigninToMobile;
    }

    public void setSigninToMobile(Boolean signinToMobile) {
        mSigninToMobile = signinToMobile;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

}
