package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@Generated("com.robohorse.robopojogenerator")
public class DetailLoanWithDetailResultHeader {


	@SerializedName("date")
	private String date;

	@SerializedName("note")
	private String note;

	@SerializedName("datePaid")
	private String datePaid;

	@SerializedName("installmentAmount")
	private Long installmentAmount;

	@SerializedName("approverID")
	private String approverID;

	@SerializedName("dateCair")
	private String dateCair;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("approver2")
	private String approver2;

	@SerializedName("employee")
	private DetailLoanWithDetailEmployee employee;

	@SerializedName("isApproved2")
	private Boolean isApproved2;

	@SerializedName("dateApproved")
	private String dateApproved;

	@SerializedName("isApproved")
	private Boolean isApproved;

	@SerializedName("loanID")
	private long loanID;

	@SerializedName("dateApproved2")
	private String dateApproved2;

	@SerializedName("approver")
	private DetailLoanWithDetailApprover approver;

	@SerializedName("approverID2")
	private String approverID2;

	@SerializedName("amount")
	private Long amount;

	@SerializedName("comment2")
	private String comment2;

	@SerializedName("userCair")
	private String userCair;

	@SerializedName("methodCair")
	private String methodCair;

	@SerializedName("organizationID")
	private int organizationID;

	@SerializedName("isCair")
	private Boolean isCair;

	@SerializedName("isPaid")
	private Boolean isPaid;

	@SerializedName("employeeApprover")
	private DetailLoanWithDetailEmployeeApprover employeeApprover;

	@SerializedName("isPbonMingguan")
	private Boolean isPbonMingguan;

	@SerializedName("installment")
	private Long installment;

	@SerializedName("installmentPayOut")
	private Long installmentPayOut;

	@SerializedName("organization")
	private String organization;

	@SerializedName("name")
	private String name;

	@SerializedName("comment")
	private String comment;

	@SerializedName("bankName")
	private String mBankName;
	@SerializedName("bankNumber")
	private String mBankNumber;
	@SerializedName("bankAccountName")
	private String mBankAccountName;
	@SerializedName("xendit")
	private List<XenditItem> xendit;

	@SerializedName("loanStartDate")
	private String loanStartDate;

	@SerializedName("keyXendit")
	private String mKeyXendit;


	public String getKeyXendit() {
		return mKeyXendit;
	}

	public void setKeyXendit(String xendit) {
		mKeyXendit = xendit;
	}


	public String getLoanStartDate() {
		return loanStartDate;
	}

	public void setLoanStartDate(String contractExpiredDate) {
		loanStartDate = contractExpiredDate;
	}

	public void setXendit(List<XenditItem> xendit){
		this.xendit = xendit;
	}

	public List<XenditItem> getXendit(){
		return xendit;
	}

	public String getBankName() {
		return mBankName;
	}

	public void setBankName(String contractExpiredDate) {
		mBankName = contractExpiredDate;
	}

	public String getBankNumber() {
		return mBankNumber;
	}

	public void setBankNumber(String contractExpiredDate) {
		mBankNumber = contractExpiredDate;
	}

	public String getBankAccountName() {
		return mBankAccountName;
	}

	public void setBankAccountName(String contractExpiredDate) {
		mBankAccountName = contractExpiredDate;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setNote(String note){
		this.note = note;
	}

	public String getNote(){
		return note;
	}

	public void setDatePaid(String datePaid){
		this.datePaid = datePaid;
	}

	public String getDatePaid(){
		return datePaid;
	}

	public void setInstallmentAmount(Long installmentAmount){
		this.installmentAmount = installmentAmount;
	}

	public Long getInstallmentAmount(){
		return installmentAmount;
	}

	public void setApproverID(String approverID){
		this.approverID = approverID;
	}

	public String getApproverID(){
		return approverID;
	}

	public void setDateCair(String dateCair){
		this.dateCair = dateCair;
	}

	public String getDateCair(){
		return dateCair;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setApprover2(String approver2){
		this.approver2 = approver2;
	}

	public String getApprover2(){
		return approver2;
	}

	public void setEmployee(DetailLoanWithDetailEmployee employee){
		this.employee = employee;
	}

	public DetailLoanWithDetailEmployee getEmployee(){
		return employee;
	}

	public void setIsApproved2(Boolean isApproved2){
		this.isApproved2 = isApproved2;
	}

	public Boolean isIsApproved2(){
		return isApproved2;
	}

	public void setDateApproved(String dateApproved){
		this.dateApproved = dateApproved;
	}

	public String getDateApproved(){
		return dateApproved;
	}

	public void setIsApproved(Boolean isApproved){
		this.isApproved = isApproved;
	}

	public Boolean isIsApproved(){
		return isApproved;
	}

	public void setLoanID(long loanID){
		this.loanID = loanID;
	}

	public long getLoanID(){
		return loanID;
	}

	public void setDateApproved2(String dateApproved2){
		this.dateApproved2 = dateApproved2;
	}

	public String getDateApproved2(){
		return dateApproved2;
	}

	public void setApprover(DetailLoanWithDetailApprover approver){
		this.approver = approver;
	}

	public DetailLoanWithDetailApprover getApprover(){
		return approver;
	}

	public void setApproverID2(String approverID2){
		this.approverID2 = approverID2;
	}

	public String getApproverID2(){
		return approverID2;
	}

	public void setAmount(Long amount){
		this.amount = amount;
	}

	public Long getAmount(){
		return amount;
	}

	public void setComment2(String comment2){
		this.comment2 = comment2;
	}

	public String getComment2(){
		return comment2;
	}

	public void setUserCair(String userCair){
		this.userCair = userCair;
	}

	public String getUserCair(){
		return userCair;
	}

	public void setMethodCair(String methodCair){
		this.methodCair = methodCair;
	}

	public String getMethodCair(){
		return methodCair;
	}

	public void setOrganizationID(int organizationID){
		this.organizationID = organizationID;
	}

	public int getOrganizationID(){
		return organizationID;
	}

	public void setIsCair(Boolean isCair){
		this.isCair = isCair;
	}

	public Boolean isIsCair(){
		return isCair;
	}

	public void setIsPaid(Boolean isPaid){
		this.isPaid = isPaid;
	}

	public Boolean isIsPaid(){
		return isPaid;
	}

	public void setEmployeeApprover(DetailLoanWithDetailEmployeeApprover employeeApprover){
		this.employeeApprover = employeeApprover;
	}

	public DetailLoanWithDetailEmployeeApprover getEmployeeApprover(){
		return employeeApprover;
	}

	public void setIsPbonMingguan(Boolean isPbonMingguan){
		this.isPbonMingguan = isPbonMingguan;
	}

	public Boolean isIsPbonMingguan(){
		return isPbonMingguan;
	}

	public void setInstallment(Long installment){
		this.installment = installment;
	}

	public Long getInstallment(){
		return installment;
	}

	public void setInstallmentPayOut(Long installmentPayOut){
		this.installmentPayOut = installmentPayOut;
	}

	public Long getInstallmentPayOut(){
		return installmentPayOut;
	}

	public void setOrganization(String organization){
		this.organization = organization;
	}

	public String getOrganization(){
		return organization;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setComment(String comment){
		this.comment = comment;
	}

	public String getComment(){
		return comment;
	}

	@Override
 	public String toString(){
		return 
			"ResultHeader{" + 
			"date = '" + date + '\'' + 
			",note = '" + note + '\'' + 
			",datePaid = '" + datePaid + '\'' + 
			",installmentAmount = '" + installmentAmount + '\'' + 
			",approverID = '" + approverID + '\'' + 
			",dateCair = '" + dateCair + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",approver2 = '" + approver2 + '\'' + 
			",employee = '" + employee + '\'' + 
			",isApproved2 = '" + isApproved2 + '\'' + 
			",dateApproved = '" + dateApproved + '\'' + 
			",isApproved = '" + isApproved + '\'' + 
			",loanID = '" + loanID + '\'' + 
			",dateApproved2 = '" + dateApproved2 + '\'' + 
			",approver = '" + approver + '\'' + 
			",approverID2 = '" + approverID2 + '\'' + 
			",amount = '" + amount + '\'' + 
			",comment2 = '" + comment2 + '\'' + 
			",userCair = '" + userCair + '\'' + 
			",methodCair = '" + methodCair + '\'' + 
			",organizationID = '" + organizationID + '\'' + 
			",isCair = '" + isCair + '\'' + 
			",isPaid = '" + isPaid + '\'' + 
			",employeeApprover = '" + employeeApprover + '\'' + 
			",isPbonMingguan = '" + isPbonMingguan + '\'' + 
			",installment = '" + installment + '\'' + 
			",installmentPayOut = '" + installmentPayOut + '\'' + 
			",organization = '" + organization + '\'' + 
			",name = '" + name + '\'' + 
			",comment = '" + comment + '\'' + 
			"}";
		}
}