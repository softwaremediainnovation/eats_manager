
package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class DataKasbon {

    @SerializedName("activityID")
    private Long mActivityID;
    @SerializedName("amount")
    private Long mAmount;
    @SerializedName("approver")
    private Object mApprover;
    @SerializedName("approverID")
    private Object mApproverID;
    @SerializedName("comment")
    private String mComment;
    @SerializedName("date")
    private String mDate;
    @SerializedName("dateApproved")
    private Object mDateApproved;
    @SerializedName("dateCreated")
    private Object mDateCreated;
    @SerializedName("employee")
    private Object mEmployee;
    @SerializedName("employeeApprover")
    private Object mEmployeeApprover;
    @SerializedName("employeeID")
    private Long mEmployeeID;
    @SerializedName("filePath")
    private Object mFilePath;
    @SerializedName("isApproved")
    private Boolean mIsApproved;
    @SerializedName("kasbonID")
    private Long mKasbonID;
    @SerializedName("note")
    private String mNote;
    @SerializedName("organizationID")
    private Object mOrganizationID;
    @SerializedName("reimbursementCategory")
    private Object mReimbursementCategory;
    @SerializedName("reimbursementCategoryID")
    private Long mReimbursementCategoryID;
    @SerializedName("reimbursementID")
    private Long mReimbursementID;

    public Long getActivityID() {
        return mActivityID;
    }

    public void setActivityID(Long activityID) {
        mActivityID = activityID;
    }

    public Long getAmount() {
        return mAmount;
    }

    public void setAmount(Long amount) {
        mAmount = amount;
    }

    public Object getApprover() {
        return mApprover;
    }

    public void setApprover(Object approver) {
        mApprover = approver;
    }

    public Object getApproverID() {
        return mApproverID;
    }

    public void setApproverID(Object approverID) {
        mApproverID = approverID;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Object getDateApproved() {
        return mDateApproved;
    }

    public void setDateApproved(Object dateApproved) {
        mDateApproved = dateApproved;
    }

    public Object getDateCreated() {
        return mDateCreated;
    }

    public void setDateCreated(Object dateCreated) {
        mDateCreated = dateCreated;
    }

    public Object getEmployee() {
        return mEmployee;
    }

    public void setEmployee(Object employee) {
        mEmployee = employee;
    }

    public Object getEmployeeApprover() {
        return mEmployeeApprover;
    }

    public void setEmployeeApprover(Object employeeApprover) {
        mEmployeeApprover = employeeApprover;
    }

    public Long getEmployeeID() {
        return mEmployeeID;
    }

    public void setEmployeeID(Long employeeID) {
        mEmployeeID = employeeID;
    }

    public Object getFilePath() {
        return mFilePath;
    }

    public void setFilePath(Object filePath) {
        mFilePath = filePath;
    }

    public Boolean getIsApproved() {
        return mIsApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        mIsApproved = isApproved;
    }

    public Long getKasbonID() {
        return mKasbonID;
    }

    public void setKasbonID(Long kasbonID) {
        mKasbonID = kasbonID;
    }

    public String getNote() {
        return mNote;
    }

    public void setNote(String note) {
        mNote = note;
    }

    public Object getOrganizationID() {
        return mOrganizationID;
    }

    public void setOrganizationID(Object organizationID) {
        mOrganizationID = organizationID;
    }

    public Object getReimbursementCategory() {
        return mReimbursementCategory;
    }

    public void setReimbursementCategory(Object reimbursementCategory) {
        mReimbursementCategory = reimbursementCategory;
    }

    public Long getReimbursementCategoryID() {
        return mReimbursementCategoryID;
    }

    public void setReimbursementCategoryID(Long reimbursementCategoryID) {
        mReimbursementCategoryID = reimbursementCategoryID;
    }

    public Long getReimbursementID() {
        return mReimbursementID;
    }

    public void setReimbursementID(Long reimbursementID) {
        mReimbursementID = reimbursementID;
    }

}
