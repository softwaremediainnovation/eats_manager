package app.direksi.hras.model;

import com.google.gson.annotations.SerializedName;

public class DataInformations {

	@SerializedName("suratIjin")
	private Integer suratIjin;

	@SerializedName("cutiHaid")
	private Integer cutiHaid;

	@SerializedName("suratDokter")
	private Integer suratDokter;

	@SerializedName("cutiTahunan")
	private Integer cutiTahunan;

	@SerializedName("employee")
	private EmployeeInformations employee;

	@SerializedName("mangkir")
	private Integer mangkir;

	@SerializedName("gajiSekarang")
	private Integer gajiSekarang;

	@SerializedName("suratKeterangan")
	private Integer suratKeterangan;

	@SerializedName("dispensasi")
	private Integer dispensasi;

	@SerializedName("lastCreatedDate")
	private String lastCreatedDate;

	@SerializedName("lastEvaluatioID")
	private Integer lastEvaluatioID;

	@SerializedName("encrypted")
	private String encrypted;

	public Integer getSuratIjin(){
		return suratIjin;
	}

	public Integer getCutiHaid(){
		return cutiHaid;
	}

	public Integer getSuratDokter(){
		return suratDokter;
	}

	public Integer getCutiTahunan(){
		return cutiTahunan;
	}

	public EmployeeInformations getEmployee(){
		return employee;
	}

	public Integer getMangkir(){
		return mangkir;
	}

	public Integer getGajiSekarang(){
		return gajiSekarang;
	}

	public Integer getSuratKeterangan(){
		return suratKeterangan;
	}

	public Integer getDispensasi(){
		return dispensasi;
	}

	public String getLastCreatedDate(){
		return lastCreatedDate;
	}

	public String getEncrypted(){
		return encrypted;
	}

	public Integer getLastEvaluatioID(){
		return lastEvaluatioID;
	}
}