package app.direksi.hras.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class CEmployeeShare {

	@SerializedName("isGraduate")
	private boolean isGraduate;

	@SerializedName("majorID")
	private int majorID;

	@SerializedName("address")
	private String address;

	@SerializedName("employeeID")
	private int employeeID;

	@SerializedName("dateEnd")
	private String dateEnd;

	@SerializedName("employee")
	private Object employee;

	@SerializedName("instituteName")
	private String instituteName;

	@SerializedName("score")
	private Double score;

	@SerializedName("major")
	private Object major;

	@SerializedName("dateStart")
	private String dateStart;

	@SerializedName("educationLevel")
	private Object educationLevel;

	@SerializedName("employeeEducationID")
	private int employeeEducationID;

	@SerializedName("educationLevelID")
	private int educationLevelID;

	public void setIsGraduate(boolean isGraduate){
		this.isGraduate = isGraduate;
	}

	public boolean isIsGraduate(){
		return isGraduate;
	}

	public void setMajorID(int majorID){
		this.majorID = majorID;
	}

	public int getMajorID(){
		return majorID;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setEmployeeID(int employeeID){
		this.employeeID = employeeID;
	}

	public int getEmployeeID(){
		return employeeID;
	}

	public void setDateEnd(String dateEnd){
		this.dateEnd = dateEnd;
	}

	public String getDateEnd(){
		return dateEnd;
	}

	public void setEmployee(Object employee){
		this.employee = employee;
	}

	public Object getEmployee(){
		return employee;
	}

	public void setInstituteName(String instituteName){
		this.instituteName = instituteName;
	}

	public String getInstituteName(){
		return instituteName;
	}

	public void setScore(Double score){
		this.score = score;
	}

	public Double getScore(){
		return score;
	}

	public void setMajor(Object major){
		this.major = major;
	}

	public Object getMajor(){
		return major;
	}

	public void setDateStart(String dateStart){
		this.dateStart = dateStart;
	}

	public String getDateStart(){
		return dateStart;
	}

	public void setEducationLevel(Object educationLevel){
		this.educationLevel = educationLevel;
	}

	public Object getEducationLevel(){
		return educationLevel;
	}

	public void setEmployeeEducationID(int employeeEducationID){
		this.employeeEducationID = employeeEducationID;
	}

	public int getEmployeeEducationID(){
		return employeeEducationID;
	}

	public void setEducationLevelID(int educationLevelID){
		this.educationLevelID = educationLevelID;
	}

	public int getEducationLevelID(){
		return educationLevelID;
	}

	@Override
 	public String toString(){
		return 
			"C{" + 
			"isGraduate = '" + isGraduate + '\'' + 
			",majorID = '" + majorID + '\'' + 
			",address = '" + address + '\'' + 
			",employeeID = '" + employeeID + '\'' + 
			",dateEnd = '" + dateEnd + '\'' + 
			",employee = '" + employee + '\'' + 
			",instituteName = '" + instituteName + '\'' + 
			",score = '" + score + '\'' + 
			",major = '" + major + '\'' + 
			",dateStart = '" + dateStart + '\'' + 
			",educationLevel = '" + educationLevel + '\'' + 
			",employeeEducationID = '" + employeeEducationID + '\'' + 
			",educationLevelID = '" + educationLevelID + '\'' + 
			"}";
		}
}