package app.direksi.hras;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.util.Iterator;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.DataReprimand;
import app.direksi.hras.model.ResponseDetailReprimand;
import app.direksi.hras.model.ResponseGeneral;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 16/05/2019.
 */

public class DetailTeguranKuActivity extends AppCompatActivity {

    DataReprimand dataTeguran;

    public TextView txtTanggal, txtKategori, txtCreator, txtCatatan,
            profil_nama,
            profil_role,
            profil_email,
            profil_phone,
            txtOrganisasi;
    private ImageView profile_image;
    private ImageView mIconStatus;
    private CardView mLayoutImage, mLayoutImages;
    private ImageView mImageView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;
    private Long id;
    private Button prosesDelete;
    private static final int REQUEST_PHONE_CALL = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailTeguranKuActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading


        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_teguran));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_teguran);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        dataTeguran = gson.fromJson(getIntent().getStringExtra("detail"), DataReprimand.class);
        //dataCutiItem = getIntent().getParcelableExtra("detail");

        txtTanggal = findViewById(R.id.txtTanggal);
        txtKategori = findViewById(R.id.txtKategori);
        txtCreator = findViewById(R.id.txtCreator);
        txtCatatan = findViewById(R.id.txtCatatan);
        mImageView = findViewById(R.id.mImageView);
        mLayoutImages = findViewById(R.id.mLayoutImages);
        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);
        messageDialog = new MessageDialog();
        prosesDelete = findViewById(R.id.prosesDelete);

        prosesDelete.setVisibility(View.GONE);

        loading = new SweetAlertDialog(DetailTeguranKuActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);

        profile_image = (ImageView) findViewById(R.id.profile_image);
        profil_nama = (TextView) findViewById(R.id.profil_nama);
        profil_role =  (TextView) findViewById(R.id.profil_role);
        profil_email = (TextView)  findViewById(R.id.profil_email);
        profil_phone = (TextView) findViewById(R.id.profil_phone);
        txtOrganisasi = (TextView) findViewById(R.id.txtOrganisasi);


        if (dataTeguran != null) {
            DetailTeguran(dataTeguran.getReprimandID().intValue());
        }


        onNewIntent(getIntent());






    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        DetailTeguran(idx);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void terimaCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // ProsesDelete();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void tolakCuti(View v) {

        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //  ProsesUpdate();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void prosesDelete(View v) {
        new android.app.AlertDialog.Builder(DetailTeguranKuActivity.this)
                .setTitle(getResources().getString(R.string.string_confirmation))
                .setMessage(getResources().getString(R.string.title_apakah_anda_yakin))
                .setPositiveButton(getResources().getString(R.string.string_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteData();
                    }

                })
                .setNegativeButton(getResources().getString(R.string.txt_no), null)
                .show();


    }

    public void DeleteData() {

        loading.show();

        try {




            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTeguranKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));

            String idx = String.valueOf(id);


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseGeneral> call = api.deleteTeguran(idx, Authorization);

            call.enqueue(new Callback<ResponseGeneral>() {
                @Override
                public void onResponse(Call<ResponseGeneral> call, Response<ResponseGeneral> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            PreferenceManager.getDefaultSharedPreferences(DetailTeguranKuActivity.this).
                                    edit().putString(getResources().getString(R.string.pref_dayoff_flag), "0")
                                    .apply();
                            loading.dismiss();

                            new SweetAlertDialog(DetailTeguranKuActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getResources().getString(R.string.title_sukses))
                                    .setContentText(getResources().getString(R.string.title_berhasil_hapus_data))
                                    .setConfirmText("OK")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {


                                            Intent login = new Intent(DetailTeguranKuActivity.this, WarningLetterActivity.class);
                                            login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                            startActivity(login);



                                        }
                                    })
                                    .show();


                        }
                        else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(DetailTeguranKuActivity.this,getResources().getString(R.string.title_gagal)
                                    , "");
                        }

                    } else {
                        loading.dismiss();
                        messageDialog.mShowMessageError(DetailTeguranKuActivity.this,getResources().getString(R.string.title_gagal)
                                , "");
                    }
                }

                @Override
                public void onFailure(Call<ResponseGeneral> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(DetailTeguranKuActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(DetailTeguranKuActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }

    }

    public void DetailTeguran(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailTeguranKuActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailReprimand> call = api.getTeguranDetailReprimand(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDetailReprimand>() {
                @Override
                public void onResponse(Call<ResponseDetailReprimand> call, Response<ResponseDetailReprimand> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            ShowData(response.body().getData());



                        }
                        else {
                            loading.dismiss();
                            messageDialog.mShowMessageError(DetailTeguranKuActivity.this,getResources().getString(R.string.title_gagal)
                                    , response.body().getErrMessage());
                        }

                    } else {
                        loading.dismiss();
                        messageDialog.mShowMessageError(DetailTeguranKuActivity.this,getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailReprimand> call, Throwable t) {
                    loading.dismiss();
                    messageDialog.mShowMessageError(DetailTeguranKuActivity.this, getResources().getString(R.string.title_gagal)
                            , getResources().getString(R.string.loading_error));
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            messageDialog.mShowMessageError(DetailTeguranKuActivity.this, getResources().getString(R.string.title_gagal)
                    , getResources().getString(R.string.loading_error));
        }
    }

    private void ShowData(DataReprimand data){
        id = data.getReprimandID();
        if (data.getDate() != null)
            txtTanggal.setText(DefaultFormatter.changeFormatDate(data.getDate()));

        txtOrganisasi.setText(data.getOrganization()!=null ? data.getOrganization() : "");
        txtKategori.setText(data.getReprimandCategory().getName());
        txtCreator.setText(data.getIssuer().getFirstName() + " " + data.getIssuer().getLastName());
        txtCatatan.setText(data.getNote());




        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);



        if (data.getFilePath() != null) {


            Glide.with(DetailTeguranKuActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.getFilePath()))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);
            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                    intent.putExtra("image", data.getFilePath());
                    startActivity(intent);
                }
            });
        }
        else {
            mImageView.setVisibility(View.GONE);
        }

        if (data.getEmployee() != null) {
            if (data.getEmployee().getFirstName() != null) {
                profil_nama.setText(data.getEmployee().getFirstName() + " " + data.getEmployee().getLastName());
            }
            if (data.getEmployee().getNik() != null) {
                profil_role.setText(data.getEmployee().getNik());
            }
            if (data.getEmployee().getEmail() != null) {
                profil_email.setText(data.getEmployee().getEmail());
            }
            if (data.getEmployee().getPhone() != null) {
                profil_phone.setText(data.getEmployee().getPhone());
            }

            if (data.getEmployee().getPhone().equals("") || data.getEmployee().getPhone() == null) {

            } else{
                mLayoutImages.setOnClickListener(new View.OnClickListener() { //set on click dialog saat di clik no telepon Contack person
                    @Override
                    public void onClick(View v) {
                        final Context context = v.getContext();
//                    CharSequence colors[] = new CharSequence[]{"Call", "Send SMS", "Send WA", "Save Contact"};
                        CharSequence colors[] = new CharSequence[]{context.getResources().getString(R.string.title_panggil), context.getResources().getString(R.string.title_kirim_sms), context.getResources().getString(R.string.title_simpan_kontak), context.getResources().getString(R.string.title_chat_wa)};

                        AlertDialog.Builder builder = new AlertDialog.Builder(context);
                        builder.setTitle(getResources().getString(R.string.title_aksi));
                        builder.setItems(colors, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                        // Check Permissions Now
                                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CALL_PHONE}, REQUEST_PHONE_CALL);
                                    } else {
                                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + profil_phone.getText().toString().trim()));
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.getApplicationContext().startActivity(intent);
                                    }
                                } else if (which == 1) {
                                    Uri uri = Uri.parse("smsto:" + profil_phone.getText().toString().trim());
                                    Intent mSendSms = new Intent(Intent.ACTION_SENDTO, uri);
                                    mSendSms.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.getApplicationContext().startActivity(mSendSms);
                                } else if (which == 2) {
                                    String name = data.getEmployee().getFirstName() + " "+ data.getEmployee().getLastName();
                                    String phone = data.getEmployee().getPhone();
//                            String email = ReportResult.get(position).getEmail();
                                    Intent contactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                                    contactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

                                    contactIntent
                                            .putExtra(ContactsContract.Intents.Insert.NAME, name)
                                            .putExtra(ContactsContract.Intents.Insert.PHONE, phone);
//                                    .putExtra(ContactsContract.Intents.Insert.EMAIL, email);

                                    ((Activity) context).startActivityForResult(contactIntent, 1);

                                } else if (which == 3) {

                                    voiceCall(profil_phone.getText().toString().trim());
                                }
                            }
                        });
                        builder.show();
                    }
                });
            }

            if (data.getEmployee().getPhotoUrl() != null) {

                Glide.with(DetailTeguranKuActivity.this)
                        .load((getResources().getString(R.string.base_url) + data.getEmployee().getPhotoUrl()))
                        .apply(requestOptions)
                        .error(Glide.with(profile_image).load(R.drawable.profile))
                        .into(profile_image);
                profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent intent = new Intent(getApplicationContext(), ZoomPhotoActivity.class);
                        intent.putExtra("image", data.getEmployee().getPhotoUrl());
                        startActivity(intent);
                    }
                });

            }
            else {
                // profile_image.setVisibility(View.GONE);
            }
        }
    }

    public void voiceCall(String no_Telp){

        try {

            String str = no_Telp;


            if (str.substring(0, 1).equals("0")) {
                str = "+62" + str.substring(1);

            } else {
                str = "+62" + str;
            }


            Uri uri = Uri.parse("smsto:" + str);
            Intent i = new Intent(Intent.ACTION_SENDTO, uri);
            i.putExtra("sms_body", "Hello");
            i.setPackage("com.whatsapp");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(i);

        } catch ( Exception e){
            MDToast.makeText(DetailTeguranKuActivity.this, getResources().getString(R.string.title_tidak_terimstal_wa),
                    MDToast.LENGTH_LONG, MDToast.TYPE_WARNING).show();
           // Toast.makeText(DetailTeguranKuActivity.this, "Tidak terinstall WA", Toast.LENGTH_LONG).show();

        }


    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailTeguranKuActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}
