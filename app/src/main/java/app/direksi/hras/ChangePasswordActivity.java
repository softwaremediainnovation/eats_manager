package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import app.direksi.hras.APIInterface.RegisterAPIInterface;


import java.util.HashMap;
import java.util.Map;

import app.direksi.hras.model.ResponseDataChangePassword;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.Validation;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 07/03/2018.
 */

public class ChangePasswordActivity extends AppCompatActivity {


    private EditText inputOldPass, inputNewPass, inputConfirmNewPass;
    private TextInputLayout til_old_password, til_new_password, til_confirm_new_password;
    private Button btnUbahPassword;
    private Validation val;
    SweetAlertDialog loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(ChangePasswordActivity.this);
        setContentView(R.layout.activity_chage_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setElevation(0);//value 0 to remove line

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.change_password));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);

        //change transition open and close layout
        //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        btnUbahPassword = (Button) findViewById(R.id.btnUbahPassword);
        inputOldPass = (EditText) findViewById(R.id.input_old_password);
        inputOldPass.setTransformationMethod(new PasswordTransformationMethod());
        inputNewPass = (EditText) findViewById(R.id.input_new_password);
        inputNewPass.setTransformationMethod(new PasswordTransformationMethod());
        inputConfirmNewPass = (EditText) findViewById(R.id.input_confirm_new_password);
        inputConfirmNewPass.setTransformationMethod(new PasswordTransformationMethod());
        til_old_password = (TextInputLayout) findViewById(R.id.input_layout_old_password) ;
        til_new_password = (TextInputLayout) findViewById(R.id.input_layout_new_password) ;
        til_confirm_new_password = (TextInputLayout) findViewById(R.id.input_layout_confirm_new_password) ;
        btnUbahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickChangePassword();
            }
        });


        loading = new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*moveTaskToBack(true);
        finish();*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

     void clickChangePassword() {


         til_old_password.setError(null);
         til_new_password.setError(null);
         til_confirm_new_password.setError(null);


        String old = inputOldPass.getText().toString();
        String neww = inputNewPass.getText().toString();
        String conf = inputConfirmNewPass.getText().toString();


        boolean cancel = false;
        View focusView = null;


        /* if (!val.isEmpty(inputOldPass)) {
             if (!val.isPasswordValid(val.GetText(inputOldPass))) {
                 til_old_password.setError(getResources().getString(R.string.error_invalid_password));
                 focusView = inputOldPass;
                 cancel = true;
             }
         } else {
             til_old_password.setError(getResources().getString(R.string.error_password_required));
             focusView = inputOldPass;
             cancel = true;
         }

         if (!val.isEmpty(inputNewPass)) {
             if (!val.isPasswordValid(val.GetText(inputNewPass))) {
                 til_new_password.setError(getResources().getString(R.string.error_invalid_password));
                 focusView = inputNewPass;
                 cancel = true;
             }
         } else {
             til_new_password.setError(getResources().getString(R.string.error_password_required));
             focusView = inputNewPass;
             cancel = true;
         }

         if (!val.isEmpty(inputConfirmNewPass)) {
             if (!val.GetText(inputConfirmNewPass).equals(val.GetText(inputNewPass))) {
                 til_confirm_new_password.setError(getResources().getString(R.string.error_field_not_match));
                 focusView = inputNewPass;
                 cancel = true;
             }
         } else {
             til_confirm_new_password.setError(getResources().getString(R.string.error_password_required));
             focusView = inputNewPass;
             cancel = true;
         }*/





        if (TextUtils.isEmpty(old)) {
            til_old_password.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = inputOldPass;
            cancel = true;
        }

        if (!TextUtils.isEmpty(neww)) {
            if (!isPasswordValid6(neww)){
                til_new_password.setError(getResources().getString(R.string.error_invalid_password));
                focusView = inputNewPass;
                cancel = true;
            }


        }else {
            til_new_password.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = inputNewPass;
            cancel = true;
        }

        if (TextUtils.isEmpty(conf)) {
            til_confirm_new_password.setError(getResources().getString(R.string.title_isian_harus_diisi));
            focusView = inputConfirmNewPass;
            cancel = true;
        } else {

            if (!neww.equals(conf)) {
                til_confirm_new_password.setError(getResources().getString(R.string.error_field_not_match));
                focusView = inputConfirmNewPass;
                cancel = true;
            }

        }




        if (cancel) {
            // There was an error; don't attempt register and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            ForgetPassword();
        }










    }


    public void ForgetPassword() {
        loading.show();

        {
            try {
                String userID = PreferenceManager.getDefaultSharedPreferences(this).getString(
                        getResources().getString(R.string.pref_id), "");
                String Authorization = PreferenceManager.getDefaultSharedPreferences(this).getString(
                        getResources().getString(R.string.pref_token), "");
                String email = PreferenceManager.getDefaultSharedPreferences(this).getString(
                        getResources().getString(R.string.pref_email), "");

                Map<String, String> data = new HashMap<>();
                data.put("userID", userID);
                data.put("oldPassword", inputOldPass.getText().toString());
                data.put("NewPassword", inputNewPass.getText().toString());




                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.base_url))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

                // simplified call to request the news with already initialized service
                Call<ResponseDataChangePassword> call = api.changePassword(Authorization, data);

                Log.v("ForgetPassword", "sebelum call enqueue");

                call.enqueue(new Callback<ResponseDataChangePassword>() {
                    @Override
                    public void onResponse(Call<ResponseDataChangePassword> call, Response<ResponseDataChangePassword> response) {

                        if (response.isSuccessful()) {

                            if(response.body().getData().getStatus().equals("Success")){

                                loading.dismiss();
                                 SweetAlertDialog alertDialog = new SweetAlertDialog(ChangePasswordActivity.this,SweetAlertDialog.SUCCESS_TYPE);
                                    alertDialog.setTitleText(getResources().getString(R.string.title_sukses));
                                    alertDialog.setContentText("");
                                    alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                 public void onClick(SweetAlertDialog sweetAlertDialog) {


                                      Intent login = new Intent(ChangePasswordActivity.this, HomeActivity.class);
                                        login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                     startActivity(login);


                                 }
                                 });
                                 alertDialog.show();

                                 Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
                                  btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                                   btn.setTextColor(getResources().getColor(R.color.colorWhite));
                            }
                            else
                            {
                                loading.dismiss();
                                gagal(getResources().getString(R.string.title_password_salah));
                            }


                            Log.v("ForgetPassword", "Response Successful");

                        }else if (response.code() == 403) {

                            loading.dismiss();
                            gagal("");
                        } else if (response.errorBody() != null) {

                            loading.dismiss();
                            gagal("");

                        } else if (response.code() == 401) {
                            loading.dismiss();
                            gagal("");

                        }  else {

                            loading.dismiss();
                            gagal("");
                        }
                        // loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<ResponseDataChangePassword> call, Throwable t) {

                        loading.dismiss();
                        gagal("");
                    }
                });
                Log.v("ForgetPassword", "akhir dari TRY");




            }

            catch (Exception e) {

                loading.dismiss();
                gagal("");
            }


        }


    }


    private void gagal(String message){

        SweetAlertDialog alertDialog = new SweetAlertDialog(ChangePasswordActivity.this,SweetAlertDialog.ERROR_TYPE);
        alertDialog.setTitleText(getResources().getString(R.string.title_gagal));
        alertDialog.setContentText(message);
        alertDialog .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {

                sweetAlertDialog.dismiss();
            }
        });
        alertDialog.show();

        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
        btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btn.setTextColor(getResources().getColor(R.color.colorWhite));

    }




    private boolean isPasswordValid6(String password) {
        return password.length() > 5;
    }
    private boolean isPasswordValid8(String password) {
        return password.length() > 7;
    }


    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(ChangePasswordActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }

}
