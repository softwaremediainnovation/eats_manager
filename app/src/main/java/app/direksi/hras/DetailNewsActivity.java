package app.direksi.hras;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.appcompat.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;

import java.util.Iterator;
import java.util.Set;

import app.direksi.hras.APIInterface.RegisterAPIInterface;
import app.direksi.hras.model.DataDetailNews;
import app.direksi.hras.model.DataNews;
import app.direksi.hras.model.ResponseDetailNews;
import app.direksi.hras.util.DefaultFormatter;
import app.direksi.hras.util.MessageDialog;
import app.direksi.hras.util.MyContextWrapper;
import app.direksi.hras.util.TypefaceUtil;
import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.direksi.hras.util.MyContextWrapper.changeLang;

/**
 * Created by dhimaz on 12/03/2019.
 */

public class DetailNewsActivity extends AppCompatActivity {

    DataNews dataClaim;
    public TextView txtHeader, txtDate, txtDeskripsi;
    private ImageView mImageView;
    SweetAlertDialog loading;
    private MessageDialog messageDialog;
    private ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyContextWrapper.refreshBahasa(DetailNewsActivity.this);

        //remove line in bar
        //  getSupportActionBar().setElevation(0);//value 0 to remove line
        //enable back button in action bar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //change transition open and close layout
      //  overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_scale);
        //change font app
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Avenir.otf");
        //animation loading

        SpannableStringBuilder ss = new SpannableStringBuilder(getResources().getString(R.string.detail_news));
        ss.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),0, ss.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE); //bold
        getSupportActionBar().setTitle(ss);


        setContentView(R.layout.activity_detail_news);
        if (getSupportActionBar() != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);

                getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_white);
            }
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }
        Gson gson = new Gson();
        dataClaim = gson.fromJson(getIntent().getStringExtra("detail"), DataNews.class);
        txtHeader = findViewById(R.id.txtHeader);
        txtDate = findViewById(R.id.txtDate);
        txtDeskripsi = findViewById(R.id.txtDeskripsi);
        mImageView = findViewById(R.id.mImageView);

        scrollView = findViewById(R.id.scrollView);
        scrollView.setVisibility(View.GONE);


        loading = new SweetAlertDialog(DetailNewsActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        loading.getProgressHelper().setBarColor(Color.parseColor("#071f4b"));
        loading.setTitleText(getResources().getString(R.string.loading));
        loading.setCancelable(false);
        messageDialog = new MessageDialog();


        if (dataClaim != null) {
            DetailLoan(dataClaim.getArticleID().intValue());
        }else {
            onNewIntent(getIntent());
        }




    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        try {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                Set<String> keys = bundle.keySet();
                Iterator<String> it = keys.iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    if (key.toLowerCase().contains("body")) {
                        String mBody = bundle.get(key) + "";
                        // dataTeguran.setReprimandID(Long.parseLong(mBody));
                        int idx = Integer.parseInt(mBody);
                        // String a = String.valueOf(mIdReimbursement);
                        dataClaim = new DataNews();
                        dataClaim.setArticleID(Long.parseLong(String.valueOf(idx)));
                        DetailLoan(idx);
                        PreferenceManager.getDefaultSharedPreferences(DetailNewsActivity.this).
                                edit().putString(getResources().getString(R.string.pref_dayoff_notif), "1")
                                .apply();
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                //overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void DetailLoan(int id) {
        loading.show();

        try {

            String Authorization = (PreferenceManager.getDefaultSharedPreferences(DetailNewsActivity.this).getString(
                    getResources().getString(R.string.pref_token), ""));


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            RegisterAPIInterface api = retrofit.create(RegisterAPIInterface.class);

            // simplified call to request the news with already initialized service
            Call<ResponseDetailNews> call = api.getDetailNews(Authorization, String.valueOf(id));

            call.enqueue(new Callback<ResponseDetailNews>() {
                @Override
                public void onResponse(Call<ResponseDetailNews> call, Response<ResponseDetailNews> response) {


                    if (response.isSuccessful()) {
                        long statusCode = response.body().getErrCode();

                        if (statusCode == 0) {
                            loading.dismiss();
                            scrollView.setVisibility(View.VISIBLE);
                            ShowData(response.body().getData());



                        }
                        else {
                            loading.dismiss();
                            try {
                                messageDialog.mShowMessageError(DetailNewsActivity.this,getResources().getString(R.string.title_gagal)
                                        , getResources().getString(R.string.loading_error));
                            }catch (Exception r){

                            }
                        }

                    } else {
                        loading.dismiss();
                        try {
                            messageDialog.mShowMessageError(DetailNewsActivity.this,getResources().getString(R.string.title_gagal)
                                    , getResources().getString(R.string.loading_error));
                        }catch (Exception r){

                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailNews> call, Throwable t) {
                    loading.dismiss();
                    try {
                        messageDialog.mShowMessageError(DetailNewsActivity.this, getResources().getString(R.string.title_gagal)
                                , getResources().getString(R.string.loading_error));
                    }catch (Exception r){

                    }
                }
            });
        } catch (Exception e) {
            loading.dismiss();
            try {
                messageDialog.mShowMessageError(DetailNewsActivity.this, getResources().getString(R.string.title_gagal)
                        , getResources().getString(R.string.loading_error));
            }catch (Exception r){

            }
        }
    }

    private void ShowData(DataDetailNews data){

        txtHeader.setText(data.getTitle());
        txtDeskripsi.setText(data.getNews());
        txtHeader.setText(data.getTitle());
        if (data.getDate() != null) {
            txtDate.setText(DefaultFormatter.changeFormatDateWithOutHour(data.getDate()));
        } else {
            txtDate.setText("");;
        }





        RequestOptions requestOptions = new RequestOptions();
        //  requestOptions.placeholder(R.drawable.warnawarni);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);





        if (data.getPath() != null) {


            Glide.with(DetailNewsActivity.this)
                    .load((getResources().getString(R.string.base_url) + data.getPath()))
                    .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                    .apply(requestOptions)
                    .error(Glide.with(mImageView).load(R.drawable.no_picture))
                    .into(mImageView);

        }
        else {
            mImageView.setVisibility(View.GONE);
        }




    }
    @Override
    public void onResume(){
        super.onResume();

        MyContextWrapper.refreshBahasa(DetailNewsActivity.this);


    }

    protected void attachBaseContext(Context newBase) {

        String bahasa = (PreferenceManager.getDefaultSharedPreferences(newBase).getString(
                newBase.getResources().getString(R.string.pref_bahasa), ""));

        if (bahasa.equals("") || bahasa.equals(null)){

            bahasa = "en";
        }
        Context context = changeLang(newBase, bahasa);
        super.attachBaseContext(context);

    }


}