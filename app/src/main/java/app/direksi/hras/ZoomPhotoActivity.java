package app.direksi.hras;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;


/**
 * Created by dhimaz on 10/03/2018.
 */

public class ZoomPhotoActivity extends AppCompatActivity {

    //PhotoViewAttacher pAttacher;
    TouchImageView frag_imageview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().hide();
        setContentView(R.layout.activity_zoom);
        Intent intent = getIntent(); // get Intent which we set from Previous Activity
        RequestOptions requestOptions = new RequestOptions();
        frag_imageview = (TouchImageView) findViewById(R.id.frag_imageview);

        //requestOptions.placeholder(R.drawable.ic_logo);
        /*requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);*/
        /*requestOptions.fitCenter();*/
        requestOptions.placeholder(R.drawable.logobaru);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.skipMemoryCache(true);
        String url = (intent.getStringExtra("image"));
        //Toast.makeText(getApplicationContext(), url, Toast.LENGTH_LONG).show();
      //  Log.v("Campaignssssss", url);
      //  Glide.with(getApplicationContext()).load(url).apply(requestOptions).into(selectedImage);// set logo images
        Glide.with(getApplicationContext())
                .load(url.contains("warnawarni") ? url : getResources().getString(R.string.base_url) + url)
                .thumbnail(Glide.with(getApplicationContext()).load(R.drawable.loooading_icon))
                .apply(requestOptions)
                .into(frag_imageview);// set logo images




    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
